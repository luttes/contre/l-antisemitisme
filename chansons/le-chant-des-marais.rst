.. index::
   pair: Chant ; chant des marais (ou Le Chant des déportés)

.. _chant_des_marais:

==================================================
**Le chant des marais (ou Le Chant des déportés)**
==================================================

- https://projet-canto.fr/chants/chant-des-marais/
- https://fr.wikipedia.org/wiki/Le_Chant_des_d%C3%A9port%C3%A9s


Description
=============


Le Chant des déportés ou Chant des marais (en allemand Wir sind die
Moorsoldaten (« nous sommes les soldats du marais »), Moorsoldatenlied,
« chanson des soldats du marais », ou Börgermoorlied, « chant de Börgermoor » ou )
est l'adaptation en français d'un chant allemand composé en 1933 par
des prisonniers communistes du camp de concentration de Börgermoor,
dans le Pays de l'Ems, en Basse-Saxe.

Ce chant de déportés allemands a intégré le répertoire militaire français
ainsi que le répertoire scout, sous le nom **de Chant des marais**.


Paroles
==========

I
---

::

    Loin vers l'infini s'étendent
    De grands prés marécageux
    Et là-bas nul oiseau ne chante
    Sur les arbres secs et creux

Refrain
---------

::

    Ô terre de détresse
    Où nous devons sans cesse
    Piocher, piocher.

II
-----

::

    Dans ce camp morne et sauvage
    Entouré de murs de fer
    Il nous semble vivre en cage
    Au milieu d'un grand désert.

III
-----

::

    Bruit des pas et bruit des armes
    Sentinelles jours et nuits
    Et du sang, et des cris, des larmes
    La mort pour celui qui fuit.

IV
-----

::

    Mais un jour dans notre vie
    Le printemps refleurira.
    Liberté, liberté chérie
    Je dirai : « Tu es à moi. »

Dernier refrain
---------------------

::

    Ô terre enfin libre
    Où nous pourrons revivre,
    Aimer, aimer.


Interprétations
=================

- :ref:`chant_des_marais_2023_11_09`
- :ref:`chant_des_marais_2023_04_16`
