.. index::
   ! Glossaire

.. un·e

.. _glossaire:

===================================================================================
Glossaire
===================================================================================

.. glossary::


   antisémitisme
   Antisémitisme
       - https://fr.wikipedia.org/wiki/Antis%C3%A9mitisme

       L’antisémitisme est la discrimination et l'hostilité manifestées
       à l'encontre des Juifs en tant que groupe ethnique, religieux ou
       supposément racial.

       Étymologiquement, ce terme pourrait s'appliquer aux peuples
       sémites parlant l’une des langues sémitiques (comme l'arabe ou l'amharique)
       mais **il désigne, dès sa formulation vers la fin du XIXe siècle,
       une forme de racisme à prétentions scientifiques et visant spécifiquement
       les Juifs**.

       Les motifs et mises en pratique de l'antisémitisme incluent divers
       préjugés, des allégations, des mesures discriminatoires ou d’exclusion
       socio-économique, des expulsions, des massacres d’individus ou de
       communautés entières.

       Voir aussi : :ref:`jjr:jjr_2024_02_25`

       L’antisémitisme, comme le racisme et toutes les oppressions, est un
       abandon des autres et une démission de soi (:ref:`raar_2024:stora_def_antisem_2024_03_29`)

       Sur wikipedia

       - https://fr.wikipedia.org/wiki/Antis%C3%A9mitisme#Origine_et_d%C3%A9finitions

       Le mot "Antisemitismus" traduit ici par"antisémitisme » est
       introduit dans le discours politique en 1879 par le journaliste
       allemand Wilhelm Marr.
       Rejetant le concept d'une assimilation possible, il plaide pour une
       expulsion de tous les Juifs vers la Palestine.

       Les définitions:

       - :ref:`declaration_jerusalem`
       - :ref:`def_ihra`

       Les avantages de la Déclation de Jérusalem par rapport à la définition de l’IHRA ?

       - :ref:`dja_versus_ihra`
       - :ref:`faq_dja_ihra_replacement`

   antisionisme
       Chaim Weizmann définissait l’antisémitisme comme la détestation des Juifs
       plus que nécessaire.
       Je (Denis Charbit) marche dans ses pas en disant que l’antisionisme consiste à détester Israël
       plus que nécessaire.

   autosémitisme
       Néologisme créé par les complotistes soraliens (Soral 2019) pour suggérer que l'antisémitisme
       ou sa recrudescence sont l'oeuvre de ceux qui prétendent en être
       les victimes, le terme "autosémitisme" permet de contester la
       réalité de l'antisémitisme... tout en en attribuant la responsabilité
       aux Juifs eux-mêmes.

       Cela fait plusieurs années qu'Alain Soral et ses séides tentent d'imposer le
       terme "autosémitisme". En février 2019, l'une des premières occurrences
       de ce néologisme apparaît sur son site, Égalité & Réconciliation.

       Voir https://www.conspiracywatch.info/autosemitisme-chronique-numerique-dun-coup-de-force-semantique.html

   CAA
   Campaign Against Antisemitism

       Campaign Against Antisemitism is a volunteer-led charity dedicated
       to exposing and countering #antisemitism through education and
       #zerotolerance law enforcement

       - https://antisemitism.org/
       - https://fediverse.com/antisemitism


   campisme
       Celui-ci est une sorte de pensée binaire qui divise le monde en deux camps rivaux,
       qui voit dans l’impérialisme américain l’ennemi principal et qui
       refuse volontairement de prendre en considération d’autres aspects qui
       permettraient de ne pas réduire le monde à deux camps rivaux, opposant des
       "bons" et des "méchants" : d’un côté, vous valorisez, en toutes
       circonstances, le camp du "Bien", de l’autre vous délégitimez le camp
       du "Mal". C’est ainsi qu’un grand nombre de luttes émancipatrices,
       le féminisme et d’autres luttes pour la liberté qui n’entrent pas dans ce
       schéma binaire, sont reléguées au second plan. Dans le cas présent, le camp
       du "Mal" est l’impérialisme américain, ainsi que son allié, le sionisme.

       Voir

       - :ref:`raar_2024:campisme_2024_02_09`
       - :ref:`raar_2024:bulle_campisme_2024_03_11`


   complotisme
       - :ref:`faq_dja_complotisme`
       - :ref:`ligne_2_complotisme`

   confusionnisme
       Le confusionnisme politique c’est-à-dire la fusion (souvent
       inconsciente) de récits qui s’inspirent de représentations ou d’idées
       de gauche et de représentations ou d’idées réactionnaires/d’extrême
       droite - **fait un tort énorme à la gauche**.

       Il est l’expression **d’une politique de plus en plus polarisée**, de points
       de vue hypercritiques pour attirer l’attention (notamment sur les médias sociaux),
       d’attaques personnelles et de diabolisation au lieu de critiques économiques
       et politiques systémiques.

       Voir:

       - :ref:`raar_2024:confusionnisme_2024_09_12`
       - :ref:`raar_2024:confusionnisme_2024_02_09`
       - :ref:`la_grande_confusion`
       - :ref:`raar_2024:confusionnisme_2024_04_29`


   appel du pied
   langage codé
   dilogie
   dog whistle
   dog whistling
       En politique, un appel du pied, dilogie ou sous-discours
       (en anglais dog whistle, sifflet à chien), est l'utilisation d'un
       langage codé ou suggestif dans les messages politiques pour obtenir
       le soutien d'un groupe particulier sans provoquer d'opposition.

       Un appel du pied utilise un langage qui semble normal à la majorité
       mais adresse des messages spécifiques au public visé.

       Ils sont généralement utilisés pour porter des notions sur des
       sujets susceptibles de susciter la controverse sans attirer une
       attention négative.

       Ainsi l'expression "valeurs familiales » aux États-Unis pour
       signaler aux chrétiens qu'un candidat soutient des politiques
       promouvant les valeurs chrétiennes sans s'aliéner les partisans
       non chrétiens.

       Un autre exemple est l'utilisation de l'expression "banquiers internationaux »
       pour signaler aux racistes qu'un candidat est lui-même antisémite
       sans s'aliéner les partisans non racistes.

       Voir :ref:`dog_whistles`

   diatribe
       Écrit ou discours dans lequel on attaque, sur un ton violent et souvent
       injurieux, quelqu'un ou quelque chose; critique violente

   double allégeance
       Un des tropes antisémites les plus répandus aujourd'hui c'est celui
       de la "double allégeance"

       C'est l'idée comme quoi les juifves de la Diaspora seraient en
       fait des excroissances d'une "entité sioniste" à l'échelle internationale.


   EHRC
   Equality and Human Rights Commission

       Promoting and upholding equality and human rights ideals and laws
       across England, Scotland and Wales.

       The Equality and Human Rights Commission is Great Britain’s national
       equality body and has been awarded an ‘A’ status as a National
       Human Rights Institution (NHRI) by the United Nations.

       Our job is to help make Britain fairer. We do this by safeguarding
       and enforcing the laws that protect people’s rights to fairness,
       dignity and respect.

       - https://www.equalityhumanrights.com/en
       - https://www.equalityhumanrights.com/en/about-us/who-we-are
       - https://fediverse.com/EHRC
       - https://www.equalityhumanrights.com/sites/default/files/investigation-into-antisemitism-in-the-labour-party.pdf


   Genocide
       Génocide dans le code pénal français https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000006417533

       Constitue un génocide le fait, en exécution d'un plan concerté
       tendant à la destruction totale ou partielle d'un groupe national,
       ethnique, racial ou religieux, ou d'un groupe déterminé à partir
       de tout autre critère arbitraire, de commettre ou de faire commettre,
       à l'encontre de membres de ce groupe, l'un des actes suivants :

       - atteinte volontaire à la vie ;
       - atteinte grave à l'intégrité physique ou psychique ;
       - soumission à des conditions d'existence de nature à entraîner la destruction totale ou partielle du groupe ;
       - mesures visant à entraver les naissances ;
       - transfert forcé d'enfants.

       Le génocide est puni de la réclusion criminelle à perpétuité.
       Les deux premiers alinéas de l'article 132-23 relatif à la période
       de sûreté sont applicables au crime prévu par le présent article.

       - https://www.mediapart.fr/journal/international/100824/nous-sommes-tres-loin-de-la-reconnaissance-de-genocide

       Philippe Sands, Retour à Lemberg, livre sur les origines des concepts
       de génocide et de crime contre l'humanité dans le droit international,
       en parallèle à l’histoire de sa propre famille,

       vidéo Albin Michel https://youtu.be/OCDOQH-DXP8 (2018-04-13)

   Gaslighting

       Definition santemagazine

           Source: https://www.santemagazine.fr/psycho-sexo/psycho/gaslighting-comprendre-cette-technique-de-manipulation-1029828

           Le gaslighting est un terme employé en psychologie pour désigner une
           technique de manipulation mentale visant à faire douter une personne
           d’elle-même.
           Le but ?
           Tirer profit de l’anxiété et de la confusion générées.

           Les moyens employés pour y parvenir sont nombreux.
           On retrouve notamment le mensonge, le déni, la déformation de faits,
           ou encore l’omission sélective.

           Néanmoins, ces dernières années, le gaslighting est devenu une expression
           du langage courant pour désigner plus largement le fait d’induire
           quelqu’un en erreur afin d’en tirer profit.
           Dans cet usage, le terme vise les formes modernes d’influence comme les
           fake news, les deepfakes, et l’intelligence artificielle.

           À ce titre, le célèbre dictionnaire anglophone Merriam-Webster a révélé
           que gaslighting était son **mot de l’année 2022**.

           Il figurait parmi les termes les plus recherchés en ligne l’année dernière.

       Definition fr.wikipedia
           Source: https://fr.wikipedia.org/wiki/Gaslighting

            Le gaslighting ou gas-lighting, connu sous le nom de détournement
            cognitif au Québec, est une forme d'abus mental dans lequel
            l'information est déformée ou présentée sous un autre jour, omise
            sélectivement pour favoriser l'abuseur, ou faussée dans le but
            de faire douter la victime de sa mémoire, de sa perception et
            de sa santé mentale.

            Les exemples vont du simple déni par l'abuseur de moments pénibles
            qu'il a pu faire subir à sa victime, jusqu'à la mise en scène
            d’événements étranges afin de la désorienter.

            Le terme provient de la pièce Gas Light (en) et de son adaptation
            cinématographique.
            Depuis, le terme a été utilisé dans le domaine clinique et la
            littérature spécialisée.

   instrumentalisation
       Une Citation::
           "Cependant cette question (l'antisionisme) n’est pas principale,
           le principal étant la lutte contre l’antisémitisme.
           Il y a aussi une instrumentalisation de l’instrumentalisation".

   GT
       Groupe de Travail


   Jewish Labour Movement

       The Jewish Labour Movement, précédemment connu sous le nom de Poale Zion
       (Britain) de 1903 à 2004, est l’une des plus anciennes organisations
       socialistes ("socialist societies ») affiliées au parti travailliste britannique.

       The Jewish Labour Movement is a membership organisation of Labour
       supporting members of the Jewish Community and a formal affiliate
       of the Labour Party in the Uk since 1920

       - https://www.jewishlabour.uk/
       - https://fediverse.com/JewishLabour



   Jewish Voice for Labour
       The Jewish Voice for Labour est une organisation créée en 2017 au sein
       du Labour pour ses membres de confession juive, visant à lutter
       contre toute forme de racisme et d’antisémitisme.

       Priorities: universal human rights & dignity;justice for all;
       freedom of expression; democracy in the Labour Party

       - https://www.jewishvoiceforlabour.org.uk/
       - https://fediverse.com/jvoicelabour

       - https://www.jewishvoiceforlabour.org.uk/article/institutional-racism-the-case-of-the-labour-party/

   ILT
   Indice longitudinal de tolérance
       L’indice longitudinal de tolérance a été créé en 2008 par
       vincent tiberj selon la méthode élaborée par le politiste américain
       James Stimson.

       Son objectif est de mesurer de manière synthétique les évolutions de
       l’opinion publique à l’égard de la tolérance à la diversité avec une
       mesure comparable dans le temps

       - :ref:`ilt_2023_04_24`

   LAAS
   Labour Against Antisemitism

       Labour Against Antisemitism was started by Labour members to combat
       what we recognised as the rise of anti-Jewish sentiment in the
       Labour Party.

       We are a voluntary organisation and we hold fighting antisemitism
       as our main aim above all else and any other allegiances.

       Our aim is to eradicate antisemitism from the Party completely and
       become a redundant organisation. However, it appears that this is
       an unlikely scenario in the near future, so we expose and report
       Labour antisemitism in the hope it will become socially unacceptable.

       Labour antisemitism can appear as Soviet era style racism, far
       right style racism, Medieval racism or religious racism.
       Whichever guise it appears in, the effect is the same for the
       Jewish community - racist abuse.



       - https://labouragainstas.org.uk/
       - https://fediverse.com/LabourAgainstAS

   lieu commun
       https://fr.wikipedia.org/wiki/Lieu_commun

           Dans le langage courant, "lieu commun" est une expression péjorative,
           qui disqualifie un discours en affirmant qu'il n'est fait que d'idées
           reçues, qu'il montre l'absence d'originalité de la pensée et surtout
           d'invention rhétorique.

           En ce sens, "lieu commun" se remplace aisément, faute de métaphore
           nouvelle, par les termes poncif ou cliché.
           Dans un sens encore plus vague, on parle de platitude, de banalité,
           de truisme ou de lapalissade

   méphitique
   méphitiques
       Qui corrompt l'âme, l'esprit


   Nuit de Cristal
       Une expression que nous rejetons

       Malheureusement c'est le terme qui s'est imposé dans l'histoire que tout
       le monde utilise mais c'est un terme qui a été inventé par les nazis
       pour euphémiser la violence du pogrom, du massacre, de la destruction qu'ils
       ont réalisé ce jour-là

       - :ref:`nuit_de_cristal_2023_11_09`

   November Pogrom
   November Reich pogrom
      Le nom allemand correspondant à la :term:`Nuit de Cristal`

       - :ref:`november_pogrom_2023_11_09_130`

   polarisation
      Definition 1
          On décrit souvent un monde politique "radicalisé" alors qu’il est en
          fait "polarisé". Les deux phénomènes ne sont pas similaires, et ont
          des implications différentes sur le plan de notre rapport au politique et
          aux événements politiques.

          La notion de polarisation politique n’est pas nouvelle. Le politiste italien
          Giovanni Sartori a étudié le "pluralisme polarisé" dès les années 1960.

          Parmi les critères retenus dans sa définition, Sartori identifiait
          la présence de partis "antisystème", l’existence d’oppositions binaires,
          un rapport idéologique à l’action politique, et une surenchère rhétorique.

          Il notait enfin que les régimes politiques polarisés étaient caractérisés
          par des écarts idéologiques incommensurables entre les principaux partis.

          Depuis quelques années, il existe un intérêt renouvelé pour l’étude de
          la polarisation politique. De nombreux articles et ouvrages académiques ont
          redéfini la notion.

          Contrastant avec la définition ancienne de Sartori, la polarisation ne
          désigne plus aujourd’hui les attaques de partis "extrémistes" contre la
          démocratie et le mainstream politique, mais renvoie à des conflits entre
          partis mainstream autour de leur engagement démocratique.

          Plus précisément, **il n’existerait plus de consensus sur ce que constitue
          la démocratie, ses valeurs et comment il convient de la défendre**
          Contrastant avec la définition ancienne de Sartori, la polarisation ne
          désigne plus aujourd’hui les attaques de partis "extrémistes" contre la
          démocratie et le mainstream politique, mais renvoie à des conflits entre
          partis mainstream autour de leur engagement démocratique.

          Plus précisément, **il n’existerait plus de consensus sur ce que constitue
          la démocratie, ses valeurs et comment il convient de la défendre**.

           Voir :ref:`raar_2024:polarisation_2024_02_12`

      Définition 2
          La polarisation n’est pas synonyme de radicalité, mais de conflictualité.
          La radicalité (du latin radix, la racine) est un processus de transformation
          profonde des structures sociales.
          Elle est donc très souvent de gauche, comme les révolutionnaires qui renversèrent
          l’Ancien Régime en 1789.

          **La polarisation, phénomène discursif, est un terrain qui favorise au contraire
          le conservatisme, le sexisme ou le racisme**.

          Voir :ref:`raar_2024:radicalite_2024_02_12`

   porc
       On peut qualifier une personne non-juive de "porc" sans que cela soit antisémite.
       Mais qualifier un Juif de porc est sans conteste une insulte antisémite.
       C'est même l'une des plus vieilles insultes antisémites.

       Du fait de l'interdit alimentaire (les Juifs ne mangent pas de porc),
       des personnes non-juives considèrent que les Juifs appartiennent à la
       race des porcs; que ce sont effectivement des porcs.

       La bestialisation est clairement antisémite et cette association est immémoriale.

   pogrom
       https://invidious.fdn.fr/watch?v=XE3gzRB7q_E
       "(7 octobre 2023: peut-on parler de pogrom ?" Par Marie Moutier-Bitan

       Peut-on parler de pogrom pour désigner les massacres commis en
       Israël ?
       L'historienne Marie Moutier-Bitan, spécialiste de la Shoah à l'est
       de l'Europe, retrace l'étymologie de ce terme et explique pourquoi
       son usage actuel lui parait pertinent.


   poncif
   poncifs
       https://www.cnrtl.fr/definition/poncif

           **Expression ou oeuvre (littéraire, artistique, etc.), banale, de
           routine, copiant manifestement un modèle et dépourvue de toute
           originalité**

       https://fr.wikipedia.org/wiki/Poncif
           En linguistique, un poncif est un lieu commun.

   racisme
       Car si les Juifs ne sont certes pas une race, c’est aussi le cas de tous
       les autres groupes humains frappés par le racisme.

       Pour autant, la notion de race reste présente dans les textes internationaux –
       même si ce fut récemment débattu – car en la supprimant, on supprimerait
       celle de racisme.

       Race ne veut donc pas dire race réelle mais construction toujours à l’œuvre.

       Pour tous les groupes exposés au racisme, on n’admettra donc que si les
       races n’existent pas, le racisme lui, demeure.
       On admettra, sans trop chercher à creuser la question, qu’ils ont une
       identité, au demeurant difficilement qualifiable – ethnique, religieuse,
       de phototype? – et on ne leur demandera pas de le prouver

   sédévacantisme
   sédévacantiste
       https://fr.wikipedia.org/wiki/S%C3%A9d%C3%A9vacantisme

       Le sédévacantisme (de l'expression latine sede vacante signifiant
       "le siège [étant] vacant », utilisée entre la mort ou la renonciation
       d'un pape et l'élection de son successeur1) est une position religieuse
       défendue par une minorité de catholiques du courant traditionaliste
       se regroupant dans de multiples chapelles.

       Elle affirme que, depuis 1958 (mort de Pie XII) ou 1963 (mort de Jean XXIII),
       le siège du pape est vacant.

       D'autres encore estiment le début du sédévacantisme à 1965, à la
       déclaration Dignitatis Humanae sur la liberté religieuse.

       La position sédévacantiste comprend différentes déclinaisons :
       le sédévacantisme, qui considère les papes contemporains comme des
       usurpateurs, le sédéprivationnisme et le catholicisme semper idem

       "Quelqu’un qui aujourd’hui explique que les Juifs ont tué Jésus
       c’est quelqu’un qui ne connaît pas Vatican II, qui est au minimum
       sédévacantiste autrement dit un tenant de théories qui sont totalement
       marginales au sein de l'Eglise catholique"


       voir :ref:`raar_2023:bruttmann_melenchon_2023_12_18_949`

   trope
   tropes
       https://www.cnrtl.fr/definition/trope

       [Dans l'anc. rhét.] Figure par laquelle un mot prend une signification
       autre que son sens propre.
       Les Tropes sont certains sens plus ou moins différens du sens primitif,
       qu'offrent, dans l'expression de la pensée, les mots appliqués à
       de nouvelles idées (P. Fontanier, Les Figures du disc., Paris, Flammarion, 1968, [1821], p. 39).

       Terme de Rhétorique. Emploi d'une expression en sens figuré.
       Cent voiles pour cent vaisseaux, cent chevaux pour cent cavaliers,
       Ce sont des tropes.

       Un des tropes antisémites les plus répandu aujourd'hui c'est celui
       de la "double allégeance"
