.. index::
   ! Mémorial de la Shoah

.. _memorial_shoah:

===================================================================================
**Mémorial de la Shoah** Paris 4e
===================================================================================

- https://www.memorialdelashoah.org/
- https://fr.wikipedia.org/wiki/M%C3%A9morial_de_la_Shoah
- http://www.memorialdelashoah.org/newsletter.html
- https://www.youtube.com/@memorialdelashoah/videos


Adresse
========

- 17 rue Geoffroy-l'Asnier 75004 Paris

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3545637726783757%2C48.85422922896889%2C2.358104288578034%2C48.85566401673969&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.85495/2.35633">Afficher une carte plus grande</a></small>


Description
==============

- https://fr.wikipedia.org/wiki/M%C3%A9morial_de_la_Shoah

Le Mémorial de la Shoah est un lieu de mémoire du génocide des Juifs,
situé à Paris en France. Il réunit dans un même lieu :

- un musée consacré à l'histoire juive durant la Seconde Guerre mondiale
  dont l'axe central est l'enseignement de la Shoah.

  Ce musée a ouvert ses portes en janvier 2005, dans le quartier du Marais
  (4e arrondissement de Paris) ;

- plusieurs « lieux de mémoire » : le tombeau du martyr juif
  inconnu (dans la crypte), le Mur des Noms, le mémorial des enfants,
  et le Mur des Justes ;
- :ref:`le Centre de documentation juive contemporaine (CDJC) <cdjc>`.

En face de `l'ancien camp d'internement de La Muette à Drancy <https://fr.wikipedia.org/wiki/Camp_de_Drancy>`_, une antenne du
Mémorial de la Shoah a été ouverte en 2012.


Historique du mémorial
============================

En avril 1943, pendant la Seconde Guerre mondiale à Grenoble,
Isaac Schneersohn  et Léon Poliakov fondèrent clandestinement le Centre
de documentation juive contemporaine, dans le but de réunir des preuves
documentaires sur la destruction des Juifs d'Europe.

En 1957, fut inauguré le Mémorial du Martyr juif inconnu.

**En janvier 2005, l'ensemble du site avec le Mur des Noms prend le nom de
Mémorial de la Shoah**.

Le 21 septembre 2012, un mémorial situé sur le site de l’ancien camp de
Drancy a été inauguré par François Hollande, président de la République
française.

Le mémorial est présidé depuis 2005 par Éric de Rothschild.


Le « Mur des Justes » L’Allée des Justes et le mémorial
============================================================

En bordure du mémorial, dans l'allée des Justes, un « Mur des Justes »
rend hommage aux Justes de France, dont il présente la liste par année
(de nomination au titre de Juste) et par ordre alphabétique. Ce « Mur des
Justes » a été réalisé par Antoine Jouve, Simon Vignaud et Anne Sazerat,
architectes et Bernard Baissait, graphiste. Il est inauguré le 14 juin 2006,
en même temps qu'une exposition temporaire consacrée aux Justes.

Il y a, en 2015, 3 899 Justes de France3, mais seuls 3 853 noms figurent sur
le mur, la dernière mise à jour datant de 2014.


Le Mur des Noms
===================

- https://fr.wikipedia.org/wiki/Mur_des_Noms

Le Mur des Noms, taillé dans des pierres provenant de Jérusalem, se trouve
à l'entrée du mémorial. Son rôle est de garder la mémoire des 75 568
juifs français et étrangers, dont 11 400 enfants, déportés de France.

Les noms sont classés par année, de 1942 à 1944 et par ordre alphabétique.

Le service des archives du :ref:`CDJC <cdjc>` fait régulièrement des mises
à jour : lorsqu'il retrouve le nom d'autres personnes juives déportées
de France ou constatent qu'un nom a été mal orthographié sur les listes
de déportation, les noms sont gravés en fin de liste avec comme introduction
« Le Nom des déportés découverts après l'inscription ».

Ceux qui souhaitent se recueillir peuvent déposer des bougies, des fleurs
ou des cailloux au pied des murs où les noms sont inscrits.

Le Mur des Noms a été inauguré le 25 janvier 2005 par le président Jacques
Chirac.

Librairie
============

Après le Mur des Noms, après l'entrée, un peu au fond une librairie
spécialisée sur la Shoah, le nazisme, `l'antisémitisme <https://fr.wikipedia.org/wiki/Antis%C3%A9mitisme>`_ et l'histoire du peuple juif.


.. _memorial_des_enfants:

**Le Mémorial des enfants**
=============================

- :ref:`resistance_gre:portrait_des_enfants_deportes`

Dans la salle du Mémorial des enfants, 3000 photographies d'enfants juifs
déportés depuis la France sont présentées les unes à côté des autres.
