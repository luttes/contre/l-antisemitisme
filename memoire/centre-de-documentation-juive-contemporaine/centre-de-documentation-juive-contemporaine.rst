.. index::
   ! Centre de documentation juive contemporaine

.. _cdjc:

===================================================================================
**Centre de documentation juive contemporaine (CDJC)**
===================================================================================

Description
==============

- https://fr.wikipedia.org/wiki/Centre_de_documentation_juive_contemporaine

Le Centre de documentation juive contemporaine (CDJC) était une organisation
française créée par Isaac Schneersohn durant la Seconde Guerre mondiale.

Elle avait pour but de collecter et préserver les preuves des exactions
nazies envers la communauté juive lors de cette période, en vue du devoir
de mémoire ainsi que des procès contre d’anciens criminels de guerre ou
pour la restitution de biens spoliés.

Le CDJC a été fusionné avec le Mémorial du martyr juif inconnu pour fonder
le Mémorial de la Shoah, qui a ouvert ses portes le 27 janvier 2005.


Création
===========

:ref:`Isaac Schneersohn <isaac_schneersohn>` **fonde le Centre à Grenoble en avril 1943**.

:ref:`Léon Poliakov <leon_poliakov>`, Joseph Billig ou Lucien Steinberg
participent à l’organisation.

À la réunion de fondation dans son appartement rue Bizanet, se retrouve
la plupart des organisations juives de l'époque comme l'UGIF, l'OSE, l'ORT.

Il semble que l'objectif de départ n'était pas d'écrire l'histoire des
persécutions subies par les Juifs mais de préparer leur réintégration
dans la société française après-guerre.

Des documents comme la liste des biens aryanisés sont rassemblés.

À la fin de la guerre, Isaac Schneersohn charge Léon Poliakov de récupérer
les archives allemandes en France que ceux-ci ont abandonnées en fuyant
Paris.

Ce dernier, muni d'une lettre de recommandation d'un ancien ministre de
la IIIe République, peut récupérer une caisse de bois contenant les
archives SS en France.

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.730233788490295%2C45.195276869219754%2C5.737314820289613%2C45.198350142671444&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.19681/5.73377">Afficher une carte plus grande</a></small>


Les archives du CDJC sont rapidement enrichies.

Ainsi en 1947, elles sont composées entre autres de celles de la Gestapo,
des services d'Otto Abetz à l'ambassade d'Allemagne, du gouvernement de Vichy,
de l'état-major allemand, du commissariat aux questions juives...

On trouve aussi 75 000 fiches où sont recensés les biens volés aux Juifs.

**Ce fonds documentaire très riche** a permis l'établissement de dossiers
individuels pour des pensions dans le cadre de la politique de réparation
de l'Allemagne.

**Il a aussi servi au procès Eichmann à Jérusalem et à celui de Klaus Barbie
en France**.


Travail du CDJC
===================

Le CDJC effectue dès le début un véritable travail historique grâce à des
chercheurs travaillant pour le centre comme Poliakov ou Billig.

Georges Wellers y travaille de façon bénévole, édite des livres et une
revue dès 1945.

Le travail du centre est réparti dans plusieurs commissions (justice, camps…).

La commission des camps établit une chronologie minutieuse du sort des
Juifs dans les camps français.

Localisation
================

En 1945, le siège du centre est 10, rue Marbeuf Paris VIII, le secrétariat
général, 9, rue Notre Dame des Victoires, Paris II.

Le Centre est depuis 1997 partie intégrante du Mémorial de la Shoah (situé à Paris),
à la suite de la fusion entre le Centre et le Mémorial de la Shoah, fondé en 1956.
