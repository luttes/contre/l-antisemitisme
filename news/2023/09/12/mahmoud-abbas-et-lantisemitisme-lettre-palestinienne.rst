
==========================================================================
2023-09-12 Mahmoud Abbas et l'antisémitisme - Lettre palestinienne
==========================================================================

- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/120923/mahmoud-abbas-et-lantisemitisme-lettre-palestinienne


Un ensemble d'universitaires, écrivains, artistes, activistes palestinien·nes et gens de tous milieux condamnent les commentaires du Président de l’Autorité Palestinienne à propos de l’holocauste, selon qui le génocide avait été causé par « le rôle dans la société [des juifs] ». Le génocide est « né de l’antisémitisme, du fascisme ». « Le peuple palestinien souffre suffisamment du fardeau du colonialisme israélien, sans avoir à supporter les narratifs ignorants et antisémites propagés par ceux qui prétendent parler en notre nom. »
Les invités de Mediapart (avatar)

Les invités de Mediapart

Dans cet espace, retrouvez les tribunes collectives sélectionnées par la
rédaction du Club de Mediapart.

Nous, soussignés, universitaires, écrivains, artistes, activistes palestiniens
et gens de tous milieux, condamnons sans équivoque les commentaires moralement
et politiquement répréhensibles du Président de l’Autorité Palestinienne,
Mahmoud Abbas sur l’holocauste.

Ancré dans une théorie raciale largement répandue dans la culture et la
science européennes à l’époque, le génocide nazi du peuple juif est né
de l’antisémitisme, du fascisme et du racisme.

Nous rejetons catégoriquement toute tentative de réduire, déformer ou
justifier l’antisémitisme, les crimes nazis contre l’humanité ou le
révisionnisme historique vis-à-vis de l’holocauste.

Le peuple palestinien souffre suffisamment du fardeau du colonialisme
israélien de peuplement, de la dépossession, de l’occupation et de
l’oppression sans avoir à supporter l’effet négatif de narratifs aussi
ignorants et profondément antisémites propagés par ceux qui prétendent
parler en notre nom.

Nous sommes également touchés par le régime de plus en plus autoritaire
et draconien de l’Autorité palestinienne qui a un effet disproportionné
sur ceux vivant sous occupation.

Resté au pouvoir près d’une décennie après l’expiration de son mandat
présidentiel qui a pris fin en 2009, soutenu par les forces occidentales
et pro-israéliennes qui cherchent à perpétuer l’apartheid israélien,
Abbas et son entourage politique ont perdu tout crédit dans la représentation
du peuple palestinien et de notre lutte pour la justice, la liberté et l’égalité,
une lutte qui s’inscrit contre toute forme de racisme et d’oppression systémiques.
