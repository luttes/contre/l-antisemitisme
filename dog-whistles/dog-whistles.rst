.. index::
   pair: Dog whistles ; Les dog whistles des extrêmes droites (liste non-exhaustive)

.. _dog_whistles:

===================================================================================
**Dog whistles** => langage codé,  appel du pied, dologie
===================================================================================

- :term:`dog whistle`
- https://blogs.mediapart.fr/ilkor/blog/211222/les-dog-whistles-des-extremes-droites-liste-non-exhaustive

.. toctree::
   :maxdepth: 3

   definition/definition
   les-mains-rouges/les-mains-rouges
