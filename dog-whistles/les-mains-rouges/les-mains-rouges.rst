.. index::
   pair: Dog whistle ; Les mains rouges
   pair: Assassinat ; Yosef Avrahami (2000-10-12)
   pair: Assassinat ; Vadim Norznich (2000-10-12)


.. _mains_rouges:

===================================================================================
**Les mains rouges**
===================================================================================

.. figure:: images/sfar.webp


.. tags:: ignorance, science-po


2024-05-14 Debunkers des rumeurs/hoax d'extrême droite **Des mains rouges peintes sur le mur des Justes du mémorial de la Shoah à Paris**
=============================================================================================================================================

🤬 Pourquoi c'est antisémite ?

Des mains rouges peintes sur le mur des Justes du mémorial de la Shoah à Paris.

📌 C'est la découverte faite ce mardi 14 mai 2024 à Paris.

- https://www.francebleu.fr/infos/faits-divers-justice/paris-le-memorial-de-la-shoah-vandalise-dans-la-nuit-de-lundi-a-mardi-4480310

- 1. Quand on vandalise un mémorial, on porte atteinte à la mémoire.
     Peu importe le message véhiculé, il devient secondaire ;
     c'est la démarche qui l'emporte, dont le sens est de toucher à la
     reconnaissance, ainsi qu’à ce qui est commémoré.

     Contrairement à ce que l'on croit, le mémorial est un lieu de vie, un
     espace où l’on entretient la mémoire, et cette dernière est une chose vivante.
     Ici, c'est le mur des Justes qui est touché.

     Les Justes sont ceux qui ont sauvé des Juifs de la déportation au risque
     de leur vie.

     Si leurs noms tiennent sur un mur, c'est que les Justes étaient rares à
     une époque où l'humanité l'était d'autant plus.
- 2. Ensuite, le symbole utilisé, les mains rouges, est devenu ces dernières
     semaines un signe de ralliement pour les étudiants soutenant la cause palestinienne.
     Son origine est discutée :

     - pour Joann Sfar, c'est une référence à l'assassinat  de deux israéliens en 2000,
     - pour les étudiants, c'est un symbole universel.

     Cette différence d'interprétation a été sujette à polémique pendant
     l'occupation de Science-po, et l’ambiguïté est encore plus forte dans
     le cas de cette vandalisation.
- 3. Le 14 mai 1941, c'est "la rafle du billet vert", première grande rafle
     en France. Une résonance plus que douteuse avec cet acte.
     Voir aussi:

     - http://www.memorial98.org/article-memoire-la-rafle-meconnue-du-20-aout-1941-82199443.html
     - https://fr.wikipedia.org/wiki/Rafle_du_billet_vert

- 4. Enfin, l'amalgame.
     Le martyr des gazaouis est de toute évidence la motivation de cette profanation.
     Quel rapport entre les Justes d'hier et le gouvernement israélien d'aujourd'hui ?
     Aucun, si ce n'est tordre les faits et tout mélanger, en tirant un fil
     entre le nazisme et Israël ; voilà ce qui arrive quand on manie ce genre
     de comparaison.
     **Ça ne produit rien de bon, si ce n'est de l'antisémitisme**.



2024-04-28 Pourquoi le symbole des mains rouges, utilisé par des étudiants de Sciences-Po Paris en soutien à la Palestine, fait polémique ? par Anaïs Condomines
=====================================================================================================================================================================

Journaliste Anaïs Condomines
-----------------------------------

- https://www.liberation.fr/auteur/anais-condomines/


Pourquoi le symbole des mains rouges, utilisé par des étudiants de Sciences-Po Paris en soutien à la Palestine, fait polémique ?
----------------------------------------------------------------------------------------------------------------------------------

Gaza, l'engrenage A l’Institut d’études politiques de Paris, des militants
propalestiniens ont exhibé leurs mains peintes en rouge et sont accusés depuis
de reprendre un symbole antisémite.
Auprès de CheckNews, les étudiants assurent qu’ils n’avaient pas connaissance
de cette référence historique

Vendredi 26 avril 2024, dans le cadre du blocus de Sciences-Po Paris commencé la
veille au soir par des étudiants propalestiniens, plusieurs manifestants ont
effectué un geste symbolique : celui de lever vers le ciel leurs mains peintes
en rouge.

Une initiative qui suscite désormais une vive polémique.

Le soir même, des publications mettant en parallèle des images de la manifestation
parisienne et le cliché d’un homme aux mains ensanglantées sont diffusées
sur les réseaux sociaux par des comptes pro-israéliens.
Lesquels accusent les étudiants d’utiliser "un symbole qui a une histoire et une symbolique
d’appel au meurtre" d’Israéliens.


.. _sfar_2024_04_28:

**Ce symbole date du 12 octobre 2000. Ce n’est pas  un appel à la paix, c’est le signe du massacre à mains nues de Yosef Avrahami et Vadim Norznich**
--------------------------------------------------------------------------------------------------------------------------------------------------------

Dès le lendemain, le dessinateur Joann Sfar publie sur `son compte Instagram <https://www.instagram.com/p/C6QaN0iIkTi/?hl=en>`_
une série de croquis.

Le premier d’entre eux montre un homme posté à une fenêtre, surplombant une
foule et exhibant ses deux mains de couleur rouge, ensanglantées.

En légende, il indique : "Ce symbole date du 12 octobre 2000. Ce n’est pas
un appel à la paix, c’est le signe du massacre à mains nues de Yosef Avrahami
et Vadim Norznich."

Un dessin partagé dans la foulée par l’écrivain et chroniqueur Raphaël Enthoven,
qui écrit alors : "A l’attention des incultes.
Le symbole des mains rouges est une référence directe au massacre de deux
Israéliens par la population de Ramallah. Pas un appel au cessez-le-feu."

Comme ils l’expliquent dans leurs publications respectives, Joann Sfar et
Raphaël Enthoven font donc ici référence à un évènement qui s’est déroulé
dans la ville palestinienne de Ramallah, le 12 octobre 2000.

Ce jour-là, deux réservistes israéliens sont tués par les membres d’une foule
palestinienne qui parviennent à s’introduire dans le commissariat où les soldats
sont retenus.
L’un d’entre eux montre à la foule ses mains ensanglantées par la fenêtre,
avant que les corps ne soient, pour l’un jeté dans la foule, pour l’autre pendu.

Ces évènements s’inscrivent dans le contexte du début de la seconde intifada.
A l’époque, Libé a consacré sa une à l’évènement et à la photo choc du meurtrier
haranguant la foule.

Lors de la cérémonie des oscars, plusieurs personnalités avaient arboré un
pin’s représentant une main rouge et suscité une semblable polémique, des
pro-israéliens liant le symbole aux évènements de 2000.

Le 11 mars 2024, une tribune dans le Times of Israël détaille
"ce que les mains rouges sur les pin’s du cessez-le-feu signifient pour
beaucoup d’entre nous qui aimons Israël".

Et son autrice de se souvenir de la découverte de l’image du lynchage, plus de
vingt ans auparavant : "Ses mains, ces deux mains avec cinq doigts chacune,
comme les miennes, comme les vôtres, couvertes du sang d’un autre homme…
un juif.

Juif comme moi.

Donc quand je vois les pin’s des artistes pour le cessez-le-feu avec des mains
rouges dessus, c’est ce à quoi je pense. Je ne peux pas ne pas y penser."
"Chaque Israélien s’en souvient"

De son côté, Hubert Launois, membre du Comité Palestine et étudiant de
Sciences-Po, a tenté de se défendre sur le plateau de BFM TV, en fin de journée,
face à la députée Renaissance des Hauts-de-Seine Maud Bregeon qui a dénoncé des
étudiants au "positionnement douteux", arborant "des slogans et des symboles qui
flirtaient avec l’antisionisme et l’antisémitisme".

Réponse de l’étudiant, à propos des mains rouges : "C’est un symbole qui peut
être choquant, qui est controversé, ça fait référence à des événements tragiques,
effectivement, si ça fait référence à cet événement, **alors c’est une dérive
antisémite qu’il faut commettre…**"
Avant de se corriger précipitamment : "Qu’il faut combattre, pardon."

Auprès de CheckNews ce dimanche, il précise sa pensée : "Le symbole des
mains rouges, c’est un symbole commun pour dénoncer le fait que quelqu’un,
ou qu’une institution, a du sang sur les mains. Il signifie qu’on dénonce
une complicité de crimes, un laisser-faire, et c’était tout notre propos.
Ce symbole est largement utilisé dans les manifestations occidentales, notamment
par des militants écolos, ou même à l’ONU, par des diplomates." Il est vrai
qu’on retrouve souvent ce geste lors d’actions pour le climat. Par exemple,
il était repris par les Amis de la Terre pour alerter sur la sortie du rapport du
Giec, en février 2022. Les mains rouges sont aussi utilisées par des militants
du mouvement Black Lives Matter, la même année. Ou encore, dès la période
1998-1999, soit avant le lynchage de Ramallah, par les anti-Pinochet au Chili.


.. _bruttmann_2024_04_28:

2024-04-28 **Tal Bruttmann, historien de l’antisémitisme, peine à se laisser convaincre par l’argument des étudiants de Sciences-Po**
--------------------------------------------------------------------------------------------------------------------------------------------

Tal Bruttmann, historien de l’antisémitisme, peine à se laisser convaincre
par l’argument des étudiants de Sciences-Po selon lequel ce geste est repris
à d’autres mouvements contestataires :

"Moi, ces images m’ont renvoyé directement au lynchage de Ramallah.
C’est vrai que ce geste peut être utilisé dans d’autres circonstances, notamment par
des militants écolos, **mais ici, on ne peut pas mettre de côté le contexte**.

Sur un sujet aussi tendu, on ne peut pas se débarrasser du contexte en disant
que ce geste renvoyait simplement à autre chose", juge-t-il auprès de CheckNews.

Mais surtout – et c’est un point que Huber Launois, invité sur BFMTV, n’a
pas développé en plateau – certains étudiants de Sciences-Po concernés
assurent à CheckNews qu’ils n’avaient pas connaissance de cette référence
historique. Hubert Launois reprend ainsi : "Je comprends que ça puisse choquer.

On a appris après que cela pouvait renvoyer à l’image d’un lynchage à Ramallah
en 2000. Moi personnellement je l’ignorais, je n’avais pas cette référence,
mes camarades non plus. Je suis né en 2004. En 2000, beaucoup n’étaient pas
nés, ou bien avaient 1 ou 2 ans. Ce n’est pas une image qui parle à notre
génération."

Hubert Launois se dit "désolé" et assure qu’à l’avenir, "il faudra faire attention
à ce symbole.
