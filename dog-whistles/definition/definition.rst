.. index::
   pair: Définition ; Dog whistle


.. _def_dog_whistle:

===================================================================================
Définition Dog whistles, langage codé appel du pied
===================================================================================

Glossaire
================

- :term:`dog whistle`

appel du pied
langage codé
dilogie
dog whistle
dog whistling

   En politique, un appel du pied, dilogie ou sous-discours
   (en anglais dog whistle, sifflet à chien), est l'utilisation d'un
   langage codé ou suggestif dans les messages politiques pour obtenir
   le soutien d'un groupe particulier sans provoquer d'opposition.

   Un appel du pied utilise un langage qui semble normal à la majorité
   mais adresse des messages spécifiques au public visé.

   Ils sont généralement utilisés pour porter des notions sur des
   sujets susceptibles de susciter la controverse sans attirer une
   attention négative.

   Ainsi l'expression « valeurs familiales » aux États-Unis pour
   signaler aux chrétiens qu'un candidat soutient des politiques
   promouvant les valeurs chrétiennes sans s'aliéner les partisans
   non chrétiens.

   Un autre exemple est l'utilisation de l'expression « banquiers internationaux »
   pour signaler aux racistes qu'un candidat est lui-même antisémite
   sans s'aliéner les partisans non racistes.
