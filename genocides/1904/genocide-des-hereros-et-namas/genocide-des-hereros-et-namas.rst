.. index::
   pair: Génocide ; des Héréros et des Namas (1904à

.. _hereros_namas:

========================================
**Génocide des Héréros et des Namas**
========================================

- https://fr.wikipedia.org/wiki/G%C3%A9nocide_des_H%C3%A9r%C3%A9ros_et_des_Namas

Description
==============

Le génocide des Héréros et des Namas perpétré sous les ordres de Lothar
von Trotha dans le Sud-Ouest africain allemand (Deutsch-Südwestafrika,
actuelle Namibie) à partir de 1904 est considéré comme le premier génocide
du XXe siècle.

Ce programme d'extermination s'inscrit au sein d'un processus de conquête
d'un territoire par les troupes coloniales allemandes entre 1884 et 1911.

Il entraîna la mort de 80 % des autochtones insurgés et de leurs familles
(65 000 Héréros et près de 20 000 Namas).
