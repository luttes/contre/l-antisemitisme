.. index::
   pair: Génocide ; Arménien (1915)

.. _genocide_armenien:

========================================
**Génocide arménien**
========================================

- https://fr.wikipedia.org/wiki/G%C3%A9nocide_arm%C3%A9nien

Description
==============

Le génocide arménien (en arménien :Hayots tseghaspanoutyoun ;
en turc : Ermeni Soykırımı), ou plus précisément le génocide des Arméniens,
est un génocide perpétré d'avril 1915 à juillet 1916, voire 1923, au
cours duquel **les deux tiers des Arméniens qui vivent alors sur le
territoire actuel de la Turquie périssent du fait de déportations,
famines et massacres de grande ampleur**.

Il est planifié et exécuté par le parti au pouvoir à l'époque, le Comité
Union et Progrès (CUP), plus connu sous le nom de « Jeunes-Turcs »,
composé en particulier du triumvirat d'officiers Talaat Pacha, Enver Pacha
et Djemal Pacha, qui dirige l'Empire ottoman alors engagé dans la
Première Guerre mondiale aux côtés des Empires centraux.

Il coûte la vie à environ un million deux cent mille Arméniens d'Anatolie
et d'Arménie occidentale.
