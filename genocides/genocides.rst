.. index::
   ! Génocides

.. _genocides:

================
**Génocides**
================

.. https://fr.wikipedia.org/wiki/Discussion:Risque_de_g%C3%A9nocide_%C3%A0_Gaza_depuis_2023#c-Marc-AntoineV-20241008202300-JMGuyon-20241008124200


.. toctree::
   :maxdepth: 3

   1915/1915
   1904/1904
