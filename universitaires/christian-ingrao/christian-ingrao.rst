.. index::
   ! Christian Ingrao

.. _christian_ingrao:

===================================
**Christian Ingrao**
===================================

- https://fr.wikipedia.org/wiki/Christian_Ingrao

Biographie
================


- https://www.tallandier.com/auteur/christian-ingrao/

Christian INGRAO est agrégé d’histoire, docteur de l’université d’Amiens,
Dr. Phil. de l’université de Stuttgart, directeur de recherches au CNRS
et ancien directeur de l’IHTP.

Il est l’auteur de La Promesse de l’Est. Espérance nazie et génocide, 1939-1943 (2016)
et d’une dizaine d’ouvrages individuels et collectifs, traduits en une
dizaine de langues.

Qui êtes-vous Christian Ingrao ? (ABC Penser) 2020-05
=========================================================

.. youtube:: 5cprdPrJYYE

C’est l’amour qui conduit Christian Ingrao à travailler sur les intellectuels
nazis.
Etudiant à Clermont-Ferrand, sa ville natale, il opte pour ce sujet
afin de suivre sa future femme à Paris, car c’est seulement dans cette
ville qu’il trouvera un directeur de mémoire.

Il le trouve à l’arrêt d’un bus, en la personne de Stéphane Audouin-Rouzeau
qui deviendra ensuite son directeur de thèse.

Il devient par la suite chercheur au CNRS, puis directeur de l’IHTP,
un parcours qu’il dit brinquebalant, mais somme toute impressionnant.

Christian Ingrao se livre ici à cœur ouvert.

Livres 2024
==================

- :ref:`monde_nazi_1919_1945`
