.. index::
   pair: Historien ; Simon Perego
   ! Simon Perego

.. _simon_perego:

=======================
**Simon Perego**
=======================

- https://www.sciencespo.fr/histoire/fr/chercheur/Simon%20Perego/79641.html


.. figure:: images/simon_perego.png

   https://www.sciencespo.fr/histoire/fr/chercheur/Zo%C3%A9%20Grumberg/76231.html

Prix et distinctions
========================


- prix de thèse francophone en études juives décerné par la Société des
  études juives, la Commission française des archives juives et la Fondation
  pour la mémoire de la Shoah (2017)
- prix Henri Hertz décerné par la Chancellerie des Universités de Paris (2018)


Cité en 2023
===============

- :ref:`simon_perego_2023_04_16`
