.. index::
   ! Nonna Mayer

.. _nonna_mayer:

===================================
**Nonna Mayer** |NonnaMayer|
===================================

- https://fr.wikipedia.org/wiki/Nonna_Mayer
- https://www.sciencespo.fr/centre-etudes-europeennes/fr/chercheur/nonna-mayer.html
- :ref:`cncdh:rapports_cncdh`


Biographie
=============

Nonna Mayer, née le 25 mars 1948 à Neuilly-sur-Seine, est une chercheuse
en sciences politiques et directrice de recherche émérite au CNRS,
spécialiste de sociologie électorale et notamment de l'extrême droite,
du racisme et de l'antisémitisme.

Elle a été présidente de l'Association française de science politique
de 2005 à 2016.

Rattachée au Centre d'études européennes et de politique comparée de
Sciences Po (CEE), elle est responsable de la série « Contester »
aux Presses de Sciences Po, consacrée aux transformations de
l'action collective.


Recherches
===========

Ses recherches portent sur la sociologie électorale et la participation
politique, sur le racisme et l’antisémitisme.

Elles s'articulent autour de trois axes : la dynamique électorale du
Rassemblement National et des droites radicales populistes en Europe ;
les transformations du racisme et de l’antisémitisme (à partir des données
de la CNCDH) ; les relations entre minorités et population majoritaire
et entre minorités (en particulier entre juifs et musulmans).


Webinaires/Discours 2024
================================

- :ref:`cncdh:rapport_cncdh_2023`

- :ref:`raar_2024:nonna_mayer_2024_12_12`
- :ref:`raar_2024:nonna_mayer_2024_07_09`
- :ref:`raar_2024:nonna_mayer_2024_07_04`

Conférences/Discours 2023
===============================

- :ref:`raar_2023:table_ronde_1_2023_04_24_mayer`
