.. index::
   ! Alain Policar

.. _alain_policar:

===================================
**Alain Policar**
===================================


Biographie
=============

- https://theconversation.com/profiles/alain-policar-326680


Articles 2025
==================

- :ref:`raar_2025:alain_policar_2025_02_25`

Articles 2018
================

- https://theconversation.com/la-democratie-pervertie-un-antisemitisme-sans-antisemites-91012
