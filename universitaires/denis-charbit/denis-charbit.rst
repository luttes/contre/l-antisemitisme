.. index::
   pair: Sionisme ; Spécialiste du sionisme : Denis Charbit
   ! Denis Charbit


.. _denis_charbit:

=========================================================
**Denis Charbit** spécialiste du sionisme
=========================================================

- https://akadem.org/fiche_conferencier.php?id=363
- https://akadem.org/author/denis-charbit
- https://laviedesidees.fr/_Charbit-Denis


Présentation
==============

Denis Charbit a coordonné une anthologie très intéressante de textes,
intitulée :ref:`Sionisme(s). Textes fondamentaux <charbit_livre_sionismes_1998>`, qui
montre la diversité du mouvement sioniste et les discussions qui l'ont
traversé dès le début.


Biographie
=============

Denis Charbit est spécialiste du sionisme, professeur de sciences politiques
à la Faculté des Sciences humaines de l'Open University d’Israël (Raanana).

Il est également titulaire d’un doctorat de l’Université de Tel Aviv.

Ses champs de recherches recouvrent l'histoire intellectuelle de la
France au XXème siècle ainsi que l'histoire des idées et l'histoire
politique d'Israël, l'étude de la société et de la littérature israélienne.

Il est l'auteur, entre autres, de;

- :ref:`charbit_livre_paradoxe_2023`
- :ref:`charbit_livre_paradoxe_2015`
- :ref:`charbit_livre_sionisme_2007`
- :ref:`charbit_livre_sionismes_1998`


Proche des idées du mouvement “La paix maintenant”, il intervient
régulièrement dans les médias français sur la situation au Proche-Orient.

Denis Charbit est professeur de science politique à l’Open University d’Israël.
Ses recherches publiées en français, en hébreu et en anglais, portent, d’une part,
sur le sionisme et, d’autre part, sur les intellectuels français de l’Affaire
Dreyfus à nos jours.

Denis Charbit est directeur de l’Institut de recherche sur les relations
entre juifs, chrétiens et musulmans et professeur de science politique à
l’Open University d’Israël (Ra’anana).

Discours/rencontres 2024
=============================

- :ref:`raar_2024:denis_charbit_2024_09_16`
- :ref:`raar_2024:denis_charbit_2024_09_15`
- :ref:`conflitpal_2024:charbit_2024_06_19`
- :ref:`raar_2024:charbit_2024_05_03`
- :ref:`raar_2024:charbit_2024_04_03`
- :ref:`conflitpal_2024:charbit_2024_03_21`
- :ref:`raar_2024:raar_2024_03_17`
- :ref:`charbit_2024_02_29`

Livres
========

- https://www.librairielesquare.com/listeliv.php?base=paper&mots_recherche=&auteurs=Denis%20Charbit

- :ref:`israel_et_ses_paradoxes_2023`
- :ref:`israel_et_ses_paradoxes_2015`
- :ref:`charbit_livre_sionisme_2007`
- :ref:`sionismes_textes_fondamentaux_1998`
