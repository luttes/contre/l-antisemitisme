.. index::
   pair: Historien ;Tal Bruttmann
   pair: Tal ; Bruttmann
   ! Tal Bruttmann

.. _tal_bruttmann:

=======================
**Tal Bruttmann**
=======================

- https://fr.wikipedia.org/wiki/Tal_Bruttmann
- https://www.radiofrance.fr/personnes/tal-bruttmann
- https://www.cairn.info/publications-de-wd--41957.htm
- https://www.seuil.com/auteur/tal-bruttmann/15519
- https://www.youtube.com/results?search_query=tal+bruttmann

Biographie
===========

- https://fr.wikipedia.org/wiki/Tal_Bruttmann

Tal Bruttmann, né le 7 février 1972, est un historien français.

**Il est spécialiste de la Shoah et de l'antisémitisme en France
au XXe siècle et pendant Vichy**


En 1999, il publie son premier article significatif dans le catalogue
de l'exposition « Journal d'un cinéaste amateur 1934-1944 »
(archives départementales de l'Isère), sous le titre
« L'École des cadres de la Milice d'Uriage : les chevaliers du Maréchal ».

Il y analyse un film amateur montrant les seules images connues de la
Milice iséroise, découvert par Arnaud Ragon, concepteur et réalisateur
de l'exposition.

Il dirige de 2001 à 2011 les travaux scientifiques de recherches pour
la commission d'enquête de la ville de Grenoble sur les spoliations
des juifs de leurs biens en Isère durant la Seconde Guerre mondiale.

Il est également membre de la Fondation pour la mémoire de la Shoah.

Tal Bruttmann publie en 2003 La Logique des bourreaux.

L'ouvrage, préfacé par l'historienne Annette Wieviorka, trouve son
origine dans le dépouillement systématique des archives de l'Isère
pour la période, et s'attache à reconstituer l'organisation et la
mise en œuvre sur le terrain de l'extermination des Juifs par le
Sicherheitsdienst de Grenoble (SD, service de la sécurité du Reichsführer-SS,
service de renseignement et de maintien de l'ordre de la SS) et met
notamment en lumière l'action d'Aloïs Brunner et des militants de l'ultra-collaboration.

Avec Au bureau des affaires juives : L'Administration française et
l'application de la législation antisémite publié aux éditions
La Découverte en 2006, l'historien étudie l'élaboration du statut des
juifs par le gouvernement de Vichy et sa mise en œuvre par l'administration.

Il constate que l'administration française, dans la pratique, appliqua
avec zèle la législation antisémite mise en place sous le régime de Vichy.

En 2006 paraît également un ouvrage coécrit avec l'historien Laurent Joly,
La France antijuive de 1936 : L’Agression de Léon Blum à la chambre des
députés, aux Éditions des Équateurs, réédité aux éditions du CNRS.

Tal Bruttmann participe à la commission Mémoire et transmission de
la Fondation pour la mémoire de la Shoah.

En 2020, il est conseiller historique pour le documentaire Ravensbrück :
Le Camp oublié, réalisé par Sophie Jeaneau.

Tal Bruttmann est notamment l’auteur du livre de référence Auschwitz,
qui vient d’être réédité (2025-01, La Découverte, "Repères", 124 pages, 11 euros,
numérique 8,50 euros), et d’Un album d’Auschwitz. Comment les nazis ont
photographié leurs crimes (Seuil, 2023), écrit avec les historiens allemands
Stefan Hördler et Christoph Kreutzmüller.

Livres
========

2023 Un album d'Auschwitz. Comment les nazis ont photographié leurs crimes
-----------------------------------------------------------------------------------

- :ref:`livre_bruttmann_2023`

En 2023, il co-signe avec Stefan Hördler et Christoph Kreutzmüller
:ref:`Un album d'Auschwitz. Comment les nazis ont photographié leurs crimes <livre_bruttmann_2023>`
aux éditions du Seuil.

2004 **Internement et camps d'internement en Isère (1940-1947)**
--------------------------------------------------------------------

- :ref:`internement_isere_1940`


Articles/vidéos de Tal Bruttmann en 2025
===========================================


Articles/vidéos de Tal Bruttmann en 2024
===========================================

- :ref:`raar_2024:tal_bruttmann_2024_06_19`
- :ref:`bruttmann_2024_04_28`
- https://www.gremag.fr/article/249/17-tal-bruttmann-defricheur-de-memoire.htm
- :ref:`conflitpal_2024:bruttmann_2024_01_18`
- https://k-larevue.com/entretien-avec-tal-bruttmann-lhistorien-de-la-shoah-face-au-7-octobre/

Articles/vidéos de Tal Bruttmann en 2023
===========================================

- :ref:`webinaire_2023_12_18`
- :ref:`tal_bruttmann_2023_11_21`
- :ref:`bruttmann_2023_11_16`
