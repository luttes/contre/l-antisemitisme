.. index::
   ! Universitaires ;

.. _universitaires:

=======================
**Universitaires**
=======================

.. https://centrehistoire19esiecle.pantheonsorbonne.fr/carole-reynaud-paligot

.. toctree::
   :maxdepth: 3

   alain-policar/alain-policar
   charlotte-recoquillon/charlotte-recoquillon
   christian-ingrao/christian-ingrao
   denis-charbit/denis-charbit
   emmanuel-debono/emmanuel-debono
   eva-illouz/eva-illouz
   felicien-faury/felicien-faury
   laurent-joly/laurent-joly
   leon-poliakov/leon-poliakov
   marie-anne-matard-bonucci/marie-anne-matard-bonucci
   nonna-mayer/nonna-mayer
   pierre-andre-taguieff/pierre-andre-taguieff
   pierre-savy/pierre-savy
   renee-poznanski/renee-poznanski
   simon-perego/simon-perego
   sylvaine-bulle/sylvaine-bulle
   tal-bruttmann/tal-bruttmann
   vincent-tiberj/vincent-tiberj
   zoe-grumberg/zoe-grumberg
