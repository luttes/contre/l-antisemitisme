.. index::
   pair: Historienne ; Renée Poznanski
   ! Renée Poznanski

.. _renee_poznanski:

=======================
**Renée Poznanski**
=======================


Biographie
=============

- https://fr.wikipedia.org/wiki/Ren%C3%A9e_Poznanski


Renée Poznanski, née à Paris le 26 avril 1949, est une historienne
d'origine russe, juive, ayant émigré en Israël en 1973 , spécialiste de
la Shoah, et de la Résistance juive en France durant la Seconde Guerre mondiale.

Elle a été professeur titulaire à l'université Ben Gourion du Néguev,
à Beer-Sheva, en Israël.


Cité en 2023
===============

- :ref:`renee_poznanski_2023_04_24`
