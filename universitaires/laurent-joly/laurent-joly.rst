.. index::
   pair: Historien ; Laurent Joly
   ! Laurent Joly

.. _laurent_joly:

==============================================================
**Laurent Joly** (Historien spécialiste de l'antisémitisme)
==============================================================

- https://fr.wikipedia.org/wiki/Laurent_Joly

Biographie
=============

- https://fr.wikipedia.org/wiki/Laurent_Joly


Laurent Joly, né le 26 juillet 1976, est un chercheur et un historien français.

Directeur de recherche au CNRS, il est spécialiste de l’antisémitisme durant
le régime de Vichy.


Laurent Joly étudie à la faculté des sciences historiques de Strasbourg.

En 2004, il obtient un doctorat en histoire à l'université Panthéon-Sorbonne
avec une thèse intitulée Vichy et le commissariat général aux questions juives :
contribution à l'histoire de la Shoah en France : 1941-1944 et rédigée sous
la direction de Pascal Ory.

Il est recruté en 2006 en tant que chargé de recherche au CNRS.
Il est affecté au Centre de recherche d'histoire quantitative de l'université
de Caen jusqu'en 2015, puis au Centre de recherches historiques de l'École
des hautes études en sciences sociales.
Il obtient en janvier 2015 une habilitation à diriger des recherches (HDR)
à l'École nationale des chartes, avec pour sujet « Naissance de l’Action française.
Maurice Barrès, Charles Maurras et l’extrême droite nationaliste au tournant
du XXe siècle ». La même année, il devient directeur de recherche au CNRS.

En 2017, il est l'auteur, avec le réalisateur David Korn-Brzoza, du documentaire
La Police de Vichy qui, se basant sur des archives inédites et colorisées,
« retrace les années noires de la police française, de la collaboration à la
traque des communistes en passant par les rafles de Juifs, la Résistance, la
Libération et l’épuration »

Il est nommé président du conseil scientifique du Mémorial du camp de Rivesaltes
en mars 2023.

Il dirige par ailleurs la collection « Seconde Guerre mondiale » aux éditions
du CNRS[réf. nécessaire].

En 2023, Laurent Joly reçoit le Prix Pierre Lafue pour son ouvrage La rafle
du Vél d'Hiv, paru aux éditions Grasset.


Livres
==============

- :ref:`rafle_vel_dhiv`

Videos
=====================

- :ref:`raar_2024:ujre_2024_03_23`
