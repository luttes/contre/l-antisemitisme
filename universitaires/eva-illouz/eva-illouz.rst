.. index::
   ! Eva Illouz

.. _eva_illouz:

==========================================================================
**Eva Illouz**
==========================================================================


Biographie Wikipedia
======================

- https://en.wikipedia.org/wiki/Eva_Illouz


Eva Illouz (en hébreu : אווה אילוז), née le 30 avril 1961 à Fès au Maroc, est
une sociologue et universitaire franco-israélienne spécialisée dans la sociologie
des sentiments et de la culture.

Elle est directrice d'études à l'École des hautes études en sciences sociales (EHESS)

Biographie
===============

Eva Illouz est née le 30 avril 1961 à Fès au Maroc et est allée vivre en France
avant l'âge de 10 ans.

Elle est diplômée de l'université Paris-Nanterre en sociologie.

Elle a ensuite suivi des cours de communication à l'université hébraïque de
Jérusalem entre 1983 et 1986.

Entre 1986 et 1991, elle étudie à l'école de communication et d'études culturelles
de l'université de Pennsylvanie, dont elle obtient un doctorat.

Elle parle couramment le français, l'hébreu, l'anglais et l'allemand.



Livre 2024
================

- :ref:`le_8_octobre_2024_10`

Articles 2024
===========================

- :ref:`raar_2024:eva_illouz_2024_12_10`
- :ref:`raar_2024:illouz_2024_05_14`
- :ref:`raar_2024:illouz_2024_04_19`
- :ref:`raar_2024:eva_illouz_2024_03_13`

Articles 2023
==============

- https://k-larevue.com/genocide-a-gaza-eva-illouz-repond-a-didier-fassin/
