.. index::
   pair: Alarmer ; Marie-Anne Matard Bonucci

.. _marie_anne_matard_bonucci:

===================================
**Marie-Anne Matard Bonucci**
===================================

- https://fr.wikipedia.org/wiki/Marie-Anne_Matard-Bonucci

Biographie
=============

Présidente :ref:`d'alarmer <alarmer>`

Professeure Paris VIII.

Marie-Anne Matard-Bonucci, née en 1960, est une universitaire française,
spécialiste du fascisme, du totalitarisme et de l’antisémitisme.


En 2024
===========

- :ref:`raar_2024:bonucci_2024_07_03`

En 2023
===========

- :ref:`raar_2023:table_ronde_1_2023_04_24_bonucci`
