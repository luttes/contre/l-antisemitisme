.. index::
   ! Charlotte Recoquillon

.. _charlotte_recoquillon:

===================================
**Charlotte Recoquillon**
===================================

- https://www.editions-msh.fr/contributeur/recoquillon-charlotte/

Biographie
=============


Webinaires/Ecrits 2025
================================

- :ref:`raar_2025:charlotte_recoquillon_2025_03_10`


Livres 2024
===============

- https://www.editions-msh.fr/livre/harlem-une-histoire-de-la-gentrification/
