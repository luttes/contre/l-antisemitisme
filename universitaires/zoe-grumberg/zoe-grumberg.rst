.. index::
   pair: Historien ; Zoe Grumberg
   ! Zoe Grumberg

.. _zoe_grumberg:

=======================
**Zoe Grumberg**
=======================

- https://www.sciencespo.fr/histoire/fr/chercheur/Zo%C3%A9%20Grumberg/76231.html


.. figure:: images/zoe_grumberg.png

   https://www.sciencespo.fr/histoire/fr/chercheur/Zo%C3%A9%20Grumberg/76231.html

Prix et distinctions
========================

- Prix de thèse 2021 en études juives francophones - Ex aequo
  (Fondation pour la Mémoire de la Shoah)
- Bourse de recherche 2021 sur la xénophobie et l'antisémitisme
  (Ville de Paris)


Discours Zoe Grumberg
=============================

- :ref:`zoe_grumberg_2023_04_16`
