.. index::
   ! Sylvaine Bulle

.. _sylvaine_bulle:
.. _bulle:

===================================
**Sylvaine Bulle** sociologue
===================================

- https://lap.ehess.fr/membres/sylvaine-bulle
- https://aoc.media/auteur/sylvaine-bulleaoc-media/
- https://www.multitudes.net/author/Bulle-Sylvaine/

Travail
===========

- https://lap.ehess.fr/membres/sylvaine-bulle

Sylvaine Bulle est sociologue.

Ses travaux portent en particulier sur les mouvements contemporains
d’émancipation en lien avec l’écologie, en France et en Israël.


- https://aoc.media/auteur/sylvaine-bulleaoc-media/

Sylvaine Bulle est sociologue, spécialiste de la conflictualité politique
et environnementale en France et en Israël.
Professeure de Sociologie (ENSA de Paris-Diderot) et actuellement accueillie
au CNRS au Centre de Recherche Français de Jérusalem.

Ses travaux portent principalement sur la sociologie de la conflictualité
et de la violence politique à partir du cas israélo-palestinien et plus
récemment à partir de l’autonomie politique et des contestations radicales.


Publications
================

Elle a récemment publié :

- Sociologie du conflit (avec F. Tarragoni, 2021) ;
- Irréductibles. Enquête sur des milieux de vie (2020)
- et Sociologie de Jérusalem (2020).

Liens Philosopher au présent: **Israël Palestine : Les possibilités de la paix**
==================================================================================

- https://www.youtube.com/playlist?list=PL_icmLCLTKLyVi7k5xeruoQazukpi0t-T (Israël Palestine : Les possibilités de la paix)

.. youtube:: 7FyoZCkqVKs


Débats/Textes 2025
========================

- :ref:`raar_2025:sylvaine_bulle_2025_03_28`

Textes 2024
================

- :ref:`raar_2024:sylvaine_bulle_2024_10_01`
- :ref:`conflitpal_2024:bulle_2024_06_28`
- :ref:`raar_2024:bulle_campisme_2024_03_11`

Textes 2023
================

- :ref:`raar_2023:bulle_2023_10_11`


Textes 2022
================

- https://k-larevue.com/lanarchisme-juif-et-ses-resurgences-ecologiques-contemporaines/
- :ref:`raar:bulle_2022_11_23`
