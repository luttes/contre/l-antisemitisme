.. index::
   pair: Historien ; Léon Poliakov
   ! Léon Poliakov

.. _leon_poliakov:

=========================================================
**Léon Poliakov** (né le 1910-11-25 mort le 1997-12-08)
=========================================================


Biographie
=============

- https://fr.wikipedia.org/wiki/L%C3%A9on_Poliakov


Léon Poliakov (en russe : Лев Влади́мирович Поляко́в), né le 25 novembre
1910 à Saint-Pétersbourg et mort à Orsay le 8 décembre 1997, est un
historien français.

Il est un pionnier de l'histoire de la Shoah et un spécialiste de l'étude
de l'antisémitisme.


Prisonnier de guerre, évasion et résistance
-------------------------------------------------

Il s'engage dans l'armée française au début de la Seconde Guerre mondiale,
vit la débâcle, est fait prisonnier par les Allemands à Saint-Valery-en-Caux
le 13 juin 1940 avec son bataillon, s'évade trois mois plus tard du
Frontstalag de Doullens. « Apatride sous arrêté d'expulsion », il obtient
des faux papiers sous le nom de Robert Paul et entre dans la résistance.

Ses activités au sein du « Réseau André d'action contre la déportation »
dirigé par Joseph Bass consistent principalement à fabriquer des faux
papiers, à convoyer des Juifs en danger de la zone sud vers le plateau
protestant du Chambon-sur-Lignon pour les mettre en sécurité et à transporter
des armes vers les maquis juifs actifs sur le plateau auvergnat.


Historien
============

- https://fr.wikipedia.org/wiki/Schneour_Zalman_Schneersohn

Cofondateur du Centre de documentation juive contemporaine (CDJC)
---------------------------------------------------------------------

En 1943, il devient secrétaire du rabbin `Schneour Zalman Schneersohn <https://fr.wikipedia.org/wiki/Schneour_Zalman_Schneersohn>`_ et
fonde avec le cousin de ce dernier, Isaac Schneersohn, le CDJC,
Centre de documentation juive contemporaine, qui se voue à recueillir
les preuves documentaires de la Shoah.

Il réussit à prendre possession des archives du Commissariat général aux
questions juives, des archives de l'ambassade d'Allemagne à Paris, de
l'état-major, et surtout du service anti-juif de la Gestapo, ce qui lui vaut,
après la victoire alliée, d'assister en tant qu'expert, avec Edgar Faure,
le chef de la délégation française, au procès de Nuremberg.

Il rapporte de Nuremberg, avec son collègue Joseph Billig, de nombreux
documents qui rejoignent les fonds du CDJC.

Sous l'égide du CDJC, il publie ses premiers livres : La Condition des Juifs
sous l'occupation italienne en 1946 et L’Étoile jaune en 1949.

En 1947, il rencontre sa femme, `Germaine Poliakov née Rousso morte le 19 février 2020 à 101 ans <https://fr.wikipedia.org/wiki/Germaine_Poliakov>`_.
De ce mariage naît un fils, en 1960.

Pionnier de l'histoire de la Shoah
======================================

Naturalisé français en 1947, Léon Poliakov publie, quatre ans plus tard,
le Bréviaire de la haine, dans la collection de Raymond Aron, livre qui
est la première grande étude consacrée à la politique d'extermination
des Juifs menée par les nazis.

Sa plongée dans les archives allemandes, les innombrables témoignages
qu'il recueille et cinq années d'efforts lui permettent de mettre au jour
les rouages implacables de l'idéologie et de la technique qui ont rendu
possible la Shoah.

Le Bréviaire de la haine est préfacé par François Mauriac, et régulièrement
remis à jour par son auteur au fil des rééditions, jusqu'en 1993.

Léon Poliakov est également le premier historien à mettre en cause
l'attitude du pape Pie XII et du Vatican à propos de la Shoah.

La publication de ce livre aux éditions Calmann-Levy, affirmation de besoin
d'autonomie, entraîne un conflit avec le CDJC et finalement sa mise en
congé définitif de l'institution.


Historien de l'antisémitisme
==================================

Mû par la volonté de trouver une réponse à la question "Pourquoi a-t-on voulu me tuer ?"
et décidé à remonter jusqu'aux racines, Léon Poliakov se consacre ensuite
à sa vaste Histoire de l'antisémitisme en cinq volumes, allant de
l'Antiquité au XXe siècle.

Il soutient en 1964 une thèse de doctorat de 3e cycle à l'EHESS, sous la
direction de Ruggiero Romano (it) sur Le Commerce de l'argent chez les
Juifs d'Italie du XIIIe siècle au XVIIe siècle, puis en 1968 une thèse
de doctorat ès lettres, sous la direction de Raymond Aron, sur
Le développement de l'antisémitisme en Europe aux temps modernes (1700-1850)10,
qui est reprise dans le troisième volume de son Histoire de l'antisémitisme.

Directeur de recherche du CNRS, il mène des recherches sur les minorités
persécutées et sur le racisme, ses origines et toutes les formes qu'il
peut revêtir.

Il publie en 1971 Le Mythe aryen, ouvrage qui interroge l'Europe sur ses
propres mythes.

Poliakov est avec Pierre Vidal-Naquet pionnier dans la lutte contre les
révisionnistes et les négationnistes qui minimisent ou nient l'extermination
des Juifs pendant la Seconde Guerre mondiale.

En février 1979, il prend avec Vidal-Naquet l'initiative d'une déclaration
signée par 34 historiens, parue dans Le Monde, démontant la rhétorique
de Robert Faurisson12.

En 1981, il publie ses mémoires qu'il intitule L'Auberge des musiciens
et dont une large partie est consacrée à son passé de résistant et aux
aventures vécues durant l'occupation nazie.

Ses ouvrages sont traduits dans de nombreuses langues et finalement en russe,
à la fin de sa vie et à sa plus grande joie.

Il est victime d'un accident vasculaire à 87 ans en novembre 1997, et
meurt à Orsay à la fin de l'année.


Portrait
==============

Rappelant que Poliakov n’a cessé de dire que le mot "antisémitisme",
pour désigner toutes les formes d’hostilité visant les Juifs, était moins
approprié que le terme de **judéophobie**, Pierre-André Taguieff dresse ce
portrait de Poliakov::

    "Historien certes, mais aussi anthropologue, et psychologue, et politologue,
    cet esprit toujours en éveil cherchait dans tout l’espace des sciences
    sociales et chez les philosophes de quoi éclairer ses recherches et
    nourrir ses réflexions sur cette 'animosité haineuse' à l’égard des
    Juifs qu’on a pris l’habitude – depuis le début des années 1880 –
    d’appeler 'antisémitisme'"



Cité en 2023
===============

- cité par Tal Bruttmann en décembre 2023 (à compléter)
