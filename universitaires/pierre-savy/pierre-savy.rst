.. index::
   pair: Historien ; Pierre Savy
   ! Pierre Savy

.. _pierre_savy:

=======================
**Pierre Savy**
=======================



Discours Pierre Savy
=============================

- :ref:`pierre_savy_2023_11_09_1751`
- :ref:`pierre_savy_2023_11_09_1715`
- :ref:`pierre_savy_2023_11_09_850`
