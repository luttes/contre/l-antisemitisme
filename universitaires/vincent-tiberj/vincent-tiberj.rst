.. index::
   ! Vincent Tiberj

.. _vincent_tiberj:

==========================================================
**Vincent Tiberj (prononcer Tiberi)** |VincentTiberj|
==========================================================

- https://fr.wikipedia.org/wiki/Vincent_Tiberj
- https://www.radiofrance.fr/personnes/vincent-tiberj

Biographie
=================

Vincent Tiberj est professeur en sociologie politique à Sciences Po Bordeaux et
chercheur au Centre Émile Durkheim depuis 2015.

Entre 2002 et 2015 il a été chargé de recherche FNSP à Sciences Po Paris, d’abord
au Cevipof puis au Centre d’études européennes.

Il a récemment fait paraître aux Puf:

- Citoyens et partis après 2022.
- Éloignement, fragmentation (en codirection, 2024), Extinction de vote ?
  (codirigé avec Tristan Haute, « La vie des idées », 2022)
- et Les citoyens qui viennent (2017).

Spécialiste des comportements électoraux et politiques, il fait partie de l’équipe
de recherche qui travaille au « baromètre racisme » de la Commission nationale
consultative des Droits de l’Homme et **a notamment élaboré l’indice longitudinal
de tolérance (ILT)**.


Livres
==========

- :ref:`droitisation_2024_09_04`
