.. index::
   ! Félicien Faury

.. _felicien_faury:

===================================
**Félicien Faury** |FelicienFaury|
===================================

- https://www.cesdip.fr/faury-felicien/
- https://aoc.media/auteur/felicien-faury/


Félicien Faury
===============

Félicien Faury est sociologue et politiste, chercheur postdoctoral
au `CESDIP (Centre de recherches sociologiques sur le droit et les institutions pénales) <https://www.cesdip.fr>`_.

Livres 2024
==================

- :ref:`domination_oubliee_2024_10_25`
- :ref:`electeurs_ordinaires_2024_05`



Cité en 2025
==============

- :ref:`extremes_droites:felicien_faury_2025_01_11`

Articles 2024
==============

- :ref:`raar_2024:felicien_faury_2024_11_07`
- :ref:`raar_2024:felicien_faury_2024_05_22`
