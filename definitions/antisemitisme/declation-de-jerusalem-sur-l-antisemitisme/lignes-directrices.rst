.. index::
   pair: Déclaration de Jérusalem sur l’antisémitisme ; Lignes directrices


.. _lignes_directrices_dja:

============================================================================
**Lignes directrices de la déclation de Jérusalem sur l'antisémitisme**
============================================================================

.. _lignes_dja_generalites:
.. _lignes_dja_generales:
.. _lignes_dja_a:

A. Généralités (lignes directrices générales 1 à 5)
==========================================================

.. _essentialisation_dja:
.. _ligne_1_dja:

1. L’essentialisation
-----------------------

**L’essentialisation**, c’est-à-dire le fait de considérer qu’un trait de
caractère est inhérent à un groupe humain ou le fait de faire des généralisations
négatives "à l’emporte-pièce" à propos d’une population donnée, constitue
**un acte raciste**.

**Ce qui est vrai du racisme en général, est vrai de l’antisémitisme en particulier**.


.. _ligne_2_dja:
.. _ligne_2_complotisme:

**2. L’idée que les juifs entretiennent un lien particulier avec les forces du mal caractérise l’antisémitisme "classique"**
-------------------------------------------------------------------------------------------------------------------------------

L’idée que les juifs entretiennent un lien particulier avec **les forces du mal**
caractérise l’antisémitisme "classique".

Un tel sentiment est au cœur de **nombreux fantasmes antijuifs**, par exemple la
fiction d’une conspiration juive, c’est-à-dire l’attribution aux juifs
d’un pouvoir caché qu’ils utiliseraient pour faire avancer leurs propres
objectifs aux dépens de ceux de la population au sein de laquelle ils vivent.

Un certain nombre **d’idées délirantes**, toujours présentes aujourd’hui, incarnent
ce lien fantasmé entre les juifs et le mal, notamment:

- le fait qu’ils contrôleraient,  en coulisse, les gouvernements ;
- que les banques leur appartiendraient ;
- qu’ils exerceraient le véritable pouvoir médiatique ;
- qu’ils agiraient comme "un État dans l’État" ;
- et qu’ils seraient responsables de la diffusion de certaines maladies,
  par exemple la COVID-19.

Toutes ces caractéristiques supposées peuvent être instrumentalisées par les
tenants de causes politiques différentes, voire antagonistes.


.. _ligne_3_dja:

3. Les mots, les images et les actes peuvent servir de véhicule à l’antisémitisme
----------------------------------------------------------------------------------------

**Les mots, les images et les actes peuvent servir de véhicule à l’antisémitisme**.

On peut citer, comme discours antisémite, l’affirmation que tous les juifs
sont riches, qu’ils sont naturellement avares ou qu’ils ne sont pas patriotes.

Dans les caricatures antisémites, les juifs sont souvent dépeints comme grotesques
ou monstrueux, affublés de caractéristiques physiques désobligeantes, par exemple
un grand nez, et associés à la richesse.

Les actes antisémites comprennent notamment les agressions contre les juifs,
parce qu’ils sont juifs, les attaques contre les synagogues, la dégradation
de tombes juives en y représentant des croix gammées, ainsi que le refus
d’embaucher ou de promouvoir quelqu’un parce que juif.

.. note:: Le masculin est utilisé tout au long de ce texte comme une forme neutre,
   afin d’en faciliter la lecture, tout en désignant indifféremment toutes les
   personnes, quel que soit leur sexe ou leur genre.


.. _ligne_4_dja:

4. L’antisémitisme peut être direct ou indirect, explicite ou codé
-------------------------------------------------------------------------

L’antisémitisme peut être direct ou indirect, explicite ou codé.

Affirmer, par exemple, que "les Rothschild contrôlent le monde" est une
déclaration codée sur le pouvoir présumé des juifs sur les banques et sur la
finance internationale.

**De même, dépeindre Israël comme "le mal absolu" ou grossièrement exagérer
son influence réelle peut être une manière codée de racialiser et de stigmatiser
les juifs**.

Dans de nombreux cas, la mise en évidence du caractère codé d’un discours
est une question de contexte et de jugement prenant en compte les présentes
lignes directrices.

.. _ligne_5_dja:

**5. Nier ou minimiser l’Holocauste**
----------------------------------------

Nier ou minimiser l’Holocauste, par exemple en affirmant que le génocide
délibéré des juifs par les nazis n’a pas eu lieu, qu’il n’y a pas eu de
camps d’extermination, que les chambres à gaz n’ont pas existé ou qu’il y a
eu infiniment moins de victimes qu’il n’y en a réellement eu, relève de l’antisémitisme.



.. _examples_antisem_israel_pal:

**B. Israël et la Palestine : quelques exemples relevant, a priori, de l’antisémitisme**
===========================================================================================

.. _ligne_6_dja:

6. Appliquer les symboles, les images et les stéréotypes négatifs de l’antisémitisme classique
--------------------------------------------------------------------------------------------------

Appliquer les symboles, les images et les stéréotypes négatifs de l’antisémitisme
classique (voir lignes directrices :ref:`nos 2 <ligne_2_dja>` et :ref:`3 <ligne_3_dja>`)
à l’État d’Israël.


.. _ligne_7_dja:

**7. Tenir les juifs collectivement responsables de la conduite d’Israël**
-----------------------------------------------------------------------------

**Tenir les juifs collectivement responsables de la conduite d’Israël ou traiter
les juifs, simplement parce qu’ils sont juifs, comme des agents d’Israël**.


Exemples
-------------

- :ref:`balibar_2024_09_23`

.. _ligne_8_dja:

**8. Exiger des gens, parce qu’ils sont juifs, qu’ils condamnent publiquement Israël**
-----------------------------------------------------------------------------------------

**Exiger des gens, parce qu’ils sont juifs, qu’ils condamnent publiquement
Israël ou le sionisme (par exemple, lors d’une réunion politique)**.


Exemples
++++++++++++

- :ref:`chinsky:chinsky_2024_09_20_dja_8`

.. _ligne_9_dja:

9. Faire l’hypothèse que les juifs citoyens d’autres pays sont plus loyaux vis-à-vis d’Israël que de leur propre pays
---------------------------------------------------------------------------------------------------------------------------

Faire l’hypothèse que les juifs citoyens d’autres pays sont plus loyaux vis-à-vis
d’Israël que de leur propre pays, uniquement parce qu’ils sont juifs.


.. _ligne_10_dja:

**10. Refuser le droit des juifs de l’État d’Israël à exister**
-----------------------------------------------------------------

**Refuser le droit des juifs de l’État d’Israël à exister et à s’épanouir,
collectivement et individuellement, en tant que juifs, conformément au principe
d’égalité**.


.. _lignes_dja_c:
.. _examples_not_antisem_israel_pal:

C. Israël et la Palestine : quelques exemples ne relevant pas, a priori, de l’antisémitisme
==================================================================================================

(Ces exemples sont fournis indépendamment du fait que l’on approuve ou non
le point de vue exprimé ou la démarche mise en œuvre.)


.. _ligne_11_dja:

**11. Soutenir l’exigence de justice du peuple palestinien**
-------------------------------------------------------------

Soutenir l’exigence de justice du peuple palestinien et sa recherche de
l’obtention de l’intégralité de ses droits politiques, nationaux, civiques
et humains, en conformité avec le droit international.


.. _ligne_12_dja:

12. Critiquer le sionisme ou s’y opposer, en tant que forme de nationalisme
-------------------------------------------------------------------------------

Critiquer le sionisme ou s’y opposer, en tant que forme de nationalisme, ou
plaider pour la mise en place de différents types de solutions constitutionnelles,
pour les juifs et pour les Palestiniens, dans la région située entre le Jourdain
et la Méditerranée.

Il n’est pas antisémite de se prononcer en faveur de modalités politiques
accordant une égalité pleine et entière à tous les habitants de cette région,
qu’il s’agisse de prôner:

- une solution à deux États,
- la création d’un État binational,
- d’un État unitaire démocratique
- ou d’un État fédéral,
- ou la mise en place de tout autre système politique, quelle qu’en soit la forme.

.. _ligne_13_dja:

13. Critiquer Israël en tant qu’État, en s’appuyant sur des faits
----------------------------------------------------------------------

Critiquer Israël en tant qu’État, en s’appuyant sur des faits ;

cette critique peut notamment porter sur les institutions nationales de ce
pays et sur ses principes fondateurs.

Elle peut également inclure la remise en cause des politiques et des pratiques
d’Israël, à l’échelon national et international ;

il peut, en particulier, s’agir de critiquer son comportement en Cisjordanie
et à Gaza, son rôle dans la région ou toute autre manière dont cet État exerce,
en tant que tel, une influence sur les événements dans le monde.

Il n’est pas antisémite de mettre en exergue une discrimination raciale
systématique en Israël.

D’une manière générale, le débat sur la situation en Israël et en Palestine
doit être soumis à des normes identiques à celles qui prévalent dans le cas
d’autres États et d’autres conflits d’autodétermination nationale.

Ainsi, même s’il s’agit d’une position controversée, il n’est pas antisémite,
en soi, d’établir des parallèles entre la situation d’aujourd’hui en Israël et
d’autres contextes historiques, y compris de colonisation de peuplement
ou d’apartheid.


.. _ligne_14_dja:

**14. Les mesures de boycott, de désinvestissement et de sanction**
---------------------------------------------------------------------

Les mesures de boycott, de désinvestissement et de sanction constituent des
formes répandues et non violentes de lutte politique contre des États.

Dans le cas d’Israël, de telles stratégies ne sont certainement pas, en elles-mêmes,
antisémites.


.. _ligne_15_dja:

15. Il n’y a nulle nécessité qu’un discours politique soit mesuré, proportionné, modéré ou raisonnable, pour être protégé
---------------------------------------------------------------------------------------------------------------------------

Il n’y a nulle nécessité qu’un discours politique soit mesuré, proportionné,
modéré ou raisonnable, pour être protégé:

-  en vertu de l’article 19 de la Déclaration universelle des droits de l’homme
- ou de l’article 10 de la Convention européenne des droits de l’homme,
- ou de tout autre texte relatif aux droits de la personne.

Des critiques que certains pourraient considérer comme excessives ou litigieuses,
ou comme étant l’application d’une démarche de type "deux poids deux mesures",
ne sont pas intrinsèquement antisémites.

Il convient de noter que, d’une manière générale, ce qui sépare un discours
antisémite d’un discours qui ne l’est pas ne s’inscrit pas nécessairement
en cohérence avec la différence entre ce qui est raisonnable et ce qui ne l’est pas.
