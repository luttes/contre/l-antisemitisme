.. index::
   pair: Préambule ; Déclaration de Jérusalem

.. _preambule_dja:

=================================================
**Préambule à la Déclaration de Jérusalem**
=================================================


Préambule
=============

Nous, soussignées et soussignés, présentons la Déclaration de Jérusalem
sur l’antisémitisme, fruit d’une initiative née dans cette ville.

On compte, parmi les signataires de la DJA, un grand nombre d’universitaires,
chercheuses et chercheurs originaires du monde entier, dont les études portent:

- sur l’antisémitisme et sur des sujets connexes,
- ainsi que sur divers autres domaines, notamment:

  - l’histoire juive et le judaïsme,
  - l’Holocauste,
  - Israël,
  - la Palestine et le Moyen-Orient.

Le contenu de la Déclaration est le fruit de nombreuses consultations avec
des juristes, des chercheurs et des universitaires, ainsi qu’avec des
représentantes et des représentants de la société civile.

La Déclaration universelle des droits de l’homme de 1948, la Convention
internationale sur l’élimination de toutes les formes de discrimination
raciale de 1969, la Déclaration du Forum international de Stockholm sur
l’Holocauste de 2000 et la Résolution des Nations Unies sur la mémoire
de l’Holocauste de 2005 ont constitué le socle de l’élaboration de la
DJA.

Dans ce contexte, nous soutenons que s’il est vrai que l’antisémitisme
présente certaines caractéristiques propres, **il n’en demeure pas moins que
le combat contre ce fléau ne saurait être dissocié de la lutte globale contre
toutes les formes de discrimination raciale, ethnique, culturelle, religieuse
et sexuelle**.

Conscientes et conscients des persécutions qu’ont subies les juifs tout au
long de l’histoire, sensibles aux leçons universelles de la Shoah et observant,
avec inquiétude, divers groupes, qui mobilisent la haine et la violence, aussi
bien en politique qu’au sein de la société dans son ensemble, notamment sur Internet,
réaffirmer leur haine des juifs, nous avons estimé indispensable d’élaborer
une définition de base — utilisable dans la pratique, concise et s’appuyant
sur des faits historiques — de ce qu’est l’antisémitisme, accompagnée d’un
ensemble de lignes directrices, et de la mettre à la disposition de tous.


.. _update_ihra:

**La Déclaration de Jérusalem sur l’antisémitisme répond à "la définition de l’IHRA"**
------------------------------------------------------------------------------------------

- :ref:`def_ihra`

La Déclaration de Jérusalem sur l’antisémitisme répond à "la définition de l’IHRA",
le document qui a été adopté par l’Alliance internationale pour la mémoire
de l’Holocauste (IHRA) en 2016.

Cette définition présentant quelque ambiguïté par rapport à plusieurs aspects
clés de la problématique de l’antisémitisme et s’avérant largement ouverte
à des divergences d’interprétation, elle a été à l’origine d’une certaine
confusion et de nombre de controverses, affaiblissant, par là même, la lutte
contre l’antisémitisme.

Prenant bonne note du fait qu’elle se qualifie, elle-même, de "définition de travail",
nous avons cherché à l’améliorer (a) en offrant une définition de base
plus claire et (b) en proposant un ensemble cohérent de lignes directrices.

Nous formons le vœu que cette démarche s’avère utile, non seulement dans
le cadre de la surveillance de l’antisémitisme et du combat à mener contre
cette haine particulière, mais également à des fins éducatives.

Nous proposons notre Déclaration de Jérusalem sur l’antisémitisme, non
juridiquement contraignante, comme une solution de rechange à la définition
de l’IHRA.

Les institutions ayant déjà adopté la définition de l’IHRA peuvent s’appuyer
sur notre texte pour l’interpréter.

**La définition de l’IHRA comprend 11 "exemples" de comportements antisémites**,
7 d’entre eux ayant essentiellement trait à l’État d’Israël.

Une telle démarche met indûment l’accent sur un aspect unique ; pour autant,
nous ne nions pas que nombreux sont celles et ceux qui estiment qu’il convient
de clarifier les limites de la légitimité du discours et de l’action politiques,
lorsqu’il s’agit du sionisme, d’Israël et de la Palestine.

Notre objectif est double :

- (1) renforcer la lutte contre l’antisémitisme en levant, autant que faire se peut,
  toute ambiguïté quant à sa définition
- et (2) préserver la possibilité d’un débat public sans entrave sur l’épineuse
  question de l’avenir d’Israël et de la Palestine.

Nous ne partageons pas tous les mêmes opinions politiques et nous ne cherchons
aucunement à mettre en avant un programme politique partisan.

Lorsque nous posons qu’un point de vue ou que des actes, pour controversés
qu’ils soient, ne sont pas antisémites, cela ne signifie aucunement ni que
nous les approuvons ni que nous les réprouvons.

Les lignes directrices portant sur Israël et sur la Palestine (nos 6 à 15)
doivent être considérées comme un tout.

D’une manière générale, chacune de ces lignes directrices doit être interprétée,
en vue de son application, à la lumière des autres, en tenant compte systématiquement
du contexte.
Lorsque nous parlons de contexte, nous faisons référence à l’intention présidant
à une prise de parole ou à la formulation d’un énoncé, ou à un certain schéma
discursif, se reproduisant au fil du temps, voire à l’identité de l’orateur,
en particulier lorsque le sujet traité est Israël ou le sionisme.

Ainsi, si l’hostilité envers Israël peut être l’expression d’une animosité
antisémite, il n’en demeure pas moins qu’elle peut également constituer une
réaction à une violation des droits de l’homme ou la manifestation des sentiments
et de l’émotion qu’une Palestinienne ou un Palestinien peuvent ressentir du
fait de ce que l’État d’Israël leur fait subir.

**Pour exprimer les choses en peu de mots, il est indispensable de faire preuve
de jugement et de sensibilité dans l’application de ces lignes directrices à
des situations concrètes**.


.. _dja_antisem_definition:

**Définition de l'antisémitisme par la déclation de Jérusalem**
======================================================================

**On appelle antisémitisme la discrimination, les préjugés, l’hostilité ou la
violence envers les juifs, en tant que juifs (ou contre les institutions juives,
en tant qu’elles sont juives)**.
