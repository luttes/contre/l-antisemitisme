.. index::
   pair: Définition antisémitisme ; DJA
   pair: Déclaration de Jérusalem sur l’antisémitisme ; DJA


.. _def_antisemitisme_dja:
.. _declaration_jerusalem:

============================================================================================
2021-06-09 **Déclaration de Jérusalem sur l’antisémitisme** (DJA)
============================================================================================

- https://jerusalemdeclaration.org/
- https://jerusalemdeclaration.org/#signature
- https://jerusalemdeclaration.org/wp-content/uploads/2021/06/2021-06-09_JDA_fr_final-version.pdf

.. toctree::
   :maxdepth: 3

   introduction
   preambule-et-definition
   lignes-directrices
   faq
