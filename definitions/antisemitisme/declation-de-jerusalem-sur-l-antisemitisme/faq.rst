.. index::
   pair: FAQ ; DJA (Déclaration de Jérusalem sur l’antisémitisme)
   pair Complotisme ; Déclaration de Jérusalem sur l’antisémitisme
   pair: Institut Van Leer ; Déclaration de Jérusalem sur l’antisémitisme
   pair: discrimination ; discrimination par perception
   pair: discrimination ; discrimination par association


.. _faq_dja:

=======================================================
**FAQ déclation de Jérusalem sur l'antisémitisme**
=======================================================


.. _faq_dja_quest-ce:

Qu’est-ce que la Déclaration de Jérusalem sur l’antisémitisme (DJA) ?
===============================================================================

La DJA est une ressource visant à renforcer la lutte contre l’antisémitisme ;
elle comprend:

- :ref:`un préambule <preambule_dja>`,
- :ref:`une définition <dja_antisem_definition>`
- :ref:`et un ensemble de 15 lignes directrices <lignes_directrices_dja>`.


.. _faq_dja_auteurs:

Qui sont les auteurs de la DJA ?
===========================================

La DJA a été rédigée par des universitaires, chercheuses et chercheurs spécialistes
des études sur l’antisémitisme ainsi que d’autres champs connexes, qui, à partir
de juin 2020, se sont réunis dans le cadre d’une série d’ateliers en ligne,
avec la participation de différents intervenants à différents moments.

Elle a été approuvée par un large spectre de chercheuses et de chercheurs
de premier plan, ainsi que de dirigeantes et de dirigeants de divers instituts
de recherche en Europe, aux États-Unis, au Canada et en Israël.


.. _faq_dja_jerusalem:

Pourquoi l’intitulé de la DJA contient-il le terme "Jérusalem" ?
=========================================================================

- https://en.wikipedia.org/wiki/Van_Leer_Jerusalem_Institute
- https://www.vanleer.org.il/en/

À l’origine, les travaux sur la DJA ont été organisés à Jérusalem, sous la
houlette de l’Institut Van Leer.

Pourquoi la DJA est-elle publiée maintenant (2021) ?
=======================================================

La DJA répond (en 2021) à la définition de travail de l’antisémitisme adoptée, en 2016,
par l’Alliance internationale pour la mémoire de l’Holocauste (IHRA).

En effet, la définition de l’IHRA n’est ni cohérente ni exempte d’ambiguïtés,
notamment dans les exemples qu’elle fournit.

Quelles que soient les intentions de ceux qui souhaitent la mettre en avant,
elle brouille la différence entre un discours antisémite et une critique
légitime d’Israël et du sionisme.

Non seulement une telle démarche est source de confusion, mais elle délégitime
également la voix des Palestiniennes et des Palestiniens et celles d’autres
personnes, notamment juives, ayant des opinions extrêmement critiques à l’égard
d’Israël et du sionisme.

**Rien de tout cela ne permet de lutter contre l’antisémitisme**.

La DJA se veut une réponse à une telle situation.

.. _faq_dja_ihra_replacement:

La DJA vise-t-elle à incarner une solution de remplacement à la définition de travail de l’IHRA ?
========================================================================================================

Certainement, c’est exactement de cela dont il s’agit.

Toutes les personnes de bonne volonté sont à la recherche d’orientations et
d’éclairages permettant de déterminer si un discours politique à propos d’Israël
ou du sionisme franchit les limites et verse du côté de l’antisémitisme ou
s’il doit être légitimement protégé.

La DJA a comme objectif de fournir de telles orientations et de tels éclairages
et doit donc être perçue comme une solution de remplacement à la définition
de l’IHRA.

Toutefois, **si une organisation a d’ores et déjà adopté officiellement la
définition de l’IHRA, elle peut s’appuyer sur la DJA pour en corriger les lacunes**.


.. _faq_dja_portee:

Quelle est la portée de la DJA ?
==============================================

Cette définition s’applique quel que soit l’angle que l’on adopte pour définir
l’identité juive, que cette dernière soit comprise comme étant ethnique,
biologique, religieuse, culturelle, etc.

Les situations dans lesquelles une personne ou une institution non juive
sont considérées, à tort, comme étant juives (ce que l’on appelle la
**"discrimination par perception"**) ou sont ciblées en raison d’un lien avec
d’autres personnes ou d’autres institutions juives (ce que l’on appelle la
**"discrimination par association"**) relèvent également de la DJA.

.. _faq_dja_adoption:

La DJA devrait-elle être officiellement adoptée, notamment par des gouvernements, par des partis politiques ou par des universités ?
=========================================================================================================================================

Il est possible d’avoir recours à la DJA, comme à une ressource pertinente,
à diverses fins.

Il peut notamment s’agir:

- d’éduquer et de former à ce qu’est l’antisémitisme ;
- de mieux faire connaître les critères permettant de différencier un
  discours ou une conduite antisémites d’un discours ou d’une conduite qui
  ne le sont pas ;
- ou d’élaborer des politiques de lutte contre l’antisémitisme.

Elle peut être utilisée à l’appui de la mise en œuvre d’une législation
anti-discrimination, dans le cadre des paramètres fixés par le droit et des
normes protégeant la liberté d’expression.

.. _faq_dja_haine:

La DJA pourrait-elle s’intégrer à un code plus général sur les "discours haineux" ?
==========================================================================================

Non, tel n’est pas son objectif.

La DJA n’a pas été conçue pour constituer un instrument juridique ou quasi
juridique, de quelque nature que ce soit.

Elle ne doit pas non plus être codifiée dans la loi ni utilisée pour restreindre
l’exercice légitime de la liberté universitaire (que ce soit dans l’enseignement
ou dans la recherche) ou pour annihiler la possibilité d’un débat public,
libre et ouvert, sous réserve qu’il se situe dans les limites du droit régissant
les crimes haineux.

.. _faq_dja_fin:

La DJA mettra-t-elle fin à toutes les controverses actuelles sur ce qui est ou ce qui n’est pas antisémite ?
=======================================================================================================================

La DJA est l’expression non ambiguë d’un groupe d’expertes et d’experts du
milieu universitaire et du monde de la recherche, **spécialistes de l’antisémitisme**
et d’un certain nombre de sujets connexes, faisant autorité dans leur domaine.

Elle n’est toutefois certainement pas en mesure, à elle seule, de mettre un
terme à toutes les polémiques ayant cours sur ce sujet.

En matière d’antisémitisme, aucun document ne saurait être totalement exhaustif
ni anticiper toutes les formes que pourrait prendre ce fléau à l’avenir.

Un certain nombre de lignes directrices, par exemple :ref:`la no 5 <ligne_5_dja>`,
ne donnent que quelques exemples pour illustrer un point d’ordre général.

La DJA se veut une aide à la réflexion et entend favoriser des échanges éclairés.

Dans un tel cadre, **il s’agit d’une précieuse ressource pour mener des consultations
avec différentes parties prenantes, en vue de déterminer les meilleurs moyens
de mettre en évidence l’antisémitisme et d’y réagir le plus efficacement possible**.

.. _faq_dja_palestine:

Pourquoi 10 des 15 lignes directrices de la DJA concernent-elles Israël et la Palestine ?
=================================================================================================

Il s’agit, en fait, de répliquer à la mise en avant, par la définition de l’IHRA,
de cette problématique, 7 des 11 exemples que donne cette dernière ayant
trait à Israël.

La DJA s’inscrit, en outre, comme un élément de réponse pertinent à la question,
faisant l’objet d’un débat public aussi bien dans la sphère juive que non juive,
consistant à déterminer ce qui est, ou ce qui n’est pas, antisémite, dans les
différents discours politiques à propos d’Israël ou du sionisme.

De telles discussions démontrent clairement la nécessité de conseils et d’orientation,
**en vue de trancher dans un sens ou dans un autre et de distinguer, sans ambigüité,
entre ce qui est condamnable et ce qui doit être protégé en vertu de la liberté
d’expression**.

.. _faq_dja_contexte:
.. _faq_dja_complotisme:

**Qu’en est-il de l’application de la DJA à des contextes autres qu’Israël et la Palestine ?**
==================================================================================================

:ref:`Les lignes directrices générales (nos 1 à 5) <lignes_dja_generales>`
s’appliquent dans tous les contextes, notamment aux mouvements et à l’idéologie
d’extrême droite, porteurs d’un discours dans le cadre duquel l’antisémitisme
prend une place de plus en plus importante.

Elles s’appliquent, par exemple:

- à un certain nombre de théories du complot,  fantasmant "la main des juifs"
  derrière la pandémie de COVID-19
- ou faisant l’hypothèse que George Soros finance les manifestations de protestation
  des mouvements Black Lives Matter et Antifa, avec comme volonté de faire
  avancer un certain nombre "d’objectifs juifs cachés".


.. _faq_dja_antisionisme:

**La DJA fait-elle une distinction entre l’antisionisme et l’antisémitisme ?**
===============================================================================

Tout à fait, **ces deux concepts sont radicalement différents**.

**Le nationalisme, notamment juif, peut prendre de nombreuses formes, mais il
s’agit d’un sujet dont on peut toujours débattre**.

Le sectarisme et la discrimination, que ce soit contre les juifs ou contre
quelque autre groupe que ce soit, ne sont jamais acceptables.

Il s’agit là d’un postulat de base de la DJA.


.. _faq_dja_antisio_antisem:

Dans ce cas, la DJA suggère-t-elle que l’antisionisme n’est jamais antisémite ?
====================================================================================

**Non.**

La DJA cherche à établir des critères non ambigus pour déterminer les situations
dans lesquelles la critique ou l’hostilité envers Israël ou envers le sionisme
tombent dans l’antisémitisme et celles pour lesquelles ce n’est pas le cas.

L’une de ses caractéristiques, à cet égard, réside dans le fait que, contrairement
à la définition de l’IHRA, elle ne se contente pas d’indiquer ce qui est
antisémite, mais fournit également des exemples clairs de ce qui, en
tant que tel, ne l’est pas.


.. _faq_dja_program:

Quel est le programme politique sous-jacent de la DJA, en ce qui concerne Israël et la Palestine ?
=======================================================================================================

**Il n’y en a aucun !**

C’est cette absence de programme politique qui constitue le fondement et l’esprit
même de la DJA.

Les signataires ont des points de vue différents sur le sionisme et sur le
conflit entre Israël et les Palestiniens, notamment en ce qui concerne les
solutions privilégiées, qu’elles soient à un État ou à deux États.

Ce qu’ils et elles partagent, en revanche, c’est un double engagement :

- lutter contre l’antisémitisme
- et protéger la liberté d’expression sur la base de principes universels.


.. _faq_dja_directive_14:

Mais, la ligne directrice 14 ne soutient-elle pas le mouvement BDS en tant que stratégie ou tactique dirigée contre Israël ?
=====================================================================================================================================

- :ref:`ligne_14_dja`

Absolument pas !

**Les signataires de la DJA ont des points de vue différents sur ce mouvement**.

:ref:`La ligne directrice 14 <ligne_14_dja>` se contente de clarifier le fait
que les mesures de boycott, de désinvestissement et de sanctions visant Israël,
pour aussi controversées qu’elles soient, ne sont pas, en elles-mêmes, antisémites.


.. _faq_dja_bds:

Alors, comment peut-on déterminer si une mesure de type BDS ou de tout autre type est antisémite ?
===========================================================================================================

:ref:`Les lignes directrices générales 1 à 5 <lignes_dja_generales>` ont
précisément été rédigées pour répondre à cette question.

Dans certains cas, la manière dont elles s’appliquent est évidente, dans d’autres
non.

Comme cela a toujours été le cas lorsque l’on porte un jugement sur une forme
quelconque de haine ou de discrimination, **le contexte joue un rôle absolument
essentiel**.

En outre, il convient de garder à l’esprit que chacune de ces lignes directrices
doit être interprétée à la lumière des autres.

Dans certains cas, il faut, en dernier recours, se déterminer en s’appuyant
sur son jugement, :ref:`les 15 lignes directrices <lignes_directrices_dja>`
visant à aider les gens à prendre des décisions de ce type.


La ligne directrice 10 dit qu’il est antisémite de "refuser le droit des juifs de l’État d’Israël à exister et à s’épanouir, collectivement et individuellement, en tant que juifs". Les lignes directrices 12 et 13 ne s’inscrivent-elles pas en  contradiction avec cet énoncé ?
====================================================================================================================================================================================================================================================================================

Il n’y a aucune contradiction.

Les droits mentionnés dans la :ref:`ligne directrice 10 <ligne_10_dja>`
s’attachent aux habitantes et aux habitants juifs de n’importe quel État
actuel ou futur, quels que soient sa constitution ou son nom.

Les lignes directrices 12 et 13 précisent qu’il n’est pas antisémite, en tant
que tel, de proposer un ensemble de solutions constitutionnelles et politiques
différentes de celles qui prévalent actuellement.


.. _dja_versus_ihra:

Quels sont, en bref, les avantages de la DJA par rapport à la définition de l’IHRA ?
==============================================================================================

Ils sont nombreux, notamment le fait que la première bénéficie de plusieurs
années de réflexion et d’évaluation critique de la deuxième.

En conséquence, la DJA est, selon nous, plus claire, plus cohérente et plus nuancée.

La DJA précise non seulement ce qu’est l’antisémitisme, mais aussi, dans le
contexte d’Israël et de la Palestine, ce qui ne relève pas, en tant que tel,
de cette forme de haine.

Il s’agit donc de conseils et d’orientations ayant une portée très large.

La DJA invoque des principes universels et, contrairement à la définition
de l’IHRA, lie clairement la lutte contre l’antisémitisme à la lutte contre
d’autres formes de haine et de discrimination.

Elle contribue à créer la possibilité d’un débat franc et respectueux sur
des enjeux difficiles, notamment la question épineuse de l’avenir politique
de tous les habitants d’Israël et de la Palestine.

**Pour toutes ces raisons, la DJA est plus convaincante et, plutôt que de susciter
la division, elle vise à unir toutes les forces dans une lutte la plus large
possible contre l’antisémitisme**.
