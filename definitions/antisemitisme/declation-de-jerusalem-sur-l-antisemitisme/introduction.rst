.. index::
   pair: Déclaration de Jérusalem ; Groupe

.. _intro_dja:

=========================================================================
Introduction à la déclaration de Jérusalem sur l’antisémitisme
=========================================================================

Introduction
===============

La Déclaration de Jérusalem sur l’antisémitisme (DJA) est un outil
permettant de reconnaître l’antisémitisme, tel qu’il se manifeste,
de nos jours, dans différents pays du monde, de s’y opposer et de mieux
faire connaître cet enjeu.

Elle comprend:

- :ref:`un préambule <preambule_dja>`,
- :ref:`une définition <dja_antisem_definition>`,
- et un ensemble de :ref:`15 lignes directrices <lignes_directrices_dja>`
  proposant des orientations et des conseils détaillés **à l’intention de
  tous ceux qui souhaitent diagnostiquer l’antisémitisme, en vue de concevoir une riposte**.

Elle a été élaborée par un groupe de spécialistes:

- de l’histoire de l’Holocauste,
- des études juives et des études sur le Moyen-Orient,

issus du milieu universitaire et du monde de la recherche, afin de relever
ce défi qui n’a jamais été aussi crucial qu’aujourd’hui : **être en mesure
de repérer l’antisémitisme et de le combattre, tout en protégeant la liberté d’expression**.

La DJA a, à ce jour, reçu l’appui de plus de `200 signataires <https://jerusalemdeclaration.org/#signature>`_.


.. _a_propos_dja:

À propos de la DJA
========================

En 2020, un groupe d’universitaires, chercheuses et chercheurs spécialistes
de l’antisémitisme et d’un certain nombre de domaines connexes, notamment
l’histoire juive et le judaïsme, l’Holocauste, Israël, la Palestine et le Moyen-Orient,
s’est réuni **sous les auspices de l’Institut Van Leer de Jérusalem, en vue
de relever le défi crucial consistant à repérer l’antisémitisme et à s’y opposer**.

Au cours d’une année de discussions et d’échanges, ce groupe a réfléchi à
l’utilisation des outils existants, notamment la définition de travail adoptée
par l’Alliance internationale pour la mémoire de l’Holocauste (IHRA), et
ses implications pour la liberté universitaire et la libre expression.

Les créatrices et les créateurs de la DJA, et ses signataires, représentent
un large éventail de disciplines universitaires et de perspectives nationales
et régionales et ont des points de vue diversifiés sur les différents enjeux
liés au conflit opposant Israël et les Palestiniens.

Toutes et tous sont toutefois tombés d’accord sur la nécessité de disposer
d’un outil plus précis et plus didactique, permettant de mettre en évidence,
plus facilement, les caractéristiques auxquelles il est possible d’avoir
recours pour qualifier un discours, des images, des actes, etc. comme
antisémites, ainsi que celles qui ne constituent pas une preuve concluante
d’antisémitisme.


.. _groupe_dja:

Groupe de coordination de la déclaration de Jérusalem
=========================================================

- Seth Anziska, professeur agrégé en relations judéo-musulmanes,
- Mohamed S. Farsi-Polonsky, Collège universitaire de Londres (UCL)
- Aleida Assmann, professeure docteure en études littéraires et en études
  sur l’Holocauste, sur les traumas et sur la mémoire, Université de Constance
- Alon Confino, professeur d’histoire et d’études juives, directeur de l’Institut
  sur l’Holocauste, le génocide et les études mémorielles, chaire d’études
  sur l’Holocauste Pen Tishkach, Université du Massachusetts, Amherst
- Emily Dische-Becker, journaliste
- David Feldman, professeur, directeur de l’Institut de recherche sur l’antisémitisme,
  Birkbeck, Université de Londres
- Amos Goldberg, professeur à la chaire d’études sur l’Holocauste
- Jonah M. Machover, directeur de l’Institut de recherche sur le judaïsme
  contemporain
- Avraham Harman, Université hébraïque de Jérusalem
- Brian Klug, agrégé supérieur de recherche en philosophie, St. Benet’s Hall, Oxford ; membre de
  la faculté de philosophie, Université d’Oxford
- Stefanie Schüler Springorum, professeure docteure, directrice du Centre de recherches sur
  l’antisémitisme, Université technique de Berlin
