.. index::
   pair: Définition antisémitisme ; AIMH
   pair: Définition antisémitisme ; IHRA
   pair: International Holocaust Remembrance Alliance; IHRA


.. _def_antisemitisme_aimh:
.. _def_ihra:

=========================================================================================================================================
2016 **Définition AIMH (Alliance internationale pour la mémoire de l’Holocauste)** (IHRA, International Holocaust Remembrance Alliance)
=========================================================================================================================================

- https://www.holocaustremembrance.com/fr/resources/working-definitions-charters/la-definition-operationnelle-de-lantisemitisme-utilisee-par
- https://www.youtube.com/@IHRANews

What is the International Holocaust Remembrance Alliance ?
==============================================================

- https://holocaustremembrance.com/who-we-are
- https://holocaustremembrance.com/resources/stockholm-declaration

The IHRA is the only intergovernmental organization with a mandate focused
on addressing contemporary challenges related to the Holocaust and genocide
of the Roma.

We foster education, remembrance, and research about what happened in the past,
to build a world without genocide in the future.


.. youtube:: KVrekt_o-Ho

Introduction
===============

L’Alliance internationale pour la mémoire de l’Holocauste (IHRA) rassemble
des gouvernements et des experts dans le but de renforcer et de promouvoir
l’éducation, le travail de mémoire et la recherche sur l’Holocauste et
de mettre en œuvre les engagements de la déclaration de Stockholm de 2000.

La définition opérationnelle de l’antisémitisme, non contraignante, a
été adoptée par les 31 États membres de l’IHRA le 26 mai 2016:

"L’antisémitisme est une certaine perception des Juifs qui peut se manifester
par une haine à leur égard. Les manifestations rhétoriques et physiques
de l’antisémitisme visent des individus juifs ou non et/ou leurs biens,
des institutions communautaires et des lieux de culte.»

Les exemples suivants, destinés à guider le travail de l’IHRA, illustrent
cette définition:

L’antisémitisme peut se manifester par des attaques à l’encontre de
l’État d’Israël lorsqu’il est perçu comme une collectivité juive.

Cependant, critiquer Israël comme on critiquerait tout autre État ne
peut pas être considéré comme de l’antisémitisme.

L’antisémitisme consiste souvent à accuser les Juifs de conspirer
contre l’humanité et, ce faisant, à les tenir responsables de
"tous les problèmes du monde".

Il s’exprime à l’oral, à l’écrit, de façon graphique ou par des actions,
et fait appel à des stéréotypes inquiétants et à des traits de caractère
péjoratifs.

Parmi les exemples contemporains d’antisémitisme dans la vie publique,
les médias, les écoles, le lieu de travail et la sphère religieuse,
on peut citer, en fonction du contexte et de façon non exhaustive:

- l’appel au meurtre ou à l’agression de Juifs, la participation à ces
  agissements ou leur justification au nom d’une idéologie radicale ou
  d’une vision extrémiste de la religion;
- la production d’affirmations fallacieuses, déshumanisantes, diabolisantes
  ou stéréotypées sur les Juifs ou le pouvoir des Juifs en tant que collectif
  comme notamment, mais pas uniquement, le mythe d’un complot juif ou
  d’un contrôle des médias, de l’économie, des pouvoirs publics ou
  d’autres institutions par les Juifs;
- **le reproche fait au peuple juif dans son ensemble d’être responsable
  d’actes, réels ou imaginaires**, commis par un seul individu ou groupe
  juif, ou même d’actes commis par des personnes non juives;
- la négation des faits, de l’ampleur, des procédés (comme les chambres à gaz)
  ou du caractère intentionnel du génocide du peuple juif perpétré par
  l’Allemagne nationale-socialiste et ses soutiens et complices pendant
  la Seconde Guerre mondiale (l’Holocauste, ou Shoah NDLR);
- le reproche fait au peuple juif ou à l’État d’Israël d’avoir inventé ou
   d’exagérer l’Holocauste (la Shoah, NDLR);
- **le reproche fait aux citoyens juifs de servir davantage Israël ou les
  priorités supposés des Juifs à l’échelle mondiale que les intérêts de
  leur propre pays**;
- **le refus du droit à l’autodétermination des Juifs, en affirmant par
  exemple que l’existence de l’État d’Israël est le fruit d’une entreprise raciste**;
- **le traitement inégalitaire de l’État d’Israël, à qui l’on demande
  d’adopter des comportements qui ne sont ni attendus ni exigés de tout
  autre État démocratique**;
- l’utilisation de symboles et d’images associés à l’antisémitisme
  traditionnel (**comme l’affirmation selon laquelle les Juifs auraient
  tué Jésus ou pratiqueraient des sacrifices humains**) pour caractériser
  Israël et les Israéliens;
- **l’établissement de comparaisons entre la politique israélienne contemporaine et celle des Nazis**;
- l’idée selon laquelle les Juifs seraient collectivement responsables
  des actions de l’État d’Israël.


**Un acte antisémite est une infraction lorsqu’il est qualifié ainsi par
la loi** (c’est le cas, par exemple, du déni de l’existence de l’Holocauste
ou de la diffusion de contenus antisémites dans certains pays).

Une infraction est qualifiée d’antisémite lorsque les victimes ou les
biens touchés (comme des bâtiments, des écoles, des lieux de culte et
des cimetières) sont ciblés parce qu’ils sont juifs ou relatifs aux Juifs,
ou perçus comme tels.

La discrimination à caractère antisémite est le fait de refuser à des
Juifs des possibilités ou des services ouverts à d’autres.

Elle est illégale dans de nombreux pays.


Mise à jour de la définition de l'IHRA par la déclaration de Jérusalem
===========================================================================

- :ref:`update_ihra`



Handbook for the practical use of the IHRA Working Definition of Antisemitism
=================================================================================

:download:`Download of the Handbook for the practical use of the IHRA Working Definition of Antisemitism <pdfs/handbook-for-the-practical-use-of-the-ihra-working-ds0321002enn-1.pdf>`

Used by
============

- https://blog.babka.social/community-guidelines/
- https://blog.babka.social/


Articles/critiques
==========================

Articles 2024
----------------------

- :ref:`raar_2024:stern_2024_05_21`


Articles 2023
-----------------------

2023-04-04 ONU Protéger les droits de l'homme en luttant contre l'antisémitisme - Ne pas adopter de définition controversée - LDH
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

:download:`Télécharger onu-proteger-les-droits-de-l-homme-en-luttant-contre-l-antisemitisme-ne-pas-adopter-de-definition-controversee-ldh (LDH) <pdfs/onu-proteger-les-droits-de-l-homme-en-luttant-contre-l-antisemitisme-ne-pas-adopter-de-definition-controversee-ldh.pdf>`

Lettre ouverte de 104 organisations, dont la LDH, adressée à M. Guterres,
secrétaire général des Nations unies, et M. Moratinos, secrétaire général
adjoint des Nations unies.

Les groupes de défense des droits de l’Homme et d’autres groupes de la société
civile exhortent les Nations unies à respecter les droits de l’Homme dans la
lutte contre l’antisémitisme.

Les Nations unies devraient respecter les droits de l’Homme dans leurs efforts
pour combattre l’antisémitisme, ont déclaré 60 organisations de défense
des droits de l’Homme et des droits civils, dont Human Rights Watch, dans
une lettre ouverte au Secrétaire général António Guterres et au Haut
Représentant pour l’Alliance des civilisations de l’ONU Miguel Ángel
Moratinos.

Les groupes ont déclaré que l’antisémitisme était pernicieux, qu’il causait
un réel préjudice aux communautés juives du monde entier et qu’il nécessitait
une action significative pour le combattre.

Toutefois, les dirigeants de l’ONU doivent veiller à ce que leurs efforts de
lutte contre l’antisémitisme “n’aient pas pour effet d’encourager ou de soutenir
des politiques et des lois qui portent atteinte aux droits de l’Homme fondamentaux,
y compris le droit de s’exprimer et de s’organiser en faveur des droits
des Palestiniens et de critiquer les politiques du gouvernement israélien”,
ont déclaré les groupes.

Les groupes ont exhorté les Nations unies à ne pas approuver ou adopter la
définition de travail de l’antisémitisme de l’Alliance internationale pour
la mémoire de l’Holocauste (IHRA).

Cette définition a été utilisée à tort pour qualifier d’antisémites certaines
critiques des politiques du gouvernement israélien et/ou la défense des
droits des Palestiniens.

Ils ont noté qu’il existe au moins deux autres définitions qui, selon un certain
nombre d’universitaires, sont moins susceptibles d’être utilisées à mauvais escient
:ref:`la Déclaration de Jérusalem sur l’antisémitisme <declaration_jerusalem>`
et le Document de Nexus.

Articles 2021
----------------------

2021-06-16 Contre l’instrumentalisation de l’antisémitisme à des fins politiques (LDH)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

:download:`Télécharger contre l’instrumentalisation de l’antisémitisme à des fins politiques (LDH) <pdfs/ldh-contre-l-instrumentalisation-de-l-antisemitisme-a-des-fins-politiques.pdf>`

Articles 2020
-----------------------

2020-11-30 Déclaration sur l’antisémitisme et la question de Palestine
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://blogs.mediapart.fr/les-invites-de-mediapart/blog/301120/declaration-sur-l-antisemitisme-et-la-question-de-palestine
