.. index::
   ! Olivier Manonni

.. _olivier_manonni:

==========================================================================
**Olivier Manonni** |OlivierManonni|
==========================================================================

- https://fr.wikipedia.org/wiki/Olivier_Mannoni


Biographie
============

Olivier Mannoni, né le 14 septembre 19601, est un traducteur d'allemand,
journaliste et biographe français.


Olivier Mannoni est le fils de Nicole Casanova, écrivaine, traductrice d'allemand
et critique littéraire française et d'un père professeur d'allemand.

Son père lui apprend l'allemand dès l'âge de 6 ans

Il étudie en classe préparatoire littéraire au lycée Henri-IV (Paris) puis
à l'université.

En 1978, au même moment qu'il rédige des piges (Libération, L'Événement du
jeudi, Le Magazine littéraire), il commence à traduire de petits ouvrages.
Il devient critique littéraire à La Quinzaine littéraire, entre 1992 et 19984.

Sa carrière de traducteur d'allemand démarre, entre autres, par un essai
sur la vie et l'œuvre du peintre allemand George Grosz.
Plus tard, on lui confie les biographies de Günter Grass et de Manès Sperber,
dont il a dirigé, aux éditions Odile Jacob, la publication des œuvres
complètes.
Un article élogieux à son égard dans Le Monde, lui ouvrira les portes à des
traductions plus complexes et prestigieuses.

Ainsi, on lui commande la traduction de philosophes tels Hans Blumenberg,
Helmuth Plessner, Odo Marquard ou Peter Sloterdijk, de sociologues
(Harald Welzer, Wolfgang Sofsky), d'historiens (Peter Reichel, Joachim Fest),
ainsi que les principaux textes et correspondances de Sigmund Freud,
et de romanciers comme Martin Suter, Sherko Fatah, Peter Berling, Wolfram Fleischhauer,
Thomas Glavinic, Gaby Hauptmann, Jörg Kastner, Uwe Tellkamp, Bernhard Schlink,
Milena Michiko Flašar, Franzobel, Frank Witzel, Philipp Weiss, etc.

Il se distingue également par une cinquantaine de traductions de l’histoire
de l’antisémitisme et du nazisme.

Sa renommée s'accroitra d'autant plus lorsque sera publié **Historiciser le mal** :
une édition critique de Mein Kampf qu'il traduira de 2011 à 2021.

Il préside `l'Association des traducteurs littéraires de France <https://fr.wikipedia.org/wiki/Association_des_traducteurs_litt%C3%A9raires_de_France>`_ de 2007 à 2012.


Livre en 2024
=================

- :ref:`coulee_brune_2024`

Livre en 2022
=================

- :ref:`traduire_hitler_2022`
