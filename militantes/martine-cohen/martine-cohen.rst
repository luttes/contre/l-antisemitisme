.. index::
   ! Martine Cohen

.. _martine_cohen:

=================================================================================================================
**Martine Cohen**
=================================================================================================================


Biographie
=================

- https://esprit.presse.fr/ressources/portraits/martine-cohen-2829

**Martine Cohen**
================================================


- :ref:`raar_2025:martine_cohen_2025_01_01`
- :ref:`raar_2024:martine_cohen_2024_12_15`
- :ref:`raar_2024:martine_cohen_2024_06_01`
