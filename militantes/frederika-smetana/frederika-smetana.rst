.. index::
   pair: Militante ; Frederika Smetana
   pair: Artiste ; Frederika Smetana
   ! Frederika Smetana
   ! Marco Lipszyc
   ! Parcours de résistants

.. _frederika_smetana:

================================================
**Frederika Smetana**
================================================


.. figure:: images/frederika_smetana.webp

   Frederika Smetana le 20 janvier 2024


Biographie
===========

Après une formation au CNR de Nice, elle entre à l’Académie Supérieure
de Théâtre de Prague.

À Paris, elle suit les cours de Niels Arestrup, Philippe Minyana, Francine Bergé
à l’École du Passage.

Elle a travaillé avec Petr Forman et Ivo Krobot au Théâtre National
de Prague. Elle a interprété le rôle de Jeanne d’Arc dans l’oratorio de
Honegger-Claudel aux côtés de Michel Favory, de la Comédie française,
sous la direction de Serge Baudo.

Après la Révolution de velours, elle devient responsable de la programmation Théâtre
et Danse aux côtés d’Olivier Poivre d’Arvor, à l’Institut français de Prague.
Elle a assisté Daniel Mesguich pour la création de l’opéra de Laurent Petitgirard
« Elephant-man » à l’Opéra d’État de Prague, puis à l’Opéra de Nice.
Elle a traduit plusieurs textes de Michal Laznovsky ou d’auteurs tchèques
destinés aux créations de la compagnie.


.. _marco_lipszyc:

**Marco Lipszyc** (né le 26 novembre 1912 à Łódź et mort le 21 juillet 1944 à Seyssinet-Pariset)
======================================================================================================


Frederika Smetana est la petite fille de Marco Lipszyc.


.. figure:: images/marco_avec_sa_fille_et_sa_femme.webp

Biographie
-------------

- https://fr.wikipedia.org/wiki/Marco_Lipszyc

Marco Lipszyc, dit Jean Figiel ou Marc Lenoir, né le 26 novembre 1912
à Łódź et mort le 21 juillet 1944 à Seyssinet-Pariset, est un militant
socialiste puis communiste d’origine polonaise **qui s’impose comme l’un
des organisateurs les plus efficaces de l’action clandestine dans la
région grenobloise**.

.. _livre_marco_lipszyc_2015:

Le livre **Marco Lipszyc, étranger et notre frère pourtant** édité en 2015
--------------------------------------------------------------------------------

En 2015, l'historien Claude Collin écrit Marco Lipszyc, étranger et notre
frère pourtant édité par le Musée de la Résistance et de la Déportation
de l'Isère, dans la collection Parcours de résistants.

.. figure:: images/couverture_marco_lipszyc.webp

.. figure:: images/verso_livre.webp

.. figure:: images/preface.webp

Recherches
++++++++++++++


.. figure:: images/sources_livre.webp

Remerciements
+++++++++++++++

.. figure:: images/remerciements_livre.webp

Parcours de résistants
-------------------------

.. figure:: images/parcours_de_resistantes.webp

|GolemTheatre| Actions/Pièces/Représentations 2024
=======================================================

- :ref:`raar_2024:golem_theatre_2024_01_20`
