.. index::
   pair: Militante ; Tal Piterbraut-Merx
   pair: Tal ; Piterbraut-Merx
   ! Tal Piterbraut-Merx

.. _tal_piterbraut_merx:

===============================================================================
**Tal Piterbraut-Merx (1992-2021 🖤 5732-5782)** ❤️ |TalPiterbrautMerx| ❤️
===============================================================================

- https://fr.wikipedia.org/wiki/Tal_Piterbraut-Merx
- https://www.editionsblast.fr/auteur-es

.. figure:: images/tal.webp

   ❤️

.. figure:: images/mur_hommage.webp

Communiqué des ami·es et proches de Tal
============================================

Source: https://www.editionsblast.fr/outrages

Notre ami et camarade Tal Piterbraut-Merx s'est donné la mort le lundi
25 octobre 2021.

Victime d'inceste pendant son enfance, il pensait, écrivait et luttait
depuis des années contre la domination adulte et les violences sexuelles.

Très active au sein du collectif Clafoutis, il co-animait des ateliers
de prévention des violences faites aux enfants à destination des
personnels-les des écoles, des enfants et de leurs adultes référents-es.

Elle avait déjà produit de nombreux articles et communications universitaires
et militantes autour de ces sujets, sur lesquels portait également la
thèse qu'il était en train de rédiger.

Gouine, elle était en première ligne des luttes féministes et lesbiennes.

Juif, **il se battait sans relâche contre l'antisémitisme et toutes formes de
racismes**, aux côtés de ses camarades de Juives etJuifs Révolutionnaires
et des Juifves VNR.

Tal avait amorcé au fil des années **un courageux retour vers le judaïsme,
une réappropriation de ses rites et aspects spirituels, toujours à travers
un regard critique, queer et féministe**.

Il cherchait à s'élever au-dessus de la seule dimension de la judéité
qui lui avait été transmise, celle vécue à travers la douleur de la Shoah.

Il avait dans ce sens participé à la co-création d'espaces et de moments
autour de célébrations féministes et égalitaires des fêtes et des rites juifs,
dans une volonté permanente de rassembler dans la joie et la douceur,
si chères à son cœur.

Il avait également publié deux romans, dont le dernier, :ref:`Outrages <outrages_2021_03_12>`,
est  paru aux éditions Blast en mars 2021.
Elle travaillait à la rédaction d'un troisième ouvrage.

Il s'opérait à l'intérieur de Tal cette réconciliation extraordinaire,
celle d'être juif, queer, gouine, d'extrême gauche.
C'est une perte qui nous bouleverse, et dont nous continuerons longtemps
à prendre la mesure, tant sa présence nous enrichissait, tous-tes,
personnellement, politiquement, professionnellement.

**Tal ne passait jamais inaperçu, elle était monumentale**.

Dès l'annonce de sa mort, ses ami-e-s et proches, sa sœur, son frère, ses
camarades de lutte se sont réuni-e-s chaque jour pour se soutenir et rendre
justice à ses engagements.

Tal est mort de l'inceste, de l'antisémitisme, de l'homophobie, de la
domination patriarcale contre lesquelles elle luttait chaque jour, et
contre lesquels nous continuerons de lutter.

Nous ressentirons longtemps son manque dans la pensée politique et
philosophique, dans nos luttes et dans nos quotidiens.

Il nous manquera aussi comme ami, sincère, pleinement présent, exigeant
et constant dans l'attention et l'amour qu'il nous portait, à chacun-e.

L'intelligence et la puissance collective font des merveilles depuis sa mort,
et nous espérons de tout cœur que vous nous accompagnerez dans ces luttes
et dans le soin que nous tentons de porter au souvenir de notre amie.

Nous espérons que chaque personne qui nous it, et qui est affectée par
cette nouvelle, est entourée et trouvera le réconfort nécessaire dans
les jours qui viennent.

Les obsèques de Tal auront lieu vendredi 5 novembre à 11h, au cimetière de
Bagneux. Elles se dérouleront selon les rites juifs et il est donc inutile
d'apporter et de faire envoyer des fleurs.

Prenez soin de vous, Sincèrement,

Les ami-ess et proches de Tal


Articles sur Tal en 2025
==============================

- :ref:`raar_2025:tal_piterbraut_merx_2025_02_28`

Livre 2024 posthume
=========================

- :ref:`tal_piterbraut_merx_2024_10_25`

Livre 2021
==============

- :ref:`outrages_2021_03_12`


Fonds Tal Piterbraut-Merx :
=================================

- https://www.editionsblast.fr/outrages

Cher·es adelphes, nous tenions à vous informer de la création du Fonds
Tal Piterbraut-Merx que nous accueillons avec émotion.

La famille de Tal a en effet souhaité que les droits d'auteur issus de
la vente de son livre Outrages puissent bénéficier à d'autres écrits liés
aux thématiques féministes, queers et antiracistes.

Chaque livre dont les auteurices recevront le modeste soutien de ce fonds
en portera la mention. Que la mémoire de Tal perdure.


Titres concernés
-------------------

- Les Gisantes, Coline Fournout


Hommages 2024
================================

- :ref:`raar_2024:tal_piterbraut_merx_2024_10_25`


Tal Piterbraut-Merx (1992 - 2021) est écrivaine et chercheure en philosophie.
Elle rédigeait une thèse à l'ENS de Lyon portant sur la domination des
adultes sur les enfants comme point aveugle des théories contemporaines
de la domination.

Il s'agissait de montrer que les rapports sociaux adulte-enfant ne font
que rarement l'objet d'une analyse politique.

Militante et féministe, elle était engagée autour de ces questions.

Tal Piterbraut-Merx a également écrit le roman La Funambule (éditions Maurice Nadeau, 2017).
Outrages est son deuxième roman.

À titre posthume, le manuscrit de ses travaux de thèse a été revu, adapté
et enrichi par Anaïs Bonanno, Élise de la Gorce, Selma (Sam) Ducourant,
Félicien Faury, Léo Manac'h, Margaux Nève, Pierre Niedergang, Marion Pollaert,
Audrey Smadja Iritz et Léa Védie-Bretêcher, sous le titre "La domination oubliée, Politiser les rapports adulte-enfant".

3 questions à Tal Piterbraut-Merx
=====================================

À quoi bon la littérature ?
-----------------------------------
​
Le confinement a été le théâtre de nombreux débats sur le caractère essentiel
de commerces telles que les librairies. À
la lecture de la question « à quoi bon la littérature ? », je réalise
pourtant que curieusement les mots m’échappent, car l’acte de lecture
se déroule pour moi en silence.
Il y a quelque chose d’innommable dans l’effet produit par ces lettres
dansantes sur les pages, qui font cligner mes yeux et retrousser mes lèvres,
quelque chose qui ne doit pas s’énoncer à voix haute, par peur de se montrer
indiscret. La littérature, paradoxalement, agit en moi en deçà du langage
articulé ; elle me serre le poignet, trop fort, et à la place de phrases
me laisse en bouche un embarrassant magma, qui déborde souvent.

Si le livre est une denrée essentielle, il l’est alors comme un aliment
étrange et inquiétant, trop dur et trop mou à la fois, au goût âcre et
sucré, dont on pourrait aisément se passer, au risque toutefois d’y perdre ses dents.

​

L’acte d’écriture est-il un acte d’engagement ?
--------------------------------------------------------
​
Écrire, c’est soigneusement cueillir dans un champ de mots ceux qui
semblent les plus adéquats pour composer son récit.

En cela, l’acte d’écriture est bien un acte d’engagement : il impose de
prendre position, de trancher dans le vif, et peut-être surtout de nommer
des régions qui n’avaient pas pu l’être jusqu’alors.

Et pourtant, on n’écrit – heureusement – pas un roman comme on écrit un discours.

Mon militantisme est étroitement relié à mon écriture : il détermine en
grande partie le choix des thèmes, le choix des mots, et mon désir de
placer sous le feu des projecteurs des personnages que l’on préfère
souvent enfermer à double tour, dans un triste placard.

J’y déploie la même colère, la même tendresse que je pourrais le faire
dans d’autres espaces de mon existence.

Mais l’acte d’écriture exige de moi un positionnement bien plus risqué,
car j’y raconte des histoires.

En cela, le rapport à la vérité se pose de manière bien plus périlleux :
par peur d’être accusée de mensonge, par peur de travestir des ambiguïtés,
je dépose ma bouche contre les orties du langage, et l’en ressors gonflée.

Au sens strict, je ne parle pas la même langue.
​

Qu’est Outrages pour toi ?
------------------------------


La perspective adoptée dans Outrages peut selon moi se comprendre à
partir d’un film qui m’a vivement marquée adolescente, Festen de Thomas Vinterberg.

Outrages est un anti-Festen : il s’agit de refuser les coups d’éclat,
les grands règlements de comptes familiaux.

Ou plutôt il s’agit de montrer que vouloir rompre par la parole le silence
qui entoure les violences exercées au sein de la famille peut être un
piège, car on s’épuise à parler sans être entendu·e.

La thématique du secret est très importante pour moi, en particulier
lorsqu’elle touche à l’inceste, parce qu’on a tendance à considérer
qu’un secret doit être rompu.

L’envers du secret est ainsi représenté par le cri.

Or, l’acte du coming-out, qu’il touche d’ailleurs à l’inceste ou à
l’homosexualité, impose à la personne une manière de se dire qui est
aussi douloureuse et limitante.

**Je ne sais pas si on peut mourir de se taire, mais la captation de la
parole comporte des risques certains**.

​

​

​

​

​
