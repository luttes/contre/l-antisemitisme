.. index::
   pair: Militante ; Illana Weizman
   pair: Illana ; Weizman
   ! Illana Weizman


.. _illana_weizman:

===============================================
**Illana Weizman**
===============================================

- https://fr.wikipedia.org/wiki/Illana_Weizman


Introduction
===============

llana Weizman est une sociologue, autrice et militante féministe franco-israélienne.

Elle est la co-créatrice du hashtag #MonPostPartum qui dénonce les tabous
entourant cette période.


Biographie
===============

Illana Weizman, née Attali, naît et grandit en Île-de-France.

Elle étudie la communication, le droit et la sociologie avant de déménager
en Israël.
Installée depuis le début des années 2010 à Tel Aviv, elle est doctorante
en sociologie. Elle prépare une thèse ayant pour titre
**L’identité des Juifs français dans le monde digital : une cartographie de
Facebook et un cas d’étude de deux groupes**.

Militante féministe, elle est notamment la fondatrice du groupe israélien
de colleuses de rues HaStickeriot avec Morgane Koresh.

En 2020, elle prend part aux manifestations en soutien à une adolescente
violée à Eilat par une trentaine d'hommes.

2022
===========

En octobre 2022, elle publie l'ouvrage Des Blancs comme les autres?

Les Juifs, angle mort de l'antiracisme dans lequel elle pose cette question :
« L’antisémitisme est un racisme, mais pourquoi n’est-il pas considéré
comme tel au sein des luttes antiracistes ? »

Elle appelle à la convergence des luttes antiracistes et rappelle avec
Frantz Fanon : « Quand vous entendez dire du mal des juifs, dressez l’oreille :
on parle de vous. »
