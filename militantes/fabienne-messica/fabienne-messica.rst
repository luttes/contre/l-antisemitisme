.. index::
   pair: People ; Fabienne Messica
   ! Fabienne Messica

.. _fabienne_messica:

================================================
|FabienneMessica| **Fabienne Messica** |golem|
================================================

- https://blogs.mediapart.fr/messicafabienne

Biographie
============

Membre du collectif :ref:`golem` et du comité national de la :ref:`Ligue des
Droits de l'Homme <ldh>`.

.. figure:: images/fabienne_messica_2023.png

Fabienne Messica est sociologue, spécialiste de l’éducation, l’immigration
et les discriminations.

Elle a également travaillé avec l’Escale qui accueille des femmes victimes
de violences.
Membre de la Ligue des Droits de l’Homme, elle a publié des ouvrages sur
divers sujets : les parents et l’école, l’humanitaire, l’objection de
conscience en Israël (livre sur les refuzniks de l'armée israélienne)

LDH
====

Co-autrice du guide `Lutter contre le racisme <https://www.ldh-france.org/wp-content/uploads/2021/03/Guide-Lutter-contre-le-racisme-1.pdf>`_
de la LDH.

Il est disponible en ligne.

Dernières publications
===========================

- |Pornographes| Auteure de :ref:`Les pornographes du malheur <les_pornographes>` Éditions Rue de Seine. Mai 2023
- un roman pour adolescent·es, Mon nouveau pays est ici, aux Éditions Rue de Seine
  (histoire de deux jeunes migrants, leur fuite et leur découverte de la France)
- février 2022 un essai : Ce que le féminisme n’est pas, Éditions Rue de Seine.
- https://www.ldh-france.org/wp-content/uploads/2021/03/Guide-Lutter-contre-le-racisme-1.pdf


Discours/Ecrits de Fabienne Messica
========================================

Discours/Ecrits de Fabienne Messica 2024
-------------------------------------------------

- :ref:`raar_2024:fabienne_messica_2024_12_19`
- :ref:`raar_2024:fabienne_messica_2024_08_20`
- https://www.ldh-france.org/lutte-contre-le-racisme-lantisemitisme-et-les-discriminations/ (2024-03-20)

Discours/Ecrits de Fabienne Messica 2023
-------------------------------------------------

- :ref:`conflitpal:messica_2023_12_09`
- :ref:`messica_2023_11_16`
- :ref:`fabienne_messica_2023_11_09_3236`
- :ref:`fabienne_messica_2023_11_09_1595`
- :ref:`fabienne_messica_2023_11_09_820`
- :ref:`fabienne_messica_2023_11_09_660`
- :ref:`fabienne_messica_2023_11_09_236`
