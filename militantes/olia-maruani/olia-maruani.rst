.. index::
   pair: Militante ; Olia
   pair: RAAR ; Olia
   ! Olia Maruani

.. _olia:
.. _olia_maruani:

=================================================
**Olia Maruani**
=================================================

- https://blogs.mediapart.fr/olia-maruani
- https://nitter.cz/Oliouchka/rss (nitter n'est plus disponible depuis février 2024, X est une boite noire)
- https://web.archive.org/web/20231029102154/http://golema.net/analyses/quelques-reflexions-sur-lantisemitisme-et-son-deni-a-la-france-insoumise/

Bio
======

**Olia Maruani est auteure vidéaste.**

Elle réalise des vidéos pour la chaîne `Osons causer/Osons comprendre <https://www.youtube.com/@Osonscauser/videos>`_


Discours/interventions 2024
================================

- :ref:`raar_2024:olia_2024_05_23`
- :ref:`raar_2024:olia_2024_03_03`
- :ref:`raar_2024:olia_2024_02_09`
- :ref:`raar_2024:olia_2024_01_28`

Discours 2023
=================

- :ref:`raar_2023:olia_2023_05_14_638`
- :ref:`raar_2023:olia_2023_05_14_1169`
- :ref:`raar_2023:olia_2023_04_16`

Ecrits 2022
=============

- :ref:`raar:olia_2022_02_07`
- http://golema.net/analyses/quelques-reflexions-sur-lantisemitisme-et-son-deni-a-la-france-insoumise/
