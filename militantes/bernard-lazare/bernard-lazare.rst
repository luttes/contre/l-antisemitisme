.. index::
   pair: Militant ; Bernard Lazare
   ! Bernard Lazare


.. _bernard_lazare:

=================================================
**Bernard Lazare**
=================================================

- https://fr.wikipedia.org/wiki/Bernard_Lazare
- https://fr.wikipedia.org/wiki/Bernard_Lazare#Affaire_Dreyfus
- https://fr.wikipedia.org/wiki/Sionisme_libertaire

Lazare Bernard, dit Bernard Lazare, né à Nîmes (Gard) le 14 juin 1865
et mort à Paris le 1er septembre 1903, est un écrivain, critique littéraire,
journaliste politique (il couvre les événements de la mine de Carmaux),
anarchiste et polémiste français juif, et « très sincèrement athée ».

Il fut l'un des premiers dreyfusards et a théorisé le `sionisme libertaire <https://fr.wikipedia.org/wiki/Sionisme_libertaire>`_


Cité en 2023
=================

- :ref:`albert_herszkowicz_2023_11_09_3704`
