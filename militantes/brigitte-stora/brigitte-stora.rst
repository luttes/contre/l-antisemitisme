.. index::
   pair: Militante ; Brigitte Stora
   pair: RAAR ; Brigitte Stora
   ! Brigitte Stora

.. _brigitte_stora:

===========================================
**Brigitte Stora** |BrigitteStora|
===========================================

- http://www.brigitte-stora.fr
- https://x.com/BrigitteStora
- https://akadem.org/author/brigitte-stora
- https://raar.info/2022/11/webinaire-brigitte-stora-antisemitisme/
- https://www.youtube.com/results?search_query=brigitte+stora
- https://www.youtube.com/watch?v=qQofsEhKiO8&ab_channel=LaR%C3%A8gledujeu (Gauchisme / islamisme : le témoignage de Brigitte Stora)


Biographie
=============

Brigitte Stora Sociologue de formation, journaliste indépendante,
**autrice de documentaires historiques** et de fictions radiophoniques
(France Culture, France Inter).

Autrice du livre : "Que sont mes amis devenus : les Juifs, Charlie,
puis tous les nôtres" (Ed. Le Bord de l’Eau) paru en 2016.

Docteure en psychanalyse, thèse soutenue le 20 septembre 2021 à
l’Université Paris Diderot sur "l’antisémitisme : un meurtre du sujet
et un barrage à l’émancipation" dont est issu le livre :ref:`L'antisémitisme : un meurtre intime <meurtre_intime_stora_2024>`

Maman de :ref:`guerrieres:hanna_assouline` 💚❤️

Livres
=========

- :ref:`livre_brigitte_stora_2024`
- :ref:`stora_livre_2016`

Fictions/Discours/écrits/recontres 2024
============================================

- :ref:`raar_2024:brigitte_stora_2024_12_09`
- :ref:`raar_2024:brigitte_stora_2024_11_19`
- :ref:`raar_2024:brigitte_stora_2024_10_14`
- :ref:`raar_2024:brigitte_stora_2024_10_13`
- :ref:`raar_2024:webinaire_2024_09_16`
- :ref:`cbl_grenoble:stora_2024_06_20`
- :ref:`raar_2024:stora_2024_05_30`
- :ref:`raar_2024:stora_2024_05_29`
- :ref:`raar_2024:stora_2024_05_26`
- :ref:`raar_2024:brigitte_stora_2024_05_23`
- :ref:`raar_2024:stora_2024_03_28`
- :ref:`raar_2024:stora_2024_02_18`
- :ref:`raar_2024:brigitte_stora_2024_02_07`

Discours/écrits 2023
=======================

- :ref:`stora_2023_11_27`
- :ref:`raar_2023:brigitte_stora_2023_10_15`

Discours/écrits 2021
=======================

- http://www.brigitte-stora.fr/2021/01/19/314/#comment-4133 (La caution juive des antisémites, "UJFP")
