.. index::
   pair: Militant ; Samuel Delor

.. _samuel_delor:

===================================================
**Samuel Delor**
===================================================


Livres 2024
=============

- :ref:`petit_manuel_2024`

Articles 2025
=====================================

- :ref:`raar_2025:samuel_delor_2025_02_19`
