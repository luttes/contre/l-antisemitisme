.. index::
   pair: Militant ; Emmanuel Revah
   pair: Humour ; Emmanuel Revah

.. _emmanuel_revah:

==================================================
**Emmanuel Revah** Humoriste |EmmanuelRevah|
==================================================

- https://emmanuelrevah.fr/
- https://tube.hoga.fr/c/emmanuel_revah/videos
- https://tube.hoga.fr/feeds/videos.xml?videoChannelId=1323
- https://www.youtube.com/@emmanuelrevah/videos
- https://framapiaf.org/@manurevah
- https://framapiaf.org/@manurevah.rss
- https://www.instagram.com/EmmanuelIRevah/
- https://blogs.mediapart.fr/emmanuel-revah
- https://blogs.mediapart.fr/emmanuel-revah/blog/310122/psychose-collective
- https://ko-fi.com/emmanuelrevah (support)
- https://www.paypal.com/paypalme/emmanuelrevah (support)

::

    Ma vie a changé le jour ou j'ai compris que l'autodérision ne consistait
    pas à se moquer des bagnoles.

    Humoriste par nécessité · anciennement sysadmin
    il/lui

    [EN]
    Comedian by necessity · previously sysadmin
    He/Him


Vidéos/sketchs 2024
=========================

- :ref:`raar_2024:revah_2024_10_21`
- :ref:`raar_2024:revah_2024_09_27`

.. _revah_2024_03_25:

Petit teaser d'un sketch sur les sorties antisémites de Jean-Luc Mélenchon
============================================================================


.. youtube:: rPgvnm9oLfI

   Melenchonmitisme [teaser]
