.. index::
   pair: Militante ; Elisheva Gottfarstein
   ! Elisheva Gottfarstein

.. _elisheva_gottfarstein:

========================================
**Elishéva Gottfarstein**
========================================

- https://k-larevue.com/author/elishevagottfarstein/

Ecrits 2024
=================

- :ref:`raar_2024:gottfarstein_2024_06_26`


Ecrits/Videos 2023
=====================

- :ref:`raar_2023:gottfarstein_2023_12_15`
