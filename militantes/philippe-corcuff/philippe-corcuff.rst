.. index::
   pair: RAAR ; Philippe Corcuff
   pair: Philippe ; Corcuff
   ! Philippe Corcuff


.. _philippe_corcuff:
.. _antisem_corcuff:
.. _corcuff:

=================================================================================================================================
**Philippe Corcuff**  professeur des universités en science politique à l’Institut d’Etudes Politiques de Lyon |corcuff|
=================================================================================================================================

- http://www.cerlis.eu/team-view/corcuff-philippe/
- http://www.grand-angle-libertaire.net/etape-explorations-theoriques-anarchistes-pragmatistes-pour-lemancipation
- https://blogs.mediapart.fr/philippe-corcuff/blog
- https://www.nouvelobs.com/journalistes/840/philippe-corcuff.html
- https://www.multitudes.net/author/philippe-corcuff/

Bio
=========

Membre du :ref:`RAAR <raar>`


- Professeur des universités en science politique à l'Institut d'Etudes
  Politiques de Lyon [philippe.corcuff@sciencespo-lyon.fr
- Membre du CERLIS (Université Paris Cité/Université Sorbonne Nouvelle/CNRS, UMR 8070)
- Contributions to Domination and Emancipation. Remaking Critique, Daniel Benson (ed.),
  Lanham (MD), Rowman & Littlefield International,
  Series "Reinventing Critical Theory", 2021, 294 p., https://rowman.com/ISBN/9781786607010/Domination-and-Emancipation-Remaking-Critique
- La grande confusion.
  Comment l'extrême droite gagne la bataille des idées, Paris, Textuel, 2021, 672 p.,
  https://www.editionstextuel.com/livre/la_grande_confusion
- Coanimateur depuis juin 2013 du séminaire de recherche militante,
  libertaire et pragmatiste ETAPE
  (Explorations Théoriques Anarchistes Pragmatistes pour l'Emancipation,
  http://www.grand-angle-libertaire.net/etape-explorations-theoriques-anarchistes-pragmatistes-pour-lemancipation/)
- Blog sur Mediapart, nommé "Quand l'hippopotame s'emmêle...",
  depuis avril 2008​ : https://blogs.mediapart.fr/philippe-corcuff/blog
- Chronique mensuelle sur le site de L'Obs, intitulée
  "Rouvrir les imaginaires politiques", à partir de mars 2023 :
  https://www.nouvelobs.com/journalistes/840/philippe-corcuff.html


Auteur récemment et notamment de
==================================

- Marx/Bourdieu: Convergences and Tensions, Between Critical Sociology
  and Philosophy of Emancipation", in Gabriella Paolucci (ed.),
  Bourdieu and Marx. Practices of Critique, London, Palgrave Macmillan,
  Series "Marx, Engels, and Marxism", 2022, pp. 131-151,
  https://doi.org/10.1007/978-3-031-06289-6_6
- "¿Hay un futuro político para el "postfascismo"?", Revista Stultifera
  de Humanidades y Ciencias Sociales (Universidad Austral de Chile),
  vol. 5, número 2, secundo semestre del 2022, pp. 267-278, https://doi.org/10.4206/rev.stultifera.2022.v5n2-11
- "Television Series as Critical Theories: From Current Identarianism to
  Levinas. American Crime, The Sinner, Sharp Objects, Unorthodox",
  Open Philosophy, vol. 5, no. 1, 2022, pp. 105-117, https://www.degruyter.com/document/doi/10.1515/opphil-2020-0160/pdf


.. _ecrits_corcuff_2025:

Ecrits/livres 2025
=====================

- :ref:`raar_2025:philippe_corcuff_2025_05_09`
- :ref:`raar_2025:philippe_corcuff_2025_03_14`
- :ref:`raar_2025:philippe_corcuff_2025_02_26`
- :ref:`raar_2025:philippe_corcuff_2025_01_24`
- :ref:`raar_2025:philippe_corcuff_2025_01_07_telerama`
- :ref:`raar_2025:philippe_corcuff_2025_01_07`


.. _ecrits_corcuff_2024:

Ecrits/livres 2024
=====================

- :ref:`raar_2024:philippe_corcuff_2024_10_21`
- :ref:`raar_2024:philippe_corcuff_2024_10_11`
- :ref:`raar_2024:philippe_corcuff_2024_10_01`
- :ref:`raar_2024:corcuff_2024_09_16`
- :ref:`raar_2024:corcuff_2024_09_12`
- :ref:`raar_2024:corcuff_2024_07_04`
- :ref:`raar_2024:corcuff_2024_07_01`
- :ref:`raar_2024:corcuff_2024_06_24`
- :ref:`raar_2024:corcuff_2024_06_11`
- :ref:`raar_2024:corcuff_2024_06_06`
- :ref:`raar_2024:corcuff_2024_05_21`
- :ref:`raar_2024:corcuff_2024_05_07`
- :ref:`raar_2024:corcuff_2024_04_29`
- :ref:`raar_2024:corcuff_2024_04_22`
- :ref:`raar_2024:corcuff_2024_04_05`
- :ref:`tontons_flingueurs_2024`
- :ref:`raar_2024:corcuff_2024_03_29`
- :ref:`libertaires_2024:etape_2024_01_19`

Ecrits 2023
=================

- :ref:`philippe_corcuff_2023_11_22`

Ecrits/Livres 2021
====================

- :ref:`la_grande_confusion`
