.. index::
   pair: Militant ; Alexandre Journo
   pair: Revue daï ; Alexandre Journo

.. _alexandre_journo:

=========================================
**Alexandre Journo** |golem| revue daï !
=========================================

Biographie
================

Alexandre Journo est critique et chercheur indépendant.

Il écrit sur l’histoire des idées qui ont animé le monde juif, pour les
revues Conditions, l’INRER, Daï ou encore Sifriatenou.

Ses recherches portent sur la littérature juive, du franco-judaïsme,
du problème de l’assimilation, du sionisme et de l’antisionisme.

Il est par ailleurs membre du bureau de La Paix Maintenant.

Il fait partie du comité éditorial de Daï

Discours/Ecrits 2024
======================

- :ref:`raar_2024:alexandre_journo_2024_11_25`
