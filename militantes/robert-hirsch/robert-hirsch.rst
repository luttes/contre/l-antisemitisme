.. index::
   pair: Militant ; Robert Hirsch
   pair: Livre ; La gauche et les Juifs
   pair: RAAR ; Robert Hirsch
   ! Robert Hirsch


.. _robert_hirsh:
.. _robert_hirsch:

======================================
|RobertHirsch| **Robert Hirsch**
======================================

Membre fondateur du :ref:`RAAR <raar>`

.. rhirsch@sfr.fr

Biographie
===============

Agrégé, Docteur en Histoire, Robert Hirsch fut professeur de lycée en
Lorraine, Seine Saint-Denis, à Paris, et chargé de cours à Paris XIII.

Il est l’auteur d’un ouvrage sur le **syndicalisme enseignant** et de
**Sont-ils toujours des Juifs allemands ? La gauche radicale et les Juifs
depuis 1968** (Arbre bleu éditions)

L’historien Robert Hirsch, auteur de « La gauche et les juifs », déplore
que sa frange radicale, dont il fait partie, ait délaissé ce combat
depuis plus de 20 ans.


.. _livres_robert_hirsch:

**Livres de Robert Hirsch** |GaucheEtJuives| |JuifsAllemands|
======================================================================

- https://www.syllepse.net/resistance-antinazie-ouvriere-et-internationaliste-_r_89_i_1052.html
- :ref:`la_gauche_et_les_juifs`
- :ref:`juifs_allemands`


Discours/rencontres 2024
===========================

- :ref:`raar_2024:robert_hirsch_2024_10_14`
- :ref:`raar_2024:hirsh_shoah_2024_09_16`
- :ref:`raar_2024:hirsch_2024_06_14`
- :ref:`raar_2024:hirsch_2024_05_26`


Discours/écrits 2023
=======================

- :ref:`robert_hirsch_2023_11_18`
- :ref:`robert_hirsch_2023_11_16`
- :ref:`robert_hirsch_2023_10_15`
- :ref:`table_ronde_3_2023_04_24_hirsch`

Dsicours/écrits 2022
=======================

- https://akadem.org/conferences/conf/politique/les-silences-coupables-de-la-gauche/46076
