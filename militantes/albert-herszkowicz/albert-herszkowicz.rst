.. index::
   pair: Militant ; Albert Herszkowicz
   pair: RAAR ; Albert Herszkowicz
   ! Albert Herszkowicz


.. _albert_herszkowicz:

=================================================
|AlbertHerszkowicz| **Albert Herszkowicz**
=================================================

- https://blogs.mediapart.fr/albert-herszkowicz


Militant antiraciste, également bloggeur à

- http://www.memorial98.org
- http://info-antiraciste.blogspot.fr/.

Santé publique, justice sociale.
Membre fondateur de :ref:`Memorial 98 <memorial98>` et du :ref:`RAAR <raar>`

..  albert.herszkowicz @ gmail.com


Discours 2025
=================

- :ref:`raar_2025:albert_herszkowicz_2025_01_27`


Discours 2024
=================

- :ref:`raar_2024:albert_herszkowicz_2024_11_09`
- :ref:`raar_2024:albert_herszkowicz_2024_08_25`
- :ref:`raar_2024:raar_com_2024_07_05_monde`
- :ref:`raar_2024:herszkowicz_intervention_2024_05_23`
- :ref:`raar_2024:herszkowicz_2024_05_23`

Discours 2023
=================

- :ref:`transcription_bruttmann_2023_12_18`
- :ref:`albert_herszkowicz_2023_11_09_4375`
- :ref:`albert_herszkowicz_2023_11_09_4142`
- :ref:`albert_herszkowicz_2023_11_09_3704`
- :ref:`albert_herszkowicz_2023_11_09_2950`
- :ref:`albert_herszkowicz_2023_11_09_2525`
- :ref:`albert_herszkowicz_2023_11_09_1975`
- :ref:`albert_herszkowicz_2023_11_09_1698`
- :ref:`albert_herszkowicz_2023_11_09_1761`
- :ref:`albert_herszkowicz_2023_11_09_37`
- :ref:`albert_herszkowicz_2023_10_15_52`
- :ref:`albert_herszkowicz_2023_10_15_3365`
- :ref:`albert_herszkowicz_2023_05_14_1800`
- :ref:`albert_herszkowicz_2023_05_14_917`
- :ref:`table_ronde_1_2023_04_24_herszkowicz`
