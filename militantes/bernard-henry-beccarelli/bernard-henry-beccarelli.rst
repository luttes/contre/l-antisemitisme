.. index::
   pair: Militant ;  Bernard Henry-Beccarelli
   pair: Vidéos ;  Bernard Henry-Beccarelli

.. _bernard_j_henry_beccarelli:
.. _bernard_henry_beccarelli:

===================================
**Bernard Henry-Beccarelli** 🎥
===================================

- https://x.com/GlobalSuburban

Membre du RAAR.

Chaine vidéo Global Suburban
===============================

- https://invidious.fdn.fr/channel/UCvX-BQE-l_KeoSbdV9J5Ywg
- https://invidious.fdn.fr/feed/channel/UCvX-BQE-l_KeoSbdV9J5Ywg


.. figure:: images/videos_2023_11.png


Nouvelle chaine GlobSubLife le samedi 25 mai 2024
======================================================

- https://www.youtube.com/@GlobSubLife/videos

.. figure:: images/chaine_global_sub_life.webp


Vidéos Bernard Henry-Beccarelli
======================================

- :ref:`bourse_du_travail_2024_05_23`
- :ref:`nuit_de_cristal_2023_11_09`
- :ref:`videos_raar_2023_10_15`
- :ref:`raar_varsovie_2023_04_16`
- https://invidious.fdn.fr/watch?v=4tvmA71gb0Q (Attentat antisémite à Djerba : "On oublie pas, on pardonne pas !" 15/05/2023 00:04)
- https://invidious.fdn.fr/watch?v=y3EOYLV0CJA (Avec le RAAR, contre l'étoile jaune d'hier et d'aujourd'hui 09/06/2022 01:08)
