.. index::
   pair: Militant ; Nadine Fresco
   pair: RAAR ; Nadine Fresco

.. _nadine_fresco:

=======================
**Nadine Fresco**
=======================

Membre fondateur de :ref:`Memorial 98 <memorial98>` et du :ref:`RAAR <raar>`


Biographie
============

- https://fr.wikipedia.org/wiki/Nadine_Fresco

Nadine Fresco est une historienne française qui s'est particulièrement
consacrée à l'étude la Shoah et du problème du négationnisme.

Elle fait des études d'histoire à l'université Paris 1 et, en 1974, y
soutient une thèse de troisième cycle en histoire médiévale1.

En 1980, à la suite du déclenchement de l'affaire Faurisson (liée à la
collusion du maître de conférences Robert Faurisson, du chercheur Serge Thion,
de l'activiste Pierre Guillaume et de son groupe ultra-gauchiste La Vieille Taupe),
elle intervient par un article dans Les Temps Modernes :
« Les redresseurs de morts » (juin 1980), à l'époque où Pierre Vidal-Naquet
est en train d'écrire « Un Eichmann de papier » (Esprit, novembre 1980).

Depuis lors, elle consacre une part de son activité à la question du
négationnisme (étude la trajectoire de Paul Rassinier, dans Fabrication
d'un antisémite en 1999) et du racisme, ainsi qu'à l'histoire de la
déportation des Juifs pendant la Deuxième Guerre mondiale.

En 1981, elle participe aux côtés de Léon Poliakov à la fondation de la
revue Le Genre humain et est toujours membre de la rédaction.

Elle s'intéresse aussi aux problèmes de bioéthique humaine.

Elle est actuellement chercheuse au CNRS, dans le cadre de l'Institut de
recherche sur les sociétés contemporaines (IRESCO) (après avoir été membre
du Centre de sociologie européenne).


Discours 2023
=================

- :ref:`nadine_fresco_2023_10_15`
