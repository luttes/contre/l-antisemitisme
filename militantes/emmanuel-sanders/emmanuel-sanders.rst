.. index::
   pair: Militant ; Emmanuel Sanders
   pair: JJR ; Emmanuel Sanders

.. _emmanuel_sanders:

=================================================
**Emmanuel Sanders** |EmmanuelSanders|
=================================================

- https://twitter.com/JackyCho8

Membre fondateur de :ref:`JJR <jjr>`


Discours/écrits 2024
======================

- :ref:`sanders_2024_08_25`
- :ref:`raar_2024:raar_com_2024_07_05_monde`
- :ref:`sanders_2024_05_23__2`
- :ref:`raar_2024:sanders_2024_05_23`
- :ref:`jjr:sander_antisem_2024_02_25`
- :ref:`jjr:sander_whistle_2024_02_25`
- :ref:`jjr:sander_shoah_2024_02_25`

Discours 2023
=================

- :ref:`manu_sanders_2023_11_09`
- :ref:`manu_sanders_2023_05_14`
