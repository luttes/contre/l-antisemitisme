.. index::
   pair: Militante ; Benoit Drouot
   pair: RAAR ; Benoit Drouot

.. _benoit_drouot:

=======================
**Benoit Drouot**
=======================

- https://www.youtube.com/watch?v=E8TuBhacAak

Biographie
=============

Vice-président de l’association ALARMER, Membre du cabinet du recteur
de l’académie de Dijon

Discours 2023
=================

- :ref:`table_ronde_3_2023_04_24_drouot`
