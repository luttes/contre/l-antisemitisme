.. index::
   pair: Militante ; Lorenzo Leschi
   pair: Golem ; Lorenzo Leschi

.. _lorenzo_leschi:

===========================
**Lorenzo Leschi** |golem|
===========================

Discours 2024
=================

- :ref:`raar_2024:raar_com_2024_07_05_monde`
- :ref:`raar_2024:lorenzo_2024_05_23`
