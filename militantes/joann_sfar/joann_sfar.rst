.. index::
   pair: People ; Joann Sfar

.. _joann_sfar:

=========================
**Joann Sfar**  |LeHai|
=========================

- https://www.instagram.com/joannsfar/
- :ref:`sfar_2023:joann_sfar_2023`

.. _pins_hay:

**Nous vivrons**, Restons unis face à la haine !
====================================================

- https://www.helloasso.com/associations/jem-01/boutiques/pin-s-hay
- :ref:`sfar_2023:le_hai_2023_10_11`

.. figure:: images/nous_vivrons.png

   https://www.instagram.com/p/CyGGxgToNoJ


Contact de l'organisme Jeunesse@judaismeenmouvement.org (2023-11-10)
------------------------------------------------------------------------

"Bonjour, Merci pour votre soutien. Sachez que le prix du pin's ne sert
qu'à recouvrir les frais de production et de gestion.

Les pin's sont d'ailleurs en cours de production et **nous les recevrons
dans une semaine**.

Dès qu'ils seront disponibles, nous vous les enverrons par courrier
(pour ceux qui ont choisi cette option) ou vous pourrez venir les chercher
au secrétariat de nos synagogues parisiennes aux heures habituelles
d'ouverture. Restons unis face à la haine ! "


Biographie par wikipedia
===========================

- https://fr.wikipedia.org/wiki/Joann_Sfar


Joann Sfar, né le 28 août 1971 à Nice (France), est un auteur prolifique
de bande dessinée, illustrateur, romancier et réalisateur français.

Auteur de très nombreuses bandes dessinées, il est notamment connu pour
ses séries Le Chat du rabbin qu'il a ensuite adaptées au cinéma, et Donjon.

Il a également illustré de nombreux ouvrages.

Depuis 2010 et son film Gainsbourg, vie héroïque, il est également réalisateur.

Depuis 2013, il s'est mis à l'écriture de romans, comme Le Plus Grand Philosophe de France.

Joann Sfar interroge tout particulièrement les rapports qu'entretiennent
entre elles les religions. Il traite de questions existentielles,
identitaires et philosophiques à travers les différents supports qu'il
emploie.


2023
======

- :ref:`joann_sfar_2023_11_12_censure`
- :ref:`joann_sfar_2023_11_12_pogrom`
- :ref:`grande_librairie_2023_11_08`
