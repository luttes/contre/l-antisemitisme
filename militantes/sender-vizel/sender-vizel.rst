.. index::
   pair: Militant ; Sender Vizel
   ! Sender

.. _sender:
.. _sender_vizel:

=================================================
**Sender Vizel** alias Volia  |jjr| |sender|
=================================================

- https://blogs.mediapart.fr/volia-vizeltzer
- https://blogs.mediapart.fr/sender-vitz
- https://www.instagram.com/sender_vitz/
- https://x.com/nounoursvener

.. _volia_vizeltzer:

Alias Volia
============

.. figure:: images/volia_2023.png

Bio
=====

Dessinateur de BD / militant juif de gauche.


Discours/écrits/dessins de Sender Vizeltzer en 2024
=============================================================

- :ref:`raar_2024:vizel_2024_08_18`
- :ref:`raar_2024:sender_2024_07_11`
- :ref:`raar_2024:sender_2024_07_06`
- :ref:`raar_2024:sender_2024_06_29`
- :ref:`raar_2024:sender_2024_05_23`
- :ref:`raar_2024:ref_jjr_2024_02_25`

- :ref:`goysplaining_2024_01_19`

Discours/dessins de Volia Vizeltzer en 2023
=============================================================

- :ref:`poncif_instrumentaliser`
- :ref:`volia_2023_11_09_3383`
- :ref:`volia_2023_11_09_3403`
- :ref:`volia_2023_11_09_2320`
- :ref:`volia_2023_11_09_1810`
- :ref:`volia_2023_11_09_381`
- :ref:`lettre_2023_10_28`
