.. index::
   pair: Archiviste ; Isaac Schneersohn
   pair: Grenoble ; Centre de Documentation Juive Contemporaine
   ! Isaac Schneersohn

.. _isaac_schneersohn:

===============================================================================================================
**Isaac Schneersohn** rabbin, industriel et **archiviste** (né en 1881 ou 1879, mort le 1969-06-24)
===============================================================================================================

Biographie
=============

- https://fr.wikipedia.org/wiki/Isaac_Schneersohn

Isaac Schneersohn, né le 14 mars 18811 à Kamianets-Podilskyï et mort
le 24 juin 1969 à Paris 16e, est un rabbin, industriel et **archiviste** juif russe.

Émigré en France après la Première Guerre mondiale, il fonde à Grenoble
en 1943 un centre de documentation qui, relocalisé dans le quartier du
Marais après la Libération, devient le Centre de documentation juive
contemporaine (CDJC).

Schneersohn en demeure le directeur ainsi que le rédacteur en chef de sa
Revue jusqu'à sa mort.


Jeunes années
-------------------

Isaac Schneersohn naît en 1879 ou en 1881 à Kamenetz-Podolsk, alors située
dans l’Empire russe.
Issu d’une illustre lignée hassidique qui remonte à Shneour Zalman de Liadi,
le fondateur du hassidisme Habad, il est nommé rabbin à l'âge de seize
ans et demi.

Officiant comme rabbin de la couronne (en) à Gorodnia puis à Chernigov,
il est vite attiré par les idées de la Haskala, entamant une carrière
dans les affaires et dans la politique, siégeant comme membre du conseil
municipal et maire adjoint de Riazan dans la région occidentale de la Russie.
Membre du parti Kadet (constitutionnel démocrate), il s’implique particulièrement
dans l'éducation de la communauté juive russe, s’attachant à les faire
franchir les quotas pour l’accès à l’enseignement supérieur en usant de
ses contacts avec le Tsar et d'autres personnalités.


Immigration en France
----------------------------

À la suite de l’accession au pouvoir des bolchéviques, Isaac Schneersohn
immigre en France en 1920..

Ayant acquis la citoyenneté française durant l'entre-deux-guerres, il
délaisse le rabbinat bien qu’il continue à observer les préceptes du
judaïsme par égard pour sa femme.
Devenu administrateur délégué de la Société anonyme de Travaux métalliques
(SATM, sise 10 rue Marbeuf à Paris)9,10 et menant la vie d’un « grand seigneur hassidique »,
il tient un salon où se rencontrent de nombreux dirigeants juifs parmi
les plus connus, dont Chaim Weizmann et Vladimir Jabotinsky !!,
Isaac Schneersohn adhérant lui-même au **sionisme révisionniste !!** de ce dernier.

Il accueille aussi, à l’occasion, ses cousins Menachem Mendel Horensztajn
et Menachem Mendel Schneerson, futur dirigeant des hassidim Habad.

Celui-ci pourrait d’ailleurs avoir été influencé dans ses choix académiques
par trois des fils de son cousin, Boris, Arnold, et Michel, étudiants à
l'École spéciale des travaux publics, du bâtiment et de l'industrie.

En 1938 il participe aux côtés du sculpteur Jacques Lifshitz et du peintre
`Marc Chagall <https://fr.wikipedia.org/wiki/Marc_Chagall>`_ à la transformation de « l'Arbeiter Orden » en Union des
Sociétés Juives de France.

La Seconde Guerre mondiale
=============================

- https://fr.wikipedia.org/wiki/Jacob_Gordin

Lorsque la Seconde Guerre mondiale éclate, les fils d'Isaac Schneersohn
sont mobilisés comme officiers de réserve de l'armée française.

Arnold et Boris, sont faits prisonniers. Isaac Schneersohn se voit
dépossédé de son poste en vertu des lois d'aryanisation.

Interné dans un Oflag, Arnold y organise un noyau de résistance, ce qui
lui vaudra d’être transféré dans un camp disciplinaire de Lübeck.

Quant à Boris, libéré en août 1940, il participera aux combats dans les
maquis de Dordogne.

C'est donc à Mussidan, en Dordogne, qu'Isaac Schneersohn s'installe
avec sa famille en 1941, après avoir quitté Paris pour Bordeaux.
**Il se réfugie ensuite à Grenoble, dans la zone d'occupation italienne**.

**À Grenoble, il conçoit le projet de créer un centre de documentation juive
en vue d’« amasser des preuves et des archives, constituer des dossiers
aisément accessibles, préparer le travail des historiens »**.

Une réunion se tient à son domicile ; y participent son secrétaire Léon Poliakov
et une quarantaine de délégués d'organisations juives dont le philosophe
`Jacob Gordin <https://fr.wikipedia.org/wiki/Jacob_Gordin>`_.

Le comité de direction est composé, outre Isaac Schneersohn lui-même, de
deux représentants du Consistoire central israélite de France, deux représentants
de la Fédération des Sociétés juives de France, deux représentants de
l'Union des Sociétés Juives de France, un représentant de l'Organisation
Reconstruction Travail et un représentant du rabbinat.

**Les travaux du comité sont interrompus par l'invasion allemande de la zone
italienne en septembre 1943**.

Isaac Schneersohn et Léon Poliakov rejoignent Paris lors de l'insurrection
d'août 1944, réussissant à prendre possession des archives du Commissariat
général aux questions juives, de l'ambassade d'Allemagne à Paris,
de l'état-major et, surtout, du service antijuif de la Gestapo.

**Le Centre de Documentation Juive Contemporaine est officiellement fondé
peu après dans le Pletzl**, centre de la vie juive avant la guerre ;

il publie  dès 1945 trois ouvrages et une quinzaine d’autres au cours des six années
suivantes, permettant aux historiens de la Seconde Guerre mondiale de
découvrir ce que fut la condition des Juifs de France au cours de la Shoah..
