.. index::
   pair: Militant ; Jonas Pardo

.. _jonas_pardo:

===================================================
**Jonas Pardo** Formateur contre l'antisémitisme
===================================================

- https://akadem.org/author/jonas-pardo
- https://x.com/jonaspardoform (Jonas Pardo)
- https://www.lemonde.fr/m-le-mag/article/2023/11/08/jonas-pardo-antidote-a-l-antisemitisme-au-sein-de-la-gauche_6199014_4500055.html

.. figure:: images/jonas_pardo.png


Introduction
================

Militant d’extrême gauche, Jonas Pardo a longtemps passé sous silence
ses origines juives.

Jusqu’au jour où, après la tuerie de l’Hyper Cacher, à Paris, en 2015,
il a décidé de tendre un miroir à son propre camp, qu’il juge rongé par
le déni d’un antisémitisme latent.

Le trentenaire dispense depuis deux ans des formations visant à déconstruire
le racisme antijuif.

Livres
===========

- :ref:`petit_manuel_2024`
- :ref:`jonas_pardo_2023_11_12`


**Association Boussole contre l'Antisémitisme et tous les Racismes**
========================================================================

- https://www.helloasso.com/associations/association-boussole-contre-l-antisemitisme-et-tous-les-racismes/adhesions/adhesion-a-l-association-boussole

.. figure:: images/boussole_antiraciste.webp

Cela fait maintenant plus de deux ans que j’anime des formations contre
l’antisémitisme.
Au fur et à mesure que le nombre de stagiaires augmente, un réseau de
personnes qui veulent passer à l'action voit le jour.

Il nous fallait donc un moyen de rassembler les énergies et les initiatives
pour concrétiser des passages à l'action.

Cette initiative est une contribution pour inverser la tendance actuelle
du monde à se refermer.

Il s'agit d'un espace pour réunir celles et ceux qui pensent qu'un
changement est toujours possible, qui veulent se mettre en mouvement
plutôt que céder à la résignation et l'immobilisme.

Avec la Boussole, il s’agit non seulement de multiplier l’ampleur et le
nombre d’interventions que je mène déjà, mais aussi de développer des f
ormations spécifiques aux autres formes de racisme.

Pour contribuer à mettre en place un front commun antiraciste efficace.
Notre  méthode conjugue la pédagogie, la recherche scientifique et l’écoute
attentive des victimes du racisme avec des actions dans l’espace public.

L’association Boussole Antiraciste rassemble celles et ceux qui cherchent
des moyens d’agir qui reposent sur la pédagogie et l'action publique.

Elle fédère les propositions et les énergies qui font avancer concrètement
la lutte contre l’antisémitisme et, **très prochainement, les autres formes de racisme**.


Articles/videos/formations 2025
=====================================

- :ref:`raar_2025:jonas_pardo_2025_02_19`
- :ref:`raar_2025:jonas_pardo_2025_02_14`

Articles/videos/formations 2024
=====================================

- https://drive.google.com/file/d/1HDg9Oi-icCJTVnHml-tuzawVt3qaCkMa/view
  Une interview pour le Nouvel Obs de Samuel Delor et Jonas Pardo, auteurs
  de Petit manuel de lutte contre l'antisémisme. Rémi Noyon, "Il ne faut
  pas opposer la lutte contre l’antisémitisme et la solidarité avec les Palestiniens"

- :ref:`raar_2024:jonas_pardo_2024_12_10`
- :ref:`raar_2024:jonas_pardo_2024_11_24`
- :ref:`raar_2024:jonas_pardo_2024_11_19`
- :ref:`raar_2024:jonas_pardo_2024_10_25`
- :ref:`raar_2024:pardo_2024_10_03`
- :ref:`raar_2024:pardo_2024_07_13`
- :ref:`raar_2024:pardo_2024_06_26`
- :ref:`raar_2024:pardo_2024_06_17`
- :ref:`raar_2024:meurice_2024_05_29`

Articles 2023
=================

- :ref:`jonas_pardo_2023_11_12`
- :ref:`jonas_pardo_2023_11_09`
- :ref:`pardo_2023_10_12`
