.. index::
   pair: Militant ; Aleksander Edelman
   pair: RAAR ; Aleksander Edelman

.. _aleksander_edelman:

=================================================
**Aleksander Edelman**
=================================================

.. figure:: images/aleksander.png

Préface du livre "Ghetto de Varsovie"
=========================================

- :ref:`livre_ghetto_de_varsovie`

Discours 2023
=================

- :ref:`aleksander_edelmann_2023_04_16`
