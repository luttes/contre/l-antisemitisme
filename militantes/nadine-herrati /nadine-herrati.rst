.. index::
   pair: Militante ; Nadine Herrati
   pair: EELV ; Nadine Herrati
   ! Nadine Herrati

.. _nadine_herrati:

===============================================
|Nadine Herrati| **Nadine Herrati**
===============================================

- :ref:`gt_eelv`

Membre du :ref:`groupe de travail contre l'antisémitisme <gt_eelv>`


Discours/organisation 2023
================================

- :ref:`table_ronde_1_2023_04_24_herrati`
- :ref:`table_ronde_3_2023_04_24_herrati`
