.. index::
   pair: Militant ; Arié Alimi

.. _arie_alimi:

=================================================================
**Arié Alimi, avocat, vice-président de la LDH** |ArieAlimi|
=================================================================

- https://twitter.com/AA_Avocats
- https://fr.wikipedia.org/wiki/Ari%C3%A9_Alimi
- https://www.ldh-france.org/bordeaux-2024-la-ldh-en-congres/

Depuis le 20 mai 2024, Arié Alimi est vice-président de la Ligue des Droits
de l'Homme (LDH)

Livres
===========

- :ref:`alimi_livre_2024_03`
- :ref:`extremes_droites:etat_hors_loi_2023_09_14`
- :ref:`extremes_droites:coup_d_etat_d_urgence_2021_01_21`

Articles/interventions 2024
==============================

- :ref:`raar_2024:arie_alimi_2024_11_07`
- :ref:`raar_2024:arie_alimi_2024_10_25`
- :ref:`raar_2024:arie_alimi_2024_05_17`
- :ref:`raar_2024:alimi_2024_05_11_freres_et_soeurs`
- :ref:`crha_2024:alimi_2024_05_11`
- :ref:`raar_2024:alimi_2024_04_07`
- :ref:`raar_2024:arie_alimi_2024_03_21`
- :ref:`raar_2024:alimi_2024_04_02`
