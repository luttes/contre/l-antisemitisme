.. index::
   pair: Militant ; Gregory Gutierez
   pair: EELV ; Gregory Gutierez
   ! Gregory Gutierez


.. _gregory_gutierez:

===============================================
|Gregory Gutierez| **Gregory Gutierez**
===============================================

- https://pouet.chapril.org/@Greguti
- https://www.gregorygutierez.com/doku.php/accueil


Membre du :ref:`groupe de travail contre l'antisémitisme <gt_eelv>`


Actions 2024
=================

- :ref:`raar_2024:gutierez_2024_08_05`
- :ref:`raar_2024:gutierez_2024_05_14`


Discours 2023
=================

- :ref:`gregory_2023_10_15_3941`
