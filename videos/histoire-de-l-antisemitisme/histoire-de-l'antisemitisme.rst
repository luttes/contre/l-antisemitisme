.. index::
   pair: Arte Vidéo ; Histoire de l'antisémitisme

.. _histoire_antisemitisme:

==============================================
**Histoire de l'antisémitisme** sur Arte
==============================================

- https://boutique.arte.tv/detail/histoire-de-l-antisemitisme
- https://www.youtube.com/playlist?list=PLCwXWOyIR22t-06VPU4-VaNSBLEb14jzV
- https://www.arte.tv/fr/videos/089973-001-A/histoire-de-l-antisemitisme-1-4/
- https://www.arte.tv/fr/videos/089973-002-A/histoire-de-l-antisemitisme-2-4/
- https://www.arte.tv/fr/videos/089973-003-A/histoire-de-l-antisemitisme-3-4/
- https://www.arte.tv/fr/videos/089973-004-A/histoire-de-l-antisemitisme-4-4/


Introduction
================

De l’antijudaïsme à l’antisémitisme moderne, ce grand récit documentaire
explore les multiples facettes du phénomène, de ses origines jusqu’à
nos jours.

Quelle est l’origine de ce phénomène que nous appelons l’antisémitisme ?

Comment, depuis plus de deux mille ans, a-t-il accompagné la manière dont
nos sociétés se conçoivent et se construisent ?

Cette série retrace l’histoire de l’hostilité manifestée à l’encontre
des Juifs, depuis l’Antiquité jusqu’à nos jours, des bords de la
Méditerranée à ceux de la Volga, de la péninsule espagnole aux forêts de
bouleaux polonaises.

Elle croise Saint-Louis et Spinoza, Staline et le pape Jean
XXIII, Hitler et Alfred Dreyfus, des figures de haine ou de bienveillance.

Grâce aux traces laissées à travers le monde, aux reconstitutions historiques,
notamment à l’aide des images 3D d’Ubisoft, aux expertises d’une trentaine
d’historiens, sociologues, psychanalystes de premier plan, la série explore
les multiples facettes et les évolutions de cette « haine des Juifs ».



cette traversée de deux millénaires d’antisémitisme impressionne par
sa riche iconographie, son érudition et sa densité.

Elle accable aussi, tant l’histoire se répète.

Aux brèves accalmies (la période Al-Andalus,
la Pologne des XVIe et XVIIe siècles…) succèdent des torrents de haine,
le fléau voyageant à travers l’Europe.

Battant en brèche l’idée d’un
rejet immémorial, la série montre par quels ressorts l'antisémitisme,
instrumentalisé pour des raisons religieuses, politiques ou économiques,
s’est construit et recyclé à travers le temps. Les nazis ont par exemple
puisé dans l’imagerie moyenâgeuse pour stigmatiser leurs contemporains. Mais
ces persécutions ont paradoxalement renforcé l’existence de la communauté
juive, qui s’est structurée à force d’être ramenée à une identité
fantasmée. Cette histoire des discriminations est donc aussi celle d’une
émancipation et d’une résistance, des protestations du philosophe Philon
au Ier siècle à l’épithète rageuse de Robert Badinter (interviewé dans
la série) contre le négationniste Robert Faurisson, qualifié de "faussaire
de l’histoire", en passant par le poème du Russo-Israélien Haïm Bialik en
réaction au pogrom de Kichinev de 1903. Cette œuvre littéraire, pour une
fois suivie d’effet, exhortait les juifs à "se réveiller" et provoqua un
exode massif vers des terres plus hospitalières.

Réalisation

    Jonathan Hayoun

Producteur/-trice

    Simone Harari Beaulieu

Pays

    France

Année
    2022


.. _arte_video_antisem_1_4:

Histoire de l'antisémitisme (1/4) Aux origines, 38-1144
==============================================================

- https://www.arte.tv/fr/videos/089973-001-A/histoire-de-l-antisemitisme-1-4/

Les historiens situent le premier massacre en 38, à Alexandrie, qui abritait
une communauté juive florissante, ce qui déchaîna la fureur des Égyptiens.

Dès le IIe siècle, l’ardeur du prosélytisme ruine l’entente entre chrétiens
et juifs, les premiers accusant les seconds de déicide.

En 413, saint Augustin publie La cité de Dieu.

Ce livre influera durant près d’un millénaire sur la place dans la morale
chrétienne des juifs, "peuple témoin" qu’il s’agit à la fois de protéger et
d’assujettir, une idée reprise ensuite par les musulmans.

Aux XIe et XIIe siècles se déroulent les premières croisades, qui combattent
les musulmans mais aussi les juifs, vus comme l’ennemi intérieur.

.. _arte_video_antisem_2_4:

Le temps du rejet 1144-1791 (2/4)
======================================

- https://www.arte.tv/fr/videos/089973-002-A/histoire-de-l-antisemitisme-2-4/

Aux XIIe et XIIIe siècles, la situation des juifs en Europe se dégrade.

Les souverains durcissent les règles sous l’influence d’une chrétienté qui
s’étend et leur confère une légitimité. Ils vont s’astreindre à rendre visibles
des juifs que rien ne distinguait du reste de la population, à travers une
iconographie délirante (nez crochus, chapeaux pointus…) et des mesures
discriminatoires, tel le port de la rouelle, anneau jaune dont la couleur,
symbole de traîtrise, resurgira sous l’ère nazie.

En 1290, les premières expulsions surviennent en Angleterre avant de gagner
l’Europe de l’Ouest.


.. _arte_video_antisem_3_4:

De l’émancipation à la Shoah 1791-1940 (3/4)
================================================

- https://www.arte.tv/fr/videos/089973-003-A/histoire-de-l-antisemitisme-3-4/

Sous l’influence des révolutionnaires français, de nombreux juifs accèdent à
la citoyenneté en Europe.
Mais l’essor industriel amène de nouvelles formes d’hostilité.

Le poids de la religion diminuant, l’antisémitisme s’appuie désormais sur
des théories raciales.

Les juifs servent aussi de bouc émissaire à un courant socialiste populiste
qui réactive le cliché moyenâgeux du "juif homme d’argent" et leur impute
la misère ouvrière.

Sous l’impulsion du journaliste Theodor Herzl émerge le mouvement sioniste et
la revendication d’un État refuge.

En 1903, le pogrom russe de Kichinev indigne la communauté internationale.

Le XXe siècle est aussi marqué par l’avènement du régime hitlérien, à la
doctrine ouvertement antisémite.


.. _arte_video_antisem_4_4:

Les nouveaux visages de l’antisémitisme 1945 à nos jours (4/4)
=================================================================

- https://www.arte.tv/fr/videos/089973-004-A/histoire-de-l-antisemitisme-4-4/

Après la Shoah, la communauté internationale découvre avec stupeur l’extermination
de 6 millions de juifs.
Pourtant, l’antisémitisme n’a pas disparu, comme en témoigne le pogrom de
Kielce, en Pologne, où des rescapés des camps sont massacrés.

Après la proclamation de l’État d’Israël en 1948, les populations juives doivent,
en majorité, quitter les pays arabes. En 1965, à l’issue du concile Vatican II,
l’Église met fin à deux mille ans d’antijudaïsme.

Après une période d’accalmie dans les années 1960, l’antisémitisme adopte de
nouveaux visages.
