.. index::
   pair: Tuerie à l'école juive Ozar Hatorah ; 2012
   ! Ozar Hatorah
   ! Jonathan Sandler
   ! Gabriel Sandler
   ! Aryeh Sandler
   ! Myriam Monsonégo

.. _ozar_hatorah_2012:

===================================================================================================================
2012-03-19 **Tuerie à l'école juive Ozar Hatorah #JonathanSandler #AriéSandler #GabrielSandler #MyriamMonsonego**
===================================================================================================================

- https://fr.wikipedia.org/wiki/Attentats_de_mars_2012_en_France#Tuerie_%C3%A0_l'%C3%A9cole_juive_Ozar_Hatorah
- https://fr.wikipedia.org/wiki/Attentats_de_mars_2012_en_France
- https://fr.wikipedia.org/wiki/Otzar_Hatorah

#JonathanSandler #AriéSandler #GabrielSandler #MyriamMonsonego #OzarHatorah


.. figure:: images/jonathan_arie_gabriel_myriam.webp
   :width: 500

.. figure:: images/les_victimes.webp
   :width: 500

Un attentat au collège-lycée juif Ozar Hatorah situé rue Jules-Dalou
dans le quartier de La Roseraie au nord-est de Toulouse a lieu
**le 19 mars 2012** vers 8 h.


Un homme qui porte une caméra sanglée sur la poitrine arrive devant
l'école à bord d'un scooter.

Il descend de son véhicule et ouvre immédiatement le feu en direction
de la cour d'école.

Les 4 victimes sont:

- Jonathan Sandler âgé de 30 ans
- les fils de Jonathan Sandler Gabriel, 3 ans, et Aryeh, 6 ans
- Myriam Monsonégo, la fille du directeur de l'école, Yaakov Monsonégo,
  âgée de 8 ans


Le tueur blesse grièvement Aaron "Bryan" Bijaoui, âgé de 15 ans et demi
qui sera hospitalisé jusqu'au 12 avril.

Les corps des quatre victimes sont transportés mardi soir, 20 mars 2012,
à Roissy, où ils sont salués par le président Sarkozy.

Ils sont inhumés à Jérusalem au cimetière Har Hamenouhot de Givat Shaul
le 21 mars 2012 au matin, en présence d'Alain Juppé, ministre des Affaires
étrangères.

Le père de Jonathan Sandler, Samuel (Shmuel) Sandler, **qui vient de
perdre son fils et deux de ses petits-fils**, déclare :
"Mon petit cousin a été déporté à Auschwitz à l'âge de 8 ans en 1943.
Je pensais que les enfants ne seraient plus assassinés en France, ou
dans le monde.


Commentaires
================

Commentaires 2024
-----------------------

::

    - 12 ans que les magnifiques boucles blondes de Myriam
      tirées par les mains de l’infâme me hantent.
    - 12 ans que j’ai intériorisé le silence qui a suivi le massacre.
    - Il est de ces catastrophes qui changent votre rapport
      et votre compréhension du monde.

Articles
============

Articles 2025
-------------------

- :ref:`raar_2025:raar_com_2025_01_07`

Articles 2024
-------------------

- :ref:`jjr:jjr_2024_03_19`


Articles 2023
-----------------

- :ref:`toulouse_2023_11_14`

Marche contre l'antisémitisme organisée par le RAAR, Paris 13 mars 2022
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://raar.info/2022/03/marche-contre-lantisemitisme-13-mars/


.. youtube:: 6bG098YaYZE

.. youtube:: naWyUfcBk38

Articles 2017
-------------------

- https://juivesetjuifsrevolutionnaires.wordpress.com/2017/05/08/massacre-antisemite-de-toulouse-5-ans-apres-on-noublie-pas/


.. _samuel_sandler_2024_01_12:

2024-01-12 **Mort de Samuel Sandler**
----------------------------------------

- https://kolektiva.social/@raar/111747342533203534
- https://www.lemonde.fr/societe/article/2024/01/12/samuel-sandler-pere-et-grand-pere-de-trois-victimes-de-mohammed-merah-est-mort_6210451_3224.html

.. figure:: images/samuel_sandler.webp

- http://nitter.moomoo.me/Yonathan_Arfi/status/1745754781019255048#m

Depuis le 19 mars 2012, Samuel Sandler ne vivait plus pour lui.
Il vivait pour les autres.

**Il portait d'abord avec courage et dignité la mémoire de son fils Jonathan
et ses petis-fils, Arié et Gabriel, lâchement assassinés**.

Il portait aussi pour nous des paroles justes et sages, pour que notre
société garde les yeux ouverts face à l'islamisme et l'antisémitisme qui ont
frappé sa famille.

Que son souvenir et celui de Jonathan, Arié et Gabriel Sandler accompagne
nos pas.
