.. index::
   pair: Assassinats ; Sarah Halimi (4 avril 2017)
   pair: Sarah ; Halimi
   ! Sarah Halimi


.. _sarah_halimi:

==============================================
2017-04-04 **Sarah Halimi**
==============================================

- https://fr.wikipedia.org/wiki/Affaire_Sarah_Halimi


L'affaire Sarah Halimi est une affaire judiciaire française qui a pour origine
le meurtre antisémite d'une femme juive à Paris en avril 2017.

En décembre 2019, l'auteur des faits, Kobili Traoré, est jugé pénalement
irresponsable par la cour d'appel de Paris.

En avril 2021, après une large polémique publique, la décision est confirmée
par la Cour de cassation.

Le meurtrier est, depuis son interpellation, hospitalisé sous contrainte.
