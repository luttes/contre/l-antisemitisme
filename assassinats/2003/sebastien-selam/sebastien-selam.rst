.. index::
   pair: Assassinats ; Sébastien Selam (20 novembre 2003)
   ! Sébastien Selam

.. _sebastien_selam:

=======================================
2003-11-20 **Sébastien Selam**
=======================================

- https://fr.wikipedia.org/wiki/Affaire_S%C3%A9bastien_Selam

L'affaire Sébastien Selam concerne un meurtre antisémite commis à Paris
le 20 novembre 2003.

Son auteur, Adel Amastaibou, souffrant de schizophrénie, sera définitivement
reconnu comme irresponsable pénalement en raison de l'abolition de son
discernement, en 2010.
