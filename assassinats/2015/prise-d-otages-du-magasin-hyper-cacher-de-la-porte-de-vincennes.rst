.. index::
   pair: Assassinats ; 2015
   ! Yoav Hattab
   ! Yohan Cohen
   ! Philippe Braham
   ! François-Michel Saada

.. _hypercasher_2015_01_09:

=================================================================================================================
2015-01-09 **Les 4 victimes juives de la prise d'otages du magasin HyperCacher de la porte de Vincennes**
=================================================================================================================

- https://fr.wikipedia.org/wiki/Prise_d%27otages_du_magasin_Hyper_Cacher_de_la_porte_de_Vincennes
- https://fr.wikipedia.org/wiki/Prise_d%27otages_du_magasin_Hyper_Cacher_de_la_porte_de_Vincennes#Victimes


#YoavHattab #YohanCohen #PhilippeBraham #FrançoisMichelSaada


.. figure:: images/plaque.png

.. figure:: images/les_4_portraits.webp


**Yohan Cohen (20 ans)**
=============================

Yohan Cohen, 20 ans, étudiant et employé du magasin, est le premier
touché alors qu'il tente de s'interposer.
Il agonise longtemps sous les yeux des otages.


.. figure:: images/plaque_yohan_cohen.png

**Philippe Braham (45 ans)**
==================================

Philippe Braham, 45 ans, cadre commercial dans une société d'informatique
et frère du rabbin de la synagogue de Pantin;


**François-Michel Saada (63 ans)**
=======================================

François-Michel Saada, 63 ans, cadre supérieur à la retraite, tente
de ressortir, mais Coulibaly lui tire dans le dos ;


.. _yoav_hattab:

**Yoav Hattab (21 ans)**
=================================

Yoav Hattab, 21 ans, de nationalité tunisienne, jeune homme issu
d'une famille de sept enfants, fils de Benjamin Batou Hattab,
grand-rabbin de Tunis, directeur de l'école juive de Tunis.

Après avoir obtenu son baccalauréat au lycée français de Tunis, il
vient à Paris pour poursuivre des études de commerce international.

Remontant du sous-sol où il s'était caché, il s'empare d'une des
armes abandonnées — car hors service — par le preneur d'otages qui,
alerté par le bruit du maniement de l’arme qui s'enraye, le tue.

Patrice Oualid, gérant du magasin, est blessé au bras dès l'irruption
de Coulibaly et réussit à fuir.

Quatre autres personnes, dont trois policiers, ont été blessées
durant l'assaut.

Articles
===========

Articles 2025
-------------------

- :ref:`raar_2025:raar_com_2025_01_07`
