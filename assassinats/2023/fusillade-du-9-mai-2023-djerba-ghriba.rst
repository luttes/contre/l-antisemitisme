.. index::
   pair: Assassinats ; Synagogue de Ghriba à Djerba (2023)
   pair: Assassinats ; Benjamin Haddad (2023-05-14)
   ! Benjamin Haddad

.. _ghriba_djerna_2023:

==============================================================================================================
2023-05-09 **Fusillade du 9 mai 2023 dans la synagogue de Ghriba à Djerba** #BenjaminHaddad #AvielHaddad
==============================================================================================================

- https://fr.wikipedia.org/wiki/Synagogue_de_la_Ghriba_(Djerba)


.. _benjamin_haddad:

Benjamin Haddad #BenjaminHaddad
=======================================

- https://www.ouest-france.fr/monde/tunisie/qui-est-le-francais-tue-dans-lattaque-de-la-synagogue-qui-a-fait-quatre-morts-en-tunisie-37eac58c-ef02-11ed-8f02-f38554d448ff
- https://fr.timesofisrael.com/djerba-les-deux-victimes-juives-de-la-fusillade-identifiees/

Le Français tué mardi 9 mai 2023 dans l’attaque de la synagogue la Ghriba,
en Tunisie, est un commerçant marseillais de 42 ans, père de quatre enfants,
d’après La Provence et BFM Marseille.

La seconde victime civile serait son cousin tunisien de 30 ans, avec qui
il était en pèlerinage à la synagogue de la Ghriba.

.. _aviel_haddad:

Aviel Haddad #AvielHaddad
============================

Aviel, Tunisien-Israélien âgé de 30 ans, vivait à Djerba.


Benjamin et Aviel Haddad
============================

.. figure:: images/aviel_et_benjamin_haddad.png
   :align: center

   Aviel et Benjamin Haddad


.. figure:: images/benjamin_haddad.png
   :align: center

   Merci @OM_Officiel pour ce merveilleux hommage à Benjamin Haddad,
   juif marseillais tué dans l’attentat terroriste antisémite de la
   synagogue de la Ghriba à Djerba.


Cités en 2023
=============

- :ref:`djerba_2023_05_14`

Fusillade
==============

- https://fr.wikipedia.org/wiki/Fusillade_du_9_mai_2023_%C3%A0_Djerba

L'assaillant, qui est affilié au centre naval de la garde nationale à
Aghir, tue son collègue avant de prendre des munitions et de se diriger
vers la synagogue.

À son arrivée, il tire à l'extérieur de la synagogue, tuant deux cousins

- Aviel Haddad (résident de Djerba)
- et Benjamin Haddad (commerçant marseillais)

et deux policiers, avant d'être abattu.

Un troisième policier meurt des suites de ses blessures.

Neuf personnes sont blessées, dont cinq agents de sécurité et quatre
civils.

Il n'a pas été confirmé si les victimes civiles assistaient aux cérémonies
du pèlerinage.
Peu avant l’attentat, il avait publié sur Facebook une vidéo d’une
prière dans la synagogue.
