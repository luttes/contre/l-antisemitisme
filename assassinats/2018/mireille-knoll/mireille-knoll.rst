.. index::
   pair: Assassinats ; Mireille Knoll (23 mars 2018)
   pair: Mireille ; Knoll
   ! Mireille Knoll

.. _mireille_knoll:

============================================================
2018-03-23 **Assassinat de Mireille Knoll**
============================================================

- https://fr.wikipedia.org/wiki/Meurtre_de_Mireille_Knoll

Le meurtre de Mireille Knoll a été commis le 23 mars 2018 dans le 11e
arrondissement de Paris.

Mireille Knoll, âgée de quatre-vingt-cinq ans et rescapée de la Shoah,
a été poignardée à son domicile de l'avenue Philippe-Auguste.

Dans les jours qui ont suivi, deux suspects, Yacine Mihoub et Alex
Carrimbacus, ont été mis en examen pour meurtre antisémite.

Survenu presque un an après l'homicide de :ref:`Sarah Halimi <sarah_halimi>` dans le même
arrondissement, et le même jour que les attaques terroristes de
Carcassonne et Trèbes, l'événement a suscité un vif émoi.
