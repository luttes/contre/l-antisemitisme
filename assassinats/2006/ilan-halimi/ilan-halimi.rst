.. index::
   pair: Assassinats ; Ilan Halimi (13 février 2006)
   ! Ilan Halimi

.. _ilan_halimi_2006:

============================================================================
2006-02-13 **Assassinat et tortures d'Ilan Halimi**
============================================================================

- https://fr.wikipedia.org/wiki/Affaire_du_gang_des_barbares


.. figure:: images/ilan_halimi.webp

Contexte
=========

**Ilan Halimi**, de son nom de naissance **Ilan Jacques Élie Halimi**, fils
d'une famille juive marocaine, né le 11 octobre 1982, est un vendeur
travaillant dans un magasin de téléphonie mobile du boulevard Voltaire à Paris.

Le soir du vendredi 20 janvier 2006, après avoir dîné chez sa mère,
il rejoint en voiture Yalda dite aussi "Emma", jeune fille qui l'a
ouvertement dragué dans son magasin, une semaine plus tôt, et
l'a rappelé pour passer une soirée avec lui.

Après avoir pris un verre ensemble porte d'Orléans, il pense la
raccompagner chez elle dans la banlieue sud de Paris vers Sceaux
(Hauts-de-Seine).
Ilan Halimi, à peine sorti de son véhicule, est roué de coups par des
individus qui tentent d'appliquer un chiffon imbibé d'éther sur
son visage.
Alors qu'il se débat et appelle à l'aide, ils "l’assomment et le
jettent dans le coffre d’un 4X4", le menottent et lui couvrent
les yeux et la bouche ensanglantés de ruban adhésif.

Le 21 janvier 2006 au petit matin, sa petite amie Stéphanie Yin,
d'origine asiatique, avec qui il vit, cherche vainement la voiture
d'Ilan et, inquiète, téléphone à ses amis et à la famille Halimi.

Ce même jour, Youssouf Fofana envoie un courriel depuis un cybercafé
d'Arcueil à la famille Halimi, qui réclame 450 000 euros en échange
de la libération d’Ilan Halimi, assorti d'une photographie du jeune
homme yeux bandés, journal du jour dans les mains et pistolet pointé
sur la tempe.

"La famille prévient alors la police, et la brigade criminelle,
l'unité d'élite de la police judiciaire de Paris, se saisit de l’enquête".

Découvert nu, tondu, bâillonné, menotté, défiguré, le corps brûlé et
agonisant, le 13 février 2006, le long des voies ferrées du RER C à
Sainte-Geneviève-des-Bois dans le département de l'Essonne,
Ilan Halimi meurt peu de temps après son transfert à l'hôpital

Articles 2025
==================

- https://laregledujeu.org/2025/01/21/41463/le-20-janvier-2006-lenlevement-dilan-halimi-un-crime-antisemite-qui-a-bouleverse-la-france/
- https://kolektiva.social/@raar/113996176219374684
- https://bsky.app/profile/raar2021.bsky.social/post/3li2j5s4h3c2h

Articles 2022
==================

- https://raar.info/2022/02/ilan-halimi_rassemblement/

Articles 2021
==================

- :ref:`raar:ilan_halimi_2021_02_14`
