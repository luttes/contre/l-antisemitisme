.. index::
   ! Assassinats

.. _assassinats:

================
**Assassinats**
================

.. toctree::
   :maxdepth: 3

   2023/2023
   2018/2018
   2017/2017
   2015/2015
   2012/2012
   2006/2006
   2003/2003
