.. index::
   pair: Freemo ; Antisémite (qoto.org)

.. _freemo:
.. _freemo_2023_11_27:

=========================================================================================
2023-11-27 **Freemo antisémite (il héberge un serveur cloné de mastodon qoto.org)**
=========================================================================================

Le lundi 27 novembre 2023 un florilège de commentaires antisémites hallucinants.
Je n'en cite qu'un, le plus écoeurant.

**No but their parents did, and those parents were very irrespoinsible for invading a country**
=================================================================================================

- https://qoto.org/@freemo/111484603970984697

.. figure:: images/la_gerbe_freemo.png

   https://qoto.org/@freemo/111484603970984697

::

	@kravietz

	No but their parents did, and those parents were very irrespoinsible for
	invading a country, murdering tons of people and then having a 4 year old
	child they bring into a war zone **they** created...

	IT does **not** justify the hamas taking those kids, but it absolutely is
	a consequence of their parents choices that they were well aware of when
	they made it.

	@realcaseyrollins
