.. index::
   pair: Cas ; Urgence Palestine
   ! Urgence Palestine (antisémite)

.. _urgence_palestine:

======================================
**Urgence Palestine, antisémite**
======================================


Citée en 2024
================

- https://x.com/JackyCho8/status/1853555264517018064
  Il appelle à refaire le 7 octobre partout dans le monde oklm, et t'as
  quasi toute l'extrême gauche qui le regarde comme ça 🤩 ou qui au mieux
  ferme les yeux.
  La gueule de bois qui se prépare elle est phénoménale
  Merci à la vigilance de @goodbyelenine

- https://x.com/JackyCho8/status/1853561761711964646
  Ça fait déjà plusieurs mois que cette tribune a alerté sur les dérives antisémites à urgence Palestine.
  @UnionCoLib @NPA_officiel @lessoulevements c'est pas encore assez ?
  - degageons-lantisemitisme-du-soutien-au-peuple-palestinien/

- :ref:`raar_2024:urgence_palestine_2024_06_07`
- :ref:`raar_2024:urgence_palestine_2024_02_20`
