.. index::
   pair: Cas ; Frédéric Lordon
   ! Frédéric Lordon

.. _frederic_lordon:

======================================================================================================
**Frédéric Lordon** (confusionisme)
======================================================================================================


Cité en 2024
================

- :ref:`raar_2024:lordon_2024_03_28`
- https://www.editions-crise-et-critique.fr/ouvrage/benoit-bohy-bunel-contre-lordon/
  Benoît Bohy-Bunel, Contre Lordon
  Anticapitalisme tronqué et spinozisme dans l’œuvre de Frédéric Lordon
