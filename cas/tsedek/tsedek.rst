.. index::
   pair: Ignorance ; Tsedek
   pair: Dérapages ; Tsedek
   pair: Cas ; Tsedek

.. _tsedek:

===================================================================================
**Tsedek** (ignorance, dérapages anti-humanisme, et caution des antisémites)
===================================================================================

- :ref:`ujfp`

Analyse
=========

.. toctree::
   :maxdepth: 3

   analyse/analyse


Membres de Tsedek
=======================

.. toctree::
   :maxdepth: 3

   maxime-benatouil/maxime-benatouil


En 2025
=======================

- :ref:`raar_2025:tsedek_2025_02_19`
- :ref:`raar_2025:tsedek_2025_02_10`
- :ref:`raar_2025:tsedek_com_2025_01_30`


En 2024
=========

- :ref:`raar_2024:tsedek_2024_12_09`
- :ref:`raar_2024:tsedek_2024_10_25`
- :ref:`raar_2024:tsedek_2024_05_23__2`
- :ref:`raar_2024:tsedek_2024_05_23__1`
- :ref:`raar_2024:tsedek_caution_2024_05_22`
- :ref:`raar_2024:tsedek_2024_01_15`
- support à LFI

Cité en 2023
=======================

- :ref:`tsedek_2023_11_22_decolonial`
