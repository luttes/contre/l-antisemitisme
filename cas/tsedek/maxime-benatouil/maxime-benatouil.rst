.. index::
   pair: Maxime ; Benatouil
   ! Maxime Benatouil

.. _maxime_benatouil:

===================================================================================
**Maxime Benatouil** sait qui est un bon juif et qui est de gauche ou non
===================================================================================


Cité en 2025
==================

- :ref:`raar_2025:maxime_benatouil_2025_02_19`

Cité en 2024
==================

- :ref:`raar_2024:benatouil_2024_11_25`
