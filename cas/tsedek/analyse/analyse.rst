
.. _tsedek_analyse:

==========================================================
Analyse du texte Pourquoi un collectif juif décolonial
==========================================================

27 novembre 2023 | Par Tsedek!

Tsedek ! est un collectif né en juin dernier pour porter la voix de juifs et
juives décoloniaux·ales en France. Suite aux événements survenus depuis
le 7 octobre et afin de mieux faire comprendre nos positions, nous reprenons
ici notre manifeste, jamais encore publié sur Mediapart.  Tsedek ! est un
collectif de juifs et juives décoloniaux·ales luttant contre le racisme
d’État en France et pour la fin de l’apartheid et de l’occupation en
Israël-Palestine. Nous sommes en rupture avec les discours promulgués par
les institutions juives censées nous représenter et par la majeure partie
des collectifs juifs antiracistes français. Il est grand temps de faire
entendre notre voix et de construire ensemble un front juif antiraciste et
décolonial. Ce manifeste en est le premier jalon.

Pour une parole juive décoloniale – En tant que militant·e·s juifs·ve·s
décoloniaux·ales, nous comprenons à quel point notre société est
structurée par son histoire coloniale et raciale. En tant que juifs et juives,
nous n’oublions pas que c’est aux États-nations européens que nous devons
notre destruction, celle de nos histoires et de nos cultures. Que ce sont eux qui
ont fait du Juif un parasite, un corps étranger à l’État, justifiant les
persécutions et l’extermination des juifs et juives d’Europe. Eux encore
qui ont causé l’arrachement des juif·ve·s des pays arabo-musulmans aux
sociétés qui étaient les leurs, par la mise en concurrence des colonisé·e·s
et le soutien qu’ils ont apporté au projet nationaliste et colonial sioniste.

Aujourd’hui, l’État français et sa politique assimilationniste continuent
d’abîmer les juif·ve·s. Ni trop visibles, ni trop barbares, nous sommes
acceptables à condition de rester des victimes éternelles, pour permettre à
l’État de se rêver comme notre protecteur. En réalité, il continue de
fabriquer les conditions de notre disparition par son racisme et son rapport
identitaire à la laïcité, la mise en avant d’une prétendue culture «
judéo-chrétienne », et l’association des juif·ve·s français·es à
l’État d’Israël – faisant de nous des citoyen·ne·s à part.

Être juif·ve et le rester – L’histoire des communautés juives a donné
naissance à des rapports multiples au judaïsme, à la judéité et aux
non-juif·ve·s. Cette pluralité s’est heurtée aux murs de l’identité
blanche des États-nations européens, qui ont fait des juifs l’une des
premières figures de l’altérité. Né de cette modernité européenne, le
sionisme a fabriqué une version réductrice, anhistorique et ethno-nationale
de l’identité juive. Avec le soutien des pays occidentaux, il s’est
constitué comme le prolongement du judaïsme, voire comme son incarnation,
et a transformé notre expérience de la judéité en France. En faisant du
discours religieux un discours nationaliste, le sionisme détruit et déforme
les fondements du judaïsme et adopte précisément les structures à partir
desquelles les juif·ve·s ont été historiquement exclu·e·s de la société
occidentale : État-nation, colonialisme et race.

Tsedek ! rassemble des juives et juifs de différentes origines, croyant·e·s
et athé·e·s, aux parcours divers. Pour nous, la création d’une
ethnocratie n’est pas la condition préalable à l’émancipation et
à l’autodétermination des juif·ve·s. Nous nous réapproprions une
identité juive en la conjuguant à la lutte antiraciste et en proposant
une alternative culturelle, qui met en avant la préservation des cultures
juives et la solidarité avec d’autres minorités et groupes historiquement
opprimés. Notre judéité se décline autour de traditions partagées, de
joie, de poursuite de la justice sociale et de la réparation du monde qui
repose sur trois piliers : justice, paix et vérité.

Pour un militantisme internationaliste contre le colonialisme, le fascisme
et l’impérialisme – Nous voulons nous battre aux côtés de celles et
ceux qui luttent contre la violence économique, politique et symbolique que
continue d’exercer la France néocoloniale sur le monde, notamment dans
les outre-mers et en Afrique. Les politiques françaises et européennes
de l’immigration et le traitement xénophobe des exilé·e·s ne sont que
d’autres manifestations de la colonialité occidentale. De Calais à Lesbos,
les frontières, les murs et les barbelés tuent, et nous nous tenons aux
côtés des migrant·e·s et des travailleurs·euse·s sans-papiers.

Contre le racisme d’Etat
=============================

La lutte contre l’antisémitisme est une lutte pour l’égalité, qui
s’inscrit aux côtés d’autres combats antiracistes – Nous sommes en
rupture avec les institutions étatiques et les organisations qui prétendent
combattre l’antisémitisme en le définissant comme un racisme exceptionnel, à
part, et en le réduisant à des comportements individuels et anhistoriques. Ce
qui devrait être une lutte pour un monde plus juste, partout et pour tous·tes,
se voit instrumentalisé au profit d’une entreprise moralisatrice et trop
souvent détourné comme un outil de gouvernance raciste ou de défense du
projet colonial sioniste.

Refuser d’identifier les forces structurelles qui produisent le racisme
empêche la convergence des luttes antiracistes, protège les intérêts de
l’État, et offre un boulevard à l’autoritarisme. Comme tout racisme,
l’antisémitisme contemporain est un phénomène politique. Bien qu’ayant
ses spécificités historiques et étant façonné par diverses mutations, il
reste un produit du nationalisme et de la suprématie blanche, qui nourrit les
idéologies réactionnaires. Notre réponse doit donc aussi être politique. Nous
refusons de séparer la lutte contre l’antisémitisme des autres combats
antiracistes, et souhaitons participer à la construction d’un projet
politique de justice pour tous·tes.

Dépasser l’instrumentalisation de l’antisémitisme – En France et
ailleurs, des organisations conservatrices pro-Israël et des institutions
étatiques qui prétendent lutter contre l’antisémitisme perpétuent
l’amalgame « juif = sioniste » en présentant toute critique du sionisme,
de l’occupation, ou de l’apartheid israélien comme intrinsèquement
antisémite. Nous refusons que la lutte contre l’antisémitisme soit
détournée de son objectif égalitaire pour devenir un outil de gouvernance
autoritaire qui cible en particulier les musulman·e·s, la gauche, et les
organisations des droits humains. Nous refusons d’être la caution morale
d’un État qui a persécuté et exterminé les nôtres. La sécurité ou
les droits des juif·ve·s ne doivent servir à justifier des politiques
discriminant d’autres minorités ou limitant la liberté d’expression,
comme nous l’avons vu avec l’interdiction de manifestations en soutien au
peuple palestinien, la déprogrammation de militant·e·s lors de conférences
publiques, ou bien encore l’expulsion d’imams.

Contre l’islamophobie d’État – C’est l’islamophobie qui structure
aujourd’hui les nationalismes européens et c’est la figure du Musulman qui
représente l’altérité et l’intru. La théorie du grand remplacement,
de plus en plus normalisée, légitime la persécution des populations de
l’immigration post-coloniale, des réfugié·e·s et demandeur·euse·s
d’asile non-blanc·he·s. Les théories du complot caractéristiques de
l’antisémitisme sont désormais aussi mobilisées contre les personnes
musulmanes, accusées de vouloir déstabiliser et diviser nos sociétés. La
mémoire collective ne doit pas fermer les yeux sur les analogies entre
l’islamophobie actuelle et l’antisémitisme du siècle passé, ni sur
la façon dont la fixation sur un « islamo-gauchisme » imaginaire fait
écho au judéo-bolchévisme de la première moitié du XXe siècle. Face
au racisme d’État, nous luttons aux côtés de nos camarades ciblé·e·s
par l’islamophobie.

Pour la fin de l’apartheid et de l’occupation en Israël-Palestine
=======================================================================

L’État d’Israël est une émanation du colonialisme européen et doit son
existence à la dépossession des Palestinien·ne·s et à la négation de leurs
droits – Ni « conflit religieux », ni « lutte civilisationnelle », mais une
situation coloniale en Israël/Palestine. L’oxymore « démocratie juive » ne
désigne rien d’autre qu’une démocratie de façade, réservée exclusivement
aux juif·ve·s. De fait, seul un véritable processus de décolonisation
répondra à un objectif de justice et d’égalité. Aujourd’hui, deux
blocs aux forces et aux moyens radicalement inégaux s’opposent : d’un
côté un État suprémaciste et colonial qui bafoue le droit international et
bénéficie du soutien des puissances occidentales, de l’autre une population
colonisée, opprimée et dispersée, dont tout acte de résistance est perçu
comme illégitime. Nous nous tenons aux côtés des Palestinien·ne·s et des
Israélien·ne·s qui se battent pour une alternative réellement démocratique,
qui accordera les mêmes droits à tous·tes les habitant·e·s de la région,
de la Méditerranée au Jourdain.

Le combat antiraciste ne peut qu’être antisioniste – Le sionisme intègre
et prolonge les logiques raciales de la modernité européenne. Il cherche à
faire des juif·ve·s des blanc·he·s comme les autres, à les émanciper avec
les mêmes mécanismes qui ont fait d’elles et eux des opprimé·e·s. Dans ce
processus, les personnes juives non-européennes ont un statut de candidates à
la blanchité, et les Palestinien·ne·s constituent un corps étranger et non
assimilable. Nous ne fermerons pas les yeux sur le racisme de l’entreprise
sioniste – qui compte aussi des victimes juives – car nous pensons qu’il
est important de substituer à la lecture juif·ve/non-juif·ve, discours qui
bénéficie au régime israélien, une lecture du rapport colon/indigène.

De nombreuses voix juives au sein des milieux antiracistes français cherchent
à concilier antiracisme et sionisme, en le présentant comme un mouvement de
libération compatible avec des valeurs dites « progressistes ». Le sionisme
est un projet raciste colonial et ethno-nationaliste, dont les liens structurants
avec l’antisémitisme sont de plus en plus apparents. Nous le voyons notamment
avec le soutien inconditionnel des mouvements d’extrême droite à travers
le monde pour la politique israélienne de colonisation et d’apartheid. Les
systèmes coloniaux produisent et renforcent le fascisme, la suprématie,
et le racisme, y compris l’antisémitisme. Le sionisme n’a pas sa place
au sein des luttes antiracistes et anticoloniales, ni dans le combat pour les
droits humains. Une voix juive antiraciste ne peut qu’être antisioniste.

Juifs et juives contre l’apartheid israélien – Nous constatons avec
inquiétude que pour de nombreuses personnes juives, le judaïsme et le
sionisme ne font qu’un, que les juif·ve·s et l’État d’Israël ne font
qu’un. Cette articulation traverse tous nos espaces, qu’ils soient familiaux,
communautaires ou institutionnels. Ainsi, pour beaucoup de juifs et juives, les
appels à la libération de la Palestine sont perçus comme une menace pour notre
sécurité. Nous refusons que l’histoire douloureuse de l’antisémitisme
soit utilisée pour jouer avec la peur des nôtres et légitimer une entreprise
coloniale qui nie les droits des Palestinien·ne·s. Nous pensons au contraire,
comme d’autres militant·e·s juif·ve·s antisionistes – d’hier ou
d’aujourd’hui, d’Israël ou d’ailleurs – que la sécurité des
juif·ve·s, y compris celle des Israélien·ne·s, ne peut passer par la
colonisation et l’oppression du peuple palestinien. Nous savons aussi que
la colonisation détruit le colon et son humanité. Elle l’expose à la
violence conjoncturelle du colonisé, tout comme à celle de son propre système.

Tsedek ! soutient les Palestinien·ne·s dans leur lutte pour la liberté et
pour le respect de leurs droits, notamment à travers la campagne BDS (Boycott,
Désinvestissement, Sanctions). Tant que le « droit à l’autodétermination
des juif·ve·s » se fera au détriment des droits des Palestinien·ne·s,
il ne sera pas légitime et nous ne pourrons connaître une paix juste.

En tant que juif·ve·s français·e·s nous ne sommes pas responsables
de la politique israélienne, mais celle-ci est menée en notre nom. Nous
avons donc la responsabilité de la changer. Solidaires de collectifs juifs
antiracistes, anti-occupation et antisionistes dans le monde entier, comme
de groupes palestiniens et israéliens, notre combat n’est pas solitaire,
il s’inscrit dans un soulèvement international.
