.. index::
   ! Le pamphlet antisémite: Contre l'antisémitisme Et son instrumentalisation
   pair: pamphlet antisémite ; Contre l'antisémitisme Et son instrumentalisation

.. _livre_et_son_instrumentalisation:
.. _livre_et_son_instrumentalisation_2024:

==============================================================================================
2024-11-25 Le pamphlet antisémite: Contre l'antisémitisme **Et son instrumentalisation**
==============================================================================================

- :ref:`alexandre_journo_2024_11_25`


Préambule
==================

Si ce livre est un instrument, c’est celui-ci : prendre prétexte de
l’instrumentalisation de l’antisémitisme pour justifier les réactions
piteuses des auteurs au lendemain du 7 octobre, et persister et signer.


Cité en 2025
=============

Ceci n'est pas un livre sur l'antisémitisme (avec Alexandre Journo)
--------------------------------------------------------------------------

- https://podcasts.apple.com/us/podcast/ceci-nest-pas-un-livre-sur-lantis%C3%A9mitisme-avec/id1782640720?i=1000688157978

La critique écrite d'Alexandre Journo : https://www.dai-la-revue.fr/articles/202411/instrumentalisation-antisemitisme-fabrique

Sources en Français sur Moishe Postone : https://www.palim-psao.fr/search/moishe%20postone/

Bibliographie de Moishe Postone :

- https://urls.fr/4ROZWF
- https://urls.fr/KLmxbG
- https://urls.fr/ZAmI1T


Cité en 2024
=================

- https://www.dai-la-revue.fr/articles/202411/instrumentalisation-antisemitisme-fabrique
