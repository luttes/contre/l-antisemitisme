.. index::
   pair: Ignorance ; UJFP
   pair: Dérapages ; UJFP
   pair: Cas ; UJFP

.. _ujfp:

=====================================================================================
**L'UJFP et ses discours intolérables, anti-humanisme, caution des antisémites**
=====================================================================================

- :ref:`tsedek`

.. tags:: UJFP


En résumé
==============

- **l'UJFP rend responsables les Juifs de l’antisémitisme**
- **l'UJFP sert de caution aux antisémites**

Citée en 2025
=======================

- :ref:`raar_2025:ujfp_2025_02_19`
- :ref:`raar_2025:ujfp_2025_02_10`
- :ref:`raar_2025:ujfp_com_2025_01_30`

Citée en 2024
=======================

- :ref:`raar_2024:ujfp_2024_12_09`
- :ref:`raar_2024:ujfp_2024_10_25`
- :ref:`raar_2024:ujfp_2024_07_11`
- :ref:`raar_2024:ujfp_a_2024_06_04`
- :ref:`raar_2024:tsedek_ujfp_2024_05_23`
- :ref:`raar_2024:ujfp_caution_2024_05_22`
- :ref:`raar_2024:ujre_2024_02_21`
- :ref:`raar_2024:ujfp_2024_02_20_nega`
- :ref:`raar_2024:ujfp_2024_02_20`
- :ref:`jjr:jjr_2024_04_25`


Citée en 2023
=======================

- :ref:`bruttmann_ujfp_2023_12_18_2571`
- :ref:`bruttmann_ujfp_2023_12_18_4662`
- :ref:`bruttmann_ujfp_2023_12_18_5182`
- :ref:`bruttmann_ujfp_2023_12_18_5618`
- :ref:`bruttmann_ujfp_2023_12_18_5762`
- :ref:`jjr:ujfp_2023_10_24`
- :ref:`jjr:ujfp_2023_02_15`

Citée en 2019
=======================

- :ref:`jjr:jjr_antiracisme_2019_11_06`

Citée en 2015
=======================

- :ref:`jjr:jjr_ujfp_2015_04_27`

Membres de l'UJFP
===================

.. toctree::
   :maxdepth: 3

   pierre-stambul/pierre-stambul

Autres faits (à documenter)
===============================

- Dominique Vidal, l’un des militants pro palestiniens le plus engagé notamment
  pour la reconnaissance de l’épuration ethnique de 48 a quitté l’UJFP dont il
  faisait partie en les traitant d’antisémites.
- l’UJFP a participé à une réunion publique avec Michel Collon, un journaliste
  pro-russe et un conspirationniste bien connu.
  Ce dernier, traité d’antisémite par un non loin célèbre blogueur belge a
  été débouté lors du procès intenté contre le blogueur:
  Rappelant que “**le terme antisémite n’est pas réservé à ceux qui ont été
  condamnés pénalement pour de tels faits**", la Cour a estimé que Marcel Sel
  “pouvait parfaitement considérer que les prises de position de M. Collon,
  en dépit de ses dénégations, cachaient de l’antisémitisme et pouvaient
  entraîner de **dangereuses dérives**.
  La cour a énoncé que pour ce faire, “il disposait d’une base factuelle suffisante
  pour exprimer son opinion à ce sujet et **qu’il n’avait pas outrepassé les
  limites de sa liberté d’expression en accusant M. Collon - dans un style propre
  au caractère satirique de son blog - d’être antisémite** et de réveiller la
  théorie du grand complot juif internationaliste et impérialiste qui a fait
  les beaux jours du nazisme et ce quand bien même il ne bénéficierait pas
  d’une protection similaire à celle accordée aux journalistes".
