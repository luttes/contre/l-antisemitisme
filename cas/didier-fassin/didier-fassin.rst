.. index::
   pair: Cas ; Didier Fassin

.. _didier_fassin:

==============================================================
**Didier Fassin** (confusionnisme, mémoire sélective)
==============================================================


Cité en 2024
=============

- :ref:`conflitpal_2024:bruttmann_2024_01_18`


Cité en 2023
=============

- https://k-larevue.com/genocide-a-gaza-eva-illouz-repond-a-didier-fassin/
