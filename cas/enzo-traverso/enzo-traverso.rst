.. index::
   pair: Cas ; Enzo Traverso

.. _enzo_traverso:

======================================================================
**Enzo Traverso** (mensonges, mémoire sélective, double standard)
======================================================================


Cité en 2024
=============

- https://www.unioncommunistelibertaire.org/?Lire-Enzo-Traverso-Gaza-devant-l-histoire
  Encore un article bien écoeurant diffusé par l'UCL:
  "C'est une tragédie méthodiquement préparée par ceux qui voudraient aujourd'hui
  être vus comme les victimes"
- :ref:`conflitpal_2024:bruttmann_2024_01_18`
