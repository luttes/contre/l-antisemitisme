.. index::
   pair: Cas ; Libre Palestine
   ! Intersectionnel·le·s (indignation à géométrie variable)

.. _intersectionnelles:

===============================================================
**Intersectionnel·le·s** indignation à géométrie variable
===============================================================


En 2024
================

- :ref:`raar_2024:intersec_2024_06_06`
- :ref:`raar_2024:intersec_2024_05_21`
- :ref:`raar_2024:intersec_2024_03_29`
- :ref:`raar_2024:intersec_2024_01_28`

En 2023
================

- :ref:`raar_2023:intersec_2023_11_29`
- :ref:`raar_2023:intersec_2023_11_25`
- :ref:`intersec_2023_11_16`
