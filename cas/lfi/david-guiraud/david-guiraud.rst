.. index::
   pair: Cas ; David Guiraud
   pair: Antisémitisme ; David Guiraud
   pair: Déni ; David Guiraud
   ! David Guiraud (antisem)

.. _guiraud:
.. _david_guiraud:

=======================================================================================================
**David Guiraud** (député LFI, **se 'cultive' du côté du néo nazi Soral**)
=======================================================================================================

- https://fr.wikipedia.org/wiki/David_Guiraud_homme_politique


Cité en 2024
=============

- :ref:`raar_2024:guiraud_2024_06_11`
- :ref:`raar_2024:guiraud_soral_2024_05_23`


Cité en 2023
=============

- :ref:`raar_2023:bruttmann_guiraud_2023_12_18_1030`
- :ref:`raar_2023:bruttmann_guiraud_2023_12_18_1593`
- :ref:`raar_2023:bruttmann_guiraud_2023_12_18_2366`
- :ref:`raar_2023:bruttmann_guiraud_2023_12_18_3043`
