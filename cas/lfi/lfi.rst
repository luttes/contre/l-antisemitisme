.. index::
   pair: Cas ; LFI
   pair: Antisémitisme ; LFI

.. _lfi:

==============================================================
**Cas d'antisémitisme chez LFI**
==============================================================

2024
======

- :ref:`lfi_ipsos_diffusion_2024_11_21`
- :ref:`raar_2024:lfi_2024_07_16`
- :ref:`raar_2024:lfi_2024_07_10`
- :ref:`raar_2024:lfi_2024_06_04`
- :ref:`raar_2024:lfi_rafowicz_2024_03_22`

Membres de LFI
===================

.. toctree::
   :maxdepth: 3

   david-guiraud/david-guiraud
   jean-luc-melenchon/jean-luc-melenchon
   ludivine-bantigny/ludivine-bantigny
   mathilde-panot/mathilde-panot
   manuel-bompard/manuel-bompard
   rima-hassan/rima-hassan
   sophia-chikirou/sophia-chikirou
