.. index::
   pair: Cas ; Manuel Bompard
   pair: Antisémitisme ; Manuel Bompard
   pair: Déni ; Manuel Bompard

.. _bompard:

=======================================================================================================
**Manuel Bompard** ( coordinateur de La France insoumise, **déni total de d'antisémitisme chez LFI**)
=======================================================================================================


Cité en 2024
=============

- :ref:`raar_2024:bompard_2024_03_22`
