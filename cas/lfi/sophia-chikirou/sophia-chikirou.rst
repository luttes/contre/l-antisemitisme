.. index::
   pair: Cas ; Sophia Chikirou

.. _sophia_chikirou:

===============================================================================================
**Sophia Chikirou** (hommage indigne de Sophia Chikirou au numéro 1 du Hamas)
===============================================================================================


Citée en 2024
=============


- :ref:`raar_2024:golem_2024_08_02`
- :ref:`raar_2024:chikirou_2024_08_01`
