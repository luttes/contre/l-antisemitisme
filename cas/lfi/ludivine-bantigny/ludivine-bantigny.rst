.. index::
   pair: Cas ; Ludivine Bantigny
   ! Ludivine Bantigny

.. _ludivine_bantigny:

===========================================================================================================
**Ludivine Bantigny** (a écrit un article scandaleux de défense de LFI particulièrement malhonnête)
===========================================================================================================


TODO
======

- https://blogs.mediapart.fr/ludivine-bantigny/blog/170624/reponse-collective-une-infamie-sur-l-accusation-d-antisemitisme-portee-contre-lfi

En réalité, cet article interminable est en tout point scandaleux.
Il n’est pas du tout rigoureux, il assène ses conclusions qui disculpent
sans rien démontrer et, surtout, il ne prend pas en compte les critiques
précises d’associations telle que le RAAR.

En gros, Il s’agit de disculper Melenchon et cie, mais aussi de tuer dans
l’œuf tout débat sur l’antisémitisme à / de gauche.

L’autrice écrit en particulier que la LFI n’a rien à se reprocher dans la
nature de ses critiques contre Israël ou l’emploi des termes sioniste / antisioniste.

Elle démontre essentiellement que Mélenchon et sa clique ne sont pas antisémites
car… ils jurent qu’ils ne le sont pas! **C’est d’un niveau critique…**

Des réactions
----------------

::

    Le texte est incroyablement nul, sur le fond comme sur la forme.
    Franchement, quelle différence avec un texte d'apparatchik du PCF sur
    l'union soviétique pendant les années 50 ?



.. warning:: ce qui est particulièrement ignoble c'est qu'elle a écrit
   ça pendant la campagne législative le 17 juin 2024!



A voir
-------

::

    - https://x.com/samanthawhite__/status/1802715812123365879
    - https://x.com/JackyCho8/status/1802764276597244345
    - https://x.com/Golem21182346/status/1802742503264735642
    - https://x.com/MarylinMaeso/status/1802700416959889492
    - https://x.com/JuChanet/status/1802700968364044657
    - https://x.com/cohaerentiaT/status/1802692658579513710
    - https://x.com/treadstone78651/status/1802713842830487811
    - https://x.com/Oliouchka/status/1802697685201293607
    - https://x.com/josephhirsch5/status/1802750522220540019
    - https://x.com/MarylinMaeso/status/1802751992336400606
    - https://x.com/JackyCho8/status/1802770388549779881
    - https://x.com/BursteinVK/status/1802736397998006759
