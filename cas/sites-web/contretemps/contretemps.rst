.. index::
   pair: Site web  ; contretemps
   pair: Revue papier  ; contretemps
   ! Site web contretemps  (complotiste, mélenchonisme)

.. _site_contretemps:

================================================================================================================================================================================================
**Site web contretemps** (complotiste, néocampiste, mélenchonisme) contribution aux passerelles antisémites et néo-nazies **A ne pas confondre avec la revue contretemps version papier**
================================================================================================================================================================================================


.. warning::
   A ne pas confondre avec la revue contretemps **version papier** dirigée par  Francis Sitel
   https://www.syllepse.net/contretemps-_r_94_va_1.html


Description
================

- https://www.contretemps.eu/projet/  dirige par Ugo Palheta
- ne se donnent pas la peine de définir les différents sionismes
  pas plus qu'ils ne donnent une définition de l'antisionisme
- "Abou Léon", Judith Bernard, Bouteldja, etc.- se retrouvent aussi
  à la la Fabrique et sur Parole d'Honneur.


Articles confus 2025
===========================


.. _bouteldja_2025_02_15:

2025-02-15 Critique d'un article de Houria Bouteldja par Vincent Presumey
----------------------------------------------------------------------------------

- https://www.facebook.com/vincent.presumey/posts/pfbid022jmT4kdewxGqUSaE4pJ2v4Xq4ttZJtejHk6x3T7ST68gLb21VdHDCddpRxHBHz1gl

Je viens de lire les dernières pensées profondes de Houria Bouteldja, antisémite
racialiste que des Bisounours dangereux admirent en la prenant pour une
représentante des victimes du racisme, alors qu'elle est une parfaite
représentante de l'intelligentsia petite-bourgeoisie parasitant l'université
publique (ce qu'elle sait et assume parfaitement).

Résumé des idées phares de cet article : seule l'extrême-droite apporte du rêve,
il faut donc faire comme elle, ainsi que savait le faire Otto Strasser
(le nazisme de gauche tend à devenir la référence explicite).

Ensuite, un long gloubi-boulga "dialoguant" avec Lordon et Friot, et Kouvélakis,
pour aboutir à cette fantastique révélation : "la transcendance s'appelle France".

Mais le "patriotisme" ne suffit pas, il faut y ajouter l' "anti-impérialisme".

Parfait pour aller recruter les petits soldats de Trump/Musk/Poutine dans un
séminaire de l'Institut La Boétie, non ?

Au fait, **c'est sur le site de Contretemps, antichambre de ce type de dérives
néo-soraliennes**, bien entendu précédé d'un chapeau hypocrite expliquant que
cette soi-disant "revue de critique communiste" publie (comme elle l'a toujours fait)
cette **contribution aux passerelles antisémites et néo-nazies** parce que,
voyez- vous, il "questionne les rapports de la gauche et des classes populaires
à la nation, sans abandonner une perspective antiraciste et décoloniale",
et appelle au "débat" autour de la perspective de la "nouvelle France" de J.L. Mélenchon.

Pas plus que je ne mettrai ici un lien vers Soral, je ne mets de lien vers
Contretemps [le site Web] - l'un comme l'autre est facile à trouver.

.. note::  Qu'il n'y ait pas de malentendu : je ne mets pas de signe égal
   entre Contretemps et Soral, mais je me trouve dans la même obligation
   que si je parlais de Soral, de ne pas mettre de lien vers Contretemps
   pour cet article.
   Ceci dit Contretemps n'est pas rouge-brun : Contretemps est juste néocampiste
   en version intello, ce qui fournit un terreau tout à fait suffisant à la
   prose bouteljesque.

Articles confus 2024
===========================

- Abou Leon (le neveu de Doda Houria) a commis un nouvel article contre
  notre camarade Jonas Pardo et les associations Golem, JJR et le RAAR
  https://www.contretemps.eu/7-octobre-sionisme-gauche-antisemitisme-israel/
