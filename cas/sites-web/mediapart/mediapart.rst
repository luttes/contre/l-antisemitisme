.. index::
   pair: Cas ; Mediapart
   ! Mediapart  (parfois censure, confusionnisme, mélenchonisme)

.. _mediapart:

============================================================================
**Mediapart** parfois censure, confusionnisme, mélenchonisme
============================================================================


.. note:: Témoignage du 2024-10-21
   Il y une dizaine d’années, la rédaction employait des journalistes venus
   de rédactions et d’horizons différents. Les contenus étaient plus variés,
   informatifs et… intelligents.
   Quant au Club des abonnés, même phénomène. La nouvelle ligne est résolument
   gauchiste et très Mélenchon-compatible sur tous les sujets.
   Les articles de blog mis en une se ressemblent tous, font dans le registre
   de l’indignation et de la surenchère verbale permanente.
   Un billet qui va à l’encontre de cette doxa sera assailli de trolls
   mélenchonistes. C'est insupportable et sans intérêt.
   De nombreux abonnés qui y publiaient ont cessé de le faire


Citée en 2024
================

- :ref:`mediapart_2024_10_21_meurice`
- :ref:`raar_2024:mediapart_2024_05_24`
- :ref:`raar_2024:mediapart_2024_02_09`


Membres de mediapart
=========================

.. toctree::
   :maxdepth: 3

   edwy-plenel/edwy-plenel
