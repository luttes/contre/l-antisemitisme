.. index::
   pair: Cas ; Gaza et l'emploi du mot génocide

.. _genocide_gaza:

===========================================================
**Vouloir employer le mot 'génocide' dès le 8 octobre**
===========================================================

- https://fr.wikipedia.org/wiki/Discussion:Risque_de_g%C3%A9nocide_%C3%A0_Gaza_depuis_2023#c-Marc-AntoineV-20241008202300-JMGuyon-20241008124200

.. note:: ce qui se passe à Gaza est tout à fait horrible.
   On parle de crime de guerre et de crimes contre l'humanité
