

==================================================================
2022-06-05 Signature de la chartede lutte contre l'antisémitisme
==================================================================

- https://www.charte-lutte-antisemitisme.fr/
- https://www.charte-lutte-antisemitisme.fr/les-signataires

Salomé #circo3801
=====================

- https://fediverse.com/pvergain/status/1533486556421423104?s=20&t=3NvgmiAzd8btaVhNtSvxyw

Cyrielle Lisa #circo3802
==============================

- https://fediverse.com/pvergain/status/1533481082208804865?s=20&t=3NvgmiAzd8btaVhNtSvxyw


Elisa Martin #circo3803
============================

- https://fediverse.com/pvergain/status/1533488736473911297?s=20&t=V4CPEYfXgyFsMhNbAdkZ5A


Jérémie  #circo3805
=======================

Bonjour Jérémie et bravo pour votre campagne ! J'ai appris vendredi 3 juin 2022
qu'une charte de lutte contre l'antisémitisme était proposée aux 577
candidat·e·s NUPES. Allez vous la signer ?
(Cette charte est à l'initiative du groupe GT EELV)


Le mardi 7 juin 2022 ce sera le triste 80e anniversaire de l'imposition du port de l'étoile jaune pour les juifs (7 juin 1942, https://raar.info/2022/05/etoile-jaune-rassemblement/)
J'imagine que vous êtes plus que débordée mais si vous pouviez signer la charte de lutte contre l'antisémitisme
ce serait apprécié (la camarade Cyrielle Chatelain de la circo3802 l'a fait aujourd'hui voir
https://fediverse.com/Cyrielle_Lisa/status/1533576076449955840?s=20&t=WeBsE7Nkcv9ZBiMh7I7n7ge84J9EWy_PI1jPTgcu8txw
https://fediverse.com/Cyrielle_Lisa/status/1533576076449955840?s=20&t=WeBsE7Nkcv9ZBiMh7I7n7g


La charte comporte 10 points, vous n'êtes pas obligé de tout cocher. Cela permettra de voir
ce qui bloque tant à gauche en ce qui concerne la lutte contre l'antisémitisme.


Quelle que soit votre réponse ou non réponse je voterai NUPES et comme je suis sur
#circo3803 je voterai donc Elisa Martin qui elle aussi n'a pas encore répondu à ma demande
de signature.

Pour signer:

    - https://www.charte-lutte-antisemitisme.fr/les-signataires


Autres liens d'informations
