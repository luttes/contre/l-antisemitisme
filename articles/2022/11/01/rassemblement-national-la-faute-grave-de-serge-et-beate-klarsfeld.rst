.. index::
   pair: Serge et Beate Klarsfeld; Rassemblement national: la faute grave de Serge et Beate Klarsfeld

.. _klasrfeld_2022-11_01:

=====================================================================================
2022-11-01 **Rassemblement national : la faute grave de Serge et Beate Klarsfeld**
=====================================================================================

- https://raar.info/2022/11/rn-aliot-klarsfeld/
- https://www.mediapart.fr/journal/politique/201022/la-strategie-des-klarsfeld-face-au-rn-divise-les-amis-du-crif


En choisissant de se faire décorer le 13 octobre 2022 par Louis Aliot, maire RN
de Perpignan et candidat à la présidence de ce parti, mais aussi ancien
directeur de cabinet de Jean-Marie Le Pen, **Serge et Beate Klarsfeld tournent
le dos à leur combat contre l’extrême-droite et sèment la confusion**.

Serge Klarsfeld aggrave encore la portée de son geste en apportant son
soutien à Aliot dans la compétition interne au RN face à Jordan Bardella.

Son prestige immense et mérité favorise ainsi le discours de «
normalisation » de Louis Aliot et donc de Marine Le Pen, dont le maire
de Perpignan se réclame constamment et dont il promeut la candidature
pour la présidentielle de 2027.

Or la cheffe du RN vient encore de démontrer à propos de la tragique
affaire Lola, qu’elle n’a absolument pas changé d’orientation.

L’agitation raciste reste son principal axe de propagande, comme Serge
et Beate Klarsfeld eux-mêmes l’analysaient encore récemment.

En effet, lors de l’élection présidentielle d’avril 2022 Serge et
Beate Klarsfeld signaient une déclaration s’opposant frontalement à
Marine Le Pen.  Beate et Serge Klarsfeld décorés par Louis Aliot

Cette déclaration proclamait fort justement « Non à Le Pen, fille du
racisme et de l’antisémitisme» et rappelait la continuité de son
parti ».

Elle se concluait ainsi : « Si elle l’emportait, elle appliquerait un
programme d’exclusion, sans rapport avec les attentes sociales légitimes
que nos concitoyens expriment et qu’elle prétend comprendre. Ne nous
laissons pas tromper : elle n’a pas changé ! »

Serge Klarsfeld s’était également engagés dans le combat contre
Eric Zemmour, à travers de nombreuses tribunes et déclarations liant
la défense de Pétain à des mesures extrêmes contre les populations
issues de l’immigration.

Afin de justifier son brusque tournant, Klarsfeld cherche à démontrer
que Marine Le Pen, cheffe incontestée du RN, a elle aussi évolué
positivement. Ainsi déclare-t-il dans un entretien avec Libération :
« Marine Le Pen a pris position l’an dernier et cette année encore
sur le Vél d’Hiv. J’ai dit publiquement que c’était “un pas en
avant” ».

Or Marine Le Pen a au contraire déclaré lors de la campagne
présidentielle de 2017 « Je pense que la France n’est pas responsable
du Vél’ d’Hiv » ajoutant :

« … La France a été malmenée dans les esprits depuis des années. En
réalité, on a appris à nos enfants qu’ils avaient toutes les raisons
de la critiquer. Je veux qu’ils soient à nouveau fiers d’être
Français. À condition que la France rompe avec le sentiment que nos
dirigeants ont mis en place une politique qui vise à avantager les autres
plutôt que les nôtres ».

Ainsi c’est le souvenir du nazisme et de la collaboration qui
expliquerait la difficulté à faire passer l’orientation de la «
préférence nationale ». Pour elle comme pour tous les dirigeants
fascistes, antisémitisme, négationnisme et racisme anti-immigrés sont
intimement liés.

Aliot lui-même défend fermement la « préférence nationale » qui
constitue le noyau dur de l’idéologie xénophobe du RN. Quant au «
Grand remplacement » il n’en rejette pas le principe mais considère
dans ses déclarations à l’Opinion qu’il donne une mauvaise image
de l’extrême-droite car il « inquiète plus qu’il ne rassure et
entraîne inexorablement à la défaite, voire à la violence aveugle ».

On ne peut donc que condamner le blanc-seing délivré par Serge et Beate
Klarsfeld à Aliot dont ils disent « il n’y a pas grand-chose à y
redire du point de vue républicain. C’est courageux de sa part. »

Nous n’oublions pas que Aliot a été directeur de cabinet de Jean
Marie Le Pen de janvier 1999 à septembre 2000 donc après le « détail
des chambres à gaz » de 1987 et également coordinateur de la campagne
présidentielle de ce dernier en 2002. D’ailleurs en août 2015, en
tant que membre du comité exécutif du parti, il se prononça contre
l’exclusion de Jean-Marie Le Pen du Front national. Il indiquait alors
qu’il se « voyait mal » écarter celui avec qui il était « entré
en politique ».

Très récemment, le 22 septembre 2022, Aliot a fait voter par le Conseil
municipal la dénomination d’une esplanade de Perpignan au nom du
député Front National des Pyrénées-Orientales et ancien cadre de
l’OAS, organisation terroriste responsable de milliers de morts par
ses attentats en métropole et dans les territoires algériens.

**Les raisons du tournant de Serge et Beate Klarsfeld, lequel constitue
un coup de poignard porté à la lutte contre l’antisémitisme et le
racisme, semblent relever d’un sentiment de panique à la pensée d’une
victoire future du RN lors de la prochaine élection présidentielle
et face à la présence dès maintenant de 89 députés de ce parti.**

Serge et Beate Klarsfeld estiment dès lors nécessaire d’intervenir
au sein même du RN et d’apporter un soutien explicite au protagoniste
qui leur semble le moins extrémiste.

**Le RAAR appelle à poursuivre le combat contre le RN et ses idées, dans
la continuité du combat qu’ont mené Serge et Beate Klarsfeld, pour
le jugement des responsables de la Shoah et des autres génocides, contre
l’impunité des antisémites et autres racistes quels qu’ils soient,
contre l’extrême-droite sous toutes ses versions, y compris celles
les plus attachées à dissimuler tactiquement leur véritable nature.**

**Il n’y a pas de pactisation possible avec une quelconque aile ou fraction
du RN.**

Ceux qui prétendent défendre des positions « républicaines » ne
peuvent le prouver qu’en quittant ce parti, qui porte la continuité des
fascistes et anciens SS qui l’ont fondé il y a 50 ans, ainsi que celle de
la torture en Algérie et celle de l’antisémitisme acharné symbolisé
par les chambres à gaz, « point de détail » de la 2e guerre mondiale.

**C’est pourquoi nous rejetons la tentation gravissime de Serge et Beate
Klarsfeld et les appelons à revenir au combat contre l’extrême-droite
raciste et antisémite, toujours aussi malfaisante.**
