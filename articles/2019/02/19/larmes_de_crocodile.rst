Larmes de crocodiles
======================

.. seealso:

   - https://www.facebook.com/permalink.php?story_fbid=2090544167682018&id=795943817142066&__xts__%5B0%5D=68.ARAleb9pDZ17SUxt8Xw-8BGzcmrS8BDDChwyyaor28DVcCetPmryzuo8kt8pTOdsGtpnaygBeN0DNSrDgiJtTlBfnrGxGWliObssPl53c3wtjdtD_RN7lsfav3FgYkfW5KS8idLqW5tMgVltye4k-TtOEvyOYgcOy1bY_MUD5FHNF9vSLSL6CvBbY1v0XkUDhMc5VUDloiE275q_qmboy4wO2MmxCv_nKdKU9GaVRS_vFLOx0AMZMpW_CZ3XM6kMLsIGxbjv8nfo3KxWYBMLO40_M7-y0449TGafUSA-21JkwinXXlnB4GJajJEeH4dhLn36pNfGovTd9UCih1eUZkRhWg&__tn__=K-R


Depuis son origine, l’antisémitisme fonctionne par vagues. Après des
périodes d’apparent reflux, il revient. Il surprend ainsi régulièrement
celles et ceux qui, expérimentant un répis dans les persécutions, en
viennent à baisser la garde. L’histoire de notre minorité est ainsi
pleine de périodes de ce genre, où, après le ressac, revient la vague.

La flambée actuelle n’est pas nouvelle, pas plus que l’antisémitisme
en France. Ses formes actuelles ne sont pas nouvelles : les juives et les
juifs sont désignés une nouvelle fois comme boucs émissaires des problèmes
sociaux.

L’antisémitisme est un outil de maintien de l’ordre capitaliste,
un outil de maintien de l’ordre colonial.
Il est utilisé pour protéger la bourgeoisie et l’ordre colonial.
L’antisémitisme, ancré profondément dans le roman national et la culture
française, est réactivé par les moyens de communications modernes que sont
les réseaux sociaux.

**La moindre vidéo antisémite cumule des centaines de milliers de vue, quand
dans le même temps les analyses matérialistes et révolutionnaires du capitalisme
restent confidentielles**.

Mais le pouvoir politique n’est pas en reste: de la réhabilitation de Pétain à
celle de Mauras, il s’agit avant tout d’absoudre la responsabilité française
dans l’antisémitisme, et de le présenter comme un **corps étranger**, importé.

Or s’il y a bien une idéologie qui a été forgée en France et en Europe, puis
exporté mondialement, c’est bien l’antisémitisme, de Vacher de Lapouge
en passant par Gobineau, de Drumont à Maurras.
Le parti colonial, par le biais de Drumont, s’est ainsi fait fort de diffuser
le poison antisémite au Maghreb à l’époque coloniale, pour mieux protéger l’ordre
raciste qu’il a mis en œuvre. Aujourd’hui, il cherche à effacer les
traces, et d’évacuer sa responsabilité dans cette histoire là aussi.

Il utilise la lutte contre l’antisémitisme pour diffuser un poison raciste,
ou pour **évacuer la question sociale**, sans avoir l’air d’y toucher.

Alors qu’est-ce que-ce que le positionnement de ces partis institutionnels qui
s’émeuvent de l’antisémitisme, sinon des larmes de crocodiles ?

Qu'est ce que ce positionnement d'organisateurs qui considèrent que Marine Le Pen
et son parti, l'un des vecteurs historiques de l'antisémitisme militant en
France, aurait sa place dans un tel rassemblement, si ce n'est une masquarade.

Larmes de crocodile aussi, que ce rassemblement alternatif organisé par l'UJFP
et le PIR qui depuis plus de 15 ans au mieux minimisent l’antisémitisme, au
pire ont recyclé le vieux thème du **privilège juif** sous l’appelation
de **philosémitisme d’Etat**.
Qui pour certains comme le PIR ont d’abord participé à la défense de Dieudonné
et ses dérives, avant de le voir comme un concurrent puis de le condamner
lorsque ses liens avec les fascistes sont devenus trop explicites.
Qui sont incapables, comme le gouvernement, ou comme les courants sionistes
d’aborder la question de l’antisémitisme pour elle-même, comme un combat
antiraciste en soi et non un avatar de la situation en Palestine .

Larmes de crocodile, cette tendance à ne parler d’antisémitisme que pour
dénoncer l’instrumentalisation que certains en font.

Pourtant cette instrumentalisation n’est possible que pour une unique raison:
**l’abandon massif de la lutte contre l’antisémitisme par une large part
du mouvement progressiste et révolutionnaire depuis 15 ans**.

L’indigence de l’analyse qui en est faite, l’absence de formation réelle des
militantes et militants sur le sujet.
Quelles initiatives ont donc été prises, ces quinze dernières années ?

Pour celles et ceux qui ont traversé les années qui vont de l’assassinat
d’Ilan Halimi, en passant par les attentats d’Ozar Hatorah et de l’hyper
casher ou l’assassinat de Mireille Knoll, nous nous souvenons : minimisation,
banalisation, indifférence…

Mais qui donc a abandonné le terrain de la lutte contre l’antisémitisme au
gouvernement, à la droite, aux différents courants sionistes, si ce n’est
les mêmes qui aujourd’hui viennent s’émouvoir de leur instrumentalisation ?

**Combattre l’instrumentalisation, c’est occuper le terrain de la lutte
contre l’antisémitisme. Au quotidien.**

Pas seulement quand il s’agit de dénoncer la mauvaise foi du gouvernement.




