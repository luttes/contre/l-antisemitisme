
.. index::
   pair: Vacarme ; 2019-02-19


.. _non_sujet_antisemitisme_gauche:

=====================================================================
Mardi 19 février 2019 : Le non-sujet de l’antisémitisme à gauche
=====================================================================

- https://www.cairn.info/revue-vacarme-2019-1-page-36.htm
- https://www.cairn.info/revue-vacarme-2019-1.htm

Vacarme 86 / Les fractures ouvertes de la gauche Le non-sujet de
l’antisémitisme à gauche

par Camilla Brenni, Memphis Krickeberg, Léa Nicolas-Teboul & Zacharias
Zoubir

L’antisémitisme ne fait pas à proprement parler partie des fractures
de la gauche radicale. Les positionnements sur cette question varient peu
d’une organisation de gauche à l’autre. Au contraire, celles-ci
ont souvent en partage un désinvestissement de ce thème perçu
comme secondaire voire négligeable, relativement à d’autres formes
de racialisation. Plutôt qu’une fracture, c’est donc davantage
un silence qu’il s’agit d’interroger ici, en plaidant pour une
critique radicale de l’antisémitisme à l’heure où celui-ci a des
effets pratiques et idéologiques jusque dans les formes que prennent
certaines luttes sociales [1].

Pour nombre d’organisations de la gauche radicale et de l’antiracisme
français, l’antisémitisme contemporain n’est pas une question. Plus
précisément, les pratiques et les discours qui ciblent des (supposés)
juifs tenus pour responsables de maux politiques, économiques ou sociaux
n’attirent souvent l’attention que dans la mesure où cet antisémitisme
est instrumentalisé par des politiciens ou intellectuels réactionnaires.

Il ne s’agit pas de nier l’existence d’une telle
instrumentalisation. On assiste, depuis une vingtaine d’années, à
la multiplication de discours sur une « nouvelle judéophobie » qui
construisent l’image d’une « extrême-gauche » ou de « jeunes
de quartiers » antisémites parce qu’antisionistes ou musulmans
[2]. Pourtant, face à cette instrumentalisation, les organisations de la
gauche radicale ou de l’antiracisme politique [3] ne s’enquièrent que
rarement de la réalité de l’antisémitisme dans la période actuelle
et faillissent souvent à développer leur propre critique des actes et
de la symbolique antisémites. Ces organisations forgent plutôt diverses
justifications d’une position de principe : l’antisémitisme ne serait
une question que pour les défenseurs et les idéologues de l’ordre
existant. L’antisémitisme contemporain devient ainsi une question que
l’on ne doit tout simplement pas poser en tant que révolutionnaire
et/ou antiraciste [4]. À l’heure actuelle, l’antisémitisme dans la
gauche française est moins une ligne de fracture délimitant différentes
positions que l’objet d’un silence. C’est ce silence que nous allons
tenter de décrire et décrypter ici.

Parler de « la gauche » pour désigner tout un spectre politique
extrêmement hétérogène et éclaté, allant de la France Insoumise
aux autonomes en passant par l’antiracisme politique, les mouvements
écologistes ou les différentes formations trotskystes ou maoïstes,
peut paraître essentialisant, voire dénué de sens. Or, justement, le
silence face à l’antisémitisme semble constituer une des rares choses
partagées par la grande majorité des courants et organisations de la
gauche française. Ériger l’antisémitisme en fracture constituerait
ainsi une avancée et signifierait, a minima, d’en faire un point de
débat et un enjeu politique.  L’antisémitisme et les silences de
la gauche

Le peu de statistiques dont nous disposons font apparaître que les
discours et les actes antisémites ont connu une recrudescence au début
des années 2000. Depuis ce tournant, leur nombre annuel enregistré
est supérieur à 200 et a été supérieur à 800 à cinq reprises
[5]. Certains lient le tournant du début des années 2000 à la Seconde
Intifada, tandis que d’autres avancent qu’une part conséquente des
actes antisémites en France proviendraient des soi-disant « milieux
arabo-musulmans » [6]. Il est à noter qu’ils se fondent souvent sur
des catégorisations vagues et des données empiriques partielles. On
peut cependant constater que le 11-Septembre a inspiré une panoplie
d’élucubrations conspirationnistes sur le rôle qu’une organisation
juive mondiale jouerait dans l’orchestration du cours du monde [7],
jusque dans la crise des subprimes de 2008 [8].

Plus frappante est la multiplication d’attaques antisémites physiques
visant des personnes, l’année 2012 étant la première où ce genre
d’actes ont dépassé en nombre les actes touchant des lieux associés à
la judéité (synagogues, cimetières juifs, etc.) [9]. Face aux meurtres
ciblant des juifs du fait de leur judéité réelle ou supposée, les
organisations qui revendiquent une identité de révolutionnaires et/ou
d’antiracistes politiques ont le plus souvent adopté deux types de
positions. Il y a d’abord l’attitude sceptique qui consiste à douter de
la dimension antisémite de ces meurtres. Albert Herszkowicz, fondateur de
Mémorial 98, notait déjà en 2009 que des organisations comme la Ligue
Communiste Révolutionnaire (LCR), l’Union Juive Pour la Paix (UJFP)
et le Mouvement contre le Racisme et pour l’Amitié entre les Peuples
(MRAP) avaient refusé de manifester en solidarité avec Ilan Halimi sous
prétexte, notamment, que la police n’aurait pas établi le caractère
antisémite du meurtre. « [É]trange prétexte », écrivait-il avec
justesse, « s’agissant d’organisations habituellement peu enclines
à s’aligner sur les analyses policières » [10]. Toutefois, quand ce
caractère antisémite est indéniable — même pour la police ! —
au vu du discours explicite de l’auteur du meurtre, la position de
la relativisation peut prendre le relais. Celle-ci revient à minimiser,
voire à faire disparaître la dimension antisémite en la contrebalançant
avec un phénomène jugé plus important. On se réfère alors surtout à
la position minoritaire de l’assassin (Mohamed Merah et l’école juive
toulousaine [11]) ou encore aux dispositifs autoritaires et islamophobes de
l’antiterrorisme (Coulibaly et l’Hyper Cacher [12]). Rares sont donc les
propositions d’analyse de ce qui est : des juifs tués parce que juifs.

Les dénégations et le manque de réactivité face aux meurtres
antisémites ne sont que l’expression la plus manifeste d’un profond
silence de la gauche radicale. Il existe de fait un décalage frappant
avec le niveau de thématisation de l’antisémitisme dans le débat
public et les médias mainstream où s’enchaînent polémiques autour de
faits divers antisémites et tribunes contre le « nouvel antisémitisme
». Au mieux, l’antisémitisme est inséré dans le cadre de la lutte
contre les partis et groupements de l’extrême-droite et la critique
de leurs idéologies. L’antiracisme politique a pourtant su montrer,
contre l’antiracisme institutionnel et moral, que les représentations
racistes sont produites en premier lieu par la profonde racialisation de
la société française. Celle-ci a permis aux partis d’extrême-droite
d’émerger et de trouver une audience. Ce type de raisonnement
sur les mécanismes structurels du racisme semble ne pas valoir pour
l’antisémitisme. La gauche radicale reste silencieuse devant les deux
facettes principales d’un antisémitisme qui dépasse largement le
seul public de l’extrême-droite traditionnelle : l’antisémitisme
conspirationniste moderne liant les juifs au pouvoir et à l’argent et
les éléments d’antisémitisme qui peuvent être reformulés dans des
invectives contre Israël.

Le renforcement contemporain d’un antisémitisme classique associant les
juifs au pouvoir et à l’argent a pour trame de fond l’émergence
d’un conspirationnisme de masse à partir de la seconde moitié
des années 1990. La forme de pensée conspirationniste a deux effets
principaux [13]. D’une part, elle offre une vision du monde évacuant
la complexité, voire le caractère impénétrable d’une société
organisée autour de l’impératif de valorisation capitaliste et de
ce qui s’y associe : formes sociales (marchandise, État, droit …),
rapports de domination (classe, genre, race), stratégies d’hégémonie,
idéologies, formes de gouvernement, rapports d’exploitation ... Cette
vision du monde se focalise sur des petits groupes maléfiques œuvrant
dans l’ombre. D’autre part, cette conception paranoïaque d’un monde
régi par des forces occultes et terrifiantes fait apparaître la réaction
contre les groupes incriminés comme un mécanisme d’auto-défense où
une « communauté » fantasmée (le peuple, la race blanche, parfois la
Oumma) se dresse face aux conspirateurs.

Le nouvel essor du conspirationnisme constitue un phénomène de masse
prenant de court la gauche. Il s’explique sans doute en partie par les
bouleversements sociaux générés par la restructuration néolibérale
du capitalisme initiée dans les années 1970 et accélérée par la
crise de 2008 et la fin des grands récits de la modernité, notamment
l’affrontement idéologique entre libéralisme occidental et stalinisme
soviétique qui avait structuré la guerre froide. Ensuite, l’émergence
et la popularisation rapide d’Internet ont permis une diffusion massive
de discours « alternatifs » bien au delà de l’extrême-droite ou
des communautés conspirationnistes « geek » qui avaient émergé à
partir des années 1950 et 1960, notamment autour des spéculations sur
l’existence des extraterrestres. Une enquête d’opinion menée en
2017 par la fondation Jean Jaurès et Conspiracy Watch fait apparaître
le complotisme comme un phénomène majeur, 79% des sondés croyant à
au moins une théorie du complot [14].

Si toute les théories du complot ne sont pas fondées sur
l’antisémitisme, un grand nombre d’entre elles se laissent facilement
décliner dans un sens antisémite en raison des représentations qu’elles
charrient et de leurs effets sociaux. Par-delà la malléabilité et
l’hétérogénéité des figures de la conspiration, celle du juif
semble invariante depuis le XIXe siècle dans son rôle de surface de
projection permettant de concrétiser et de personnifier les rapports
sociaux capitalistes et l’idée même de domination et d’injustice. En
outre, l’existence d’autres figures du complot dans un même récit
n’évince pas forcément la « juiverie mondiale ». Derrière les
premières couches apparentes de la conspiration se trouvent des strates
plus profondes impliquant généralement les juifs, incarnation du pouvoir
maléfique ultime. La « juiverie mondiale » se trouve alors au sommet de
la pyramide des conspirations, à l’œuvre derrière « la finance »,
les machinations de la CIA ou l’orchestration du « grand remplacement ».

Certaines figures publiques juives cristallisent ainsi le ressentiment
et la paranoïa antisémite. À travers ses activités philanthropiques,
le milliardaire juif américain d’origine hongroise Soros oeuvrerait
à la déstabilisation des sociétés et des nations au profit de la
domination juive mondiale. Selon les élucubrations antisémites, Bernard
Henri-Lévy serait l’un des principaux instigateurs des interventions
militaires françaises et le dîner du CRIF (Conseil Représentatif des
Institutions Juives de France) le lieu où les hommes politiques prêtent
allégeance à leurs maîtres juifs.

Cette structure fantasmagorique se décline à travers tout un ensemble
de stéréotypes sur les juifs qui se manifestent au quotidien. Selon un
sondage CNN réalisé en France et en Europe, entre 24 et 28% des personnes
interrogées estiment que « la communauté juive a trop d’influence
à travers le monde » dans la sphère de la « finance et des affaires
» et 21% partagent cette idée au sujet du champ politique et des
médias [15]. Certes, les stéréotypes sur les juifs ne se traduisent
pas forcément dans des formes d’hostilité, les juifs étant souvent
loués pour leur supposé « sens des affaires » et leur « solidarité
communautaire ». Toutefois, le contexte de défiance à l’égard des
« élites » et de dégradation des conditions matérielles d’une
partie de la population marqué par une agitation populiste peut faire
de ces stéréotypes les mobiles d’agressions verbales ou physiques
dirigés contre des (supposés) juifs. Celles-ci apparaissent, pour les
antisémites s’estimant victimes de la domination juive, comme une
forme de rébellion [16].

La gauche, pourtant toujours prête à dénoncer le péril fasciste, ne
s’alarme pas particulièrement de cette propension au conspirationnisme
et à sa déclinaison antisémite, qui constituent pourtant le terreau
pour la popularisation d’idéologies fascisantes. Elle pense encore
largement l’idéologie comme un ensemble de mécanismes procédant
uniquement « par le haut » : manipulation, diffusion de visions du monde
(néo)libérales et/ou réactionnaires, production de subjectivités
par les appareils idéologiques d’État (école, université …)
et les médias dans l’intérêt des classes dominantes. Si l’État
diffuse une partie des récits conspirationnistes, notamment la vision
raciste et sécuritaire du « péril islamiste », figure réactualisée
du vieil « ennemi intérieur », une grande partie du conspirationnisme
est produit « par le bas ». La paranoïa conspirationniste se situe
souvent au niveau des mécanismes de défense psychologiques spontanés des
individus. Récits et figures fantasmagoriques émergent d’une myriade
diffuse de micro-médias complotistes (blogs, sites, chaines YouTube, pages
Facebook, comptes Instagram et fediverse ...) dont les contenus circulent
massivement sur les réseaux sociaux. En ce sens, le conspirationnisme est
devenu aujourd’hui une idéologie mainstream et « dominante ». Ce ne
sont pas l’État ni les médias du « grand capital » qui produisent et
diffusent l’antisémitisme en France mais un ensemble d’entrepreneurs
antisémites jouant sur des idées antisémites répandues et sur le
rejet populaire spontané du « système » et de « ceux d’en haut ».

Ainsi, l’analyse de la spécificité et de l’autonomie relative
de ces phénomènes est le plus souvent éludée à gauche. Évoquer
l’antisémitisme populaire reviendrait pour une grande partie de
la gauche à faire le jeu des discours dominants discréditant les «
dominés ». Le portrait du prolétariat post-colonial ou plus récemment
des prolétaires blancs des « périphéries » en barbares antisémites
doit, par contraste en quelque sorte, légitimer les projets hégémoniques
des classes dominantes drapés dans le langage de la défense de la
civilisation. Quand il traite du problème de l’antisémitisme,
le collectif anti-impérialiste Quartiers Libres se concentre sur la
recomposition d’une partie de l’extrême-droite autour de Soral et
Dieudonné. Ne retient l’attention que la manière dont l’accusation
d’antisémitisme se « surajouterait » aux représentations racistes et
islamophobes qui nourrissent les processus de relégation et de contrôle
des prolétaires racisés [17].

Corrélat direct, l’accusation d’antisémitisme ne viserait
qu’à délégitimer les critiques du capitalisme et les tentatives
d’auto-organisation des subalternes. Cette position a été portée
notamment par Le Monde diplomatique et Frédéric Lordon : « cet ordre
[...] en crise profonde […], vide d’arguments, […] ne trouve plus à
opposer que des disqualifications. Comme un premier mouvement de panique,
‘‘l’antisémitisme’’ a été l’une des plus tôt jetées à
la tête de toute critique du capitalisme ou des médias » [18].

De sorte que le conspirationnisme et par extension l’antisémitisme
seraient, pour une large partie de la gauche, le résultat malheureux
de la propagande idéologique de l’État et des médias néolibéraux
au service des élites qui « déposséderaient » les dominés de leur
capacité à penser leur situation [19].

Cette minimisation peut déboucher sur une position particulièrement nocive
: derrière l’antisémitisme se cacherait une forme de proto-critique
sociale et une aspiration à la résistance légitime. La critique du
capitalisme en termes d’« élites » dominant « le peuple » ou tout
équivalent, influente dans de nombreuses mobilisations (Occupy, Nuit
debout, forums et mouvements sociaux ...) et courants politiques [20],
reprend parfois à son compte une certaine concrétisation manichéenne
des rapports sociaux. Il faudrait se placer du côté des « dominés »,
vus comme des sujets révolutionnaires potentiels – qu’importe ce que
ces derniers font et pensent. Qu’on confonde ressentiment et propension
à la lutte, on excusera les manifestations d’antisémitisme populaire
par des considérations pseudo-sociologiques et politiques. On y discernera
même des aspects positifs. « Derrière l’hostilité envers les juifs,
il y a la critique de la pyramide raciale, de l’État Nation et de
l’impérialisme […] derrière chacune de nos régressions, il y a
une dimension révolutionnaire [21] ». L’analyse d’Houria Bouteldja
résume parfaitement, ici dans le langage de la décolonialité, ce type
de positionnement d’une gauche dont elle essaie pourtant constamment
de se distinguer.

L’aspect faussement contestataire et libératoire de l’antisémitisme
trouve une forme d’expression particulièrement claire dans l’entretien
d’un ressentiment à l’égard d’Israël reposant sur une vision
totalement fantasmée d’une toute-puissance de l’État juif. Celle-ci
dépasse le cadre de la condamnation légitime de la politique israélienne
et de ses exactions que l’on peut trouver dans les cercles de gauche. La
critique d’Israël telle qu’elle s’exprime au quotidien, dans les
lycées, les universités, sur YouTube et les réseaux sociaux, dans les
discussions de comptoir, la musique populaire, etc., est généralement
largement déconnectée d’une connaissance des enjeux historiques
précis de la question. Un sondage Ifop de 2013 a ainsi montré que
plus de 60 % de la population française partage une image mauvaise ou
très mauvaise d’Israël alors que 72 % a une connaissance mauvaise ou
très mauvaise de l’État juif et son histoire [22]. À rebours de la
perception bienveillante d’Israël de l’État français et des organes
médiatiques dominants, un pseudo-savoir sur Israël mêlant fake news,
pathos et conspirationnisme est partagée par une partie importante de
la population française.

Ce cadre fournit les éléments de langage, les récits et les images
à travers lesquels s’exprime une partie de l’antisémitisme
contemporain et alimente diverses formes de conspirationnisme. Israël
serait l’incarnation même de l’injustice sur Terre, de la maladie
nationaliste et la concrétisation de la violence souveraine. L’État
juif devient le principal, voire l’unique, responsable des désordres
et malheurs du monde – bref, « le juif des nations », pour reprendre
la formule de Poliakov [23]. Une géopolitique de comptoir recycle ainsi
des clichés antisémites éculés : Israël empoisonnerait des puits
palestiniens ; Tsahal s’adonnerait au trafic d’organes d’enfants
palestiniens.

La place centrale qu’occupe le conflit israélo-palestinien à gauche et
dans les luttes d’une partie des populations issues de l’immigration
post-coloniale n’est pas problématique en soi. Elle s’explique
d’abord par un faisceau de raisons historiques. La France a participé
à la création de l’État d’Israël (avec notamment comme argument la
nécessité de combattre la ligue arabe et donc la lutte des Algériens)
; de nombreux Maghrébins ont voulu combattre en Palestine en 1948 ;
les comités Palestine, créés en France en 1967 par des travailleurs
et étudiants arabes, devinrent ensuite les bases des mouvements de
luttes des ouvriers spécialisés dans les années 1970. Par ailleurs,
il y a bien une tendance univoque de la part des autorités israéliennes
et de leurs alliés et relais médiatiques à entretenir l’amalgame
entre antisémitisme et critique d’Israël afin de délégitimer cette
dernière.

À rebours des lectures en termes de « nouvel antisémitisme » ainsi
que certaines analyses marxistes antideutsch de l’antisémitisme et
du sionisme [24], notre propos n’est donc pas ici d’affirmer que
l’antisionisme ou l’anti-impérialisme de la gauche produiraient
d’eux-même, par leur seule structure argumentative et les
représentations qu’ils charrient, de l’antisémitisme. Si
l’antisionisme et l’anti-impérialisme peuvent, sous certaines
conditions, reprendre, consciemment ou inconsciemment, des éléments
d’antisémitisme, il s’agit d’avantage de voir comment la
gauche ignore et minimise un antisémitisme plus large qui, au fond,
se moque éperdument du sort des Palestiniens. En effet, la reprise
de tropes antisémites dans des invectives contre Israël demeure
largement sous-thématisée voire niée par une grande partie de la
gauche [25]. Quand elle s’y confronte, ses réactions reproduisent
les mécanismes d’évitement évoquées au sujet de stéréotypes
antisémites plus traditionnels.

Pour une grande partie de la gauche, évoquer les actes antisémites commis
au nom d’une pseudo-solidarité avec les Palestiniens reviendrait à
discréditer les mouvements anti-impérialistes et la résistance à la
politique d’Israël. Dans une discussion du 16 juillet 2014 sur Oumma
TV autour des polémiques sur la manifestation pro-palestinienne du 13
juillet 2014, Michèle Sibony, membre de l’UJFP et Youssef Boussoumah,
théoricien décolonial et membre du Parti des Indigènes de la République
(PIR), ne tentent aucunement de distinguer entre fake news (attaque
d’une synagogue) et nombreuses manifestations d’antisémitisme
avérées [26] . Ils récusent toute accusation d’antisémitisme en
invoquant des « provocations » visant à discréditer la mobilisation et
justifier les interdictions de manifestation » [27]. Cette reconduction
de l’opposition entre bons manifestants et éléments provocateurs
élimine la possibilité même d’une critique pratique et théorique des
expressions d’antisémitisme dans les rangs de ceux qui se revendiquent
de la dignité ou de la révolution.

En dernière instance, Israël serait responsable de
l’antisémitisme. L’État juif, l’impérialisme et ses relais
alimenteraient la confusion antisémitisme/antisionisme et juifs/Israël et
généreraient des antisémites et des tensions inter-communautaires. Dans
un article de Rouge, l’hebdomadaire de l’ancienne LCR à propos d’une
manifestation pro-palestinienne en 2000, Christian Picquet affirmait
par exemple qu’il fallait certes critiquer ceux qui crient « mort aux
Juifs » en appelant à la guerre sainte dans les manifestations, mais que
les leaders de la communauté juive, en appelant à une identification
avec Israël, ne peuvent « qu’encourager de part et d’autre la
confrontation entre Juifs et Arabes ». Selon l’auteur, les positions
pro-israéliennes du CRIF et des responsables de la communauté juive
alimenteraient l’antisémitisme en soutenant l’État d’Israël [28].
Une longueur d’avance à droite ?

Depuis plus de deux décennies, la question de l’antisémitisme est
captée et noyautée par la droite et plus récemment par certaines
parties de l’extrême-droite FN-RN. Dans la conjoncture actuelle,
l’antisémitisme n’est pas le seul sujet sur lequel la droite et
l’extrême-droite donnent le ton. La gauche et l’extrême-gauche sont
le plus souvent dans une position défensive et peinent à proposer leurs
propres analyses. La question de l’antisémitisme, délaissée, a été
abandonnée à la droite [29]. Comment ce glissement a-t-il eu lieu ?

En 1995, c’est Jacques Chirac qui reconnaît enfin la responsabilité
du régime de Vichy, de l’État français et de sa police dans
la déportation et le massacre de plus de 70 000 juifs français
et étrangers dans le cadre de la collaboration avec l’Allemagne
nazie, alors que François Mitterrand s’était tu. Désormais, chaque
présidence est marquée par un moment de commémoration, d’appel à
la vigilance et d’alerte contre la persistance, le renaissance ou le
regain de l’antisémitisme. Cette politisation de l’antisémitisme
mêle moralisme anhistorique et « devoir de mémoire ». La proposition
de Sarkozy, en 2008, que chaque enfant de CM2 parraine un enfant mort
en déportation, d’ailleurs vite abandonnée, n’est que le versant
dadaïste sinon obscène de ce traitement mémoriel.

Deux stratégies simultanées se sont révélées délétères pour les
juifs en France et déterminent la faiblesse des réponses politiques
apportées par la gauche. D’abord, les discours publics ne cessent
d’entretenir l’amalgame entre défense des juifs et défense de
l’État d’Israël. Ensuite, les représentants politiques de droite
et de centre-gauche œuvrent à séparer la question de l’antisémitisme
des autres formes de racisme en France.

Au moment même où il emboîte le pas du FN en célébrant l’identité
nationale tout en hystérisant les questions migratoires et de sécurité,
Sarkozy fait de nombreuses déclarations au sujet de l’antisémitisme. Son
discours de 2011 au dîner annuel du CRIF mélange des considérations
générales sur l’État d’Israël, sur le rôle de la France au
Moyen-Orient et sur la question de l’antisémitisme en France. ll
évoque la contribution du judaïsme à l’identité de la France
ainsi que ses double racines chrétiennes et juives [30]. Le discours
libéral sur l’émancipation des juifs et leur participation à la
nation française est repris sous une forme droitière et utilisée
contre d’autres groupes racisés sur lesquels le contrôle sécuritaire
s’approfondit. Ce positionnement de la présidence Sarkozy cristallise
parfaitement le discours civilisationnel imposé par la droite et
l’extrême-droite dans le champ politique et intellectuel. Celui-ci
dessine un « conflit de civilisation » interne à la France entre
une identité judéo-chrétienne et la figure de l’ennemi fondée
sur un continuum allant du « jeune de banlieue » au « terroriste
islamiste ». Sur ce sujet, Manuel Valls est dans la droite ligne de
ses prédécesseurs et associe systématiquement soutien à Israël et
défense inconditionnelle des juifs de France [31].

Plus récemment, la stratégie de Marine le Pen de « normalisation
du FN » s’est accompagnée d’un tournant sur la question de
l’antisémitisme. Alors que son père était ouvertement antisémite,
négationniste et hostile à l’égard de l’État d’Israël,
Marine le Pen prend progressivement position en faveur de la politique «
d’auto-défense » d’Israël et de la lutte contre l’antisémitisme,
quitte à s’aliéner certains membres et fractions ouvertement
antisémites de son propre parti [32]. Au printemps 2018, dans le cadre de
la répression par Tsahal de la marche au retour à Gaza, Marine Le Pen
et Louis Aliot critiquent Macron pour sa dénonciation des violences de
l’armée israélienne et associent allégrement la menace terroriste que
subissent les Israéliens à la menace dans « nos rues de France » [33].

Ce dispositif de mise en concurrence des diverses formes de racisme et le
« mémorialisme » des discours publics sur l’antisémitisme ont fourni
les termes du débat pour certains entrepreneurs antisémites. Ils ont
consolidé la popularité de Kemi Seba et ont aussi été un vecteur de
l’alliance entre Soral et Dieudonné. Le langage du « deux poids deux
mesures » s’est imposé : on parle trop des juifs ; on parle trop de la
Shoah. Le dispositif de concurrence victimaire s’est renforcé : on ne
parle que d’Auschwitz et pas de l’esclavage ; on ne parle du camp de
Drancy que comme antichambre à l’extermination pendant l’Occupation,
mais il n’existe pas de lieu de mémoire pour les colonisés. Cet
antisémitisme mémoriel, aux relents négationnistes, dont la chanson
« Shoananas » de Dieudonné est le chef-d’oeuvre incontesté, est
donc aussi le produit d’un traitement droitier de l’antisémitisme
[34]. Un véritable jeu de miroirs réfléchissant s’est installé entre
cet antisémitisme mémoriel et une forme de bien-pensance droitière qui
sature l’atmosphère politique et intellectuelle autour de la question
de l’antisémitisme.

Cette recomposition politique s’accompagne d’une hyper-exposition
médiatique d’un certain nombre de personnalités juives très à droite,
comme Finkielkraut, et d’un glissement à droite du CRIF. Est-ce le
signe d’un inexorable basculement général des juifs de France vers
la droite ? Si Traverso parle de Fin de la modernité juive [35], et
s’il est difficile de nier que de nombreux juifs de France sont plus
conservateurs qu’il y a vingt ans, rien ne permet d’affirmer que
ce mouvement a eu lieu à un rythme différent de la droitisation de
l’ensemble de la société française.

Sans analyse de la spécificité de l’antisémitisme, et réticente
à déplacer les termes du débats, en ignorant trop souvent la question,
la gauche, loin de se fracturer autour de l’antisémitisme, reste passive
et défensive. Dans le champ de l’extrême-gauche, cela se marque par
une coupure entre la vivacité des réflexions sur le racisme depuis les
années 2000 et l’interrogation sur l’expérience historique de la
persécution juive. Fanon avait pourtant pu rattacher l’expérience des
colonisés à l’antisémitisme et faire des réflexions sartriennes
un aiguillon pour un questionnement de l’expérience coloniale
[36]. Pierre Goldmann pouvait relater sa découverte du racisme anti-noir
aux États-Unis lorsque, emprisonné avec des blancs, il constatait que
son seul co-détenu non raciste était juif et que celui-ci considérait
que les attitudes racistes des autres détenus blancs relevaient d’«
histoires de goys » [37]. Force est de constater que ce continuum ne
fonctionne plus aujourd’hui.

Au printemps 2018, il semble qu’on ait vu l’aboutissement d’une forme
de recomposition du champ politique autour de l’antisémitisme. La
marche d’hommage à Mireille Knoll, dame de 85 ans tuée le 23 mars
2018 par un voisin antisémite dans son appartement du Nord parisien,
s’est insérée dans l’agenda national-sécuritaire de l’État et à
sa rhétorique anti-terroriste. La marche coïncide avec l’hommage au
gendarme « premier de cordée » Arnaud Beltrame. Elle permet aussi au
RN-FN de se présenter en « ami des juifs ». S’il semble difficile,
dans les faits, de séparer le bon grain du « meurtre crapuleux » et
paranoïaque de l’ivraie antisémite, il n’en demeure pas moins que
les formes de pensée de l’assassin ont fortement joué dans le choix
de la victime et les mobiles du meurtre. La violence du mode opératoire
— brûler sa victime après l’avoir longuement poignardée — ayant
touché, de plus, une personne rescapée de la rafle du Vel d’Hiv’,
a aussi suscité un émoi bien compréhensible.

Après une campagne destinée à faire reconnaître le caractère
antisémite de l’assassinat, une marche blanche fut l’occasion
d’affirmer l’unité républicaine autour de ce problème et toute la
classe politique annonce son intention de défiler. Cependant, Francis
Khalifa, le président du CRIF, annonce que ni Marine le Pen ni Jean-Luc
Mélenchon n’y sont bienvenus. Dans les faits, c’est Jean-Luc Mélenchon
qui se fait évacuer de la manifestation par l’insignifiante Ligue de
Défense Juive (LDJ) qu assure cependant la « sécurité » de Marine
Le Pen jusqu’à la fin. En ce sens, sur ce terrain symbolique, cette
marche semble marquer une forme de paroxysme dans le traitement droitier
de l’antisémitisme. À cet égard, encore une fois, la faiblesse
de la réponse de la gauche, en particulier la réponse de Mélenchon
à son éviction de la marche blanche [38], est significative de son
incapacité à proposer, ne serait-ce que sur un terrain symbolique,
une grille de lecture adéquate à l’événement.  Quelle place pour
l’antisémitisme vécu ?

Cette sous-estimation de la question de l’antisémitisme et les
reconfigurations politiques dont elle a été l’objet ont eu des
conséquences fortes sur les juifs, leur implication dans le champ
politique à l’extrême-gauche et la possibilité d’y trouver des
alliés pour répondre à ces problèmes.

Le vécu de l’antisémitisme en France, ce que ses manifestations
représentent pour ceux qui les vivent, ont été constamment minimisés
à l’extrême-gauche. Il n’existe pas ou peu d’espace où il est
possible de parler du caractère profondément agressif et anxiogène
de ses manifestations quotidiennes et diffuses. Une conversation de
comptoir où l’on entend que les juifs ont trop de pouvoir, un voyage
en tramway où un prêcheur improvisé évoque « l’extermination
de Sion », des remarques sur « l’efficacité » de tel syndicaliste
dans la négociation d’un conflit de fin de grève parce que son
patronyme le prédispose à la puissance et à l’argent ne sont
pas des incidents isolés. De tels stéréotypes sont confus, parfois
peu idéologisés. Ils sont pourtant persistants et stables dans leur
contenu. Et cette intrication laisse souvent démuni.

Tout ceci mine le quotidien mais peut aussi avoir des conséquences dans
la relation des juifs politisés à certains espaces politiques. Les
luttes de base ne sont pas indemnes de ces stéréotypes antisémites qui
traversent la lutte de classe : une manif, un rassemblement de solidarité
sur un piquet de grève... Les espaces d’élaboration théoriques
ou culturels non plus. Il n’y a, sur ce terrain comme sur d’autres
d’ailleurs, aucune autonomie idéologique stricte de la gauche ou
de l’extrême-gauche. Celle-ci semble aussi éponger, en mineur,
les lames de fond idéologiques qui traversent la société française.

En fonction des subjectivités juives, des outils intellectuels et
politiques dont on dispose, selon les circonstances, on protestera ou
on se taira. Réagir en solitaire n’est pas simple. Pour beaucoup, la
socialisation de ces problèmes — incidents quotidiens, commentaires de
l’actualité — a lieu principalement dans l’espace de la famille
ou avec quelques rares camarades juifs partageant la même vision du
monde et peu disposés à laisser ces questions à la droite et à
l’extrême-droite. Ainsi, la configuration objective de la période
rétrécit singulièrement les espaces politiques où ces questions
sont audibles.

Ceux qui s’attachent à réellement investir ces questions font donc face
à un double front. Le premier est celui des antisémites patentés et de
manière plus diffuse, ceux qui peuvent les relayer. Le second est occupé
par ceux qui proposent une lecture droitière de l’antisémitisme ou
l’intègrent à l’agenda national-sécuritaire de l’État,
ce qu’Ivan Segré a appelé « la réaction philosémite »
[39]. Cette configuration complexe a des effets retors. Poser la question
de l’antisémitisme comme fait social à l’extrême-gauche aurait
comme préalable la démarcation et de la politique israélienne et de
toutes les lectures droitières, en particulier la thèse du « nouvel
antisémitisme », qui se sont imposées dans l’espace public. Certes,
mais cet amas de prérequis court-circuite parfois tout simplement la
possibilité de pouvoir poser la question.

Il s’avère qu’un banal récit d’expérience d’une personne juive
victime de racisme ou un simple questionnement — pourquoi ce sentiment
d’une montée de l’antisémitisme de plus en plus répandu chez les
juifs en France ? — n’ont le plus souvent tout simplement pas droit
à l’existence. Ne posons pas ces questions, donc, ou posons-les avec
de telles circonvolutions que l’antisémitisme en tant que tel ne sera
pas nommé. Effet boomerang. Certains juifs d’extrême-gauche semblent
avoir intégré l’idée qu’ils porteraient une responsabilité
congénitale dans la politique israélienne ou qu’ils devraient en
permanence se blanchir de toute compromission avec les « Sentinelles
» qui gardent depuis quelques années les portes des synagogues. Ce
désintérêt n’est pas sans nourrir en retour le déni ou
l’incapacité à faire entendre une voix juive ancrée dans ce vécu
et qui ne nourrisse pas les lectures du CRIF et consorts.

D’autre part, il convient de ne pas minimiser le caractère
socio-centré de cette invisibilisation de l’antisémitisme. La
capacité à le nommer n’est pas délié de la question des
espaces politiques susceptibles d’accueillir et de légitimer de
tels énoncés. Mais les juifs aussi sont pris, comme d’autres
populations racialisées, dans des formes de subjectivation d’autant
plus fortes qu’ils sont plus bas socialement. L’universel est un
luxe. Le fait de porter un nom juif n’est pas un obstacle pour un
directeur de maison d’éditions, il ne l’est pas nécessairement
pour une femme d’origine juive séfarade quand elle fait son marché
à Aubervilliers. Mais si le premier se rappellera qu’il est juif
en écrivant depuis son bureau une missive de solidarité avec « la
Palestine », la seconde ne l’oubliera pas, parce que, dans l’espace
social où elle évolue souvent, tout le monde le lui rappelle. « —
Tu viens d’où ? — Shalom Madame ».

C’est cet aspect situé mais labile de l’expérience juive, qui
renvoie aussi à un vécu de l’antisémitisme possiblement très
différencié, qui peine à se faire entendre. De plus, il semble que
ce soit une caractéristique intrinsèque des discours antisémites de
faire appel à une force invisible, masquée, relevant souvent d’une
construction idéologique diffuse et floue, « la rumeur sur les juifs
» [40] dont parle Adorno et Horkheimer. L’antisémitisme fonctionne
d’abord comme « fausse projection » [41]. Or, tout se passe comme si,
pour une grande partie de la gauche, on considérait que « la rumeur
» n’affectait pas des personnes, des groupes sociaux. Comme si une
quenelle, des propos négationnistes, le renvoi d’un militant juif
au pouvoir ne relevaient que de la sphère du discours. Alors, si les
discours violemment antisémites ou négationnistes peuvent être
relevés, toute phénoménologie précise des manifestations de
l’antisémitisme, tout éclaircissement du continuum existant entre
moments hystériques et rumeur de basse intensité font défaut.

Cause ou conséquence, une fracture semble se creuser entre l’antiracisme
politique et la question de l’antisémitisme, deux enjeux pourtant
historiquement liés. Dans les espaces spécifiquement dédiés à
l’antiracisme, à même de servir de courroie de transmission entre les
expériences vécues du racisme et une grille d’analyse plus large,
les juifs n’ont pas droit de cité. On considère qu’ils ne sont
pas victimes du même racisme structurel que les noirs, les arabes ou
les rroms. À raison : les juifs ne subissent pas de discrimination
au logement ou à l’embauche et ne sont plus victimes de racisme
d’État. Ce signifiant de « structurel » fétichisé se réduit en
l’espèce à l’idée que la structure, c’est l’État. Il joue
dans ce contexte précis le rôle de cache-sexe de la socialisation
spécifique de l’antisémitisme et empêche toute lecture de ses
effets bien réels.

Les expériences subjectives de personnes juives n’offrent pas
nécessairement une vision claire de la situation. Mais quelque chose
qui relevait il y a quelques années encore d’une sorte de malaise
difficilement nommable ne peut que se dire et se politiser. La conjoncture,
même si elle est trop souvent appréhendée par le biais d’une
comparaison impressionniste entre l’époque de crise sociale et
politique que nous vivons et les années trente, redonne un certain
relief à la question de l’antisémitisme. À une échelle large,
il devient difficile de ne pas corréler cette conjoncture et un réseau
d’actes antisémites qui vont de la prise d’otages de l’Hyper Cacher
en janvier 2015 au massacre de Pittsburgh en octobre 2018. À l’échelle
des subjectivités politiques gauchistes, la formation d’un petit groupe
comme les Juives et juifs révolutionnaires en est le symptôme [42].

Ainsi, la conjoncture invite à replacer les poussées d’antisémitisme
dans une montée du racisme et des réponses nationales-sécuritaires
et populistes à la crise sociale. Elle invalide sérieusement la
thèse d’un antisémitisme exclusivement « arabo-musulman » et nous
invite plutôt à comprendre les effets de vase communicants entre
plusieurs types de racisme, quoique leurs moteurs soient partiellement
différenciés : le racisme dit structurel qui frappe les populations
noires et arabes et la matrice spécifique de la racialisation des
juifs, matrice conspirationniste, possiblement protestataire – le
« socialisme des imbéciles ». Tout semble indiquer que la violente
hostilité aux migrants en Italie s’est accompagnée d’une montée
de l’antisémitisme [43] et que la présidence Trump aux États-Unis
est elle aussi marquée par une recrudescence des actes antisémites.

Qu’en est-il de l’antisémitisme dans le mouvement des gilets
jaunes qui a fait irruption en France à la fin du mois de novembre
? Considérer le problème, c’est d’abord acter une fois pour
toutes que, si antisémitisme il y a, il a un aspect diffus, et n’est
ni nouveau ni ethnique. Il peut autant concerner le « vrai Français »
paupérisé que « l’arabo-musulman ». Serait-ce un terrain susceptible
de mettre tout le monde d’accord ?

Avec les gilets jaunes, la question sociale, celle de la dégradation
généralisée des conditions matérielles semble être entrée
par la petite porte de la question fiscale et d’une hostilité à un
président des riches qui ne respecte plus aucune « économie morale »
[44]. Dans la socialité des ronds-points et la dynamique émeutière
plus métropolitaine, une fraction du prolétariat et de la classe moyenne
prolétarisée frappée par la crise s’est manifestée indépendamment
des cadres de la politique classique. Les gilets jaunes sont d’abord
le symptôme de l’ampleur de la crise sociale et se donnent comme une
révolte émancipée de l’encadrement des organisations issues du
mouvement ouvrier. Celle-ci n’est d’aucune manière réductible
à ses composantes les plus fascistoïdes.

Il est néanmoins établi que des thèses antisémites sont diffusées
chez les gilets jaunes à une échelle relativement large, d’abord sur
Internet, mais aussi dans les énoncés et les slogans que le mouvement
s’est donnés [45]. Cet antisémitisme a des accents complotistes
contre la « toute-puissance » de Rothschild, responsable de la « ruine
de la France », et mêle l’association de la puissance politique et
médiatique à Sion, la focalisation sur BFMTV et son patron Drahi, à
des références récurrentes à la « Quenelle » soralo-dieudonnienne
— le « Macron, une quenelle dans ton cul » n’est pas un slogan
isolé –- voire des propos clairement négationnistes.

L’opinion libérale saute à pieds joint sur ces manifestations
d’antisémitisme bien réelles. Contre le mouvement des gilets jaunes,
la bourgeoisie française fait preuve de tout le mépris de classe
qu’elle a en réserve. Elle se dédouane aussi des parts les plus
sales de son histoire en les attribuant aux classes dangereuses.

Quant à la gauche radicale, lorsqu’elle n’a pas opté pour
l’hostilité à une dynamique qui lui échappe largement, elle ne
déroge le plus souvent pas à ses habitudes et ne prend aucunement
la mesure du problème. Elle minimise cet antisémitisme ou ne parle
que d’instrumentalisation dans une optique pseudo-tactique (ne pas
nourrir l’offensive idéologique contre le mouvement). Qu’une
vieille dame juive agressée à la fin de l’Acte VI par des gilets
jaunes négationnistes ne porte pas plainte, doit-on en déduire que
cette agression, qui n’est malheureusement pas isolée, n’est
qu’ivrognerie [46] ? Dans ce cas, on s’interdirait de penser
l’antisémitisme comme une violence raciste et ce qu’il implique,
y compris pour les juives et juifs impliqués dans la dynamique des
gilets jaunes. On s’empêcherait aussi de penser l’antisémitisme
comme une véritable limite du mouvement.

D’une part, ces thèmes antisémites accompagnent souvent le racisme
anti-migrants et l’exaltation de la communauté nationale. L’aspect «
post-idéologique » des gilets jaunes et la crise ultime de la gauche
dont il est le signe laissent place libre à des figures comme Étienne
Chouard ou le journaliste Vincent Lapierre qui mêlent démocratisme
radical, révolution nationale et désignation d’un Autre responsable
des « malheurs de la France ». Ils sont au cœur du possible devenir
Cinque-stelle des gilets jaunes. D’autre part, l’expansion des tropes
antisémites constitue une forme de remède aux difficultés tactiques
internes. Que reculent les blocages, barricades sur les Champs-Élysées
ou les pillages de magasins, on se concentrera sur les Rothschild
– faible substitut à l’avancée tactique de la révolte. Si
l’antisémitisme n’est jamais complètement fonctionnel, et s’il
conserve un fond archaïque et pulsionnel, on ne peut le minimiser
comme ingrédient essentiel de la mayonnaise nationale-populiste et
comme courroie de transmission entre une opposition à « ceux d’en
haut » et un scénario de restructuration populiste.

L’antisémitisme n’est pas tant une fracture de la gauche que
**l’objet d’un silence**, source de malaise pour les juifs en milieu
« progressiste ». Dans la lignée de la thématisation douloureuse
du racisme structurel accomplie par l’antiracisme politique au cours
des années 2000 et 2010, le travail de « fracturation » autour de
l’antisémitisme, avec tous les déchirements qu’il implique, reste
à faire. À rebours du cas allemand où une large partie de la gauche
s’enfonce de plus en plus dans une islamophobie voire xénophobie
assumée au nom de lutte contre l’antisémitisme, un tel travail
de problématisation devra s’insérer dans une confrontation plus
large avec l’ensemble des processus de racialisation des sociétés
capitalistes occidentales. Cela nécessiterait de se pencher à la fois
sur la spécificité de la racialisation des juifs et la manière dont
celle-ci fonctionne conjointement avec d’autres formes de racisme au
sein de la totalité sociale. En effet, cette racialisation des juifs
prend souvent la forme particulière de l’attribution d’un pouvoir
ou d’une influence démesurés à ce groupe. Pour autant, elle partage
avec d’autres formes de racialisation leur caractère essentialisant et
homogénéisant, qui a pour effet de construire le groupe visé comme une
filiation nocive et inchangeable. La spécificité de l’antisémitisme ne
pourrait donc être saisie que dans ses liens historiques et contemporains
à d’autres formes de racialisation, notamment l’islamophobie et le
racisme anti-Rrom. C’est d’autant plus important que, sur le plan
pratique, on ne peut combattre l’antisémitisme qu’à condition de
combattre conjointement ces autres formes de racialisation qui concourent
plus ou moins directement à un acharnement sur les (supposés) juifs. Si
donc l’analyse et la théorisation de l’antisémitisme requièrent de
prendre en compte le mode opératoire propre de celui-ci, on ne pourra que
déplorer la séparation de la lutte contre l’antisémitisme des autres
luttes anti-racistes.Par conséquent, sous les conditions actuelles d’un
regain de l’antagonisme social et d’expressions d’antisémitisme de
plus en plus ouvertes, faire de ce dernier un enjeu des luttes antiracistes
est une tâche politique des plus urgentes. À défaut, l’alliance entre
différentes fractions de classe, ou encore entre les « beaufs » et les
« barbares », à laquelle certains appellent, loin de s’effectuer dans
l’amour révolutionnaire autour d’un projet d’émancipation sociale,
se fera sur le dos des juifs.


Bibliographie
===============

Contact : contactarticleantisem@gmail.com

Léa Nicolas-Teboul est militante et féministe. Memphis Krickeberg
est chercheur en politiques pénales et animateur du blog
http://solitudesintangibles.fr.

[1] Cet article est ici publiée dans une version longue par rapport à la
version publiée dans l’édition imprimée de Vacarme 86 (hiver 2019),
disponible en librairies depuis le 15 février 2019.

[2] L’une des principales références de ces discours est l’ouvrage
de Pierre-André Taguieff, La nouvelle judéophobie, Paris, Mille et une
nuits, 2002.

[3] Contre un antiracisme moral se contentant de dénoncer les préjugés
racistes individuels, l’antiracisme politique considère que le racisme
nécessite d’être appréhendé comme une structure de domination devant
être combattue sur la base de l’auto-organisation des sujets subissant
le racisme.

[4] À quelques exception près : depuis le début des années 2000,
des penseurs comme Ivan Segré, le site ultra-gauche Mondialisme.org,
le site maoïste Les matérialistes.com ou l’association Mémorial 98
fournissent un très important travail de réflexion sur l’antisémitisme
et le rapport de la gauche à ce dernier. Depuis 2015, le groupe Juives
et Juifs Révolutionnaires, le collectif antifasciste Lignes de Crètes
et le blog communiste Solitudes Intangibles.fr ont émergé avec pour
mission de thématiser l’antisémitisme du point de vue d’une critique
émancipatrice de la société.

[5] Commission nationale consultative des droits de l’Homme (CNCDH),
La Lutte contre le racisme, l’antisémitisme et la xénophobie. Année
2017, La documentation française, p. 98 et sq.

[6] Michel Wieviorka, L’antisémitisme est-il de retour ?, Larousse, 2008.

[7] Marilyn Mayo, « Anti-Semitic 9/11 Conspiracy Theorists
Thrive 15 Years After Attacks », Anti-Defamation League
blog, 9/09/2016, https://www.adl.org/blog/anti-semit...,
https://www.adl.org/blog/anti-semit...

[8] Financial Crisis Sparks Wave of Internet Anti-Semitism,
rapport d’enquête de la Anti-Defamation League, 24/10/2008,
https://www.adl.org/sites/default/f....

[9] CNCDH, La lutte contre le racisme, l’antisémitisme et la
xénophobie. Année 2012, La Documentation française, p. 103.

[10] Albert Herszkowicz, « L’antisémitisme tue », 17/07/2009,
https://npa2009.org/content/lantise.... Sans forcément s’en remettre à
la police, plusieurs analyses informées par l’enquête sur le rapt et
le meurtre d’Ilan Halimi ont clairement établi que l’un des principaux
mobiles en était l’association des juifs à l’argent. Voir par exemple
les références citées dans Enzo Traverso, La Fin de la modernité juive,
La Découverte, p. 114.

[11] Houria Bouteldja, « Mohamed Merah et moi », indigenes-republique.fr,
6/04/2012 http://indigenes-republique.fr/moha...

[12] « Vous avez dit Charlie ? », zad.nadir.org, 20/01/2015,
https://zad.nadir.org/spip.php?arti...

[13] Johann Jacoby, « Verschwörungstheorien – Eine
einfache Welterklärung ? », emafrie.de, 03/2011,
http://emafrie.de/verschworungstheo...

[14] Rudy Reichstadt, « Le conspirationnisme dans l’opinion
publique française », jean-jaures.org, 07/01/2018,
https://jean-jaures.org/nos-product...

[15] « Antisémitisme : 21% des Français de 18 à 24 ans n’ont
jamais entendu parler de la Shoah », huffingtonpost.fr, 28/11/2018,
https://www.huffingtonpost.fr/2018/...

[16] Theodor W. Adorno et Max Horkheimer, « Éléments de
l’antisémitisme. Limites de la raison » in La Dialectique de la raison,
trad. E. Kaufholz, Gallimard, 1974, p. 249-305.

[17] Quartiers Libres, « Lutte contre l’antisémitisme, lutte contre
l’islamophobie : un parallèle ? », quartierslibres.bandcamp.com,
04/04/2016, https://quartierslibres.bandcamp.co... Voir aussi Quartiers
Libres, « Antisémitisme », quartierslibres.bandcamp.com,
05/01/2016, https://quartierslibres.bandcamp.co... et
Quartiers Libres, « Anticonspirationnisme : un opportunisme
politique ? », quartierslibres.bandcamp.com, 02/03/2016,
https://quartierslibres.bandcamp.co...

[18] Frédéric Lordon, « Le complotisme de l’anticomplotisme »,
monde-diplomatique.fr, octobre 2017, https://www.monde-diplomatique.fr/2...

[19] Frédéric Lordon, « Le symptome d’une dépossession »,
monde-diplomatique.fr, juin 2015, https://www.monde-diplomatique.fr/2...

[20] Populisme mélenchonien ; formations staliniennes nostalgiques du
Parti Communiste Français (PCF) des années 1960 ; formations écologistes
et alternatifs ; anarchisme dans sa forme la plus frustre…

[21] Houria Bouteldja, « Progressisme blanc et régression féconde
indigène. Les Beaufs et les Barbares : sortir du dilemme. »,
indigenes-republique.fr, 21/06/2018, http://indigenes-republique.fr/les-....

[22] « Sondage Ifop : Ce que les français pensent d’Israël »,
coolisrael.fr, 2013, http://coolisrael.fr/8561/sondage-i...

[23] Léon Poliakov, De Moscou à Beyrouth. Essai sur la désinformation,
Calmann-Levy, 1983.

[24] Voir notamment Stephan Grigat, « Société libérée et Israël. Du
rapport entre théorie critique et sionisme », solitudesintangibles.fr,
2018, http://solitudesintangibles.fr/soci...

[25] Par exemple, en 2013, au sujet de la politique économique européenne
de Pierre Moscovici, J.L. Mélenchon a évoqué « le comportement
de quelqu’un qui ne pense plus en français… qui pense finance
internationale ». Mélenchon a été accusé d’antisémitisme et a
répondu avec un discours ferme qui le dénonce. Cependant, on aurait
aimé voir émerger au sein du Parti de Gauche une réflexion sur le
glissement possible entre « critique de la finance » et « critique de
la juiverie organisée » ou « du capitalisme juif ». Bref, au-delà
d’une position défensive contre le « chantage » à l’antisémitisme,
aucune réflexion n’a été entamé sur les tropes qui peuvent véhiculer
l’antisémitisme et faire glisser vers des positions ouvrant la voie
à un Querfront (front transversal) entre gauche souverainiste et droite
nationaliste.

[26] « Oui il y a des problèmes d’antisémitisme
chez des pro-palestiniens », paris-luttes.info, 2014,
https://paris-luttes.info/oui-il-y-... ; « À propos des
manifestations “pro-Gaza” : réflexions sur l’antisémitisme,
le sionisme et le nationalisme », larotative.info, 2014,
https://larotative.info/a-propos-de...

[27] « Incidents synagogue : nouvelles révélations », Oumma, 16/07/2014,
https://www.youtube.com/watchtime_c...

[28] Christian Picquet cité in Robert Hirsch, Sont-ils toujours des
juifs allemands ? La gauche radicale et les Juifs depuis 1968, Arbre
bleue éditions, p. 175.

[29] Robert Hirsch, Sont-ils toujours des juifs allemands ? La gauche
radicale et les juifs depuis 1968, L’Arbre bleu, 2017.

[30] Nicolas Sarkozy, « Discours au dîner annuel du Crif », Lemonde.fr,
28/04/2008, https://www.lemonde.fr/politique/ar...

[31] Cécile Chambraud, « Au dîner du CRIF, Manuel Valls
associe antisionisme et antisémitisme », Lemonde.fr, 08/03/2016,
https://www.lemonde.fr/religions/ar...

[32] « Gaza:pour Marine le Pen, « Israël a envoyé un message
clair sur l’inviolabilité de sa frontière », rt.com, 15/05/2018,
https://francais.rt.com/internation...

[33] Ibid.

[34] En réaction à l’hégémonie de Soral et Dieudonné sur le you
tubing politique, ces derniers ayant commencé à investir You Tube à
la fin des années 2000, une génération de youtubeurs se situant à
gauche et à l’extrême gauche du spectre politique a émergé (Usul,
Osons Causer, Le Stagirite ...). Il à ce titre fascinant de constater que
parmi ces youtubeurs de gauche mus notamment par la volonté de contrer
la fachosphère et sa longueur d’avance en matière de contenus vidéo,
aucun n’a pris le temps de critiquer, d’analyser ou de faire une
videos sur l’antisémitisme, cheval de bataille et point de convergence
des vidéos de Soral et Dieudonné.

[35] Enzo Traverso, La Fin de la modernité juive, La Découverte, 2013.

[36] Frantz Fanon, Peaux noires masques blancs, Seuil, 1952.

[37] Pierre Goldmann, Souvenirs obscurs d’un juif polonais né en France,
Seuil, rééd. 2005, p. 55.

[38] Mélenchon se contente ainsi d’invectiver le CRIF, « secte
communautaire » dont l’action serait contraire à l’universalisme
républicain. Voir Jean-Luc Mélenchon, « Le jour de la honte »,
mélenchon.fr, 02/04/2018, https://melenchon.fr/2018/04/02/le-....

[39] Ivan Segré, La Réaction philosémite, Paris, Lignes, 2009.

[40] Theodor W. Adorno et Max Horkheimer , « Éléments de
l’antisémitisme. Limites de la raison », op. cit..

[41] Ibid.

[42] https://www.facebook.com/pages/cate....

[43] Ariel F. Dumont, Au-delà du foot, la tentation antisémite de
l’Italie, marianne.net, 2017, https://www.marianne.net/monde/au-d....

[44] Sur la reprise de cette notion de Thompson, voir Samuel Hayat,
« Les gilets jaunes, l’économie morale et le pouvoir »,
samuelhayat.wordpress.com, https://samuelhayat.wordpress.com/2....

[45] La question de l’antisémitisme a été appréhendée
uniquement sous l’angle du fait divers jusqu’à présent. De nombreux
témoignages de militants participant au mouvement font cependant état
d’une diffusion très importantes de tropes antisémites dans les
manifestations et groupes Facebook et Whatsapp. Dans les groupes de
Gilets Jaunes d’Île-de- France, la question de l’antisémitisme
et la lutte contre les soraliens constituent des enjeux politiques
majeurs au sein du mouvement. On peut lire à ce sujet « Ne laissons
personne récupérer notre révolte », un tract de Gilets Jaunes
contre l’extrême droite et l’antisémitisme relayé sur
le compte Facebook de la Plateforme d’Enquête Militantes le 31
décembre 2018 https://paris-luttes.info/publicati.... Voir aussi Lucie
Soullier, « Les “gilets jaunes”, nouveau terrain d’influence de
la nébuleuse complotiste et antisémite », lemonde.fr, 19/01/2019,
https://www.lemonde.fr/societe/arti....

[46] Claude Askolovitch, « La défense des juifs, ultime morale
des pouvoirs que leurs peuples désavouent », slate.fr, 26/12/2018,
http://www.slate.fr/story/171594/gi....
