
.. index::
   pair: 2019-03-02; Ménilmontant
   pair: 2019-03-02; PopCultWeapon



.. _precisions_menilmontant_2019:

=============================================================================================================================
Précisions concernant le rassemblement contre l’antisémitisme et son instrumentalisation du 19 février 2019 à Ménilmontant
=============================================================================================================================


.. seealso::

    - https://paris-luttes.info/precisions-concernant-le-11745
    - https://fediverse.com/PopCultWeapon/status/1102134381668376576


.. figure:: travailleuses_juives.jpg
   :align: center

Intervention d’un juif communiste sur la question de la prise en compte
de l’antisémitisme dans nos luttes et milieux au rassemblement contre
l’antisémitisme et son instrumentalisation du 19 février 2019 à
Ménilmontant.

Un contre-rassemblement « Contre l’antisémitisme, contre son
instrumentalisation, pour le combat contre toutes les formes de
racisme » était appelé le 19 février 2019 par l’Union des
Juifs Français pour la Paix (UJFP) en réaction à la grande messe
républicaine « contre l’antisémitisme » place de la République
rejointe par treize partis politiques.

Avec une camarade, nous nous rendons à Ménilmontant et réussissons à prendre
le micro pour partager des éléments d’analyse sur l’antisémitisme.

Un petit muret surplombe une foule d’environ 400 personnes agglutinées autour
de la bouche de sortie du métro ; elle écoute la petite sono et les
porte-paroles des quatorze organisations ayant signé l’appel de l’UJFP
(http://www.ujfp.org/spip.php?article6932). Voici la retranscription de
l’une de nos interventions.  Une parole juive contre l’antisémitisme

« Bonsoir Haverim vehaverot, Akhim wa Okhtet, cher-es camarades. Je
m’appelle Yunes, je suis juif, j’habite en Bretagne. J’ai grandi
dans une famille traditionnelle juive, dix ans d’école juive et de
talmud tora, je fréquente les milieux de gauche depuis quelques années
seulement. Je félicite et remercie les organisateurs et organisatrices
de ce rassemblement, je trouve ça très important qu’une voix
juive de gauche s’exprime en cette période pleine de troubles mais
aussi pleine de possibles.  Si je suis ici, c’est parce que je suis en
colère. Contre qui ? Je suis en colère d’abord contre les antisémites,
et tous les racistes. Vous allez me dire « ça mange pas de pain », oui
c’est vrai.  Je suis en colère aussi contre l’instrumentalisation
de l’antisémitisme. Vous allez me dire « ha ça tombe bien, c’est
exactement le titre de ce rassemblement ! ». Super ! Alors je suis
au bon endroit, je devrais me sentir soulagé, content d’être avec
ces personnes et ces orgas avec qui nous allons pouvoir construire un
formidable front contre l’antisémitisme ! Pourtant, ce n’est pas
le cas, j’ai la boule au ventre en venant ici. Pourquoi ? Parce que ce
soir, la majorité des miens ne sont pas ici. Alors que le gouvernement,
par son instrumentalisation raciste et sécuritaire de l’antisémitisme
et sa politique de casse sociale entretient le terreau d’un ressentiment
populaire facilement exploité par les entrepreneurs antisémites, mes
frères et sœurs sont Place de la République. Pourquoi ?

Je crois qu’une majorité de juifs se représentent la gauche antiraciste
comme leur ennemi et je vois au moins trois raisons qui viennent expliquer
cela :

La première, c’est que la gauche ne croit pas les juifs. Plus
rapide pour dénoncer l’instrumentalisation de l’antisémitisme que
l’antisémitisme en lui même alors qu’il est en augmentation. Quand
des juifs parlent d’une augmentation de 74 % comme on le voit dans
les médias récemment, la gauche répond « mais que recouvre les
réalités de ce chiffre ? », « les médias mentent, le gouvernement
instrumentalise les juifs », « non c’est pas 74 mais 52 % ». Alors
que tout le monde sait que tous les racismes augmentent, la gauche
antiraciste ne nous croit pas quand nous disons simplement « nous
vivons de plus en plus de racisme ». Au mieux on nous dit « oui, mais
c’est moins que l’islamophobie », « on parle tout le temps de vous
». J’ai entendu ce soir beaucoup de paroles contre la hiérarchisation
des racismes pourtant quand on s’exprime en tant que juif sur le racisme
dans la gauche, on nous discrédite d’emblée si on ne commence pas par
« nous ne sommes pas ceux qui vivons le plus de racisme ». Imaginez un
seul instant deux personnes débattant de qui des rroms ou des asiatiques
sont les plus opprimés ? Quelle absurdité ! Voilà un bon moyen pour
affaiblir toutes les luttes contre le racisme ! La concurrence victimaire,
la concurrence des mémoires nous affaiblit tous !

La deuxième raison, c’est la notion problématique de « philosémitisme
d’État ». Quand je discute avec quelqu’un qui me parle de
philosémitisme d’État, je lui demande qu’est-ce que c’est ? Je
reçois des réponses qui s’inscrivent à l’intérieur d’un large
spectre de confusion. À une extrémité de ce spectre on me dit :
l’État instrumentalise la lutte contre l’antisémitisme pour mieux
taper sur les musulmans ou comme aujourd’hui les gilets jaunes. Dans
ce cas là, je dis d’accord c’est vrai, l’État fait mine de se
préoccuper des juifs alors qu’il s’en sert comme bâton pour mieux
taper sur le musulman ou le mouvement social. Mais puisqu’au fond,
il s’en bat les reins des juifs et nous utilise en faisant du même
coup monter les tensions contre nous, pourquoi ne pas tout simplement
appeler ça de l’antisémitisme ? A l’autre bout de ce spectre, on me
dit « philosémitisme d’État parce que les juifs sont privilégiés,
regarde le diner du CRIF, regarde la banque Rothschild, regarde Israël,
regarde la criminalisation de l’antisionisme », certains vont même
jusqu’à parler de « privilège juif ! ». Là je dis, mon pauvre,
dans quelle monde, dans quelle réalité historique tu vis ? C’est
carrément craignos comme croyance. Donc, même si on peut se comprendre,
pourquoi utiliser une notion qui conforte les préjugés antisémites
qui dit que les juifs sont du côté du pouvoir ?

La troisième raison, c’est ce que j’appelle l’injonction
géopolitique.  Vous savez, quand on est juif évoluant dans la gauche
antiraciste, on rase les murs. On préfère dire qu’on est vegan plutôt
que dire qu’on mange casher. Pourquoi ? Parce qu’en ramenant la
soi-disante épineuse « question juive » on va nous faire chier ! Très
souvent, quand je rencontre un militant de la gauche antiraciste, arrive
fatalement le moment ou il me demande avec un regard de travers « et tu
penses quoi du conflit israélo-palestinien ? », sous entendu « tu serais
pas un peu sioniste sur les bords ? ». En fait c’est ça : il faut
d’abord se justifier d’être antisioniste pour pouvoir fréquenter
la gauche, alors que comme moi, très peu de juifs ont une histoire
en commun avec Israël ! Ma mère est marocaine, mon père égyptien,
j’ai grandi en France, c’est quoi ce délire ? Est-ce qu’il vous
viendrait à l’esprit en rencontrant un arabe de lui demander son avis
sur la politique coloniale de l’Arabie Saoudite vis-à-vis du Yémen ?

En réalité, la gauche antiraciste semble beaucoup plus préoccupée par
les questions d’antisionisme que d’antisémitisme. Et ainsi, elle trie
les juifs ! Les juifs antisionistes avec qui il faut s’allier, et les
juifs sionistes qu’il faut combattre ! Il y a donc les bons juifs et les
mauvais !  Vous savez, l’antisémite, lui, est beaucoup plus tolérant,
il ne fait pas la distinction entre un juif sioniste et antisioniste,
ils sont juifs pareils pour lui !  Gabriel et Aryeh Sandler, trois et six
ans, filles et fils de Jonathan Sandler ainsi que Myriam Monsonégo, huit
ans – les victimes de la tuerie antisémite devant l’école de Ozar
Hatorah à Toulouse étaient-ils sionistes ? Ce n’est pas la question !!
Tout aussi absurde : Abel Chennouf, Mohamed Legouad et Imad Ibn Ziaten
– tués pendant le même attentat étaient-ils sionistes ? Ce n’est
pas la question !!  Si, ce soir, la gauche anti-raciste déclare vouloir
lutter contre l’antisémitisme, il va falloir cesser la solidarité
sélective, sinon il ne s’agit pas de lutte contre le racisme vécu par
les juifs mais d’une utilisation de la lutte contre l’antisémitisme
à d’autres fins.  Merci pour votre écoute. » Précisions

Dès la première phrase de mon intervention qui contient deux mots en
hébreu, j’entends « ha, ça commence bien » dans la tribune derrière
moi. Dans le public, ça hue, j’entends « Provocateur ! Sioniste
! ». Mon intervention terminée, je rends le micro et fais quelque pas pour
reprendre mes esprits, sortir de la stupeur de ce moment bizarre. Je me fais
assaillir par des gens qui veulent débattre, des gens qui veulent prendre
mon numéro, des gens pas d’accord, des gens venus me soutenir. Puis
j’entends ma camarade en train de parler. Plusieurs personnes hurlent
pour couvrir sa voix et tentent de lui arracher le micro des mains,
dans le public également « Ta gueule ! Ferme ta gueule, provocatrice
! » même si on entend aussi quelques applaudissements. Elle termine sa
prise de parole par « et si ça bouge dans la tête des gauchistes et
bien Mazal Tov comme on dit chez moi ! ».  Le micro à peine rendue,
une femme vient la voir : « j’ai beaucoup apprécié ce que t’as
dis mais je voudrais te dire quand même.... les juifs sont un groupe
fermé ». Une deuxième : « je voudrais te poser une question, tu as dit
"chez moi" mais c’est où ? c’est pas en France ? c’est en Israël
? ». Les débats deviennent houleux, on éloigne la personne. À côté,
un gars ricane, il dit « ici collectif quenelle 92 »... il se met sur
le petit muret et fait une quenelle. Une jeune juive l’invective avec
vigueur... Sur place, une partie des gens disent à cette dernière de se
calmer : « va régler ça en dehors de la manif avec lui »...  Le climat
et les incidents qui ont entouré nos prises de parole relèvent autant de
la crétinerie que de la haine viscérale des juif-ves. Le plus étonnant a
été la non-réaction absolue des organisateurs. En tant que juif engagé
dans les luttes sociales, il a été très difficile d’accéder à la
parole. Cette même gauche antiraciste ne prétend-elle pas valoriser la
parole et l’auto-organisation « des premiers/premières concerné-es »
? Les communiqués faisant suite à ce rassemblement font état d’un
succès total, je lis même que Ménilmontant serait « la capitale
de l’antiracisme ». Lutte-t-on contre le racisme en se contentant
d’énoncer ses désirs ?

Un juif communiste

Contact : unjuifcommuniste@riseup.net
