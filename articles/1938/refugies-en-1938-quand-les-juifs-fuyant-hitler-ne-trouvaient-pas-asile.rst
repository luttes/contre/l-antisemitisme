
.. index::
   pair: 1938; Refugiés


.. _refugies1938:

========================================================================================================
**Réfugiés : en 1938, quand les juifs fuyant Hitler ne trouvaient pas asile** par Chloé Leprince
========================================================================================================

- https://www.radiofrance.fr/franceculture/refugies-en-1938-quand-les-juifs-fuyant-hitler-ne-trouvaient-pas-asile-4628590


En Méditerranée, l'enlisement diplomatique est à son comble avec le refus
d'un port sûr pour les bateaux de secours en mer. En 1938 les
grandes puissances échouaient déjà à accueillir 600 000 réfugiés juifs,
allemands ou autrichiens, alors qu'un bateau errait dans les eaux
internationales du Danube.
