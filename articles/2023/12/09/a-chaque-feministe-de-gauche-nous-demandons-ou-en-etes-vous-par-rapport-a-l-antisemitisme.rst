.. index::
   pair: Feminisme ; A chaque féministe de gauche, nous demandons : où en êtes-vous par rapport à l’antisémitisme ? (2023-12-09)

.. _feminisme_2023_12_09:

===============================================================================================================================
2023-12-09 **A chaque féministe de gauche, nous demandons : où en êtes-vous par rapport à l’antisémitisme ?**
===============================================================================================================================

- https://notrecrideralliementfeministe.wordpress.com/
- https://notrecrideralliementfeministe.wordpress.com/2023/12/04/signataires-de-la-tribune/

Préambule
===============

Se sentant isolées, voire stigmatisées depuis les massacres
(**crimes contre l'humanité**, NDLR)  du 7 octobre 2023, la journaliste
Noémie Toledano et l’autrice Lisa Serero, rejointes par l’actrice
Agnès Jaoui, lancent, dans une tribune au "Monde", **un cri de
ralliement contre l’antisémitisme et toutes les discriminations**.

Si nous prenons aujourd’hui la parole, ce n’est pas pour critiquer
gratuitement les féministes de gauche, dont nous faisons fièrement partie.
Mais parce que depuis le massacre du 7 octobre 2023, les femmes françaises juives
ne se sentent pas assez soutenues par notre camp.

Le 7 octobre 2023 a accentué un sentiment déjà latent en France, celui
de la solitude quand on est juive.
Des féministes progressistes et des organisations ont bien pris la parole
pour dénoncer les faits, puis la flambée de haine qui a suivi en France,
mais ces voix ont été noyées par un discours médiatique et politique réactionnaire.

Remerciements
=================

- https://www.notaweaponofwar.org/
- https://www.notaweaponofwar.org/web-app-backup
- https://www.instagram.com/wearenotweaponsofwar/
- https://www.instagram.com/celinebardet/

Nous remercions celles qui ont dit leur soutien aux Israéliennes dès
que les crimes sexuels du Hamas ont été évoqués : les militantes de la
Fondation des femmes, la juriste `Céline Bardet <https://www.instagram.com/celinebardet/>`_, fondatrice de l’ONG
`We are not Weapons of War <https://www.notaweaponofwar.org/>`_, actuellement
en train de documenter ces crimes pour une prise en compte juridique
internationale, la députée écologiste Sandrine Rousseau et toutes les
autres, tout comme nous remercions les féministes qui manifestent leur
soutien aux femmes palestiniennes, victimes de la guerre et de la déshumanisation.


Contre la binarité dans laquelle nous sommes enfermées
=======================================================

Contre la binarité dans laquelle nous sommes enfermées, nous encourageons
chacun à penser les événements dans leur complexité.

On peut être en empathie avec les victimes du 7 octobre 2023 et avec les
victimes palestiniennes de la guerre, dont un nombre effarant de femmes
et d’enfants.

On peut se battre pour le droit des Palestiniens à un Etat sans vouloir
la destruction d’Israël.

On peut condamner la politique coloniale du gouvernement israélien
et les actes terroristes du Hamas.

**Malaise rampant**
=====================

Quoi qu’en disent actuellement la droite, l’extrême droite et ces
féministes dont l’universalisme est à géométrie variable, ces voix
existent.

**Elles doivent devenir plus nombreuses**.

Par ailleurs, nous entendons des féministes juives progressistes dire
qu’elles ne se sentent pas assez soutenues par notre mouvement, un
sentiment accru depuis le 7 octobre 2023.
**Certaines témoignent même de propos et d’actes antisémites dans nos
espaces. C’est intolérable**.

Nous nous félicitons que le féminisme se soit emparé des enjeux décoloniaux
et tente de défendre toujours plus de femmes, en France et dans le monde.
Pour autant, on ne peut accepter que la guerre entre Israël et le Hamas,
venue exacerber l’antisémitisme, exclue les femmes françaises juives de
nos combats contre le patriarcat.

Parce que ce malaise rampant ralentit notre projet politique : la fin des
violences et des discriminations pour toutes les femmes.
Parce que nous ne pouvons tolérer que la droite et l’extrême droite se
présentent comme seules défenseures des femmes juives, quand l’une n’a
que faire des droits des femmes et que dans les veines de l’autre coule
l’antisémitisme.

Un travail d’introspection

    Où en êtes-vous par rapport à l’antisémitisme ?
    Comment faire mieux ?
    Quelles solutions concrètes mettre en place pour que chaque femme
    juive se sente en sécurité, écoutée et soutenue face aux discriminations ?

Parce que trop d’opportunistes, qui jamais ne soutiennent un
féminisme antiraciste, contre l’islamophobie, le validisme [pression,
voire discrimination envers les personnes vivant avec un handicap], les
LGBTphobies…, tentent d’invalider nos combats.
Parce que nous sommes féministes et de gauche.

**Oppressions spécifiques**
=============================

Nous devons porter plus fort la lutte contre l’antisémitisme, au même
titre que tous les racismes et leur intersection avec les autres
discriminations.

A chaque féministe de gauche, à chaque organisation, nous demandons :

- une meilleure prise en compte des oppressions spécifiques envers les
  femmes juives, comme la peur d’arborer des signes religieux en public,
  la difficulté à obtenir un divorce religieux ou la crainte de dénoncer
  des violences quand on appartient à une communauté stigmatisée.
  Depuis le 7 octobre 2023, ce sont plus de 1 500 actes antisémites qui ont été
  enregistrés en France, soit l’équivalent de trois années d’actes
  antisémites. Des chiffres record et des violences qui s’abattent
  notamment sur les femmes juives.
- **une résistance à la tentation de simplifier des réalités complexes**.
  Malgré leurs traits communs, toutes les histoires de colonisation, de
  massacres et de guerres ne sont pas identiques.
  La lutte contre les oppressions, en particulier, les violences de genre
  et leur intersection avec les racismes, mérite mieux que des analyses à
  gros traits, qui empêchent de comprendre l’histoire des peuples et des
  nations.
  Les juifs, comme tout groupe humain, ont le droit d’être vus dans leur complexité.
- Prendre conscience qu’il existe aussi, dans tous les camps politiques,
  dont le nôtre, des discours de déshumanisation à l’encontre des Israéliens
  et des juifs, souvent perçus comme détenteurs de pouvoir.
  Au même titre que les récits dominants visent à déshumaniser Palestiniens,
  Arabes et musulmans, nous renvoyons dos à dos celles et ceux qui en sont coupables.

**En finir avec l’instrumentalisation de la souffrance**
==========================================================

Par ailleurs, nous souhaitons que les autorités israéliennes collaborent
avec les instances onusiennes et soutiennent les organisations indépendantes
qui enquêtent actuellement sur les exactions commises par le Hamas contre des
femmes, afin qu’elles soient reconnues juridiquement.

Nous demandons la fin de l’instrumentalisation de la souffrance des
victimes du 7 octobre 2023 à des fins politiques, plus précisément, le
non-respect du droit international et la justification de crimes contre
des civils palestiniens.

Nous, féministes de gauche, devons nous montrer à la hauteur de la situation.

**Nous devons montrer avec plus de force à nos sœurs juives qu’elles ont
toute leur place dans nos rangs**.

Nous devons écouter et croire leurs vécus.

Nous devons accepter de regarder en face la tentation antisémite et la
combattre de toutes nos forces, comme nous combattons l’islamophobie,
le racisme, la transphobie, le validisme et toutes les discriminations qui
s’ajoutent à celles que nous vivons déjà en tant que femmes.

Premières signataires
==========================

Aurélia Blanc, journaliste et autrice ; Agnès Jaoui, actrice, scénariste
et réalisatrice ; Jessica Mwiza, militante de la mémoire ;
Blanche Sabbah, militante féministe et autrice de BD ;
Fiona Schmidt, autrice et militante féministe ; Lisa Serero, militante et autrice ;
Noémie Toledano, secrétaire générale de We Sign It ;
Victoire Tuaillon, autrice et journaliste, créatrice du podcast "Les couilles sur la table" ;
Eva Vocz, chargée de plaidoyer d’Act Up-Paris ; Illana Weizman, essayiste.

Retrouvez la liste complète des signataires sur `ce lien (https://notrecrideralliementfeministe.wordpress.com/2023/12/04/signataires-de-la-tribune/) <https://notrecrideralliementfeministe.wordpress.com/2023/12/04/signataires-de-la-tribune/>`_.
