
.. _le_neve_2023_12_01:

===============================================================================================================================
2023-12-01 **L’ombre de l’antisémitisme fracture la communauté étudiante dans les universités**  par Soazig Le Nevé
===============================================================================================================================


- https://www.lemonde.fr/societe/article/2023/12/01/a-l-universite-l-ombre-de-l-antisemitisme-fracture-la-communaute-etudiante_6203293_3224.html
- https://www.lemonde.fr/signataires/soazig-le-neve/

Introduction
==============

Des dizaines de faits relevant de l’incitation à la haine envers les juifs
ont été relevés dans les universités depuis l’attaque terroriste du Hamas.

Sur les campus, des collectifs d’extrême gauche revendiquent leur
« antisionisme » au nom de l’« anticolonialisme ».
