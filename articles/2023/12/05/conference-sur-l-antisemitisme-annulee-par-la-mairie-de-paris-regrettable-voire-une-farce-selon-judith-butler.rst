

.. _magnaudeix_2023_12_05:

===============================================================================================================================================================
2023-12-05 **Conférence sur l’antisémitisme annulée par la mairie de Paris :"regrettable, voire une farce", selon Judith Butler** par Mathieu Magnaudeix
===============================================================================================================================================================

- https://www.mediapart.fr/journal/france/051223/conference-sur-l-antisemitisme-annulee-par-la-mairie-de-paris-regrettable-voire-une-farce-selon-judith-but
- https://www.mediapart.fr/biographie/mathieu-magnaudeix


Conférence sur l’antisémitisme annulée par la mairie de Paris : «regrettable, voire grotesque", selon Judith Butler 5 décembre 2023 Par Mathieu Magnaudeix
=============================================================================================================================================================

La mairie de Paris a annulé un événement"contre l’antisémitisme et
son instrumentalisation" prévu le 6 décembre 2023, dont la philosophe du genre
étasunienne était l’oratrice principale.

Raison invoquée : la présence, parmi les organisateurs, de l’association
de la militante décoloniale Houria Bouteldja.
À l’hôtel de ville, cette annulation motivée par de possibles"troubles à l’ordre public"
ne convainc pas tout le monde.

Tout était prévu. Judith Butler, théoricienne queer étasunienne mondialement
connue, intellectuelle juive de gauche critique de la colonisation israélienne,
chevalier des Arts et lettres actuellement accueillie avec les honneurs
au Centre Pompidou, devait prendre la parole.
L’activiste et philosophe afro-américaine Angela Davis, icône antiraciste
récemment accueillie au Festival d’Automne à Paris, avait enregistré un message vidéo.

Tout était prévu, mais la soirée"contre l’antisémitisme, son instrumentalisation
et pour la paix révolutionnaire", annoncée pour jeudi 6 décembre au Cirque
électrique, dans le XXe arrondissement de Paris, n’aura pas lieu.
**En tout cas, pas dans le lieu prévu.**

Ce week-end, la mairie de Paris, qui finance le lieu à hauteur de 160 000 euros
par an selon la ville, a débranché l’événement, initié par l’appel
collectif"Guerre permanente ou paix révolutionnaire" publié en 2021
sur Mediapart, et coorganisé par les organisations juives décoloniales
UJFP (Union juive française pour la paix) et Tsedek, l’Action antifasciste
Paris-banlieue, Révolution permanente, le Nouveau Parti anticapitaliste (NPA)
et Paroles d’honneur.

On adore Judith Butler", balaie Aurélie Filippetti
=========================================================

Judith Butler , qui a"condamné sans la moindre réserve les
violences commises par le Hamas", aurait-elle été censurée pour d’anciens
propos sur le parti islamiste, comme le suggère Libération ?"On adore
Judith Butler", balaie Aurélie Filippetti, ancienne ministre de la culture
socialiste, désormais directrice des affaires culturelles de la ville de Paris.



Tout à voir, en revanche, avec la présence, parmi les organisateurs de la soirée, de Paroles d’honneur
=========================================================================================================

- https://linktr.ee/Parolesdhonneur

À l’entendre, l’annulation n’a donc rien à voir avec la philosophe
étasunienne, mais a tout à voir, en revanche, avec la présence, parmi les
organisateurs de la soirée, de Paroles d’honneur, un collectif et un média
liés à la militante décoloniale Houria Bouteldja, autrice notamment de Les
Blancs, les juifs et nous. Vers une politique de l’amour révolutionnaire
(éd. La Fabrique, 2016).

À cause de certains de ses écrits, Bouteldja, une voix influente dans la
sphère décoloniale, dont les livres sont traduits à l’étranger, est persona
non grata dans une grande partie du champ médiatique et politique français.

En cause : son évocation, entre autres, d’un"philosémitisme d’État
" ou le fait d’avoir écrit qu’"on ne peut être israélien innocemment
" (le texte en question avait été retiré du Club de Mediapart). Houria
Bouteldja a par ailleurs dénoncé un"impérialisme gay" et estimé que
l’"indigène" gay qui fait son coming-out est un"héros à deux balles
". Elle a également jugé que le féminisme occidental est"un mot d'ordre
conçu par et pour des féministes blanches".

Discussion publique réfléchie
======================================

"La ville de Paris refuse d’être associée à l’organisation d’Houria Bouteldja,
explique Aurélie Filippetti à Mediapart.
**Elle est homophobe, antiféministe et antisémite."**

L’ancienne ministre de la culture dit assumer une "décision conjointe" avec
l’adjointe d’Anne Hidalgo à la culture, Carine Rolland.
Sollicitée par Mediapart mardi matin sur les propos d'Aurélie Filippetti,
**Houria Bouteldja n’a pas répondu**.

Aurélie Filippetti dit avoir expliqué ce week-end cette décision à Judith
Butler.
**"Judith Butler m’a dit que c’était normal que la ville, comme les collectivités
en général, mette des limites."**

Pourtant, la philosophe assure à Mediapart qu’elle a passé d’autres
messages à l’ancienne ministre de la culture de François Hollande : «
J’ai aussi dit qu’il n’est pas raisonnable d’exiger que des espaces
artistiques subventionnés par le public reflètent les points de vue des
autorités, et elle a semblé d’accord."

Selon la théoricienne du genre, la discussion avec Aurélie Filippetti a aussi
porté sur Houria Bouteldja."J’ai signalé que la personne à laquelle elle
s’oppose n’allait pas prendre la parole lors de l'événement.

Je lui ai dit que mes idées n’étaient pas les mêmes que les siennes.
======================================================================

Je lui ai dit que mes idées n’étaient pas les mêmes que les siennes.
J’ai aussi fait remarquer qu’il est important, en ces temps où beaucoup de slogans
et d’explications réductrices circulent à grande vitesse, que se tienne
une discussion publique réfléchie entre des gens qui partagent des points
de vue différents."

De cet épisode, la philosophe tire une leçon :"La ville de Paris entretient
une dispute avec quelqu’un qui n’allait pas parler, et a fini par annuler
quelqu’un qui allait parler.
En cela, cette annulation a un caractère regrettable, voire grotesque."

"Victoire !", a salué ironiquement Houria Bouteldja sur son compte
Facebook."Le vœu des juives et juifs révolutionnaires (dont je rappelle
qu’ils sont les nouveaux copains de Mediapart) exaucé ! Cette rencontre
est annulée par la Mairie de Paris", a ajouté l’ancienne porte-parole
du parti des Indigènes de la République (PIR). La militante fait référence
aux JJR, une organisation de juives et juifs de gauche.

Dimanche, les JJR avaient posté sur Instagram un message dénonçant
"les groupes UJFP et Tsedek", accusés d’"instrumentalis[er]"
l’antisémitisme"à des fins politiques douteuses", et d’être
"régulièrement utilisés comme alibis […] pour diffuser des discours
antisémites ou silencier toute tentative de dénonciation de l’antisémitisme
lorsque celui-ci s’exprime dans certains secteurs de la gauche".

:ref:`Des membres des JJR ont été interrogés sur Mediapart depuis le 7 octobre (le 9 novembre 2023, NDLR) <video_2023_11_09>`,
tout comme Tsedek et d’autres voix décoloniales.

Judith Butler

	La ville de Paris entretient une dispute avec quelqu’un qui n’allait pas
	parler, et a fini par annuler quelqu’un qui allait parler.

Les massacres du 7 octobre et la guerre au Proche-Orient ont réactivé un
conflit politique ancien.

`En octobre (le 25 octobre 2023, NDLR) <https://juivesetjuifsrevolutionnaires.wordpress.com/2023/10/25/la-librairie-terra-nova-victime-de-son-confusionnisme/>`_
les JJR ont"salu[é]" un collage réalisé sur une librairie toulousaine où était invitée Houria
Bouteldja, aux côtés de Louisa Yousfi, autrice de Rester Barbare (éd. La
Fabrique).

Selon les JJR, ce collage ("Souviens-toi Hozar Ha Torah") se
référait" **au texte abject d’Houria Bouteldja** quelques jours après les
tueries antisémites de Mohammed Merah", terroriste qui avait assassiné en
2012 sept personnes à Toulouse et Montauban, dont trois enfants juifs devant
une école juive de Toulouse.

Maison d’édition des deux autrices, La Fabrique avait dénoncé un «
pur délire","une agression qui s’insère dans une campagne sournoise
contre les militant·es décoloniaux, régulièrement insultés et empêchés
de s’exprimer par des adversaires qui préfèrent les diaboliser que de se
confronter sur le terrain du débat".

"La mairie de Paris a-t-elle besoin d’un chiffon rouge qui s’appelle
Houria Bouteldja ? C’est un faux procès, s’insurge Michèle Sibony, membre
de l’Union juive française pour la paix, une des structures organisatrices
de l’événement annulé. Elle n’est pas présente dans l’organisation,
pas prévue parmi les orateurs, elle n’est pas au programme."


Sibony, cosignataire avec des intellectuel·les de gauche de tribunes de soutien à Houria Bouteldja, en 2017 et en 2022
==========================================================================================================================

"Il y a visiblement un haineux quelque part dans la ville de Paris que
l’ombre de Bouteldja fait flipper. Nous, nous ne sommes ni dans la chasse
aux sorcières, ni dans le sectarisme", ajoute Sibony, cosignataire avec
des intellectuel·les de gauche de tribunes de soutien à Houria Bouteldja,
en 2017 et en 2022.

"Il y a des manifestations d’extrême droite autorisées à Paris.
La manif contre l’antisémitisme s’est faite avec l’extrême droite.
Ça n’a pas l’air de déranger la mairie de Paris. Et au-delà de la France,
de très nombreux Juifs sont en train de se manifester sur cette question de
l’antisémitisme et de son instrumentalisation pour faire taire sur ce qui
se passe à Gaza", complète Michèle Sibony, citant Jewish Voice for Peace,
dont Judith Butler est membre, et qui mobilise actuellement aux États-Unis
contre le soutien de l’administration Biden à l’armée israélienne.


Sur Révolution permanente
=============================

Sollicitée par Mediapart lundi, la ville de Paris a d’abord envoyé
un communiqué qui ne citait pas Houria Bouteldja mais "les positions
politiques et sociétales extrémistes, connues et réaffirmées" de certaines
"organisations" comme Révolution permanente, issue d’une scission du
NPA, qui a soutenu Anasse Kazib à l’élection présidentielle.
Et, plus curieusement, de "Paix révolutionnaire", un mouvement qui n’existe pas.

la lutte contre l’antisémitisme et le racisme, contre les inégalités femmes-hommes et la [sic] contre l’homophobie font partie des valeurs cardinales" de la ville
=====================================================================================================================================================================

Ce communiqué rappelait que "la lutte contre l’antisémitisme et le racisme,
contre les inégalités femmes-hommes et la [sic] contre l’homophobie font
partie des valeurs cardinales" de la ville."

Lors du débat du 6 décembre, le risque est majeur que des propos soient tenus
qui contreviennent à ces principes non négociables, poursuivait ce texte.
Par ailleurs les polémiques inévitables qui s’ensuivront seront de nature
à troubler l’ordre public."

Le précédent de la Flèche d’Or
=====================================

Dans la majorité d’Anne Hidalgo, certain·es élu·es s’en sont étonné·es.

"Dangereux et consternant", a commenté sur X la conseillère de Paris Alice Coffin,
membre du groupe écologiste.

Un élu de gauche du XXe arrondissement, où est installé
le Cirque électrique, s’interroge, lui, sur le procédé."Pourquoi
la ville intervient-elle dans la programmation de lieux qu’elle a en
conventionnement ? Elle pourrait intervenir si le préfet de Paris évoquait
un trouble à l’ordre public. Ce n’est pas le cas : c’est la ville qui
s’est auto-saisie en évoquant un trouble à l’ordre public. Le risque
est celui d’une glissade : que des événements un peu moins normés, un
peu plus radicaux, un peu moins mainstream ne puissent plus se tenir dans des
lieux subventionnés par la ville. C’est regrettable, surtout au moment où
l’extrême droite exerce des pressions en ce sens."

À l’hôtel de ville, l’affaire rappelle un précédent, lorsque l’adjointe
à la culture, Carine Rolland, avait dit son désaccord à un concert de soutien
"aux familles des jeunes interpellés" organisé en juillet à la Flèche
d’Or, un autre lieu du XXe arrondissement, juste après les révoltes qui
avaient suivi la mort de Nahel Merzouk, tué par un policier à Nanterre. La
Flèche d’Or avait annulé l’évènement.

D’autres y voient une prise de position révélatrice dans un contexte
politique bouillonnant depuis le 7 octobre. Le 14 novembre, en plein conseil
de Paris, après qu’un élu insoumis, Laurent Sorel, avait parlé d’un «
crime de guerre à Gaza", Anne Hidalgo s’était dite "heureuse" de ne
pas compter La France insoumise (LFI) dans sa majorité.

"Toutes les organisations participantes posaient problème", explique ainsi
à Mediapart Robert Lacombe, sous-directeur de la création artistique et adjoint
d’Aurélie Filippetti, qui a communiqué la décision de la mairie au Cirque
électrique.
"Le débat peut avoir lieu dans un autre lieu parisien adapté
à ce type de prise de parole publique “engagée”, dit-il.


Sur **les prises de position présentent des actes de barbarie à l’encontre de civils comme des formes légitimes de résistance**
=================================================================================================================================

En revanche, un établissement lié à la mairie de Paris, ville elle-même durement
frappée par le terrorisme, ne peut être perçu comme engagé aux côtés de
coorganisateurs visés par une enquête pour “apologie du terrorisme”, ou
dont les prises de position présentent des actes de barbarie à l’encontre
de civils comme des formes légitimes de résistance."

À l’appui de ses propos, Robert Lacombe cite:

- le communiqué de Révolution permanente du 7 octobre, le jour de l’attaque
  du Hamas, qui évoquait "la force morale" du peuple palestinien ;
- celui de Tsedek, publié sur le site de l’UJFP et datant du même jour,
  qui estimait que"si la résistance palestinienne à la colonisation israélienne
  peut prendre des formes violentes, ce n’est jamais aveuglément".

Porte-parole de Tsedek, Simon Assoun rappelle qu’après son premier communiqué,
son organisation a "condamné, le 12 octobre, de la plus ferme des manières,
les crimes du Hamas contre les civils israéliens".


Sur Tsedek et la "republication" d’une « tribune qui relativise les viols du Hamas"
==============================================================================================

Robert Lacombe reproche aussi à **Tsedek la "republication" d’une «
tribune qui relativise les viols du Hamas", désormais étayés**.

Il vise aussi le texte du NPA, toujours datés du 7 octobre, où le parti de gauche
radicale rappelait son"soutien aux Palestinien·nes et aux moyens de lutte
qu’ils et elles ont choisis pour résister".


Le NPA, rappelle Lacombe, est visé par une enquête pour "apologie du terrorisme"
====================================================================================

Le NPA, rappelle Lacombe, est visé par une enquête pour "apologie du
terrorisme", annoncée par Gérald Darmanin lui-même – une incrimination
"scandaleu[se]", selon Manuel Bompard, coordinateur de LFI.

Mardi midi, l’hypothèse d’un lieu de repli pour la conférence semblait
abandonnée."Vu la menace qu’a fait peser la mairie sur le Cirque
électrique, les formulations accusatrices qu’elle a lancées sans même
chercher à vérifier avec nous ce que nous prévoyions, en spéculant en
avance sur des polémiques, c’est un débat intellectuel qui est a priori
interdit, regrette la politologue et militante décoloniale Françoise Vergès,
qui a participé à l’organisation de l’événement.
Vu tout cela, nous n’avons pas voulu mettre des espaces en danger."


Boite noire
==================

Voici les mots exacts en anglais de Judih Butler, envoyés par mail mardi 5 décembre.

Sur l'annulation de l’événement : « I also said that it was not reasonable
to demand publicly-funded arts spaces to replicate the viewpoints of the
government, and she seemed to agree. »

Sur Houria Bouteldja : « I pointed out, however, that the person to whom
she objects was not speaking at the event, and that my own views are not
the same as hers.

I also pointed out that a thoughtful public discussion among people with
different points of view is important during times such as these when
many slogans and reductive explanations circulate at great speed.

The mayoral office is having a quarrel with someone who was not speaking,
but has ended up cancelling someone who was to speak.

In this way, the cancellation has an unfortunate, if not farcical, quality. »

Correction, mardi 6 décembre, 23h45. J’avais choisi de traduire l'adjectif
« farcical » par « grotesque » comme l'indiquent plusieurs dictionnaires.

Judith Butler, après la parution de l'article, a préféré l'emploi du mot
« farce », plus proche de la « comédie ».
