.. index::
   ! leftrenewal

.. _leftrenewal_2023_12_10:
.. _leftrenewal_en_2023_12_10:

========================================================================================================================================
2023-12-10 leftrenewal **For a consistently democratic and internationalist left A contribution to left renewal and transformation**
========================================================================================================================================

- :ref:`raar_2023:leftrenewal_en_2023_12_10`
