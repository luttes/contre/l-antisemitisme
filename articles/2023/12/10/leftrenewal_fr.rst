.. index::
   pair: Pour une gauche démocratique et internationaliste ; 2023-12-10

.. _leftrenewal_fr_2023_12_10:

=================================================================================================================================================
2023-12-10 leftrenewal **Pour une gauche démocratique et internationaliste Une contribution au renouveau et à la transformation de la gauche**
=================================================================================================================================================

- :ref:`raar_2023:leftrenewal_fr_2023_12_10`
- :ref:`raar_2023:leftrenewal_en_2023_12_10`
