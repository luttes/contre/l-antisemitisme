.. index::
   pair: Ghetto de Varsovie ; 2023-04-16

.. _raar_varsovie_2023_04_16:

===============================================================================
2023-04-16 **Ghetto de Varsovie, 80 ans : "Mir zaynen do", Nous sommes là !**
===============================================================================

- :ref:`Les informations ont été tranférées sur le site https://antiracisme.frama.io/infos-2023 <raar_2023:raar_varsovie_2023_04_16>`
