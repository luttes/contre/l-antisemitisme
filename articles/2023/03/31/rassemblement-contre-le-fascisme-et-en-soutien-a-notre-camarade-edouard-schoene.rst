.. index::
   pair: Antisémitisme; Édouard Schoene

.. _edouard_2023_03_31:

======================================================================================================
2023-03-31 **Rassemblement contre le fascisme et en soutien à notre camarade Édouard Schoene**
======================================================================================================

- https://www.placegrenet.fr/2024/03/05/injures-et-menaces-antisemites-la-plainte-dedouard-schoene-classee-sans-suite/624935
