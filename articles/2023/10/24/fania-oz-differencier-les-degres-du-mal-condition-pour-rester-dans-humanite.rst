.. index::
   pair: Fania Oz ; 2023-10-24

.. _fania_oz_2023_10_24:

==================================================================================================
2023-10-24 **Fania Oz Différencier les degrés du mal, condition pour rester dans l'humanité**
==================================================================================================

- https://peertube.iriseden.eu/w/kWXy5JxHn1P5cDghMeBz7p
- https://www.kan.org.il/content/kan/podcasts/p-569737/580979/
- https://en.wikipedia.org/wiki/Fania_Oz-Salzberger

Nous commençons avec la prof et historienne Fania Oz Salzberger


::

    Harvard combia qui refusent de critiquer les actions du Hamas
    On a un peu envie de crier, « je suis en colère vis-à-vis de personnes
    dont je pensais qu’elles étaient comme moi »
    J’ai un compte twiter qui arrive à des 100 de milliers de personnes
    souvent identifiées à la gauche

    Toute personne qui s’identifie à la gauche n’est pas de gauche
    Comme un troupeau automatique derrière le Hamas
    Judith Butler

Mes héros culturels suivent cela souvent

**Il y a une paresse intellectuelle dans la gauche européenne**
Je veux d’abord parler de la colère qui nous prend il y a parfois des
réactions lourdes
Il est important de voir qu’une grande part des progressistes aux usa
par exemple chaine twiter washington express, ils soutiennent israel
et non le hamas, la gauche humaniste Allemagne, France et UK soutiennent
israel et non le Hamas

Dans les artistes et l’académie ont décidé que s’identifier avec les
palestiniens est égal à s’associer au Hamas (elle critique cela).
Free Palestine signifie en réalité, tue israel
Je veux me battre pour que ceux qui se définissent c la gauche humaniste
comprennent, j’ai bcp d’amis dans le monde de la culture allemand et
dans les universités et je leur parle
Toute personne qui crée le narratif allemand se tient du côté d’israel,
les personnes humanistes

Tu as perdu des ami.es ?

Regarde je suis la fille de mon père qui a dit qu’il faut différencier
entre les degrés du mal et que celui qui ne le fait pas contribue au mal
Certains artistes israéliens publient pour le Hamas, ils n’ont pas fait
partie de mes amis, mais d’autres personnes sont perdues et il faut
s’en occuper, des européens et des américains.

**Il n’y a pas de différence entre un enfant palestinien et israélien,
mais il y a une différences énorme entre les personnes qui ont causé
ces morts, les personnes qui ne comprennent pas cela sont les
partenaires du mal**.

Clarté intellectuelle renouvelée nécessaire, différencier les personnes
innocentes, celles qui ne le sont pas etc..

Quand tu vois des femmes, des homos qui défendent daech ?

Ma premiere réaction est d’entrer dans une crise de colère, après il
faut dire là aussi il faut garder son sang froid, je me tourne vers
toutes les personnes que je connais dans le monde du brésil à la Californie
pour faire cette transmission morale qui est nécessaire, **qu’en fin de
compte il n’est pas possible que le Hamas reste, les vies des innocents
sont importantes mais quand le couteau est sous ma gorge, la vie de mes
enfants est plus importante**, en fin de compte il faut différencier
quand le couteau est sous ma gorge et je n’ai pas le choix et quand
j’ai le choix et que je dois tout faire pour défendre des innocents.

Nous faisons de mieux pour développer une philosophie morale, il faut
en créer une très fine. C’est un combat, tout en cheminant nous essayons
de créer cette pensée, et on la
partage avec la gauche humaniste qui est prête à entendre, et je veux
encore rappeler qu’une grande partie des aimants du Hamas dans les
réseaux sociaux ne sont pas de gauche en aucune façon, ce sont des
fachistes enfants de fachistes

Même si ils se considèrent de gauche…

**Et ils n’ont aucune idée de ce qu’est la gauche = social démocratie,
humanisme, chercher la paix autant que possible**. Une partie d’eux sont
perdus infiniment qui essaient de penser qu’ils sont du bon côté.

**Ils représentent une paresse intellectuelle incroyable**.

On te dit merci,


Je veux revenir sur la citation de mon père il y a 10 ans amos oz :

**Celui qui ne fait pas la différence entre les degrés du mal finira
par devenir un serviteur du mal**

**Il est nécessaire de différencier aujourd’hui entre les degrés du mal
est c’est une exigence premiere pour toute personne qui se considère
comme gauche morale ou être humain moral**
