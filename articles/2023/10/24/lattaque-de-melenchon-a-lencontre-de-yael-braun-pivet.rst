.. index::
   pair: Mélenchon ; 2023-10-24

.. melenchon_2023_10_24:

============================================================================
2023-10-24 **L’attaque de Mélenchon à l’encontre de Yaël Braun-Pivet**
============================================================================

- https://raar.info/2023/10/melenchon-braun/
- https://raar.info/tag/melenchon-2/

**L’attaque de Mélenchon à l’encontre de Yaël Braun-Pivet joue avec
l’imaginaire antisémite**.

Le nouveau tweet de Jean-Luc Mélenchon à l’encontre de Yaël Braun-Pivet
(« Madame Braun-Pivet campe à Tel-Aviv pour encourager le massacre.

Pas au nom du peuple français ! ») a recours à un trope antisémite évident.

Le choix de la dénonciation spécifique de cette personnalité d’origine juive,
dont il est de notoriété publique qu’elle a subi plusieurs attaques
antisémites, en témoigne clairement.

Pour rappel, Yaël Braun-Pivet n’a pas « campé » en Israël où elle aurait
pris ses quartiers pour « encourager le massacre ».

**Cette phrase, qui situe les Juif/ves du côté de la traîtrise, du pouvoir
et du meurtre des innocent.es, fait directement écho à la prose antisémite
et à son très ancien imaginaire**.

En désignant la Présidente de l’Assemblée Nationale comme pas vraiment
« française » et en insinuant qu’elle serait téléguidée par une influence
étrangère, Mélenchon ravive l’accusation bien connue de la « double loyauté »
des Juif/ves.

Nous dénonçons ce message et le consentement de ceux et celles qui ne
voient pas où est le problème.

C’est un pas supplémentaire dans la série des dérapages calculés du
chef de la FI, déjà relevés et condamnés par le RAAR.

Sur le site retrouvez les réactions du RAAR face aux précédentes
déclarations à caractère antisémite de Mélenchon, notamment à propos
de Zemmour, des enfants victimes de M. Merah, des Juif/ves « déicides »
et du CRIF.
