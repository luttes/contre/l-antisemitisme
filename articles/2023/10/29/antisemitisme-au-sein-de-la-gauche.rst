

====================================================================================================================================================================
2023-10-29 **Le second phénomène, l’antisémitisme au sein de la gauche, nourrit les rapprochements et dangers les plus corrosifs à force d’être nié par celle-ci**
====================================================================================================================================================================

- https://cabrioles.substack.com/p/un-cas-decole-de-genocide-raz-segal


Le second phénomène, l’antisémitisme au sein de la gauche, nourrit
les rapprochements et dangers les plus corrosifs à force d’être nié
par celle-ci.

Nous avons vu de larges pans de la gauche et des mouvements révolutionnaires
défilés aux côté d’antisémites assumés, prendre leur défense, relativiser
le génocide des Juifves d’Europe.

Nous avons vu nombres de camarades se rapprocher de formations fascisantes
en suivant cette voie.

À travers l’antisémitisme la déshumanisation des Juifves opère en en
faisant non un rebut mais un groupe prétendument homogène qui détiendrait
le pouvoir, suscitant des affects de haine d’autant plus féroces.

Ces deux phénomènes ont explosé ces dernières semaines.
À l’animalisation des palestinien·nes en vue de leur nettoyage ethnique
est venue répondre la culpabisation par association de toute la population
israélienne, si ce n’est de tous les Juifves de la terre, aux massacres
perpétués par le gouvernement d’extrême-droite de l’État d’Israël et
les forces capitaliste occidentales.

La projet de colonisation de la Palestine est né des menées impérialistes
de l’occident capitaliste et de l’antisémitisme meurtrier de l’Europe.
Ils ne pourront être affrontés séparément.
Les forces fascisantes internationales qui prétendent désormais sauver
le capitalisme des désastres qu’il a produit par un nationalisme et un
suprémacisme débridé, se nourrissent de l’intensification de tous les
racismes - islamophobie, antisémitisme, négrophobie, antitsiganisme,
sinophobie…- en vue de capturer les colères et de désigner comme surplus
sacrifiables des parts de plus en plus larges de la population.

En France l'extrême-droite joue habilement de l’islamophobie et de
l’antisémitisme structurels, présents jusque dans les rangs de la
gauche radicale, en potentialisant leurs effets par un jeu de miroirs
explosifs.

Face à cela il nous faut un front uni qui refuse la déshumanisations
des morts et des otages israelien·nes tout en attaquant le système
colonial qui domine et massacrent les palestinien·nes.
Il nous faudra également comprendre l’instrumentalisation historique des
Juifves et de l’antisémitisme par l’impérialisme occidental dans la
mise en place de ce système.

Nous n’avons pas trouvé les forces pour faire ce dossier.
Nous republions donc ce texte important de l’historien israélien Raz Segal
paru il y a maintenant deux semaines dans la revue Jewish Current.

Deux semaines qui semblent aujourd’hui une éternité. Il nous faut nous
organiser pour combattre de front la montée incendiaire de l’antisémitisme
et de l’islamophobie. Et faire entendre haut et fort :

Un génocide est en cours en Palestine.
Tout doit être fait pour y mettre un terme.
