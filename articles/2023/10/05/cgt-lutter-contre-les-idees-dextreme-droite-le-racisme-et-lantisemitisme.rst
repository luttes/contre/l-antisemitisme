.. index::
   pair: CGT ; 2023-10-05

.. _cgt_2023_10_05:

===============================================================================================
2023-10-05 CGT **Lutter contre les idées d’extrême droite, le racisme et l’antisémitisme**
===============================================================================================

- https://www.cgt.fr/actualites/france/interprofessionnel/extreme-droite/lutter-contre-les-idees-dextreme-droite-le-racisme-et-lantisemitisme
- :ref:`compte_rendu_cgt_2023_11_09`
- :ref:`sophie_binet_2023_11_09_partie_2525`

Présentation
====================


**Les idées d’extrême droite se banalisent**.

L’imposture sociale véhiculée par les partis d’extrême droite se propage
dans les entreprises, collectivités et services.

**Le monde du travail n’échappe pas à cette réalité**.

L’instrumentalisation de la précarité, l’absence de réponse aux besoins
des travailleur·ses nourrit le terreau de l’extrême droite et engendre
la banalisation de ses idées.

La plupart des pays européens sont également touchés par cette montée
des idées d’extrême droite avec à la clé des alliances avec la droite
(Italie, Suède, Finlande, peut-être l’Espagne dans quelques semaines),
des partis néo-fascistes, xénophobes, populistes, accèdent au pouvoir.

Le risque est grand de voir l’extrême droite encore progresser lors des
élections européennes de juin 2024 et devenir ainsi une force centrale
du Parlement européen.

L’inquiétude de l’accession au pouvoir de l’extrême droite à la présidentielle
est grande.
Les attaques de l’extrême droite se multiplient, en particulier sur les
réseaux sociaux et la fachosphère.

Reprendre l’offensive contre les idées d’extrême droite
-------------------------------------------------------------

**Lors de son congrès, en mars 2023, la CGT a réaffirmé son engagement
dans la lutte contre l’extrême droite et dans le combat contre le racisme
et l’antisémitisme**.

**Elle en a fait une priorité absolue**.

Dans ce cadre, la direction confédérale a décidé de lancer le plan de
travail CGT avec la tenue d’un colloque « la CGT résolument à l’offensive
dans la lutte contre les idées Extrême droite – Non au racisme et
Non à l’antisémitisme », le 5 octobre à Montreuil. (⏬️ voir programme détaillé)

La première table ronde sera consacrée aux matrices de la construction
des idées d’extrême droite et de ses diverses composantes : le racisme,
le rejet de l’autre, la peur de l’étranger. Le concept du préférence
national, base du programme économique du RN, en est un exemple frappant.

Les intervenants de la deuxième table ronde reviendront sur l’engagement
de la CGT pour faire barrage à la montée des idées d’extrême droite et
aux moyens de déconstruire celles-ci au quotidien dans les lieux de travail.

Des militants de la CGT exprimeront leur vécu, et ce sera l’occasion
de partages d’expériences avec des associations de défense des droits,
et de lutte contre le racisme.

A l’occasion de ce colloque, la CGT inaugurera l’esplanade Henri Krasucki
en hommage à l’ancien secrétaire général de la CGT de 1982 à 1992,
engagé très jeune face dans la résistance face au nazisme et à
l’extrême droite française durant la Seconde Guerre mondiale.

Programme
============

:download:`Le programme au format PDF <pdfs/programme_contre_extreme_droite_2023_10_05.pdf>`

8 h 30 – Accueil
--------------------

9 h 00 – Mot d’ouverture et présentation de la journée, Nathalie Bazire, secrétaire confédérale
----------------------------------------------------------------------------------------------------


9 H 15 – « Le racisme, l’antisémitisme, le rejet de l’autre, matrices de l’extrème droite » – table ronde suivie d’un débat, animée par Nathalie Bazire, secrétaire confédérale, et Emmanuel Vire, CEC
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- Origine et définition du racisme et de l’antisémitisme ; Jérôme Beauvisage, IHS-CGT.
- L’imposture sociale et le racisme (« la préférence nationale ») dans
  les programmes économiques de l’extrême droite ; Pierre Khalfa,
  Fondation Copernic, membre du conseil scientifique d’Attac
- L’antisémitisme contemporain ; Albert Herszkowicz, RAAR
  (Réseau d’actions contre l’antisémitisme et tous les racismes)
- Le racisme structurel, un mécanisme de discrimination et d’exclusion ;
  Mornia Labssi, coordination pour la défense des habitants des quartiers populaires

12 h 00 – Inauguration du parvis Henri Krasucki, avec Sophie Binet, secrétaire générale de la CGT, Pierre Krasucki et Patrice Bessac, maire de Montreuil
------------------------------------------------------------------------------------------------------------------------------------------------------------


13 h 00 – Déjeuner
-----------------------


14 h 15 – « Le syndicalisme, à l’offensive pour combattre les idées d’extrême droite » – table ronde suivie d’un débat, animée par Dominique Besson-Milord et Damien Pagnoncelli, CEC
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


- La lutte au quotidien contre le système mis en place par Robert Ménard ;
  Julien Rader, secrétaire général de l’UL de Béziers
- Face à la progression des idées d’extrême droite en Europe, quelles
  réponses du syndicalisme ? Valérie Parra Balayé, responsable Europe,
  secrétariat des relations internationales, UGT - Espagne ; Monica Ceremigna,
  conseillère espace Europe et International, CGIL - Italie
- L’alliance avec la société civile pour combattre le racisme et l’antisémitisme ;
  Pierre Tartakwoski, CNCDH (Commission nationale consultative des Droits de l’Homme)
- La lutte des travailleur·ses sans papiers et la responsabilité
  du patronat dans ce système d’exploitation ; Cheick Camara,
  CGT-Sepur (collecte des déchets)

16 h 45 – Conclusions par Sophie Binet, secrétaire générale de la CGT
---------------------------------------------------------------------------
