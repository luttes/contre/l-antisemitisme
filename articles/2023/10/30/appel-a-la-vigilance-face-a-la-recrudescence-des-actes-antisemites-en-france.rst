.. index::
   pair: RAAR; Appel à la vigilance face à la recrudescence des actes antisémites en France (2023-10-30)


.. _raar_vigilance_2023_10_30:

====================================================================================================
|halte| 2023-10-30 **Appel à la vigilance face à la recrudescence des actes antisémites en France**
====================================================================================================

- https://raar.info/2023/10/antisemtisme-france/

.. figure:: images/halte_antisemitisme.jpg
   :width: 500


Depuis les massacres antisémites et terroristes de civil.es par le Hamas
en Israël le 7 octobre 2023 et la guerre qui s’en est suivie, plus de
800 plaintes pour actes antisémites ont été enregistrées en France,
alors qu’on en a compté 436 sur l’année 2022 tout entière.

A Carcassonne, le tag **tuer des Juifs est un devoir** est inscrit sur
le mur extérieur d’un stade.

Trois clients d’une fromagerie de Dijon traitent de **sale petite pute juive**
une employée qui porte une étoile de David.

Une étudiante de l’Institut Catholique de Paris s’est entendue dire
qu’ Hitler n’avait « pas fini son travail »,

un autre qu’ « il était temps que les Palestiniens détruisent ce pays
de colonisateurs ».

Les menaces et les messages de haine prolifèrent sur les réseaux sociaux
et ne sont pas modérés, notamment sur X.

Des figures publiques juives de gauche y sont désignées comme « sionistes »,
terme qui sert ici de code antisémite pour dire « juif/ves ».

De nombreuses personnes juives sont gagnées par l’anxiété et la peur.

La RAAR appelle toutes les organisations progressistes à la vigilance
face à ce nouveau moment antisémite.

Celles et ceux qui soutiennent la cause palestinienne et un processus
de paix ainsi que toutes celles et ceux qui rejettent le racisme et la
haine doivent se désolidariser de l’ensemble de ces agressions, les
dénoncer et lutter contre l’antisémitisme d’où qu’il vienne.

Paris, le 30 octobre 2023

Le Réseau d’Actions contre l’Antisémitisme et tous les Racismes (RAAR)
