.. index::
   pair: RAAR; 2023-10-15

.. _raar_2023_10_15:

===========================================================================================================
Dimanche 15 octobre 2023 15h30 **Débat du RAAR : La gauche et la lutte contre l'antisémitisme**
===========================================================================================================

- :ref:`Les informations ont été tranférées sur le site https://antiracisme.frama.io/infos-2023 <raar_2023:raar_2023_10_15>`
