.. index::
   pair: Volia Vizeltzer; Lettre d’un juif français de gauche, qui veut bien entendre

.. _lettre_2023_10_28:

==============================================================================
2023-10-28 **Lettre d’un juif français de gauche, qui veut bien entendre**
==============================================================================

- https://blogs.mediapart.fr/volia-vizeltzer/blog/281023/lettre-d-un-juif-francais-de-gauche-qui-veut-bien-entendre
- https://www.instagram.com/vizeltzer/
- https://kolektiva.social/@jjr/111274344184451548
- :ref:`volia_2023_11_09_381`

#israel #palestine #racisme #antisemitisme #france #paix #lutte #abaslaconcurrencevictimaire


Je pense que je ne me suis jamais senti aussi juif, de manière aussi
brutale, que depuis le 7 octobre 2023.

Je ressens aujourd'hui le besoin de témoigner en mon nom, en me situant
comme un juif français, de ce qu'il se passe aujourd'hui dans ce pays.

Le samedi 7 octobre 2023, j’étais à un salon de livre en tant que bénévole
pour tenir le stand de camarades libraires.
Après avoir installé mon stand, je me suis assis derrière. J’ai pris mon
téléphone qui n’arrêtait pas de sonner pour regarder mes messages.
Il devait être 11h environ.
Je peux dire que je n’ai pas l’impression de l’avoir posé depuis.
Je suis toujours dans ce moment, du matin du 7 octobre, dans ce moment
figé, figé dans l’incompréhension et emporté par le flot chaotique de
l’enchaînement de l’Histoire.

Au départ, je n’arrivais pas à comprendre ce qu’il se passait.
Je n’arrivais qu’à rafraîchir en boucle la page de direct du quotidien
Haaretz qui donnait les infos une par une sur l’évolution de la situation.

Le chiffre de morts qui doublait toute les heures. La nature civile de
ses personnes assassinées de plus en plus visible.
C’est vrai qu’en tant que juif, la violence de cette séquence était
tout particulièrement insoutenable de base. Mais c’était sans compter
la suite directe qui allait rendre tout cela bien plus difficile.
La suite qui consiste en l’offensive militaire meurtrière de l’armée
israélienne, et de la réaction des personnes, notamment la gauche, en France.

La droite à fait ce que la droite fait. Elle s’est emparée du sujet pour
en faire un conflit de civilisation, en instrumentalisant le vécu des
juifves a des fins islamophobes.
Je suis de gauche, et juif, donc la droite, et sa droite, je n’en
attends rien.

C’est alors qu’est arrivée la gauche.
La première réaction de la gauche (dans son immense majorité, et ça me
peine de le dire) à été de l’ordre d’un soutien à la "résistance palestinienne"
peu importe "ses méthodes de lutte".

On peut débattre de la sémantique, on peut débattre du contexte et de la
géopolitique, et bien évidemment qu’on ne peut pas analyser les évènements
du 7 octobre 2023 hors de leur contexte de colonisation et d’une population
palestinienne violemment opprimée.

Mais publier le week-end du 7-8, des communiqués qui, que la gauche
aujourd’hui veuille l’accepter ou pas, **cautionnaient le massacre
antisémite du Hamas**, c’était, pour faire un bel euphémisme, légèrement
maladroit.

Je rajouterai que son rétropédalage d'une fausseté évidente n'a fait
que l'enfoncer plus profondément qu'elle ne l'était déjà.

Ce que je veux dire dans cette lettre, chère "gauche", c'est que ce que
je vis aujourd'hui comme juif français, c'est loin d'être nouveau.

**Ca fait longtemps que je les vois moi en tout cas, vos biais antisémites**.

L'antisémitisme en France, c'est une affaire de quotidien. Alors rappelons
tout de même certaines réalités qui sont les nôtres, à nous Juifves.

Il est habituel en France que les Juifves se voient sommé.es de répondre
des agissements de l’État d’Israël.

Il est habituel que dans les milieux de gauche, la parole juive soit
conditionnée. Conditionnée le plus souvent à l’aveu catégorique
“d’antisionisme”.
Conditionnée aussi au fait de préciser que d’autres groupes minoritaires
souffrent plus que nous.
Conditionnée à tout pleins d’impératifs qui traduisent les biais
antisémites de chaque personne qui les emet.

**Depuis le 7 octobre 2023, ce sentiment là il a explosé**.

J’ai beau m’être exprimé sur des réseaux sociaux, et avec des proches,
dans l’immédiateté du “pendant” et dans le “juste-après”.

Aujourd’hui j’ai énormément plus de mal à parler. Je pense que l’on
est beaucoup à ressentir ce mutisme.
On n'arrive pas à raconter ce que l’on traverse, en tant que juifves
en France. Et cela, encore une fois, pas seulement depuis 3 semaines,
il faut s’en rendre compte.

On n'arrive tout d'abord pas à parler car la droite crie trop fort.
Dans son élan d’instrumentalisation obscène, elle se sert de nos
douleurs, de nos craintes, de notre deuil, pour faire de notre
condition un prétexte a l’islamophobie d’État.
C’est une stratégie politique immonde et éminemment dangereuse, où la
population juive est brandie comme étendard par les nationalistes
racistes qui veulent uniquement attaquer les musulmans de France
(et du monde à beaucoup d'égards).

**Je n’en peux plus de voir la droite occuper le terrain de la dite
«lutte contre l’antisémitisme».**
Vous ne vous rendez pas compte de ce que ça fait de se voir
instrumentalisé de cette façon.
**Et si la droite instrumentalise ce sujet si facilement, vous savez quoi
la gauche ?... Vous n’y êtes pas pour rien**.

**C’est vous aussi qui avez abandonné les Juifves de France**.

Vous nous avez abandonné en rentrant dans le jeu politicien immonde de
la droite qui consiste à faire de la lutte contre l’antisémitisme un
élément de language partisan.

Vous êtes rentrés volontairement dans cette binarité qui consiste à
considérer la reconnaissance de l’existence d’un antisémitisme à gauche,
comme un gage envers la droite.
Vous vous êtes laissé gagné par votre laxisme envers vos biais
antisémites existants, en vous enfonçant dans ce confort dédaigneux
qui vous amène à systématiquement nier.

Vous avez sciemment adopté l’idée que l’on ne pouvait pas vous
reprocher quelque chose dont vous niez l’existence.

Car vous avez parié sur le campisme.

**Vous avez délibérément laissé un boulevard à la droite pour nous
instrumentaliser**. Et c’est bien plus facile pour elle d’instrumentaliser
nos luttes quand en face…c’est vide.

Et on le voit. L’antisémitisme en France semble faire peu débat à gauche
aujourd’hui en tout cas. Soit elle vient, en effet, de l’extrême droite,
soit c’est forcement une accusation houleuse visant à discréditer quelqu’un…

Vous êtes d’ailleurs tellement rentré dans ce narratif que vous y
semblez piégés.
Parce que maintenant **vous êtes face à des résurgences d’antisémitisme
dans vos milieux, ces biais résurgents que vous avez encouragés par
votre confusionnisme et votre manque de clarté politique**.

L'antisémitisme dans votre famille politique n'est aucunement résiduel,
il est structurel, et on le voit d'autant plus clairement désormais.

Vous pouvez alors soit changer votre posture au risque de briser votre
semblant de “cohérence”, soit nier.

Et vous, vous niez.

Vous niez le caractère trans-partisan de l’antisémitisme.

Vous niez le fait que comme chaque discrimination en France, ce phénomène
n’est pas l’apanage d’un camp, et que tout le monde doit faire un travail
là dessus.
Vous niez ce qui constitue la nature même de l'antisémitisme, c'est à
dire son caractère systémique et structurel.
**En associant systématiquement toute accusation d’antisémitisme à de la
délation, vous avez entretenu ce préjugé, profondément antisémite, de la
collusion entre les Juifs et le pouvoir**.

Vous communiquez ouvertement depuis des années que chaque accusation
d'antisémitisme à votre égard est une manipulation sournoise pour vous
attaquer sous la ceinture.
Pourtant, ne devrait-on pas attendre d’une gauche qu’elle sache faire
la part des choses entre de l’instrumentalisation honteuse, et des
paroles critiques de biais existants par des personnes concernées ?
Personnes qui ne sont ni le CRIF ni le gouvernement israelien par ailleurs.

**Vous n’avez aucune excuse**.

Vous n’avez aucune excuse d’avoir laissé prospérer en votre sein une
complaisance envers l’antisémitisme.
Vous n’avez aucune excuse de votre positionnement du week-end du
7-8 octobre 2023.
Et vous n’avez aucune excuse, d’ignorer les Juifves que vous avez
négligé et méprisé depuis des décennies.

Je ne parle pas à la droite, encore une fois, je n’en attends rien.

**Je vous parle à vous, mes soit-disant ”camarades”...**

Enfin sommes nous camarades ? **Surement qu’une bonne partie d’entre vous
me diront que non**.
Surement parce que personnellement je reconnais la légitimité de la
création de l’État d’Israël. Je suis donc, comme qui dirait, un “sioniste”.

Il y a eu beaucoup de gens dernièrement sur les réseaux pour me le
rappeler ça, mon identité soit disante “sioniste”.
**Et ce "sionisme" il est forcement disqualifiant, puisque dans la matrice
de pensée à gauche, le sionisme est l'équivalent du mal sur Terre**.

**Vous le nierez encore, mais je n'en attends pas moins de vous à présent**.

Et j'aimerais dire quelque chose de fondamental aussi.
Ce n’est pas moi au final, juif de gauche, qui avait, et a toujours, un
problème avec la gauche, c’est la gauche qui en premier lieu, a un
problème avec moi.

Et pourquoi donc ?

Parce que j’ai besoin de parler d’antisémitisme en France en tant que
juif ? C’est ça le problème ? Quand il s’agit de dire à quel point
l’antisémitisme ça n’existe pas à gauche là y’a du monde, mais pour se
remettre en question et accepter des torts, y’as plus personne.

C’est ça la gauche ?

C’est l’endroit où si j’explique pas au préalable que je soutiens les
palestiniens je n’ai pas le droit d’exprimer mon vécu de juif français ?

C’est l’endroit où parce que certain.es instrumentalisent la Shoah on
considère qu’on “en fait trop” ?

C’est l’endroit où en tant que juif, tu es sommé de mettre ce que tu vis
avec l’antisémitisme de coté car tu souffrirai “moins que les autres” ?

Et qu’on ne m’y méprenne pas.
**Les évènements qui ont lieu depuis le 7 octobre 2023, en Israël,
comme à Gaza, et comme en Cisjordanie, sont un cauchemar**.

Il faut être inhumain pour ne pas être en effroi devant des charniers
de civils, de l’un ou de l’autre coté du mur.

- Bien sur qu’il faut soutenir le peuple palestinien.
- Bien sur que les gouvernement israelien successifs ayant continués la
  colonisation sont à combattre et sont responsables aussi évidemment
  de ce qu’il se passe.
- Bien sur que la France est coupable d’encensement de crime de guerre
  contre une population civile.
- Bien sur que l’interdiction érigée par Darmanin est une atteinte grave
  aux libertés fondamentales et une instrumentalisation islamophobe et autoritaire.

Mais est ce que pour se faire il faut abandonner les Juifves ?

Les Juifves n’ont iels pas le droit d’être sous le choc ?
N’avons nous pas le droit qu’on nous écoute ?

Nous n’avons pas le droit d’avoir une condition singulière qui mérite
qu’on la considère avec sincérité et préoccupation ?

Dites le nous, franchement, on vous a fait quoi ?
Qu’est ce qu’on vous a bien fait pour que vous refusiez comme ça de nous
regarder ?

Dites le si on vous dégoute, si on vous emmerde, si on est chiants avec
nos deuils et nos angoisses.
En faites dites le nous si on est de trop.
Parce que c’est de ça que ça donne l’impression.
Moi j’ai l’impression que vous ne vous en rendez même pas compte.
Vous pensez que parce que vous soutenez une cause légitime, qu’est celle
de la société palestinienne (et non du Hamas) vous êtes dans le camp
du bien.
Ce qui vous opposerai au camp du mal, de la droite, du fascisme.

**Votre lecture est une lecture binaire et romantique de posture militante**.

Et vous oubliez (consciemment ou pas) quelque chose.

La droite n’en a que faire des Juifves. Et vous, dans votre partisanisme
aveugle, vous avez fini par nous assimiler ensemble.

Dans votre cerveau poreux aux structures de pensée antisémites, **vous
avez associés la défense des Juifves avec la défense du pouvoir**.

Vous avez décidé de penser que les Juifves ne subissaient pas réellement
de discriminations, en tout cas pas systématiquement et structurellement.

**Vous vous êtes convaincus que l’idée même d’antisémitisme à gauche et
dans la société de manière générale, était une stratégie fourbe, servant
à museler votre camp**.

Vous ne réalisez même pas que les différents aspects de votre refus de
voir l’antisémitisme, **sont des énormes biais antisémites**.

Depuis le 7 octobre 2023, j’ai l’impression de n’avoir plus aucun allié.
J’ai l’impression que l’on exige des Juifves des choses que l’on ne
devrait exiger de personne.
Nous avons le droit, et il est même nécessaire, que nous parlions de
nos vécus. Il est nécessaire que l’antisémitisme ne soit plus un angle
mort.
Et nous avons besoin de pouvoir le faire sans faire l’objet d’injonctions,
de conditions préalables, ou d’une forme de suspicion, de méfiance,
que l’on ne connait que trop bien...

Pour beaucoup, ce que je dis là, j’en demande trop.
Ca ne m’étonne même plus honnêtement. On en demande toujours trop nous
les Juifves. On nous le fait bien comprendre depuis le temps.
Mais rappelez vous une chose.

L’antisémitisme il n’a pas attendu le conflit israelo-palestinien
pour exister, et il n’attend pas la “fin” potentielle de ce conflit
pour s’en aller.
L’antisémitisme est présent, et ne fait que se manifester à différentes
intensités lorsque l’occasion se présente. C'est une toile de fond, qui,
en France particulièrement, est profondément ancrée dans la société.
C'est un système de pensée, d'explication tronquée de la complexité du
monde, qui n'existe pas au sein de tel ou tel conflit, mais qui en fait
l'usage pour appuyer sa conclusion finale et absolue : Les Juifves sont coupables.

**Il serait vraiment malhonnête de ne pas constater que lorsque les
choses explosent au Proche-Orient, la violence finit par arriver ici aussi**.
**Car ces structures de pensées se manifestent plus violemment dans un
contexte où les portes s'ouvrent et que l'expression antisémite n'est
plus marginalisée et contenue**.

Dire ça ce n’est pas déshumaniser les palestinien·nes, c’est simplement
parler des Juifves de France.

Vous savez, nous sommes habitué.es à être des cibles, entre l’extrême
droite suprémaciste blanche et l’extrême droite djihadiste.
Nous savons ce que c’est un attentant. Des attentats. Ne faisons pas
comme si tout cela n’existait pas. Nombre d'entre nous en ont pleuré
longuement.

Nous savons aussi ce que c’est de l’antisémitisme. Nous savons assez bien
distinguer, en tout cas mieux que vous ne le pensez, une personne qui
se revendique “antisioniste” car contre la colonisation israelienne, et
un antisémite qui se cache derrière le terme.

Terme au passage dont la définition mériterait plus de rigueur
qu'aujourd'hui, vu l'extrême fine limite entre la critique d’Israël,
et la haine d’Israël.
**Vous, vous ne faites pas la distinction**. Vous laissez exister des
propos ahurissants au sein de vos cercles militants sous couvert
d’antisionisme, et donc “pas d’antisémitisme”.
**Mais vous le savez tout autant que nous, que cette vision là est
simpliste et fausse. Il existe une porosité manifeste entre
l’antisionisme et l’antisémitisme**.

Dire ça ce n’est pas interdire toute critique du gouvernement israelien,
ni interdire des manifestations de soutien au peuple palestinien.
C’est tout simplement dire qu’ici en France, sous prétexte d'antisionisme,
sont commis des actes antisémites allant jusqu’au meurtre indiscriminé
de civils juifves.

Et vous, votre solution à gauche, c’est qu’en observant la droite
prendre le pas de qualifier tout antisionisme comme de l’antisémitisme,
vous avez décidé comme d’habitude, de jouer sur la posture.
Pour vous donc, au contraire, aucun antisionisme n’est de l’antisémitisme
on dirait bien.
**C’est pas nouveau pourtant ici la convergence des deux**.
Aurai-je besoin de rappeler la carrière de Dieudonné par exemple ?
Aurai-je besoin de rappeler la tuerie de l'Hyper Casher en 2015,
perpétrée "au nom" des palestinien·nes ?

L'idée n'est pas d'amalgamer bien évidemment, mais la solution ne sera
jamais de nier non plus.

Et nous y revoilà. **La droite instrumentalise et la gauche nie**.

Et en attendant que la politique française règle ses querelles de
chapelle, nous, les juifves, nous sommes sommés de rester assis à coté
et de ne pas faire trop de bruit.
**On comprend bien, on vous dérange**.

La seule chose que j’arriverai à dire pour finir ce texte, c’est ceci :
si la gauche veut faire sans les Juifves, on ne peut pas l’en empêcher.

Dans ce cas, alors, nous on va devoir faire sans aussi.

Vous savez, entre juifves, en tout cas pour moi et mes camarades, on
trouve vraiment peu le besoin de s’assurer si tout le monde est toujours
aussi horrifié que la veille par les victimes civiles à Gaza.

On le sait. Parce qu’on ne se méfie pas de nous entre nous. Personne
n’a l’impression qu’un autre joue un double jeu, manipule pour arriver
à ses fins.
On se voit comme des êtres humains, qui sommes en deuil de la mort
de toustes.
C’est un regard que je peine trop à retrouver aujourd’hui, en dehors
des cercles où nous luttons contre ce que nous devons vivre quotidiennement,
par le biais du silence ou du bruit de nos dits “camarades”.

Il serait temps de se regarder dans un miroir.

Car si vous êtes heurté de vous faire traiter d'antisémites, imaginez
seulement ce que c'est de le vivre l'antisémitisme.

Ayez de l'humilité, et changez. Car vous foncez dans un mur.

Si je vous dis ça, ce n'est plus par compassion envers vous, c'est parce
que sur votre chemin vers ce mur, il y a nous les Juifves, **et vous nous
piétinez**.
