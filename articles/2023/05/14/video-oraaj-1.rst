.. index::
   pair ORAAJ ; Rassemblement suite à l'attentat antisémite de Djerba (2023-05-14)

.. _oraaj_2023_05_14:

==================================================================================================================================================================
2023-05-14 **Discours de ORAAJ au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Fusillade_du_9_mai_2023_%C3%A0_Djerba
- https://fr.wikipedia.org/wiki/Synagogue_de_la_Ghriba_(Djerba)
- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed


Discours de ORAAJ
=====================

- https://youtu.be/4tvmA71gb0Q?t=197
- :ref:`oraaj`

Présentation de ORAAJ
-------------------------

merci beaucoup maintenant il va y avoir une prise
de parole de ORAAJ donc ORAAJ juive c'est organisation révolutionnaire
antiraciste, antipatriarcale, et juive

Discours
------------

- https://youtu.be/4tvmA71gb0Q?t=213
- https://fr.wikipedia.org/wiki/Fusillade_du_9_mai_2023_%C3%A0_Djerba

Ce rassemblement est en hommage aux victimes de l'attentat de la Ghriba en soutien à nos camarades
qui étaient présentes à toutes les personnes présentes sur place
dans la synagogue et aux communautés juives de Tunisie et de France la
les histoires de celle-ci s'entremêlent à toutes les personnes qui ont
été la cible dans l'attentat antisémite mardi dernier et à toutes les
personnes qui sont tuées par les racistes en France et ailleurs


- https://youtu.be/4tvmA71gb0Q?t=235

deux attaques antisémites avaient déjà eu lieu à la Ghriba en 95 et en
2002 en tuant trois  et blessant 21 personnes continuent à vivre sous la menace de
l'antisémitisme et malheureusement le lot commun des Juifs de Tunisie
de France et d'ailleurs

nous ne cessons de le répéter l'antisémitisme
tue ici et ailleurs nous voulons honorer la mémoire des personnes tuées
pèlerins avaient la date Benjamin Haddad et gendarme a démarré il a
dit #AvielHaddad #BenjaminHaddad #AbedlmajidAig #MaherElArbi #KheireddineLafi


Le pèlerinage de la Ghriba est un événement religieux et culturel très important pour les Juifs d'origine nord africaine
==============================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=263

le pèlerinage de la Ghriba est un événement religieux et culturel
très important pour les Juifs d'origine nord africaine où des milliers de
personnes se rendent chaque année

la Ghriba est une synagogue sur l'île
de Djerba pour réside l'une des dernières communautés juives importantes
d'Afrique du nord

90% des Juifs de Tunisie sont partis de Tunisie pour la
plupart fuyent l'antisémitisme auquel ils ont
finalement été confrontés dès leur arrivée en France et ailleurs

ceux et celles qui sont restés qui sont pas nombreux et sont bien présents

certains
de notre  génération y reviennent pour retrouver le pays de leurs parents
et grands-parents et certains d'entre elles certains d'entre nous se sont
retrouvés la cible de cet attentat antisémite


Ce rassemblement est aussi un moment où nous voulons dénoncer les propos antisémites
=========================================================================================

- https://youtu.be/4tvmA71gb0Q?t=301

Ce rassemblement est aussi un moment où nous voulons dénoncer les propos antisémites qui
ont suivi cet attentat

en effet que ce soit sur les réseaux sociaux de la
part d'amis ou dans l'attitude du gouvernement tunisien

nous avons vu et entendu des discours qui niait ou minimisait le caractère antisémite
de cet attentat

nous avons notamment entendu dire ces derniers jours
qu'il ne s'agissait que d'un règlement de compte entre policiers

sur les réseaux sociaux on assiste à une reprise de rhétorique complotiste


et de discours attribuant aux victimes un rapport fantasmé à Israël
et au sionisme pour légitimer l'attentat antisémite c'est
normal la victime avait un passeport israélien

je pense qu'ici tout et tous nous avons vu et lu  des choses

la date de cet attentat coïncidant
avec un regain de violence perpétuée par Israël et la mort de nombreux
Palestiniens civils des discours mobilisent le contexte géopolitique
pour illigitimer le drame que représente les morts de la Ghriba

nous souhaitons dénoncer le caractère insupportable de ces rhétoriques
qui encore une fois minimisent les violences mortelles dont les Juifs et
juives n'ont jamais cessé d'être la cible.

L'antisémitisme n'a pas attendu Israël pour exister
=========================================================

- https://youtu.be/4tvmA71gb0Q?t=384

L'antisémitisme n'a pas attendu Israël pour exister

**Nous nous opposons à toute politique raciste et coloniale**.

**Nous critiquons la politique raciste et coloniale du gouvernement israélien
qui tue et opprime les populations palestiniennes**

comme nous critiquons donc toute structure politique ou administrative dans
l'espace mondial dont les dispositifs sont meurtriers

cette dénonciation
doit s'accompagner d'un rappel que les critiques d'Israël en France et
ailleurs produisent trop souvent de l'antisémitisme en instrumentalisant
transformant une critique politique légitime en antisémitisme.

Tenir une critique ferme de la politique du gouvernement israélien est essentiel
=====================================================================================

- https://youtu.be/4tvmA71gb0Q

Depuis des années 60 70 en Afrique du Nord et en Tunisie
particulièrement ainsi qu'en France ce qu'il se passe en Israël-Palestine
est mobilisé pour exercer des violences sur les Juifs de Tunisie et en
France aussi

et également mobilisé pour justifier et tenter de légitimer
ses violences

tenir une critique ferme de la politique du gouvernement
israélien est essentiel pour nos luttes anticapitalistes, antiracistes
et pour notre défense des droits des peuples à l'autodétermination.

Il faut aussi critiquer les discours selon lesquels cela justifie le meurtre de Juifs et de Juifs
=====================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=458

**Il faut aussi critiquer les discours selon lesquels cela justifie le
meurtre de Juifs et de Juives** car cela est antisémite

nous rappelons que
l'antisémitisme est un racisme fondé sur l'idée que les juifs seraient un
danger pour la nation et l'ordre social parce qu'il serait une sous-race qui
dominerait pourtant le monde


Et l'antisémitisme contient malheureusement dans son fonctionnement idéologique un caractère génocidaire
=============================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=475

Et l'antisémitisme contient malheureusement dans son fonctionnement idéologique
un caractère génocidaire

il faudrait éradiquer ceux et celles qui dominent le monde

derrière les rhétoriques complotistes et celles qui nient le caractère
antisémitisme antisémite de cet attentat ce sont les mêmes idéologies
antisémites qui sont présentes ne l'oublions pas

.. _négrophobie_2023_11_09:

Nous pensons aussi aux personnes noires en Tunisie qui ont vécu des violences racistes (négrophobie)
=====================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=494
- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed

Nous tenons à ce que ce message porte et alerte aussi celles et ceux
qui n'accordent pas l'importance qu'elles devraient accorder à la lutte
contre la négrophobie.

Nous pensons aussi aux personnes noires en Tunisie qui ont vécu des
violences racistes exacerbées ces derniers mois suite au discours du
président Kaïs Saïed et suite à une campagne haineuse et populiste
qui a eu  lieu en Tunisie il y a deux mois et qui demandait l'expulsion
des migrantes subsahariennes

et reprenait des thèses xénophobes du Grand remplacement

la négrophobie en
Tunisie qui s'exerce à l'encontre des migrants subsahariens et aussi des
Tunisiens noirs ne datent pas d'hier

ce racisme résulte d'un héritage
de l'esclavage producteur de négrophobie en Tunisie

et d'une politique
migratoire externalisée raciste de l'Europe

les rhétoriques racistes
s'articulent entre elles

et on constate une tendance des deux côtés de
la Méditerranée à une exacerbation de tous les racismes dont l'antisémitisme
fait pleinement partie remettant en cause le droit des personnes visées à
vivre dans le pays

tous les racismes ont cela de commun qu'ils questionnent
la légitimité de groupe à faire partie de la nation


Alors comment vivre avec le racisme et l'antisémitisme quelle lutte devons-nous aujourd'hui mettre en place
================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=560

Alors comment vivre avec le racisme et l'antisémitisme quelle lutte
devons-nous aujourd'hui mettre en place

Nous pensons ici à :ref:`ORAAJ <oraaj>` que c'est dans la construction de
solidarité féministe antiraciste que nous allons pouvoir déconstruire
et mettre à mal les constructions sociales propres à tous les racismes

mais aussi celles spécifiques à chacun d'eux

c'est ici que nous devons
nous organiser pour déjouer, analyser, et mettre à mal l'antisémitisme
et tous les racismes quel que soit le contexte sociopolitique dans lequel
il s'exprime

nous souhaitons organiser ce rassemblement pour continuer à
dénoncer ces meurtres qui ciblent les personnes racisées

il nous rappelle à chaque violence extrémiste et d'extrême droite
chaque violence raciste d'État

chaque violence capitaliste est aussi décoloniale

à quel point les vies des personnes racisées sont menacées
et fragiles

Nous demandons:

- la mise en place de cellules psychologiques gratuites
- des congés maladie
- une reconnaissance des traumas des personnes qui résident en France
  ciblées par cet attentat  et pour toutes les personnes victimes de
  violence raciales


Nous souhaitons rappeler que la date du 14 mai est également celle du triste anniversaire du 14 mai 1941 (rafle du billet vert)
===================================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=619
- https://fr.wikipedia.org/wiki/Rafle_du_billet_vert


Nous souhaitons rappeler que la date du 14 mai est également celle du
triste anniversaire du 14 mai 1941

ce jour-là 3700 hommes Juifs sont arrêtés à Paris lors de la rafle
dite du billet vert et transférés dans les camps de Beaune-la-Rolande et
de Pithiviers dans le Loiret d'où ils furent envoyés vers la mort


Fin
=====

- https://youtu.be/4tvmA71gb0Q?t=638

merci


Interventions des militantes d'OORAJ
----------------------------------------


.. figure:: images/photo_youtube.png
   :align: center


.. figure:: images/oraaj.png
   :align: center

.. figure:: images/oraaj_2.png
   :align: center


.. figure:: images/oraaj_3.png
   :align: center


.. figure:: images/oraaj_4.png
   :align: center


.. figure:: images/oraaj_5.png
   :align: center


.. figure:: images/oraaj_6.png
   :align: center
