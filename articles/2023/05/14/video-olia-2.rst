
.. _olia_2023_05_14_1169:

==================================================================================================================================================================
2023-05-14 **Discours de Olia (RAAR) au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed


Commentaire sur une plaque évoquant la rafle du biller vert
===============================================================

- https://youtu.be/4tvmA71gb0Q?t=1169


[Applaudissements] ok il y a une plaque derrière vous ne pourrez tous
aller admirer et vous prendre en photo devant

le billet vert


Discours de Olia
====================

- https://youtu.be/4tvmA71gb0Q?t=1188


c'était juste pour terminer sur les suites de l'attentat en Tunisie pour qu'on se
rende compte ce qui se passe

la plupart des gens quand ils font mémoire
aux victimes ils font mémoire aux victimes aux trois policiers et pas aux
2 juifs tués

il y a cette idée que les Juifs même s'ils sont Tunisiens
sont pas chez eux en Tunisie

on sait pas du coup où ils sont chez eux

il y a eu des suites c'est à dire que dès le lendemain il y a eu des vidéos
qui ont été tournées dans le souk avec des pour ceux qui connaissent
Djerba qui ont été tournés dans le souk de Djerba vers umtsuk etc avec
des T-Shirts floqués marqué Djerba is safe

et plein de touristes avec à
qui on a demandé de porter le drapeau tunisien et de dire Djerba is safe
Djerba is safe sans jamais rappeler les noms des deux victimes juives

et par
ailleurs le président Kaïs Saïed  n'est jamais venu à Djerba depuis l'attentat et
la seule préoccupation là tout de suite de la Tunisie quand on entend
les plateaux télé c'est la survie de la saison touristique de cet été

pourquoi je rajoute ça parce que c'est pour montrer que en fait je sais
qu'il y a des associations tunisiennes et que la Tunisie aime ses Juifs
à n'en  pas douter mais d'un point de vue institutionnel c'est pas du tout le
cas

donc il y a quand même ce truc qi vous regardez sur les réseaux
sociaux par exemple Djerba is safe on peut on peut atterrir sur vraiment des
discours assez dingues dont sur des plateaux télé une femme qui dit
que si jamais il y a eu cet attentat évidemment c'est à cause c'est en
représailles de Gaza et de la mort des enfants palestiniens

je veux dire
c'est dit clairement alors que les deux victimes étaient tunisiennes

quoi
donc les Juifs sont chez eux nulle part selon des gens quoi voilà merci

Fin
======

- https://youtu.be/4tvmA71gb0Q?t=1308
