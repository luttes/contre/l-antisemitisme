.. index::
   pair François béchaud ; Rassemblement suite à l'attentat antisémite de Djerba (2023-05-14)

.. _bechaud_2023_05_14:

==================================================================================================================================================================
2023-05-14 **Discours de François béchaud au Rassemblement suite à l'attentat antisémite de Djerba**
==================================================================================================================================================================


Discours de François béchaud
===============================

- https://youtu.be/4tvmA71gb0Q?t=51

Oui bonjour à toutes et à tous

je suis François béchaud maire adjoint du
19e arrondissement pour particulièrement chargé de la vie associative et
de l'expérimentation d'un minimum garanti.

je suis également conseillé à la Métropole du Grand Paris.

et secrétaire national du mouvement néo progressiste qui est un mouvement
résolument créé à gauche résonnant ancré dans les solidarités au pluriel
dans l'inclusion de notre société qui en a bien besoin.

alors bravo à toutes et tous

je pense qu'il faut pas rougir de l'organisation d'aujourd'hui on sait que c'est
très difficile et le sujet aujourd'hui mérite bien que nous soyons
rassemblés

l'antisémitisme l'antisémitisme bien sûr aujourd'hui dans
ces lâches et odieux attentats qui ont assassiné une famille à Djerba une
famille de juifs mais aussi l'antisémitisme à travers notre société
française à travers nos sociétés occidentales et à travers le monde
un antisémitisme encore une fois qui a traversé les temps hélas et
qui est aujourd'hui bien ancré dans notre société

l'antisémitisme en France n'a pas attendu la création de l'État d'Israël
===========================================================================

- https://youtu.be/4tvmA71gb0Q?t=114

Donc moi je souhaite toujours dire et redire que l'antisémitisme en France
n'a pas attendu la création de l'État d'Israël

l'antisémitisme on a qu'à lire Voltaire ou d'autres classiques de français
pour s'en rendre bien compte et il n'y
a bien il n'y a bien qu'à étudier également l'histoire de France moi
personnellement j'ai fait une étude sur le crime en Corse à la fin
de l'Ancien Régime 1769 1789 je peux vous assurer que les personnes
d'origine et de confession juives sont pas notées françaises elles sont
notées juives sur les registres

donc bravo à vous à toutes et tous
je pense que c'est un travail de fourmis auquel il est exigeant et ou
auquel il est urgent de s'atteler aujourd'hui comme demain et vous pouvez
compter sur mon soutien et sur le soutien de mon mouvement pour tous les
jours pour aujourd'hui comme demain travailler avec acharnement contre
l'antisémitisme contre tous les racismes bien entendu et contre celles et
ceux qui d'une façon ou d'une autre exprime un antisémitisme viscéral
dont on a donc qu'on voudrait bien à ne plus avoir à faire

en bravo à
vous et puis je suis très heureux aussi de voir autant de jeunes c'est
important parce que ça c'est vraiment un message d'espoir donc bravo
et donc à très vite

Fin
=======

- https://youtu.be/4tvmA71gb0Q?t=197

merci beaucoup maintenant il va y avoir une prise
de parole de orage donc orage vif c'est organisation révolutionnaire
antiraciste antipatriarcale juive se rassemblement de l'organisons en
hommage de victimes de l'attentat de la griba en soutien à nos camarades
qui étaient présentes à toutes les personnes présentes sur place
dans la synagogue et aux communautés juifs de Tunisie et de France la
les histoires de celle-ci s'entremêlent à toutes les personnes qui ont
été la cible dans l'attentat antisémite mardi dernier et à toutes les
personnes qui sont tuées par les racistes en France et ailleurs deux
attaques antisémites avaient déjà eu lieu à la grippe en 95 et en
2002 tu en croix et 21 personnes continuent à vivre sous la menace de
l'antisémitisme et malheureusement le lot commun des Juifs de Tunisie
de France et d'ailleurs nous ne cessons de le répéter l'antisémitisme
tue ici et ailleurs nous voulons honorer la mémoire des personnes tuées
pèlerins avaient la date Benjamin Haddad et gendarme a démarré il a
dit le pèlerinage de la grippa est un événement religieux et culturel
très important pour les Juifs d'origine dans africaine ou des milliers de
personnes se rendent chaque année la débat est une synagogue sur l'île
de Djerba pour réside l'une des dernières communautés juives importantes
d'Afrique noire 90% des Juifs de Tunisie sont partis de Tunisie pour la
plupart donc je suis l'antisémitisme à l'antisémitisme auquel ils ont
finalement été confrontés dès leur arrivée en France et ailleurs ce
SS qui sont restés qui sont pas nombreux et sont bien présents certains
notent génération y reviennent pour retrouver le pays de leurs parents
et grands-parents et certains d'entre elles certains d'entre nous se sont
retrouvés la cible de cette attentat antisémite [Musique] ce rassemblement
est aussi un moment où nous voulons dénoncer les propos antisémites qui
ont suivi cet attentat en effet que ce soit sur les réseaux sociaux de la
part d'amis ou dans l'attitude du gouvernement tunisien nous avons vu et
entendu des discours qui n'y est ou minimiser le caractère antisémite
de cette attentat nous avons notamment entendu dire ces derniers jours
qui nous agissaient que d'un règlement de compte entre policiers sur les
réseaux sociaux on assiste à une reprise de rhétorique complotiste
et de discours attribuant aux victimes un rapport fantasmé à Israël
et aussionisme pour légitimité légitimer l'attentat antisémite c'est
normal la victime avait un passeport israélien je pense qu'ici tout et
tous nous avons vu élu des choses la date de cette attentat coïncidant
avec un regain de violence perpétuée par Israël et la mort de nombreux
Palestiniens civils des discours mobilisent le contexte géopolitique
pour l'île jtimée le drame que représente les morts de la grippe
nous souhaitons dénoncer le caractère insupportable de ces rhétorique
qui encore une fois minimise les violences mortelles dont les Juifs et
juivent n'ont jamais cessé d'être la cible l'un de ces mythisme n'a
pas attendu Israël pour exister nous nous opposons à toute politique
raciste et coloniale nous critiquons la politique raciste et coloniale du
gouvernement israélien qui tue et oppriment les populations palestiniennes
comme nous critiquons donc toute structure politique ou administrative dans
l'espace mondial dont les dispositifs sont meurtriers cette dénonciation
doit s'accompagner d'un rappel que les critiques d'Israël en France et
ailleurs produisent trop souvent de l'anxémitisme en instrumentalisant
transformant une critique politique légitime en antisémitisme
[Applaudissements] depuis des années 60 70 en Afrique du Nord et en Tunisie
particulièrement ainsi qu'en France ce qu'il se passe en Israël Palestine
et mobilisé pour exercer des violences sur les Juifs de Tunisie et en
France aussi et également mobilisé pour justifier et tenter de légitimer
ses violences tenir une critique ferme de la politique du gouvernement
israélien est essentiel pour nos luttes anticapitalistes antiraciste
et pour notre défense des droits des peuples à l'autodétermination
il faut aussi critiquer les discours selon lesquels cela justifie le
meurtre de Juifs et de Juifs car cela est antisémite nous rappelons que
l'antisémitisme est un racisme fondé sur l'idée que les juifs seraient un
danger pour la nation et l'ordre social parce qu'il serait une sourate qui
dominerait pourtant le monde et l'antisémitisme contient malheureusement
dans son fonctionnement idéologique un caractère génocidaire il faudrait
éradiquer ceux et celles qui domine le monde derrière les rhétoriques
complotistes et celles qui nie le caractère antisémitisme antisémite
de cette attentat ce sont les mêmes idéologies antisémites qui sont
présentes ne l'oublions pas nous tenons à ce que ce message porte et
alerte aussi celles et ceux qui n'accordent pas l'importance qui elle
devrait accorder accorder à la lutte contre la nécrophobie nous pensons
aussi aux personnes noires en Tunisie qui ont vécu des violences racistes
exacerbées ces derniers mois suite au discours du président Kais et
suite à une campagne haineuse et populiste qui a eu lieu en Tunisie il
y a deux mois et qui demandait l'expulsion des migrants de subsahariennes
et reprenez des tests xénophobes du Grand remplacement la négrophobie en
Tunisie qui s'exerce à l'encontre des migrants subsahariens et aussi des
Tunisiens noirs de datent pas d'hier ce racisme résulte d'un héritage
de l'esclavage producteur de négrophobie en Tunisie et d'une politique
migratoire externalisée raciste de l'Europe les rhétoriques racistes
s'articulent entre elles et on constate une tendance des deux côtés de
la Méditerranée à un exercice de tous les racismes dont l'antisémitisme
fait pleinement partie remettant en cause le droit des personnes visées à
vivre dans le pays tous les racismes on se là de commun qu'il questionne
la légitimité de groupe à faire partie de la nation alors comment vivre
avec le racisme et l'antisémitisme quelle lutte devons-nous aujourd'hui
mettre en place nous pensons ici à orage que c'est dans la construction de
solidarité féministe antiraciste que nous allons pouvoir déconstruire
et mettre à mal les constructions sociales propres à tous les racistes
mais aussi celles spécifiques à chacun d'eux c'est ici que nous devons
nous organiser pour déjouer analyser et mettre à mal l'antisémitisme
et tous les racismes quel que soit le contexte sociopolitique dans lequel
il s'exprime nous souhaitons organiser ce rassemblement pour continuer à
dénoncer ses meurtres qui ciblent les personnes racisées il nous appelle
il nous rappelle à chaque violence extrémiste et d'extrême droite
chaque violence raciste d'État chaque violence capitaliste est aussi des
coloniales à quel point les vies des personnes racisées sont menacées
et fragiles nous demandons la mise en place de cellules psychologiques
gratuites des congés malades une reconnaissance des traumas des personnes
qui résidents en France ciblées par cette attentat et pour toutes les
personnes victimes de violence raciales nous souhaitons rappeler que la
date du 14 mai est également celle du triste anniversaire du 14 mai 1941
ce jour-là 3700 hommes Juifs sont arrêtés à Paris lors de la rafle
dit du billet vert et transféré dans les camps de Beaune-la-Rolande et
de Pithiviers dans le Loiret d'où ils furent envoyés vers la mort merci
[Applaudissements] merci maintenant prise de parole du RA réseau d'action
entre l'antisémitisme et tous les racisme donc il y a quelques jours quand
les copines d'orage ont proposé aux organisés ce rassemblement j'avoue
que j'étais assez soulagé parce qu'il fallait faire quelque chose et que
il fallait que quelque chose se mette en place et j'étais super contente
que vous soyez le mouvement moteur de de rassemblement quoi donc merci de
nous avoir proposé de participer et j'espère qu'on avait fait tout ce
qu'il fallait pour vous aider au maximum pour organiser au pied levé se
rassemblement moi j'aimerais revenir juste parce que j'ai des liens assez
forts avec la Tunisie sur quelque chose qui du coup sur lequel on travaille
horreur depuis longtemps puisque on est l'organisation qui lui tombe contre
l'antimisme et tous les racismes et pour nous le lien entre l'antimisme
et les autres racismes et évident en fait c'est sûr les propos de Saïd
en février 23 qui a câblé les migrants subsahariens allant jusqu'à les
accuser de vouloir modifier la composition démographique de la Tunisie il
a parlé de horde de migrants clandestins qui s'aiment selon lui violence
et crime à travers le pays ça a pas fait grand bruit à l'époque
enfin c'est à dire que les associations antiracistes ont commencé à
dire mince ça sent pas bon mais nous à ce moment-là déjà on s'est
rendu compte que quelque chose était en train de bouger en Tunisie et
[Musique] il reprend carrément donc la théorie du grand emplacement
avec l'idée que la population arabe est remplacée par la population
donc lui il dit africaine comme si la Tunisie n'était pas en Afrique
mais donc on entend noir ça fait écho propos de Trump sur dans la
même veine les travailleurs immigrés mexicains ça fait écho à
plein de dirigeants populistes comme ça qui vont reprendre à chaque
fois principigmatiser une partie de la population pour répondre à
des problèmes imminents dans leur propre pays et c'est pas un hasard
si enfin selon moi je pense si quelque temps après les Juifs ont été
attaqués en Tunisie en fait parce que la théorie du grand emplacement
c'est toujours cette idée que il y a un processus de substitution de
population avec cette idée derrière d'un changement de civilisation
qui est soutenu voire organisé par une élite politique intellectuelle
et médiatique qui est qualifiée de remplaciste et souvent infinie c'est
les Juifs et si c'est pas les Juifs d'abord ça va être les substars il
y a un puits ensuite les Juifs donc c'est vrai que dès février en fait
on se disait qu'il se passait quelque chose en Tunisie qui sentait qui
sont tes mauvais quoi et cette vieille idéologie raciste et antisémite
elle a pas de d'aujourd'hui elle date de la Troisième République des
nationalismes d'avant guerre qui peut viser en fonction des moments
politiques soit les Juifs les Arméniens les Italiens à Marseille un
moment c'était en 1901 on disait que la population de Marseille allait
être remplacée par les Italiens soit les Maghrébins soit et maintenant
les Noirs au Maghreb donc pourquoi je parle de ça parce qu'en fait je
pense que l'antiracisme peut pas avec ce genre d'attaque et ce genre de
mouvement global international de masse réactionnaire on peut pas ne
pas se rendre compte que c'est quelque chose qui va se passer dans tous
les pays à un moment et si jamais on fait pas front dans l'antiracisme
à se rendre compte que chaque minorité peut être à un moment prise
à partie et que tout le monde se lève pas dans seul homme à chaque
fois qu'une seule femme on n'arrivera pas à combattre à combattre ce
mouvement de masse réactionnaire voilà je vais laisser la parole à
elle je voudrais juste dire un mot l'antisémitisme en Tunisie n'est pas
un phénomène nouveau et là nous avons eu un signaler ces dernières
années de nombreuses campagnes antisémites sur le thème les Juifs de
Tunisie sont pas des vrais tunisiens ce sont des pièces rapportées on
connaît le thème ils sont ici ils sont en France d'ailleurs des juifs
tunisiens ont été tués lors de l'attentat de l'hyper cacher un des un
des fils du rabbin de Tunis a été tué lors de l'attentat de l'hyper
cacher et de manière plus générale c'est quelque chose quand il y a eu
un ministre du Tourisme qui devait être nommé monsieur Trabelsi le fait
qu'il soit d'origine juive après qu'il a été empêché de nominations en
tout cas on en a pas parlé bref il y a de nombreux phénomènes y compris
dans la gauche politique la présence de militants juifs dans le parti
communiste tunisien et dans tous les mouvements de libération a été
longtemps occulté au point que des gens étaient ont changé leur nom
pour pas qu'il y ait trop de militants juifs dans ces parties voilà donc
c'est quelque chose qui existe qui s'est accentuée comme l'a dit Olia
avec l'arrivée au pouvoir de Kate qui est un nationalisme d'exclusion
comme il l'a montré à l'encontre des subsahariens oui et bien sûr
et juste sur les Subsahariens et le grand remplacement il faut dire les
choses comme elles sont encore plus directement les racistes les fascistes
considèrent que ce sont les Juifs qui organisent le grand remplacement
actuellement commencent le procès de la tuerie de Pittsburg dans lequel
un suprématiste blanc et entrer dans la synagogue en octobre 2018 a tiré
dans le tas à tuer 12 juillet le thème de son attaque c'était les Juifs
font venir les immigrants pour remplacer la nation américaine c'est un
reproche vieux comme le monde si je puis dire puisque les Juifs sont là
leur but c'est de détruire les nations on les appelle qu'on veut pas
dire les Juifs on dit les mondialistes les globalises comme vient de dire
Poutine il y a deux jours les les droits de Lomé etc etc pour pas dire
les yoopains on dit les droits de l'hommeiste et il paraît même qu'il y
a des jeux vidéo maintenant dans lesquels il y a un terme qui permet de
désigner les juifs sans les nommer bon voilà donc c'est quelque chose
qui est très directement une attaque contre tout le monde et il faut
qu'on soit vraiment unique contre ça oui oui j'aurais dit volontiers
dit un mot sur le 14 mai 1941 mais je crois que c'est peut-être pas
le lieu sachez quand même c'est la première rafle des Juifs à Paris
3700 comme la diolia juif étranger qui sont convoqués et qui a été
dit aussi pardon par Alexia qui ont été convoqués par un petit papier
qui était de couleur verdâtre c'est pour ça qu'on parle de l'arabe du
billet vert qui ont été convoqués dans les commissariats pour examen
de votre situation et puis le jour venu le 14 mai il y a eu cette offre
ses Juifs souvent étrangers ont été envoyés dans les camps du Loiret
Beaune-la-Rolande et Pithiviers et de là ils ont été tous déportés vers
Auschwitz assassiné donc c'est une date il y a souvent dans l'histoire
du racisme et de l'antisémitisme des coïncidences de dates hélas et je
crois qu'il est important qu'on est sain aussi en mémoire merci [Musique]
[Applaudissements] ok il y a une plaque derrière vous ne pourrez tous
aller admirer et vous prendre en photo devant la vieux verte c'était
juste pour terminer sur les suites de l'attentat en Tunisie pour qu'on se
rende compte ce qui se passe la plupart des gens quand ils font mémoire
aux victimes ils font mémoire aux victimes aux trois policiers et pas aux
2 juifs tués il y a cette idée que les Juifs même s'ils sont Tunisiens
sont pas chez eux en Tunisie on sait pas du coup où ils sont chez eux il y
a eu des suites c'est à dire que dès le lendemain il y a eu des vidéos
qui ont été tournées dans le souk avec des pour ceux qui connaissent
Djerba qui ont été tournés dans le souk de Djerba vers umtsuk etc avec
des T-Shirts floqué marqué Djerba essaye et plein de touristes avec à
qui on a demandé de porter le drapeau tunisien et de dire Djerba safe
Djerba safe sans jamais rappelé les noms des deux victimes juives et par
ailleurs le président n'est jamais venu à Djerba depuis l'attentat et
la seule préoccupation là tout de suite de la Tunisie quand on entend
les plateaux télé c'est la survie de la saison touristique de cet été
pourquoi je rajoute ça parce que c'est pour montrer que en fait je sais
qu'il y a des associations tunisiennes et que la Tunisie MC Juifs n'ont
pas douter mais d'un point de vue institutionnel c'est pas du tout le
cas donc il y a quand même ce truc qui vous regardez sur les réseaux
sociaux par exemple Djerba essaye on peut on peut atterrir sur vraiment des
discours assez dingues dont sur des plateaux télé une femme qui qui dit
que si jamais il y a eu cette attentat évidemment c'est à cause c'est en
représailles de Gaza et de la mort des enfants palestiniens je veux dire
c'est dit clairement alors que les deux victimes étaient tunisienne quoi
donc les Juifs sont chez eux nulle part selon des gens quoi voilà merci
[Applaudissements] merci beaucoup aura pour son soutien maintenant il y a
[Musique] il propose qu'on chante ben abial on n'oublie pas peut-être
qu'on peut le faire après la prise de parole maintenant la prise de
parole de JJR juif et juif révolutionnaire merci bonjour à tout le
monde je m'appelle Emmanuel je suis membre du groupe révolutionnaire je
voulais remercier en premier lieu les camarades d'orage qui ont permis
de se rassemblement a eu lieu je pense que avec le choc qu'on a eu à
l'annonce de cette nouvelle on avait besoin de se rassembler de se voir
en vrai c'était un moment difficile ça m'aidera du temps à passer et
voilà je vous remercie d'avoir fait quelque chose même si on n'avait pas
beaucoup de temps pour le faire le fait qu'on soit réuni aujourd'hui c'est
un ça réchauffe le coeur ça permet de se dire qu'on est pas tout seul
comme on a malheureusement trop souvent l'impression de l'être je vais je
vais pas je suis d'accord avec ce qu'on dit les camarades qui ont parlé
juste avant je vais pas répéter répéter je peux rajouter une ou deux
choses sur le fait que l'antisémitisme en Tunisie les structurelles depuis
longtemps maintenant on peut même parler d'antisémitisme d'État avec les
réactions du président qui a comme beaucoup de gens le font en France
se dit ah mais d'accord on 40 on en a sauvé et maintenant ils attaquent
des Palestiniens et on leur dit rien et eux ils disent rien là-dessus
c'est la réaction qui a eu le président en visite à Djerba non pardon
très récemment il a eu cette réaction juste de dire de faire le lien
tout de suite avec les le conflit en Israël et en Palestine et c'est
pas un hasard puisque depuis des années les régimes arabes utilisent
ce conflit pour pouvoir diviser les forces progressistes à l'intérieur
même de leur de leur pays et c'est pour ça que l'antisémitisme est si
fort aujourd'hui et qu'il a été diffusé de façon aussi forte que ce
soit en Égypte en Tunisie on le voit parce que l'antimisme il est très
fort dans la population le fait que les Juifs se sentent pas forcément à
leur en place là-bas qui sentent que leur place est menacée c'est s'en
est un signe fort en 1985 en 2002 il y a eu d'autres attentats c'était
pas encore l'époque de la théorie du Grand remplacement en tout cas pas
formulé comme ça ceci dit ce qu'on peut observer c'est que le fait que
le président tunisien aujourd'hui fasse ce lien avec la théorie du Grand
remplacement ça dit quelque chose de l'internationalisation des luttes
fascistes eux ils se mettent ensemble et il commence à se rendre compte
qu'ils ont des accointances et que peut-être c'est le moment de s'unir
on voit plein de regroupements de rapprochement auxquels on n'aurait pas
songé pendant très longtemps moi j'ai été choqué personnellement
que Kais parle de cette théorie du Grand remplacement on voit des blancs
parler de ça on n'imagine pas un chef d'État d'un pays arabe parlait de
ça de même on voit des rapprochements entre Dieudonné et l'extrême
droite israélienne par exemple on voit des rapprochements comme ça
entre des personnes qui se rendent compte qu'ils ont plus d'affinités
que ce qu'il ne pensait auparavant ça ça doit nous mettre la puce à
l'oreille sur une chose c'est que comme vous l'avez dit auparavant oui
on ne s'en sortira pas par des solutions nationales nationalistes on
s'en sortira que par des solutions internationales on s'en sortira que
dans l'union entre les personnes victimes de racisme et pas que d'ailleurs
entre toutes les personnes qui veulent lutter contre le racisme ce sera la
seule possibilité et dès aujourd'hui voilà je suis touché que qu'on
ait pu faire ce rassemblement pour pouvoir aller dans ce sens voilà je
pense qu'il faut qu'on songe à des stratégies pour pouvoir lutter de
façon beaucoup plus massive évidemment contre ces phénomènes parce
que eux s'organisent se rendent compte qu'ils sont forts se rendent compte
qu'ils sont d'accord ils prennent plein de prétextes ils prennent plein
de sujets de thèmes qui sur lesquels ils se rejoignent tous souvent
la nécrophobie en fait partie souvent l'homophobie en fait partie
l'utilisation de thèmes comme la pédocrinalité toutes les tous les
thèmes complotistes possibles font des alliances qu'on aurait pas imaginé
entre même des personnes qui se prétendaient de gauche il y a quelques
années et des personnes qui sont clairement identifiées à l'extrême
droite par nous depuis longtemps voilà donc je vous remercie beaucoup
d'être venus ça réchauffe le cœur et puis voilà merci beaucoup
camarade d'avoir organisé ce moment merci à tous [Applaudissements]
hier [Applaudissements] et donc organisé par orage que j'ai oublié de
présenter je m'en excuse c'est donc un collectif féministe de lutte
contre l'antisémitisme et il était organisé avec le soutien du rare du
réseau d'action contre l'antisémitisme et tous les racismes et le soutien
de JJR juif et juif révolutionnaire comme proposé iris on va juste dire
ben et à viel on n'oublie pas on pardonne pas et voilà je vous remercie
beaucoup d'être venu en fait on espère aussi que ce soit aussi le début
de la construction enfin ce sont encore une étape pour construire des
luttes féministes antiraciste pour lutter contre l'antisémitisme ici
et peut-être que ce serait bien qu'on continue à faire des choses par
rapport à cette question de l'antisémitisme en Tunisie en Afrique du Nord
ici aussi par des débats des discussions et encore des rassemblements et
des manifestations donc je vous invite peut-être à venir nous contacter
pour qu'on puisse discuter ensemble on peut aussi prendre le temps de
discuter et de voir qu'est-ce qu'on veut faire pour la suite c'est aussi
un comme ça par ailleurs on vous propose voilà on va on souhaiterait
lire le cadich donc la prière juive en hébreu pour les morts pour les
personnes décédées donc on vous invite à rester pendant pour réduire
les veines et alors donc tu seras un cash pour habiller la date Benjamin
Adam et tous les autres voilà on vous invite à rester pendant cette
lecture et à allumer des bougies avec nous si vous souhaitez il Gadal
chez miraba Allemagne pour calmer et mes chéries [Musique] je signale
au passage que la prière du Cadix n'est pas en hébreu mais en araméen
parce que c'était la langue parlée à l'époque c'est un peu décalé
mais pour ceux et celles ici que cela intéresse demain nous organisons
pas très loin d'ici au Carré de Baudouin 120 rue de Ménilmontant
une rencontre avec Lola Lafont écrivaine autour de son livre quand tu
écouteras cette chanson autour de la figure d'Anne Frank c'est demain à
19h si cela vous intéresse faites-nous le savoir maintenant en principe
il y a une inscription mais voilà on vous dira comment il faut faire
en tout cas on vous invite très largement à venir et à participer à
cette discussion il portera en partie sur l'ancien autisme merci à vous
allez on n'oublie pas on part de pas on parle pas on oublie pas on part
dans une part on souhaiterait juste aussi simplement mentionné et parler
notre camarade Cléo Cohen qui était là pendant l'attentat et Iris bien
sûr je pense à vous deux spécifiquement et il sera la classe merci à
vous d'être venu [Applaudissements] [Musique] [Musique]
