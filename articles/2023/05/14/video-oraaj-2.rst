
.. _oraaj_2023_05_14_1620:

==================================================================================================================================================================
2023-05-14 **Discours de ORAAJ au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- :ref:`oraaj`

Présentation de ORAAJ
==========================

- https://youtu.be/4tvmA71gb0Q?t=1620

Merci beaucoup à:ref:`JJR <jjr>`

donc ce rassemblement était organisé par :ref:`oraaj` que j'ai oublié de
présenter je m'en excuse

c'est donc un collectif féministe de lutte contre l'antisémitisme et il
était organisé avec le soutien:

- du :ref:`raar` du réseau d'action contre
  l'antisémitisme et tous les racismes et le soutien
- de :ref:`JJR <jjr>` juives et juifs révolutionnaires

comme proposé Iris on va juste dire

Ben(jamin) et Aviel (Haddad)on n'oublie pas on pardonne pas

et voilà je vous remercie
beaucoup d'être venu

en fait on espère aussi que ce soit aussi le début
de la construction enfin ce sont encore une étape pour construire des
luttes féministes antiracistes, pour lutter contre l'antisémitisme ici

et peut-être que ce serait bien qu'on continue à faire des choses par
rapport à cette question de l'antisémitisme en Tunisie en Afrique du Nord
ici aussi

par des débats, des discussions et encore des rassemblements et
des manifestations

donc je vous invite peut-être à venir nous contacter
pour qu'on puisse discuter ensemble

on peut aussi prendre le temps de
discuter et de voir qu'est-ce qu'on veut faire pour la suite

c'est aussi un appel pour ça.

On vous propose voilà on va on souhaiterait lire le Kaddich
=====================================================================

- https://youtu.be/4tvmA71gb0Q?t=1690
- https://fr.wikipedia.org/wiki/Kaddish
- :ref:`benjamin_haddad`
- :ref:`aviel_haddad`

Par ailleurs on vous propose

voilà on va

on souhaiterait lire le `Kaddich <https://fr.wikipedia.org/wiki/Kaddish>`_
donc la prière juive en hébreu pour les morts pour les
personnes décédées donc on vous invite à rester pendant

pour réduire les peines et alors donc tu seras un Kaddish pour :ref:`Aviel Haddad <aviel_haddad>`,
:ref:`Benjamin Haddad <benjamin_haddad>`  et tous les autres.

voilà on vous invite à rester pendant cette
lecture et à allumer des bougies avec nous si vous souhaitez

Début du kaddish
===================

- https://youtu.be/4tvmA71gb0Q?t=1729

::

    lecture du kaddish en arraméen

Fin du kaddish
==================

- https://youtu.be/4tvmA71gb0Q?t=1798


.. _albert_herszkowicz_2023_05_14_1800:

2023-05-14 Commentaire d'Albert sur le Kaddish
==========================================================

- https://youtu.be/4tvmA71gb0Q?t=1800
- https://fr.wikipedia.org/wiki/Kaddish

Je signale au passage que la prière du Kaddish n'est pas en hébreu mais
en araméen parce que c'était la langue parlée à l'époque.


Annonce rencontre avec Lola Lafont par Albert
===================================================

c'est un peu décalé
mais pour ceux et celles ici que cela intéresse demain nous organisons
pas très loin d'ici au Carré de Baudouin 120 rue de Ménilmontant
une rencontre avec Lola Lafont écrivaine autour de son livre "quand tu
écouteras cette chanson" autour de la figure d'Anne Frank c'est demain à
19h

si cela vous intéresse faites-nous le savoir maintenant en principe
il y a une inscription mais voilà on vous dira comment il faut faire

en tout cas on vous invite très largement à venir et à participer à
cette discussion

il portera en partie sur l'antisémitisme merci à vous

Ben, Aviel on n'oublie pas on pardonne pas
==============================================

- https://youtu.be/4tvmA71gb0Q?t=1860

::

    Ben, Aviel on n'oublie pas on pardonne pas
    Ben, Aviel on n'oublie pas on pardonne pas


- https://youtu.be/4tvmA71gb0Q?t=1871

on souhaiterait juste aussi simplement mentionné et parler
notre camarade Cléo Cohen qui était là pendant l'attentat et Iris bien
sûr je pense à vous deux spécifiquement

merci à vous d'être venu

Fin
=====

- https://youtu.be/4tvmA71gb0Q?t=1891
