.. index::
   pair Olia ; Rassemblement suite à l'attentat antisémite de Djerba (2023-05-14)

.. _olia_2023_05_14_638:
.. _raar_2023_05_14_638:

==================================================================================================================================================================
2023-05-14 **Discours de Olia (RAAR) au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed

Présentation du RAAR par une militante d'ORAAJ
===================================================

- https://youtu.be/4tvmA71gb0Q?t=638


merci maintenant prise de parole du RAAR réseau d'action
entre l'antisémitisme et tous les racisme

.. _olia_2023_05_14:

2023-05-14 Discours d'Olia au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba
======================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=649

Donc il y a quelques jours quand
les copines d':ref:`oraaj` ont proposé d'organiser ce rassemblement j'avoue
que j'étais assez soulagé parce qu'il fallait faire quelque chose

et que il fallait que quelque chose se mette en place et j'étais super contente
que vous soyez le mouvement moteur de de rassemblement quoi

donc merci de nous avoir proposé de participer

et j'espère qu'on a fait tout ce
qu'il fallait pour vous aider au maximum pour organiser au pied levé ce
rassemblement

https://youtu.be/4tvmA71gb0Q?t=700

moi j'aimerais revenir juste parce que j'ai des liens assez
forts avec la Tunisie sur quelque chose qui du coup sur lequel on travaille
au :ref:`RAAR <raar>` depuis longtemps puisque on est l'organisation qui luttons
contre l'antisémitisme et tous les racismes

Pour nous le lien entre l'antisémitisme et les autres racismes est évident
=================================================================================

- https://youtu.be/4tvmA71gb0Q?t=715
- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed

**et pour nous le lien entre l'antisémitisme et les autres racismes est évident**

en fait c'est sûr les propos de Kaïs Saïed en février 23 qui a ciblé??
les migrants subsahariens allant jusqu'à les
accuser de vouloir modifier la composition démographique de la Tunisie

il a parlé de hordes de migrants clandestins qui sèment selon lui violences
et crimes à travers le pays

ça a pas fait grand bruit à l'époque
enfin c'est à dire que les associations antiracistes ont commencé à
dire mince ça sent pas bon mais nous à ce moment-là déjà on s'est
rendu compte que quelque chose était en train de bouger en Tunisie

et il reprend carrément donc la théorie du grand emplacement
avec l'idée que la population arabe est remplacée par la population
donc lui il dit africaine comme si la Tunisie n'était pas en Afrique

mais donc on entend noire

ça fait écho propos de Trump sur dans la
même veine les travailleurs immigrés mexicains

ça fait écho à plein de dirigeants populistes comme ça qui vont reprendre à chaque
fois stigmatiser une partie de la population pour répondre à
des problèmes imminents dans leur propre pays

et c'est pas un hasard
si enfin selon moi je pense si quelque temps après les Juifs ont été
attaqués en Tunisie en fait parce que la théorie du grand remplacement
c'est toujours cette idée que il y a un processus de substitution de
population avec cette idée derrière d'un changement de civilisation
qui est soutenu voire organisé par une élite politique intellectuelle
et médiatique qui est qualifiée de remplaciste

et souvent in fine c'est
les Juifs et si c'est pas les Juifs d'abord ça va être les subsahariens
puis  ensuite les Juifs

donc c'est vrai que dès février en fait
on se disait qu'il se passait quelque chose en Tunisie qui sentait qui
sentait mauvais quoi

et cette vieille idéologie raciste et antisémite
elle date pas d'aujourd'hui elle date de la Troisième République

des nationalismes d'avant guerre

qui peut viser en fonction des moments politiques soit les Juifs les Arméniens les Italiens à Marseille un
moment c'était en 1901 on disait que la population de Marseille allait
être remplacée par les Italiens

soit les Maghrébins soit et maintenant les Noirs au Maghreb

On peut pas ne pas se rendre compte que c'est quelque chose qui va se passer dans tous les pays à un moment
================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=870

Donc pourquoi je parle de ça parce qu'en fait je
pense que l'antiracisme peut pas avec ce genre d'attaque et ce genre de
mouvement global international de masse réactionnaire **on peut pas ne
pas se rendre compte que c'est quelque chose qui va se passer dans tous
les pays à un moment**

et si jamais on fait pas front dans l'antiracisme
à se rendre compte que chaque minorité peut être à un moment prise
à partie et que tout le monde se lève pas d'un seul homme à chaque
fois

::

    d'une seule femme

on n'arrivera pas à combattre à combattre ce
mouvement de masse réactionnaire voilà je vais laisser la parole à
Albert

Fin
====

- https://youtu.be/4tvmA71gb0Q?t=917


.. figure:: images/raar_1.png
   :align: center

   Olia, https://nitter.net/Oliouchka/rss
