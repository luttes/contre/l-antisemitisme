
.. _manu_sanders_2023_05_14:
.. _jjr_2023_05_14:

==================================================================================================================================================================
2023-05-14 **Discours de Emmanuel Sanders (JJR) au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- :ref:`emmanuel_sanders`

Commentaire sur un chant
============================

- https://youtu.be/4tvmA71gb0Q?t=1308

merci beaucoup au RAAR pour son soutien maintenant il y a

il propose qu'on chante ben abial on n'oublie pas peut-être
qu'on peut le faire après la prise de parole

maintenant la prise de
parole de JJR juif et juif révolutionnaire

Discours de Emmanuel Sanders (JJR)
=======================================

- https://youtu.be/4tvmA71gb0Q?t=1341

merci bonjour à tout le monde

je m'appelle Emmanuel je suis membre du groupe JJR (Juives et Juifs Révolutionnaires)

je voulais remercier en premier lieu les camarades d':ref:`oraaj` qui ont permis
que ce rassemblement ait lieu

je pense que avec le choc qu'on a eu à
l'annonce de cette nouvelle on avait besoin de se rassembler de se voir
en vrai c'était un moment difficile ça m'aidera du temps à passer et

voilà je vous remercie d'avoir fait quelque chose même si on n'avait pas
beaucoup de temps pour le faire

le fait qu'on soit réuni aujourd'hui c'est
un ça réchauffe le coeur

ça permet de se dire qu'on est pas tout seul

comme on a malheureusement trop souvent l'impression de l'être

je vais je
vais pas je suis d'accord avec ce qu'on dit les camarades qui ont parlé
juste avant je vais pas répéter répéter


L'antisémitisme en Tunisie est structurel depuis longtemps
===============================================================

- https://youtu.be/4tvmA71gb0Q?t=1395
- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed

je peux rajouter une ou deux
choses sur le fait que l'antisémitisme en Tunisie est structurel depuis
longtemps

maintenant on peut même parler d'antisémitisme d'État avec les
réactions du président Kaïs Saïed  qui a **comme beaucoup de gens le font en France**
se dit ah mais d'accord on 40 on en a sauvé et maintenant ils attaquent
des Palestiniens et on leur dit rien et eux ils disent rien là-dessus.

c'est la réaction qui a eu le président en visite à Djerba
non pardon c'était à côté très récemment


Les régimes arabes utilisent ce conflit pour pouvoir diviser les forces progressistes à l'intérieur même de leur de leur pays
================================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=1430

Il a eu cette réaction juste de dire de faire le lien
tout de suite avec le conflit en Israël et en Palestine et c'est
pas un hasard puisque **depuis des années les régimes arabes utilisent
ce conflit pour pouvoir diviser les forces progressistes à l'intérieur
même de leur de leur pays**

et c'est pour ça que l'antisémitisme est si fort aujourd'hui et qu'il a
été diffusé de façon aussi forte que ce soit en Égypte en Tunisie on le
voit parce que l'antimisme il est très fort dans la population

le fait que les Juifs se sentent pas forcément à
leur en place là-bas qui sentent que leur place est menacée c'est s'en
est un signe fort

en 1985 en 2002 il y a eu d'autres attentats c'était
pas encore l'époque de la théorie du Grand remplacement en tout cas pas
formulé comme ça


Le fait que le président tunisien aujourd'hui fasse ce lien avec la théorie du Grand remplacement ça dit quelque chose de l'internationalisation des luttes fascistes
========================================================================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=1484

Ceci dit ce qu'on peut observer c'est que le fait que
le président tunisien aujourd'hui fasse ce lien avec la théorie du Grand
remplacement **ça dit quelque chose de l'internationalisation des luttes
fascistes**

eux ils se mettent ensemble et ils commencent à se rendre compte
qu'ils ont des accointances et que peut-être c'est le moment de s'unir

on voit plein de regroupements de rapprochement auxquels on n'aurait pas
songé pendant très longtemps

moi j'ai été choqué personnellement
que Kaïs Saïed  parle de cette théorie du Grand remplacement

on voit des blancs
parler de ça on n'imagine pas un chef d'État d'un pays arabe parlaer de
ça


.. _duieudonne_2023_05_14:

De même on voit des rapprochements entre Dieudonné et l'extrême droite israélienne
=======================================================================================

- https://youtu.be/4tvmA71gb0Q?t=1522

De même on voit des rapprochements entre Dieudonné et l'extrême
droite israélienne par exemple

on voit des rapprochements comme ça
entre des personnes qui se rende compte qu'ils ont plus d'affinités
que ce qu'il ne pensait auparavant

ça ça doit nous mettre la puce à
l'oreille sur une chose c'est que comme vous l'avez dit auparavant oui
on ne s'en sortira pas par des solutions nationales nationalistes

on
s'en sortira que par des solutions internationales

on s'en sortira que
dans l'union entre les personnes victimes de racisme

et pas que d'ailleurs

entre toutes les personnes qui veulent lutter contre le racisme

ce sera la
seule possibilité et dès aujourd'hui voilà je suis touché que qu'on
ait pu faire ce rassemblement pour pouvoir aller dans ce sens

voilà

je
pense qu'il faut qu'on songe à des stratégies pour pouvoir lutter de
façon beaucoup plus massive évidemment contre ces phénomènes parce
que eux s'organisent se rendent compte qu'ils sont forts se rendent compte
qu'ils sont d'accord ils prennent plein de prétextes ils prennent plein
de sujets de thèmes qui sur lesquels ils se rejoignent tous

souvent
la négrophobie en fait partie souvent l'homophobie en fait partie

l'utilisation de thèmes comme la pédocriminalité

toutes les tous les
thèmes complotistes possibles font des alliances qu'on aurait pas imaginé
entre même des personnes qui se prétendaient de gauche il y a quelques
années et des personnes qui sont clairement identifiées à l'extrême
droite par nous depuis longtemps

voilà donc je vous remercie beaucoup
d'être venus ça réchauffe le cœur et puis voilà merci beaucoup
aux camarades d'avoir organisé ce moment merci à tous

Fin
========

- https://youtu.be/4tvmA71gb0Q?t=1620


.. figure:: images/jjr_1.png
   :align: center

   Emmanuel
