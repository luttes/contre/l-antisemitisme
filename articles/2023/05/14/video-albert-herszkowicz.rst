.. index::
   pair Albert Herszkowicz ; Rassemblement suite à l'attentat antisémite de Djerba (2023-05-14)

.. _albert_herszkowicz_2023_05_14_917:
.. _raar_2023_05_14_917:

==================================================================================================================================================================
2023-05-14 **Discours de Albert Herszkowicz (RAAR) au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba**
==================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed


|AlbertHerszkowicz| Discours de Albert Herszkowicz  au Rassemblement suite à l'attentat antisémite dans la synagogue de la Ghriba à Djerba
=================================================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=917

je voudrais juste dire un mot l'antisémitisme en Tunisie n'est pas
un phénomène nouveau

hélas nous avons eu un signaler ces dernières
années de nombreuses campagnes antisémites sur le thème les Juifs de
Tunisie sont pas des vrais tunisiens

ce sont des pièces rapportées on connaît le thème

ils sont ici ils sont en France d'ailleurs des juifs
tunisiens ont été tués lors de l'attentat de l'hyper cacher un des un
des fils du rabbin de Tunis a été tué lors de l'attentat de l'hyper
cacher

et de manière plus générale c'est quelque chose quand il y a eu
un ministre du Tourisme qui devait être nommé monsieur Trabelsi le fait
qu'il soit d'origine juive a fait qu'il a été empêché de nominations en
tout cas on en a pas parlé

bref il y a de nombreux phénomènes y compris
dans la gauche politique la présence de militants juifs dans le parti
communiste tunisien et dans tous les mouvements de libération a été
longtemps occulté au point que des gens étaient, ont changé leur nom
pour pas qu'il y ait trop de militants juifs dans ces partis.


C'est quelque chose qui existe qui s'est accentué comme l'a dit Olia avec l'arrivée au pouvoir de Kaïs Saïed
==================================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=1001
- https://fr.wikipedia.org/wiki/Ka%C3%AFs_Sa%C3%AFed


voilà

donc c'est quelque chose qui existe qui s'est accentué comme l'a dit Olia
avec l'arrivée au pouvoir de Kaïs Saïed  qui est un nationaliste d'exclusion
comme il l'a montré à l'encontre des subsahariens

oui et bien sûr

et juste sur les Subsahariens et le grand remplacement il faut dire les
choses comme elles sont, encore plus directement.

.. _grand_remplacement_2023_11_09:

Les racistes, les fascistes considèrent que ce sont les Juifs qui organisent le grand remplacement
======================================================================================================

- https://youtu.be/4tvmA71gb0Q?t=1030
- https://fr.wikipedia.org/wiki/Fusillade_dans_une_synagogue_de_Pittsburgh

**Les racistes les fascistes considèrent que ce sont les Juifs qui organisent
le grand remplacement**

Actuellement commence le procès de la tuerie de Pittsburg dans lequel
un suprémaciste blanc et entrer dans la synagogue en octobre 2018 a tiré
dans le tas à tuer 12 juifs

le thème de son attaque c'était les Juifs
font venir les immigrants pour remplacer la nation américaine

c'est un
reproche vieux comme le monde si je puis dire puisque les Juifs sont là
leur but c'est de détruire les nations

on les appelle qu'on veut pas
dire les Juifs on dit les mondialistes, les globalises comme vient de dire
Poutine il y a deux jours

les les droits de lhommsite etc etc

pour pas dire les youpins on dit les droits de l'hommiste

et il paraît même qu'il y
a des jeux vidéo maintenant dans lesquels il y a un terme qui permet de
désigner les juifs sans les nommer

bon voilà donc c'est quelque chose
qui est très directement une attaque contre tout le monde et il faut
qu'on soit vraiment unis contre ça

oui oui j'aurais dit volontiers
dit un mot sur le 14 mai 1941 mais je crois que c'est peut-être pas
le lieu

sachez quand même c'est la première rafle des Juifs à Paris

3700 comme l'a dit Olia

juifs étrangers qui sont convoqués et qui a été
dit aussi

::

    pardon par Alexia


qui ont été convoqués par un petit papier
qui était de couleur verdâtre c'est pour ça qu'on parle de la rafle du
billet vert

qui ont été convoqués dans les commissariats pour examen
de votre situation et puis le jour venu le 14 mai il y a eu cette rafle

ces Juifs souvent étrangers ont été envoyés dans les camps du Loiret
Beaune-la-Rolande et Pithiviers et de là ils ont été tous déportés vers
Auschwitz et assassinés donc c'est une date il y a souvent dans l'histoire
du racisme et de l'antisémitisme des coïncidences de dates hélas et je
crois qu'il est important qu'on est ça aussi en mémoire

merci

Fin
====

- https://youtu.be/4tvmA71gb0Q?t=1169



.. figure:: images/raar_2.png
   :align: center

   Albert
