.. index::
   pair: Attentat; Djerba
   pair Victime ; Aviel Haddad (2023-05-10)
   pair Victime ; Benjamin Haddad (2023-05-10)

.. _djerba_2023_05_14:

=======================================================================================================================================================
2023-05-14 **Rassemblement suite à l'attentat antisémite de Djerba, dimanche 14 mai 2023 à 16h sur la Place des Fêtes**
=======================================================================================================================================================

.. toctree::
   :maxdepth: 3

   video-attentat-antisemite-a-djerba-on-oublie-pas-on-pardonne-pas
   annonces
