
.. _annonces_djerba_2023_05_14:

=============================================================================================================================================
2023-05-14 **Annonces, divers**
=============================================================================================================================================

- :ref:`djerba_memorial_98_2023_05_14`

#AvielHaddad #BenjaminHaddad #AbedlmajidAig #MaherElArbi #KheireddineLafi

.. figure:: images/aviel_et_benjamin_haddad.png
   :align: center

   Aviel et Benjamin Haddad


.. figure:: images/benjamin_haddad.png
   :align: center

   Merci @OM_Officiel pour ce merveilleux hommage à Benjamin Haddad,
   juif marseillais tué dans l’attentat terroriste antisémite de la
   synagogue de la Ghriba à Djerba.


Annonce de la manifestation
===============================


Pour signer, nous écrire à: oraajuive@gmail.com

.. figure:: images/oraajuive_1.png
   :align: center

.. figure:: images/oraajuive_2.png
   :align: center

Vidéo Attentat antisémite à Djerba : "On oublie pas, on pardonne pas
=======================================================================

- https://youtube.owacon.moe/watch?v=4tvmA71gb0Q (Attentat antisémite à Djerba : "On oublie pas, on pardonne pas)
- https://www.youtube.com/watch?v=4tvmA71gb0Q


Remerciements sur instagram
==============================

- https://www.instagram.com/p/CsN-bqJr_R1/?utm_source=ig_web_copy_link&igshid=MzRlODBiNWFlZA==

::


    Photo de profil de oraaj___
    RV AUJOURDHUI A 16 PLACE DES FETES Merci aux camarades pour les soutiens @raar_france
    @juifvesrevolutionnaires
    @agfeministeparisbanlieue
    Acceptess- T
    @agfeministemontreuil
    @collages.judeites.queer
    @collages_feministes_juifves_bx
    @collages_feministes_auber93
    @decolonisonslefeminisme
    @diivineslgbtqi_plus
    @feministesrevolutionnaires
    @marchefeministeantiraciste
    Mémorial 98
    @collectif_ntarajel
    @collectif_qra
    @pridedesbanlieues
    @raizesarrechas
    @tapages_collectif




.. figure:: images/les_5_victimes.png
   :align: center


Le rassemblement du 14 mai annoncé sur @Mediapart A partager et diffuser
===================================================================================

- https://nitter.cz/RAAR2021/status/1657080149592924168#m
- https://blogs.mediapart.fr/raar/blog/120523/rassemblement-suite-lattentat-antisemite-de-djerba-dimanche-14-mai
- https://www.instagram.com/oraaj___/

Rassemblement antiraciste contre l'antisémitisme ce dimanche 14 mai à 16h s
ur la Place des Fêtes (sortie métro).

En hommage aux victimes de l'attaque antisémite meurtrière qui a eu lieu
ce mardi 9 mai à la Ghriba à Djerba, et en soutien aux personnes ciblées
par cette attaque, dont des camarades et ami·es présent·es sur place

.. figure:: images/synagogue_la_ghriba.png
   :align: center


Rassemblement antiraciste contre l'antisémitisme ce dimanche 14 mai à 16h
à Place des Fêtes (Paris 19ème-sortie du métro), en hommage aux victimes
de l'attaque antisémite meurtrière qui a eu lieu ce mardi 9 mai à la
Ghriba à Djerba,  et en soutien aux personnes ciblées par cette attaque,
dont des camarades et ami.es présent.es sur place, à l'appel de
l'ORAAJuive (organisation révolutionnaire antiraciste antipatriarcale juive)
avec le soutien du Réseau d'actions contre l’Antisémitisme et tous les
Racismes ( RAAR) et de JJR.


- https://fediverse.owacon.moe/Papiillon/status/1656970943762145281#m

On est en train de repousser les limites de l'indécénce C'est la 3eme attaque
sur la ghriba depuis 1985 et la majorité des plateaux expliquent qu'il
n'y a pas de problème d'antisetimisme en Tunisie avec comme arguments
"on est pas antisémites puisqu'on est sémites".


En Tunisie, le choc et des questions après l’attentat contre une synagogue de Djerba
=========================================================================================

- https://www.mediapart.fr/journal/international/120523/en-tunisie-le-choc-et-des-questions-apres-l-attentat-contre-une-synagogue-de-djerba

:download:`pdfs/Djerba_Mediapart_2023_05_12.pdf`

L’attaque menée par un agent de la garde nationale mardi à proximité de
la synagogue de la Ghriba, lors d’un pèlerinage juif annuel, a fait cinq
morts, deux parmi les fidèles, dont un Franco-Tunisien.

L’île de Djerba est traumatisée et les autorités sont critiquées pour
leur manque de réaction.

DjerbaDjerba (Tunisie).– Dans le quartier de la Hara Kbira, à Djerba,
juifs et musulmans cohabitent depuis des décennies. Les échoppes des
vendeurs de bricks (sorte de chaussons frits traditionnels du Maghreb)
sont mitoyennes des maisons.

Dans les ruelles, les enfants des deux confessions se croisent habituellement
sur le chemin de l’école.

Mais jeudi 11 mai, deux jours après l’attentat contre la synagogue de
la Ghriba, la plupart des boutiques sont fermées, en signe de deuil.

Le silence est pesant et les rares passants disent ne pas vouloir parler.
« Nous sommes encore sous le choc », explique une femme qui balaie devant
sa maison.

Benjamin Haddad, 41 ans, et Aviel Haddad, 30 ans, deux cousins, l’un français,
l’autre tuniso-israélien, qui assistaient au pèlerinage annuel de la Ghriba
à l’occasion de la fête juive de Lag Ba’omer, ont été tués mardi dans
l’attaque.

Trois policiers également. Le précédent acte terroriste contre la synagogue
de la Ghriba remontait à 2002, lorsqu’une attaque au camion piégé revendiquée
par Al-Qaïda avait fait 21 morts.

Cette fois, les motivations de l’assaillant et son profil restent encore flous.


- http://nitter.unixfox.eu/laregledujeuorg/status/1656273646061592576#m

L’attaque perpétrée hier par un gendarme contre la synagogue la Ghriba a
provoqué la mort de quatre personnes, dont un Français.
@Knobel7 se souvient de ce lieu d'une rare beauté et revient sur la
terrifiante israélophobie qui sévit en #Tunisie. #Djerba bit.ly/3LVya4O



2023-05-10 Nous apprenons avec une infinie tristesse l’attentat d’hier soir aux abords de la synagogue de la Ghriba à Djerba
==============================================================================================================================

- https://kolektiva.social/@jjr/110344936675813951

Nous apprenons avec une infinie tristesse l’attentat d’hier soir aux abords
de la synagogue de la Ghriba à Djerba.

Cette semaine se tenait le pèlerinage annuel réunissant près de 5000
personnes sur cette île tunisienne qui compte une communauté juive d’environ
1300 personnes (la plus importante d’Afrique et du Maghreb).

Au lendemain de l’attentat, nous comptons 4 morts: 2 gendarmes et 2 fidèles
juifs, un marseillais originaire de Djerba et son cousin djerbien.

Nos pensées vont aux familles mais aussi à toute la communauté djerbienne
et tunisienne meurtrie.

Nous ne connaissons pas encore les motivations du tueur, mais nous savons
que c’est un gendarme tunisien.

La synagogue de la Ghriba avait déjà été une cible d’attentat en 2002
qui avait fait 19 morts.
Cet endroit est un symbole pour toute la communauté sépharade, c’est la
plus vieille synagogue d’Afrique, et les juifs djerbiens représentent
une communauté juive arabe encore debout et active.

Un rassemblement est prévu à 18h au métro Belleville en hommage aux victimes,
nous y serons et invitons nos lecteurs et lectrices à y participer.
