.. index::
   pair: JJR ; 2023-11-14 (12 novembre 2023 : un premier bilan)

.. _jjr_2023_11_14:

===============================================================
2023-11-14 |jjr| JJR **12 novembre 2023 : un premier bilan**
===============================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/14/12-novembre-un-premier-bilan/
- https://kolektiva.social/@jjr/111408249310270954

Introduction
=================

De nombreux débats ont traversé la gauche autour de la manifestation
du 12 novembre 2023 contre l’antisémitisme.

- Fallait-il y aller au risque de manifester aux côtés de l’extrême-droite ?
- Fallait-il organiser un contre-rassemblement ?
- Fallait-il rester chez soi ?


.. _jjr_ras_2023_11_14:

Echec du rassemblement des jeunesses de gauche au Square des martyrs juifs du Vélodrome d’Hiver
================================================================================================

- :ref:`autres_2023_11_11`

Plusieurs organisations de jeunesse de gauche ont fait le choix
d’organiser un autre rassemblement le matin même au `Square des martyrs
juifs du Vélodrome d’Hiver <https://www.openstreetmap.org/#map=19/48.85385/2.28700>`_
Nous :ref:`l’avons relayé sur les réseaux sociaux <autres_2023_11_11>`
et certain·es de nos camarades y étaient présent·es.

**Force est d’admettre que ce rendez-vous n’a pas rencontré le succès
escompté : des contre-manifestant·es, issu·es de la minorité juive et
choqué·es par la présence de député·es LFI dans ce lieu symbolique
ont interrompu le rassemblement**.

Si une telle situation est désastreuse, elle était malheureusement prévisible
étant donné les déclarations problématiques, depuis plus d’un mois,
d’une partie des organisations de gauche qui appelaient à s’y rendre,
poussant à remettre en question la sincérité de l’initiative.

Le constat est par ailleurs triste au vu du faible nombre de manifestant·es :
**cette initiative n’a pas mobilisé la gauche et encore moins les Juifs
et les Juives**.


Cela signifie prendre réellement au sérieux, dans la durée, la question de l’antisémitisme et non se contenter de se raccrocher aux branches
===============================================================================================================================================

Parler aux Juifs et aux Juives et ne pas rester dans l’entre soi est un enjeu
pour la gauche si elle veut sortir de l’impasse sur ces questions.

**Cela signifie prendre réellement au sérieux, dans la durée, la question de
l’antisémitisme et non se contenter de se raccrocher aux branches**.


.. _jjr_discussion_2023_11_14:

Après de longues discussions, nous avons collectivement fait le choix de nous rendre aux rassemblements institutionnels organisés dans plusieurs villes
===========================================================================================================================================================

- :ref:`volia_2023_11_09_3383`

Après de longues discussions, nous avons collectivement fait le choix de
nous rendre aux rassemblements institutionnels organisés dans plusieurs
villes. Non pour marcher paisiblement, **mais pour faire que ce que les
antifascistes ont toujours fait : ne pas laisser la rue aux fascistes**.

Il s’agissait pour nous d’empêcher que l’extrême-droite, héritière
des nazis et des collabos, puisse prétendre en toute impunité être
du côté des Juif·ves.

.. _jjr_rapaces:

Les rapaces (Enquête sur la "Mafia Varoise" de Marine Le Pen)
----------------------------------------------------------------------

- :ref:`les_rapaces`

Cela est d’autant plus insupportable étant donné l’historique du RN,
fondé par d’anciens nazis, dirigé pendant des décennies par un antisémite
notoire, ayant compté Alain Soral parmi ses cadres dirigeants, mais
également étant donné son actualité puisque sa collusion avec des antisémites
notoires dont le GUD a été rappelée par plusieurs enquêtes ces dernières
années et alors même :ref:`qu’un livre "Les rapaces", NDLR) de la journaliste Camille Vigogne le Coat
sorti il y a quelques jours (le 2 novembre 2023) fait état de l’antisémitisme
et du racisme de la municipalité vitrine du RN à Fréjus <les_rapaces>`.


**L’extrême-droite n’était présente hier que dans le but de marginaliser un peu plus la minorité musulmane en instrumentalisant nos souffrances**
====================================================================================================================================================

Ne nous leurrons pas quant à son agenda politique raciste : **l’extrême-droite
n’était présente hier que dans le but de marginaliser un peu plus la
minorité musulmane en instrumentalisant nos souffrances**.

Par ailleurs, nous avons aussi décidé de rejoindre ces manifestations
contre l’antisémitisme car nous avons estimé qu’une faible
mobilisation aurait un effet dévastateur sur le moral de la minorité
juive, déjà traversée par un profond sentiment d’isolement.


.. _golem_jjr_2023_11_14:

|golem| **Dans une démarche antifasciste nous avons participé à la construction du collectif Golem**
=======================================================================================================

C’est ainsi dans une démarche antifasciste que nous avons participé
à la construction du :ref:`collectif Golem <golem>`, créé pour l’occasion à
Paris avec des camarades d’autres organisations et des individus,
et que nous avons ensemble tenté de perturber le show médiatique
du RN.

Nous nous sommes mobilisé·es avec la même ambition à Marseille ou à
Besançon où nos camarades ont été violemment pris à partie.

Cette fois-ci, nous avons été trop peu nombreux·ses pour
sortir l’extrême-droite des rassemblements, mais nous avons montré
que **celle-ci ne peut impunément venir faire son beurre sur la lutte
contre l’antisémitisme**.

L’extrême-droite n’a pu défiler que sous la protection de la police
(qui a violenté plusieurs de nos camarades) et de groupuscules fascistes
(dont :ref:`l’odieuse Ligue de Défense Juive, organisation prétendant défendre
notre minorité mais qui a choisi de s’en prendre à des Juif·ves afin
d’escorter des antisémites <streetpress_2023_11_13>`).

Cette action a été un succès, notre message de contestation de la
normalisation du RN a été massivement entendu et relayé par la presse.

.. _jjr_participation_2023_11_14:

**Les participant·es étaient surtout des personnes sincèrement et légitimement choquées par la recrudescence de la violence antisémite**
==========================================================================================================================================

Suite à la manifestation, **certaines personnalités de gauche, notamment
issues de LFI, ont tenté de minorer cette mobilisation** ou l’ont
présenté comme un rassemblement d’extrême-droite.

Ces réactions sont bien entendues **puériles et simplificatrices** : si des
éléments d’extrême-droite ont bien participé à ces manifestations, comme
ils participent à une grande partie des mobilisations sociales de ces
dernières années (notamment contre le pass vaccinal), si le gouvernement
et les organisateurs poursuivaient eux aussi leur propre agenda politicien
et réactionnaire, les participant·es étaient surtout des personnes
sincèrement et légitimement choquées par la recrudescence de la violence
antisémite.

Une telle démarche étant fondamentalement antiraciste, la place des
antiracistes était à leurs côtés pour pousser dehors les fascistes, pour
combattre la stratégie de normalisation du RN menée par les organisateurs,
pour lutter contre toute instrumentalisation raciste, d’où qu’elle vienne,
de la lutte contre l’antisémitisme en s’adressant aux Juif·ves et non en
restant dans l’entre soi militant.


.. _jjr_gauche_2023_11_14:

**Comment en sommes-nous arrivé·es là ?**
============================================

Comment en sommes-nous arrivé·es là ? Deux éléments expliquent la
situation inextricable dans laquelle les Juif·ves de gauche ont été
mis·es.

**Premier point : la gauche incapable d’organiser un évènement pour faire face à la flambée de l’antisémitisme**
------------------------------------------------------------------------------------------------------------------

Le premier, c’est que, malgré un début de réflexion, **la gauche, largement
décrédibilisée sur le sujet, n’a pas été capable d’organiser un évènement
pour faire face à la flambée de l’antisémitisme qui dure depuis plus
d’un mois**.

Il a fallu attendre l’initiative de Gérard Larcher et de Yaël Braun-Pivet
pour que cela soit sérieusement envisagé.

**Ceci est pourtant le rôle historique de notre camp social et on observe
ici à quel point il s’est perdu**.


**Deuxième point : la banalisation de l’extrême droite**
-----------------------------------------------------------

Le deuxième élément est la banalisation de l’extrême droite.

**Celle-ci a été accélérée par les discours et la politique menés par Macron et
ses gouvernements successifs qui lui empruntent de plus en plus d’idées,
ainsi que par leur refus d’empêcher le RN d’assister aux marches et
rassemblements de ce dimanche 12 novembre 2023**.


Nous le répéterons tant qu’il le faudra : quel que soit le contexte,
aussi désespérants que soient les abandons et les compromissions de
larges pans de la gauche, **l’extrême-droite est l’ennemie des Juif·ves
et de toutes les minorités**.

**Nous lutterons de toutes nos forces pour le rappeler à chaque fois.**

Nous refusons également de considérer ce gouvernement comme un allié
============================================================================

Nous refusons également de considérer ce gouvernement comme un allié car
nous subissons depuis six ans déjà sa casse du système social et du
service public qui favorise la montée du confusionnisme et de tous les
racismes.

**Sa politique n’est aucunement antiraciste, elle prend particulièrement
pour cible la minorité musulmane**.


Nous espérons que notre action constituera les prémices de nombreuses
initiatives sociales pour remettre la lutte contre l’antisémitisme à
la place qui est la sienne.

Nous appelons ainsi **tous les courants de la gauche à se réengager urgemment dans la lutte contre l’antisémitisme**
=======================================================================================================================

Nous appelons ainsi **tous les courants de la gauche à se réengager
urgemment dans la lutte contre l’antisémitisme**.

**Contre l’antisémitisme et tous les racismes d’où qu’ils viennent,
organisons l’autodéfense !**
