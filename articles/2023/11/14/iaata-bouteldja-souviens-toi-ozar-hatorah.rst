.. index::
   pair: Antisémitisme ; Terra Nova (Bouteldja, souviens-toi Ozar Hatorah)

.. _toulouse_2023_11_14:

=========================================================================================================================
2023-11-14 **Bouteldja : souviens-toi Ozar Hatorah** par quelques féministes anti-autoritaires de l’Est Toulousain
=========================================================================================================================

- https://iaata.info/Bouteldja-souviens-toi-Ozar-Hatorah-6230.html
- :ref:`ozar_hatorah_2012`

Quand l’antisémitisme s’invite dans une librairie de gauche toulousaine.

#JonathanSandler #AriéSandler #GabrielSandler #MyriamMonsonego #OzarHatorah


:download:`Revendication au format PDf <pdf/ozar.pdf>`

Introduction
==============

Si nous choisissons de publier cette revendication plus de deux semaines
après le collage, c’est parce que la situation internationale n’a
eu de cesse de nous occuper depuis.


Ces positions tenues conjointement sont non seulement possibles, mais même impératives
========================================================================================

Dissipons d’avance toute confusion au sujet de nos intentions : si
nous condamnons le massacre pogromiste et les enlèvements du 7 octobre 2023
commis par le Hamas, et revendiquons la libération des otages, nous
nous **opposons aussi fermement aux crimes de guerre et aux bombardements
de milliers de civils palestinien⋅nes** commis depuis un mois à Gaza par
l’Etat d’Israël, mais aussi par Tsahal et les colons en Cisjordanie qui
attaquent et tuent des civils palestinien⋅nes en toute impunité.

Toute notre solidarité va là-bas, à la fois aux victimes civiles et otages
israélien⋅nes visé⋅es par le Hamas d’une part, et aux civil⋅es
arabes d’Israël et de Palestine ciblé⋅es par la violence de l’Etat,
des colons et fascistes israéliens.

**Notre solidarité va en outre à celles et ceux qui, ici ou là-bas,
luttent pour une paix juste et durable, dans une perspective émancipatrice
et internationaliste**.

**Ces positions tenues conjointement sont non seulement possibles, mais
même impératives**.

On peut et on doit lutter contre l’antisémitisme, l’islamophobie et le
racisme et dans un même geste soutenir la lutte pour les palestinien⋅nes
dans une perspective d’émancipation sociale.

Pour nous, c’est indissociable, et ça explique en partie les motivations
de notre collage.

Venons en aux faits
======================

Dans la nuit du vendredi 20 au samedi 21 octobre 2023, nous avons collé sur
la vitre de la librairie Terra Nova les lettres "SOUVIENS TOI OZAR HATORAH".

.. figure:: images/terra_nova.png


Nous avons décidé d’écrire ce communiqué pour rétablir plusieurs
choses :

S.A.V des antisémites de gauche (UJFP/Tsedek)
-------------------------------------------------

Premièrement, il n’aura pas fallu attendre plus de 24h après
notre collage pour que l’éternel S.A.V des antisémites de gauche
(UJFP/Tsedek) accourent pour expliquer notre geste à notre place, et en
quoi nous aurions eu tort.

L’ironie de l’histoire veut que dans leur défense empressée d’Houria Bouteldja,
ils aient immédiatement saisi à quoi faisait référence le collage.

Aveux coupables ?

une "attaque de librairie" (à la colle et au papier ?)
------------------------------------------------------------

Il n’aura pas fallu attendre plus de 48h aussi pour que Houria Bouteldja
et La Fabrique éditions communiquent à grand renfort d’exagérations
larmoyantes sur les réseaux sur une "attaque de librairie" (à la colle
et au papier ?), une "campagne sournoise" (quelques mails et appels pour
dénoncer la présence de l’intéressée et ses thèses ?) et un collage
"de nuit" (ouuuh).

On est pourtant assez loin des cassages de vitrines de librairies de
gauche, tags et menaces qu’ont pu subir les camarades qui dénonçaient
ces mêmes discours il y a encore quelques années.

Là, étrangement, on a pu à l’époque entendre les mouches voler dans le
silence des mêmes bourgeois intellectuels qui pleurent aujourd’hui
leurs larmes de crocodiles pour un collage : "Ouin Ouin".


Tout ce beau monde accuse comme un seul homme les J.J.R
-----------------------------------------------------------

Il n’aura par ailleurs pas fallu attendre longtemps pour que tout ce
beau monde accuse comme un seul homme les J.J.R en taisant le fait que
nous sommes nombreux⋅ses et varié⋅es à nous être opposé⋅es à
cette invitation et à la tenue de cette conférence.
Voilà pourquoi aussi nous décidons de prendre la parole.

**Or, ce collage n’a pas été fait par les J.J.R, mais par nous**.


En mars 2012 étaient perpétrées ici à Toulouse, les tueries de Mohamed Merah, notamment contre l’école juive Ozar HaTorah
-------------------------------------------------------------------------------------------------------------------------------

En effet, il y a un peu plus de 11 ans, en mars 2012, étaient perpétrées
ici à Toulouse, les tueries de Mohamed Merah, notamment contre l’école
juive Ozar HaTorah, principalement contre des enfants en bas âge
(Miriam Monsonego, Arié et Gabriel Sandler, abattus froidement, visés en
tant qu’enfants) ainsi que le père de deux d’entres eux, le rabbin
Jonathan Sandler, parce qu’iels étaient Juif·ves (cette explication
a été donnée et revendiquée par le tueur lui-même au prétexte de
"venger les enfants palestiniens").


La solidarité d’Houria Bouteldja est allée directement trouver la famille Merah
-----------------------------------------------------------------------------------

Immédiatement après cet acte abominable, et quoi qu’on en dise, la
solidarité d’Houria Bouteldja est allée directement trouver la famille
Merah, notamment sa mère, selon elle principale victime des évènements,
visiblement.

Dès avril 2012, en effet, elle nous a gratifié d’un des textes puants
dont elle a le secret et qui fera l’objet d’une lecture en conférence
toujours disponnible en ligne :"Mohamed Merah et moi", où elle écrivait
notamment "Mohamed Merah, c’est moi" (sic), mais en même temps "ce n’est pas moi".


Aucun véritable mot d’empathie pour les victimes de la tuerie
-----------------------------------------------------------------

L’unique objet de ce texte, toujours en ligne, semblait être de justifier
comment toute l’empathie devait aller d’abord vers les musulman·es
qui subiront le racisme consécutif à cette tuerie, dans un acte pervers
de retournement de situation, et comme si l’islamophobie avait attendu
Merah.

**Aucun véritable mot d’empathie pour les victimes de la tuerie**,
contre l’antisémitisme fondamental qui s’y exprimait, en dehors d’un
larmoyant et lapidaire laïus sur "les enfants" objectifiés comme support
de cet argumentaire abject qui **replaçait encore Israël au coeur de son
argumentaire**, rendu in fine encore une fois responsable par capilarité
de ces meurtres inqualifiables.


Dans ce texte, à vomir
-----------------------------

Dans ce texte, à vomir, elle tentait de nous *rassurer* que "aucun Juif
ne nait avec le sionisme dans le sang" (ce n’est donc pas congénital
? ouf !), mais nous expliquait comment "chacun sait" que "DES Juifs"
peuvent toujours aller enfiler "l’uniforme israélien" impunément,
contrairement à Mohamed Merah, donc (?).

Comme si les autres ne pouvaient pas enfiler plusieurs autres uniformes
"impunément", ailleurs, pour commettre "impunément" d’autres "exactions".

C’est un privilège juif ça aussi, apparement.

Surtout, donc, à travers cette bouillie et ses parallèles insensés, le
rapport entre les victimes de Merah et l’Etat d’Israel est établi
directement et sans équivoque par ces évocations.


Je ne suis pas raciste mais
---------------------------------

Il faut lire ou relire ce texte pour comprendre que pour Bouteldja,
Israël et Palestine sont une pierre angulaire pour comprendre le monde
**au travers de catégories sociales réfiées et homogénéisées** ("Juifs",
"Indigènes", etc) même si elle prétend régulièrement le contraire
en préambule de chaque frasque comme d’autres disent **"Je ne suis pas
raciste mais..."**.


C’est *sa* grille de lecture du monde
-----------------------------------------

C’est *sa* grille de lecture du monde.

**C’est aussi en ça qu’elle est antisémite**.

Parce que cette vision du monde, qui fait peu de cas de la réalité et
des vies humaines, se retrouve au final à partager l’obsession du tueur
auquel elle s’associe "sociologiquement" pour s’en dissocier immédiatement
après (?) comme une petition de principe.

Mais surtout, dans ce texte, l’autrice disserte complaisament sur la
"conscience indigène" du monde suite à ce bain de sang en trivialisant les
assassinats de ces enfants ramenés à d’autres victimes "trop souvent,
des enfants [...] des innocents, à Gaza, à Kaboul...".

L’argumentaire du tueur est en fait repris tel quel.


Un argumentaire "anti-impérialiste" abstrait
------------------------------------------------

La focale de ce qui s’est passé est volontairement déplacée, s’est
éloignée à tous les niveaux et le dicours se retrouve noyée dans **un
argumentaire "anti-impérialiste" abstrait** qui passe par cette formule
cinglante : "Celui qui confond oppresseur et opprimé est un barbare".


**Mais qui étaient donc "les barbares" à Ozar HaTorah ? On ne le saura pas**
-----------------------------------------------------------------------------

**Mais qui étaient donc "les barbares" à Ozar HaTorah ? On ne le saura pas.**


Et à cette époque néanmoins il faut relever que pour Bouteldja
"les barbares" étaient "NOS ennemis", et Merah en était, comme pour
s’absoudre d’un autre "mensonge sublime" qu’on vient de prononcer.

On s’y interrogeait aussi, **avec des accents complotistes** de savoir si Merah
était peut être un agent de la DCRI.

**Pourquoi pas du Mossad tant qu’on y est ?**

Des propos sur lesquels elle n’est en fait jamais revenue, et qu’elle "assume".


Un sujet politique bricolé sur les ruines fumantes du soralisme
--------------------------------------------------------------------

Depuis, chez cette même autrice, le "barbare" (contrairement au Juif,
à moins qu’il se range derrière et se taise en dehors de moments
choisis), est devenue une figure courtisable, un sujet politique bricolé
sur les ruines fumantes du soralisme, comme le "beauf" fasciste, "pauvre
victime des évenements" lui aussi.

**"Il faut rendre à Soral ce qui est à Soral" écrit elle ainsi dans son
dernier livre. Allons donc !**

Il a, selon elle, "le mérite d’avoir su toucher simultanément les âmes
de deux groupes aux intérêts contradictoires et d’avoir envisagé
avant tout le monde une politique des beaufs et des barbares.
Il est le premier à avoir vu. Le premier à avoir senti."


Comment expliquer la popularité de cette proposition d’alliance **fondamentalement antisémite**
---------------------------------------------------------------------------------------------------

Comment expliquer la popularité de cette proposition d’alliance
fondamentalement antisémite, parce que l’antisémitisme est son ciment
idéologique ?

Eh bien, nous explique t’elle, parce qu’il "rencontre
l’adhésion puisque les Juifs sont perçus tant par les petits Blancs
que par les indigènes comme des chouchous et des privilégiés".

Ont-ils tort de le penser ? Certainement pas : "La préférence (qu’on
préférera appeler philosémitisme) dont la communauté juive est
l’objet [...] est perçue comme une profonde injustice".
(c’est toujours une histoire vraie, dans laquelle ses soutiens ne voient "pas
en quoi c’est antisémite").

C’est écrit noir sur blanc, nous y sommes : le "privilège juif"
qu’on préfère appeler "philosémitisme d’Etat".

Ce n’est pas une maladresse ou un "dérapage". Cette thèse antisémite
du "philosémitisme d’Etat" (et pas juste "philosémitisme") explicitée
ici comme une "préférence", un "privilège juif", est développée par
la même autrice dans son précédent ouvrage, mais aussi dans d’autres
textes du PIR.

C’est une théorie politique assumée. C’est au nom de cette théorie
que l’autrice (qui ne tient pas toujours le même discours en fonction
de ses interlocuteur·ices) explique dans un discours donné à Londres
et intitulé "Fighting Philo-Semitism to Fight Islamophobia and Zionism"
("Combattre le Philo-Sémitisme pour combattre l’Islamophobie et le
Sionisme") de façon beaucoup plus assumée comment il ne "s’agit pas
de lutter contre l’antisémitisme", mais "contre le philosémitisme
d’Etat" pour lutter contre l’islamophobie.


Vous voulez arrêtez de subir de l’antisémitisme ? Arrêtez d’en parler
------------------------------------------------------------------------

La formule magique est trouvée ! Vous voulez arrêtez de subir de
l’antisémitisme ? Arrêtez d’en parler, et luttez contre autre chose,
ça se réglera magiquement tout seul ! Ben voyons.

Partant de là, pour nous, la hiérarchisation raciste et antisémite
des luttes est claire, et ses motivations on ne peut plus limpides à
l’écoute de ses discours et à la lecture de ses textes.

Mann’donné, il n’y a pas pires aveugles que ceux ou celles qui
refusent de voir.

Alors pourquoi reparler d’Ozar HaTorah ?
----------------------------------------------

Alors pourquoi reparler d’Ozar HaTorah ?

Parce qu’un fil conducteur traverse le discours d’Houria Bouteldja
de la compassion pour l’auteur de cette tuerie et sa famille (et
pour les autres, Toz ?), aux explications délirantes de son geste, en
passant par la validation honteuse, même partielle, des thèses du plus
célèbre théoricien néonazi de france, et maintenant la justification
du massacre de civil⋅es (dont encore des enfants) par "antisionisme"
en Israël/Palestine d’une "résistance palestinienne" applaudie et
soutenue inconditionnellement à travers le Hamas depuis 2008 au moins
(ce n’est pas inutile de le rappeler).

Pourtant, force est de constater que le soutien à la lutte des palestinien·nes
n’a pas besoin de cette bouillie raciste et antisémite, et de ses
injonctions contradictoires et impossibles à tenir.

On peut parler aussi de celles des thèses de Bouteldja, du PIR et de leurs épigones reprises bouche en coeur par une partie de l’extrême-gauche française actuellement
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Enfin on peut parler aussi de celles des thèses de Bouteldja, du
PIR et de leurs épigones **reprises bouche en coeur par une partie de
l’extrême-gauche française** actuellement, selon laquelle tout doit être
subordonné à la "lutte antisioniste", anti-coloniale ou anti-impérialiste
(**à la sauce campiste, dépeignant le monde en "bons et mauvais camps"**),
y compris les revendications progressistes,féministes et queers (y compris
en Palestine), et on se retrouve ici dans des situations ubuesques où
des organisations antiracistes féministes et/ou queers sont silenciées
en partie par des blancs"décoloniaux" acquis aux thèses bouteldjistes
(leurs principaux alliés, dont l’immense majorité sont des classes
moyennes hautement "éduquées", et pas les fameux "beaufs" blancs tant
fantasmés "qui votent RN").


Bouteldja n’est pas seulement antisémite, c’est simplement une réactionnaire
-----------------------------------------------------------------------------------

Mais comment aurait il pu en être autrement avec des thèses et discours
aussi intolérables et délirants ?

Bouteldja n’est pas seulement antisémite, c’est simplement une
réactionnaire : le reste découle de ça.

Et nous nous moquons que certain·e choisissent de jouer la caution
féministe, queer, ou juive pour cette clique.


Terra Nova a donc fait le choix d’inviter et maintenir cette personne et ce malgré le fait que plusieurs voix se soient levées et aient alerté contre le contenu inadmissible de ses textes et ses propos
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Le jour de notre collage, la librairie Terra Nova à Toulouse, librairie
historiquement de gauche, a donc fait le choix d’inviter et maintenir
cette personne pour présenter son livre, ceux qui l’éditent et ses
dicours, et ce malgré le fait que plusieurs voix se soient levées
et aient alerté contre le contenu inadmissible de ses textes et ses
propos. Ils choisissent maintenant de défendre ces personnes et leurs
thèses. Dont acte.

**Nous avons voulu rappeler à ceux et celles qui font les autruches à
quoi ces discours aboutissent**.

Comment en effet, ne pas avoir le lien entre le type de discours défendus
au-dessus et l’explosion d’actes antisémites (parallèlement à d’autres
actes racistes) depuis le 7 octobre 2023 ici, en France, et ailleurs dans le monde ?


Nous ne tolérerons jamais, ni aujourd’hui ni demain, que ces discours empoisonnés, antiféministes, LGBTphobes et antisémites s’invitent dans notre camp
------------------------------------------------------------------------------------------------------------------------------------------------------------------

Cette nuit là, nous avons choisi en guise de rappel de coller sur leur
vitrine pour leur rappeler que **nous ne tolérerons jamais, ni aujourd’hui
ni demain, que ces discours empoisonnés, antiféministes, LGBTphobes et
antisémites s’invitent dans notre camp** (et ce peu importe le degrés
de mauvaise foi et de vernis "de gauche" apposé sur la couverture).

On se passera donc de cet « amour révolutionnaire » là (entre «
barbares » et "beaufs") qui, de toute façon, ne nous est pas destiné
et se fait toujours contre nous.

**Quelques féministes anti-autoritaires de l’Est Toulousain**
