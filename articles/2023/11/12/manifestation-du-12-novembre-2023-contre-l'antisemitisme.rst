

.. _manif_2023_11_12:

============================================================================
2023-11-12 **Manifestation du 12 novembre 2023 contre l'antisemitisme**
============================================================================


L'extrême droite antisémite n'a rien à faire dans une marche contre l'antisémitisme", a justifié Jonas Pardo
======================================================================================================================

- https://piaille.fr/@collectifgolem/111402088972281447

Extrait : "Les slogans "Le Pen casse-toi ! Les juifs ne veulent pas de toi",
ou encore "Nous, on dégage les fachos" ont été scandés par les militants
de ce collectif composé de jeunes juifs de gauche.

"Nous voulons faire en sorte que les antisémites n'aient pas leur place
ici.

L'extrême droite antisémite n'a rien à faire dans une marche contre
l'antisémitisme", a justifié :ref:`Jonas Pardo <jonas_pardo>`, l'un des
membres de :ref:`Golem <golem>`, lors d'un micro tendu capté par un
journaliste de L'Obs."
