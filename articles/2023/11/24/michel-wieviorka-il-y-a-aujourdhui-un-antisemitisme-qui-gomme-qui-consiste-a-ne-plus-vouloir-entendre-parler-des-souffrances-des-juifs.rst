.. index::
   pair: Wieviorka ; 2023-11-24

.. _wieviorka_2023_11_24:

==============================================================================================================================================================
2023-11-24 **Michel Wieviorka : Il y a aujourd’hui un antisémitisme qui gomme, qui consiste à ne plus vouloir entendre parler des souffrances des Juifs**
==============================================================================================================================================================

- https://www.liberation.fr/societe/religions/michel-wieviorka-il-y-a-aujourdhui-un-antisemitisme-qui-gomme-qui-consiste-a-ne-plus-vouloir-entendre-parler-des-souffrances-des-juifs-20231124_ILFRSMVCNZC5VNYQWZJQXXODLA/

Introduction
=================

Michel Wieviorka : «Il y a aujourd’hui un antisémitisme qui gomme,
qui consiste à ne plus vouloir entendre parler des souffrances des Juifs»

Le sociologue, spécialiste des discriminations et de la violence, revient sur
la recrudescence des actes antisémites en France depuis la guerre déclenchée
par l’attaque du Hamas le 7 octobre 2023 en Israël, et en décrypte les différents
registres.

Sociologue, spécialiste des discriminations, du racisme et de la violence, Michel Wieviorka
est l’un des premiers à avoir étudié l’émergence, en France, d’un nouvel antisémitisme
au tournant des années 2000. Reconnu aussi pour ses travaux sur le terrorisme,
il est l’un des meilleurs connaisseurs de la question antisémite dans l’Hexagone.

Michel Wieviorka vient de publier un court essai, la Dernière Histoire juive, âge d’or et déclin de l’humour juif (éd. Denoël)
==================================================================================================================================

Michel Wieviorka vient de publier un court essai, la Dernière Histoire juive,
âge d’or et déclin de l’humour juif (éd. Denoël), où il décrit, par le prisme
d’un sujet apparemment léger, les évolutions du positionnement des milieux juifs en diaspora.


Profaner des tombes juives est un classique de l’ultra-droite, un type
d’actions assez caractéristique de la mouvance ou de groupuscules,
du genre skinhead ou néonazi.

Je ne dis pas que ce sont eux qui sont responsables de ce qui a eu lieu dans
l’Oise ; il faut, bien sûr, attendre les résultats de l’enquête pour y voir clair.

Mais pour rappel, en 1990, la profanation du cimetière de Carpentras était de
leur fait.
En Alsace, plusieurs cimetières juifs ont été profanés par cette mouvance,
notamment en 2004 à Herrlisheim.


Dans l’Hexagone, le ministère de l’Intérieur en a recensé plus de 1 500. Comment expliquez-vous cette flambée inédite ?
============================================================================================================================

Libération

	Après les massacres du 7 octobre 2023 et la guerre déclenchée par Israël
	contre le Hamas, la France et plusieurs pays occidentaux ont connu
	une vague très importante d’actes antisémites.
	Dans l’Hexagone, le ministère de l’Intérieur en a recensé plus de 1 500. Comment
	expliquez-vous cette flambée inédite ?


**Ce chiffre est très préoccupant**.

Jamais les actes antisémites n’ont atteint un tel degré d’intensité.
Mais, en tant que tel, le phénomène n’est pas exceptionnel et ne me surprend pas.

A d’autres moments, je pense notamment à la période de la seconde intifada au
début des années 2000, il y a eu des poussées importantes sur un laps de temps
assez court.

A ce jour, la difficulté est de savoir d’où proviennent ces actes antisémites.
Beaucoup sont commis sous couvert d’anonymat, comme les tags ou les insultes
et menaces proférées sur les réseaux sociaux.

L’hypothèse la plus couramment admise consiste à mettre ces actes sur
le dos de jeunes issus de l’immigration maghrébine. Pour le moment, nous
n’en savons pas grand-chose. En ce qui me concerne, je n’écarte aucune
hypothèse. Mais il faut savoir qu’il existe encore une extrême droite
agissante, échappant à celle qui s’institutionnalise.

Ce n’est pas parce que Marine Le Pen et quelques dirigeants du Rassemblement national
ont marqué leur opposition à l’antisémitisme que celui-ci a disparu
parmi leurs sympathisants et leurs électeurs, et il existe une mouvance
radicale, groupusculaire, qui se situe en dehors des partis.


Pourquoi cette flambée d’antisémitisme ne vous surprend-elle pas ?
==========================================================================

Au début des années 2000, j’ai vu naître un nouveau climat d’antisémitisme, dans
lequel ces actes antisémites prospèrent aujourd’hui.

A cette époque, l’image d’Israël était en cours de dégradation.
Parallèlement à cela, les grandes institutions juives de France, tel le Crif
[Conseil représentatif des institutions juives de France, ndlr], appelaient
au soutien inconditionnel à la politique israélienne.
De ce fait, une tension s’est installée ou s’est renforcée entre les Juifs
et d’autres segments de la société française.
Si les Juifs français tenaient le même discours que le gouvernement israélien,
ils devenaient de facto comptables de sa politique aux yeux de pans entiers
de l’opinion publique.

En ce début des années 2000, nous avons aussi pris la mesure de la montée de
l’islam radical et du terrorisme islamiste qui a frappé avec les terribles
attentats du 11 septembre 2001.

**Au cœur de ce terrorisme, il faut rappeler qu’il y a une haine inextinguible
et sans limite du juif**.


L’antisémitisme, est, selon vous, une réalité complexe et mouvante. Pouvez-vous l’expliquer ?
=======================================================================================================


L’antisémitisme ne se limite pas – loin de là – aux actes antisémites.

C’est un premier point important à souligner.

L’antisémitisme se décline sur plusieurs registres qu’il faut savoir distinguer.
Lorsqu’ils sont publics, les discours antisémites ne sont le jamais explicitement.
**Cela n’empêche pas qu’ils aient prospéré, notamment en revêtant les habits de
l’antisionisme**.

Entendons-nous bien, la critique de la politique israélienne, même radicale,
est tout à fait justifiée, et vivace, y compris en Israël.

**L’antisémitisme et l’antisionisme se conjuguent lorsqu’il se dit que les  Juifs n’ont pas droit à cet Etat, qu’il faut les jeter à la mer, que l’Etat hébreu,  pourtant créé légalement en 1948, n’a pas le droit d’exister**
==================================================================================================================================================================================================================================

**Mais l’antisémitisme et l’antisionisme se conjuguent lorsqu’il se dit que les
Juifs n’ont pas droit à cet Etat, qu’il faut les jeter à la mer, que l’Etat hébreu,
pourtant créé légalement en 1948, n’a pas le droit d’exister**.

Le discours de la gauche de la gauche nous rapproche chez quelques-uns de ce lien
entre antisémitisme et antisionisme. Pour autant, je n’imagine pas un
seul instant que Jean-Luc Mélenchon se réclame d’un seul des actes
antisémites que nous recensons aujourd’hui.


**Pourquoi mettez-vous en cause la gauche de la gauche ?**
==============================================================

Dans cette mouvance, il existe un substrat antisémite inconscient.
--------------------------------------------------------------------------

**Dans cette mouvance, il existe un substrat antisémite inconscient**.

Aucun militant ne se présentera, bien sûr, comme antisémite ou ne se
réclamera de l’antisémitisme.

Mais l’oubli, **la minimisation**, sans parler de la justification de la
barbarie du 7 octobre 2023 y mènent tout droit.

Cette dérive au sein de la gauche de la gauche est liée à la manière dont
l’histoire s’est écrite depuis la Seconde Guerre mondiale, notamment à cause
de la création de l’Etat d’Israël et de la mémoire de la Shoah dans
l’espace public.

Ce qui la structure est l’anticapitalisme et une critique du colonialisme,
mais aussi l’idée que les Juifs empêchent de parler d’autres souffrances
historiques.

Qu’ils auraient fait une OPA sur la souffrance historique, qu’ils occupent tout
le terrain, sans laisser de place pour parler de la traite négrière, par exemple,
ou de la colonisation.

S’ajoutent **une identification à la cause palestinienne**, le soutien à une nation
opprimée, à un peuple sans terre qui avaient un peu disparu, mais qui refont surface.

Quand j’entends dire que le Hamas est un mouvement de résistance, je ne peux
pas ne pas penser que cela gomme la barbarie et la cruauté antisémites des
massacres perpétrés le 7 octobre 2023, que cela gomme les 1 200 morts de ce
jour-là.

Ce n’est pas un antisémitisme qui tue, blesse, détruit ; c’est un antisémitisme
qui s’exprime sur le mode : «Ne me parlez plus des souffrances des Juifs !».

A LFI aussi ?
================

Il y a toujours eu une tendance à faire fructifier un antisémitisme, je dirais,
par omission.

Un antisémitisme qui gomme, un antisémitisme qui dit d’arrêter de faire des
Juifs des victimes, car ils seraient maintenant des coupables, des colonisateurs.

C’est un renversement.

Et l’un des éléments de ce renversement consiste à ne plus vouloir entendre
parler des souffrances des Juifs.

**Cela conduit à arracher les affiches représentant les otages détenus par le Hamas ?**
=========================================================================================

**Arracher ces affiches est un acte actif d’antisémitisme**
----------------------------------------------------------------

**Arracher ces affiches est un acte actif d’antisémitisme**.

Effectivement, **c’est l’aboutissement de ce discours antisémite qui veut annuler
la souffrance des Juifs**.

Pour vous, les flambées d’actes antisémites sont-elles liées au conflit israélo-palestinien ?
=================================================================================================

Libération

	Il y a débat sur ce qui déclenche les flambées d’actes
	antisémites.
	Des responsables communautaires estiment qu’il y a
	d’abord un effet de mimétisme, qu’un acte antisémite libère le
	passage à l’acte chez d’autres personnes.

	Pour vous, les flambées
	d’actes antisémites sont-elles liées au conflit israélo-palestinien ?

Il est difficile de ne pas le constater ! S’il ne s’était rien passé au Proche-Orient
le 7 octobre 2023 et ce qui a suivi, pensez-vous qu’il y aurait eu tous ces
actes antisémites ?
Nous ne pouvons pas le dissocier.


L’antisémitisme constitue-t-il une fracture au sein de la société française ?
=================================================================================

Oui, mais c’est une fracture parmi d’autres.

Et elle n’a pas du tout l’intensité qui régnait à la fin du XIXe siècle au moment
de l’affaire Dreyfus.

La France connaît un antisémitisme diffus.

Par exemple, les Juifs peuvent être perçus comme des obstacles à la liberté
d’expression.
Dieudonné a beaucoup alimenté l’idée d’un deux poids, deux mesures en la matière :
lui ne peut pas tourner en dérision les chambres à gaz alors que Charlie Hebdo
aurait le droit de s’en prendre à l’islam, au prophète.

Les Juifs détourneraient la liberté d’expression à leur profit.

Cette façon de penser traverse un peu toute la société.


La France est-elle un pays antisémite ?
==========================================

Pas du tout ! Il y a de l’antisémitisme en France, c’est clair.

Mais il ne se décline pas sous la forme de partis, ne s’instille pas dans
l’espace public, ne s’élève pas au niveau de l’Etat.

L’antisémitisme, en fait, a beaucoup régressé en France depuis la Seconde
Guerre mondiale.

L’antijudaïsme chrétien a, par exemple, quasiment disparu [la tradition catholique
jusqu’au concile Vatican II considérait les juifs comme un peuple déicide,
responsable de la mort du Christ].

L’antisémitisme est un phénomène qui peut avoir plusieurs moyens d’expression.

Nous pouvons du coup nous retrouver face à des situations paradoxales : constater
d’un côté une flambée d’actes antisémites commis par certaines catégories de
personnes et, de l’autre, le déclin général des préjugés antisémites, au sein
de l’ensemble de la population.
