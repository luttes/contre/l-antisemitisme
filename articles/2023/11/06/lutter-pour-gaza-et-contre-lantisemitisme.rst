.. index::
   pair: ORAAJ; 2023-11-06

.. _oraaj_2023_11_06:

============================================================================
2023-11-06 **Lutter pour gaza et contre l'antisémitisme** par ORAAJ
============================================================================

- https://blogs.mediapart.fr/oraaj/blog/061123/lutter-pour-gaza-et-contre-lantisemitisme
- :ref:`oraaj`
- :ref:`articles_oraaj_2023`
- https://www.instagram.com/oraaj___/
- https://blogs.mediapart.fr/oraaj

Billet de blog 6 novembre 2023 LUTTER POUR GAZA ET CONTRE L'ANTISEMITISME

par :ref:`ORAAJ, Organisation Révolutionnaire Antiraciste Antipatriarcale Juive <oraaj>`

LÀ-BAS (Palestine-Israël)
===============================

Nous appelons au cessez-le-feu immédiat à Gaza.
**Nous appelons à la libération des otages**.

Nous rêvons d’une mobilisation sociale en Israël qui mènerait à la
chute du gouvernement en place, nous soutiendrons toutes mobilisations qui
viseraient à faire cesser les massacres d’aujourd’hui et d’hier.
**Nous appelons à l'arrêt des bombardements sur Gaza** qui tuent actuellement
des milliers de palestinien·nes et rendent la vie des autres invivable
sous le siège imposé, sous blocus depuis des années.

Sortir de cette situation inhumaine **ne pourra se faire sans exiger, indissociablement, la chute du Hamas comme pouvoir politique en Palestine**
----------------------------------------------------------------------------------------------------------------------------------------------------

**Sortir de cette situation inhumaine ne pourra se faire sans exiger,
indissociablement, la chute du Hamas comme pouvoir politique
en Palestine**.
Nous ne voulons plus jamais assister à des attaques antisémites comme
celles qui ont eu lieu le 7 octobre 2023.

Nous appelons à l'arrêt du soutien apporté par toutes les puissances
occidentales à cette hécatombe terrible en cours à Gaza, ainsi qu'au
déplacement forcé et permanent de la population.

La situation humanitaire met en danger de mort chaque habitant·e de Gaza,
touché·e ou non par les bombes, par le manque de ressources qui affecte
les besoins vitaux, médicaux, psychiques, de toute la population.


Nous appelons à l'arrêt des exactions perpétrées sur les palestinien·nes par les colon·nes soutenu·es par le gouvernement d’extrême droite israélien en Cisjordanie
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

**Nous appelons à l'arrêt des exactions perpétrées sur les palestinien·nes
par les colon·nes soutenu·es par le gouvernement d’extrême droite israélien
en Cisjordanie**.

Nous dénonçons les humiliations, et les modalités racistes de Tsahal,
des colon·es et des milices d’extrême droite israélien·nes, envers
les palestinien·nes, encouragées par les déclarations déshumanisantes
racistes des ministres israélien·nes.

**Nous nous opposons aussi à la manière dont ce gouvernement mate l'opposition
militante israélienne** qui tente de participer à la lutte contre les
massacres et contre l'occupation en Palestine.

**Nous dénonçons le Hamas car nous nous opposerons toujours à une organisation complotiste qui s'est constituée dans sa charte de 1988 en appelant à tuer les juifves dans le monde**
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

**Nous dénonçons le Hamas** car nous nous opposerons toujours
à une organisation complotiste qui s'est constituée dans sa charte de 1988
**en appelant à tuer les juifves dans le monde** et revendique le Protocole
des Sages de Sion.

Le 13 octobre 2023, le Hamas a appelé les musulman·nes de "toutes les nations"
à "faire leur devoir" en versant le sang des sionistes.
Étant donné la confusion massive régnant entre juif.ves et sionistes,
**l'appel à la haine inter-communautaire que constitue cette
intervention a des conséquences graves, pour toustes les juivfes**.

Les modes d'exactions du Hamas le 7 octobre 2023 ont toutes les caractéristiques
d'actes de haine raciale : exhiber et cracher sur des corps nus - violence
sexuelle, tirer à bout portant, transporter des corps morts dans des
scènes de liesse, incendier des maisons... nous refusons de comparer
ces exactions de manière grossière à n’importe quel événement de
lutte d’émancipation (dont la guerre d’Algérie), ou de les porter
comme un étendard de la lutte décoloniale féministe.

**Votre incapacité (la gauche, NDLR) à parler de ces massacres comme tel nous inquiète**
-------------------------------------------------------------------------------------------------

Considérant les nombreux soutiens au Hamas et confusions, nous appelons
les militant·es de nos rangs à se questionner sur l’efficacité
antiraciste de ces postures militantes.
**Votre incapacité à parler de ces massacres comme tel nous inquiète**, elle
constitue une complaisance: est-ce qu'identifier et analyser l'antisémitisme
d'une organisation rendrait obsolète toute lutte pour Gaza ?
Nous ne le croyions pas.

Cette dénonciation ne nous éloigne pas de notre opposition aux politiques
coloniales contre lesquelles nous lutterons toujours.

**Fantasmer Israël** comme une entité abstraite du mal qu'il faut nier
en bloc ne permet pas de construire une critique décoloniale et
antiraciste efficace, et rejoue des **fantasmes antisémites** du
"grand complot mondial impérialiste intouchable".

Nous voulons analyser ce qui se passe Palestine et en Israël en termes
politiques, pour que les gazaouis s'en sortent.

C'est-à-dire s'opposer aux méthodes anti-terroristes d'Israël reprises
à l'Occident (nous pensons à la guerre en Irak) qui s'abattent sur les
populations; lutter contre les complaisances envers l'extrême droite
et l’extrémisme; s'organiser contre les répressions politiques des voix
critiques israéliennes et palestiniennes qui s’élèvent, etc.

ICI ET AILLEURS
==================

**Nous sommes furieux·ses de l'antisémitisme qui se nourrit partout de cette
situation.**

Nous sommes terrifié·es par les slogans et appels islamistes ou
fascistes aux meurtres de juifves dans le monde.

En France, l'antisémitisme est chaque jour plus visible, dans les médias
certes, mais surtout dans nos vies. Il y a les menaces, les agressions,
les insultes (même dans la rue), le harcèlement sexiste antisémite...
la liste est longue.
Il y a aussi, dans ces moments, un fort relent de messages et imageries
complotistes, avec des mèmes, vidéos, grafs expliquant l'association
des juifves au nazisme, aux finances, à l'impérialisme...

Nous voulons que notre camp antiraciste se batte aussi contre cet antisémitisme
-------------------------------------------------------------------------------------

Nous voulons que notre camp antiraciste se batte aussi contre cet
antisémitisme.

**L'antisémitisme** est fondé sur une vision du monde qui s'organise
autour de l'association des juifves au pouvoir, à l'idée qu'iels
voudraient contrôler le monde, et qu’iels seraient un danger pour
la nation.

**L'antisémitisme repose en grande partie sur des théories complotistes**.
Pour se débarrasser de cette menace que représenteraient
les juifves, les antisémites veulent faire disparaitre "les traitres",
les juifves, et ceci depuis 3000 ans, par, tour à tour, des exactions, des
accusations de sorcellerie, des lois interdisant le culte, des expulsions,
des pogroms, des ghettoïsations, la Shoah, des meurtres plus récemment,
jusqu'à la stigmatisation et aux fantasmes sur leurs liens avec Israël.

Cette semaine, en Tunisie, **un projet de loi antisémite visant à
criminaliser des personnes "en contact" avec Israël a été présenté
une énième fois: tout·e juifve en Tunisie, qui serait supposément ou
réellement en lien avec Israël pour raisons personnelles par exemple,
pourrait être condamné·e à de lourdes peines, jusqu'à la prison
à vie**.

C'est, dans la lignée des réactions du président tunisien
Kais Saied à l'attentat de la Ghriba, une manière de consolider une
tunisianité de laquelle les personnes juifves et personnes noir·es sont
exclu·es. **C'est donc une loi antisémite qui légitime et appelle à des
actes antisémites, déjà très présents en Tunisie**.

Un autre exemple de cet antisémitisme est celui de **la chasse d'israélien·nes
par des militant·es à l'aéroport Makhatchkala au Daghestan la semaine dernière**.

L'antisémitisme se nourrit donc de la situation politique en Israël et
en Palestine, **mais ce n'est pas cette situation qui est à son origine,
ni qui le produit**.

Nous sommes dégouté·es par les politiques islamophobes alimentées par cette situation
---------------------------------------------------------------------------------------

Nous sommes dégouté·es par les politiques islamophobes alimentées
par cette situation.

Au nom de la lutte contre l'islamisme, tous les gouvernements occidentaux
en profitent pour durcir leurs politiques racistes et les expulsions du
territoire se multiplient partout en Europe
sous prétexte de lutte contre le terrorisme.

Les musulman·es et/ou arabes sont stigmatisé·es, reprimé·es, humilié·es,
dans un contexte déjà islamophobe, qui se sert de l'actualité.
Une grande partie des médias normalisent encore plus l'islamophobie, et
cela met en danger les personnes musulmanes.
**Nous appelons tout le monde à résister à cette islamophobie d'état et populaire**.

Le quotidien des musulman·nes, arabes, juivfes, ici, est un tourbillon
d’angoisses et d’agressions depuis un mois, et notre lutte se
situe précisément à cet endroit.

La liste des actes antisémites et islamophobes s’alourdira chaque jour.
C'est sur ce terrain raciste que les manifestations en soutien à Gaza
sont pénalisées et interdites.
Nous refusons de nous plier à n'importe quel récit de mise en concurrence
des juifves et musulman·nes, nous voulons trouver tous les moyens de nous
opposer à ça.

**Nous pensons qu'il faut résister aux injonctions faites aux juifves et aux musulman·es de prendre parti***
---------------------------------------------------------------------------------------------------------------

Pour résister à ces phénomènes de mise en concurrence, **nous pensons
qu'il faut résister aux injonctions faites aux juifves et aux musulman·es
de prendre parti** selon leurs positions dans les rapports sociaux de race
concernant Israël-Palestine : elles font de chacun·e un·e d'entre nous
des suspect·es potentiel.les - selon des idées souvent fondées sur des
représentations complotistes (théories "d’allégeance à l'entité
supranationale sioniste", théorie du grand remplacement etc.) **et
nous essentialisent**.

Quiconque nous demanderait des comptes sur les offensives militaires
israéliennes, raisonnerait par antisémitisme, en essentialisant ici
toustes les juifves.
Quiconque demande des comptes aux musulman·nes sur l'islamisme et sur
le Hamas, raisonne par islamophobie.

Une mobilisation sociale pour gaza contre l'antisémitisme
===============================================================

Pour rendre possible un mouvement de lutte émancipatrice pour Gaza
ici en France, **un travail critique de l'antisémitisme qui traverse les
mobilisations pour la Palestine est indispensable**.

**Cela fait si longtemps que ce travail aurait du être fait, cela fait 30 ans
que les forces de gauche et de lutte décoloniale en France aurait du y travailler**.

Nous ne voulons plus entendre que les violences et les actes antisémites
seraient des résistances à la colonisation israélienne (en mai 2023,
les discours qui qualifiaient les meurtres antisémites de la Ghriba en
Tunisie comme une résistance à l'occupation israélienne étaient sur
ce registre).


**Nous ne pouvons que douter de la cohérence de notre présence dans ces mouvements en tant que militant·es antiracistes contre l'antisémitisme**
-------------------------------------------------------------------------------------------------------------------------------------------------

Quand nous entendons dans les manifs des rhétoriques complotistes selon
lesquelles des juif.ves s'immisceraient dans l’État français pour
influencer sa politique en soutien à Israël, quand nous voyons partout des
comparaisons au nazisme, quand nous devons nous justifier d'être juifves,
**nous ne pouvons que douter de la cohérence de notre présence dans ces
mouvements en tant que militant·es antiracistes contre l'antisémitisme**,
et douter du caractère émancipateur de ces mouvements.

Si bien sûr, les manifestations en soutien à la Palestine sont hétérogènes,
et constituées de positionnements politiques variés, **nous appelons à
construire un mouvement qui soit capable de lutter contre l'antisémitisme
qui le traverse**.
Pour que nous puissions y participer toustes, et pour ne pas attiser la
haine des juifves en France.

Il faut se former à repérer les rhétoriques qui participent au "folklore"  antisémite !
----------------------------------------------------------------------------------------------

Il faut se former à repérer les rhétoriques qui participent au "folklore"
antisémite !

Nous continuerons, dans la lignée de notre rôle politique, à interpeller
sur l'antisémitisme des mouvements sociaux qui en sont traversés, pour
construire des mobilisations émancipatrice·s.

**Nous appelons donc à la construction d'un mouvement massif pour Gaza,
qui lutte aussi contre l'antisémitisme.**

APRÈS
=========

Nous ne voulons pas d'un "retour à la normale" en Palestine et
Israël.

Nous estimons que cette "normalité" n'est autre que celle d'un
quotidien caractérisé par un statu-quo de siège, par la négation d'un
tabou, qu'il est urgent de nommer et défendre : **l'auto-détermination
palestinienne**.

Nous voulons lutter en faveur d'un projet décolonial qui prenne en compte
l’existence de toutes les communautés sur des territoires qui leur sont
communs.

Nous rêvons de révolutions, non seulement en Israël et à l'échelle du  Proche-Orient, mais aussi en Occident
--------------------------------------------------------------------------------------------------------------

Nous rêvons de révolutions, non seulement en Israël et à l'échelle du
Proche-Orient, mais aussi en Occident, **qui fassent s'effondrer ces modèles
capitalistes, patriarcaux, écocides, et racistes qui s'affrontent**.

Nous appelons donc nos camarades antiracistes, antifascistes, féministes,
afroféministes, décoloniales, à construire ensemble un front décolonial
féministe antiraciste, qui prend en compte l’antisémitisme, pour traverser
cette histoire ensemble.
