
.. _video_2023_11_09:

==============================================================================================================
2023-11-09 Video mediapart **Antisémitisme : les risques et la confusion** (A l'air libre, 58 minutes)
==============================================================================================================

- https://invidious.fdn.fr/watch?v=aYCNMxlukbg

Depuis la guerre et les massacres au Proche-Orient, nombre de Français
juifs s’inquiètent. D’autant que leur pays, la France, a une funeste
histoire d’antisémitisme.

« Antisémitisme, les risques et la confusion » : une nouvelle édition
de « À l’air libre », l’émission d’actualité et de débats
de Mediapart.

Depuis le 7 octobre 2023, avec les massacres du Hamas en Israël, le cycle
de la violence, une histoire longue de 8 décennies, s'est réenclenché
au Proche-Orient, avec d'autres massacres en cours à Gaza par l'armée
israélienne que Mediapart continue, évidemment, de documenter.

Ce cycle de violence comporte le risque de la déshumanisation des Gazaouis. Nous
en avons parlé dans cette émission, sur Mediapart.  Il comporte un
autre risque : l'antisémitisme. Ce risque est déjà là, ce sont ces
actes antisémites qui augmentent.  On va y revenir. Cette période
est aussi source de confusion possible. La confusion, c'est celle d'un
moment extrême de tension où nos émotions sont convoquées, où il est
possible d'oublier la souffrance, la colère de l'autre ou la peur qu'il
ressent, celle qui le conduit à assimiler tout juif, où qu'il vive, à
un agresseur ou à un colon et à s'en prendre à lui pour cette raison,
celle qui est entretenue aussi dans notre pays par ceux qui souhaitent
un conflit de civilisation et qui visent clairement les Arabes et les
musulmans. Confusion enfin avec cette présence du Rassemblement National et
d'Éric Zemmour à la manifestation de dimanche contre l'antisémitisme,
alors que le Rassemblement National a été fondé par un antisémite
notoire, condamné pour ces faits.

Face à ces risques, face à cette
confusion possible, ces confusions possibles, sur notre plateau, un
historien, Pierre Savy.
Bonjour. Spécialiste de l'antisémitisme.
Vous avez co-dirigé en 2020 une « Histoire des Juifs ». C'est paru
aux Presses universitaires de France.

3 Français de gauche militants,
engagés et juifs : Fabienne Messica, bonjour. Vous êtes membre du comité
national de la Ligue des droits de l'Homme, engagée de longue date dans
le mouvement pour la paix.

Vous êtes co-autrice du guide « Lutter contre le racisme » de la LDH.
Il est disponible en ligne. Vous avez publié il y a quelques années un
livre sur les refuzniks de l'armée israélienne.

Nadav Joffe, bonjour. Co-fondateur du collectif Tsedek,
un collectif juif décolonial.  Vous êtes israélien et français. Vous
avez grandi entre les deux pays.

Volia Vizeltzer, bonjour. Vous êtes
dessinateur et blogueur sur Mediapart et membre du mouvement Juives et
Juifs révolutionnaires. Vous m'avez dit en préparant cette émission
que vous veniez à titre personnel.
Je le mentionne pour ceux qui nous
regardent.

Merci à tous les quatre d'avoir accepté notre invitation.
S'exprimer en ce moment, c'est difficile. Cette conversation a des
résonnances intimes. Donc, merci beaucoup de le faire.

Pierre Savy, depuis le 7 octobre 2023, de nombreux Français juifs expriment une peur. De quoi est faite cette peur ?
=========================================================================================================================

C'est difficile de sonder l'opinion de
centaines de milliers de compatriotes. Je pense qu'elle est faite d'un
sentiment très simple d'inquiétude pour leur sécurité, leurs biens,
leur personne, la sécurité de leurs proches, celle de leurs enfants
dont ils voient soudain les écoles protégées par des militaires, ce
qui n'était plus le cas depuis quelques années, celle de... Voilà.

On parle de résurgence. Je crois qu'il faut être exact.  Le travail de
l'historien, c'est de parler dans la longue durée. Ça fait 20 ans qu'une forme
nouvelle d'inquiétude a ressurgi.  Elle a ressurgi alors qu'elle avait
disparu il y a 80 ans. Je crois qu'elle est radicalement différente.
Quand l'État menace la vie des juifs ou quand, au contraire, une menace,
dont on va parler à présent, existe et que l'État, tant bien que mal,
essaie de vous protéger, c'est très différent.

Résurgence d'une peur, je ne sais pas.  Apparition d'une peur, il y a une vingtaine d'années,
avec des hauts et des bas, avec Toulouse, avec l'Hyper Cacher, quelque
chose qui, en plus, commence par une agression contre Israël, d'une
part, et se situe loin du territoire national, d'autre part.


.. _fabienne_messica_2023_11_09_236:

|FabienneMessica| Fabienne Messica, cette peur, quels mots vous mettriez dessus, par rapport à ce que Pierre Savy vient de dire ? (t+236)
===============================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=236

Moi, je mettrais « réminiscence », parce qu'il y a le souvenir...
Il y a le souvenir de quelque chose qui n'a pas forcément été vécu.
La Shoah n'a pas été vécue forcément par tous les Français juifs
actuellement sur le territoire, puisque beaucoup viennent quand même de
pays qui ont été... D'un espace colonial, déjà le Maghreb, qui a été
colonisé par la France, ou d'autres pays.

les séfarades, ils ont été en quelque sorte exclus, du fait de leur judéité, pour des tas de raisons
--------------------------------------------------------------------------------------------------------

Je pense qu'il faut prendre conscience que - comment dire ? - ça transporte aussi
autre chose, et une autre histoire, c'est-à-dire aussi cette histoire des
libérations nationales dans tous ces pays dans lesquels cette fraction-là
des juifs, ceux qui sont dits séfarades, en tous les cas du Maghreb,
et qui sont souvent en France, certains en Israël aussi, donc, ils ont
des familles en Israël, ils ont été en quelque sorte exclus, du fait
de leur judéité, pour des tas de raisons.

Il se trouve que...  Je pense
qu'ils ont ramené aussi de cette expérience historique quelque chose
dans le rapport avec les Arabes musulmans de positif et de négatif.

De positif, je le dis rapidement, c'est-à-dire qu'en réalité, il y avait
beaucoup de proximité, il y avait une symbiose culturelle et ça, il
ne faut pas l'oublier, mais d'un autre côté, il y a aussi cette peur.

Il y a aussi cette peur liée à l'histoire.
Tous ces juifs, de leur provenance, qui sont donc des exilés post-coloniaux en provenance de
ces pays, sont toujours très sensibles à tout ce qui se passe dans ces
pays.

Vous voyez, si on touche à une synagogue en Tunisie, par exemple...
Ce qui s'est passé là. ...ça a un effet absolument dévastateur, etc.

Donc, il faut se rendre compte que, quand on est exilé, on n'est pas que
dans un pays, en fait.


Cette inquiétude, elle s'exprime évidemment dans nos colonnes, Mediapart, par la bouche du metteur en scène israélien  Yuval Rozman
======================================================================================================================================

Cette inquiétude, elle s'exprime évidemment dans
nos colonnes, Mediapart, par la bouche du metteur en scène israélien
Yuval Rozman, qui l'avoue - il vit à Paris -, il avoue une peur qu'il
n'avait jamais ressentie aussi physiquement en France, alors qu'il
est là depuis 11 ans.
Il le dit, il baisse la voix au téléphone,
par exemple, quand il discute en hébreu dans la rue, avec sa mère.
On est en train de recueillir beaucoup de témoignages qui vont dans ce
sens.
Elle s'exprime aussi dans les témoignages qui apparaissent sur le
Club de Mediapart de la part de lecteurs, d'abonnés et vous, notamment,
Volia.


.. _volia_2023_11_09_381:

Vous avez écrit cette lettre d'un Français « juif français de gauche à qui veut bien entendre », Pourquoi ? (t+381)
========================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=381
- :ref:`lettre_2023_10_28`

Vous avez écrit cette lettre d'un Français juif de gauche,
d'un « juif français de gauche à qui veut bien entendre », où vous
écrivez notamment : **je pense que je me suis jamais senti aussi juif,
de manière aussi brutale, que depuis le 7 octobre.**

Pourquoi ?

Mais c'est... Je dirais que... C'est très... C'est vraiment représentatif,
en fait, de ma propre relation, on va dire, à la judéité dans le sens
où c'est quelque chose d'assez complexe que je pense que les gens,
peut-être, comprennent pas forcément assez.

C'est que...  En fait, il y a être juif pour soi, dans sa propre identité,
et puis il y a être juif dans les yeux des autres.
Et cette différence-là, elle est fondamentale, parce qu'il y a une chose
qui nous fait juifs, que nous, on choisit, et dont on peut faire un peu
ce qu'on en veut, et après, il y a l'antisémitisme, et en fait, l'antisémitisme...

Moi, je l'ai vécu comme ça, et la plupart des témoignages que j'ai eus de camarades me racontent
la même chose, c'est qu'il y a toujours ce moment très brutal en face
de l'antisémitisme, où on est rappelé du fait qu'on est juif et qu'en
fait, c'est pas vraiment notre choix, c'est pas vraiment notre décision,
c'est une forme **d'essentialisation** qui, en fait, on peut l'oublier, mais
ça nous échappe jamais vraiment.
C'est pour ça que je disais que depuis le 7... Depuis, en fait, les
événements du 7 et ce qui en a découlé, des réactions en France, etc.,
j'en parle dans la lettre, mais c'était vraiment de me rendre compte
de nouveau de cette distance qui me séparait, j'avais l'impression, des
autres, en fait.


.. _volia_2023_11_09_470:

C'était quoi, cette solitude que vous décrivez, par rapport à des camarades, ou des militants, ou des amis ? (t+470)
==========================================================================================================================

- https://www.youtube.com/watch?v=aYCNMxlukbg&t=470s

Vous êtes seul aussi, pas juste en tant que Français juif, **en tant que
militant de gauche**.

C'est ça que vous dites dans ce texte ?
C'était quoi, cette solitude que vous décrivez, par rapport à des camarades,
ou des militants, ou des amis ?


En fait, moi, comme je l'ai dit, je suis membre, je milite avec les camarades
de JJR, de Juives et Juifs révolutionnaires, et ça fait un moment, en
fait, qu'on est déjà sur ce terrain-là, qu'on développe des textes,
qu'on dit qu'en fait, il y a un problème d'antisémitisme à gauche,
qui n'est pas synonyme de « Mélenchon est un nazi », ça ne veut pas
dire ça, au contraire, ça veut juste dire qu'il y a un défaut, en fait,
de considérer l'antisémitisme comme un phénomène qui est structurel
et qui n'est pas réactionnel, qui n'est pas quelque chose qui apparaît
de temps en temps, de manière résiduelle.

C'est quelque chose... -

Qui concerne toute la société ?

Oui, c'est ça. Je pense que c'est un problème qui concerne tout le monde,
au même titre que plein d'autres problèmes différents, qui...
Allez-y. Oui, je pense que c'est un problème qui concerne tout le monde
et que, malheureusement, j'ai l'impression de constater qu'à gauche,
il y a un défaut de considération de ça et que c'est très vite balayé :
« individus isolés, individus isolés ».

On ne parle pas du problème de fond.


Nadav Joffe, vous, en tant que militant juif décolonial, est-ce que cette inquiétude, vous la ressentez aussi ?
===================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=556

Je n'utiliserais pas le terme d'« inquiétude », mais j'utiliserais le
terme de « compréhension », pour comprendre ce sentiment-là...
Mais pour moi, il ne s'agit pas de dire qu'il n'y a pas d'antisémitisme
à gauche et que l'antisémitisme, quand on dit qu'il est structurel, c'est
qu'il traverse toutes les couches de la société, y compris la gauche.

Or, pour nous, en tant que collectif antiraciste, il est
important de ne pas se focaliser sur les symptômes de l'antisémitisme,
mais sur ce qui le produit. Nous, si on veut être dans une démarche
qui n'est pas de l'ordre de la moralisation, mais de l'ordre de l'action
et du changement des structures, on aurait plutôt intérêt à cibler
les lieux de production de l'antisémitisme...

Ils sont où ? - ...qui, selon nous, ne se situent pas à gauche.
Ils se situent historiquement à l'extrême droite, adossés au nationalisme,
mais également - je finirai juste là-dessus -, ils sont également diffusés
par l'amalgame qui fait des juifs des Israéliens, des juifs des sionistes...

Amalgame produit pas uniquement par l'Etat d'Israël, mais également ses
alliés, ses soutiens, y compris à gauche.
Il y en a à gauche qui font cet amalgame-là, et on l'a vu ce mois-ci.

Et enfin, je finirai juste pour dire que le fait
de parler de ces lieux de production-là ne nous dédouane pas... de la
réalisation qu'il y a d'autres formes de judéophobie qui émergent aussi
de milieux populaires, qui sont des reproductions de cet antisémitisme,
qui est structurel à la société, en fait.


.. _fabienne_messica_2023_11_09_660:

on utilise ce mot « structurel » sans jamais le définir (t+660)
===================================================================

- https://www.youtube.com/watch?v=aYCNMxlukbg&t=660s

Je voudrais réagir... Justement, d'abord, déjà, on utilise
ce mot « structurel » sans jamais le définir.

C'est comme « racisme d'État », « racisme systémique ».

Nous, on l'utilise aussi, mais je trouve qu'il faut le définir.

En ce qui concerne l'antisémitisme, tous les racismes ont une spécificité,
ce qui ne veut pas dire qu'on ne peut pas traiter le racisme d'un point
de vue universaliste, mais tous ont une spécificité et ces singularités
s'articulent avec quelque chose de commun, qui est la déshumanisation
de la personne désignée par le racisme.

La spécificité de l'antisémitisme, aujourd'hui, si vous me
permettez, c'est de dire... C'est un racisme sans discrimination, ce qui
est un peu différent de ce que vivent, par exemple, nos compatriotes
français musulmans...

C'est-à-dire, un racisme sans discrimination ?

C'est-à-dire que vous n'avez pas... Les Français juifs n'ont pas
de difficulté particulière à obtenir un logement, par exemple,
ou à obtenir un emploi. En revanche, il y a un racisme diffus dans
la société qui a des sources à l'extrême droite qu'on connaît,
qui sont historiques, mais qui existent aussi dans...
On va l'appeler « extrême gauche », mais je ne sais pas comment le nommer,
mais en tous les cas... Soit parce qu'historiquement, il y a quelque chose qui
existait déjà chez les anarchistes, dans plein de courants, ou même
chez Marx... Déjà, il y a cette histoire.  Et puis, au-delà de cette
histoire, il y a une transformation du juif en Blanc dans la vision -
excusez-moi - décoloniale, qui voudrait qu'on soit dans un pur rapport
nord-sud quand on parle d'Israël.  D'Israël et des Palestiniens.

Et donc, quand on arrive comme ça, du coup, ça devient encore une autre façon,
une autre traduction, **et tout ceci ignorant, évidemment, l'extrême
diversité des histoires juives**, comme s'il existait un juif, ce que je,
vraiment, conteste totalement, et on arrive donc à cette essentialisation
qui fait que des gens, par exemple, moi, ont pu me dire - à l'extrême
gauche, des gens que je connais -, quand je leur expliquais, par exemple,
dans les quartiers populaires, comment ça se passait à Sarcelles, en leur
disant...

Les mêmes populations ont commencé à habiter ensemble, qui
venaient des mêmes pays.  On habitait en habitat social et tout ça... Il
faut essayer de comprendre comment ça s'est passé, toute cette histoire
des uns et des autres, parce que les uns et les autres ont eu du mal, ont
été mal accueillis dans ce pays.


.. _fabienne_messica_2023_11_09_820:

Pour eux, ça n'existait pas. Des juifs pauvres ! (t+820)
-----------------------------------------------------------

- https://youtu.be/aYCNMxlukbg?t=820

Des gens d'extrême gauche me disaient :« Ah bon, il y a des juifs pauvres ? »

Pour eux, ça n'existait pas. Des juifs pauvres ! Vous voyez ce que je veux dire ?
Donc ce genre de choses,
je peux vous dire qu'on les entend assez souvent. Et à chaque fois, on est
complètement étonné...

De leur permanence ?

...de leur permanence, et du manque de réflexion sur l'histoire.

.. _pierre_savy_2023_11_09_850:

Pierre Savy, une réaction ? (t=850)
=====================================

- https://youtu.be/aYCNMxlukbg?t=850

Non, non. Moi, je suis d'accord avec ce que vous venez de dire.
Si j'ai bien entendu, vous parlez de déshumanisation.
Je trouve que c'est un mot qui est un peu impropre, c'est ma seule
réserve par rapport à ce qui vient d'être dit.

On emploie beaucoup de mots maximalistes depuis le 7 octobre,
et peut-être même avant. On en reparlera peut être. Ce serait mieux
que non. Le génocide par-ci, l'apartheid par-là, la déshumanisation
ici...
Parlons de discrimination.

Je veux dire que c'est faire au racisme
non pas un hommage, mais lui prêter une force trop grande que d'estimer
qu'il ne commence qu'avec la déshumanisation.

La discrimination dont sont victimes les immigrés en France, c'est déjà du racisme. Pas tous
les racistes pensent que ce ne sont pas des humains.
C'est ce que je voulais rappeler. Le racisme, lui aussi, il est divers, et il ne passe
pas forcément par la mise hors de l'humanité comme, effectivement,
ça peut se produire.

Les nazis l'ont fait avec les juifs, comme chacun
sait ici, mais il ne passe pas forcément par là. C'était ma réserve.

Par ailleurs, je trouve légitime, comme la prise de parole antérieure,
encore la vôtre, monsieur, de réfléchir aux lieux de production
plutôt que de se focaliser, et je crois qu'on se comprend toutes et tous
ici, sur les populations qui sont à l'origine de propos antisémites.

Mais en revanche, je pense qu'il est nécessaire d'observer tous les lieux
de production, ceux de l'extrême droite, dont un certain nombre de gens,
juifs par exemple, pensent qu'ils ne sont plus producteurs d'antisémitisme,
et ça me paraît une grave erreur, et de dangers, pas seulement de discours
mais aussi de dangers, **mais aussi un antisémitisme de gauche**, qu'on le
veuille ou non. On parlait des anarchistes. Il y a aussi une tradition,
qui est une autre tradition.

Et puis, et je pèse mes mots parce que ne me
faites pas faire ce que je ne dis pas, un lieu de production islamiste,
qui n'est pas seulement un lieu de production de discours antisionistes,
mais aussi un lieu de production antisémite.

Et ne pas le voir, à mon sens, c'est peut-être commode mais c'est s'interdire de comprendre les
visages nombreux et qui fusionnent, c'est ça, le miracle opéré par les
juifs, c'est de réussir à mettre d'accord des gens qui ont tout pour se
haïr. Tel humoriste antisémite qui prend, je crois, tel leader fasciste
comme parrain de son enfant, ce genre de choses.

Voilà, donc tous ces courants peuvent se marier, se croiser, se combiner,
s'opposer, mais il ne faut pas en oublier, **et je pense que c'est dommage
d'en oublier un qui est un des trois courants**, si on veut être un peu schématique, qui
nous affligent en ce moment.

Nadav Joffe, vous voulez répondre depuis tout à l'heure. Et après, on passe
à la précision sur les actes antisémites qui se sont produits depuis un mois.

Nadav Joffe : Nouvel antisémitisme ? (t=980)
================================================

- https://youtu.be/aYCNMxlukbg?t=980

Oui, je ne suis pas tout à fait d'accord avec la dernière partie de votre intervention, sachant que pour
moi, il est essentiel aujourd'hui de faire la distinction entre ce qu'on
appelle le nouvel antisémitisme ?, qui est une construction qui s'opère
depuis quelques décennies, qui vise à redéfinir l'antisémitisme en
quelque chose, en un objet, un historique dépolitisé qui s'inscrirait
donc dans l'islam ou à l'extrême gauche, ou bien dans les ONG des
droits de l'homme ?.

Et on voit bien que cette nouvelle redéfinition, elle
n'amène pas plus de sécurité aux juifs. Elle ne fait que renforcer
les extrêmes droites, pas qu'en France, à l'étranger également.

Elle permet aussi de délégitimer les voix qui sont critiques de l'apartheid
israélien, en premier lieu les Palestiniens. Et voilà, pour moi, je
pense qu'il est essentiel de dire qu'aujourd'hui, et je pense que c'est
peut-être le sujet qu'on va aborder par rapport à la manifestation
de l'antisémitisme.

Il ne faut pas oublier qu'aujourd'hui, il y a un
effort de redéfinir ce qu'est l'antisémitisme qui profite aux forces
qui produisent l'antisémitisme.


.. _pierre_savy-2023_11_09_1050:

Pierre Savy je n'arrive pas à comprendre si ce nouvel antisémitisme est ancestral  ou il est nouveau ? (t+1050)
=================================================================================================================

- https://www.youtube.com/watch?v=aYCNMxlukbg&t=1050s

Mais je n'arrive pas à comprendre si ce nouvel antisémitisme est ancestral
ou il est nouveau ?. Je ne veux pas parler de nouvel antisémitisme, je
parlais comme vous, je reprenais vos propos de lieux de production.

**Nier l'existence de ces lieux de production me paraît dangereux.**

Je ne me suis pas prononcé sur l'émergence d'un
nouvel antisémitisme qui serait, comme par hasard, le fait de populations
discriminées d'origine maghrébine.


Mediapart
===========

On va poursuivre la discussion. Depuis le 7 octobre, le ministre de l'Intérieur dénombre, selon les
derniers chiffres, 1159, c'est très précis, tags, insultes et violences
à caractère antisémite. L'augmentation ne peut pas être contestée.
Il y a un problème dans ces chiffres puisque le ministère de l'Intérieur
ne les détaille pas précisément. C'est toujours embêtant pour les
journalistes quand n'a pas accès aux chiffres, ils ne communiquent
pas ce détail. Le Canard enchaîné le rappelle hier, la catégorie
antisémitisme ne figure pas dans les statistiques de la place Beauvau
car cette qualification n'existe pas dans le code pénal. Elle est
comptabilisée dans une seule rubrique mêlant discriminations sexuelles et
religieuses de tous ordres. La justice ne fait pas pareil, elle ne donne pas
de chiffres. Pour s'en tenir au seul parquet de Paris, il communique aux
rédactions des cas qui ont été judiciarisés. On peut citer plusieurs
exemples donnés ces dernières semaines par le parquet de Paris, qui
n'épuisent pas les autres exemples qu'il y a : un homme condamné à
quatre mois de sursis pour des menaces réitérées en raison de la race,
l'ethnie ou la religion envers deux femmes place Colonel-Fabien, le 12
octobre à Paris, un homme condamné à 18 mois de prison, dont 12 avec
sursis, pour avoir uriné sur les murs de l'hôpital Rothschild à Paris
et avoir tenu des propos antisémites, un homme soupçonné d'avoir mis le
feu à l'appartement d'un couple octogénaire parisien.

Le Crif alerte sur la haine des juifs « qui sort de son lit et fait tomber toutes les
digues. » C'est une expression de Yonathan Arfi, président du Conseil des
institutions juives de France. Et puis, il y a aussi des cas qui suscitent
des emballements médiatiques et les enquêtes peuvent aussi révéler des
zones d'ombre, comme les tags d'étoiles de David à Paris.

Autre affaire non encore élucidée à ce stade, le cas d'une jeune fille qui dit avoir
été poignardée à son domicile à Lyon et dont la porte a été taguée
de croix gammées.  Mais l'enquête se poursuit.

Est-ce que, les uns les autres... Et il y a évidemment des exemples à l'étranger.  On connaît
évidemment cette émeute qui a ciblé des juifs au Daghestan ou, vous
en parliez, Fabienne, la synagogue incendiée à Berlin ou en Tunisie.

Est-ce que pour vous... L'historien Nicolas Lebourg, je crois, a dit :
« À chaque fois qu'il y a eu des soubresauts », c'est son expression,
« au Proche-Orient, on assiste à cette résurgence-là », à ce retour,
vous n'étiez pas forcément d'accord avec le terme « résurgence».


.. _pierre_savy-2023_11_09_1190:

On assiste à cette résurgence-là, est-ce qu'on est là-dedans aussi, en ce moment, Volia ou Pierre ? (t+1190)
=================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1190

Allez-y. Non, non... J'aimerais réfléchir, mais à vrai dire, je n'étais
pas d'accord sur « résurgence » pour dire que ce qui se passe en 2023
s'inscrit dans une temporalité. Bien sûr, c'est dramatique, **mais ça
fait une vingtaine d'années qu'on est à mon sens plus ou moins dans
cette séquence**,

et ce n'est pas une résurgence de 1940, c'était ça que je voulais dire.
Je m'inscris dans une longue durée, donc je tiquais
juste sur ça, mais je suis d'accord avec votre analyse par ailleurs.
Donc oui, il y a une région. Il y a aussi toute la province.
Enfin, vous, vous avez, et c'est déjà quelque chose...

Je cite Le parquet de Paris, il y a d'autres exemples.  Mais je prenais un exemple.

Fabienne, Volia ?


.. _volia_2023_11_09_1230:

[Volia Vizeltzer] En fait, je voulais commencer un propos en rebondissant sur un des actes (t+1230)
=======================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1230

Avec plaisir. En fait, je voulais commencer un propos en rebondissant sur un
des actes. Il y avait un truc qui m'avait...  Je pense qu'il fait vraiment
partie du problème. La jeune femme à Lyon poignardée, croix gammées
sur le mur.  Les premières réactions que moi, j'ai vu des politiques,
que ça soit de n'importe quel bord, c'est d'accuser l'autre.

L'enquête n'est pas finie, on ne sait pas ce qu'il s'est passé, et pourtant, la
première réaction, c'est tweet de La France insoumise qui dit « non
à l'extrême droite » et tweet de l'extrême droite qui va dire « non
à La France insoumise ». Et en fait, en attendant, c'est un truc qui
dure depuis très longtemps.  C'est toujours comme ça, en fait. Je dis
souvent que l'antisémitisme ou la lutte contre l'antisémitisme, c'est
un peu la balle de ping-pong qu'on s'envoie à l'Assemblée. Sauf que la
balle de ping-pong, c'est nous.


Instrumentalisation systématique

Et en fait, en attendant, personne ne
fait attention vraiment à ça et les antisémites sont tranquilles. Il y
a vraiment une
En fait, il y a une **instrumentalisation systématique**,
et bien sûr que moi, je critique l'instrumentalisation de la droite et de
l'extrême droite, mais je pense que la gauche, elle se fourre dedans aussi,
dans cette instrumentalisation-là, elle rentre dans un jeu qui l'empêche
en fait de réellement considérer ça comme un problème sur lequel il
faudrait qu'elle travaille, en fait.

Elle se sent immunisée par rapport
à beaucoup de gens ? Les responsables de gauche se sentent, pour vous,
immunisés par rapport à des biais antisémites ?  Vous parlez de biais
antisémites dans votre texte.

Oui. Je ne sais pas, en fait. Moi, après,
ce que je dis aussi, c'est que j'ai pas envie de faire le bon point-mauvais
point de chaque député, de tel groupe ou tel groupe, mais je constate
des tendances, on va dire, et **je pense qu'il y a des biais antisémites
qui s'activent à des moments donnés**, mais parce qu'ils ont toujours
été présents.


Le "nouvel antisémitisme" c'est une appellation que je ne trouve pas correcte non plus parce qu'il y a un antisémitisme, qui a plein de formes différentes
--------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/aYCNMxlukbg?t=1335

C'est pour ça d'ailleurs, même si on revient très courtement sur le nouvel
antisémitisme, **c'est une appellation que je ne trouve pas correcte non plus**
parce qu'il y a un antisémitisme, qui a plein de formes différentes, qui a
plein de manières de...

Mais justement, qui est poreux et qui traverse en fait plein de couches différentes,
plein de sociétés différentes.  C'est comme on disait, c'est un ciment
politique aussi qui peut être utilisé sciemment ou pas consciemment.

Avant de parler de la manif... Oui, Fabienne, pardon.


.. _fabienne_messica_2023_11_09_1365:

[Fabienne Messica] Je voulais bien réagir juste sur deux points, 1) les victimes coupables 2) l'absence d'empathie (t+1365)
=============================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1365

Je voulais bien réagir juste sur deux points.

Je crois que ce qui a été frappant
dans ces différentes séquences, je ne reviens pas sur les différentes
séquences de cette guerre, puisqu'on est en guerre, mais juste la
première séquence, **ça a été le fait qu'une partie de l'extrême
gauche n'a pas voulu dénoncer ce crime** et donc a posé ces victimes
civiles, civiles israéliennes, comme des **victimes coupables**.

Ce sont des victimes coupables parce qu'Israéliens, Israélien égal colons.

Et ça, ça se répercute sur la perception des juifs.

::

    Expliquez-moi en détail
    parce qu'en préparant cette émission, vous avez dit : « Moi, dans ce
    mois dernier, moi qui suis dans les mouvements, pour la paix, notamment,
    depuis des années et des années, là, je sens des décrochages avec mes
    camarades. »
    Sur quel point ?


Sur ce point-là. Ce point-là précis.
On est tout à fait d'accord, puisque moi, je fais partie quand même du
milieu de soutien au peuple palestinien pour ses droits et donc, on est
tout à fait d'accord, par exemple, pour dire « stop, arrêtez », et
on est horrifiés.

::

    Quel mécanisme fait que vous avez eu l'impression
    que les victimes étaient coupables dans la bouche de certains ?


Dans la première séquence, **beaucoup de groupes, le NPA, Solidaires pour ne
pas les nommer**, mais aussi sur les listes sur lesquelles je discute avec
des tas de gens qui sont des anticolonialistes, des néo-tiersmondistes,
qui sont de mon univers, ce qui m'a fait grandir, j'ai grandi avec eux,
donc ce n'est pas rien pour moi, j'ai quand même vu soudain qu'une
partie ne comprenait rien à ce que je leur disais, **ils n'avaient aucune
empathie**...

::

    Aucune empathie pour les victimes, les otages ?

Et passer son temps à essayer de démontrer que c'étaient des **victimes coupables**.

Il y a plein de manières de démontrer que c'est des victimes coupables,
en partant de dès l'origine, l'histoire d'Israël, c'est l'antisionisme,
disons, d'extrême gauche, ou bien qui dit : « Ils n'ont qu'à dire qu'ils
ne sont pas d'accord, les juifs, ils n'ont qu'à dire qu'ils ne sont pas
d'accord avec ce que font les Israéliens actuellement. »


.. _fabienne_messica_2023_11_09_1495:

[Fabienne Messica] Mais ça, c'est une assignation identitaire (t+1495)
======================================================================

- https://youtu.be/aYCNMxlukbg?t=1495

**Mais ça, c'est une assignation identitaire**

Moi, je ne demande pas à ce que des personnes
de religion musulmane, à chaque fois qu'il y a un attentat, par exemple, ou
un truc comme ça, viennent dire « je suis contre les attentats, ce n'est
pas mon islam, etc. », parce que ça serait de l'assignation identitaire,
et je le refuse.

Et je le refuse aussi pour les personnes de confession
juive. Elles ont le droit d'être attachées à Israël, elles n'ont pas
à démontrer qu'elles sont innocentes parce qu'en tant que victimes du
racisme, vous êtes toujours innocent, en tant que victime civile, vous
êtes toujours innocent, et nous faisons bien la différence.

Et ceux qui n'arrivent pas à la faire, à mon avis, ont commencé quand même
très sérieusement à, je n'ai pas le mot... dérailler.


Mediapart
===========

Nadav Joffe, comment vous voyez ce que dit justement Fabienne ? J'ai repris le communiqué
de Tsedek du 7 octobre, vous parlez de l'offensive, je cite, « de la
résistance palestinienne », vous dites : « Il ne nous appartient pas
de juger de la stratégie », du Hamas, en l'occurrence, « la violence
de l'opprimé est d'abord le produit de la violence de l'oppresseur. »
Et vous parlez de crimes de guerre le 12, cinq jours après, dans un
second communiqué.

J'ai l'impression que Fabienne, c'est exactement
ce type de réaction qu'elle vise, là, quand elle vient de parler.

Nadav Joffe
==============

- https://youtu.be/aYCNMxlukbg?t=1571


Écoutez. Ce communiqué, les formulations qui je pense vous ont gênée,
sur la catégorisation de ces actes comme des actes terroristes... Des
actes de résistance ?  Oui, la résistance, j'y viens juste après. Une
organisation terroriste, pour moi, relève d'un débat qu'il faut avoir,
qui est celui de la dénomination de ce qu'est le terrorisme.


.. _fabienne_messica_2023_11_09_1595:

[Fabienne Messica] Je n'ai pas employé le terme « terrorisme », c'est ça qui est intéressant dans votre réaction (t+1595)
==========================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1595

Oui, mais vous me faites dire des mots que je n'ai pas dits.
Je n'ai pas employé le terme « terrorisme », c'est ça qui est intéressant
dans votre réaction.

::

    c'est donc la résistance ?

C'est simplement le fait de considérer que c'est un acte
légitime de résistance de tuer des civils chez eux, enfants, etc.
Donc, si vous voulez, arrêtons avec ce mot « terrorisme » parce qu'il y
a trop... Là, de fait, vous utilisez le mot « résistance ». C'est
un mot que je n'ai pas utilisé,

**Non ! Ce sont bien des meurtres de civils.**

Nadav Joffe
==============

Laissons Nadav Joffe terminer, ensuite je passe la parole et on enchaîne.


Vous avez aussi utilisé un terme que je n'ai pas utilisé non plus, qui
n'est pas dans ce communiqué, qui dit que ce sont des actes légitimes,
des actes de résistance.

::

    Je ne parlais pas de votre communiqué.

Ce sont des actes d'une résistance d'un peuple occupé, et dans le droit
international, on peut, on a le droit de résister par tous les moyens ?
à son oppresseur.

Et c'est justement ce débat-là qui nous manque depuis
un mois, qui est celui qui permet de contextualiser les choses pour nous
expliquer pourquoi l'explosion de violence à laquelle on assiste depuis
les attaques du Hamas ne débute pas par ces massacres-là, mais s'inscrit
dans au moins 75 ans de colonisation et de vies de Palestiniens sous
la botte coloniale.  Et je le dis en tant qu'Israélien, parce que mes
parents, ma famille habitent en Israël. Et pour moi, il est important
de ne pas faire l'impasse sur cette discussion-là, parce que le seul
chemin pour la sécurité des Palestiniens et des juifs passe par la
décolonisation et pas par le gommage de ces spécificités.


Mediapart
===========

Pierre Savy, on a beaucoup parlé, ça m'intéresse que vous ayez une réaction à
cette discussion, mais on a beaucoup parlé dans les médias dans les
jours après, y compris pour la réaction de Jean-Luc Mélenchon, ou les
mots qu'il n'aurait pas prononcés, **de manque d'empathie**.

C'est ce que dit aussi un peu Fabienne sur cette idée des victimes coupables.
Là, ça va plus loin, évidemment, puisque c'est désigner les juifs comme
finalement coupables, il y a une essentialisation aussi.

.. _pierre_savy_2023_11_09_1715:

[Pierre Savy] Est-ce que vous aussi, vous avez ressenti ces silences-là, durement, difficilement, au terme de l'histoire ? (t+1715)
=====================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1715

**Oui, oui, je les ai entendus, ces silences**.

Il y a beaucoup de gens qu'on entend et dont on se demande encore ce qu'ils
pensent, s'ils condamnent la mise à mort de ces enfants, de ces femmes,
de ces hommes civils, les choses ne sont pas très claires.
On refuse de prononcer des mots, on parle de résistance sans qualifier les meurtres
par quoi cela a commencé.

Qu'il y ait une histoire avant l'attaque du 7 octobre, on en est tous d'accord,
mais ce n'est pas la question.
Donc, évidemment, je trouve que...  Je ne suis pas sûr que l'empathie, on est
dans un propos politique, donc on voudrait plutôt un discours de vérité,
de clarté, d'un niveau constructif plutôt que de l'empathie.

Mais **en effet, il a beaucoup beaucoup manqué d'empathie chez certains leaders**.

::

    C'est ce que vous entendez dans le discours de Nadav Joffe ?

Oui. Si j'avais envie de discuter avec lui, je lui demanderais s'il considère
que ces meurtres étaient légitimes ou non.

**C'est quand même une vraie question.**


.. _pierre_savy_2023_11_09_1751:

[Pierre Savy] Est-ce qu'il était légitime de tuer des enfants de deux mois parce qu'ils étaient juifs ? Oui ou non ? (t+1751)
================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1751

**Est-ce qu'il était légitime de tuer des enfants de deux mois parce qu'ils étaient juifs ? Oui ou non ?**

Ne parlons pas de terrorisme.

::

   Pour moi, la question ne se situe pas ici.

C'est ma question.

Nadav Joffe défend le Hamas
=============================

OK, d'accord, vous avez votre question, vous voulez condamner le Hamas,
condamner la résistance, condamner les meurtres, les atrocités.

Pour moi, justement, dire que le 7 octobre, le Hamas a été
responsable de crimes de guerre ou peut-être de crimes de l'humanité,
ou peut-être un génocide, ça, ça relèvera du moment où, je l'espère,
on arrivera devant les tribunaux, on pourra juger des choses.
Et parler de terrorisme ou parler d'actes illégitimes maintenant, c'est incohérent ?.
C'est deux discussions différentes. Il y a une discussion sur la
nature des organisations politiques et de la lutte palestinienne et
l'autre des méthodes qui sont employées. Et ça, c'est l'histoire du
colonialisme.



.. note:: C'est abracadabrantesque, ce que vous dites.


On ne vous mettra pas d'accord. Volia.


.. _volia_2023_11_09_1810:

[Volia Vizeltzer] le mot qui m'a le plus dérangé, qui a été absent, notamment beaucoup à gauche, pour qualifier les actes du 7 octobre, c'est « l'antisémitisme »
=====================================================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1810

Moi, je remarque qu'en fait, **le mot qui m'a le plus dérangé, qui a été absent**,
notamment beaucoup à gauche, pour qualifier les actes du 7 octobre, c'est
de **l'antisémitisme**.

Personne n'a réellement appuyé le fait que, en tout cas à gauche,
parce que la droite, elle s'en est vantée, mais personne n'a dit, que
ça soit du terrorisme, un génocide, du massacre de machin truc, **c'est
de l'antisémitisme**.

**C'est une volonté de tuer des juifs parce qu'ils sont juifs**.

Donc il y a une différence quand même de l'acte entre le contexte et
entre la nature de l'acte.
Comme Nadav Joffe le dit, le contexte est colonial.

Est-ce que je peux... Le contexte étant un contexte colonial
d'oppression...

::

   Que personne, je pense, autour de cette table ne remet
   en cause.

Non, je ne pense pas.  Et pour avoir parlé avec pas mal de
personnes juives de gauche qui se retrouvaient dans ce que j'écrivais ou
dans ce que je dessine, pas une seule personne n'est en train de dire :
«Youpi, super, on peut enfin se venger », c'est pas vrai, mais par contre,


.. _volia_2023_11_09_1860:

[Volia Vizeltzer] **On constate une impossibilité, vraiment physique, presque, à nommer les choses, à nommer l'antisémitisme quand c'est de l'antisémitisme**
===============================================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1860

**On constate une impossibilité, vraiment physique, presque, à nommer
les choses, à nommer l'antisémitisme quand c'est de l'antisémitisme**,

et en fait, que ça soit un contexte colonial ou pas un contexte colonial,
ça n'a aucune importance quant au qualificatif antisémite de l'acte en
lui-même.

C'est-à-dire que si le peuple palestinien...

::

    Antisémite dans ce sens-là, parce qu'on s'en est pris à des victimes civiles juives,
    qui vivent en Israël.

Oui, en tant que juives. Et ça me fait pas
plaisir de dire ça, ça m'attriste. Je préfèrerais que ça ne soit
pas le cas, mais le cas a été qu'on a quand même assez de preuves pour
qualifier ça d'un **massacre antisémite**.

::

    D'où l'utilisation d'ailleurs du mot « pogrom », que vous ne reprenez
    pas à votre compte.

Nadav Joffe grille de lecture, inversion des responsablités, consternant ! abracadabrantesque, les vctimes sont coupables
============================================================================================================================

Non, pas du tout. Justement, parce que pour moi, pour nous, en tant que collectif,
cette grille de lecture qui fait de ces attaques des attaques antisémites
est trompeuse et permet justement ensuite de légitimer les massacres,
le génocide qui va avoir lieu sur Gaza (abracadabrantesque).

Je vais juste finir là-dessus.
C'est que moi, je me dis que lorsqu'on décrit les actes qu'a commis le
Hamas, le 7 octobre, le Hamas, qui d'ailleurs contient dans son discours des
éléments judéophobes, voire des représentations antisémites dans ses
chartes anciennes, nouvelles dans ses discours, etc., mais ce n'est pas la
question, les juifs qui ont été tués, massacrés dans les kibboutz autour
de Gaza habitaient sur le sol des Palestiniens qui sont enfermés à Gaza.

Et vous avez traduit un super texte qui justement aborde cette question et
un texte d'Enzo Traverso qui justement met en garde contre cette grille de
lecture qui vient gommer les spécificités de l'antisémitisme, qui vient
inscrire le Hamas comme étant l'héritier en fil direct du nazisme, et
j'ai une question, du coup, parce que c'est très intéressant pour moi...
Je vais essayer de poser des questions. Est-ce que si ces atrocités,
ces massacres du 7 avaient eu lieu à Hébron ou à Ma'aleh Adumim,
est-ce que vous, vous auriez dit que c'était des attaques antisémites
également ?

::

    Attendez, comment ça ?

Fabienne, vous vouliez réagir.

[Fabienne Messica] **Vous développez une pensée qui est exactement celle de la victime coupable** (t+1995)
===========================================================================================================

- https://youtu.be/aYCNMxlukbg?t=1995

Vous développez une pensée qui est exactement celle de la victime coupable,
justement, quand vous dites : « Mais ils étaient sur un territoire qui
normalement... »

Vous êtes né en Israël, vous êtes israélien, mais
vous savez bien qu'il y a plusieurs générations d'Israéliens, que c'est
une histoire complexe, que l'histoire du sionisme est complexe, qu'il y a
des sionismes, et à chaque fois, on doit toujours réagir à des visions
extrêmement simplistes du phénomène de cette histoire.

::

    Pour vous, la vision purement coloniale est simpliste ?

Elle est complètement simpliste et je la trouve même en partie fausse.


.. _fabienne_messica_2023_11_09_2042:

[Fabienne Messica] Le problème de la solidarité internationale (t+2042)
==========================================================================

- https://youtu.be/aYCNMxlukbg?t=2042

En partie vraie, en partie fausse. Et le problème qu'il y a dans votre manière de penser,
plus large que ça, c'est le problème de la solidarité internationale.

La solidarité internationale, ce n'est pas dire avec les peuples opprimés :
« Je m'identifie complètement, je m'incorpore à votre lutte et donc,
vous avez le droit de vous battre par tous les moyens que vous voulez parce
que moi, je suis une alliée, je suis un allié »,

**ce terme d'allié que je trouve assez inintéressant, pour ne pas dire
inintelligent**.

Non. **La solidarité internationale, c'est un acte libre**.

**Je me bats pour des idées, je me bats pour la justice, je me bats pour rétablir
les Palestiniens dans leurs droits**, si on parle ici de la question
palestinienne, des droits des Palestiniens, **mais je ne m'identifie
pas forcément à tous les modes de lutte qu'ils choisiront ou à tous
les modes de représentation politique qu'ils auront.**

Et je voudrais rappeler que l'Autorité palestinienne, en la personne de Mahmoud Abbas,
bien avant le 7 octobre, avait fait un discours négationniste et que
donc, moi, je suis pour soutenir évidemment tous les Palestiniens de
Cisjordanie, qu'ils récupèrent leurs territoires et tout ça, mais **je
ne peux pas soutenir Mahmoud Abbas dans un discours négationniste qui
est un discours antisémite**.

Donc, les représentants, que ce soit le Hamas ou l'Autorité palestinienne
évidemment, sont traversés par cette question de l'antisémitisme parce
que la situation est tragique et que dans l'impuissance, c'est toujours
le pire qui advient et c'est ce qui s'est passé.

Mediapart
===========

Allez, Pierre Savy, ensuite la manifestation et ensuite
une autre phase - parce qu'on avance dans l'heure. -


.. _pierre_savy_2023_11_09_2137:

[Pierre Savy] On continue d'être autour de cette question coloniale
=========================================================================

- https://youtu.be/aYCNMxlukbg?t=2137

Je sais.  Je reviens un peu en arrière...
C'est le débat crucial, il me semble.

On continue d'être autour de cette question coloniale. Vous nous avez dit qu'on était
tous d'accord.

Je le suis également pour dire qu'il y a quelque chose de
colonial et quelque chose qu'il faut dénoncer comme tel.

**Je pense aussi qu'il y a une partie de la gauche qui utilise le récit
colonial et celui de l'indépendance des colonies en plaquant quand même
des réalités qui empêchent d'y voir clair**.

::

    Qu'est-ce qui est la différence, pour vous ?

Eh bien les différences sont très nombreuses.

Israël est un foyer de peuplement ancien dont l'établissement a fait
l'objet d'un consensus en 1948...

::

    Mais qui a déclenché la Nakba, la catastrophe...

Oui, qui a toujours été refusé par la majorité - des pays arabes alentour

Avec des gens qui sont partis, 750 000 Palestiniens.
Oui, mais il en reste quand même énormément. Quand on parle de nettoyage
ethnique.

::

    C'est ça qui est en jeu.

J'entends bien, mais je veux dire que le modèle colonial...
Et puis on mélange...  Le sionisme comme grand projet
colonial, c'est quelque chose à quoi je n'adhère pas et qui me paraît
très discutable

::

    Est-ce faux, historiquement ?

Oui, il me semble.
Il existe des colonies et c'est de ça qu'on parle.

Et le glissement des
colonies qu'on appelle en anglais et en hébreu avec d'autres mots, les
colonies, en français, je les appelle ainsi, je ne dis pas que c'est mal
de les appeler ainsi, mais si ça arrive par une espèce de réduction
stupide à tout disqualifier comme projet colonial alors qu'il faut
critiquer ces colonies, eh bien à mon avis on fait fausse route et puis,
si on critique tout...

C'est ce qu'on vient d'entendre.

On donne quand même un permis de tuer à quiconque est né du mauvais côté
dans ce pays colonial, qui est glaçant, vous avez entendu comme moi, ça fait
froid dans le dos.

On a donc le droit de tuer ces gens...

::

    Non. - Là, on parle de permis de tuer. Vous répondez ?

::

   Sur ce plateau ?

Il me semble que c'est ce que vous avez dit,

::

    Il vous semble.

Vous vous réécouterez.

D'accord.

::

     On vous mettra clairement pas...

Si, sur le pogrom, en revanche, c'est mon seul point d'accord avec ce monsieur,
je pense.  L'usage de ce terme, que beaucoup de gens très respectables
utilisent pour qualifier ce qui s'est passé le 7 octobre, à moi, pour
des raisons historiques, ça ne va pas vous surprendre, ou « historiennes»,
me paraît un usage problématique.

::

    Pourquoi ? Parce que c'est un usage historiquement ancré, marqué ?

Parce que le pogrom, c'est une population majoritaire qui se débarrasse
d'une minorité faible.

Là, c'est un groupe de quelques centaines d'individus qui viennent massacrer
une population majoritaire et appartenant à un dispositif militaire et
politique plus puissant qu'elle. Je parle d'Israël. C'est très différent,
à mon avis.

Donc il y a quand même ce minuscule point d'accord.


Mediapart
===========

On vous a trouvé un point d'accord et c'est intéressant.

Alors, je voudrais continuer sur les glissements, les points de friction
et les tensions et on fera la manif à la fin.

Je voudrais vous demander si vous allez à cette manifestation ou pas et
si oui, pourquoi. Et on finira là-dessus.

Parmi... On avait parlé du 7 octobre, des premiers jours, des mots dits
ou pas dits.  Ensuite... Et évidemment beaucoup réactivés dans le
débat public, le terme sioniste/antisioniste.

Si je prends la grille de lecture, ici, des gens qui sont sur ce plateau, le mouvement Tsedek
se revendique comme clairement antisioniste. et vous, vous dites, Volia,
dans votre texte : « Je suis comme qui dirait un sioniste, forcément
disqualifiant, puisque dans la matrice de pensée à gauche, le sionisme
est l'équivalent du mal sur terre. »

Qu'est-ce qui, sachant qu'on parle
là de traditions politiques dans le mouvement culturel juif, dans le
mouvement historique juif qui existent, qui ont été, vous l'avez dit,
extrêmement variées et extrêmement vivantes et puissantes.

Qu'est-ce qui vous dérange, du coup, Volia, dans, finalement, la façon dont ces
mots sont utilisés aujourd'hui, et il va falloir nous dire par quel
locuteur, dans l'espace public ?


.. _volia_2023_11_09_2320:

[Volia Vizeltzer] **Sur le terme sionisme** (t+2320)
=============================================================

- https://youtu.be/aYCNMxlukbg?t=2329

Déjà, les locuteurs et locutrices, moi, j'entends ça de la part de la gauche,
déjà parce que je ne parle pas vraiment à la droite, mais parce que
c'est mes... C'est des gens que je côtoie.

::

    Oui, vous vous dites : la droite, de toute façon, elle
    instrumentalise. On avait préparé un magnéto pour vous montrer comment
    elle instrumentalise. On va le passer et allez-y.

En fait, déjà, moi, je trouve ça problématique que... « sioniste », à gauche, c'est
devenu une insulte.

C'est... Je suis d'accord avec le fait qu'on peut
discuter. On peut avoir des désaccords, mais je suis pas non plus un
farouche sioniste qui défend son terrain et tout.

::

    Par « sioniste », vous entendez quoi ?

Moi j'entends un peu comme ce que vous disiez,
c'est quelque chose de très multiple. Je n'ai pas les connaissances
universitaires ou historiques pour vraiment m'exprimer profondément sur ce
sujet-là.

De ce que j'en ai compris, il y a quand même plein de tendances
différentes qui vont de l'extrême droite sioniste de Jabotinsky jusqu'à
les sionistes humanistes de Buber et Scholem, etc. Des gens qui demandent
un État binational dès le départ.

Donc c'est quand même un spectre
assez large. Moi, mon rapport personnel, et là où j'essaye de retirer
un peu ça aussi en France, c'est que moi, je ne suis pas israélien.

Mes grands-parents, c'était des Juifs polonais rescapés de la Shoah.

Il y a une histoire, par rapport à la Shoah, d'ailleurs on pourra reparler
aussi de l'instrumentalisation de la Shoah. On peut en reparler.

Mais c'est une histoire qui est extrêmement déconsidérée et méprisée
selon moi, aujourd'hui, à gauche, c'est quelque chose qu'on veut pas
regarder, que même si on le regarde, on dit « non, désolé, je veux
pas. » Pourquoi est-ce que ça ?

Je pense que c'est parce que, quand on
rentre dans ce narratif sioniste-antisioniste et qu'on voit, on va dire,
les répercussions mondiales qu'il y a du conflit au Proche-Orient, on est
**en train de faire cette dimension nord-sud, presque manichéenne, en fait**
gentils-méchants.

Il y a des « gentils », il y a des « méchants ».
Du coup, si on est avec les gentils, forcément on est d'accord...

::

    Vous dites : « Pour certains gens à gauche, le sionisme est l'équivalent
    du mal sur Terre. » C'est ça que vous dites dans votre texte..

C'est
ce que j'ai pu constater, c'est que moi, je fais une BD... À un moment
donné, oui, je parle des événements du 7 octobre et de ce que ça m'a
fait en tant que juif de France. Mais on m'a traité de « sale sioniste
» dans tous les sens et on m'a demandé des comptes sur pourquoi est-ce
que je donnais pas de solution - au conflit israélo-palestinien !


Mediapart
===========

Je donne la parole autour de la table après, mais Pierre Savy, ça,
que peut-on en dire, historiquement ?


.. _pierre_savy_2023_11_09_2470:

[Pierre Savy] Sur le sionisme
=======================================

- https://youtu.be/aYCNMxlukbg?t=2470

Moi, en dépit de sa modestie, je n'ai pas beaucoup plus de profondeur
que mon voisin à apporter sur le sionisme.

Je crois qu'on peut oser, tout en maintenant l'idée très
importante qu'il a rappelée, que c'est un mouvement divers, on peut
oser le définir simplement comme un mouvement qui juge comme légitime
l'attribution au peuple juif d'un État.

Et une fois qu'on a posé, **est antisioniste celui qui juge illégitime
l'attribution au peuple juif d'un État**.

Et la question devient : pourquoi les Juifs auraient-ils ce triste
privilège ?

Et surtout, parce que c'est vraiment là que se jouent les
choses donc je me déplace un peu par rapport à votre question : pourquoi,
dès lors qu'on critique le gouvernement d'Israël, il y a une sorte
de piège qui est tendu aux gens qui, avec la plus grande sincérité
parce qu'il y en a évidemment, s'associent aux souffrances du peuple
palestinien, on leur tend ce piège qui consiste à dire : « Tu critiques
Netanyahou ? » « Oui, bien sûr. » « Tu es contre les colonies ? »
« Oui, bien sûr. » « Tu es donc contre le sionisme ! »

Non, c'est un piège grossier mais qui fonctionne et qui fait qu'effectivement...

::

    Et c'est ça qui explique pour vous la solitude, le sentiment de ne pas
    être représentés, par certains Juifs de France ?

Oui. Et puis le
sentiment qu'effectivement, l'antisionisme devient une espèce de chose
appartenant à la vulgate de la gauche, d'une partie de la gauche en tout
cas. Et qu'en effet, **l'antisionisme est parfois le faux nez, si j'ose dire,
de l'antisémitisme**.

Il y a quand même une ligne de crête. On peut être
hostile au sionisme. Il y a plein de Juifs, religieux ou pas religieux
d'ailleurs, les Satmar qui étaient hostiles au sionisme. Mais c'est
vraiment une ligne de crête. Et la plupart des gens qui se définissent
comme antisionistes basculent du mauvais côté et donc refusent au peuple
juif ce privilège, et j'aimerais savoir pourquoi.

::

    C'est votre cas, Nadav Joffe. Vous vous définissez comme antisioniste. Est-ce que vous avez
    le sentiment de basculer du mauvais côté ? Ça veut dire du côté de
    soupçons... de possibilité d'antisémitisme ? Je rappelle que vous êtes
    français et israélien, par ailleurs.

Nadav Joffe sur l'antisioniste
==================================

https://youtu.be/aYCNMxlukbg?t=2558

Vous me parlez de la légitimité
au peuple juif de s'autodéterminer, d'avoir un État, c'est ce que vous
venez de dire.
Je pense que là, il y a un débat qui est passionnant,
et qu'on ne pourrait pas avoir ici, sur : est-ce que les Juifs forment
un peuple, une nation ? Est-ce qu'ils doivent avoir droit à leur État ?
Moi, je laisse ça pour les autres et, en tout cas, je veux bien admettre
que oui. Disons-le, OK : les Juifs ont droit à un État,

mais cet État-là ne peut pas se faire, ce droit à l'autodétermination ne peut
pas se faire au détriment d'un droit à l'autodétermination d'un autre
peuple. Et en l'occurrence, le sionisme, matériellement, historiquement,
s'est fait au détriment des droits, pas juste l'autodétermination, tous
les droits des Palestiniens.

Et le sionisme ? Qu'il soit pluriel, je suis
bien d'accord. Tout à fait. De Buber à Jabotinsky en passant par Kahana,
tout ce que vous voulez, a un fil conducteur, un fil conducteur qui est
étudié entre autres par Nur Masalha, un historien palestinien qui nous
explique que le sionisme, de Buber jusqu'à Jabotinsky, a toujours en lui
l'idée de l'expulsion, du transfert forcé ou négocié des populations
locales, des Palestiniens, et c'est un fil conducteur qu'on voit jusqu'à
aujourd'hui, à Gaza.

::

   Fabienne ?


.. _fabienne_messica_2023_11_09_2630:

[Fabienne Messica] Sur le sionisme
==========================================

- https://youtu.be/aYCNMxlukbg?t=2630

Eh bien je ne suis pas d'accord, mais c'est peut-être parce que j'aime
bien l'histoire, même si je ne suis pas historienne.

::

   Rapidement ? - C'est-à-dire...

Rapidement, les choses ne sont pas déterminées au départ.
Les choses ne sont pas déterminées au départ.

Et il ne faut pas essentialiser

::

    Il ne faut pas refaire l'histoire à l'envers ?

Il n'y a pas un ADN du sionisme et
d'ailleurs il faut arrêter de parler du sionisme, mais il faut parler
des sionismes, premièrement.

Et enfin, il faut se poser la question : est-ce qu'on doit se poser encore
la question dans ces termes, aujourd'hui, dans la mesure où il y a un État
qui est reconnu par la communauté internationale et que notre problème,
c'est que là, les Palestiniens, eux, n'ont pas d'État et qu'effectivement, *
on a tout fait pour qu'ils n'en aient pas.

Et ça, c'est tout un... Je ne vais pas revenir - sur la manière dont a évolué...

::

    Vous dites : regardons en avant et regardons les solutions politiques
    s'il y en a aujourd'hui ?

Ce que je regrette
profondément, mais tout en le regrettant profondément, je ne me sens
pas, moi, de juger ce qui s'est passé chez les Juifs au XIXe siècle,
victimes de racisme et d'antisémitisme partout, et qui a donné lieu
à ce besoin d'un État juif, à un moment ou toutes les nationalités
excluaient les Juifs.

Et donc, c'est de l'histoire et l'histoire, ça
ne se juge pas à partir d'une espèce de cause première - qui serait
le mal.

Vous devez...

::

    Le sionisme - s'est élaboré en colonialisme.

Vous devez penser la complexité. Vous voulez être engagé politiquement ?

Pensez la complexité.  Je crois que vous ne le faites pas. Je le regrette.


Mediapart
==========

Bon, tout le monde s'est écouté, mais on voit bien qu'il
y a des différences.
À l'historien Pierre Savy, j'ai une question.

Le gouvernement est israélien et parfois, ses citoyens comparés à
des nazis, le Hamas comparé à des nazis.

Christian Estrosi, ce matin, nous dit : « Il faut aller à la manifestation,
parce que le Hamas, c'est des nazis. »

Que dit l'historien ? Fabienne parle de « pornographie de
la Shoah ». Vous allez répondre.

Cette nazification omniprésente, si je puis dire, des comparaisons,
qu'est-ce qui est dit ?
Est-ce que vous dites stop, en tant qu'historien, c'est-à-dire convoquer
ce référentiel de la Shoah pour nos débats, là, ou est-ce qu'on peut le
comprendre ? Et qu'est-ce qu'on en fait ? Est-ce que ça nous fait progresser ?


[Pierre Savy] Sur le terme 'nazi' employé à tout va
=======================================================

Non, là, on va gagner du temps de parole. Stop.

Je trouve ça d'une imbécillité et d'une stupidité sans limites.

Vous m'avez appris que M. Estrosi avait fait ces déclarations : stop,
évidemment.

Ce qu'on peut en faire, si vraiment on veut réfléchir avec la Shoah, ce qui est
quand même inévitable, c'est de réfléchir au choc que ça constitue,
aux traces que ça constitue dans une conscience juive, y compris des
effets compliqués.

C'est ce qu'évoquait à demi-mots Fabienne tout à l'heure. Ça oui, parlons-en.

Il ne s'agit pas d'oublier la Shoah, mais enfin nazifier l'ennemi et
gagner des points Godwin de l'imbécilité, stop.

C'est effectivement le travail de l'historienne ou de l'historienne
de dire stop. Il n'y a même pas besoin d'être historien pour voir à
quel point ça ne produit pas d'intelligibilité.

« Mal nommer les choses, c'est ajouter du malheur au monde », comme
disait l'autre. C'est vraiment ça.  Nommer le Hamas nazi...  Ce que fait
Benyamin Netanyahou lui-même. C'est une imbécilité.

::

    La complexité étant que le conflit de civilisations et la comparaison aux
    nazis ou à Daesh se fait aussi du point de vue du gouvernement israélien.


.. _fabienne_messica_2023_11_09_2834:

[Fabienne Messica] Sur les termes 'nazi' et 'daesh'
==========================================================

- https://youtu.be/aYCNMxlukbg?t=2834

C'est inacceptable. Il faut arrêter ce jeu de miroir, d'ailleurs,
parce que tout le monde se renvoie la nazification, les Palestiniens
aussi nazifient Israël depuis longtemps.

Je le sais parce que j'en ai rencontré avec qui j'en avais parlé sérieusement.

Mais j'essaye
de penser à l'effet que ça fait à nos jeunes, à nos enfants, et le
problème de l'éducation.

On est en train de faire de la Shoah quelque
chose qui devient extrêmement fascinant, sur lequel, malheureusement, les
enfants jouent ou s'amusent, parce que personne ne respecte le symbole,
pas bien même le gouvernement israélien, puisqu'ils se sont permis,
l'ambassadeur d'Israël, d'aller à l'ONU et de se poser une étoile jaune.

**J'étais révulsée. Comme des gens se sont posé des étoiles jaunes au
moment des confinements, etc., des problèmes**.

Donc il y a une manière de
jouer avec ça qui est terrible, **c'est-à-dire que se moquer de la Shoah,
ça devient super subversif**, donc c'est hyper attirant pour les gamins.
Et en plus, on a une fascination pour l'espèce de cruauté gore qu'on
présente avec la Shoah. **Donc moi, je suis scandalisée**.

[Pierre Savy]
==============

Ce qui est paradoxal, vous le dites très bien, c'est que l'Etat d'Israël
auquel on reproche souvent, en tout cas, on décrit souvent sa religion de
la Shoah, le Yom HaShoah, etc., en tout cas, certains de ses dirigeants,
et celui qui s'est le plus vautré dans cette obscène, c'est manquer
immensément de respect pour les victimes de la Shoah.

C'est invraisemblable qu'ils aient commis cela. Un autre point d'accord autour de la table.


Mediapart
===========

Je vous laisse réagir tous les deux là-dessus et sur d'autres points sur
lesquels vous auriez voulu rebondir, et on termine par cette question de
la manif, 5 minutes à la fin.


[Volia Vizeltzer]
=====================

- https://youtu.be/aYCNMxlukbg?t=2936

Je pense que c'est hyper parlant quand on
parle de la Shoah, même par rapport à aujourd'hui, parce qu'on parle
d'antisémitisme en France, mais on ne parle pas d'antisémitisme, on
parle d'antisionisme, on parle de sionisme...

En 2 secondes : « Comment qualifier les attaques du 7 octobre », alors
qu'on parle d'antisémitisme en France, avec des tags « Mort aux juifs »
dans la rue.

Donc il y a quand même un problème aussi ici.  Je ne suis
pas d'accord pour dire que c'est uniquement résiduel du conflit qui se
passé.


On a évoqué les étoiles jaunes du Covid, antivax, et compagnie
----------------------------------------------------------------------

- https://youtu.be/aYCNMxlukbg?t=2950

**On a évoqué les étoiles jaunes du Covid, antivax, et compagnie.**

C'est quelque chose qui est là depuis longtemps. On y a affaire depuis
longtemps et moi je peux témoigner.

::

    C'est là, c'est présent

Ca a toujours été là, présent.  Mon expérience personnelle vaut ce
qu'elle vaut, je suis un individu parmi d'autres individus, mais j'ai passé
mon enfance et mon adolescence avec des gens qui blaguaient sur la Shoah
en criant avec des accents allemands dans la cour de récré.

Mais ça, c'est normal. J'en ai parlé avec plein d'autres gens, tout le monde a
vécu ça. Même quand on parle de nazification, c'est quand même fou,
je trouve.  On peut en vouloir autant qu'on veut, et à juste titre,
aux actions gouvernementales israéliennes, mais de là à qualifier de
« nazi » l'État qui a été créé à cause des nazis, c'est quand
même un peu...


Il y a une objectification des Juifs
-------------------------------------------------

- https://youtu.be/aYCNMxlukbg?t=3012

En fait, ça parle aussi d'une manière dont, je pense, par l'apprentissage
de la Shoah et aussi par des biais antisémites qu'on a encore, comme je
disais, mais **il y a une objectification des Juifs**.

Les Juifs, on n'est pas quelque chose qui peut être identifiable.
Moi, j'ai souvent rencontré des gens qui pensent connaître l'histoire juive et
qui n'ont qu'une tunique rayée en tête, c'est tout, mais ils ne savent pas
ce qu'est un ashkénaze, déjà, de base.

On ne parle même pas des autres communautés juives.
Mais le passé et l'avenir n'existent juste pas. C'est à ce moment-là.

Nadav Joffe
==================

- https://youtu.be/aYCNMxlukbg?t=3036

Nadav. Je partage ce qui a été dit autour de la table sur
l'instrumentalisation effectuée par l'Etat d'Israël de l'histoire de
la Shoah, mais j'ajouterai que ce n'est pas uniquement cette partie-là
de notre histoire qui est instrumentalisée par l'Etat d'Israël.

Il trouve aussi sa légitimité en détournant nos symboles, notre foi, notre
histoire au sens le plus large, et comment on peut s'étonner ensuite que
lorsque l'Etat israélien commet des crimes au nom des Juifs, avec des
symboles juifs, qu'ensuite, en répercussions, il n'y ait pas un retour
de violence sur les personnes juives ?

[Pierre Savy]
----------------

    Donc c'est la faute des Juifs ? Oui, c'est ça.

Non mais vous confondez. J'ai parlé de l'Etat d'Israël.

::

    Vous avez parlé des Juifs.


J'ai dit « l'Etat d'Israël ».

::

    Vous avez dit : « Les personnes juives».

Répondez, Nadav. Ben, j'ai dit que l'Etat d'Israël utilise l'histoire
des Juifs, utilise les Juifs, utilise notre identité, notre histoire pour
commettre des crimes, et vous me dites que c'est la faute des Juifs.

Non, c'est l'Etat d'Israël, et je trouve ça dingue, aujourd'hui, que le fait
de dénoncer les crimes israéliens me fasse catalogué comme étant un
antisémite.

::

    Par qui ?

Par beaucoup de gens. Non, mais les personnes
qui dénoncent les crimes israéliens sont cataloguées d'antisémites,
mais c'est les crimes israéliens eux-mêmes qui produisent en retour
cette haine des Juifs.

::

    Alors, Volia veut répondre et après, on fait sur la manif.


[Volia Vizeltzer]
======================

- https://youtu.be/aYCNMxlukbg?t=3112

J'ai écrit ça aussi dans le texte que j'ai écrit sur Mediapart.
C'est qu'en fait, on est de nouveau retourné sur : « l'antisémitisme
en France, c'est la faute à Israël », mais l'antisémitisme en France,
il existe depuis bien avant 1948.

C'est ahurissant de penser ça.

::

   Mais il l'alimente.

Il l'alimente s'il veut, **mais on ne peut pas parler que de ça**, en fait.

On doit parler de l'antisémitisme comme quelque chose de cohérent, quelque
chose qui fait système, qui fait structure de pensée.

Ça, en fait, c'est pas quelque chose qui y est spécifiquement...

J'aime pas faire des comparaisons nulles, mais **c'est comme dire que
c'est à cause de l'Iran que les gens sont islamophobes. Ça n'a aucun sens**,
Ça n'a aucun sens.

::

    Ce qui n'empêche qu'on ne peut pas oublier
    le contexte des politiques de l'État d'Israël et du gouvernement
    d'extrême droite. On est d'accord ?

::

    L'Iran ne fait pas une politique au nom des musulmans.

J'attends d'une gauche qu'elle soit capable de faire la différence entre
une attaque honteuse qui instrumentalise l'antisémitisme et des vrais
critiques sur son propre mouvement.

Mediapart
============

Alors, finissons par ça. 7 octobre... On a dit demain... Dimanche, pardon.
Excusez-moi, je suis perdu dans mes notes. Dimanche, grande manifestation
annoncée.  Au départ, Olivier Faure, premier secrétaire du PS, dit :
« Il faudrait se réunir pour la République contre l'antisémitisme».

La présidente de l'Assemblée et le président du Sénat disent : «D'accord »,
et du coup, prennent cette initiative et on a un tableau où
la CFDT y va, la CGT n'y va pas, fait un autre rassemblement, d'ailleurs,
ce soir.

Nous avons la NUPES hors LFI, hors La France Insoumise, qui y
va en disant : « On va faire un cordon sanitaire avec le Rassemblement
National et Eric Zemmour » puisqu'Eric Zemmour - en procès actuellement
pour avoir dit que Pétain a sauvé des Juifs - et le Rassemblement
National - fondé par un antisémitisme notoire, Jean-Marie Le Pen
condamné pour antisémitisme - seront à la manifestation.

C'est quoi, ce schpountz, déjà ?

Et deuxièmement, déjà, les uns, les autres, qui êtes aussi engagés dans
des mouvements, dans des organisations, ou en tant que citoyens, est-ce
que vous irez, et pourquoi ?

.. _fabienne_messica_2023_11_09_3236:

[Fabienne Messica] Sur la participation à la manifestation du 12 novembre 2023
=================================================================================

- https://youtu.be/aYCNMxlukbg?t=3236

Fabienne.

On en discute encore. On saura ce soir.

::

    À la LDH, vous en discutez encore.

On en discute encore.  Qu'est-ce que vous avez en tête ? Qu'est-ce que
vous allez dire ce soir ?
Nous, notre association, elle est née à partir de l'affaire Dreyfus,
donc c'est quand même très important dans notre histoire.
Ce qui ne veut pas dire que c'est exclusif, évidemment. Notre lutte dans
le racisme est universaliste, mais on en discute encore, et on est...

**On voudrait qu'il y ait ce cordon sanitaire** et on voudrait qu'il y ait
une expression progressiste de notre refus de l'antisémitisme.
Donc, on va sortir notre propre texte, mais je ne peux pas vous répondre,
vous dire : « On y sera ou on n'y sera pas », mais j'ai plutôt l'impression qu'on y sera.

Mediapart
================

C'est une erreur, pour vous, que Jean-Luc Mélenchon, tout de suite,
dise : « On n'y va pas » ?  Avec une expression qui a été jugée
très malheureuse, voire dégueulasse par Jérôme Guedj, sur Médiapart,
député socialiste, sous prétexte d'antisémitisme.


.. _fabienne_messica_2023_11_09_3300:

[Fabienne Messica] Sur les déclarations de Mélenchon depuis des mois
============================================================================

- https://youtu.be/aYCNMxlukbg?t=3300

Ah non, mais je pense... Je suis désolée pour LFI, des
différentes... déclarations de Mélenchon depuis maintenant des mois,
bien avant les événements et tout ça.

::

   Donc vous déplorez qui ?

Donc je déplore que Luc Mélenchon, Jean-Luc Mélenchon, pardon, soit
encore à la tête de ce mouvement **où il y a des tas de gens vachement
bien, donc je trouve ça triste**.


Nadav Joffe
==============

::

    Nadav Joffe, est-ce que Tsedek ira à cette manifestation ?

Hors de question.

::

    Pourquoi ?

Parce que manifester contre le racisme avec des racistes, c'est quand
même un drôle de concept.

::

    Donc si j'y suis, je serai raciste ? Vous...

Je ne sais pas. Non, je ne vous ai pas... C'est-à-dire que les
personnes qui y seront, en tout cas, c'est-à-dire les représentants
du RN, le parti de Zemmour qui réhabilite Pétain, mais pas qu'eux.

On se focalise beaucoup sur eux. Il y aura aussi les Républicains qui,
dans la campagne, étaient aussi centrés autour du grand remplacement,
qui est une théorie islamophobe et antisémite.

On aura aussi les Marcheurs, avec Darmanin, qui recycle des tropes
antisémites dans son livre et qui dit que Marine Le Pen est trop molle.

Donc pour moi, non. Et surtout, je n'irai pas parce que pour moi, le sujet
de l'antisémitisme et du racisme est beaucoup trop important pour être
fié à des personnes irresponsables.

.. _volia_2023_11_09_3383:

[Volia Vizeltzer] **Sur sa particpation à la manifestation avec les JJR**
==============================================================================

- https://youtu.be/aYCNMxlukbg?t=3383
- :ref:`jjr_discussion_2023_11_14`

Volia, irez-vous, si ça a lieu là où vous habitez ?

Moi, comme je le disais avant, je suis membre de JJR, alors :ref:`c'est encore
une discussion qui est en cours <jjr_discussion_2023_11_14>`.

En fait, moi je me suis noté, j'aurais tendance à
dire qu'on ne peut pas marcher avec, c'est important, avec le RN contre
l'antisémitisme, mais par contre, **on ne peut pas leur laisser la rue,
on ne peut pas leur laisser ça**.


.. _volia_2023_11_09_3403:

[Volia Vizeltzer] **C'est parce que la gauche a abandonné le terrain de la lutte contre l'antisémitisme et qu'elle a laissé un boulevard à l'extrême droite et à la droite**
==================================================================================================================================================================================

- https://youtu.be/aYCNMxlukbg?t=3403

Et j'aimerais quand même dire un truc,
c'est que la présence du RN à cette manifestation et de Zemmour et
compagnie, ce n'est pas ça la cause, ce n'est pas une cause, en fait.
Ce n'est pas la cause de la rupture entre les Juifs et la gauche.
C'est la conséquence de la rupture entre les Juifs et la gauche.

**C'est parce que la gauche a abandonné le terrain de la lutte contre
l'antisémitisme et qu'elle a laissé un boulevard à l'extrême droite
et à la droite**.

**C'est parce qu'il n'y a personne en face qu'ils sont capables d'avoir
cette espèce de légitimité impromptue et soudaine**.

::

    Vous allez dire tout de suite « On y va », - alors même que l'histoire
    RN... - Bah oui, vraiment !

    On le rappellera très vite sur Mediapart avec une vidéo.

Oui, il y a pléthore de... Je veux dire...

::

    Donc c'est la gauche qui a manqué ?


Moi, je pense. Enfin, si la gauche avait été vraiment carré, qu'elle
avait porté cette lutte et qu'elle avait écouté les critiques qui lui
avaient été faites depuis longtemps, depuis probablement avant que je
sois né, on en serait peut-être pas là.

[Pierre Savy] Sur sa particpation à la manifestation
===========================================================

Pierre Savy, irez-vous, en tant que citoyen, historien-citoyen à la
manifestation ?

La vérité est que je ne sais pas, mais je ne suis pas
sûr que vous m'interrogiez sur mon agenda de dimanche.

Je me demande, je suis le quatrième à répondre donc j'ai eu le temps de réfléchir,
au fond, si le soir, à la radio, on apprend qu'il y avait trois pelés,
deux tondus, juifs pour la plupart, je serais triste.

Je préférerais qu'il y ait des milliers de Français.  Donc je pense que c'est pas mal
d'y aller.

Évidemment, si elle était venue de la gauche, si les individus
ignobles que les gens viennent de nommer n'y étaient pas, on préférerait.

Mais les choses étant ce qu'elles sont : cordon sanitaire, déclaration
de rejet, etc. à l'égard des individus que j'ai dit étant ce qu'elles
sont, je préférerais que ce soit une grosse manif.  Donc je préférais
y aller, effectivement.


Mediapart
===========

Merci beaucoup à tous d'être passés par le plateau de
Médiapart. L'émission a duré près d'une heure.  Elle correspond à des
débats, des tensions, des frictions, et j'espère qu'elle a pu contribuer
à vous éclairer.  Merci beaucoup. Si vous le pouvez, abonnez-vous
à Mediapart. Ces émissions sont en accès libre, mais elles ne sont
possibles que grâce à vos abonnements. Bonne soirée à très vite.
