

.. _nuit_de_cristal_2023_11_09:

=======================================================
2023-11-09 **85e commémoration de la nuit de cristal**
=======================================================

- :ref:`Les informations ont été tranférées sur le site https://antiracisme.frama.io/infos-2023 <raar_2023:nuit_de_cristal_2023_11_09>`
