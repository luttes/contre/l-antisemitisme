.. index::
   pair: UJRE; 2023-11-09

.. _antisemitisme_2023_11_09:

==================================================================================================================================================
2023-11-09 **Premier Communiqué UJRE : Appel à manifester dimanche 12 novembre 2023 contre la recrudescence en France de l’antisémitisme !**
==================================================================================================================================================


Contre la recrudescence en France de l’antisémitisme !
============================================================

Dans cette période où les actes antisémites se multiplient de façon
extrêmement inquiétante, l’Union des Juifs pour la Résistance et l’Entraide (UJRE)
appelle à participer massivement à la manifestation contre l’antisémitisme
du dimanche 12 novembre 2023 à Paris.

La participation illégitime et inacceptable de l’extrême droite à cette
manifestation ne nous empêchera pas de défiler au côté de tous les démocrates
et républicains attachés à la lutte contre l’antisémitisme.

**Un « cordon républicain » interdira toute forme de confusion**.

UJRE

Paris, le 09 novembre 2023

::

    Union des Juifs pour la Résistance et l’Entraide  -14 rue de Paradis 75010 Paris


Lieu de rendez-vous, Esplanade des Invalides
=============================================================

- https://www.openstreetmap.org/#map=16/48.8597/2.3193



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3051118850708012%2C48.8539856815748%2C2.333436012268067%2C48.865462888195424&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8597/2.3193">Afficher une carte plus grande</a></small>



Arrivée, Palais du Luxembourg
=============================================================

- https://www.openstreetmap.org/#map=16/48.8493/2.3365



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.325067520141602%2C48.84360734401206%2C2.3533916473388676%2C48.85508692992307&amp;layer=mapnik"
     style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8493/2.3392">Afficher une carte plus grande</a></small>
