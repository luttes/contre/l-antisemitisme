.. index::
   pair: Volia Vizeltzer; JJR
   ! Il veut dire quoi en réalité « mon nom » ?
   pair: Trope; Double allégeance


.. un·e

.. _mon_nom_2023_11_03:

================================================================
2023-11-03 **Il veut dire quoi en réalité "MON NOM" ?**
================================================================

- https://blogs.mediapart.fr/volia-vizeltzer/blog/031123/il-veut-dire-quoi-en-realite-mon-nom
- https://www.instagram.com/voliavizeltzer/
- https://blogs.mediapart.fr/volia-vizeltzer/blog


Introduction
==============

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/04/relai-il-veut-dire-quoi-en-realite-mon-nom/

Voici une bande dessinée réalisée par notre camarade Volia Vizeltzer
portant sur la démarche « Not in my name » et l’injonction faite aux
Juif·ves à condamner Israël.


1. **Il veut dire quoi en réalité "MON NOM" ?** une BD originale par Volia Vizeltzer
=========================================================================================

- https://blogs.mediapart.fr/volia-vizeltzer/blog/031123/il-veut-dire-quoi-en-realite-mon-nom
- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/04/relai-il-veut-dire-quoi-en-realite-mon-nom/
- https://www.instagram.com/voliavizeltzer/
- https://blogs.mediapart.fr/volia-vizeltzer/blog


Il veut dire quoi en réalité "MON NOM" ? **une BD originale par Volia Vizeltzer**

.. figure:: images/1_il_veut_dire_quoi_mon_nom.png
   :width: 500


2. On dirait bien qu'aujourd'hui, pour être perçu comme un "bon juif" il faut se déclarer "antisioniste"
=====================================================================================================================

On dirait bien qu'aujourd'hui, pour être perçu comme un "bon juif" il faut se
déclarer "antisioniste".
Personnellement, j'ai quelques trucs à dire à ce sujet.
Notamment sur le slogan "PAS EN MON NOM".


.. figure:: images/2_pas_antisioniste.png
   :width: 500

3. Moi, je suis un juif français
=================================

Moi, je suis un juif français. et j'en ai vraiment marre d'entendre ce slogan.
Vous savez pourquoi ? Bah parce que j'estime que je n'ai pas besoin nde devoir préciser
que les actions du gouvernement israelien ne sont pas faites "EN MON NOM".

.. figure:: images/3_juif_francais.png
   :width: 500


4. Un des tropes antisémites les plus répandus aujourd'hui c'est celui de la **double allégeance**
===================================================================================================

Un des tropes antisémites les plus répandus aujourd'hui c'est celui de la
:term:`double allégeance`.
C'est l'idée comme quoi les juifves de la Diaspora seraient en fait des
excroissances d'une "entité sioniste" à l'échelle internationale.

.. figure:: images/4_trope_double_allegeance.png
   :width: 500


5. Messages de haine, **"sale sioniste"**
============================================

Je pourrai donner comme exemple la centaine de messages sous mes publications
sur les réseaux sociaux qui, alors que je parle d'antisémitisme en France,
m'accusent de ne pas me désolidariser publiquement de l'Etat d'Israël, et de
ce fait, de n'être qu'un **sale sioniste**.


.. figure:: images/5_sale_sioniste.png
   :width: 500

6. Moi je dis que OUI, je me sens lié à l'Etat d'Israël. **De par la mémoire**.
===================================================================================

Moi je dis que OUI, en tant que juif je me sens lié à l'Etat d'Israël.
**De par la mémoire**.
De par la raison même de l'existence de l'Etat. **Un état REFUGE** pour les
juifves du monde entier, en proie à l'antisémitisme.
Un Etat uù mes aïeuxlles juifves polonais·es auraient pû éviter la Shoah.

.. figure:: images/6_lie_par_la_memoire.png
   :width: 500

7. Mais c'est infiniment différent **d'être lié symboliquement à Israël que d'y être lié politiquement.**
===========================================================================================================

Mais c'est infiniment différent **d'être lié symboliquement à Israël
que d'y être lié politiquement**.

**Il y a un fossé** entre le fait d'être concerné en tant que juifve vis à vis
de la survie d'Israël, et d'être collectivement responsable des
agisssements du gouvernement israelien.


.. figure:: images/7_pas_lie_politiquement.png
   :width: 500


8. Le vécu des juifves de la diaspora est spécifique, il faut le considérer comme tel
===========================================================================================

Le vécu des juifves de la diaspora est spécifique, il faut le considérer
comme tel.

Ce que je reproche au slogan **PAS EN MON NOM** c'est qu'il impose de facto
aux juifves de s'opposer à la politique israelienne par le biais de leur
judéité, validant implicitement un **lien politique invisible** entre les
juifves du monde et le gouvernement israélien.

Ca renforce l'idée fausse comme quoi "MON NOM" me donnerait un pouvoir
supérieur aux goys pour exercer une influence sur sa politique.

Et les juifves qui refusent ce slogan sont de fait suspecté·es.


.. figure:: images/8_juifves_de_diaspora.png
   :width: 500

9. **Je refuse de devoir me justifier vis à vis des accusations de double allégeance**
=========================================================================================

En tant que juif français, **je refuse** ce slogan.
**Je refuse** de devoir me justifier vis à vis des accusations de :term:`double allégeance`.

**Je refuse** de devoir cocher ce genre de cases pour rassurer des personnes
qui à défaut me traiteront de "sioniste suprémaciste qui soutient la colonisation."

**Je n'ai pas besoin de me faire valider par des gens qui m'essentialisent**.

.. figure:: images/9_pas_a_me_justifier.png
   :width: 500


10. Au même titre qu'on ne peut exiger d'un·e musulman·e de construire sa lutte autour de "PAS EN MON NOM" vis à vis du Hamas ou de l'Iran
=============================================================================================================================================

Au même titre qu'on ne peut exiger d'un·e musulman·e de construire sa
lutte autour de "PAS EN MON NOM" vis à vis du Hamas ou de l'Iran par exemple,
on ne peut pas exiger ça d'une juifve vis à vis d'Israël.

Ce sur quoi j'avertis c'est le potentiel effet pervers où le fait même
de se sentir obligé de **monter patte blanche** finisse par renforcer
ce trope antisémite de :term:`double allégeance`  pour les juifves de
la Diaspora.

.. figure:: images/10_pas_a_justifier_musulman_iran_ou_hamas.png
   :width: 500

11. Le droit à la paix pour les Israelien·nes
=================================================

**On peut** défendre la légitimité de l'existence de l'Etat d'Israël **sans pour
autant être vu·e comme l'ambassade du pays**.

**On peut** aussi soutenir la cause palestinienne **sans forcément se désolidariser
du concept même d'un Etat pour les juifves**.

**On peut** être juifve en France, être attaché viscéralement à Israël,
**sans avoir à rendre des comptes vis à vis des agissements du gouvernement**
et sans être obligé de désavouer le pays dans son ensemble.

On peut également être en désaccord avec l'idée du sionisme tout en
reconnaissant et en accptant le fait national israelien et le droit à la
paix pour les israelien·nes.

En fait, il y a beaucoup de façons d'être juifve en France, et aucune
ne peut revendiquer d'être "la bonne" ou "la mauvaise".

Ainsi les désaccords ne peuvent pas se faire sur la base d'une
**essentialisation "juifve" ou "sioniste"**.
Ils doivent se faire sur la base d'un dialogue politique entre deux ou
plusieurs personnes qui **reconnaissent chacun·e inconditionnellement
l'humanité pleine et entière de l'autre**.

.. figure:: images/11_pas_un_ambassadeur_d_israel.png
   :width: 500

12. Car nous y sommes, l'antisémitisme explose … et **cet antisémitisme ce n'est pas le fait d'Israël**
==========================================================================================================

Et pour finir, on peut être juifve en France et constuire **une lutte
face à l'antisémitisme** ici **sans avoir besoin de se situer par rapport
au conflit israelo-palestinien**.

**Car nous y somes, l'antisémitisme expose, les juifves sont menacé·s.**

Et cet antisémitisme, ce n'est pas le fait d'Israël, c'est le fait de
siècles d'hostilité envers les juifves en Europe et dans le monde.

Il est parti nulle part l'antisémitisme, et il n'est pas apparu d'un coup...
il était déjà là... depuis longtemps.




.. figure:: images/12_antisemitisme_explose.png
   :width: 500
