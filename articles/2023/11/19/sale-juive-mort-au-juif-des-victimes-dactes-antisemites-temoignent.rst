.. index::
   pair: Témoignages ; 2023-11-19
   pair: Grenoble ; 2023-10-17

.. _temoignages_2023_11_19:

==================================================================================================
2023-11-19 **« Sale juive », « mort au juif »… des victimes d’actes antisémites témoignent**
==================================================================================================

- https://www.mediapart.fr/journal/france/191123/sale-juive-mort-au-juif-des-victimes-d-actes-antisemites-temoignent


Auteures de l'article: Sarah Benichou, Sarah Brethes, David Perrotin et Lou Syrah


Préambule
===========

Derrière les chiffres, débattus, qui montrent une explosion des actes
antisémites en France depuis l’attaque du Hamas le 7 octobre en Israël,
il y a les victimes. Quatre d’entre elles témoignent dans Mediapart.



Dix stèles juives dégradées mercredi dans le cimetière militaire
allemand de Moulin-sous-Touvent (Oise), un adolescent de 15 ans victime
d’une agression antisémite dans un train dans les Yvelines dimanche
12 novembre ou ce jeune de 18 ans à Rouen, traité de « sale juif »
dans la rue le 8 novembre par un homme qui avait repéré son pendentif,
une étoile de David.

Depuis les attaques du Hamas du 7 octobre 2023 et la riposte israélienne,
le décompte des actes antisémites est alarmant – le ministère de
l’intérieur en a recensé 1 518, contre 436 pour l’année 2022.

Ce relevé, effectué par les services de police et de gendarmerie, fait
débat mais il est implacable : chaque semaine, le chiffre grimpe. De
quoi nourrir une peur vive au sein de la communauté juive en France.

Car derrière ce décompte, il y a des victimes à qui Mediapart a choisi
de donner la parole pour rendre compte de cet antisémitisme qui n’a
jamais disparu.

- L’histoire d’Isaac*, de sa femme et de leur bébé qui ont retrouvé leur
  appartement mis à sac, cambriolé et recouvert d’inscriptions antisémites.
- Celle d’Esther*, violemment insultée dans la rue lorsqu’elle a été
  identifiée comme juive.
- Le témoignage d’une centenaire qui, avec sa famille, a subi un véritable
  harcèlement le temps d’un déjeuner parce que son voisin de table avait deviné
  son origine avant de déverser tout un tas de préjugés.
- Et Léa*, influenceuse visée sur les réseaux sociaux, lorsqu’elle a partagé
  des stories sur Instagram et reçu de la haine en retour.

Toutes ces victimes, qui ont préféré conserver l’anonymat et dont
trois n’ont pas porté plainte, considèrent cette expérience comme
exceptionnelle et incomparable avec leur vécu antérieur.


.. _temoignages_grenoble_2023_10_17:

2023-10-17 **Un « cambriolage à caractère antisémite » à Grenoble**
=======================================================================

Le 17 octobre 2023 aux alentours de minuit, Isaac revenait d’une excursion
familiale à Lyon avec sa femme et sa fille en bas âge, quand, en
pénétrant dans son deux-pièces au rez-de-chaussée de sa résidence
située dans la banlieue de Grenoble, à Fontaine, il a découvert
effaré son appartement mis à sac.

Le canapé, la télévision du salon et de la chambre, une enceinte Google
et même les jouets de sa fille ont été volontairement brisés.

1 000 euros lui ont également été dérobés.

Mais « le plus douloureux » ce sont les tags inscrits à l’encre noire et
qu’il découvre alors.

Un « sale juif » figure sur une porte, un « mort au juif »
près de l’entrée, un tag inscrit « free Palestine » près de la
fenêtre.

Plusieurs croix gammées ont également été tracées, dont
l’une dans la chambre parentale, comme a pu le constater Mediapart
présent sur les lieux le 10 novembre 2023.

.. figure:: images/insultes.png

Le lendemain des faits, Isaac a déposé plainte « contre X menace de mort
matérialisée par écrit, dégradation et vol par effraction commis en
raison de la race, l’ethnie ou la religion ».

Le procureur de Grenoble Éric Vaillant a annoncé l’ouverture d’une enquête
pour « cambriolage à caractère antisémite».
« Les investigations sont toujours en cours », confirme le parquet à Mediapart,
qui dit n’écarter aucune piste.

Si son cas est venu grossir le chiffrage des actes antisémites dans
la presse, Isaac n’a pas souhaité en être le visage, il a refusé
les sollicitations des principaux médias et l’aide des associations
cultuelles.

« Je n’ai tout simplement pas les mots », dit le jeune homme « dégoûté »
--------------------------------------------------------------------------

« Je n’ai tout simplement pas les mots », dit le jeune homme « dégoûté »
qui avoue ne pas avoir réussi à l’annoncer à toute sa famille.

Fils d’un couple mixte issu d’un mariage entre un père juif d’origine
maghrébine et d’une mère chrétienne, Isaac marié à une non-juive pratique
son judaïsme en pointillé sans renier sa foi ou sa judéité et a toujours
fait preuve d’humour pour faire rempart aux clichés antisémites vécus à l’école.

Depuis les faits, sa traditionnelle jovialité n’y fait rien, le
jeune homme cumule « des nuits sans sommeil » et une interrogation qui
l’empêche de dormir : **« Puisque je ne porte pas d’étoile de David,
et que mon appartement n’a pas de mezouza je m’en viens à me demander
si l’acte n’a pas été commis par une personne de mon entourage. »**


.. _temoignages_esther_mila:

« Sale juive ! », en pleine rue (Ether porte une petite étoile de David)
============================================================================

Il pleut le 5 novembre 2023 après-midi à Paris, alors personne ne flâne sur
le boulevard Diderot pourtant plus peuplé qu’à l’habitude.
La station Reuilly-Diderot est fermée donc les gens se rendent à pied vers la place
de la Nation.

Esther* et Mila* font partie du flux mais ralentissent un
peu : l’homme qu’elles viennent de croiser, la cinquantaine, leur a
adressé un « Sales juives ! » très sonore.

**Militantes dans des collectifs antiracistes depuis plusieurs années,
les deux trentenaires viennent de déjeuner ensemble afin de peaufiner les
détails d’une action qu’elles préparaient pour exiger un cessez-le-feu
immédiat à Gaza**.

Esther doit attraper son train à la gare de Lyon pour rentrer à Marseille :
elles terminaient leur conversation en marchant.

Au PMU où je bois mon café chaque matin, c’est la foire au complotisme
au sujet de la Palestine. Esther

L’homme a déjà filé lorsqu’elles réalisent ce qui vient de leur
arriver. Incrédules, elles se toisent l’une et l’autre : brunes
aux cheveux courts, elles portent toutes les deux un jean et un blouson
noir. Mila a mis son écharpe sur sa tête pour se protéger de la pluie.

« J’ai entendu l’insulte, j’ai compris que c’était raciste, mais
c’est comme si mon cerveau n’avait pas voulu que ça s’adresse à
moi », confie Esther qui décrit quelques secondes de déni : « J’ai
cru que c’était de l’islamophobie et qu’il criait sur Mila à cause
de son écharpe ! »

**Mais Esther porte une petite étoile de David en or
autour du cou et Mila est formelle : « Il a regardé Esther, a vu son
collier et l’a insultée. »**


Plus de dix jours après, les deux jeunes femmes relatent l’histoire,
encore un peu abasourdies. Pour l’une comme pour l’autre, cette insulte
criée dans un espace public fréquenté, en plein après-midi, dans une
grande proximité physique et par un homme absolument inconnu, est sans
commune mesure avec leurs expériences antérieures de confrontation
avec l’antisémitisme.

Elles évoquent leur adolescence, au milieu des années 2000, traversée
par la deuxième intifada et les amalgames entre les Juif·ves et Israël
pouvant donner lieu à des menaces et des insultes.

Esther vivait alors à Créteil (Val-de-Marne), Mila dans le XIXe arrondissement de Paris
où elle se souvient, aussi, avoir été fétichisée par les garçons du
collège qui la considérait comme « une fille facile, parce que juive ».

Si le niveau d’agressivité est nouveau, Esther n’est pas étonnée de
l’antisémitisme : « Depuis le 7 octobre, au PMU où je bois mon café
chaque matin, c’est la foire au complotisme au sujet de la Palestine. Le
patron me connaît, on s’entend bien, mais il ne sait pas que je suis
juive. »

« Tu devrais avoir honte d’être juive », sur Instagram
-----------------------------------------------------------

Léa* est suivie par plus de 60 000 personnes sur son compte Instagram.

La jeune femme de 29 ans y vulgarise ses compétences professionnelles sans
aucun sponsor. Avec son mari, elle fréquente une synagogue « moderne
orthodoxe » parisienne mais la plupart de ses followers l’ignorent,
à moins d’avoir vu passer sa story de vœux pour Roch Hachana (le
nouvel an du calendrier juif), en septembre.

La jeune femme souligne qu’elle fait partie d’une communauté « de gauche,
qui s’oppose à la colonisation ».

« Tu devrais avoir honte d’être juive », « Vous allez brûler en
enfer », « Arrêtez de bombarder les enfants à Gaza », « Les juifs
commettent des crimes » ou encore « Crève sale juive ! ».

Le 9 octobre 2023, Léa désinstalle l’application de son téléphone face à l’afflux
de messages qu’elle reçoit, mêlant questions déplacées, amalgames,
insultes et menaces antisémites.

La veille, la jeune femme avait partagé son « immense tristesse et
[son] inquiétude » auprès de son audience car elle n’envisageait
pas de continuer à alimenter son compte sans évoquer la situation en
Israël et en Palestine.

Elle y disait la peur qu’elle éprouvait pour
ses proches en Israël et sa « dévastation à l’idée de ce qui attend
encore les civils (TOUS les civils) de la région ». Elle exprimait sa
peur d’une explosion de l’antisémitisme en France et condamnait la
déclaration de guerre par Nétanyahou qui « fera encore couler le sang
d’enfants et d’innocents ».

À sa demande, son mari s’occupe de bloquer les comptes insultants et
d’effacer les messages antisémites pendant plusieurs jours.
Rapidement, Instagram détecte le harcèlement et bloque la possibilité de laisser
des commentaires sous ses publications.

« La première semaine, Léa était paralysée et se levait en pleurs »,
confie son époux. Horrifiée par les bombardements contre les habitant·es
de Gaza et les détails de la violence déployée par le Hamas lors
des massacres, la jeune femme est très affectée par la situation au
Proche-Orient et peine à accomplir les tâches du quotidien.

« Ce sont les réactions de mes proches, lorsque je leur confiais le contenu
des messages reçus, qui ont commencé à me faire peur », se souvient
la jeune femme. La semaine suivante, elle vérifie que rien ne permette
d’identifier ses adresses personnelles et professionnelles sur son compte
Instagram et, parfois, ressent la peur dans la journée, au travail ou
dans la rue.


**Rapidement, la peur est remplacée par la colère. « Je n’oublierai jamais ces mots »**
---------------------------------------------------------------------------------------

Rapidement, la peur est remplacée par la colère. « Je n’oublierai
jamais ces mots », explique la jeune femme qui a renoncé à déposer
plainte, préférant s’investir dans le cercle de parole de sa synagogue
où elle s’est sentie soutenue et utile : « Nous n’avions jamais parlé
d’antisémitisme et cela nous a fait, à toutes et tous, un bien fou. »

Le harcèlement a cessé. Léa a repris ses activités d’influenceuse
bénévole à un rythme un peu moins soutenu après quinze jours de
pause. Son mari voit un changement : « Bien que son compte ne soit pas
du tout militant, à chaque fois qu’elle poste quelque chose, elle
stresse un peu alors qu’avant ce n’était pas le cas. »

Léa, elle, considère que c’est l’ambiance générale qui est lourde, et rend
son usage des réseaux sociaux moins léger.

Humiliation publique antisémite, à deux pas de l’Arc de Triomphe
======================================================================

« Rassurez-vous, madame, votre heure n’est pas encore arrivée »,
lâche un septuagénaire bon chic bon genre, sourire aux lèvres, en
replaçant l’écharpe tombée au sol sur les épaules de sa voisine
de table.
La vieille dame visée est centenaire.

Elle n’entend plus très bien et ne comprend pas l’interaction. Avant de faire allusion à
sa possible mise à mort, l’homme avait pointé et moqué son accent
dans un rire gras. « Complètement sidéré, mon père est intervenu
en soulignant l’âge de ma grand-mère, espérant que cet homme s’en
aille enfin et cesse de nous harceler : il avait passé son repas à faire
des allusions à notre judéité », résume Tatiana*, sa petite-fille,
plasticienne de 34 ans.

Les faits se sont déroulés le 5 novembre 2023 en début d’après-midi à
deux pas de l’Arc de Triomphe, dans l’atmosphère ouatée d’une
brasserie accueillant ses client·es par une débauche de glace pilée
parsemée de fruits de mer. Ce jour-là, Tatiana y rejoint l’une de
ses cousines et sa mère, son père et sa grand-mère, qui a gardé sa
Roumanie natale nichée dans ses « r » qui roulent.

La famille habite le quartier, elle déjeune dans cette brasserie plusieurs fois par an.

Si c’était arrivé il y a deux mois, je sais que je lui serais rentré
dedans, mais… ça ne serait pas arrivé il y a deux mois.

Raphaël

« Vous allez m’offrir le déjeuner monsieur, c’était shabbat hier, non ? »

Énoncée depuis la table qui jouxte la leur, la phrase est adressée
à Raphaël*, le père de Tatiana, un élégant architecte d’intérieur
de 70 ans.
L’homme l’interpelle à haute voix, « avec arrogance», à différents moments du repas.
« Je vous laisse l’addition, puisque c’est shabbat ! », « Enfin je veux dire, c’était bien
shabbat hier ? », « Vous nous offrirez bien le déjeuner ! »


Ces mots associant les Juif·ves à l’argent s’inscrivent directement dans la vieille tradition antisémite et complotiste
--------------------------------------------------------------------------------------------------------------------------

Il rit, prenant à témoin la femme et le trentenaire qui partagent sa table à
lui. Le père de Tatiana se fige et « rentre, petit à petit, dans sa
coquille ». Ces mots associant les Juif·ves à l’argent s’inscrivent
directement dans la vieille tradition antisémite et complotiste.

« Je ne l’avais jamais vu dans un tel état de désarroi », s’émeut
Tatiana. « Désarçonnée », la jeune femme a finalement couru rejoindre
l’homme sur le trottoir « pour lui dire ses quatre vérités ».
Le trentenaire qui accompagnait l’homme l’en a empêchée : « Je
suis désolé, il a un peu bu, il va toujours un peu trop loin dans ces
cas-là. »

Sollicitée par Mediapart, la brasserie a refusé de communiquer le nom
et les coordonnées du serveur ayant assisté à la scène aux abords de
l’établissement, au motif qu’il ne ferait plus partie du personnel.


« J’ai vécu une humiliation publique parce que je suis juif, c’est la première fois en soixante-dix ans. »
-------------------------------------------------------------------------------------------------------------

Raphaël, le père de Tatiana, insiste sur le caractère inédit de cette
agression : « J’ai vécu une humiliation publique parce que je suis
juif, c’est la première fois en soixante-dix ans. » « Si c’était
arrivé il y a deux mois, je sais que je lui serais rentré dedans,
mais… ça ne serait pas arrivé il y a deux mois », se désole-t-il,
confiant son effroi de voir le décompte des actes antisémites monter
en flèche depuis le 7 octobre.

Toujours sous le choc plus d’une semaine après les faits, il répète
certaines phrases en boucle et se décrit comme « très traumatisé et
sensible sur le sujet ».

Pour la jeune femme, comme pour son père, c’est l’accent de l'aïeule
qui a permis à l’homme de les identifier. « Dans un établissement
huppé, j’imagine qu’un esprit antisémite qui entend le phrasé de
ma grand-mère fait des hypothèses », analyse Tatiana en passant la
situation en revue : « Nous étions dans un restaurant de fruits de mer
[qui comptent parmi les interdits alimentaires du judaïsme – ndlr],
nous ne portons aucun signe religieux et n’avons absolument pas évoqué
la situation en Israël ni le judaïsme. »


Sans avoir vécu les persécutions antisémites du siècle dernier, Raphaël a hérité et transmis cette peur d’être « découvert » comme juif
--------------------------------------------------------------------------------------------------------------------------------------------

Sans avoir vécu les persécutions antisémites du siècle dernier,
Raphaël a hérité et transmis cette peur d’être « découvert »
comme juif : le nom qu’il a légué à sa fille n’est pas le nom de
naissance de son propre père. Comme de nombreux juif·ves arrivé·es en
France depuis l’Allemagne, l’Ukraine ou la Pologne, avant ou après la
Seconde Guerre mondiale, ses parents ont francisé leur nom de famille en
prenant la nationalité française, effaçant les traces administratives
de leur migration et de leur judéité.

Évoluant dans d’autres milieux culturels et sociaux que son
père, Tatiana le rejoint : elle n’a jamais vécu une telle violence
antisémite. « Il n’y a eu ni cri ni insulte, mais c’était glaçant,
et je témoigne pour le dire : cet antisémitisme bourgeois, dominant et
humiliant, est loin d’avoir disparu. »

Un décompte fragile mais une montée des actes incontestable
===================================================================

Depuis le 7 octobre, le gouvernement communique régulièrement sur la
hausse des actes antisémites.
Au moins deux ministères permettent d’y voir plus clair. Ainsi, celui
de la justice décompte ​​340 enquêtes ouvertes pour des actes antisémites
et apologie du terrorisme en lien avec la guerre au Proche-Orient et
l’attaque terroriste à Arras, tandis que le parquet de Paris précise
que 216 signalements concernent le Pôle national de lutte contre la
haine en ligne en rapport avec le conflit.

« Des faits majoritairement déclarés ou perçus comme antisémites »,
explique une source judiciaire à Mediapart.

Le décompte publié par le ministère de l’intérieur évoque un total de
1 518 actes antisémites, parmi lesquels 50% de tags, affiches, banderoles
(dont des « morts aux juifs », des croix gammées, etc), 22% de menaces
et insultes,  8% d'atteintes aux biens, 2% de coups et blessures,
2% d'atteintes aux lieux communautaires, ainsi que 10% d’apologie
du terrorisme et, enfin, 6% de comportements suspects.

Ces deux dernières sous-catégories ont pu, récemment, poser question,
en ce qu’elles peuvent se mêler à des expressions politiques de soutien
au peuple palestinien.

Et comment tenir un bilan aussi détaillé alors que ni dans les statistiques
du ministère ni dans le code pénal, on ne trouve de catégorie qualifiant
strictement l’antisémitisme ? Sa définition est laissée, dans les faits,
à l’appréciation des services de police.


Ce décompte presque quotidien du ministère de l’intérieur suscite
d’ailleurs des interrogations et parfois des suspicions.

Interrogé mi-novembre sur BFM-TV, Abdelali Mamoun, imam à la Grande Mosquée
de Paris, avait ainsi demandé « où sont ces 1 200 et quelques actes
antisémites qu’il y a en France ? »
« Moi, j’aimerais bien qu’on
les dévoile pour que nous puissions être véritablement solidaires et
sensibles à cette question », avait-il ajouté.
**Des propos pour lesquels il s'est excusé, quelques jours plus tard.**

Sur les réseaux sociaux de nombreuses personnes mettaient aussi en cause
la véracité de ce chiffre et s’appuyaient sur deux histoires récentes
pour attaquer sa crédibilité.

Les dizaines d’étoiles de David taguées
dans plusieurs arrondissements de Paris, d’abord considérées comme
étant un acte antisémite avant que les autorités ne privilégient la
piste d’une ingérence russe.

Et l’agression au couteau d’une femme
juive à Lyon, présentée comme antisémite par certains médias avant
que le déroulé de l’enquête n’incite à la prudence.

::

    Ce qui remonte est recoupé, fiabilisé, ce n’est pas du doigt mouillé,
    ce sont tous les actes liés au conflit.

    Une source au sein de la direction générale de la police nationale

Ce n’est pas le cas. Le décompte du ministère de l’intérieur se
fonde essentiellement sur les faits signalés à ses services dans tous
les territoires, avec si besoin un travail de fiabilisation en lien avec
le travail de collecte du SPCJ – un organisme créé en 1980 après
l’attentat de la rue Copernic, aussi `discret qu’unique en France <https://www.lemonde.fr/m-le-mag/article/2023/11/16/face-a-l-antisemitisme-le-travail-discret-du-service-de-protection-de-la-communaute-juive_6200407_4500055.html>`_.

Il s’agit d’éviter de prendre en compte deux fois le même fait, voire
de prendre en compte des victimes qui ne s’étaient pas manifestées
auprès des pouvoirs publics. Un processus de croisement des données et
de visibilisation qui n’est pas organisé de manière similaire pour
d'autres communautés religieuses, la communauté musulmane en particulier,
suscitant de nombreuses critiques.

Au ministère, ce sont les services de police, de gendarmerie, et du
renseignement territorial qui recensent et font remonter les actes
antisémites. « De manière générale, on remonte systématiquement
les actes qui ont une connotation antisémite, de près ou de loin.

Ça a toujours été comme ça. La manière de travailler n’a pas changé,
c’est le contexte politique qui a changé », appuie un commissaire de
région parisienne.

« Les services de police font remonter les faits de nature pénale, les
mains courantes. On est dans une période où, dès qu’il y a quelque
chose, on judiciarise », décrypte une source policière qui suit de
près les remontées quotidiennes. « En temps normal, ces statistiques
sont du ressort du renseignement territorial, qui les remonte mensuellement
et trimestriellement. Là, ça remonte en continu, et il y a un recensement
matin et soir, poursuit-il. Mais ce qui remonte est recoupé, fiabilisé, ce
n’est pas du doigt mouillé, ce sont tous les actes liés au conflit. »

Certes, la méthodologie de ce décompte est moins rigoureuse que les
chiffres annuels livrés par le ministère de l’intérieur car il ne
sont pas « consolidés », mais ils seraient néanmoins filtrés avant
d’être révélés à la presse. Si des actes sont d’abord jugés
antisémites avant que l’enquête ne démente cette qualification,
ils sont alors retirés de la liste.

Autre interrogation, celle d’une possible sur-déclaration, liée à
une forme de « sensibilité » conjoncturelle ces dernières semaines.
Or d’après nos différentes sources, le signalement des actes antisémites
auprès des services de police est une pratique habituelle et ancrée au
sein de la communauté juive. Par ailleurs, le bilan quotidien délivré par
le ministre de l’intérieur n’est pas non plus exhaustif.

Ainsi, trois des témoignages révélés par Mediapart n’y figurent pas.

**On observe clairement une montée du phénomène, une montée de l’antisémitisme et des gens qui se sentent autorisés à l’exprimer**
====================================================================================================================================

**« On observe clairement une montée du phénomène, une montée de l’antisémitisme
et des gens qui se sentent autorisés à l’exprimer »**, analyse une
source policière.
