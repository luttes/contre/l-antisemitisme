
.. _bloch_2023_11_29:

========================================
2023-11-29 Discours de Marc Bloch
========================================


Chers amis juifs, musulmans, chrétiens, athées ou agnostiques de Montreuil
et d’ailleurs, chers camarades de l’AFPS.

La tragédie qui se déroule depuis le 7 octobre 2023 en Israël/Palestine nous
rappelle les pires horreurs dont le vingtième siècle a été le terrifiant
précurseur.

Ce ne sont pas seulement les Israéliens et les Palestiniens qui sont en
guerre, c’est l’humanité tout entière qui est en guerre contre elle-même.

Les atrocités, viols, éventrations, décapitations, commises par les nervis du
Hamas le 7 octobre contre des civils israéliens, atrocités dont ils se sont
vantés en en diffusant les images sur les réseaux sociaux du monde entier,
ne peuvent que ranimer la mémoire des innombrables pogroms et assassinats de
masse que les juifs ont subis depuis des siècles, et réveiller leur terreur
ancestrale.

Quant aux milliers de morts Gazaouis écrasés sous les bombes
d’une vengeance aveugle, affamés par un siège interminable ou enjoints de
se réfugier toujours plus au sud du territoire le plus densément peuplé
au monde, ils sont un atroce paradigme de la souffrance, de l’oppression,
de l’humiliation que subit le peuple palestinien depuis des décennies.

Oui, le comportement de l’État d’Israël a l’égard des Palestiniens de
Gaza comme de Cisjordanie et particulièrement celui du gouvernement d’extrême
droite raciste, suprématiste, colonialiste qui le dirige depuis le 1er novembre
2022, a une responsabilité énorme dans ce qui s’est passé.

Et la vengeance meurtrière qu’il a déclenchée sur Gaza est la pire des réponses.

Nous vivons en ce moment même les dernières heures de la trêve décidée
vendredi 24 novembre 2023.

Il faut que cette trêve devienne définitive, il faut un cessez-le-feu immédiat,
un cessez-le-feu qui soit concomitant avec la libération immédiate de tous
les otages que détiennent le Hamas et ses supplétifs.

Je ne suis qu’un artiste. J’ai certes consacré ma création de ces vingt dernières
années à la question d’Israël/Palestine, mais qui suis-je, moi français juif, pour
donner des leçons à qui que ce soit ?

Pourtant la solution sur le papier, nous la connaissons tous : création aux
côtés d’Israël d’un État palestinien libre, souverain et internationalement
reconnu qui conduirait à terme, j’en suis convaincu, à une fédération
israélo-palestinienne.

Et si les extrémistes des deux camps ne s’étaient pas objectivement alliés en
assassinant les uns Yitsak Rabin en novembre 95, les autres Yasser Arafat en
2004, peut-être cette solution aurait-elle pu se réaliser.

Pour arriver à la paix juste et durable que nous appelons tous de nos vœux, il
faudrait que chacun et chacune là-bas, chacune et chacun ici et ailleurs dans
le monde, soyons capables d’entendre les raisons, les peurs et les souffrances
de l’autre, de celui 3 dont la sensibilité est colorée par une origine,
une famille, une histoire intime différente de la nôtre.

J’ai la chance, oui la chance, grâce aux amis de cœur que je me suis fait des
deux côtés du mur, de ressentir une égale empathie pour Israéliens et Palestiniens.

Le sang qui coule de mon cœur n’est pas plus rouge quand je songe aux Israéliens
assassinés que quand j’imagine l’enfer que vivent les Gazaouis.

**Et je suis abasourdi, atterré, terrorisé et encoléré quand je vois ici même,
en France que l’indignation des uns fait concurrence à celle des autres**,
que la **compassion qu’on éprouve pour les uns semble rendre sourd à celle
que l’on ressent pour les autres**.

En clair : **quand je vois que la solidarité envers les victimes juives conduit
à l’islamophobie ou que la compassion à l’égard du calvaire des Palestiniens
conduit à l’antisémitisme**.

**Ayons le courage de penser contre nous mêmes !**

le courage de ne pas laisser nos émotions étouffer notre raison.

Et pour finir, je voudrais citer ce mot magnifique d’une Palestinienne, professeure
d’arabe dans un lycée d’Athènes, qu’une amie algérienne vient de me transmettre :

**Je suis en deuil non pas des morts, mais de la conscience du monde**.

Merci de votre attention.

Bernard Bloch. Montreuil, 29 novembre 2023
