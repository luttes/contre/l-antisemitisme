.. index::
   pair: Brigitte Stora ; Un curieux féminisme" : elles dénoncent le tour pris par la manifestation du 25 novembre 2023


.. _stora_2023_11_27:

===================================================================================================================================================================================
2023-11-27 **"Un curieux féminisme" : elles dénoncent le tour pris par la manifestation du 25 novembre 2023** Laurence Rossignol, Martine Storti, Bouchra Azzouz, Brigitte Stora
===================================================================================================================================================================================

- https://www.lexpress.fr/societe/un-curieux-feminisme-neuf-militantes-denoncent-le-tour-pris-par-la-manifestation-du-25-novembre-GVQH7I4YHZCELGVIAS3ICHENMA/
- https://kolektiva.social/@raar/111487014614885391


#viols #feminicides #jewdiverse #mazeldon #FeminicideDeMasse #Hamas #25Novembre
#RAAR #Feminisme #25Novembre2023


Préambule
============

Laurence Rossignol, Martine Storti, Bouchra Azzouz, Brigitte Stora...
déplorent "la scandaleuse mise à l’écart des manifestantes venues dénoncer
les violences commises sur des femmes juives par le Hamas."


Introduction
================

Grâce aux jeunes générations de féministes, le monde ouvre de plus en plus
les yeux sur les violences faites aux femmes.

**Et ce n’est que justice**.

Samedi 25 novembre 2023 on espérait entendre résonner la solidarité internationale à l’égard de toutes les femmes
=====================================================================================================================

Samedi 25 novembre 2023, à l’occasion de la journée internationale contre les
violences faites aux femmes, plusieurs manifestations se sont déroulées en
France.

A Paris, on espérait entendre résonner la solidarité internationale à
l’égard de toutes les femmes.

Car, des courageuses Iraniennes qui continuent de résister jusqu’aux Afghanes
privées de tout droit, des femmes Ouighours violées et stérilisées dans les
camps chinois jusqu’aux violences faites aux femmes Kurdes, aux Arméniennes,
contraintes à l’exil, la liste est hélas interminable de toutes les violences
subies.

Dans les guerres, les femmes paient un bien lourd tribut comme en Ukraine où
la dénonciation des viols commis par l’armée russe sort enfin du silence.

Quant aux Syriennes, elles semblent peu intéresser le monde, malgré la poursuite
des massacres qui ont déjà fait 500 000 morts.

Nous nous tenons à leurs côtés comme nous sommes aux côtés des civils palestiniens
parmi lesquels de nombreuses femmes et enfants meurent aujourd’hui sous les bombardements.

**Mais cette solidarité ne peut en aucun cas justifier la scandaleuse mise
à l’écart d’un groupe de manifestantes venues dénoncer ces violences
faites aux femmes juives commises par le Hamas**.


Sans doute faut-il rappeler les faits
==========================================

- https://www.lexpress.fr/monde/proche-moyen-orient/attaque-du-hamas-israel-de-nouveau-en-guerre-contre-gaza-5HUCJ5IEKVD6FHMPYLBZMFZ6NQ/
- https://www.lexpress.fr/societe/metoo-sauf-pour-les-juives-le-cri-dalarme-dune-militante-feministe-WSAJY7TGOJBW5P2LO7COMH7YUU/
- https://www.lexpress.fr/idees-et-debats/amnesty-international-de-lukraine-a-israel-enquete-sur-une-derive-ideologique-MZ5TBHWM4BGKTBA6VT6LY76OXM/

Sans doute faut-il rappeler les faits : :`le 7 octobre 2023, en Israël, des
hommes et des femmes, des enfants et de vieillards ont été massacrés d’une
horrible manière par les terroristes du Hamas <https://www.lexpress.fr/monde/proche-moyen-orient/attaque-du-hamas-israel-de-nouveau-en-guerre-contre-gaza-5HUCJ5IEKVD6FHMPYLBZMFZ6NQ/>`_.

Les femmes l’ont été d’une manière spécifique, violées collectivement jusqu’à
briser leur bassin, mutilées, décapitées et brûlées vives.
Les auteurs, ivres de la jouissance de mort, les ont filmés et mis en ligne
sur les réseaux sociaux.
Les images que personne ne peut oser ignorer ont montré des femmes et parfois
leurs cadavres, exhibés nues comme des trophées sur lesquels on a craché.

Face à l’horreur, `tandis que nombre d’ONG internationales se taisaient <https://www.lexpress.fr/idees-et-debats/amnesty-international-de-lukraine-a-israel-enquete-sur-une-derive-ideologique-MZ5TBHWM4BGKTBA6VT6LY76OXM/>`_,
des féministes ont immédiatement dénoncé ces crimes, beaucoup pourtant
sont restées silencieuses, tandis que certaines n’ont pas hésité à les
considérer comme faisant partie de la "résistance palestinienne".

.. _tribune_hallucinante_2023_11_27:

Mépris et défiguration (une tribune hallucinante)
====================================================

Ainsi, dans une **hallucinante tribune**, les signataires se prétendent même les
gardiennes du sens à donner au "signifiant féministe" (:ref:`1 <remarque_1>`), un sens inédit
dans le combat des femmes, qui considère la dénonciation des violences comme
relevant d’une *obscène propagande* israélienne.

Plus loin, elles estiment que la condamnation des "combattants du Hamas" revient
à "réitérer la vision d’un monde musulman barbare contre une population
israélienne féminisée et ainsi lavée et blanchie de tout soupçon", ou
encore "s’arrime à la construction d’un Orient monstrueux".

Vision orientaliste, **pétrie de condescendance néocoloniale**
---------------------------------------------------------------------

Cette vision orientaliste, **pétrie de condescendance néocoloniale**, semble
illustrer plus que démentir l’imaginaire qu’elles prétendent dénoncer.

**Car pour notre part, nous n’avons vu ni "combattants" ni "Orient monstrueux",
seulement des violeurs et des assassins au service d’une idéologie de mort**.


"La stratégie longuement éprouvée du pinkwashing israélien" est aussi
dénoncée par ses autrices - le pinkwashing étant une démarche commerciale
hypocrite, visant à instrumentaliser les droits des personnes LGBT afin de
se disculper.

Ainsi, peut-être même à leur insu, ces "féministes" d’un nouveau genre reprennent
l’ancienne démonisation du nom d’Israël : le juif, fourbe par définition,
peindrait cette fois-ci en rose, son goût du sang et de la domination.


Les signataires de cette tribune (hallucinante) signent là un énième édit d’expulsion envers les Juives
---------------------------------------------------------------------------------------------------------------

Après le "péril judéo-capitaliste", le "complot judéo-bolchévique",
voici la nouvelle ruse juive pour asseoir sa tromperie.

**Les signataires de cette tribune signent là un énième édit d’expulsion envers
les Juives, cette fois au nom du féminisme**.

**Et c’est bien cela qui s’est traduit dans les faits lors de la manifestation
parisienne du samedi 25 novembre 2023 à Paris**.

Ces considérations se donnent comme un soutien "en tant que féministes" à "la
solidarité avec le peuple palestinien", et comme "un positionnement radicalement
solidaire de la lutte contre le colonialisme et l’impérialisme".

Étrange consentement à la salissure de ce combat que le Hamas a opérée.

**Quant à nous, nous pensons que le soutien aux Palestiniens, tout comme la lutte
anti-impérialiste et anticoloniale méritent mieux que ce mépris et cette
défiguration**.

**Manipulation du féminisme**
===============================

Les désaccords entre féministes et les différences d’analyse sur de
nombreux enjeux ont toujours accompagné les mouvements féministes et il
n’y a pas lieu de le regretter.

Même si les actuels débats liés au conflit israélo-palestinien traversent tous
les courants politiques et particulièrement la gauche, donc ne naissent pas
du champ de la cause des femmes, celui-ci, nous le voyons, est largement
concerné.

Mais il y a des lignes rouges, des franchissements de sens qui tournent le dos  au féminisme
-----------------------------------------------------------------------------------------------

**Mais il y a des lignes rouges, des franchissements de sens qui tournent le dos
au féminisme**.


Une **chute éthique inédite** et une remise en cause de décennies de combats féministes
------------------------------------------------------------------------------------------

**L’aveuglement ou la minimisation des crimes du Hamas** au nom de la
"résistance", **le consentement à son antisémitisme** constituent **une chute
éthique inédite et une remise en cause de décennies de combats féministes**.

**Un pas même est franchi, puisque l’abandon de la solidarité envers toutes
les femmes ne peut en aucun cas se déguiser en féminisme**.

De surcroit, on ne saurait justifier les violences faites aux femmes au nom
d’un combat jugé plus important, qui rend les autres combats secondaires et
qui même les oublie.

Le féminisme, rappelons-le, s’est construit sur le refus de relativiser l’oppression des femmes au nom d’une lutte principale
---------------------------------------------------------------------------------------------------------------------------------

Le féminisme, rappelons-le, s’est construit sur le refus de relativiser
l’oppression des femmes au nom d’une lutte principale.


Qu’il s’agisse de la lutte contre le capitalisme ou contre l’impérialisme, les
féministes ont exigé que leur lutte soit, non secondaire, mais partie prenante
du combat pour l’émancipation humaine.

En outre, elles ont compris la nécessité de construire des mouvements féministes
autonomes, non subordonnés à d’autres luttes.
Le féminisme peut s’associer à d’autres combats mais il ne peut pas se confondre
avec eux.
Il faut cesser – et c’est un minimum - d’entretenir les brouillages.

Pour nous le féminisme additionne:

- lutte contre l’antisémitisme,
- soutien et solidarité envers les juif.ves comme envers les victimes de tous
  les racismes et de toutes les oppressions.

Le féminisme qui est un combat politique ne peut pas, ne doit pas s’adosser au
simplisme idéologique, il peut, il doit s’emparer de la complexité et y séjourner.

Face au conflit israélo-palestinien, **exemple parfait de complexité**, nous
estimons "en tant que féministes" qu’assimiler la condamnation des crimes du
Hamas au soutien aux suprémacistes israéliens et aux colons de Cisjordanie
ou à l’oubli des femmes palestiniennes dans la bande de Gaza **relève de
la manipulation, non seulement du féminisme mais aussi de cette qualité
essentielle qu’est l’honnêteté intellectuelle**.

Il en va du féminisme comme du désir d’émancipation.

Signataires
=============

- :ref:`Brigitte Stora, militante féministe et antiraciste, autrice, <brigitte_stora>`
- Martine Storti, militante féministe, essayiste,
- Bouchra Azzouz, réalisatrice,
- Geneviève Brisac, romancière,
- Sylvaine Bulle, féministe sociologue
- Farida Gueroult, militante féministe et antiraciste
- Fatima Meziane, militante féministe et antiraciste
- Laurence Rossignol, militante féministe, sénatrice socialiste du Val de Marne,
- Catherine Vieu-Charier, militante communiste, ancienne adjointe à la ville
  de Paris, chargée de la mémoire



.. _remarque_1:

1)
---

(1) Propagande de guerre pro-israélienne ; notre féminisme ne se laissera
pas enrôler, signée par un collectif de féministes militantes, chercheuses,
artistes (Le Média, 21 novembre 2023)


**Le sentiment d'Hanna Assouline : la honte**
===============================================

- :ref:`raar_2024:hanna_2024_01_28`
