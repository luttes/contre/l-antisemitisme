.. index::
   pair: Sarah Barukh; #Metoo... sauf pour les juives" : le cri d'alarme d'une militante féministe

.. _barukh_2023_11_27_metoo:

=================================================================================================================================================
2023-11-27 **#Metoo... sauf pour les juives" : le cri d'alarme d'une militante féministe** par Sarah Barukh
=================================================================================================================================================

- https://www.lexpress.fr/societe/metoo-sauf-pour-les-juives-le-cri-dalarme-dune-militante-feministe-WSAJY7TGOJBW5P2LO7COMH7YUU/
- :ref:`barukh_2023_11_27_paria`

Les juives ne sont-elles pas des victimes dignes d'être pleurées et défendues ?
=================================================================================

TRIBUNE - Atterrée par la partialité de collectifs comme NousToutes, la
féministe Sarah Barukh interpelle : les juives ne sont-elles pas des victimes
dignes d'être pleurées et défendues ?

J’ai été violée à 27 ans. J’ai vécu 10 ans de violences
conjugales. J’ai avorté 2 fois, et fait plusieurs fausses couches avant de
connaître la maternité à presque 40 ans.

J’ai découvert les problématiques féministes par le corps.
Depuis, toute attaque délibérée ciblant le féminin m’atteint d’abord par la
chair.
Lorsque j’ai découvert le sort réservé aux femmes et leurs petits le
`7 octobre 2023 en Israël <https://www.lexpress.fr/monde/proche-moyen-orient/attaque-du-hamas-israel-de-nouveau-en-guerre-contre-gaza-5HUCJ5IEKVD6FHMPYLBZMFZ6NQ/>`_, avant même de pouvoir réfléchir, je me suis physiquement
effondrée.

Il y a 4 ans, je me suis enfuie en pleine nuit avec mon bébé.
Hébergée chez mes parents, j’ai lu les féministes de renom, fréquenté les
associations pour bénéficier de sororité, nécessaire dans mon chaos.

Je suis écrivaine. Ce chamboulement, j’en ai finalement fait un livre, "125 et
des milliers" pour savoir qui étaient ces femmes qui mouraient de la violence
des hommes et pourquoi.

Sa publication a bouleversé mon existence.

Les enjeux sociétaux derrière les chiffres m’ont obligée à jamais.
Depuis, j’ai créé "125 et après", où nous aidons chaque jour des femmes et
leurs enfants.

Depuis le 7 octobre 2023, pourtant, mes actions et mon identité cohabitent difficilement
==========================================================================================

Depuis le 7 octobre 2023, pourtant, mes actions et mon identité cohabitent
difficilement.

Parce que voilà, je m’appelle Sarah Barukh.
Je suis juive.

**Et depuis 5 semaines, je vis un déchirement auquel je ne m’attendais pas**.

**Le silence du milieu féministe**
=========================================

Dans le milieu féministe, après le 7 octobre 2023, c’est le silence.

Je suis sidérée.

Puis vient l’électrochoc.

Le 26 octobre 2023, "Nous Toutes", la principale association de soutien aux
femmes, poste sur son compte Instagram un appel au cesser le feu en écrivant ceci::

    Nous condamnerons toujours la colonisation et ses effets post-coloniaux :
    l’acculturation, dominations, humiliations, ingérences, nettoyage ethnique.

    Nous condamnerons toujours les tueries, le terrorisme (qu’il soit étatique
    ou non), les génocides et crimes de guerre.(…)

    Nous prendrons toujours position du côté des victimes, des peuples minorisés
    et opprimé,

puis apparaissent des chiffres sur la situation des femmes à
Gaza avec un drapeau de soutien à la Palestine et enfin une page générale
sur les viols en temps de guerre.

J’ai remonté le fil jusqu’au 7 octobre 2023, rien sur ce qu’avaient subi les femmes en Israël
-----------------------------------------------------------------------------------------------

**J’ai remonté le fil jusqu’au 7 octobre 2023, rien sur ce qu’avaient subi les femmes en Israël**

Le 31 octobre 2023, sur son compte Instagram, "La Fondation des femmes" poste un
portrait en hommage à Mona Chollet, une écrivaine de référence venant pourtant
de publier plusieurs tribunes ouvertement antisionistes.

**La Fondation maintient ce post même après le tweet de l’écrivaine appelant à
ne pas se rendre à la manifestation contre l’antisémitisme**.

Rien sur les femmes victimes en Israël

Depuis un mois, la plupart des médias et influenceuses féministes nouvelle
génération adoptent la même grille de lecture : les femmes déplacées à
Gaza, vivant sous les bombes, parfois enceintes et voyant leur bébé mourir.

Rien sur les femmes victimes en Israël.

**Finalement, le silence parlait**
=========================================

Alors avec d’autres, nous nous interrogeons sur cette partialité.

Il faut bien sûr parler des femmes à Gaza, mais pourquoi ne pas avoir parlé
des femmes de l’autre côté de la frontière ?

**Constater la souffrance des unes, qui découle d’une barbarie inédite, n’est
pas la négation de la souffrance des autres**.

Sélectionner la souffrance, nourrir la comparaison, voire la confrontation, c’est un choix politique
-------------------------------------------------------------------------------------------------------

En revanche, sélectionner la souffrance, nourrir la comparaison,
voire la confrontation, c’est un choix politique.

Face à nos courriers, pas ou peu de retours, réactions justifiées par la peur
des conséquences ou **pire, le soi-disant manque de preuves côté israélien**.

Finalement, le silence parlait.

Durant tout ce temps, il était une réponse.

**Ces féministes-là préfèrent ne donner qu’un seul point de vue : ce qu’elles
désignent être la voix des minorités opprimées et dites racisées**.

**Lorsque sur les réseaux, je commente leurs posts en appelant à des voix de
paix, à plus de nuance et de complexité pour tenter de comprendre ce qui se
passe et surtout, ne pas attiser de haine envers les juifs** ici, je me fais
insulter et traiter de "white feminist", de colonialiste bourgeoise.

Moi, la fille d’exilés tunisiens, russes et polonais, rescapés de la shoah,
née dans une cité !

C’est nous toutes, ou presque. #metoounlessyouareajew
-----------------------------------------------------------

Il m’apparait clair en fait que l’intersectionnalité, la convergence
des luttes, n’incluent pas la minorité juive, du moins les juives
qui ne partagent pas cette vision du monde.
C’est nous toutes, ou presque*. #metoounlessyouareajew.

Entendons-nous bien. Je suis dévastée d’assister impuissante à ce qui se
déroule sous nos yeux. La guerre me désespère. Toutes les guerres.

**Je suis happée chaque matin par un chagrin dévastateur face aux images de
Gaza. Il n’y a pas à mes yeux de morts plus graves parmi les enfants**.

Je pourrais développer mon écœurement sur des lignes mais là n’est pas le
propos de mon indignation qui se résume en un constat : **trop de féministes
ont refusé de dénoncer et de pleurer le féminicide de masse, perpétré
par le Hamas en Israël le 7/10/23**.

Quel est l’objectif ? Une envie de revanche ?

Quel est l’objectif ? Une envie de revanche ?

Après celle des femmes contre les hommes, c’est désormais les femmes dites
racisées contre les "white feminist", en particulier les "sionistes" ?

Atteindre la maternité de la nation
========================================


Il est aujourd’hui prouvé que les centaines de femmes attaquées ont écopé
d’une double peine le 7 octobre 2023.

Des bourreaux faits prisonniers ont avoué les formations qu’ils avaient reçues
pour torturer femmes et enfants, et les violer vivants puis morts.
L’objectif était d’atteindre la maternité de la nation, souiller, traumatiser
pour empêcher toute fécondité personnelle et symbolique.

Les autorités israéliennes, en présence de journalistes du monde entier ont
décrit et montré les atteintes à cette féminité avec des viols en série
(jusqu’à 30 fois pour certaines), une femme a été retrouvée écartelée,
une autre a été tellement abusée que son pelvis était fracturé, une
femme enceinte a été éventrée et son bébé encore relié tué alors
qu’elle était vivante, des petites filles ont été violées et vidées
de leur sang après mutilation de membres, des femmes ont été abattues en
plein viol, leurs bourreaux ont ensuite découpé leur sein pour jouer avec.

**Et tout cela filmé en direct et posté sur Internet**, parfois même en appelant
les familles pour qu’elles assistent aux viols et assassinat de leurs enfants.

**Et tout cela clamé haut et fort** dans une jouissance insoutenable des agresseurs
et de leurs sympathisants civils de retour chez eux.

Je ne comprends pas où peut se situer l’hésitation
--------------------------------------------------------

Je ne comprends pas où peut se situer l’hésitation.

Ou plutôt si, malheureusement, **Sarah la juive comprend très bien**.

Pour autant, il me semble que nous, **féministes humanistes**, avons l’obligation
de dénoncer ce terrorisme qui est certes religieux mais tout autant
masculiniste.

Il suffit de lire le sort réservé aux femmes dans l’islam radical et d’observer
ce qui se passe en Iran, en Afghanistan et partout ailleurs où le fanatisme
a gagné.

La cohérence de nos engagements nous oblige à reconnaître que le Hamas a
violé les femmes parce qu’elles étaient femmes, les a humiliées, les a
particulièrement faites souffrir pour atteindre les hommes et la Nation,
a détruit leurs enfants, a joui de ces tortures, en a ri, a atteint leur
intégrité physique par des mutilations effroyables, a filmé ces tortures,
les a publiées sur internet et envoyées directement aux familles pour
qu’elles assistent, impuissantes à ces atrocités.

**Au-delà de la mort il y a l’obscénité, le crime d’objectisation, de soumission
et le sadisme parce qu’elles étaient femmes et qu’ils étaient leurs enfants,
tous utilisés comme arme de guerre**.

Je me souviens, l’an passé, de nombreuses célébrités se sont élevées
pour dénoncer le sort réservé aux femmes en Iran sous la dictature des
ayatollahs et ses répressions mortelles.

Dans une vidéo aux centaines de milliers de vues, des célébrités se coupaient
une mèche de cheveux en soutien à celles qui n’ont pas le droit de montrer
les leurs.

Les femmes iraniennes sont les 1ères à défendre Israël depuis le 7/10.

Elles ont identifié sans hésitation la similitude de menace.
Pourquoi est-ce si difficile pour tant de féministes en France ?

Pour moi le féminisme n’a qu’une ligne directrice : les femmes pour la paix
et la paix pour les femmes.



----

1) 125 étant le nombre officiel moyen de féminicides en France. **L'association
Mémoire Traumatique et Victimologie*** nom de # trouvé par Annabelle Show

**Le sentiment d'Hanna Assouline : la honte**
===============================================

- :ref:`raar_2024:hanna_2024_01_28`
