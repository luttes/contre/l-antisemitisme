.. index::
   pair: Twitter ; Anne Hidalgo (Pour rester fidèle à mes convictions et à mon engagement, je quitte aujourd’hui X)

.. _hidalgo_2023_11_27:

========================================================================================================================
2023-11-27 **Anne Hidalgo : « Pour rester fidèle à mes convictions et à mon engagement, je quitte aujourd’hui X »**
========================================================================================================================

- https://www.lemonde.fr/idees/article/2023/11/27/anne-hidalgo-pour-rester-fidele-a-mes-convictions-et-a-mon-engagement-je-quitte-aujourd-hui-x-ex-twitter_6202575_3232.html
- https://www.lemonde.fr/international/article/2023/11/18/la-derive-complotiste-d-elon-musk-culmine-avec-son-tweet-antisemite-et-fait-vaciller-x_6201004_3210.html
- https://www.lemonde.fr/pixels/article/2023/08/15/twitter-deserte-par-les-defenseurs-de-l-environnement-depuis-son-rachat-par-elon-musk_6185472_4408996.html

Préambule
===========

Dans une tribune au « Monde », la maire de Paris explique pourquoi elle vient
de supprimer son compte.

Selon elle, le réseau social X (ex-Twitter), propriété du milliardaire Elon Musk
est devenu un outil de déstabilisation de la démocratie


Texte
=======

Pourquoi je quitte Twitter.

J’ai pris la décision de quitter Twitter.

Loin d’être l’outil révolutionnaire qui, au départ, permettait un accès
à l’information au plus grand nombre, Twitter est devenu ces dernières
années l’arme de destruction massive de nos démocraties.

Manipulation, désinformation, amplification des pulsions de haine, harcèlement
organisé, `antisémitisme <https://www.lemonde.fr/international/article/2023/11/18/la-derive-complotiste-d-elon-musk-culmine-avec-son-tweet-antisemite-et-fait-vaciller-x_6201004_3210.html>`_
et racisme avéré**, meutes attaquant les scientifiques,
les climatologues, les femmes, les `écologistes <https://www.lemonde.fr/pixels/article/2023/08/15/twitter-deserte-par-les-defenseurs-de-l-environnement-depuis-son-rachat-par-elon-musk_6185472_4408996.html>`_, les progressistes et toutes
celles et tous ceux de bonne volonté qui souhaitent un débat politique serein
et apaisé dans un monde de plus en plus complexe : la liste des dérives
est infinie.

Sans oublier les ingérences étrangères quotidiennes qui interférent dans les
processus électoraux et portent atteinte à l’image et à la souveraineté
de nos démocraties en voulant les déstabiliser.

Aujourd’hui, les polémiques, les rumeurs et les manipulations grossières
dictent le débat public, propulsées par l’algorithme de Twitter, où seul
compte le nombre de « Like ». Qu’importent les faits.

**Cette plateforme et son propriétaire agissent délibérément pour exacerber
les tensions et les conflits**.

Ils entravent en outre délibérément les informations nécessaires à
l’avènement de la transformation écologique et énergétique radicale
dont nous avons besoin, au profit de discours climato-sceptiques, promus
par les intérêts des énergies fossiles et de la prédation sans limite de
la planète. Nous pouvons continuer sans cesse de démentir, de déminer et
d’expliquer, mais le bruit engendré par une fausse nouvelle sera toujours
largement supérieur à l’écho d’une vérité étayée.

Ne nous trompons pas. Il s’agit d’un projet politique très clair qui veut se
passer de la démocratie et de ses valeurs pour de puissants intérêts privés.

Le dernier rapport de transparence sur le contrôle des contenus, publié par
Twitter lui-même, classe la France comme détentrice de la palme d’or des
propos violents et illicites en Europe.

Ce média est devenu un vaste égout mondial et nous devrions continuer de
nous y précipiter ?

On le voit tous les jours : Twitter empêche le débat, la recherche de la
vérité, le dialogue serein et constructif nécessaires entre les êtres
humains. Avec ces milliers de comptes anonymes et ces fermes à trolls, ce
qui se passe sur Twitter n’est pas la vie démocratique mais son exact opposé.

Je refuse de cautionner ce dessein funeste.

Je crois profondément à la démocratie, toujours à parfaire.

Je crois à la discussion, dans les temps difficiles que nous traversons.

Ne nous laissons pas intimider par des déstabilisations abjectes.

Ne laissons pas les « ingénieurs du chaos » prendre nos destins en main.

Ne laissons pas nos démocraties se déliter tous les jours sur nos écrans.

En mars 2009, j’ai été l’une des premières femmes politiques françaises
à rejoindre ce réseau, comptant aujourd’hui une communauté de plus d’1
500 000 abonnés à travers le monde.

Pour rester fidèle à mes convictions et à mon engagement, je quitte
aujourd’hui Twitter.

Je resterai sur d’autres réseaux sociaux où existe encore l’échange
respectueux : linktr.ee/anne_hidalgo

Plus que jamais il faut continuer à faire vivre la démocratie réelle,
celle des conseils municipaux, des assemblées citoyennes, des votations, des
conférences, des rencontres. Autant de lieux physiques à hauteur de regard,
où l’on se voit, où l’on se dispute, où on construit ensemble et où
tout simplement on vit ensemble.

Quand tout s’assombrit, il faut se tourner vers la lumière et ces grands
esprits qui, à certains moments de notre histoire, nous ont montré le
chemin. « Nous devons apprendre à vivre ensemble comme des frères, sinon
nous allons mourir tous ensemble comme des idiots », nous disait Martin Luther
King au début des années 1960.

Faisons-nous confiance et sachons retrouver le chemin de la démocratie,
de la paix et de la fraternité, nous en avons le pouvoir.

Retrouvez cette tribune dans @lemondefr

- https://www.lemonde.fr/idees/article/2023/11/27/anne-hidalgo-pour-rester-fidele-a-mes-convictions-et-a-mon-engagement-je-quitte-aujourd-hui-x-ex-twitter_6202575_3232.html
