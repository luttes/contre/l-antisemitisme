

.. _liberation_2023_11_27:

=================================================================================================================================================
2023-11-27 **«On était comme des parias» : l’amertume de féministes juives, empêchées de manifester contre les violences faites aux femmes**
=================================================================================================================================================

- https://www.liberation.fr/societe/droits-des-femmes/on-etait-comme-des-parias-lamertume-de-feministes-juives-empechees-de-manifester-contre-les-violences-faites-aux-femmes-20231127_ZGSNGU5FRJB2PKLOFMYNPL5H5Q/


Message

	Voici un article publié dans Libération aujourd'hui à propos de la manifestation
	parisienne du 25 novembre 2023, et de la façon dont des féministes juives
	ont été menacées et tenues à l'écart de la manifestation.

	Pour moi, en tant que féministe, c'est profondément choquant de constater
	le silence de beaucoup trop de féministes sur les violences sexistes et
	sexuelles commises par le Hamas le 7 octobre et de voir que des femmes
	qui souhaitaient à juste titre dénoncer ces violences ont été de facto
	empêchées de manifester.

	Une règle essentielle du féminisme, c'est de croire et de respecter TOUTES
	les victimes de violences patriarcales.

	Et là, ce n'est pas le cas depuis le 7 octobre 2023, il y a une invisibilisation
	et une remise en question des victimes israéliennes qui est inacceptable.


Introduction
============

Un collectif formé après le 7 octobre 2023 pour porter la voix des victimes
israéliennes affirme avoir été tenu à l’écart de la manifestation parisienne
samedi 25 novembre 2023 contre les violences faites aux femmes.
La ministre de l’Egalité a notamment réagi.


«Nous étions là pour dire : “Femmes juives, on vous croit”», résume Sarah, 38 ans
====================================================================================

«Nous étions là pour dire : **Femmes juives, on vous croit**, résume
Sarah, 38 ans, l’une des fondatrices de **Nous vivrons**

Samedi 25 novembre 2023, à l’appel de ce collectif formé après le 7 octobre 2023,
et composé d’anciens militants de l’Union des étudiants juifs de France (UEJF)
et de SOS Racisme, quelque 200 personnes, en majorité des femmes d’origine juive,
s’étaient rassemblées pour «porter la voix des victimes israéliennes du Hamas et
dénoncer le silence assourdissant des associations féministes».

Sauf que les manifestantes, armées de pancartes «Metoo unless you are a Jew» ou
«Féministes, votre silence vous rend complices», affirment ne pas avoir pu
quitter la place de la Nation pour rejoindre le cortège parisien contre les
violences faites aux femmes.

La police israélienne confrontée à la complexe enquête sur les viols du 7 octobre 2023
===========================================================================================

«Alors qu’on faisait le tour de la place en attendant le démarrage de la manif,
on a été prises à partie **par des mecs du NPA et de Révolution permanente»**,
raconte Léa, 48 ans, revêtue, comme la plupart des participantes, d’un jogging
gris taché de rouge à l’entrejambe rappelant le sort tragique de Naama Levy, 19 ans,
violée puis exhibée par des militants du Hamas dans les rues de Gaza.

Une dizaine de membres du Service de protection de la communauté juive (SPCJ),
qui encadrait le cortège, se sont alors interposés.

Une empoignade s’en est suivie mais, rapidement, «ça s’est calmé», poursuit Léa.

«Sauf qu’ensuite, on est restées deux heures place de la Nation, encerclées
par un cordon de CRS, pendant que les autres avaient le droit de manifester»,
poursuit, amère, cette prof d’histoire qui n’a pas voulu donner son nom.

«Une négation des viols commis par le Hamas»
==================================================

«Il y avait un groupuscule qui scandait : “Siamo tutti antifascisti”.

Vêtu de noir, le visage masqué», confirme **Camille Vizioz-Brami, élue socialiste
dans le XIXe arrondissement**. «Rien de méchant, mais se sentir menacées par
ces hommes en noir, le jour de la marche contre les violences envers les femmes,
franchement c’était désagréable.»

Cette féministe de 47 ans a mal vécu de n’avoir pu défiler avec le reste du
cortège. «On était comme des parias. J’en retire un grand sentiment de solitude
et d’échec», souligne la conseillère municipale.

"Finalement, dans cette manif, on a vécu tout ce qu’on dénonce : ne pas
représenter les bonnes victimes (trop juives), **faire face à des hommes qui
nous ont menacées, sans solidarité ni sororité** – le comble pour une manif féministe !"
abonde Amélie Prévot, 47 ans.

Pour la reconnaissance d’un féminicide de masse en Israël le 7 octobre 2023
=================================================================================

Après avoir piétiné dans le froid, les organisatrices ont finalement donné
l’ordre au groupe de se disperser vers 15 h 45.

Consigne a été donnée d’abandonner les pancartes sur l’herbe, de se débarrasser
des autocollants et d’ôter les joggings ensanglantés, pour ne pas attirer
l’attention sur le chemin du retour.

«Mais je tiens à préciser qu’à aucun moment la police nous a interdit de marcher.
Simplement, ils nous ont vivement déconseillé de rejoindre le cortège, car en
face on avait un mur d’antifas qui ne bougeait pas», précise Sarah.

Contactée dimanche soir, la préfecture de police a indiqué n’être pas en mesure
de confirmer dans l’immédiat avoir recommandé la dispersion.

Alors que l’indignation enflait, la ministre de l’Egalité entre les
femmes et les hommes a tenu à réagir dimanche.
«Comme je l’ai dit, il faut condamner toutes les violences faites aux femmes.
Y compris les viols de masse comme arme de guerre, en Israël et ailleurs, a
écrit Bérangère Couillard dans un message posté sur Twitter (renommé X).

Je suis choquée que certains hier n’aient pas voulu que ce message soit entendu.»

Au téléphone dimanche soir, Diane, une militante de NousToutes, principale
organisation à l’appel de cette manifestation qui a rassemblé 80 000 personnes
selon la CGT (16 500 selon la préfecture de police), a estimé que «le traitement
par les associations féministes de la question israélo-palestinienne est
assez biaisé : on ne parle que de ce qui se passe à Gaza».

Et appelé le mouvement féministe à «faire son mea culpa sur la façon dont il a géré
cette question des crimes du Hamas».

Pour Diane, «le slogan “Je te crois” est essentiel dans le combat féministe,
or vis-à-vis des femmes israéliennes il y a clairement un deux poids deux mesures»,
avec une **«minimisation, voire une négation des viols commis par le Hamas»**.

**Terrassées de douleur**
============================

Elle dit n’être pas seule à se «poser des questions» :

«Après la marche, nous avons reçu beaucoup de témoignages de femmes qui se
sont senties mal à l’aise car elles pensaient participer à une manif pour défendre
les femmes et ont eu l’impression de se retrouver dans une manif pour défendre
la Palestine», ajoute la militante, qui vise en particulier l’activisme
du collectif **Du pain et des roses, émanation de Révolution permanente**.


NousToutes
-------------

Le doute semble déjà gagner d’autres branches locales du collectif féministe
NousToutes, à l’instar de NousToutes Paris Nord.

Interpellé sur X, il s’est dit dimanche soir «solidaire avec les femmes de monde entier»,
précisant : «Le féminicide de masse des Israéliennes nous a terrassées de douleur.»

Les Femen aussi sont sorties de leur silence dimanche soir
---------------------------------------------------------------

Les Femen aussi sont sorties de leur silence dimanche soir.

«Nous avons vu les images. Nous avons vu le sang couler le long de leurs jambes»,
ont-elles écrit sur Instagram, à propos des Israéliennes violées.

«Il est de notre devoir de le dire, de leur rendre femmage et de réclamer leur
libération», ont ajouté ces activistes, pour qui «dénoncer les crimes du
7 octobre 2023 n’invisibilise en rien les violences quotidiennes que subissent
les femmes palestiniennes».


.. _barukh_2023_11_27_paria:

**Sarah Barukh** (Votre ignorance, votre refus d’écouter nos voix de juives AUSSI, est LE problème)
-------------------------------------------------------------------------------------------------------

- :ref:`barukh_2023_11_27_metoo`

De son côté, la féministe Sarah Barukh, qui était dans le cortège pour dénoncer
la hausse des féminicides en France, a aussi interpellé samedi dans un message
sur Instagram les associations féministes::

    «Votre ignorance, votre refus d’écouter nos voix de juives AUSSI, est LE problème.
    C’est ainsi que croît la haine.»
