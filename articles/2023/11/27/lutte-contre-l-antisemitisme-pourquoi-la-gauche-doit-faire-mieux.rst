.. index::
   pair Humanité ; Lutte contre l'antisémitisme : pourquoi la gauche doit "faire mieux" (2023-11-27)

.. _humanite_2023_11_27:

=======================================================================================================================================
2023-11-27 **Lutte contre l'antisémitisme : pourquoi la gauche doit "faire mieux"** par Florent le-du et Cyprien caddeo (Humanité)
=======================================================================================================================================

- https://www.humanite.fr/politique/antisemitisme/lutte-contre-lantisemitisme-pourquoi-la-gauche-doit-faire-mieux
- https://www.humanite.fr/auteurs/florent-le-du
- https://www.humanite.fr/auteurs/cyprien-caddeo

Introduction
==============

Une partie des forces progressistes est accusée d’avoir cédé aux sirènes de
cette forme de racisme.

Mauvais procès ou signe qu’elles auraient failli dans la défense des concitoyens
de confession juive qui se sentent abandonnés ?

Des voix demandent à la gauche de se ressaisir, loin des allégations outrancières
orchestrées par la droite et l’extrême droite.

Comment en sommes-nous arrivés à ce monde à l’envers ?
===========================================================

La voix, rageuse, s’élève au-dessus des huées : "Collabos ! C’est
eux les vrais fachos !"

L’homme vise le groupe d’élus socialistes, communistes et écologistes, venus
participer à la marche du 12 novembre 2023, à Paris, contre l’antisémitisme.

À quelques centaines de mètres, le cortège d’extrême droite n’est pas autant
chahuté.
**Comment en sommes-nous arrivés à ce monde à l’envers ?**

Bien sûr, vingt ans de dédiabolisation de l’extrême droite et des décennies
de stigmatisation de la cause pro-palestinienne, suspectée de se nourrir de
l’antisémitisme, sont passés par là.
Mais plusieurs voix progressistes pointent également, tantôt un "malaise",
tantôt une "minimisation", voire un "déni", sur la question de l’antisémitisme, à gauche.


Une jeune femme franco-israélienne est venue me voir, surprise que des communistes marchent contre l’antisémitisme. Nous ne sommes plus identifiés à ce combat-là, nous devons y travailler
============================================================================================================================================================================================

"Ce qui est sûr, c’est qu’un fil s’est rompu, constate Ian Brossat,
porte-parole du PCF. Pendant la marche (du 12 novembre 2023 – NDLR), une jeune
femme franco-israélienne est venue me voir, surprise que des communistes
marchent contre l’antisémitisme. Nous ne sommes plus identifiés à ce
combat-là, nous devons y travailler."

**Concurrence entre les luttes**
===========================================

Pas identifiés, car peu audibles.

**C’est le premier point d’achoppement : la gauche ne s’exprimerait pas assez
sur l’antisémitisme.**
**Comme si le sujet avait été relégué au second plan**.

"Dans nos partis a pu s’installer l’idée de concurrence entre les luttes",
reconnaît Eva Sas, députée écologiste.

La montée et la gravité de l’islamophobie auraient-elles fait oublier dans
les mouvements de gauche que l’antisémitisme perdure ?

Pourtant, tous les racismes opèrent en même temps", avance Illana Weizman
============================================================================

"On entend encore trop souvent dans le camp antiraciste que les juifs ne sont
plus une minorité véritablement oppressée.

Que post-Shoah, finalement, l’antisémitisme serait devenu marginal.
Or, il est toujours très opérant. Il y a aussi l’idée que l’islamophobie aurait
remplacé l’antisémitisme.

Pourtant, tous les racismes opèrent en même temps", avance Illana Weizman.
L’autrice du livre Des Blancs comme les autres ? (Stock, 2022) – essai sur
cette question précise – ajoute que "l’existence d’Israël, et ses différentes
politiques, a amené de l’eau au moulin à l’idée que les juifs sont des oppresseurs,
des dominants, et donc jamais des oppressés".

Produites de toutes parts, des confusions entre juifs, Israéliens et
gouvernement d’Israël complexifient la lutte contre l’antisémitisme.

Albert Herszkowicz du RAAR
-------------------------------

Albert Herszkowicz, militant du Réseau d’actions contre l’antisémitisme et tous
les racismes (RAAR), a **"souvent entendu dans les mouvements antiracistes,
depuis une quinzaine d’années, la crainte d’être accusé de soutenir le
gouvernement israélien dès lors qu’on s’engage contre la haine anti-juifs"**.

"Je ressens presque une suspicion", ajoute Eva Sas à propos des partis
de gauche.

Un signal de cette gêne : "C’est la seule lutte à laquelle on se sent toujours
obligé d’en associer une autre, comme si on avait peur d’en faire trop sur les
juifs", remarque le rabbin Emile Ackermann.

La gauche, dans sa pluralité, est depuis quelques années très rarement à l’initiative des marches contre l’antisémitisme
============================================================================================================================

Symbole de cette rupture : la gauche, dans sa pluralité, est depuis
quelques années très rarement à l’initiative des marches contre
l’antisémitisme.

Lors de l’assassinat d’Ilan Halimi par Youssouf Fofana et son "gang des barbares",
en 2006, ou lors des attentats de Mohamed Merah en 2012, la gauche tarde à
prendre en compte leur gravité et l’émotion suscitée dans la communauté juive.

Par peur de faire le jeu de l’extrême droite, d’une part, et, d’autre part,
que les coupables soient racisés "en a perturbé beaucoup", estime Albert Herszkowicz,
comme si les oppressés ne pouvaient structurellement être à leur tour parfois,
des oppresseurs.


À l’extrême gauche par exemple, en 2006, la Ligue communiste révolutionnaire (LCR) choisit de ne pas se rendre à la marche pour Ilan Halimi
==============================================================================================================================================

À l’extrême gauche par exemple, en 2006, la Ligue communiste révolutionnaire
(LCR) choisit de ne pas se rendre à la marche pour Ilan Halimi.


Le sociologue Philippe Corcuff
=================================

Le sociologue :ref:`Philippe Corcuff <philippe_corcuff>`, membre de la LCR à l’époque,
se souvient : "Il y avait une minimisation du caractère antisémite du meurtre.
C’était une rupture avec les années 1980, où la Ligue était à l’initiative des
marches.

Le psychanalyste Gérard Miller
=================================

**"À la Révolution française en permettant aux juifs de devenir des citoyens
à part entière, au moment de l’affaire Dreyfus ou pendant et à la sortie
de la Seconde Guerre mondiale, la gauche s’est trouvée du bon côté de
l’histoire"**, ajoute le psychanalyste Gérard Miller.

Sans relativiser l’importance du combat, Ersilia Soudais, vice-présidente
du groupe de travail contre l’antisémitisme à l’Assemblée nationale
et membre de la France insoumise, régulièrement pointée du doigt, esquisse
une explication sur ce recul de la lutte contre l’antisémitisme à gauche
"D’un point de vue historique, l’antisémitisme est globalement en recul.
Nous n’avons plus un antisémitisme d’État."

Elle ne nie pas cependant "qu’il existe des préjugés à gauche, comme il existe du
sexisme ou du racisme".

Des clichés persistants
=============================

C’est là une des principales critiques faites aux responsables et militants
de gauche.

Deux mille ans de clichés et de stéréotypes haineux contre les juifs nous précèdent.

Et si un Français juif ne connaît plus les mêmes discriminations – embauches,
contrôle au faciès, etc. – qu’un Français d’origine noire ou arabe, cela ne
veut pas dire que le problème de l’antisémitisme est réglé.

Emile Ackerman
--------------------

"Pour beaucoup aujourd’hui, ce serait juste la haine viscérale ou la volonté
d’exterminer les juifs.
Mais il y a tout un imaginaire collectif antisémite occidental, chrétien, qui
continue d’être véhiculé", déplore le rabbin Emile Ackerman.


Les clichés comme l’idée que les juifs auraient "trop de pouvoir dans le domaine de l’économie" ou "dans les médias" est partagée par plus de 30 % des électeurs de gauche, contre 25 % en moyenne nationale.
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Les clichés comme l’idée que les juifs auraient "trop de pouvoir dans le domaine
de l’économie" ou "dans les médias" est partagée par plus de 30 % des électeurs
de gauche, contre 25 % en moyenne nationale.

La "Radiographie de l’antisémitisme en France" de 2022 par la Fondapol
montre que des clichés comme l’idée que les juifs auraient "trop de
pouvoir dans le domaine de l’économie" ou "dans les médias" est
partagée par plus de **30 % des électeurs de gauche, contre 25 % en moyenne
nationale**.

Prompts – à raison – à combattre le racisme ordinaire, systémique, **les mouvements
de gauche sont invités à le faire avec la même force contre la diffusion de
stéréotypes judéophobes**.

**Or, certains propos tenus à gauche peuvent contribuer à les alimenter**.


Jean-Luc Mélenchon reprenant ainsi la thèse antisémite du peuple  déicide longtemps entretenue par l’Église
--------------------------------------------------------------------------------------------------------------

Chez les juifs de gauche que l’Humanité a interrogés, deux citations de
Jean-Luc Mélenchon reviennent inlassablement.
En juillet 2020, sur BFM TV, le leader de la FI déclare : "Je ne sais pas si
Jésus était sur la croix, mais je sais que, paraît-il, ce sont ses propres
compatriotes qui l’y ont mis", reprenant ainsi la thèse antisémite du peuple
déicide longtemps entretenue par l’Église.

En 2021, interrogé sur Éric Zemmour, lui-même juif, Jean-Luc Mélenchon
estime cette fois que son adversaire "ne doit pas être antisémite parce
qu’il reproduit beaucoup de scénarios culturels qui sont liés au judaïsme".

Autant que ces propos eux-mêmes, c’est l’absence d’autocritique qui
est dénoncée. Bien que sur ce dernier exemple l’insoumis s’est dit "prêt
à admettre" s’être "mal exprimé".

"Je ne dis pas que Mélenchon, pour citer cet exemple, est antisémite, ce n’est
pas le sujet. Mais il faut se remettre en question quand des propos comme
ceux-là sont prononcés, prendre du recul, se demander d’où ils viennent",
précise Illana Weizman.

La critique sociale à gauche doit faire attention à ne pas tomber dans certains écueils
============================================================================================

Si de nombreuses accusations d’antisémitisme paraissent régulièrement
abusives, la critique sociale à gauche doit faire attention à ne pas tomber
dans certains écueils.


Jonas Pardo, membre du  collectif Golem (@collectifgolem@piaille.fr )
---------------------------------------------------------------------------

Comme personnifier la domination économique, alerte Jonas Pardo, membre du
collectif Golem : "Cette personnification peut entrer en collision avec
l’antisémitisme, on le voit particulièrement avec la dénonciation des Rothschild
par exemple", développe **celui qui a créé une formation sur la question à
destination des mouvements de gauche**.

Philippe Corcuff
---------------------

- https://www.humanite.fr/politique/droite/philippe-corcuff-le-confusionnisme-renforce-la-possible-victoire-du-rn-en-2027-803567

Philippe Corcuff y voit le signe "d’un dérèglement confusionniste à gauche:
quand on passe de la critique structurelle marxiste à la stigmatisation
individuelle des personnes, on se met à chercher des boucs émissaires.
On ne vise plus le capital mais Rothschild ou Soros.
On passe de la quête d’émancipation au ressentiment".


L’historienne Marie-Anne Matard-Bonucci
-------------------------------------------

Un vieux stéréotype associant les juifs à la finance largement entretenu
au XIXe siècle par la gauche elle-même, rappelle l’historienne Marie-Anne
Matard-Bonucci : **"L’image du juif banquier se diffuse, quand l’immense
majorité des juifs est de condition modeste**.

Cela va de pair avec une **vision complotiste du pouvoir**, où le juif, puisqu’il
a l’argent, détient le pouvoir et tire les ficelles."

Amalgames et confusion
==========================

Jonas Pardo
--------------

Des slogans entendus dans des manifestations pro-palestiniennes sont aussi
pointés du doigt, comme : "Séparation du Crif et de l’État" ou
"Macron, payez-le en sheckels".

"Cela réactive l’antisémitisme politique puisqu’elle implique que les institutions
juives dicteraient la politique étrangère de l’État", considère Jonas Pardo.

Marie-Anne Matard-Bonucci
------------------------------

La question israélo-palestinienne a été parfois une courroie de
transmission entre cet imaginaire haineux multiséculaire et la colère
légitime suscitée par la politique coloniale israélienne.

"C’est au prétexte de l’anticapitalisme, via l’antiaméricanisme, que s’opère
la jonction entre l’antisémitisme et le conflit israélo-palestinien",
avance l’historienne Marie-Anne Matard-Bonucci.

Huma
----

Mais comment définir la frontière entre la critique légitime du gouvernement
israélien et l’usage, conscient ou inconscient, de clichés antisémites ?
D’autant plus quand les instrumentalisations noient le débat public
et ajoutent de la confusion.

**La clé à gauche réside sans doute, comme le défendent Illana Weizman ou Jonas Pardo,
dans une meilleure formation au sein des partis**.

Et dans la reprise de discussions sereines, constructives.

Y compris avec les instances représentatives, dont le Conseil représentatif
des institutions juives de France, qui doivent redevenir un interlocuteur,
défendent certains à gauche, même si le Crif est aligné sur la politique du
Likoud israélien et avait exclu en 2016 Jean-Luc Mélenchon de la marche pour
Mireille Knoll, le renvoyant dos à dos avec Marine Le Pen.

"Il faut pouvoir écouter ce que ressentent les juifs.

On a besoin de pédagogie, de réfléchir à ce qu’il se passe, développe Jonas Pardo.

La gauche doit être plus forte sur ces combats.

Et ainsi refermer la porte par laquelle s’est engouffrée l’extrême droite,
principale artisane de la judéophobie au XXe siècle.

"À gauche, à de rares expressions près, l’antisémitisme n’a jamais été théorisé,
souligne Gérard Miller. À l’extrême droite, oui, c’est même consubstantiel à
ce mouvement."

Rappel utile. Et raison de plus de faire mieux.
