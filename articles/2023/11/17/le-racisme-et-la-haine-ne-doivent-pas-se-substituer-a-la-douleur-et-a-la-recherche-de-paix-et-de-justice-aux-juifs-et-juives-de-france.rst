
====================================================================================================================================================================
2023-11-17 **Le racisme et la haine ne doivent pas se substituer à la douleur et à la recherche de paix et de justice. Aux Juifs et Juives de France**
====================================================================================================================================================================


- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/17/le-racisme-et-la-haine-ne-doivent-pas-se-substituer-a-la-douleur-et-a-la-recherche-de-paix-et-de-justice-aux-juifs-et-juives-de-france/

Le massacre perpétré par le Hamas a suscité chez nous, et dans
l’ensemble du monde juif, une révolte, une colère et une inquiétude
légitime, en particulier pour celles et ceux qui ont de la famille
en Israël.

Mais cette colère ne doit nous faire perdre ni la raison, ni notre
humanité.

Il s’agit de faire la part des choses entre ce que nous ressentons,
légitimement, en tant qu’individu·es ayant subi une perte, se sentant
meurtri·es dans une facette de leur identité  tout en comprenant ce que
peuvent éprouver tout aussi légitimement celles et ceux qui sont lié·es
aux Palestiniens et Palestiniennes,
et l’analyse critique qu’exige la situation politique.

Ce que nous écrivions en mai 2021 est plus que jamais d’actualité
: « aux Juifs et aux Juives de France qui ont les yeux rivés
sur le Jourdain : si vous avez à cœur la situation là-bas,
profitez du recul dont vous bénéficiez pour pousser au dialogue
et à l’apaisement. Agiter des drapeaux en revendiquant un soutien
inconditionnel alors que la politique du gouvernement israélien est
indéfendable ne fait qu’aggraver une situation déjà désastreuse. »
(https://juivesetjuifsrevolutionnaires.wordpress.com/2021/05/18/aux-juifs-et-aux-juives-de-france-a-propos-disrael/)

La fuite en avant dans une surenchère guerrière, dans le soutien à
la violence des colons en Cisjordanie et dans la déshumanisation et le
massacre de milliers de civil·es palestinien·nes à Gaza ne fera pas
revenir les otages, et encore moins les mort·es israélien·nes. On ne
répare pas une injustice par une autre. Le gouvernement d’extrême
droite au pouvoir en Israël ne fait que plonger ses habitant·es vers
l’abîme, et certains de ses ministres multiplient les appels aux accents
génocidaires (1). Leur mépris tant de la vie des otages israélien·nes
que celle des civil·es palestinien·nes est flagrant, tout comme leur
soutien à une vision théocratique et ethno-nationaliste qui n’a jamais
produit que mort et désolation. Une vision qui, par ailleurs, les amène
à fouler aux pieds la mémoire de la Shoah en la trivialisant sans cesse,
comme l’a fait l’ambassadeur d’Israël en portant une étoile jaune à
l’ONU ou Netanyahu faisant porter la responsabilité de la Shoah au grand
mufti de Jérusalem qui aurait « soufflé la solution finale à Hitler »
contre toute preuve historique.  En privant les Palestinien·nes de toute
perspective politique et individuelle, en encourageant la politique de
colonisation dans les territoires occupés, ce gouvernement a contribué au
renforcement du Hamas, l’un et l’autre faisant office de « meilleurs
ennemis » dont les dynamiques se renforcent mutuellement.

Depuis le 7 octobre 2023, les propos et actes antisémites explosent en France
(plus de 1500 actes recensés en à peine plus d’un mois) et dans
le monde.

Pour les Juifs et Juives en diaspora dont nous faisons partie, nous
savons que la période qui vient est lourde de danger, et qu’il va
nous falloir y faire face. Y faire face, cela ne signifie pas perdre nos
valeurs et basculer dans l’abomination du racisme. La résilience ne peut
se traduire par une simple impression de renforcement de ses défenses
individuelles face à un « ennemi » fantasmé en bloc. Résister face
à l’émergence d’un mouvement raciste ou antisémite nécessite de
penser une solidarité collective, qui pense la sécurité des autres,
même celleux que la colère semble nous rendre étranger·es. La
réponse à l’antisémitisme ne pourra jamais résider dans le racisme
et l’islamophobie, mais au contraire doit s’insérer dans la lutte
antiraciste. Car en France non plus, on ne répare pas une injustice par
une autre. Il nous appartient de résister sans concession aux appels du
pied hypocrites des politicien·nes de tout poil pour qui la défense des
Juifves en France n’est aucunement une fin en soi mais uniquement un moyen
de véhiculer leur racisme par la désignation de coupables préfabriqués.

Zemmour, l’antisémite qui a réhabilité le régime Pétainiste au
rôle actif dans la Shoah et qui remet en cause l’innocence de Dreyfus,
est scandaleusement invité sur certains médias communautaires, dont
I24 et Torah-Box, ce dernier média s’étant également illustré par
de nombreux tweets racistes.

Le RN, parti fondé par un Waffen SS et qui héberge de nombreux
antisémites militants se présente frauduleusement comme « Bouclier des
Juifs ». Cette affirmation est non seulement une insulte à l’Histoire,
à nos histoires individuelles et collectives, mais surtout une pure
chimère. Nous n’oublions pas non plus les efforts de réhabilitation
de Bainville, Maurras et Petain par Darmanin et Macron, dont la collusion
avec l’extrême droite est manifeste.  La chaîne de télévision Cnews
financée par le milliardaire Bolloré, qui sert constamment de plateforme
à la haine raciale, utilise la période actuelle pour développer une
explication culturaliste de l’antisémitisme.  Ici aussi l’extrême
droite tente d’embarquer les Juifves dans une prétendue « guerre de
civilisation » qui opposerait « l’occident judéo-chrétien » et
« le monde arabo-musulman ». Mais une telle vision, inspirée par les
thèses du néo-conservateur Samuel Huntington, à laquelle souscrivent
les fanatiques de tous bords, est non seulement une impasse pour les
Juif·ves mais aussi un oubli de notre histoire. Elle revient à effacer
deux mille ans d’hostilité antijuive en Occident, et à s’allier
avec les héritier·es des responsables du génocide des Juif·ves qui
continuent à diffuser un antisémitisme violent, ayant abouti à plusieurs
attentats comme celui de Halle ou de la Synagogue « Tree of life » de
Pittsburgh. En France, c’est un militant de ce courant identitaire,
Claude Hermant, qui a fourni des armes à l’auteur de l’attentat de
l’hypercasher en 2015, le djihadiste Amedy Coulibaly.  Se jeter dans les
bras hypocrites de l’extrême droite suprémaciste blanche n’amènera
pas la sécurité pour les Juifs et les Juives car nous resterons toujours,
en définitive, une cible légitime pour ces antisémites. On ne répare
pas une injustice par une autre.  La lutte contre l’antisémitisme doit
s’inscrire dans la remise en cause d’une vision raciste du monde que
partagent toutes les nuances de l’extrême droite internationale, des
suprémacistes blancs aux djihadistes en passant par les kahanistes. Cette
lutte doit ramener de l’analyse politique là où le racisme vise à
déshumaniser et essentialiser les groupes, sommés de « choisir un camp
» défini par des frontières ethniques, faussement homogène.

L’extrême droite, partout dans le monde, ne porte en elle que la guerre
et la mort.

Ici comme là-bas, la réparation du monde ainsi que la paix juste et
durable, passent par la construction de stratégies communes contre le
racisme, contre l’extrême droite, contre les logiques guerrières et
la déshumanisation.

Dans cette perspective, soutenons les voix qui en Israël et en Palestine ou
ailleurs revendiquent un cessez-le-feu des deux côtés conjointement avec
une libération des otages, l’arrêt de la colonisation en Cisjordanie
et la reprise d’un processus de paix juste et durable plutôt que la
surenchère guerrière.


(1) Voir notamment :
https://www.i24news.tv/en/news/israel/politics/1699174305-netanyahu-suspends-minister-who-suggested-israel-should-nuke-gaza
