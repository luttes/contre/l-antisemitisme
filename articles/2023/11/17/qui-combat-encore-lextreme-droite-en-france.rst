.. index::
   pair: Philippe Marlière; Qui combat encore l’extrême droite en France ? (2023-11-17)

.. _marliere_2023_11_17:

===========================================================================================
2023-11-17 **Qui combat encore l’extrême droite en France ?** par Philippe Marlière
===========================================================================================

- :ref:`philippe_marliere`


.. figure:: images/golem.png

   https://piaille.fr/@collectifgolem/111425434166543339

Préambule
============

Apres notre ami Philippe Corcuff, à mon tour d'inaugurer une chronique
politique mensuelle dans L'Obs.

Elle est en libre accès sur le site du journal. Je vous donne le lien
vers l'article en dessous de mon message.

J'ai intitulé ma chronique "La Journée d'un scrutateur" (emprunt direct
au titre de roman d'Italo Calvino publié en 1963).

**Mes tribunes se veulent un espace d'observation critique et de description
du monde politique dans sa confuse complexité**. (En tout cas, j'essaierai...).

Je reviens dans cette premiere contribution sur la marche contre
l’antisémitisme de dimanche dernier. Le RAAR et Golem y sont mentionnés.

LA JOURNEE D’UN SCRUTATEUR
-------------------------------

LA JOURNEE D’UN SCRUTATEUR. Une carte blanche du politiste Philippe Marlière,
un nouveau rendez-vous qui se veut un espace d’observation critique et
de description du monde politique dans sa confuse complexité.

Qui combat encore l’extrême droite en France ?
================================================

Le Rassemblement national (RN), un parti à l’héritage antisémite
indiscutable, a participé sereinement, parfois même sous les
applaudissements, à la marche contre l’antisémitisme à Paris, le 12 novembre.

En 2018, présente à la marche organisée après le meurtre de Mireille Knoll,
une octogénaire juive, Marine Le Pen avait été contrainte quitter le
cortège sous protection policière.

Le RN se présente aujourd’hui comme le « meilleur bouclier des juifs
de France ». Marine Le Pen le martèle d’ailleurs depuis plusieurs années :
en 2014, elle déclarait que le RN « se trouve aux côtés des juifs pour
la défense de nos libertés de pensée ou de culte face au seul vrai
ennemi, le fondamentalisme islamiste ».

En 2013, Louis Aliot estimait que « l’antisémitisme était la seule chose
qui empêchait encore les gens de voter » pour le FN.


La banalisation achevée du RN
====================================

La banalisation du FN/RN s’étire en fait sur une période de quarante années.

Elle a toutefois connu une accélération importante sous les quinquennats
d’Emmanuel Macron. Une rhétorique confusionniste n’a cessé depuis six
ans de ménager, voire de valoriser le RN.

Depuis 2022, des ministres ont rivalisé de formules de politesse à l’égard
du parti de Marine Le Pen : Elisabeth Borne a affirmé que celle-ci et
son groupe « respectent les formes » ;

Olivier Dussopt a estimé que Le Pen « était plus républicaine que d’autres »,
ou encore Yaël Braun-Pivet a considéré que Sébastien Chenu, député du RN,
« n’était pas un bon, mais un très bon vice-président de la Chambre.  »

Reflet de cette banalisation réussie, 59 % des Français étaient contre
le « rejet du RN » à l’occasion de la marche nationale.


En invitant à la marche « tous ceux qui se reconnaissent dans les valeurs
de la République », Yaël Braun-Pivet et Gérard Larcher ont déroulé le
tapis rouge au RN.

L’appel à manifester était lénifiant : « Pour la République, pour la laïcité, contre l’antisémitisme »,
renonçant à toute action pédagogique à l’égard du public (qu’est-ce que
l’antisémitisme, comment reconnaître un trope antisémite ? etc.).

Les « valeurs de la République », notion attrape-tout, et la laïcité ont
été invoquées pour justifier le vote de la loi sur le « séparatisme» en 2021.

Cet épisode avait donné libre cours à une surenchère islamophobe à droite.

En quête de dédiabolisation depuis quinze ans, le RN n’a de
cesse d’épouser avec zèle les notions totémiques françaises: la République,
la laïcité et, aujourd’hui, la lutte contre l’antisémitisme.

Notons que ces trois éléments sont utilisés pour ostraciser des musulmans
réputés « infidèles aux valeurs de la République », « hostiles à la laïcité »,
et afficher une fermeté face au terrorisme islamiste.

En se détournant de ce front commun contre l’antisémitisme, la gauche française a renoncé à l’une de ses missions historiques : c’est l’autre fait majeur.
============================================================================================================================================================

Le 12 novembre 2023, le RN a marché avec les « forces de l’arc républicain»
sans que cela ne provoque de contestation majeure.

Voilà le fait nouveau.

« Maintenant, il faut se retourner contre le fascisme en formant
un seul front », estimait Léon Trotsky en 1931.

**En se détournant de ce front commun contre l’antisémitisme, la gauche
française a renoncé à l’une de ses missions historiques : c’est l’autre fait majeur**.

Une partie de la Nouvelle Union populaire écologique (l’ex-Nupes) était
certes présente :

- Europe Ecologie Les Verts (EELV),
- le Parti communiste français (PCF)
- et le Parti socialiste (PS).

La volonté d’éduquer les adhérents à la question de l’antisémitisme
s’affiche à gauche depuis peu (notamment à EELV).

Le Réseau d’Actions contre l’Antisémitisme et tous les Racismes (:ref:`RAAR <raar>`)
est invité dans les partis pour sensibiliser les militants à l’antisémitisme.

Les syndicats commencent à se pencher sur la question aussi (dont la CGT,
pourtant absente de la marche).

La « gauche mélenchoniste » était absente de la marche, cette gauche est indifférente à l’antisémitisme
===========================================================================================================

- https://www.nouvelobs.com/opinions/20220804.OBS61669/cette-gauche-que-l-antisemitisme-indiffere-par-philippe-marliere.html

La « gauche mélenchoniste » était absente de la marche.

Cette gauche est `indifférente à l’antisémitisme <https://www.nouvelobs.com/opinions/20220804.OBS61669/cette-gauche-que-l-antisemitisme-indiffere-par-philippe-marliere.html>`_,
c’est pour elle un « non-sujet».

Il y eut le premier communiqué de LFI après les assassinats barbares
du Hamas le 7 octobre 2023, aux relents relativistes, qui ne condamnait pas
l’organisation terroriste.

Puis vinrent les explications filandreuses de Jean-Luc Mélenchon refusant
de qualifier les actes du Hamas de « terroristes ».

Une série de tweets agressifs et douteux à l’égard de Yaël Braun-Pivet,
d’origine juive, ont suivi : le 22 octobre 2023, après une manifestation
propalestinienne à Paris, Mélenchon a tweeté :
« Voici la France. Pendant ce temps Mme Braun-Pivet est à Tel Aviv pour
encourager le massacre. Pas au nom du peuple français !  »

Il est édifiant que les critiques de M. Mélenchon aient vu dans l’emploi
du verbe « camper », un dérapage antisémite.
En réalité, ce verbe est anodin.

Mais tout le monde ou presque est passé à côté du trope antisémite
=====================================================================

Mais tout le monde ou presque est passé à côté du trope antisémite :
le leader de LFI a établi une distinction entre les « vrais » Français
et Mme Braun-Pivet.

**Il a ainsi sous-entendu que Braun-Pivet, juive, était une agente d’un
Etat étranger**.

A travers cette représentation antisémite, les juifs sont perçus comme
des individus déracinés ; ce sont de mauvais patriotes qui ont fait
allégeance à Israël.


Mélenchon est coutumier de sorties péremptoires sur les juifs et le  judaïsme qui empruntent au répertoire des stéréotypes antisémites
==========================================================================================================================================

- https://reporterre.net/La-gauche-a-une-vraie-incapacite-a-identifier-ce-qu-est-l-antisemitisme

Mélenchon est coutumier de sorties péremptoires sur les juifs et le
judaïsme qui empruntent au répertoire des stéréotypes antisémites.

Des militants de gauche antiracistes ont analysé, ici et `là (La gauche a une vraie incapacité à identifier ce qu’est l’antisémitisme) <https://reporterre.net/La-gauche-a-une-vraie-incapacite-a-identifier-ce-qu-est-l-antisemitisme>`, ce tropisme
mélenchoniste.

La gauche a déserté sa mission historique
==============================================

- :ref:`Golem <golem>`
- https://piaille.fr/@collectifgolem/111425434166543339

**La gauche a déserté sa mission historique**

Mélenchon a considéré que la manifestation du 12 novembre 2023 était un
« prétexte » pour apporter un « soutien au massacre israélien » à Gaza.

Le soir de la marche, le leader de LFI a de nouveau tweeté pour
minorer l’importance de l’événement, en déclarant qu’il avait
« échoué ».

.. figure:: images/golem.png

   https://piaille.fr/@collectifgolem/111425434166543339

En réalité, cette marche digne a été un succès à deux réserves près :

1) le RN y était présent ;
2) la gauche a déserté le combat d’éducation et de mobilisation contre l’extrême
   droite.

Seul :ref:`Golem <golem>`, **un petit groupe de militants de gauche juifs,
a tenté courageusement de protester sur place contre le RN**.

**Pour la protection des juifs, la gauche repassera**.


un RN banalisé a pu participer en toute tranquillité à une marche contre l’antisémitisme en dépit de son pedigree antisémite
=================================================================================================================================

Ceci nous amène à ce constat inquiétant : **un RN banalisé a pu participer
en toute tranquillité à une marche contre l’antisémitisme en dépit de
son pedigree antisémite**.

**La responsabilité est générale**.

Mais la gauche, affaiblie, divisée, indifférente, voire hostile au
ressenti des juifs, a été incapable de construire un front commun
contre l’antisémitisme.

Il faut donc se demander : qui combat encore l’extrême droite en France ?

Par Philippe Marlière professeur de sciences politiques
