.. index::
   pair: Mapping ; https://oct7map.com/

.. _mapping_7_octobre_2023:

===========================================================================================
2023-11-21 Un massacre, des massacres : les archives du 7 octobre 2023 et la mémoire
===========================================================================================

- https://k-larevue.com/un-massacre-des-massacres-les-archives-du-7-octobre-et-la-memoire/

Dès le lendemain du massacre du 7 octobre 2023, un travail d’archivage et
de documentation a été entrepris qui rend compte d’un premier effort
pour élaborer et intégrer dans la conscience de chacun l’ampleur de
l’événement.

Ce travail de mémoire immédiat s’inscrit dans un imaginaire collectif
et un ensemble de pratiques testimoniales qui fait remonter à la fois
l’histoire de la Shoah et celle des pogroms. Sensible à l’ambigüité de
la société israélienne, Frédérique Leichter-Flack interroge les effets
de cet entrelacement mémoriel des massacres, entre reviviscence traumatique
et ressource pour ne pas se laisser sidérer par la Gorgone.

Site Mapping the Massacres
------------------------------------

- https://oct7map.com/
