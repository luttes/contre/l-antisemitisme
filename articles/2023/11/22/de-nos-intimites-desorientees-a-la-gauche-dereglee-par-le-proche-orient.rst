.. index::
   pair: Philippe Corcuff ; 2023-11-22
   pair: Tsedek ; Quand la gauche perd la boussole (2023-11-22)

.. _philippe_corcuff_2023_11_22:

===============================================================================================================
2023-11-22 **De nos intimités désorientées à la gauche déréglée par le Proche-Orient** par Philippe Corcuff
===============================================================================================================

- https://www.nouvelobs.com/opinions/20231122.OBS81170/de-nos-intimites-desorientees-a-la-gauche-dereglee-par-le-proche-orient.html


Préambule
===========

Le principe de la chronique est de passer par les cultures populaires
de masse (cette fois des chansons : Simon and Garfunkel, Axelle Red,
Louane et Emma Peters).
J'y propose ensuite une critique croisée de Guillaume Meurice/Tsedek !
et de Cary Nelson/Caroline Fourest

Cette chronique revient sur la précédente à propos de Guillaume Meurice :
"De Israël-Gaza à Arras : pour une politique de la fragilité", 17 octobre 2023,
https://www.nouvelobs.com/idees/20231017.OBS79624/de-israel-gaza-a-arras-pour-une-politique-de-la-fragilite.html


ROUVRIR LES IMAGINAIRES POLITIQUES. La quête du sens peut frôler le
non-sens : dans nos vies personnelles, comme le chantent Axelle Red,
Louane et Emma Peters, ou dans le brouillard généré au sein des gauches
par la guerre israélo-palestinienne.

« Hello darkness, my old friend », chantaient en 1964 Simon and
Garfunkel dans le ténébreux « The Sound of Silence ».

L’obscurité envahit ainsi parfois nos intimités égarées, et encore davantage quand
nos repères collectifs se déglinguent, comme aujourd’hui chez les
spectateurs des tragédies du 7 octobre 2023 et de Gaza.

Du désarroi existentiel : Axelle Red, Louane et Emma Peters
--------------------------------------------------------------------

En 1996, dans « A quoi ça sert », Axelle Red fait résonner nos doutes :
« A quoi ça sert/Ces sentiments profonds, oh non/Je me sens de travers/A
force de voir mon monde à l’envers/Faut qu’je cesse de m’inventer
des questions/Non/A quoi ça sert ».

Nos vies, ma vie a-t-elle un sens ? Dans quel état j’erre ?
Des interrogations fondamentales et futiles, sincères et ridicules,
périlleuses et surjouées, qui nous arrêtent un moment sur le chemin,
en s’éternisant dangereusement pour quelques-uns.

En 2015, Louane sort en single la chanson « Maman ».
Les incertitudes existentielles prennent aussi le pas sur le cours ordinaire de la vie :
« J’suis pas bien dans ma tête, maman/J’ai perdu le goût de la
fête, maman/Regarde comme ta fille est faite, maman/J’trouve pas de
sens à ma quête, maman ». Le brouillard s’épaissit.

Nos désirs s’ébrèchent : « Ces nuits où les promesses se tissent/Aussi vite
qu’elles se dilapident ». Nos utopies intimes suffoquent : « Les
rêves s’entassent dans les métros/Les gratte-ciel nous regardent de
haut/Comme un oiseau sous les barreaux ».


Avec Emma Peters, dans « Terrien », en 2022, la tentation de
l’irréparable se dessine : « Est-ce que tu sais pourquoi j’suis née
? /Je me fais pas confiance/Pourquoi à chaque fois que j’vois le vide
j’ai envie de sauter ». T’es rien ? En tout cas, la perplexité enfle,
y compris et surtout à l’égard de soi-même : « Souvent j’me demande
comment tourne la Terre/J’ai besoin d’espace j’sais pas comment
faire/Il faut que j’me casse il faut que j’prenne l’air/J’ai trop
d’angoisses j’voudrais les faire taire ».


Introduction
===============

Nos égarements intérieurs ne sont pas sans rapport avec le monde
extérieur et avec les repères collectifs, les valeurs, qu’on n’a pas
construits chacun avec nos petits bras (pas très) musclés et qui nous
aident pourtant à nous orienter individuellement.

Quand ces repères sont affectés par un événement dramatique, comme la guerre en cours
au Proche-Orient, nos inquiétudes existentielles peuvent s’en trouver
avivées.

.. _tsedek_2023_11_22:

Quand la gauche perd la boussole : de Guillaume Meurice/Tsedek ! à Cary Nelson/Caroline Fourest
===================================================================================================

- https://www.nouvelobs.com/idees/20231017.OBS79624/de-israel-gaza-a-arras-pour-une-politique-de-la-fragilite.html
- https://www.nouvelobs.com/monde/20231031.OBS80261/guerre-israel-hamas-saisines-de-l-arcom-pour-des-propos-tenus-par-guillaume-meurice-et-caroline-fourest.html
- https://www.nouvelobs.com/idees/20231109.OBS80605/tout-le-champ-politique-banalise-la-shoah-entretien-avec-le-militant-antiraciste-jonas-pardo.html

Du côté de celles et de ceux pour qui être de gauche constitue une
coordonnée existentielle forte, les chocs ont été rudes : non seulement
l’horreur du 7 octobre 2023 en Israël et ces corps d’enfants tués et
blessés à Gaza, mais aussi ici, dans un registre tragicomique, ayant
peu à voir avec les conséquences des massacres là-bas, **les myopies
croisées à gauche**.

Dans ma `précédente chronique <https://www.nouvelobs.com/idees/20231017.OBS79624/de-israel-gaza-a-arras-pour-une-politique-de-la-fragilite.html>`_, j’ai valorisé l’éthique de la
fragilité qui se dégage d’un récent livre de Guillaume Meurice, «
Petit Eloge de la médiocrité » (Editions Les Pérégrines).

Patatras !

Il y a aussi un « côté obscur de la force » chez l’humoriste de
France-Inter. Car il a fait surgir `le 29 octobre dernier, au cours de
l’émission « le Grand Dimanche soir » <https://www.nouvelobs.com/monde/20231031.OBS80261/guerre-israel-hamas-saisines-de-l-arcom-pour-des-propos-tenus-par-guillaume-meurice-et-caroline-fourest.html>`_,
une blague douteuse sur les antennes du service public avec la caricature
du Premier ministre israélien Benyamin Netanyahou en
*une sorte de nazi sans prépuce*.

Antisémite Meurice ? Il n’y a aucune raison de le penser
---------------------------------------------------------

- https://www.nouvelobs.com/idees/20231109.OBS80605/tout-le-champ-politique-banalise-la-shoah-entretien-avec-le-militant-antiraciste-jonas-pardo.html

Antisémite Meurice ? Il n’y a aucune raison de le penser.

Par contre, il a repris, sans le savoir, une assimilation routinisée entre « juif » et «
nazi » qui est devenue `un stéréotype antisémite <https://www.nouvelobs.com/idees/20231109.OBS80605/tout-le-champ-politique-banalise-la-shoah-entretien-avec-le-militant-antiraciste-jonas-pardo.html>`_.


En mettant sur le même plan la Shoah et les crimes de guerre en cours  à Gaza, il a mis le doigt dans une relativisation négationniste du judéocide
----------------------------------------------------------------------------------------------------------------------------------------------------------------

Et, **en mettant sur le même plan la Shoah et les crimes de guerre en cours
à Gaza, il a mis le doigt dans une relativisation négationniste du judéocide**.

Cela apparaît particulièrement dangereux dans une période de recrudescence
des actes et des propos antisémites, en France et dans le monde.

Chacun peut faire des erreurs et le maniement de l’ironie est
particulièrement propice aux maladresses.

Il aurait pu s’excuser, mais il a préféré bétonner dans l’autojustification.

« Cette soif d’autodéfense me grise », chante Axelle Red dans « A quoi ça
sert ».
« L’orgueil de l’humain sape sa clairvoyance », écrit en écho… Meurice
lui-même dans son livre, sans en avoir tiré suffisamment de conséquences
quant à sa propre humilité.


Cela ne justifie aucunement les ignobles menaces de mort dont il a été
l’objet.


.. note:: Lire aussi `France-Inter et Guillaume Meurice, de la polémique au malaise <https://www.nouvelobs.com/economie/20231114.OBS80855/france-inter-et-guillaume-meurice-de-la-polemique-au-malaise.html>`_


.. _tsedek_2023_11_22_decolonial:

Le point de vue décolonial ; **les dérapages de Tsedek**
===========================================================

Le point de vue décolonial, né parmi des intellectuels latino-américains
à la fin des années 1990, nous a permis de percevoir des aspects trop
souvent enfouis des réalités contemporaines.

Mais quand il se prétend presque exclusif en matière de racisme, il
débouche sur un aveuglement vis-à-vis d’autres dimensions d’un réel composite.

C’est le risque porté par le collectif juif décolonial Tsedek ! dans un
communiqué du `3 novembre 2023 publié sur Mediapart <https://blogs.mediapart.fr/tsedek/blog/041123/l-antisemitisme-doit-etre-combattu-son-instrumentalisation-aussi>`_
et intitulé « L’antisémitisme doit être combattu, son instrumentalisation aussi ».

Il avance notamment::

    « Si la rhétorique du Hamas emprunte volontiers des références à
    l’antisémitisme européen, son antijudaïsme est surtout adossé à l’association
    de l’identité juive au statut de colon dans le cadre des rapports sociaux
    engendrés par la colonisation israélienne. En contexte palestinien, les
    catégories “Juif” et “Arabe” renvoient d’abord à des rapports de pouvoir
    et de domination. »

Ce qui tend à relativiser les meurtres antisémites perpétrés par le Hamas et
**ouvre même la voie à une justification de ces crimes comme réaction
qui pourrait être considérée comme légitime** « à des rapports de
pouvoir et de domination ».

Ces dérapages sont rendus possibles par l’aplatissement des violences racistes sous la catégorie de « colonialisme ».
=========================================================================================================================

- https://www.cairn.info/revue-vacarme-2002-3-page-36.htm

**Ces dérapages sont rendus possibles par l’aplatissement des violences
racistes sous la catégorie de « colonialisme »**.

Dans un `article de 2002 de la revue « Vacarme » <https://www.cairn.info/revue-vacarme-2002-3-page-36.htm>`_
sur « Les divisions de la gauche mouvementée », Michel Feher distinguait
de manière suggestive deux angles en conflit dans les débats de l’époque :
« le racisme colonial » et « le racisme génocidaire ».

Si l’on choisit le premier, on minore le second.

Et vice et versa.

Et il y a même d’autres angles possibles. Par exemple,
l’angle décolonial rend mal compte du racisme anti-Noirs aux Etats-Unis.

.. note:: Lire aussi  `Robert Hirsch : « La gauche de la gauche a minoré l’antisémitisme » <https://www.nouvelobs.com/idees/20231114.OBS80853/robert-hirsch-la-gauche-de-la-gauche-a-minore-l-antisemitisme.html>`_


Les dérèglements à gauche viennent également d’autres secteurs opposés aux premiers
========================================================================================

Les dérèglements à gauche viennent également d’autres secteurs
opposés aux premiers.

Cary Nelson est un universitaire américain issu de la gauche marxiste,
aujourd’hui favorable à une paix au Proche-Orient basée sur deux Etats
mais très hostile à la campagne de désobéissance civile
BDS (Boycott, Désinvestissement et Sanctions) visant Israël.

Dans un article intitulé « A ceasefire would normalise the pogrom »
(« Un cessez-le-feu normaliserait le pogrom ») et publié fin octobre
sur le site Fathom, il met donc en cause le mot d’ordre de « cessez-le-feu à Gaza ».

Au nom de la guerre contre le Hamas, les morts civils palestiniens
comptent peu, l’hécatombe apparaît sous sa plume, malgré tout, justifiée.

**Si l’expression déshumanisante de «dommages collatéraux » n’est pas
directement utilisée, elle hante implicitement son texte**.


**Humanismes à géométrie variable**
======================================

Caroline Fourest
-----------------------

- https://twitter.com/CarolineFourest/status/1718878528383361401?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1718878528383361401%7Ctwgr%5Ec65e18b013b60699d8b971039e05965f96a8c7c1%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fwww.huffingtonpost.fr%2Fpolitique%2Farticle%2Fguerre-israel-hamas-apres-les-propos-de-fourest-sur-les-morts-de-gaza-les-deputes-lfi-saisissent-l-arcom_225122.html

La journaliste Caroline Fourest a aussi eu des mots brouillant la
boussole émancipatrice sur `BFMTV le 29 octobre 2023 <https://twitter.com/CarolineFourest/status/1718878528383361401?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1718878528383361401%7Ctwgr%5Ec65e18b013b60699d8b971039e05965f96a8c7c1%7Ctwcon%5Es1_&ref_url=https%3A%2F%2Fwww.huffingtonpost.fr%2Fpolitique%2Farticle%2Fguerre-israel-hamas-apres-les-propos-de-fourest-sur-les-morts-de-gaza-les-deputes-lfi-saisissent-l-arcom_225122.html>`_.

Elle y refuse de «renvoyer dos à dos les victimes », en précisant :
« On ne peut pas comparer le fait d’avoir tué des enfants délibérément en attaquant
comme le fait le Hamas, et le fait de tuer des enfants involontairement
en se défendant comme le fait Israël. »

Elle a certes raison de noter des différences entre les crimes contre
l’humanité réalisés par le Hamas le 7 octobre, en tant que carnage visant
principalement des civils, et les crimes de guerre que réalise l’armée
israélienne, en tant que tuerie de civils générée par une opération militaire.

Cependant, elle n’en reste pas à cette différence et nous entraîne dans
une relativisation plus radicale en refusant de « renvoyer dos à dos les
victimes » et de « comparer » les deux massacres.

Or, les victimes de crimes de guerre ont la même dignité, en tant que
victimes justement, que les victimes de crimes contre l’humanité.

Et comparer ce n’est pas poser une identité, mais c’est explorer des
ressemblances et des dissemblances.

Par ailleurs, elle absout avec son « involontairement »
le gouvernement israélien de sa responsabilité.

Pourtant il y a bien un sacrifice volontaire de milliers de civils
palestiniens au nom de buts de guerre.

.. note::  Voir aussi `Delphine Horvilleur-Kamel Daoud, l’entretien croisé : « Nous devons réaffirmer notre humanité » <https://www.nouvelobs.com/idees/20231025.OBS79981/delphine-horvilleur-kamel-daoud-l-entretien-croise-nous-devons-reaffirmer-notre-humanite.html>`_


Conclusion
==============

A l’écart des humanismes à géométrie variable, ce qu’il reste
d’une gauche en miettes ne devrait-elle pas se soucier des complications
du réel :

- l’injustice historique structurelle à l’égard des Palestiniens
  doit être combattue,
- l’analyse décoloniale est inadéquate pour saisir les violences antisémites,
- les crimes contre l’humanité du Hamas en Israël, les crimes de guerre
  du gouvernement israélien à Gaza et les crimes contre l’humanité commis
  par des colons israéliens en Cisjordanie devraient relever de la Cour pénale internationale.


« People writing songs that voices never share », chantaient Simon and
Garfunkel.

A l’inverse, nous avons besoin d’arracher des fragments de récits
partagés pour rebâtir une gauche d’émancipation sur les décombres actuels.

En tenant compte de nos bringuebalements individuels et collectifs
entre sens et non-sens.
