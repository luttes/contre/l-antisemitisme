.. index::
   pair: Tal Bruttman ; L’opération du 7 octobre 2023 ne vise pas seulement à tuer, mais à filmer les tueries et les atrocités (2023-11-21)
   pair: Tal Bruttman ; Génocides (2023-11-21)

.. _tal_bruttmann_2023_11_21:

=========================================================================================================================
2023-11-21 **L’opération du 7 octobre 2023 ne vise pas seulement à tuer, mais à filmer les tueries et les atrocités**
=========================================================================================================================

- :ref:`tal_bruttmann`
- https://www.mediapart.fr/journal/international/211123/l-operation-du-7-octobre-ne-vise-pas-seulement-tuer-mais-filmer-les-tueries-et-les-atrocites



Préambule
============

- https://fr.wikipedia.org/wiki/Fondation_pour_la_m%C3%A9moire_de_la_Shoah

Pour l’historien Tal Bruttmann, la séquence ouverte depuis le 7 octobre 2023
est révélatrice d’un changement du régime d’images et de cécités
sur la mémoire de la Shoah et ses usages, en Israël comme en France.

Tal Bruttmann est historien, spécialiste d’Auschwitz, de
l’Holocauste et de la politique de Vichy à l’égard des populations
juives.

Il est membre de la `Fondation pour la mémoire de la Shoah <https://fr.wikipedia.org/wiki/Fondation_pour_la_m%C3%A9moire_de_la_Shoah>`_
et auteur de nombreux ouvrages, parmi lesquels La Logique des bourreaux
(Hachette, 2003), Au bureau des affaires juives : l’administration française et
l’application de la législation antisémite (La Découverte, 2006)
ou Auschwitz (La Découverte, collection Repères, 2015).

Entretien.

Mediapart

    Vous avez récemment publié « Un album d’Auschwitz. Comment
    les nazis ont photographié leurs crimes » (Le Seuil, 2023).

    Les tueurs du Hamas ont filmé leurs actes.

    Les autorités israéliennes diffusent auprès des médias et politiques
    occidentaux un montage de certains de ces films.

    A-t-on changé de régime d’images par rapport à une époque où des
    cinéastes tels Rithy Panh ou Claude Lanzmann inscrivaient leur
    travail documentaire face à « l’image manquante » des éliminations
    commises par les nazis ou les Khmers rouges ?

Tal Bruttmann : Je ne suis pas philosophe, mais historien, et je ne reprendrai pas cette notion d’« image manquante »
========================================================================================================================


Tal Bruttmann : Je ne suis pas philosophe, mais historien, et je ne
reprendrai pas cette notion d’« image manquante ».

**D’autant que l’idée que l’image manque est fausse pour l’Holocauste**.
On dispose de nombreuses photos de fusillades de juifs en Europe
orientale, et ces exécutions documentent la Shoah.

Mais il y a effectivement des évolutions du régime d’images.
La Seconde Guerre mondiale, si on la compare à la Première Guerre ou aux
débuts de la colonisation durant lesquels les outils photographiques
et cinématographiques demeurent rudimentaires, est marquée par une
profusion d’images de violences et d’atrocités, notamment en Chine
et en Corée, mais aussi en Europe.


La spécificité de ces images est toutefois, hormis quelques cas
particuliers, qu’elles ne sont pas prises en vue d’être diffusées
largement et publiquement. Elles sont à usage interne, comme ces photos
de corps de personnes abattues en France par les Allemands et les collabos
qui relèvent d’une forme d’identification judiciaire ou policière
mais ne sont pas destinées au grand public.

Les nazis ont également produit de très nombreuses photos administratives,
qui constituent des images des violences commises, mais dont le côté
sordide est occulté et qui ne sont pas faites pour être vues de
manière large.

Les photos du ghetto de Varsovie après sa prise par les nazis sont prises
comme des souvenirs d’un moment considéré comme héroïque.


L’idée est de montrer le plus largement possible la violence qu’on exerce sur ceux qu’on considère comme des ennemis
=======================================================================================================================

Cela n’a rien à voir avec la démarche de Mohammed Merah avec sa GoPro,
de Daech ou du Hamas le 7 octobre 2023, où **l’idée est de montrer le plus
largement possible la violence qu’on exerce sur ceux qu’on considère
comme des ennemis**.

**Il s’agit bien de rendre la terreur le plus visible possible**,
alors que, dans les années 1930, Staline fait tourner les moteurs des
camions pour masquer les bruits des exécutions des « traîtres » à
la Loubianka.

L’opération du 7 octobre 2023 ne vise pas seulement à tuer,
**mais à filmer les tueries et les atrocités, à produire des images pour
augmenter la terreur**.



**Par ailleurs, en histoire, on n’a pas besoin d’images pour établir les faits**
=================================================================================

Mediapart

    L’idée que l’image pouvait fonctionner comme rempart face au déni
    ou au négationnisme est-elle périmée ?

La grande idée, en 1945, est celle-ci. Les images de la libération des
camps ou celles des fusillades prises par les nazis eux-mêmes visent
à montrer au monde entier ce que les nazis ont commis et, ainsi, à
justifier la nécessité de les vaincre et de les juger.

Ce paradigme a longtemps dominé au point d’être utilisé par les
négationnistes : on n’a pas d’images des chambres à gaz, donc
elles n’auraient pas existé ; on n’a pas d’images d’un avion
détruisant le Pentagone, donc aucun n’est tombé dessus…

Mais les dénis et mensonges s’accommodent tout à fait de l’existence
d’images. Celles des avions percutant le World Trade Center n’ont
pas empêché d’y voir un complot de la CIA ou du Mossad.

**Par ailleurs, en histoire, on n’a pas besoin d’images pour établir les faits**.

Le déni ou le négationnisme se jouent sur tous les plans. Si vous montrez
les images des atrocités commises par le Hamas mais que vous les refusez
pour des raisons idéologiques, vous allez affirmer qu’elles ont en
réalité été produites par Israël ou par une intelligence artificielle,
et certaines personnes peuvent naïvement adhérer à ces négations.


Il faut sans doute aller même au-delà dans l’interprétation du geste de négation des violences documentées
=============================================================================================================

Il faut sans doute aller même au-delà dans l’interprétation du geste de
négation des violences documentées.

Dans le cas du négationnisme d’un Robert Faurisson, par exemple, **il s’agit
de redoubler la violence et l’humiliation** en infligeant aux survivants
ou aux proches un discours affirmant que la réalité atroce qu’ils ont
vécue dans leur chair n’a pas existé.


.. _tal_bruttmann_2023_11_21_genocides:

Sur la définition d'un génocide
===================================

Mediapart

    Que produit sur l’historien de la Shoah que vous êtes l’emploi de
    plus en plus fréquent du mot « génocide » pour désigner ce qui se
    passe à Gaza ?

La question est celle de l’effet recherché par l’emploi d’un tel terme.

L'idée générale est d’en faire le plus grave des crimes, l’alpha et l’oméga
de la nécessité de se dresser contre un événement meurtrier.

Mais la notion est juridiquement et politiquement complexe.

Si l’on se réfère à la définition du génocide, les Khmers rouges
n’en ont pas commis un avec l’assassinat de près de trois millions
de Khmers quand ils visaient à se débarrasser du « nouveau peuple »
corrompu par la modernité au profit de « l’ancien peuple ».

Mais l’assassinat de plusieurs dizaines de milliers de Vietnamiens et de
Chams par ces mêmes Khmers rouges constitue des génocides.

Le massacre de Nankin commis en Chine par les Japonais en 1937, qui
est un des plus meurtriers de l’histoire, n’est pas considéré
comme un génocide.

En 1994, les États-Unis ont refusé de qualifier le génocide des Tutsis
du Rwanda comme tel parce que cela les aurait obligés à intervenir…


En tant qu’historien, dire « génocide » me paraît à la fois inutile et inapte à décrire ce que nous avons sous les yeux
==========================================================================================================================

En tant qu’historien, dire « génocide » me paraît à la fois
inutile et inapte à décrire ce que nous avons sous les yeux.

Chaque génocide, même si on se réfère à ceux qui sont reconnus comme tels,
est singulier.

Dans le cas du génocide des Arméniens, contrairement à ce qui s’est
produit pour les juifs, des femmes sont enlevées et réduites en esclavage,
et des enfants sont capturés puis élevés par des familles kurdes.

.. note:: `À lire aussi Guerre au Proche-Orient : le « génocide », terme juridique
          et arme politique 1 novembre 2023 <https://www.mediapart.fr/journal/international/011123/guerre-au-proche-orient-le-genocide-terme-juridique-et-arme-politique>`_

La qualification de génocide est quasi impossible à chaud
=============================================================

La qualification de génocide est quasi impossible à chaud, elle relève
du travail à froid des juristes, mais **certaines nuances sont aujourd’hui
inaudibles dans le débat hystérisé que nous connaissons**.

Si je dis que tuer des civils dans des bombardements peut ne pas relever
d’une intentionnalité génocidaire mais de la volonté de détruire
des infrastructures, comme cela a pu être le cas lors des bombardements
alliés à la fin de la Seconde Guerre mondiale,

ou

qu’une volonté de supprimer la population palestinienne de Gaza ne se
serait peut-être pas embarrassée d’ordres donnés à la population civile
de se rendre dans le sud de l’enclave,

**je vais passer pour un colon d’extrême droite**.


Quoi qu’il en soit, l’usage militant du terme « génocide » pour discréditer les actes de l’adversaire n’est pas neuf
========================================================================================================================

Quoi qu’il en soit, l’usage militant du terme « génocide » pour
discréditer les actes de l’adversaire n’est pas neuf.

Nétanyahou lui-même avait traité Yitzhak Rabin de nazi…

Quant au Hamas, c’est un mouvement islamiste ultranationaliste qui plonge
certaines de ses racines dans l’antisémitisme européen, mais ce ne sont
pas des nazis.

Il faut saluer la réaction de Dani Dayan
==============================================

- https://fr.wikipedia.org/wiki/Yad_Vashem

Mediapart

    Dani Dayan, le président de Yad Vashem, le mémorial israélien de la Shoah
    situé à Jérusalem, a sermonné l’ambassadeur d’Israël à l’ONU
    pour avoir accroché une étoile jaune sur sa poitrine.

    Cela montre-t-il une opposition entre une part d’Israël qui
    instrumentaliserait la mémoire de la Shoah pour justifier des crimes
    et une autre qui s’y refuse ?

Il faut saluer la réaction de Dani Dayan, d’autant qu’il est issu
des rangs du Likoud, c’est-à-dire de la droite israélienne.

Mais pourquoi s’indigner qu’Israël fasse usage de cette mémoire de la
Shoah ?


La Shoah fait partie de l’ADN du pays. C’est une plaie qui ne se refermera jamais
=====================================================================================

**La Shoah fait partie de l’ADN du pays. C’est une plaie qui ne se refermera jamais.**


Bien sûr, des crimes de guerre ne peuvent être exonérés au nom de
la Shoah, mais le fait qu’Israël possède une mémoire à vif de la
Shoah est logique et ancien.

En 1981, quand Israël a bombardé le réacteur nucléaire Osirak, vendu
à l’Irak par la France, le premier ministre d’alors, Menahem Begin,
a justifié l’opération en disant : « Il n’y aura plus jamais de
Shoah. »

Le fait de parler d’un « usage mémoriel » ou d’une «instrumentalisation »
de la mémoire est une question de positionnement politique et idéologique.

Pour les royalistes, la commémoration de la Révolution française par la
République est une « instrumentalisation ».


La condamnation des usages mémoriels liés à l’histoire nazie vaut pour tout le monde, et les mêmes exigences s’imposent à tous et pas seulement  à Israël
=============================================================================================================================================================

Je trouve, comme le président de Yad Vashem, que le geste de
l’ambassadeur d’Israël à l’ONU est déplorable et choquant.

Mais pourquoi est-ce qu’alors personne ne relève ni ne déplore que Jean-Luc
Mélenchon et plusieurs députés de La France insoumise portent à la
boutonnière un pin’s en forme de triangle rouge, symbole des déportés
politiques ?

Mediapart

    Parce que La France insoumise ne possède pas une armée puissante en
    train de bombarder Gaza ?

Évidemment. Mais Jean-Luc Mélenchon a été candidat à la présidence
et pourrait un jour disposer d’une armée puissante.

**La condamnation des usages mémoriels liés à l’histoire nazie vaut pour
tout le monde, et les mêmes exigences s’imposent à tous et pas seulement
à Israël**.


Qu’Israël fasse un lien entre la Shoah et ce qui s’est passé le 7 octobre 2023 est logique quand on connaît ne serait-ce qu’un peu cette société
===================================================================================================================================================

Mediapart

    Diriez-vous, comme l’historien Enzo Traverso dans nos colonnes,
    que ce qui se passe aujourd’hui à Gaza « brouille la mémoire de
    l’Holocauste » ?

**Pas du tout**.

Qu’Israël fasse un lien entre la Shoah et ce qui s’est passé le 7 octobre 2023
est logique quand on connaît ne serait-ce qu’un peu cette société.

Le rêve d’un territoire où les juifs n’auraient pas à craindre l’antisémitisme
a volé en éclats.


Les articles 22 et 32 de la charte du Hamas rendent les juifs responsables du communisme, de la Révolution française, de la Première Guerre mondiale
============================================================================================================================================================

Et ceux qui refusent d’inscrire le 7 octobre 2023 dans la trajectoire de
l’antisémitisme et de l’antijudaïsme pour en faire uniquement
un rouage de l’affrontement entre Israël et la Palestine évacuent
purement et simplement **les articles 22 et 32 de la charte du Hamas qui
rendent les juifs responsables du communisme, de la Révolution française,
de la Première Guerre mondiale**…

Le point 32 de cette charte fait explicitement référence au Protocole
des Sages de Sion. On ne peut donc pas faire comme si l’antisémitisme
européen n’avait pas imprégné une partie du monde arabe et musulman
et voir dans la guerre actuelle le seul affrontement de deux nationalismes
antagonistes.

Je ne vois pas non plus pourquoi Enzo Traverso qualifie Israël d’État
« le plus ethnocentrique et territorial que l’on puisse imaginer ».

Je n’ai aucune appétence pour le gouvernement Nétanyahou, mais cela me
semble politiquement et factuellement faux.

Que dire alors de la Russie de Poutine, de l’Inde de Modi, de la Hongrie
de Orbán ?

Le gouvernement israélien actuel est ultranationaliste, mais il est loin
d’être isolé sur la planète.

**Et parler d’État ethnocentrique pour Israël me paraît factuellement inexact
dans la mesure où sa population est composée de juifs du Moyen-Orient,
d’Afrique du Nord, d’Éthiopie ou encore de Druzes**…


Auschwitz a occupé une place centrale dans la constitution de l’antiracisme après la Seconde Guerre mondiale. Ce rôle est-il en train de s’effriter, voire s’est-il déjà désagrégé ?
=======================================================================================================================================================================================

Mediapart

    Auschwitz a occupé une place centrale dans la constitution de
    l’antiracisme après la Seconde Guerre mondiale.
    Ce rôle est-il en train de s’effriter, voire s’est-il déjà désagrégé ?

Cela fait un moment qu’il a vacillé.

L’antiracisme est historiquement lié à la lutte contre l’antisémitisme
depuis l’affaire Dreyfus.

La première organisation antiraciste française naît à la fin des années
1920 et s’intitule Ligue internationale contre l’antisémitisme (Lica).

À la fin des années 1970, elle devient Licra avec l’ajout du « R » de « racisme ».


Certains militants antiracistes, notamment communistes, ont très rapidement omis de leur champ l’antisémitisme
===============================================================================================================

- https://fr.wikipedia.org/wiki/Complot_des_blouses_blanches
- https://fr.wikipedia.org/wiki/Proc%C3%A8s_de_Prague

Mais certains militants antiracistes, **notamment communistes**, ont très
rapidement omis de leur champ l’antisémitisme, tandis que Staline menait
des politiques ouvertement antisémites, comme le prétendu complot des
blouses blanches, ou lors du procès de Prague, dont la quasi-totalité
des accusés étaient juifs.

Dans certaines manifestations, notamment aux
États-Unis, la lutte contre le racisme a évacué l’antisémitisme,
parce que les juifs étaient identifiés aux Blancs et aux dominants.


L’idée que l’antisémitisme c’était seulement l’extrême droite a permis d’invisibiliser l’antisémitisme à gauche
==================================================================================================================

**L’idée que l’antisémitisme c’était seulement l’extrême droite
a permis d’invisibiliser l’antisémitisme à gauche**.

Le racisme colonial n’était ni de droite ni de gauche.

Et c’est pareil pour l’antisémitisme.

**Il est structurant pour l’extrême droite, mais il se porte bien dans toute
une partie de la gauche et de l’extrême gauche**.


Peut-on juger que les réactions des différents pays occidentaux parrapport à ce qui se passe en ce moment à Gaza sont alignées sur les responsabilités historiques des pays dans la Shoah ?
===============================================================================================================================================================================================

Mediapart

    Peut-on juger que les réactions des différents pays occidentaux par
    rapport à ce qui se passe en ce moment à Gaza sont alignées sur les
    responsabilités historiques des pays dans la Shoah ?

Allemagne
------------

C’est évident pour l’Allemagne, dont le chancelier est le premier
à s’être rendu à Tel-Aviv après le 7 octobre 2023.

Royaume-Uni
-------------

Le Royaume-Uni a une autre histoire. Quand le prince Harry se déguise en
nazi, cela ne l’exonère pas, mais l’histoire n’est pas la même
qu’en France parce que l’Angleterre, contrairement à la France,
s’enorgueillit de n’avoir pas collaboré et d’avoir battu les nazis.

Le rapport du Royaume-Uni à ce qui se passe au Proche-Orient est encore
compliqué parce que c’est ce pays qui était en charge de la Palestine
mandataire.

La société et les institutions britanniques ne sont pas
comparables à la situation française et allemande.

En Grande-Bretagne, il existe une expression identitaire et communautaire
musulmane qui explique largement les manifestations massives qu’on voit
à Londres par rapport à Berlin ou Paris.

États-Unis
----------------

Les États-Unis sont, quant à eux, encore marqués **par une lourde
culpabilité de n’avoir pas accueilli les juifs qui fuyaient
l’extermination nazie**.

La France
-----------

La France est un cas particulier.

Elle a collaboré avec les nazis, mais c’est aussi le premier pays à avoir
accordé la citoyenneté aux juifs.

Elle a soutenu la création d’Israël mais a mené une politique pro-arabe
dans la région.

Toutes ces histoires créent des rapports hétérogènes aux événements qui
se déroulent en ce moment au Proche-Orient.


Le président de la République, dans une Lettre aux Français, a déploré « l’insupportable résurgence d’un antisémitisme débridé » : est-ce que ce sont les bons termes ?
======================================================================================================================================================================================

Mediapart

    Le président de la République, dans une Lettre aux Français, a déploré
    « l’insupportable résurgence d’un antisémitisme débridé » :
    est-ce que ce sont les bons termes ?

Oui, même si les sondages montrent que la grande majorité de la
population française réprouve l’antisémitisme et que celui-ci est
loin d’être généralisé.

**Mais il est indéniable que les antisémites sont aujourd’hui ragaillardis,
notamment à l’extrême droite et parmi les populations d’origine arabe
ou musulmane, où le conflit israélo-palestinien pèse**.


Les agressions semblent pouvoir venir de partout, et pas seulement d’actes terroristes
===========================================================================================

Nous sommes dans un moment antisémite, comme nous en avons connu
d’autres dans l’histoire. Mais celui-ci est inédit, parce qu’il
a rarement été aussi palpable dans l’atmosphère. Mille cinq cents
actes antisémites en quelques semaines, c’est énorme.

Il faut comprendre que la peur se diffuse, parce que nous sommes face à un
phénomène dynamique au sens physique du terme.

Au moment de l’attentat de Mohammed Merah ou même de l’Hyper Cacher, on
n’a pas vu cette peur du quotidien se répandre dans la communauté juive
comme elle se répand aujourd’hui, parce que **les agressions semblent
pouvoir venir de partout, et pas seulement d’actes terroristes**.
