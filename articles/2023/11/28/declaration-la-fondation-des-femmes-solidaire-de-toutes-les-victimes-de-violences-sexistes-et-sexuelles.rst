
.. _fondation_2023_11_28:

===============================================================================================================================
2023-11-28 **Déclaration : la Fondation des Femmes solidaire de toutes les victimes de violences sexistes et sexuelles**
===============================================================================================================================

Mesdames, Messieurs,

Chères amies et amis de la Fondation des Femmes,

Depuis plusieurs jours, la Fondation des Femmes est prise à partie et mise en
cause, notamment suite à la marche du 25 Novembre. Je vous écris aujourd’hui
pour éteindre toute forme de polémique et rappeler les positions exactes de
notre organisation.

Le 7 octobre 2023, nous avons été terrifiées par la violence des massacres
terroristes perpétrés par le Hamas dans le sud d’Israël. Dès le 13
octobre, j’ai pris la parole pour dénoncer sans ambiguïté cette attaque
terroriste et antisémite, ainsi que les crimes de viols commis contre les
femmes en temps de conflit, dans la chronique dont je dispose sur France Inter
chaque vendredi matin.

Le 10 novembre,  alors que des informations plus précises et circonstanciées
faisaient surface, la Fondation des Femmes, dont le périmètre d’action
est le territoire français, a pris à titre exceptionnel position par un
communiqué de presse et sur les réseaux sociaux pour dénoncer les violences
sexistes de masses et les crimes sexuels commis lors de l’attaque du 7 Octobre.

J’ai rappelé à nouveau le 10 novembre puis le 17 novembre dans mes
chroniques, que ces violences constituent des pratiques récurrentes lors
des conflits armés qui doivent être dénoncées pour leur caractère
spécifique. J’ai aussi redit notre attachement à la paix et notre engagement
aux côtés des féministes engagées depuis des années pour mettre fin
au conflit.

Enfin, nous avons également engagé notre soutien auprès de l'association
We Are Not Weapons Of War, spécialiste des viols de guerre, afin qu’elle
puisse se rendre sur place pour contribuer à établir les faits et rendre
ainsi justice aux victimes : parce que les femmes sont les premières victimes
des violences masculines et plus particulièrement lors des crises et dans les
conflits armés, partout dans le monde, parce que les viols de guerre selon
l’ONU constituent des crimes de guerre et des crimes contre l’humanité.

Depuis plusieurs jours, nous sommes interpellées quant au supposé silence
des féministes françaises et la Fondation des Femmes étant explicitement
nommée, je juge important de porter auprès de vous avec clarté ce qui a
été et demeure notre position :  nous ne nous sommes jamais tues et nous
serons toujours solidaires de toutes les victimes de violences sexistes ou
sexuelles d’où qu’elles viennent.

Ce week-end, nos bénévoles ont participé comme chaque année, à la grande
marche de lutte contre les violences faites aux femmes, et nous avons œuvré
pour que les organisatrices de cette marche puissent accueillir comme il se doit
les organisations plus particulièrement engagées pour dénoncer les crimes
sexuels du Hamas, afin qu’elles prennent la parole lors de la conférence de
presse et soient présentes à nos côtés lors de la manifestation. Malgré
cela, certaines femmes qui dénoncent les viols du Hamas ont été intimidées
par des manifestants à leur arrivée. Je le déplore et souhaite que la lumière
soit faite sur ce qui s’est passé exactement et que l’ensemble du mouvement
féministe soit en capacité de s’assurer à l’avenir qu’aucune femme
ne soit jamais intimidée lorsqu’elle dénonce des violences.

Cela ne doit pas occulter le fait que cette marche a été par ailleurs
un franc succès qui a rassemblé près de 100 000 personnes et témoigne
de l’engagement des Françaises et des Français pour en finir avec les
violences sexistes et tout particulièrement les féminicides.

Notre marche est une longue marche, elle ne souffre d’aucune ambiguïté et
appelle plus que jamais à votre mobilisation pour qu’aucune femme ne soit
victime de violence.

Merci pour votre confiance et votre soutien,

Bien à vous,

Anne-Cécile

Lien associé à la déclaration
================================

- https://www.francetvinfo.fr/replay-radio/le-vrai-du-faux/vrai-ou-faux-viols-perpetres-par-le-hamas-les-feministes-restent-elles-silencieuses_6181977.html
