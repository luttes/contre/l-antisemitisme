.. index::
   pair: SUD-education ; 2023-11-16

.. _sud_education_2023_11_16:

========================================================================================================
2023-11-16 SUD-education **L’antisémitisme nous regarde : combattons-le partout, tout le temps**
========================================================================================================


:download:`pdfs/communique_sud_education_2023_11_16.pdf`

Une explosion d’actes antisémites
========================================

Depuis le 7 octobre 2023, nous assistons en France et dans le monde à une
multiplication terrifiante d’actes antisémites : inscriptions taguées,
insultes, agressions et profanations.

Le 20 octobre 2023, à Paris, un homme a incendié la porte d’entrée d’un
couple d’octogénaires sur laquelle était accrochée une `mezouzah <https://fr.wikipedia.org/wiki/Mezouzah>`_.

Depuis, **nombreuses sont les personnes juives en France à avoir retiré
de leur porte ce symbole de protection**.

Dans l’Éducation nationale, des parent·es d’élèves conseillent à
leurs enfants de taire leur judéité et **font part aux personnels de leur
inquiétude que nos élèves de confession juive soient victimes de la haine
antisémite au sein même de l’école**.

À l’université également, les étudiant·es juif·ves alertent sur une montée
de l’antisémitisme, jusque dans les amphis, comme à Panthéon-Assas, où
un enseignant a fait un salut nazi en cours et un autre a été suspendu
pour avoir plaisanté sur les victimes du 7 octobre 2023.

Regarder l’antisémitisme en face
======================================

Dans ce déferlement de haine et la cacophonie médiatique qui en résulte,
avec son lot d’instrumentalisations et de récupérations, chacun·e
croit connaître et reconnaître les coupables, direct·es ou indirect·es,
mais personne ne regarde les victimes.

En tant qu’organisation antiraciste, ces actes nous révoltent et
doivent nous révolter. Nous ne devons ni fermer les yeux ni minimiser
leur gravité. L’antisémitisme en France n’est pas un phénomène
nouveau ni marginal, il fait partie de l’histoire française depuis
des siècles, il n’est pas apparu avec la création d’Israël.

Cette haine a ses principaux promoteurs à l’extrême-droite, mais il
est naïf et dangereux de ne pas reconnaître qu’elle traverse toute la
société, depuis des stéréotypes et des fantasmes conspirationnistes
largement répandus, jusque dans des explosions de violence qui ne
semblent jamais avoir de fin : la profanation du cimetière juif de
Carpentras en 1990, la torture et le meurtre d’Ilan Halimi en 2006,
le meurtre des trois élèves du collège Ozar Hatorah à Toulouse en
2012, la prise d’otages et l’exécution de quatre personnes juives
à l’Hyper Cacher de Vincennes en 2015, les meurtres de Sarah Halimi
en 2017 et de Mireille Knoll en 2018, etc.

L'antisémitisme prend aussi racine au plus haut sommet de l'État:
caricatures antisémites contre Macron par Les Républicains lors de la
présidentielle 2017, réhabilitation de Pétain et Maurras par Macron,
de Barrès par un député LR, propos antisémites de la part de Darmanin
dans un ouvrage. Ou encore Marine Le Pen qualifiant le macronisme comme un
« attalisme », une « philosophie du déracinement », un « nomadisme ».

Notre solidarité légitime avec le peuple palestinien dans sa lutte
pour sa décolonisation, notre lutte contre le racisme anti-Arabes et
l’islamophobie, notre lutte anticapitaliste : aucun de ces engagements ne
saurait justifier ou excuser de relayer et de propager des stéréotypes
judéophobes, de sous-estimer le sentiment de peur et de solitude des
personnes juives en France ces dernières semaines, ou encore de pratiquer
le même type d’amalgames que nous combattons ailleurs.

Ainsi, l’injonction implicite ou explicite faite à l’ensemble des
personnes juives à se désolidariser de la politique israélienne à
chaque résurgence de la guerre israélo-palestinienne est une assignation
identitaire du même ordre que celle qui vise les personnes musulmanes
après chaque attentat islamiste. Une assignation que le gouvernement
d’extrême-droite au pouvoir en Israël instrumentalise à son tour. Si
la deuxième nous indigne ; la première doit nous indigner aussi.

La haine des personnes juives, des personnes musulmanes ou supposées
musulmanes ont des mécanismes et des réalités communes, et d’autres
qui leurs sont propres. Mais nous devons les considérer toutes les deux
pour ce qu’elles sont : des idéologies racistes d’exclusion et de
domination. Et c’est notre devoir de les combattre avec la même force.

Entrer en résistance
=======================

Pour cela, nous devons résister de toutes nos forces au projet de la droite
et de l’extrême-droite qui consiste à isoler l’antisémitisme des
autres racismes, voire à les opposer, et en définitive à diviser les
opprimé·es pour mieux continuer leur oppression. Il faut non seulement
dénoncer sans relâche les instrumentalisations, les amalgames et les
récupérations, mais aussi démontrer notre solidarité pour toutes les
victimes de racisme.

N’abandonnons pas l’initiative du terrain et de la rue aux
organisations qui se revendiquent de « l’arc républicain », appellation
totalement vide, d’autant plus quand elle prétend y inclure des partis
d’extrême-droite héritiers de l’anti-dreyfusisme, du nazisme et
du pétainisme.

Refusons toute complaisance ou indulgence envers tous propos et actes
antisémites autour de nous, sur nos lieux de travail et dans les lieux
de militantisme.

Nous exprimons notre solidarité et notre soutien aux personnes juives
victimes de la haine antisémite.

**Nous le redisons : l’antisémitisme est incompatible avec la lutte
antiraciste et anticoloniale**.

Il est incompatible avec une paix juste et durable entre les Israélien·nes
et les Palestinien·nes.
