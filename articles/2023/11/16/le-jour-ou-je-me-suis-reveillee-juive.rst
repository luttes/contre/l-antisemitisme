.. index::
   pair: Fabienne Messica ; Le jour où je me suis réveillée juive (2023-11-16)

.. _messica_2023_11_16:
.. _intersec_2023_11_16:

=================================================================================================
2023-11-16 **Le jour où je me suis réveillée juive** par Fabienne Messica |FabienneMessica|
=================================================================================================

- :ref:`fabienne_messica`
- :ref:`intersectionnelles`

Introduction
==============

Là-bas : la guerre entre le Hamas et Israël est impitoyable.
---------------------------------------------------------------

**Ses victimes par milliers sont des civils**. Ses armes sont les civils
qu’il s’agit de terroriser, de tuer en masse alors que la guerre au
sens militaire, est vouée à l’échec.

Guerre de peur, fantasme de faire disparaître l’autre, cauchemar du
risque existentiel que deux peuples se renvoient en miroir.

Deshumanisation.


Et ici : guerre des mots, des symboles, confiscation de l’histoire
---------------------------------------------------------------------

Et ici : guerre des mots, des symboles, confiscation de l’histoire,
jouissance abjecte pour la mort.

Garder des repères : comment ?

Le jour où je me suis réveillée juive
===========================================

Le 7 octobre 2023, je ne savais rien.

Le 8 octobre 2023, confrontée à l’effroi d’une attaque meurtrière en Israël,
de crimes odieux et cruels commis dans les maisons et à une fête de jeunes,
je me suis sentie, peut-être pour la première fois, profondément juive.

Juive, pourquoi ?
----------------------

Juive, pourquoi ?  Parce que je suis de gauche, que j’attendais de mes
camarades de lutte dont je sais que majoritairement ils ne sont pas
antisémites, quelques mots **et non cette paralysie de l’empathie, cette
incapacité à se sentir solidaires de victimes innocentes**, comme si les
victimes étaient coupables d’être nées là et par extension, d’être
majoritairement juives.

Des **victimes coupables côté israéliens** et des **victimes forcément
bonnes côté palestinien** puisqu’ils sont les opprimés.

**Rien sur les faits**

**Aucune solidarité pour les femmes victimes de viols** par exemple.

Il y aurait de bonnes ou de mauvaises victimes, être victime serait une
essence alors que ce n’est qu’une situation, celle, dans cette séquence-là,
d’hommes, de femmes et d’enfants israéliens.

.. _nausee_npa:
.. _nausee_ujfp:
.. _nausee_solidaires:

Lisant les communiqués du **NPA de Solidaires, de l’UJFP** ..  **j’ai ressenti cette nausée**
================================================================================================

- :ref:`intersectionnelles`

Lisant les communiqués du NPA (Nouveau Parti Anticapitaliste), de Solidaires,
de l’UJFP (Union de juifs Français pour la Paix), constatant le silence
d’autres associations, groupes avec lesquels j’ai tant milité, **j’ai ressenti
cette nausée**, celle dont parlaient, chacun à leur manière, Jean-Paul Sartre
et Camus.


Vous méprisez les Palestiniens en les supposant tous et toutes en accord avec ces actes et en ôtant aux auteurs la responsabilité de leurs actes
==========================================================================================================================================================

A ceux qui soutiennent les actes du Hamas car « ils ont le droit de choisir leurs moyens de résister »,
je réponds : **non seulement vous méprisez la solidarité internationale qui
est un acte libre et non un suivisme**, mais **vous méprisez les Palestiniens
en les supposant tous et toutes en accord avec ces actes et en ôtant aux
auteurs la responsabilité de leurs actes**.

**Vous leur enlevez la liberté, celle qui reste à tout opprimé, celle-là
même que tout oppresseur veut leur confisquer**.

Puis plus tard le dégoût encore face au négationnisme, face à la négation
de la cruauté voire l’imputation des tueries à l’armée israélienne, un flot
de mensonges, de haine, d’antisémitisme.

Sans compter la fascination morbide pour ces actes, sans compter la réciproque
nazification par les Palestiniens des Juifs, par les Israéliens des
Palestiniens et ici, tous ces commentateurs et jusqu’à des humoristes
qui se vautrent dans ce langage odieux allant jusqu’à évoquer la
circoncision, ce signe qui permettait aux nazis de reconnaître des Juifs
quand ces derniers ne portaient pas l’étoile jaune.

Le 10 octobre 2023, je me suis sentie palestinienne
=======================================================

**Le 10 octobre 2023, je me suis sentie palestinienne**.

Les Palestiniens de Gaza ne sont pas coupables. Ils sont des civils.

Et puis, mais ça, c’est une autre histoire : je rêve depuis si longtemps
à la paix entre ces peuples qui se ressemblent tant et ont tant en commun,
dont les tragédies peuvent les rendre si cruels les uns envers les autres
et en même temps, pour lesquels il suffirait d’une éclaircie, d’un espoir,
**pour effacer la haine dans des cœurs pas encore aussi endurcis que
ceux dans mon pays, la France**.

Puis vint encore la flambée d’antisémitisme et la tentative de confiscation
de notre histoire, Juifs et Juives d’origines diverses, par des partis
racistes et antisémites.

Comment ne pas comprendre ? L’exil est au cœur de notre histoire dans
sa diversité et si nous sommes un peuple, c’est alors un peuple de
réfugiés.

Qui a voulu une maison. Aussi.

Qui ne peut se jeter dans les bras de l’extrême-droite, sans devenir
traître à lui-même.

Mais vous m’avez volé mes mots. Je n’en ai plus.
Mais vous n’avez rien vu à Hiroshima mon amour
