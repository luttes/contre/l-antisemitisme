.. index::
   pair: Robert Hirsch ; La gauche de la gauche a minoré l’antisémitisme (2023-11-16)

.. _robert_hirsch_2023_11_16:

=====================================================================================================
2023-11-16 **La gauche de la gauche a minoré l’antisémitisme** par Robert Hirsch |RobertHirsch|
=====================================================================================================

- :ref:`robert_hirsh`
- https://www.nouvelobs.com/idees/20231114.OBS80853/robert-hirsch-la-gauche-de-la-gauche-a-minore-l-antisemitisme.html (payant)


**Depuis l’affaire Dreyfus, la gauche a toujours lutté contre l’antisémitisme, depuis les années 2000, la frange radicale a abandonné ce combat**
==================================================================================================================================================

Depuis l’affaire Dreyfus, la gauche a toujours lutté contre
l’antisémitisme.

Mais depuis les années 2000, sa frange radicale a abandonné ce combat,
laissant le RN occuper le terrain.

L’historien Robert Hirsch, auteur de “la Gauche et les juifs”, déplore cette
inversion des rôles

Plus de 180 000 personnes dans toute la France, dont 105 000 à Paris,
selon le comptage de la police.

Depuis la marche qui avait suivi la profanation du cimetière juif de
Carpentras, en 1990, jamais une manifestation contre l’antisémitisme
n’avait réuni autant de monde que ce dimanche 12 novembre 2023.

Ce défilé, qui s’est déroulé dans le calme et la dignité, était bien
différent de tous les précédents.
Pour la première fois, l’extrême droite (sous sa double incarnation
Rassemblement national et Reconquête) y a participé, protégée par un cordon
de policiers.

C’est le cas notamment de Jean-Luc Mélenchon et de ses soutiens, aujourd’hui accusés de **fermer les yeux sur l’antisémitisme**
--------------------------------------------------------------------------------------------------------------------------------

Et pour cette raison, une partie de la gauche a refusé d’y prendre part.

**C’est le cas notamment de Jean-Luc Mélenchon et de ses soutiens, aujourd’hui
accusés de fermer les yeux sur l’antisémitisme** et d’accentuer les
fractures dans la Nupes.


Le chassé- croisé entre  le RN et La France insoumise (LFI) sur cette question marque un tournant  politique majeur
----------------------------------------------------------------------------------------------------------------------

Pour l’historien Robert Hirsch, auteur de :ref:`la Gauche et les juifs <la_gauche_et_les_juifs>`
|GaucheEtJuives| (éd. Le Bord de l’Eau, 234 p.), le chassé- croisé entre
le RN et La France insoumise (LFI) sur cette question marque un tournant
politique majeur.

**Historiquement, les manifestations contre l’antisémitisme étaient dirigées contre l’extrême droite**
=========================================================================================================

**Historiquement, les manifestations contre l’antisémitisme étaient dirigées
contre l’extrême droite.
La défense des juifs allait de pair avec la défense de la République.
La manifestation du 12 novembre 2023, marquée par la participation du RN
et de Reconquête et par l’absence de LFI, signe-t-elle la fin de cette
tradition ?**

On est face à un risque d’inversion assez dramatique, avec une extrême
droite défendant les juifs, et une gauche radicale devenant indifférente
à leur sort.


Cette situation est le fruit de deux phénomènes
==================================================

**Cette situation est le fruit de deux phénomènes.**


Le premier, c’est la stratégie de dédiabolisation du Front national (FN) devenu Rassemblement national
----------------------------------------------------------------------------------------------------------

Le premier, c’est la stratégie de dédiabolisation du Front national (FN)
devenu Rassemblement national. Jamais Jean-Marie Le Pen n’aurait participé
à une telle manifestation.
Mais dès qu’elle est arrivée à la tête du FN en 2011, Marine Le Pen a
décidé que son parti devait changer radicalement sur la question de
l’antisémitisme.

Il fallait une rupture nette avec les ambiguïtés distillées par son père :
son jeu de mots « Durafour crématoire », sa remarque sur le « détail »
de l’histoire que serait l’utilisation de chambres à gaz…

Pour elle, c’était un plafond de verre à briser pour espérer conquérir
le pouvoir. Elle a cherché, sans succès, à avoir des contacts avec le
Conseil représentatif des Institutions juives de France (Crif ) ou des
responsables israéliens.

Certes, toutes les enquêtes d’opinion montrent que c’est encore à
l’extrême droite que l’on rencontre le plus de préjugés à l’égard des
juifs.
**Mais la direction du parti, qui continue de jouer la carte du racisme,
ne cible désormais plus que les musulmans**.

Pour ce qui est de Zemmour, son cas est ambigu.

Il évoque ses origines juives et défend Israël, **mais il s’est signalé par sa
réhabilitation de Pétain, ce qui est historiquement, pour les juifs, un
marqueur d’antisémitisme déterminant**.


L’autre phénomène, c’est l’évolution de la gauche
-------------------------------------------------------

L’autre phénomène, c’est l’évolution de la gauche.

**Pendant longtemps, elle tenait une position équilibrée, avec un soutien
nuancé aux Palestiniens d’un côté, et la lutte résolue contre l’antisémitisme
de l’autre**.

**Jean-Luc Mélenchon, dans sa volonté de faire le buzz, brouille cette image**.

Après le 7 octobre 2023, **sa réaction a fait preuve d’une absence totale
d’empathie à l’égard des juifs israéliens, mais de fait aussi à l’égard
des juifs en général**, qui ont été très meurtris par le caractère
« exterminatoire » de cette attaque.


.. _melenchon_2023_11_16:

**Mélenchon n’a pas saisi qu’il s’agissait du plus grand massacre de juifs depuis la Seconde Guerre mondiale**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- :ref:`melenchon`

Mélenchon n’a pas saisi qu’il s’agissait du plus grand massacre de juifs
depuis la Seconde Guerre mondiale.

Puis le tweet dans lequel il accusait la manifestation à venir d’être
le rendez-vous des « amis du soutien inconditionnel au massacre [des Gazaouis, NDLR] »
a également soulevé un tollé.

**Il faut bien sûr rester lucide sur la part d’instrumentalisation politique
par la droite ou les macroniens** des ambiguïtés de Mélenchon ou de ses amis.

Mais il ne faut pas non plus être dans le déni: il y a bien aujourd’hui
un problème dans la partie radicale de la gauche.

**Pour Mélenchon, l’antisémitisme n’est qu’une invention de la droite afin
d’affaiblir la gauche : il appelle la dénonciation de l’antisémitisme
le « rayon paralysant ».**

Quelles sont les conséquences politiques du croisement de ces deux phénomènes ?
==================================================================================

Marine Le Pen, sans dire ni faire grand-chose, a encore marqué des points
pour sa conquête du pouvoir en 2027.

Selon un récent sondage Ifop, à la question de savoir qui est le mieux
placé pour lutter contre l’antisémitisme, 42 % répondent Marine Le Pen
contre 17 % pour Jean-Luc Mélenchon !

**C’est une inversion totale**.

Plus généralement, force est de constater que depuis quelques années,
ce n’est plus la gauche qui fait des gestes en direction des juifs,
mais la droite : je pense au discours sur la rafle du Vél’ d’Hiv
de Chirac ; c’est Mitterrand qui aurait dû le faire.

Mais aussi aux signaux envoyés par Sarkozy – même si ceux-ci avaient des
arrière-pensées politiciennes, et **jouaient sur le rejet des musulmans**.


**Cette situation est délétère pour la gauche entière**
========================================================

**Cette situation est délétère pour la gauche entière**.

Et les positions de LFI affaiblissent le reste de la Nupes, qui est
déjà en difficulté pour d’autres raisons.

Dans le défilé du 12 novembre 2023, on a vu Olivier Faure se faire prendre
à partie par un manifestant traitant de manière scandaleuse la Nupes
de « collabo ».

Le lien entre les juifs et la gauche, qui se distend depuis quelques années,
ne peut qu’être affecté davantage.

**A quand remonte ce lien ?**
===============================

La gauche a toujours été en pointe dans la lutte contre l’antisémitisme,
que ce soit sous la Révolution, au moment de l’affaire Dreyfus, ou pendant
la Seconde Guerre mondiale…
Cela n’allait pas forcément de soi : à chaque fois, il y a eu des hésitations,
mais c’est le choix de la lutte contre l’antisémitisme qui l’a toujours emporté.

Prenez l’affaire Dreyfus, le cas le plus net : une partie importante de la
gauche – derrière Jules Guesde – considérait que la défense du capitaine,
un bourgeois, ne concernait pas le mouvement ouvrier.
Même Jaurès a hésité avant de devenir un ardent dreyfusard et d’écrire en 1898
un texte magnifique en défense de la vérité.
La gauche s’est rangée aux côtés des juifs, qui l’ont reconnu en votant
très massivement pour elle.

Mais les hésitations ont continué, notamment pendant la Seconde Guerre
mondiale.

**En octobre 1940, le statut des juifs ne soulève pas une réaction très importante à gauche**
================================================================================================

**En octobre 1940, le statut des juifs ne soulève pas une réaction très
importante à gauche**.

**Les premiers textes de la Résistance insistent sur les problèmes de
ravitaillement, mais pas sur le sort des juifs**.

La réaction à l’antisémitisme ne s’exprimera qu’à partir de 1942 et de
la rafle du Vél’d’Hiv.

Avant d’être à nouveau relativisée en 1943-1944, moment où on emploie le
terme « déportation » pour… le STO.

Y avait-il un antisémitisme spécifique à la gauche, le juif étant assimilé au capital dans un poncif antisémite bien connu ?
================================================================================================================================

Au xixe siècle, un courant de la gauche amalgamait effectivement l’antisémitisme à
l’anticapitalisme.

Un certain nombre de penseurs socialistes étaient très véhéments.

Des gens comme Alphonse Toussenel ou **même Proudhon, avec son fameux
« Il faut renvoyer cette race en Asie, ou l’exterminer»**.

**Aux yeux de ces socialistes, les juifs étaient des privilégiés, ce qui
est très simpliste car à l’époque une bonne partie d’entre eux étaient
extrêmement pauvres**.

C’est l’antisémitisme que les penseurs allemands Friedrich Engels ou  August Bebel vont appeler « le socialisme des imbéciles »
----------------------------------------------------------------------------------------------------------------------------------

**C’est l’antisémitisme que les penseurs allemands Friedrich Engels ou
August Bebel vont appeler « le socialisme des imbéciles »**.

L’affaire Dreyfus constitue une rupture, mais l’idée du rapport prétendument
particulier des juifs à l’argent a perduré.

Après la Seconde Guerre mondiale et la Shoah, ces sentiments antisémites
seront refoulés.

Pour vous, un tournant a eu lieu dans les années 2000…
==========================================================

Lors de la seconde Intifada, l’antisémitisme s’exprime sous diverses formes,
à commencer par des attaques de synagogues.

**Il ne s’agit pas à mon sens d’un « nouvel antisémitisme »**, comme on a
pu le dire alors, mais d’un antisémitisme qui se développe dans des
milieux nouveaux : les banlieues.

J’ai pu le constater, étant enseignant en Seine-Saint-Denis.
Certains jeunes de familles arabo-musulmanes se sont laissé prendre.
La question de la Palestine était en toile de fond, mais pas seulement.

Ces élèves disaient : les juifs ont réussi, ils sont dans la finance,
les médias, la politique.
Il y a alors eu un grand débat sur la résurgence de l’antisémitisme.

Et la partie la plus radicale de la gauche a clairement sous-estimé l’importance du phénomène
--------------------------------------------------------------------------------------------------

**Et la partie la plus radicale de la gauche a clairement sous-estimé
l’importance du phénomène**.

Deux raisons l’expliquent.

- La première, c’est que cet antisémitisme venait de populations qui étaient
  elles-mêmes discriminées et victimes du racisme.
- La seconde, c’est la question du conflit israélo- palestinien, qui
  compliquait, à la gauche de la gauche, la perception de l’antisémitisme.

Les manifestations liées à celui-ci étaient en effet souvent organisées
par le Crif, très favorable à l’Etat d’Israël.

Une partie de cette gauche a donc préféré ne pas y participer.

En 2006, après l’assassinat antisémite d’Ilan Halimi, le Mrap ne participe
pas à la manifestation, ce qui est extraordinaire : le Mrap est historiquement
le « Mouvement contre le Racisme, l’Antisémitisme et pour la Paix »

[nom changé en 1977 pour « Mouvement contre le Racisme et pour l’Amitié
entre les peuples »].

La Ligue communiste révolutionnaire [LCR, trotskiste], qui était elle
aussi très en pointe dans la lutte contre l’antisémitisme dans les années
1970 et 1980, se retire également.


.. _intellos_2023_11_16:

Il y a là une vraie rupture certains propos tenus par des intellectuels liés à la gauche ont également joué un rôle non négligeable
============================================================================================================================================

- :ref:`intellos_2023_04_24`


**Il y a là une vraie rupture**

Certains propos tenus par des intellectuels liés à la gauche ont également
joué un rôle non négligeable.

Je pense à ceux d’Alain Badiou ou d’Edgar Morin, qui relativisent alors
l’antisémitisme, voire le nient.

**Edgar Morin parle en 2005 **antisémitisme imaginaire**.

**La gauche de la gauche n’a pas l’habitude de minorer les problèmes (le racisme,  la pauvreté…), mais, sur l’antisémitisme, elle l’a fait, ce qui a ouvert  la voie aux attaques de la droite**
====================================================================================================================================================================================================

**La gauche de la gauche n’a pas l’habitude de minorer les problèmes
(le racisme, la pauvreté…), mais, sur l’antisémitisme, elle l’a fait,
ce qui a ouvert la voie aux attaques de la droite**.

D’un autre côté, à partir de cette époque, quiconque critiquait Israël
risquait d’être traité d’antisémite…
Oui, il faut aussi rompre avec cette attitude-là.

**On peut critiquer Israël sans être antisémite, dénoncer le gouvernement
israélien, dont la politique est inexcusable**.


**Si cela signifie vouloir la  fin de l’Etat d’Israël, on touche effectivement à l’antisémitisme**
====================================================================================================

On peut être antisioniste sans être antisémite, à condition de bien
définir ce qu’on appelle l’antisionisme : **si cela signifie vouloir la
fin de l’Etat d’Israël, on touche effectivement à l’antisémitisme**.

L’antisionisme est présenté par ceux qui le défendent comme une variante de l’anticolonialisme.  Ce qui implique une volonté de voir partir les « colons »…
================================================================================================================================================================

Il y a des aspects colonialistes dans la politique d’Israël – les Palestiniens,
et c’est bien normal, le ressentent ainsi.

Mais la construction de l’Etat d’Israël ne relève pas d’un colonialisme
classique. Elle est liée à l’histoire des drames du xxe siècle, et de
ce qu’ils ont représenté pour les juifs.

**Aujourd’hui, cet Etat existe, c’est un fait acquis**
======================================================

**Aujourd’hui, cet Etat existe, c’est un fait acquis.**

Dans la conclusion de mon livre, je le formule ainsi : « L’histoire a
donné le droit aux juifs de refuser la destruction de l’Etat d’Israël.

Elle ne leur a pas donné le droit d’accepter l’oppression des Palestiniens.»

La gauche et les juifs devraient s’entendre sur ce point de vue.

**Vous reprochez à une partie de la gauche de trop fermer les yeux sur l’antisémitisme, mais allez- vous jusqu’à penser qu’il existe un antisémitisme de gauche ?**
=====================================================================================================================================================================

L’antisémitisme ne peut en aucun cas être une vision « de gauche ».

Mais il peut exister dans les marges de la gauche.

Les manifestations de « gilets jaunes », que soutenait une partie de la
gauche, ont été émaillées d’expressions antisémites.

Au départ, les manifestations portaient sur le prix de l’essence… et on
en est venu à dénoncer le sionisme !

Sur les ronds-points, on a vu fleurir des pancartes « Macron = Rothschild».

Si Macron avait travaillé à la Société générale, y aurait-il eu de telles
pancartes ?

Mais les « gilets jaunes », ce n’est pas la gauche…
=======================================================

Certes, mais ceux qui brandissaient ces pancartes n’étaient pas forcément
des gens d’extrême droite.

**Et une partie de la gauche a nié le problème.**

La même qui avait minimisé l’antisémitisme dans les banlieues vingt ans
plus tôt.

Personne à gauche ne tient des propos antisémites comme Jean-Marie Le Pen  pouvait le faire dans les années 1980…
======================================================================================================================

C’est vrai, **mais on assiste à des dérapages**.

.. _melenchon_2023_11_16_derapages:

**Dérapages de de Jean-Luc Mélenchon**
------------------------------------------

Et **notamment à ceux de Jean-Luc Mélenchon lui-même**.

En 2013, il affirme que Pierre Moscovici, ministre de l’Economie,
« ne pense plus français » mais « dans la langue de la finance internationale ».

Puis lorsque Macron commémore le Vél’ d’Hiv, en 2017, en s’inscrivant
dans la logique de la déclaration de Chirac de 1995, il le critique en
expliquant que la France n’a rien à se reprocher quant au sort des juifs
pendant la guerre.

Ensuite, il défend Jeremy Corbyn [l’ancien leader travailliste britannique
qui a étouffé des scandales liés à de l’antisémitisme dans son parti].

Il va alors jusqu’à dénoncer dans une même phrase le « capitalisme vert
et génuflexion devant les oukazes arrogants des communautaristes du Crif ».

En 2020, en réponse à une question bizarre sur BFMTV sur le Christ, il
reprend le vieux poncif du peuple déicide en disant « ce sont ses propres
compatriotes [qui l’ont mis] sur la croix».

L’année suivante, en 2021, il exonère Zemmour de l’accusation d’antisémitisme
au motif que ce dernier « reproduit de nombreux scénarios culturels »
liés au judaïsme.

Quels sont ces traits culturels ?
-----------------------------------

« On ne change rien à la tradition, on ne bouge pas.

La créolisation, mon Dieu, quelle horreur », résume-t-il.

Et il conclut : « Tout ça, ce sont des traditions qui sont beaucoup liées
au judaïsme.
Ça a des mérites, ça lui a permis de survivre dans l’histoire. »


.. _melenchon_2023_11_16_negatif:

**Je ne dis pas qu’il est antisémite, mais tout cela donne quand même une  impression très négative**
========================================================================================================

- :ref:`melenchon`

**Je ne dis pas qu’il est antisémite, mais tout cela donne quand même une
impression très négative**.

Et sur les réseaux sociaux, cela ne peut qu’attiser des polémiques malsaines.

.. _melenchon_2023_11_16_juifs_complices:

Les manifestants du 12 novembre 2023 sont les complices de ceux qui assassinent à Gaza
----------------------------------------------------------------------------------------------

**Il faut ajouter à la liste ses récentes affirmations**,  celle désignant
à l’avance les manifestants du 12 novembre 2023 comme **les complices de
ceux qui assassinent à Gaza ; or, parmi les manifestants il y a aussi les juifs**.
