.. index::
   pair: Tal Bruttmann ; 2023-11-16

.. _bruttmann_2023_11_16:

=============================================================================
2023-11-16 **L’antisémitisme est un impensé à gauche** par Tal Bruttmann
=============================================================================

- :ref:`tal_bruttmann`
- https://regards.fr/lantisemitisme-est-un-impense-a-gauche/
- https://www.youtube.com/watch?v=iEOelvaIqsM

.. figure:: images/tal.png

   https://youtu.be/iEOelvaIqsM


Bio
=====

- :ref:`tal_bruttmann`

Tal Bruttmann, historien spécialiste de l'antisémitisme et de la Shoah,
est l'invité de #LaMidinale.

Transcription
===============

- https://youtu.be/iEOelvaIqsM

Bonjour Tal

	bonjour

merci d'avoir accepté mon invitation

Tal tu es historien spécialiste de l'antisémitisme et de la Shoah en particulier
si je t'ai invité c'est justement pour parler d'antisémitisme on en parle
beaucoup notamment depuis le le 7 octobre puisqu'on a vu une recrudescence
très importante des des actes antisémites en France il y en a eu juste
pour donner un ordre de de grandeur pour ceux qui n'auraient pas suivi
mais en gros depuis 7 octobre donc en un mois il y a eu près de trois
fois le nombre d'actes antisémites qu'il y avait eu en un an l'année
passée

j'ai d'abord une question il y a eu cette grande marche qui
a été initiée par la présidente de l'Assemblée nationale il a Bron
Pivet le président du Sénat Gérard Larcher dimanche dernier  on
a beaucoup parlé de cette marche

mais finalement je trouve qu'on a assez peu parlé de lutte contre l'antisémitisme


Ca veut dire quoi la lutte  contre l'antisémitisme
=====================================================

- https://youtu.be/iEOelvaIqsM?t=60

en vérité qu'est-ce que qu'est-ce que d'après ça veut dire quoi la lutte
contre l'antisémitisme
qu'est-ce que qu'est-ce que ça veut dire est-ce
que ça veut vas-y non mais réponds-moi plutôt je vais pas spécifier ma question


- https://youtu.be/iEOelvaIqsM?t=78

non c'est une excellente question et il va falloir 3 jours pour y répondre donc il
va falloir contraindre un peu le format mais déjà enfin la remarque elle
est hyper importante c'est que initialement et on va laisser de côté les
débats politicar et j'utilise politicar à dessin il y a un appel qui est
lancé en réaction à ce qui est en train de se passer

peu importe enfin
président de l'Assemblée nationale et président du Sénat c'est des gens
qui sont institutionnels c'est des élus il y a cet appel qui est lancé

et ensuite on a eu toute une série de polémiques politiciennes autour
de ça avant et après

et l'enjeu principal c'est celui qui inquiète
les Français quand on regarde les sondages il y a quelque chose qui est
très clair déjà les 2/3 des Français condamnent ce qui est en train
de se passer en terme de recrudesence de l'antisémitisme donc ça dit
beaucoup de choses déjà la France n'est pas majoritairement antisémite
c'est important même si la recrudesence est importante

ensuite il y a
eu cette mobilisation qui était une réussite parce que si on compare aux
mobilisations contre l'antisémitisme depuis qu'il y en a c'est-à-dire 1980
et l'attentat de la contre la synagogue de la rue copernique on a une dizaine
de mobilisations il en a eu deux très importantes historiquement 1980
puis 1990 la profanation du cimetière juif à Carpentras qui ont rassemblé
200000 personnes à chaque fois

et ensuite depuis le début des années
2000 à chaque violence antisémite puis au meurtre puis aux assassinats
quand je dis meurtre c'est Ilan Halimi puis ensuite les assassinats à l'école
juif de Toulouse on a des mobilisations beaucoup moins importantes en
fait il y a pas de mobilisation c'est 20 30 40000 personnes


ce qui s'est passé dimanche dernier est à l'échelle de ce qui s'est passé en 80 en 90
=====================================================================================================

- https://youtu.be/iEOelvaIqsM?t=170
- https://fr.wikipedia.org/wiki/Attentat_de_la_rue_Copernic
- https://fr.wikipedia.org/wiki/Affaire_de_la_profanation_du_cimeti%C3%A8re_juif_de_Carpentras


donc le
premier constat c'est que ce qui s'est passé dimanche dernier est à
l'échelle de ce qui s'est passé en 81 en 90 autrement dit il y a
une mobilisation c'est une réalité ça c'est le premier constat

donc des gens ont voulu descendre dans la rue quelques ce soi les querelles
politiciennes pour dire non on est contre l'antisémitisme ça c'est un
constat froid factuel

Ensuite ta question elle est vertigineuse parce que qu'est-ce que ça veut  dire lutter contre l'antisémitisme
=================================================================================================================

- https://youtu.be/iEOelvaIqsM?t=192

ensuite ta question elle est vertigineuse parce que qu'est-ce que ça veut
dire lutter contre l'antisémitisme

c'est un problème qui se pose en France depuis 1945 qui est le moment en gros où
la République a décidé qu'il fallait lutter contre l'antisémitisme et
c'est là où il faut déjà faire plusieurs pas en arrière je suis désolé
si je vais digresser mais

d'abord il faut rappeler que l'antisémitisme et
le racisme jusqu'à la seconde gure mondiale sont pensés comme étant des
opinions comme des autres j'ai le droit d'être raciste et de dire que
j'aime pas telle ou telle catégorie j'ai le droit de détester les Juifs
et c'est mon droit j'ai le droit de l'exprimer j'ai le droit de d'en faire
une logorée de le vomir à la presse à la radio


il y a la Seconde Guerre mondiale et il y a la Shoah et ça disqualifie l'antisémitisme
=======================================================================================

- https://youtu.be/iEOelvaIqsM?t=241

il y a la Seconde Guerre
mondiale et il y a la Shoah et ça disqualifie l'antisémitisme et dans nos
société occidentale l'antisémitisme a été disqualifié

ce qui fait ce qui veut alors on a cru le 'on' etant très global que l'antisémitisme
avait été éliminé qu'il avait disparu ce qui était une erreur pour
plein de raisons il a été marginalisé c'est à dire qu'il avait plus
voix au chapitre publiquement à part de temps en temps où il y avait
une résurgence qui était perceptible mais il avait été marginalisé
jusqu'à la fin du 20e siècle


- https://youtu.be/iEOelvaIqsM?t=270

ça c'est un élément fondamental depuis 45 la République combat l'antisémitismer
et le racisme après on peut juger que c'est pas suffisant qu'il y a des lacunes
qu'il y a des impensées par exemple la question coloniale demeure un angle mort fondamental en France
jusqu'à aujourd'hui

donc c'est des réalités mais en même temps on peut
pas accuser la République de ne pas vouloir combattre ces deux choses
antisémitisme et racisme

et dans le même temps on peut se rendre compte
que depuis le début des années 2000 on est face à une résurgence et
c'est là où il y a énormément de choses qui se jouent d'abord cette
résurgence on a dit il y a une recrudescence de l'antisémitisme
c'est pas tant qu'il y a une recrudescence de l'antisémitisme qu'il y a
une revisibilisation de l'antisémitisme qui n'est pas la même chose

- https://youtu.be/iEOelvaIqsM?t=305

les antisémites qui étaient isolés jusqu'à la fin du 20e siècle et
que quand on gueulait sa haine du Juif au comptoir en buvant un petit blanc la
plupart des gens disaient au au type qui faisait ça ferme ta gueule
sauf qu'à partir de la fin des années 90 avec Internet puis ensuite
les réseaux sociaux le type qui était isolé au comptoir d'un coup
il se rend compte qu'il y a un autre type qui est pas qui est d'accord
et d'autres un peu partout dans le monde et il y a eu une résurgence
une re-revendication de l'antisémitisme sur le devant
de la scène


Ce phénomène il s'est accompagné par exemple avec des gens qui n'étaient plus sur les réseaux mais qui étaient sur la face publique comme Dieudonné
=======================================================================================================================================================

- https://youtu.be/iEOelvaIqsM?t=340

ce phénomène il s'est accompagné par exemple avec des
gens qui n'étaient plus sur les réseaux mais qui étaient sur la face
publique comme Dieudonné qui en a fait un fond de commerce et donc
là ça ça c'est vraiment démultiplié et dans le même temps il y a
eu une montée des actes des agressions puis ensuite le meurtre d'Ilan Halimi
puis les assassinats qui font que l'antisémitisme est devenu de plus
en plus virulent et de façon visible

et dans le même temps et je vais
m'arrêter là-dessus pour que laisser la place à des questions mais dans
le même temps sur les 20 années qui viennent de s'écouler on
a vu plusieurs phases de dénégation au début quand l'antisémitisme
a commencé à se manifester il y a énormément de gens je parle au
niveau politique qui ont dit mais non c'est pas ça c'est puis quand
c'est devenu de plus en plus évident c'est on a continué à à avoir des
dénégations sur certains domaines et cetera et cetera et en fait c'est


C'est depuis le le 8 octobre 2023 où il y a une vraie prise de conscience
==================================================================================

- https://youtu.be/iEOelvaIqsM?t=390

depuis le le 8 octobre 2023 où il y a une vraie prise de conscience je pense
mais là il va falloir voir dans les semaines et les mois qui viennent
que il y a un vrai problème en matière d'antisémitisme et quand je dis
un vrai problème je l'ai dit avant les 2/3 des Français sont contre
l'antisémitisme

donc le problème il concerne une partie résiduelle
importante mais qui est amplifiée par des déclarations politiques
qui elles sont quasiment pas contredites et c'est là aussi où on voit
qu'il y a des choses qui se jouent c'est quelle va être la réaction à
la fois du personnel politique qui est quand même pas une chose gagnée
parce que **le personnel politique en ce moment enfin depuis quelques années
quand même pas le plus brillant** qu'on est connu donc quelle va être
la réaction du personnel politique et quelle va être la réaction de
la société face à ces déclarations qui alimentent la dynamique qu'on
voit se développer au fil des mois et des années

Regards.fr

   - https://youtu.be/iEOelvaIqsM?t=453

   alors j'ai j'ai deux petites questions je repose ma question tout à l'heure
   sur la question de comment est-ce qu'on lutte contre l'antisémitisme
   parce que tu me l'as pas tu as pas vraiment répondu mais d'abord
   j'ai une autre question en fait que j'aurais même pu poser en première question
   là tu as cité beaucoup d'actes beaucoup de façons finalement
   d'être antisémite ça va de l'antisémitisme comme tu as dit au comptoir
   du bar

   moi je ça me fait penser au au livre qui a écrit Chloé Cormand
   qui s'appelle tu ressembles à une juive où elle parlait en fait de l'antisémitisme
   dans les salons bourgeois ou sous prétexte qu'elle avait des cheveux
   comme ci ou des cheveux comme ça on l'a on lui disait tu
   dois être une juif donc qu'est-ce que tu penses du conflit israël-palestinien
   et que elle considérait que c'était un un début d'antisémitisme et
   d'un autre côté ben les assassinats il y en a eu plus d'une dizaine
   en 20 ans ou les les actes physiques des agressions physiques ou les
   profanations de stèles  et cetera

   est-ce que toi tu crées un continuum
   en fait entre tous ces actes antisémites qui soient les plus
   bénins enfin les moins agressifs on va dire jusque au plus violents et
   aux plus meurtriers


- https://youtu.be/iEOelvaIqsM?t=520

oui alors pas un continuum en disant c'est pas parce
que tu as dit sale Juif que tu vas tuer un juif


Il y a énormément de problèmes en terme d'analyse
=======================================================

ce continuum c'est  une absurdité

mais en fait la question elle renvoie à quelque chose
de très important particulièrement à la France qui permet de voir
qu'il y a énormément de problèmes en terme d'analyse

celui qui est  célébré par certains je j'insiste sur "certains" comme le plus grand
écrivain de langue française c'est Louis Ferdinand Céline une des pires
crapules antisémites et quand on regarde des commentaires c'est oui bon ok
il était antisémite mais c'est le plus grand

et il y a une disjonction entre le
fait qu'il y ait des auteurs des écrivains dans les années 30 qui ont
pavé la voix qui ont pondu des textes enfin une logoré antisémite et
ceux qui tuent des Juifs

c'est-à-dire il y a les méchants antisémites
qui tuent des Juifs et puis il y a les intellectuels qui mais voilà

non il
y a un continuum parce que ceux qui tuent ils sortent pas de nulle part ils
s'inspirent de textes qui ont été pondus par les soi-disant intellectuels

donc on peut pas dissocier ça veut pas dire que c'est les mêmes formes
d'antisémitisme il y a différentes formes d'antisémitisme il y a plein
de formes d'antisémitisme  qui se recoupent ou qui se recoupent pas

il y a plein de façons de le manifester mais à partir du moment où on commence
à tolérer une de ces formes d'antisémitisme alors on on laisse
proliférer le reste etça ça ça vaut pour le racisme aussi c'est
il y a des gens qui sont racistes et qui vont jamais tuer personne mais
parce qu'il y a des gens racistes alors d'autres qui ont envie de passer
à l'acte vont passer à l'acte en se disant finalement il y a plein de
gens qui sont d'accord avec moi donc à partir du moment où on commence
à tolérer et je te rends la parole mais ça va permettre de répondre
à la question que tu m'as posé à laquelle j'ai pas répondu


**Comment on lutte contre l'antisémitisme  c'est en étant intransigeant**
===============================================================================

- https://youtu.be/iEOelvaIqsM?t=623

**Comment on lutte contre l'antisémitisme déjà c'est en étant intransigeant** ce qui
a mais avant de vraiment te laisser la parole il y a quand même un point
si moi je réussis à te répondre comment lutte contre l'antisémitisme
j'ai le prix Nobel demain

parce que c'est une question que tout le monde
se pose on n'a pas la réponse jusqu'à aujourd'hui il y a des choses qui
ont été faites et qui continuent à se faire qui fonctionnent il y en a
d'autres qui fonctionnent pas on a un moment où il faut réfléchir à ça
au sens propre du terme

il faut vraiment comprendre qu'on est à un moment charnière

je parle pas de créer un comité Théodule mais
d'avoir une vraie réflexion là-dessus et ce que toi les gens autour de
toi étent en train de faire ça participe de cette réflexion mais il
faut vraiment comprendre qu'on est à un moment charnière

Regards.fr

	ok tu as donné une
	interview il y a quelques jours il y a une semaine je crois dans dans le
	monde  sur  la quelque chose dont dont qui est important en fait
	on va dire dans l'éducation à la lutte contre l'antisémitisme c'est le
	film Shoah de Claude lanzman qui est un film qui dure presque 10h enfin un
	peu plus de 9h  qui est qui est un peu complexe quand même d'approche
	et ça m'a fait un peu sourire une des premières questions du journaliste
	du monde c'est mais est-ce que vous pensez vraiment que c'est avec Shoah
	de Claude lanzman qu'on va réussir à lutter contre l'antisémitisme je
	mets ça en parallèle avec ce qu'a dit Gérard Miller le  psychanalyste
	qui est qui qui est assez proche de la France insoumise enfin plus trop
	maintenant

	mais enfin jusqu'au coup

	voilà exactement et Gérard
	disait quelque chose d'intéressant il disait par rapport à la Shoah il
	disait ce qui est absolument terrible c'est que les hommes n'apprennent
	pas de l'horreur c'est à-dire que l'horreur n'est pas pédagogique on
	a beau leur dire regardez ce qui s'est passé en 1945 regardez mais
	même ça vaut pour énormément de champ de la société regardez ce
	qui s'est passé c'est pas pour ça que ensuite l'être humain va être
	moins voilà va être meilleur ou va avoir moins de propension à faire
	exactement les mêmes horreurs que pourtant il a vu en tant qu'horreur
	et qu'on lui a expliqué que c'était des horreurs comment est-ce que
	tu résous en fait ce ce ce problème là encore une fois ça peut-être
	que si tu tu as une solution prix Nobel de la Paix


- https://youtu.be/iEOelvaIqsM?t=750

ouais ou de physique enfin ou de chimie je m'en fous mais il y a un beau
chèque à l'arrivée mais surtout le c'est si moi je prétendais avoir la réponse ça serait
une escroquerie

par contre on a des éléments qui sont posés là
puisqu'on a depuis 45 il y a cette volonté manifeste de lutter contre
l'antisémitisme et le racisme il y a des choses qui ont été faites

est-ce qu'elles fonctionnent ou elles fonctionnent pas on peut constater que
après 2000 ans d'antisémitisme pour raccourcir vraiment les choses
et de manière pas caricatural mais vraiment condensé après 2000 ans
d'antisémitisme omniprésent dans les sociétés occidentales et 60 ans
donc on voit un temps très long et un temps très ramassé et 60 ans de
lutte contre l'antisémitisme ou 70 ans 90 % des Français le rejettent
donc le résultat si on veut être optimiste est pas si mal


maintenant
il faut pas se leurer on a affaire à une haine qui est millénaire sens
premier du terme et on se débarrasse pas d'une haine millénaire qui est
au cœur de nos cultures c'est pas une accusation c'est juste un constat
en quelques décennies ça c'est posé

- https://youtu.be/iEOelvaIqsM?t=815

on se rend compte qu'il y a quand
même des résultats

maintenant l'enseignement de la Shoah il est très
important quand moi j'ai j'ai expliqué que l'enseignement de la Shoah ne
vaccinait pas contre antisémitisme c'était une manière de dire qu'on peut
pas se contenter de ça alors après il y a ce jeu assez nauséabond
et qui renvoie aussi à l'antisémitisme qui consiste à dire encore pour
les Juifs toujours pour les Juifs machin

Dès qu'on dit encore pour les Juifs on a affaire à quelque chose déjà de nauséabond ça c'est le premier point
==============================================================================================================

ce qui est faux puisque si vous
regardez les programmes scolaires  on voit que il y a le génocide
des Tutsi qui est enseigné celui des Arméniens l'esclavage donc dès
qu'on dit encore pour les Juifs on a affaire à quelque chose déjà de
nauséabond ça c'est le premier point


Le deuxème point c'est que malgré cet enseignement on constate que ça ne permet pas de résoudre tout
=======================================================================================================

- https://youtu.be/iEOelvaIqsM?t=847

le deuxème point c'est que malgré
cet enseignement on constate que ça ne permet pas de résoudre tout et
donc il faut réfléchir à qu'est-ce qu'il faut rajouter d'autre ?

alors je pourrais dire simplement il faudrait s'interroger sur comment aborder
l'antisémitisme parce que la Shoah c'est le pire de l'antisémitisme et
en plus la Shoah est quelque chose qui était réalisé par le régime nazi
avec la collaboration de la France

mais en France on s'est très bien
porté en matière d'antisémitisme de l'affaire Dreyfus jusqu'au régime
de Vichy, Léon Blum qui se fait insulter quand il est élu enfin à la
tête du Front national en 36 il est insulté de sale juif à la tribune de
l'Assemblée nationale

donc il y a une histoire française et ça aussi
il faudrait envisager comment est-ce que on s'en on s'en saisit comment
se l'approprie en matière éducative qu'est-ce qu'on fait contre et je
prétends pas avoir de réponse

je dis juste que que la Shoah c'est pas
l'alpha et l'oméga qui permet de résoudre ce problème là

en plus il
y a quelque chose qui est évident dit de manière brutale si vous aimez
pas les Juifs et qu'on vous dit qu'on a tué les Juifs ben vous êtes
content et vous voulez faire la même chose

c'est pas parce que on vous
dit que c'est pas bien de tuer les gens que vous allez adhérer à ça
et quand je dis les Juifs ça vaut pour n'importe quelle autre minorité
qui est désigné comme étant une cible si vous êtes raciste et que
vous aimez pas les noirs et ben ça sera les noirs ou les Arabes ou
les musulmans ou telle ou telle minorité sexuelle  ce que vous voulez

voilà et là je suis pas en train d'édulcorer le sujet

L'antisémitisme c'est quelque chose qui est central dans nos sociétés et qui constitue la clé d'énormément de choses
========================================================================================================================

- https://youtu.be/iEOelvaIqsM?t=930

l'antisémitisme
c'est quelque chose qui est central dans nos sociétés et qui constitue
la clé d'énormément de choses

Regards.fr

	alors il y a une nouvelle donnée
	depuis quelques années en France c'est la place de l'extrême droite
	dans le champ politique français

	aujourd'hui il tourne autour de 30
	voire 40 voire plus de 40 % et donc ils occupent un espace dans le champ
	politico-médiatique important

	il s'avère que l'extrême droite a
	appelé à manifester contre l'antisémitisme dimanche dernier ce qui a
	provoqué énormément de de remous est-ce que d'après toi
	on peut décorréler la lutte contre l'antisémitisme de la lutte contre
	l'extrême droite


La lutte contre l'antisémitisme ne se limite pas à l'extrême droite déjà pour être très clair ça c'est le premier point
============================================================================================================================

- https://youtu.be/iEOelvaIqsM?t=975

alors pour moi mais depuis que j'ai commencé à bosser
la lutte contre l'antisémitisme ne se limite pas à l'extrême droite déjà pour
être très clair ça c'est le premier point

le deuxième point c'est non
ça peut pas être décorelé il suffit de regarder à quoi on a affaire
aujourd'hui

je parle pas de de Jean-Marie Le Pen

je parle de aujourd'hui
on va prendre Marine Le Pen, Jordane Bardella rassemblement national plus
l'`inénarrable Eric Zemmour <https://www.larousse.fr/dictionnaires/francais/in%C3%A9narrable/42785>`_ donc trois figures de premier plan tête d'affiche de
l'extrême droite

Jordane Bardella la semaine dernière nous explique que
Jean-Marie Le Pen n'est pas antisémite donc on a une négation du réel

Marine Le Pen n'a jamais fait de déclaration ouvertement antisémite
par contre quand on regarde ses discours ou sa "production littéraire"
entre guillemets il y a énormément de dog whistling

elle parle parle de
la finance internationale par exemple la finance internationale quand
vous êtes antisémite vous savez exactement de quoi on vous parle

et le :term:`dog whistling <dog whistling>` c'est ce concept sociologique
qui dit que on envoie des signaux qui sont audibles par ceux qui comprennent et qui passent
au dessus de ceux qui comprennent pas

donc l'antisémitisme continue à
faire partie de l'ADN du RN à différents degrés

- https://youtu.be/iEOelvaIqsM?t=1040

les deux exemples que
je viens de citer sont importants après on peut regarder l'entourage de Marine
Le Pen où il y a d'authentiques militants de l'extrême droite pure et
dure des gudards et cetera et cetera

puis si on prend Eric Zemmour lui
il nous dit beaucoup de choses

Eric Zemour pourquoi est-ce que il veut
réhabiliter Vichy parce que si on réhabilite Vichy duquel on retire
l'antisémitisme parce que tout son discours consiste à dire que Vichy
non seulement n'a pas collaboré avec le régime nazi mais en plus et il a
écrit noir sur blanc le régime de Vichy a protégé les Juifs français ce
qui est historiquement faux

- https://youtu.be/iEOelvaIqsM?t=1070


le statut des Juifs 3 octobre 40 première grand
texte de loi antisémite vise les Juifs français il y a pas différence
entre juifs français et juifs étrangers

pourquoi il dit ça parce que si
on  extrait l'antisémitisme de Vichy alors l'extrême droite devient
fréquentable et donc il y a pas de problème sauf qu'on peut pas extraire
l'antisémitisme de Vichy pas plus qu'on peut extraire l'antisémitisme
du front national/rassemblement national mais on a cette entreprise
de blanchiment qui consiste à dire l'antisémitisme est disqualifié on
en a conscience et c'est la position de Marine Le Pen et je sais pas si
elle est je sais pas si dans son for intérieur je peux pas être dans
sa tête elle a jamais fait une déclaration ouvertement antisémite

quand
elle fait du :term:`dog whistling` est-ce que c'est de la drague électorale ou
l'affirmation de sa pensée profonde je peux pas vous le dire

par contre
elle répugne pas à le faire et ça c'est très clair l'antisémitisme
continue à être véhiculé de manière plus ou moins ouverte

ouverte chez zemmour il a pas de problème là-dessus Pétain n'est pas antisémite
ce qui est faux historiquement

donc là

et de manière moins affirmé
publiquement chez Marine Le Pen donc non on peut pas le décorréler

Regards.fr

	il y a a il y a a une autre question qui est soulevée par cette le fait
	que l'extrême droite aujourd'hui se dise la première défenseuse des
	Juifs c'est la question de l'islamophobie et cette perméabilité en fait
	ce truc en fait de dire ils sont tellement islamophobes qu'ils deviennent
	défenseurs des Juifs dans leur tête et c'est d'ailleurs des alertes qui
	ont été lancées par beaucoup de personnalités de gauche mais aussi
	par le Président de la République qui hier en Suisse a dit attention
	combattre l'antisémitisme ça ne veut pas dire ça ne peut pas vouloir
	dire devenir enfin jeter l'opprobe sur les musulmans de France est-ce que
	ça comment toi tu l'envisages

- https://youtu.be/iEOelvaIqsM?t=1185

alors d'abord c'est quelque chose qui est
apparu en Europe en Europe dans les pays européens au tournant du 20e, 21e
siècle on a vu par exemple au Pays Bas des partis d'extrême droite être
islamophobes et donc et ça montre aussi l'intégration de israël-palestine
ou Israël contre les pays arabes dans la pensée européenne c'est si on
est islamophobe et on dénonce le danger d'invasion arabo-musulman parce
qu'on sait pas très bien enfin distinguer un arabe d'un musulman on sait
pas trop à quoi ça correspond alors on va s'affirmer comme défenseur
des Juifs ça permet aussi de dire qu'on a rien à voir avec l'extrême
droite d'avant la nazie ou les les assimilés

et donc les Juifs deviennent
utilisés dans ce discours là en disant on a plus de problème avec vous et
toute façon notre ennemi commun la preuve israël-palestine c'est les
Arabes musulmans enfin on sait pas trop les définir parce que si vous
dites à quelqu'un de raciste que les Arabes il peuvent être chrétiens
ou musulmans déjà il y a une disjonction cognitive

et si vous dites que
tous les musulmans sont pas arabes et que y a voilà bref

donc il y a
ça qui est fondamental mais le calcul de Zemmour qui est islamophobe au
premier degré c'est pourquoi il veut être d'extrême droite parce que
c'est l'extrême droite qui est ouvertement arabophobe ou islamophobe
ils savent

là aussi ils savent pas trop si le danger en France c'est
les Arabes ou si c'est les musulmans ce qui renvoie les Turcs les enfin
les pakistanais et cetera donc il y a tout ça

et donc si vous voulez
vous revendiquer d'extrême droite et que vous avez le le pédigré de
Zemour bah il faut quand même réhabiliter Vichy et pour réhabiliter
Vichy il faut dire Vichy était pas antisémite ni raciste et donc si
Vichy l'est pas donc vous pouvez vous en revendiquer donc oui il y a toute une
construction politique qui vise à montrer les populations musulmanes comme
étant les seules et uniques responsables ça c'est une réalité qu'on
retrouve à l'extrême droite

en même temps on peut pas faire comme si
au sein de ces populations il y avait pas des manifestation très claires
d'antisémitisme il suffit juste de prendre l'exemple de Mohamed Merah qui
est un pur produit français

- https://youtu.be/iEOelvaIqsM?t=1310

Regards.fr

	non mais c'est-à-dire que est-ce que ça
	veut dire que tu adhères à la thèse qui est aussi née dans les années
	de 2000 du nouvel antisémitisme c'est-à-dire qu'en gros on serait passé
	d'une production de l'antisémitisme de façon structurelle par
	à la fois l'extrême droite et l'Église catholique d'ailleurs avec des
	petits des petits mélanges hein à une production de l'antisémitisme
	par l'islam radical


Sur Proudhon, Blanqui ou Jules Guesde, Jacques Doriot, Marcel Déat
=====================================================================

- https://youtu.be/iEOelvaIqsM?t=1339
- https://fr.wikipedia.org/wiki/Jules_Guesde#Vers_une_premi%C3%A8re_unit%C3%A9_et_l'Affaire_Dreyfus
- https://fr.wikipedia.org/wiki/Jacques_Doriot
- https://fr.wikipedia.org/wiki/Marcel_D%C3%A9at#Le_Rassemblement_national-populaire_(1941-1944)

alors je vais répondre à ta question mais je vais
juste rajouter un élément dans la production d'antisémitisme à la fin
du 19e siècle il y a des gens comme Proudhon, Blanqui ou Jules Guesde qui sont
pas exactement l'extrême droite

donc l'antisémitisme c'est beaucoup plus
complexe ou plus large que ça

ça se nourrit d'énormément de choses
la dénonciation ouvrez les guillemets du "Juif capitaliste" fermez les guillements
n'est pas propre à l'extrême droite

ça vient de la gauche initialement donc
ça c'est important

Regards.fr

	ou mais ça jamais s'il y a eu l'antisémitisme qu'on a connu de façon
	étatique et monstrueuse c'est pas à cause de de Jules Guesde on va
	dire c'était pas alors c'est plutôt à cause de Maurras


non ça a permis de
nourrir des courants très importants qui ont trouvé à s'exprimer dans
les années 30 et 40 et on peut pas pas non plus faire abstraction de ça

ça a son importance aussi historiquement sinon on peut pas expliquer des
phénomènes comme Jacques Doriot ou Marcel Déat qui sont d'autentiques hommes de
gauche et de premier plan et qui d'un coup en 40 se découvrent une passion
antisémite donc parce que ça renvoie à ces choses là donc ça c'est le
premier point

Et ensuite non il y a pas de nouvel antisémitisme
===================================================

- https://youtu.be/iEOelvaIqsM?t=1392

Et ensuite non il y a pas de nouvel antisémitisme et c'est
pas parce qu'on est face à des populations et là il y a énormément de
guuillemets dans ce que je dire qui sont issus de l'immigration et là déjà
il y a un premier problème

c'est à partir de quand on est plus issu de l'immigration ?

quand on parle des gens de d'origine italienne ou d'origine
espagnole ou belge qui vivent qui sont français depuis deux générations
on dit pas issus de l'immigration

donc les Arabes ou les musulmans qui sont nés en France il sont plus
issus de l'immigration  ils sont français donc ça c'est le premier constat

ensuite dire qu'il y aura un truc d'importation
il y a importation israël-palestine ça c'est une évidence dans les
sociétés européennes

Il y a pas de nouvel antisémitisme, je n'ai jamais été un tenant du nouvel antisémitisme
===========================================================================================

- https://youtu.be/iEOelvaIqsM?t=1430

mais il y a pas de nouvel antisémitisme les
gens qui font profession d'antisémitisme qui sont français ou qui sont
arrivés en France dans leur jeunesse ils sont imbibés de Dieuddonné/Soral
ou d'autres qui sont des purs produits français

donc moi je n'ai jamais été un tenant du nouvel antisémitisme
dans ces manifestations il a rien de nouveau c'est la reprise de d'accusations
antisémites tout ce qu'il y a de plus traditionnel séculaire ou millénaire
ça c'est le premier constat

ensuite que ça soit alimenté entre autres au prétexte de
Israël-Palestine c'est pas le propre des populations musulmanes en France

l'extrême droite ou l'extrême gauche s'en sont saisis dès les années
60 et 70 et pour juste reprendre un exemple d'extrême gauche pour le coup
la Vieille Taupe qui était un des principaux pourvoyeurs du négationisme en
France dans les années 70 qui est une sorte de "boutique" entre guillemets
d'extrême gauche  bah il y a un fond d'Israël-Palestine aussi

si les Juifs ont inventé la Shoah c'est pour avoir un Etat

donc tous ces sujets-là irriguent l'antisémitisme depuis bien avant et qu'on l'impute uniquement
à cette population là ça dit aussi beaucoup de choses

ça dit que ça serait eux les derniers authentiques antisémites en France
ce qui est pas vraiment le cas

et ça serait les seuls antisémites en France qui n'est pas le cas

et l'antisémitisme aurait duré?? à l'ensemble des musulmanss
ce qui n'est pas le cas non plus

Donc oui il y a une essentialisation de cette population là
=================================================================

- https://youtu.be/iEOelvaIqsM?t=1510

donc oui il y a une essentialisation de cette population là qui est pointée
du doigt par certaines catégories politiques en France c'est une évidence

donc ça oui il y a pas de nouvel antisémitisme

il y a des Français ou des étrangers qui font montre d'un
antisémitisme et dont les ressorts sont les ressorts les plus traditionnels les
plus classiques de l'antisémitisme qui vont puiser aussi bien chez Maurras
chez Drumont chez Hitler ou chez d'autres penseurs

et pour certains qui
se revendiquent ouvertement de l'islamisme fondamentaliste mais l'islamisme
fondamentaliste c'est lui-même nourri de toute la
production européenne du 19e siècle comme le protocole des sages de Sion
ou d'autres brûleaux antisémites


Comment est-ce que tu juges la gauche quant à cette question de la lutte contre l'antisémitisme ?
========================================================================================================

- https://youtu.be/iEOelvaIqsM?t=1550

Regards.fr

	j'ai une dernière question Tal
	comment est-ce que tu juges  aujourd'hui mais pas forcément d'ailleurs que
	aujourd'hui pas que depuis le le 7 octobre la gauche quant à cette
	question de  la lutte contre l'antisémitisme ?


.. _tal_melenchon_2023_11_16:

2023-11-16 **Critiques de Bruttmann Sur Jean-Luc Mélenchon**
===============================================================

- :ref:`melenchon`

c'est quelque chose
qu'elle a abandonné en grande partie je dis pas toute la gauche mais le
principal parti de gauche aujourd'hui son pape et ses principaux
lieutenants ont aucun problème avec l'antisémitisme ils le martèlent depuis
alors Jean-Luc Mélenchon c'est pas depuis le 8 octobre qu'il balance
des tweets antisémites

il y a 2 ou 3 ans il avait expliqué que Jésus
avait été tué par les Juifs ça même l'Église catholique le dit plus
depuis Vatican II et les années 60

autrement dit on a une on a une série
de déclarations antisémites dites par Jean-Luc Mélenchon qui posent
aucun problème il y a pas eu de protestations massives

en 80 au moment de
l'attentat de contre la synagogue de la rue copernic Raymond Barre a fait
une sortie antisémite un attentat qui visait des Israélites et qui
a touché des des Français innocents

et il s'est fait défoncer

il en a fait qu'une

Jean-Luc Mélenchon il en a fait plusieurs dizaines sur la
décennie écoulée ça a pas provoqué de vrais remous particulièrement
pas au sein de son parti

donc oui il y a un vrai problème aujourd'hui en
matière d'antisémitisme et là où il y a un problème de fond qui renvoie
à ta question initiale et ce que j'ai expliqué sur la Shoah c'est que
la Shoah est devenue centrale dans la représentation de l'antisémitisme
et pour cause

mais ça a été commis par l'extrême droite et donc si
l'antisémitisme le plus criminel c'est l'extrême droite alors nous la
gauche on n'a pas connu d'antisémitisme on n'est pas antisémite et on ne
peut pas l'être

ce qui est faux historiquement Staline a mis en place des
politiques antisémites il y a eu des liquidations de Juifs en URSS dans
les autres pays du bloc de l'Est il y a eu des procès comme le procès
à Prague enfin il y a une politique antisémite forcenée à partir de la
fin des années 40 dans les pays du bloc de l'Est

la gauche aussi peut
être antisémite elle l'a été elle continue à l'être chez certains
de ses représentants en France on peut en parler pendant des heures mais
ça montre juste une chose c'est que régulièrement on refuse de voir
l'antisémitisme devant nous quand il nous concerne

plus facile de pointer
du doigt l'extrême droite qui est une réalité mais en même temps de
voir ce qui se passe de l'autre côté de l'échiquier politique on veut
pas le voir parce que ça vient bousculer nos représentations

on a une idée profondément ancrée depuis 45 l'antisémitisme c'est
l'extrême droite

bah non l'antisémitisme est beaucoup plus général
l'antisémitisme est une idéologie on retrouve dans sur tous les bancs
de l'Assemblée nationale pour aller très vite

Regards.fr

	en gros ce que tu leur
	reproches c'est c'est cette gauche sensément critique qui n'arrive pas
	à être critique d'elle-même


elle est pas critique d'elle-même et elle
évacue les problèmes qui se posent en son sein en disant c'est les autres
qui posent problème

non quand il y a des problèmes authentiques comme
celui-là qui est évident quand on explique un juif contrôle les médias
en l'occurrence libé BFM et qu'il est au service de l'État d'Israël on a
une demi-douzaine de mythes d'accusation antisémite typique

Regards.fr

	surtout que c'est complètement du fantasme puisque en l'urence Patrick Dr n'est
	plus propriétaire de libération

déjà et à moins qu'il ait la preuve ultime que Patrick drahi envoie des
ordres au comité de rédaction pour avoir telle ou telle ligne et qu'en
plus Patrick drahi est au service du gouvernement israélien et cetera et cetera

cette ligne d'attaque c'est exactement celle qu'il a déployé contre pour protéger
enfin défendre Jérémy Corbin il y a quelques années en disant c'est le
Likoud qui attaque corbin

donc il y a il y a une représentation du monde
qui peut s'expliquer pour plein de raisons historiques du milieu dont
il provient voilà il y a une représentation du monde qui se répète
depuis une dizaine d'années régulièrement plus ça va plus c'est même
plus du :term`dog whistling` c'est plus des trucs lancés de manière on
va dire modéré

c'est full frontal et autour de lui il y en a qui ont
protesté Ruffin, Corbière Garideau Autin voilà et je dis pas ça parce
que je suis je suis accueilli par Regards

voilà ça c'est la réalité et en
même temps il y en a qui disent mais pas du tout il y a aucun problème

- https://youtu.be/iEOelvaIqsM?t=1807

et en fait **non il y a un problème**:

- il y a ceux qui sont capables de penser contre eux c'est-à-dire de
  dire oui chez nous il y a un problème
- et il y a ceux qui soit n'ont aucun problème avec l'antisémitisme ce
  qui est déjà en soi un problème
- et ceux qui sont antisémites et qui s'y retrouvent

Voilà il faut le dire.

Regards.fr

	la lutte contre l'antisémitisme est encore devant nous
	hélas et mais bon ça fait ça fait ça fait du travail en fait je veux
	dire moi moi je trouve ça intéressant aussi de commencer de commencer
	aussi à s'autocritiquer en passant par l'antisémitisme j'espère que là
	ce qui est en train de se passer c'est un ça va susciter un déclic pour
	ensuite pouvoir travailler sur plein d'autres questions
	merci beaucoup Tal

en espérant que ça soit cas et je suis pas fan du terme autocritique mais
plutôt de réflexion c'est autocritique ça fait un peu flagellation

de mener une réflexion voilà et qui permet de réfléchir beaucoup plus
largement évidemment.
donc merci Pablo pour l'accueil et pour cette interview.


Merci beaucoup Tal.
