

.. _backseat_2023_11_18:

===========================================================================================================
2023-11-18 **Antisémitisme : qui sont le collectif Golem, remarqué pendant la marche du 12 novembre ?**
===========================================================================================================

- https://www.youtube.com/watch?v=WBnfsmHLwws&t=2245s

Jean, Sasha, Latifa et Hugo discutent des actus de la semaine avec,
au programme cette semaine : la France face à la commission Européenne
sur la définition du viol, & la polémique sur les tickets restaurants.

Nous recevons également Jonas Cardoso, porte-parole du Collectif Golem,
pour comprendre ce qu’est ce collectif qui s’est fait remarqué lors de
la marche contre l’antisémitisme.


quand on s'exprime on nous demande très vite de montrer patte blanche
sur notre avis sur Israël ce genre de choses et à côté de ça il y
a une manifestation une marche qui nous parle parce que lutter contre
l' sémitisme bah c'est quand même important et là on voit qu'il y
a le rassemblement national queil y a l'extrême droite on pouvait pas
laisser passer ça on voulait pas les laisser manifester tranquillement
et c'est pour ça que ce collectif s'est créé avec des juifs et
des Juifs de gauche mais aussi avec des alliés mais donc c'est c'est
c'est intéressant parce que tu tu tu il y a une espèce de tension
chez vous il y avait un côté on ne peut pas ne pas aller manifester
à une manifestation contre l'antimétisme et en même temps on ne peut
pas aller manifester puisqu'il y a le RN qui va manifester c'est cette
espèce de contradiction cette tension qui vous a donné envie de dire
on va y aller mais on va bordéliser le cortège du RN c'est ça c'est
parce qu'enit il y a beaucoup de de personnes à gauche qui nous ont
reproché en tout cas qui m'ont reproché il m'ont dit mais vous marchez
avec l'extrême droite j'ai dis non moi je marche contre l'antisémitisme
et si je marche contre l'antisémitisme je marche contre les antisémites
quel qu'ils soi et s'ils sont à cette marche bah je vais agir contre les
antisémites donc pour moi c'est c'est une logique en fait ça va avec si
je marche contre l'antisémitisme il faut agir contre les antisémites et
là en l'occurrence on en avait un certain nombre sous la main donc on a
pu agir avec cette action que vous avez vu qui est-ce qui vous a rejoint
vous êtes combien aujourd'hui ça représente quoi votre collectif alors
aujourd'hui on est un peu plus de 200 dans un groupe WhatsApp mais c'était
assez informel on est en train de se structurer euh l'objectif c'est de de
devenir une maison pour les juifs et les Juifs de gauche qui ne se sentent
pas représentés qui se sentent seul soit dans le organisation syndicale
soit dans leur association soit dans leur parti politique et de continuer
un certain nombre d'actions populaires joyeuse contre l'antisémitisme
d'où qu'ils viennent Sacha euh tu tu tu parles depuis le début des
Juifs et juives de gauche est-ce que tu peux nous expliquer un peu le le
pourquoi c'est difficile euh en ce moment surtout en ce moment en fait
et et aussi est-ce que vous enfin vous allez accueillir tout le monde je
suppose dans ce collectif il y a pas de oui alors d'abord pourquoi c'est
difficile plus part plus particulièrement quand on est à gauche parce
qu'en fait la gauche historiquement est censée se saisir de la lutte
contre l'antisémitisme je veux dire depuis l'affaire de rfus c'est censé
être un de ses penchants les plus importants et en fait c'est pas le cas
c'est pas le cas on voit des silences on voit des ambiguïtés et on voit
même des propos antisémites parce que aujourd'hui il faut dire que à
la France insoumise et dans d'autres parties d'extrême gauche il y a des
personnes qui tiennent des propos antisémite mais on voit pas la gauche
se lever contre cet antisémitisme ou alors juste dire l'antisémitisme
c'est pas bien mais ne pas arriver avec des propositions politiques des
plans d'action fort de la formation et donc en plus on est à gauche
plus naturellement pour la question du peuple palestinien plus attaché
à dénoncer le gouvernement israélien mais quand on est juif il y a
toujours une petite suspicion donc en fait tu es en train de nous dire
que vous êtes coincé entre la gauche qui vous soutient pas et les gens
qui vous demandent de vous justifier de tout et n'importe quoi et de la
d'Israël c'est ça et de l'autre côté ceux qui disent lutter contre
l'antisémitisme c'est la droite l'extrême droite qui en fait est juste
une meilleure façon de taper sur l'immigration et sur les musulmans
moi je veux pas d'eux je veux pas que ce soit eux qui lutent contre
l'antisémitisme je veux que ça soit mon camp politique mais pour ça
il faut s'en saisir et parfois on sent que c'est un peu compliqué donc
c'est aussi pour ça qu'on qu'on donne une une continuité au collectif
Latif ouais euh non oui moi juste de dire que du coup ce collectif là et
de voir qu'il y avait des gens qui se mobilisaient contre bah l'extrême
droite dans ce rassemblement contre l'antisémitisme ça a fait du bien à
voir quoi enfin moi en tant que en tant que personne ça m'a fait du bien
de voir ça parce que c'est pas une marche je suis pas allé à la marche
et c'est pas une marche où j'avais où j'allais me sentir bien d'aller
très très sincèrement oui parce que le Ren parce que reconquête parce
que parce que je suis une personne musulmane aussi et que forcément c'est
pas ça c'était pas ouais je me voyais pas en fait marcher à côté de
ces gens-là de près ou de loin surtout vu toutes les déclarations qu'
sur auxquelles on a assisté en plateau ou ailleurs ces dernières semaines
et moi j'avais une question sur bah sur cette solitude à gauche pour pour
certains juifs et juifs de France et et il y a d'autres collectifs à gauche
et cetera et comment un peu la ligne a été définie parce que je vois
des gens parler de CDC par exemple dans le chat ou alors d'autres d'autres
un un un collectif aussi de de de Juif qu'on a pu voir et de juives sur
les réseaux ou la o je pense c'est une ligne de démarcation qui est un
peu compliquée sur la bah la question coloniale en fait enfin et et comment
voilà ouais comment comment vous avez enfin vous vous êtes positionné
là-dessus et est-ce que ça fait l'objet de réflexion et et oui voilà
enfin c'est une question que que je poser alors d'abord il y a on a dans
dans notre collectif des gens qui viennent aussi de mouvement de juif
de gauche qui existaiit déjà je pense à orage ou au juif et et juif
révolutionnaire que salut qui qui nous ont beaucoup aidé euh oui qui
ont été actifs depuis ouais plusurs semaines déjà j'ai vu sur les
réseaux sociaux exactement et ensuite nous on dit que on lutte contre
l'antisémitisme et qu'en fait le reste ok c'est pas notre sujet moi j'ai
pas envie de dire je défends un juif ou je lutte contre l'antisémitisme
parce qu'en fait lutter contre l'antisémitisme c'est pas juste défendre
les Juifs c'est lutter contre une discrimination c'est essentiel mais que
s'il est pas d'accord avec netanahù ou que si ah il est absolument clair
sur la politique d'Israël ou alors que s'il me dit qu'il est antisioniste
non moi je je ça je peux pas accepter on peut pas accepter et pourquoi
P SDEC pour nous en tout cas pour moi c DEC c'est c'est un collectif avec
qui je n'ai pas du tout d'Atom crochu parce qu'il défendent des thèses
qui sont des thèse utilisé par les antisémites le philosémitisme
d'État est une thèse antisémite qui est soutenue par CDC et que c'est
DEC tu peux nous expliquer ce que c'est le phillosémitisme d'État en fait
c'est une bon pour le dire très simplement c'est de dire que la minorité
juive euh serait privilégiée par l'État euh serait voilà serait plus
défendu que les autres en France en France mais en fait c'est un enfin
je veux dire c'est c'est euh c'est un peu c'est en fait c'est une façon
de dire voilà on a on voit qu'il y a des policiers par exemple devant
les synagogues euh qui protègent les Juifs ah regardz l'état protège
les Juifs et ça va avec ce grand complot qui existe depuis toujours que
les Juifs sont proches du pouvoir et cetera donc ça c'est ce qui vous
éloigne de ce collectif CDEC c'est un une des choses qui nous éloigne
euh il y en a beaucoup d'autres mais je veux dire la question est pas
de taper sur ces deck ou enfin voilà oui voilà parce que là du coup
ils sont pas présent il c'est juste que il y a beaucoup de personnes
à gauche et à l'extrême gauche qui disent non mais regardez on a de
C deck avec nous donc on peut pas être antisémite non c'est pas parce
que vous parlez avec des juifs ou que dans vos manifestations vous avez
euh un une bandrôle où il y a écrit juif des coloniaux que c'est pas
pour ça que vous avez des propos ou des tropes ou des bien antisémites
oui Hugo toi tu étais à la manifestation dimanche ouais j'étais à
la manif avec fierté j'assume j'ai j'ai parce que c'est je je suis goy
moi je je je ne suis pas je ne suis pas juif mais j'ai j'ai des amis
qui le sont et B comme tu l'as comme tu l'as bien dit qui en soit le
conflit israélo-palestinien je vais pas dire qu'ils s'en foutent mais
ils sont pas sionistes et cetera eux ils ont peur c'est juste ça ils ont
peur et donc j'y suis allé pour cette manif et qu-ce que tu as vu alors
place en fait il y avait quatre cortèges pour résumer entre guillemets
pour résumer tu avais le premier cortège qui était le le cortège
gouvernemental avec François Hollande ellisabeth borne et cetera et
cetera tu avais le deuxième gouvernement qui était le gouvernement par
qui était le le cortège parlementaire en gros il y avait les élus et
le troisième cortège qui était le cortège grand public où tu avais
100000 bah où il y avait le gros des 100000 personnes pour résumer et
tu avais en queue de cortège vraiment tout à la fin je alors toi tu as
peut-être des chiffres plus précis que moi mais je pense une centaine
200 300 personnes de reconquête du RN de la LDJ considéré je rappelle
comme une organisation terroriste dans certains pays qui était présent
et en fait quand le gros du cortège est arrivé au Sénat Marine Le Pen
était encore à l'Assemblée nationale tellement il y avait de monde et
c'était bien séparé moi je suis resté jusqu'à la fin par curiosité
journ tu dis c'est qu'il y avait un cordon sanitaire de fait qui s'est
ouais il y avait le RN était à la fin comme comme l'eau et voilà il y
avait une séparation quand même le cortège RN et LDJ étai relativement
encadré il y avait même dans les cordons sanitaires entre guillemets le
service d'or du RN qui faisait vraiment un cordon il y avait en gros le
RN vraiment tout à la fin je sais pas si reconquête a manifesté ça en
tout cas jusqu'au bout en tout cas j'ai pas vu ERC Zemour et il y avait
ce cordon de la sécurité et la LDJ devant moi j'ai fait me faire casser
la gueule par la LDJ parce que j'ai commis l'outrage parce que j'étais
énervé vraiment ça m'avait énervé certaines déclarations qui avaient
été faites dernièrement notamment j' le P n'est pas antisémite de
Jordan bernella qui est une inepsie je l'ai une TAV Jean-Marie Le Pen
je peux confirmer il est antisémite il n'a jamais regretté ce qu'il
a dit et cetera et voilà il était même choqué que les jeunes soient
choqués qu'on soit qu'on soit choqué de ce qu'il a dit c'est vous dire
la pathologie du monsieur et en fait moi j'ai crié Le Pen louau on n'oublie
pas je l'ai crié trois fois en partant et il y a un jeune homme que qui
vient m'insulter dans des termes très fleuris je je présume qu'il était
de la LDJ parce que la LJ a agressé la LJ c'est la Ligue de défense juif
c'est ça la Ligue de défense juif tout à fait qui a agressé un jeune
homme en bicyclette qui avait crié je crois Le Pen non merci ou Le Pen
casse-toi qui l'ont molesté au sol euh et ce monsieur vient m'insulter
et me et me menace frontalement voilà c'est intéressant parce que toi tu
nous dis que dans le cortège donc il y avait une séparation comme entre
l'huile et l'eau avec le Ren qui était tout à la fin et qui qui était
quelques centaines de personnes pas plus de face à une marée humaine de
plus de 100000 personnes on a quand même même vu les positionnements se
mettre en place c'était assez intéressant on a vu par exemple elliizabeth
borne renvoyer la France insoumise en dehors de l'arc républicain après
le 7 octobre on a vu Serge clarfeld dire qu'il fallait se réjouir de
la place du RN dans une manifestation contre l'antisémitisme comment
est-ce que toi Jonas tu tu tu tu identifies ce nouveau paysage politique
autour de la question antisémite ben je pense que l'antisémitisme est
la bonne excuse pour chacun euh en fait oui il y a enfin je veux dire moi
pour Monsieur clarsfeld j'ai un immense respect pour ce qu'il a accompli
dans sa vie après tomber dans le piège de dire que le RN c'est OK enfin
je veux dire pour préparer l'émission je voulais me faire des petites
notes de qui est antisémite qui est négationniste au RN ça m'a pris
tellement de temps il y en a trop et d'ailleurs j'invite à j'invite à à
regarder le la vidéo de Blas qui est sortie très récemment avec Salomé
sake qui qui présente certains personnages du du rassemblement national
enfin je veux dire c'est pas juste du passé c'est un héritage et un
héritage on le garde on le conserve on l'entretient et c'est exactement
ce qui se passe au RN aujourd'hui il y a il y a par exemple pour un seul
exemple aujourd'hui sur les bancs de l'Assemblée nationale il y a monsieur
bcaletti qui a tenu une librairie négationniste et antisémite dont le
nom fait référence à Charles Moras qui était lui-même quelqu'un qui
revendiquait l'antisémitisme d'État et qui a fait de la prison aussi pour
violence avec armes au moment où il coulait des affiches et cetera Latifa
moi j'ai une question est-ce que vous vous attendiez à cet accueil entre
guillemets en en ouais en créance collectif et et la manière dont vous
êtes arrivé où vous êtes dit que ça allait que que que ça allait
globalement bien se passer non alors j'ai je sais pas j'avoue des fois
moi je suis j'ai déjà eu un peu d'expérience avec le service d'ordre
de de Monsieur Zemo je en général on n pas très bien accueilli après
euh moi ce qui m'a beaucoup choqué c'est ce moment avec la police c'est
que on laisse marcher tranquillement défiler tranquillement on protège
des antisémites à une marche contre l'antisémitisme on les protège de
Juifs de Juifs voilà et et ensuite une fois qu'on était dans la marche il
y a des gens qui disaient ah c'est pas le moment et cetera mais une fois
qu'on discutait avec eux qu'on leur rappelait certains éléments en fait
on voyait que vraiment les personnes comprenaientent lesquels enfin pour
le coup juste pour des des Juifs des Juifs des gens qui étaient là pour
la marche des gens vraiment des gens dans la foule alors il y a beaucoup
de gens qui nous applaudissaient qui nous rejoignaient je pense vraiment
qu'il y a des gens et ensuite sur les réseaux sociaux on a eu beaucoup de
messages aussi de personnes qui ont dit ça nous a fait du bien de voir
ça mais après évidemment on nattendait pas que le RN nous accueille
correctement c'est pas dans sa c'est pas dans sa façon de faire c'est
gen eu moi aussi je suis de confession juive et et autour de moi chez les
surtout les jeunes je vois beaucoup de jeunes qui se dirigent justement
vers reconquête et le rassemblement national euh je me posais la question
en esquivant un peu tout l'héritage dont don tu parlais euh et vraiment
ça m'effrait en fait ça m'effrait et on sait qu'en 2022 plus de 50 %
des Français d'Israël ont aussi voté pour Eric Zemour et je trouve ça
terrifiant parce que éc Zemour a tenu aussi des propos négationnistes
notamment sur sur sur Pétin et son entourage et son entourage euh comment
enfin qu'est-ce que tu as envie de transmettre toi comme message à ces
jeunes et est-ce que avec le collectif vous avez prévu de faire des de
la vulgarisation de l'extrême dro droite par exemple ou de l'éducation
à l'extrême droite enfin je sais pas mais mais faire comprendre que
c'est pas anodin ce qui s'est passé enfin Jean-Marie Le Pen n'est pas
anodin Jean-Marie Le Pen a toujours une enfin c'est toujours imprégné
au sein du RN au sein de reconquête C cette antisémitisme là pardon
enfin voilà c'est c'est une longue question c'est pas un détail de leur
histoire l'antisémitisme c'est sûr bien dit lui un message à faire
passer c'est en fait faut se faut comprendre ce que c'est faut aussi
dire que la gauche peut lutter contre l'antisémitisme parce qu'en fait
c'est oui on peut dire n'alllez pas chez eux mais il faut qu'il y ait une
alternative je suis convaincu que la gauche est une alternative parce
que ça va dans l'antiracisme en fait ça va dans dans la lutte contre
les discriminations et et est-ce que dans dans le collectif Golem on a
cet objectif oui on va faire descollages on va faire des infographies
enfin il y a plein de choses qui sont dans les tu on est en train de se
structurer de discuter mais c'est évidemment aussi un enjeu de rappeler
que Alex extrême droite c'est iné c'est intrasec l'antisémitisme tu
veux dire que l'antisémitisme à gauche c'est par accident alors qu'à
droite c'est intrinsè non je te pose la question est-ce que tu peux nous
expliquer ce que ça veut dire ce que toi tu as pu voir dans des collectifs
de gauche voir d'extrême gauche d'antisémite d'antisémitisme est-ce
que tu as des exemples est-ce que tu as des témoignages déjà il y a
la grande thèse du nouvel antisémitisme à gauche moi je suis pas du
tout d'accord avec avec ça l'antisémitisme c'est le même depuis des
minaires c'est juste on le réactive de façon différente euh enfin je
veux dire c'est pas un néo antisémit il y a pas un néoantisémitisme
euh les lieux de production de l'antisémitisme majoritaire ont pour
beaucoup été à l'extrême droite ouais ensuite ils sont réactivés
repris à gauche de différentes façon évidemment il y a le sujet du
conflit israël-palestinien mais pas que par exemple considérer que
les juifs ou que la lutte contre l'antisémitisme ne pourrait pas être
tout à fait dans l'antiracisme parce que les Juifs sont des blancs qui
seraient bourgeois ou privilégiés ça c'est des choses qu'on entend à
gauche le fait de truc que tu as entendu toi alors oui que j'ai vu dans
des twtes que voilà sin c'est des c'est des clichés qui reviennent quand
même régulièrement en plus les Juifs qui sont tous dans les médias qui
contrôlent l'économie mondiale enfin exactement et aussi le fait qu'on
serait euh par définition sioniste défenseur de l'État d'Israël et donc
on ne pourrait pas être allié de la gauche euh ou alors que comme on a de
l'argent on ne peut pas vraiment être antiapitaliste enfin je veux dire
euh c'est dire antisémite des prop antisémites en fait ils sont partout
dans la société ils sont pas particulièrement à gauche ENF je veux dire
ils sont dans toutes les couches et on le voit d'ailleurs quand il y a des
sondages sur la porosité au préjugés antisémite en fait le parti qui
en a le moins dans le dernier sondage de l'ugegf c'est Europe Écologie
Les ver c'est c'est pas c'est pas facile à expliquer parqu tu es toi je
c'est parti dans lequelle je suis moi mais en fait les deux endroits les
deux pôles où où c'est majoritaire c'est en tout cas dans les parties
où ça a été testé c'est l'extrême droite et la France insoumise pour
les préjugés antisémites d'accord l'antisémitisme il est partout à des
seuils très inquiétants et il faut le combattre partout moi j'entends ce
que tu disais au début que le collectif s'est créé pour lutter contre
l'antisémitisme on va dire sans préalable en fait sans dire voilà telle
personne de juive à telle ou telle position par conséquent notre soutien
va différer par rapport à ça mais comment bah on compose après avec la
réalité actuelle en fait sur là on a beaucoup parlé d'importation du
conflit et tout tout mais comment on compose avec ce qui se passe ce qui
se passe actuellement eTTH et et et aussi parfois quand tu disais bah sur
les sur les valeurs de gauche où forcément on peut pas être antiioniste
ou on est forcément on défend l'État d'Israël et cetera enfin je sais
que c'est des questions compliquées hein qui sont vraiment voilà encore
une fois sur la la question coloniale et cetera en en Palestine et et
en Israël mais ouais est-ce que ça a fait l'objet de discussion avant
vous sur ce positionnement là ou pas et et comment on traite ça en fait
com parce que on peut évidemment pas complètement exclure le fait que
il y a des gens qui aujourd'hui commettent ont des propos antisémite ou
commettre des propos antisémites parce que dans leur esprit c'est enfin
c'est très lié à la politique de l'État israélien ou d'extrême droite
de l'État israélien mais comment on traite avec cette réalité là en
fait qui qui qui est qui qui est bien existante surtout quand on voit là
toutes les condamnations ou les premières comparitions sur des propos
antisémit et cetera il y a beaucoup de gens qui amènent sur la table
ce sujetl ouai bah déjà c'est c'est malheureux parce qu'on j'ai envie
de dire quelqu'un qui qui qui victime d'acte antisémite en France euh
c'est de la faute de la personne qui a commis l'acte antisémite c'est pas
la faute d'Israël c'est pas la faute pass làbas et cetera mais ensuite
évidemment que c'est un sujet euh il faut faire de la pédagogie il faut
accepter aussi d'entendre que une personne juive peut être sioniste et
que c'est pas pour ça qu'elle peut pas être de gauche ou que c'est
pas pour ça que ça peut pas être un allié parce que d'ailleurs le
sionisme et l'antisionisme sont des termes qui sont énormément dévoyés
aujourd'hui il y a une bataille d'image il y a une bataille de mots et
quand on voit que des personnalités politiques mettent de l'huile sur le
feu en disant que par exemple une marche pour contrô l'antisémitisme
c'est une marche des soutiens inconditionnels à Israël non je veux
dire moi je suis pas un soutien inconditionnel de néanaho et il y a
beaucoup de gens dans le collectif golem qui ne le sont pas du tout et
pour autant on est allé marcher euh on est allé marcher ce dimanche
enfin il faut arrêter de tout mélanger et quand le sujet vient sur la
table être capable d'expliquer que voilà chacun peut avoir certaines
positions sur le conflit israélo-palestinien et cetera la ligne j'ai
envie de dire quand même c'est ça serait de dire que on évite les
victimes les victimes civiles et cetera enfin ça devrait être quand
même la oui ouais mais comme la colonisation le sujet colonial même si
il a des différences pour la situation israëlo-palestinienne et quand
même quelque chose d'assez comment dire une colonne vertébrale pour une
certaine gauche quoi oui mais enfin je veux dire ça ça n'empêche pas de
lutter contre l'antisémitisme ça n'empêche pas et je veux dire on ne
doit pas attendre de quelqu'un en fait c'est c'est de l'essentialisation
c'est le terme c'est que quand on rapporte quelqu'un pour ce qu'il est
au conflit au Proche Orient mais que ce soit des musulmans des Arabes
ou des Juifs c'est de l'essentialisation et nous on a on veut dire il y
a pas de compétition entre est-ce que on doit dire que ce juif là il
a une meilleure position ce musulman là il a bien condamné les actes
du Ham et ça non je veux dire fait par exemple le Éric Zemour hier ou
aujourd'hui sur en en ramenant tous les musulmans de France au conflit
Isra le Palestinien en disant tous les musulmans doivent se désolidariser
du Hamas sinon c'est c'est c'est ça l'essentialisation c'est ramen une
identité à pr je précise juste juste un truc à préciser tout à l'heure
tu as parlé d'une enquête qui a été faite sur les différents partis
politiques et l'antisémitisme parmi leurs sympathisants c'est l'UEJF la
source alors c'est c'est le le le sondage de l'uajf qui avait fait grand
bruit sur les 91 % d'étudiant victimes auprès du Figaro non c'était
peut-être ça mais je crois que mais Ipsos là c'est Ipsos c'est pour
la Commission nationale consultative des droit de l'homme c'est ce que
je voulais juste en fait il y en a plusieurs en fait il y en a plusieurs
et on voit que c'est souvent à peu près les mêmes chiffres mais dans
ils font en fait sur ils testent sur différentes populations sur enfin
ils font un large sondage et notamment sur la porosité au préjugés
antisémite selon les les les sympathisants politiques justement
j'ai des chiffs là-dessus par rapport à ça justement c'était par
exemple à la question est-ce que vous êtes d'accord ou pas d'accord
avec l'affirmation les Juifs ont un rapport particulier avec l'argent
par exemple qui est un cliché antisémite commun à l'électorat DLF
rassemblement national reconquête on est à 52 % chez LR on est à 46 %
PS 26 % de d'accord et à LFI on monte à 49 % euh et il y a d'autres il
y a d'autres chifffr c'était cette enquête pour la CNCDH c'est ça là
il y en a un autre c'est les Juifs ont trop de pouvoir en France qui est
aussi un grand cliché et cetera orn on est à 25 % alr 15 19 % LFI 36 %
donc il y a il y a des tropes antisémites où l'électorat insoumis a
répondu en faveur après il y a d'autres évidemment il y a pas que du
négatif he il y a aussi certaines réponses où LFI est dans la norme
des parties républicains et cetera et cetera exemples que tu as donné
c'éit làdessus c'est ça c'était sur les tropes antisémites il y a le
directeur je crois le de recherche de l'psos Matthieu Galard qui a fait
un traade très intéressant là-dessus sur son compte Twitter on va pas
donner tous les chiffres mais si vous voulez aller voir et le dossier fait
350 pages de l'pso si vous avez envie d'aller lire c'est sur tout le RAC
dernière question dernière question pour toi Jonas c'est quoi l'avenir du
coup du collectif Golem on l'a dit c'est né à peine la semaine dernière
euh vous vous rencontrez un succès important en ce moment vous allez
vous transformer vous allez faire quoi d'abord on va dormir euh et et
ensuite on va on est en train de se structurer on a une première on a
des premières réunions toutes et tous ensemble ou en non mixité avec
uniquement les juifs et les juives qui sont dans le collectif qui vont
arriver et euh l'objectif principal c'est de l'action c'est se retrouver
que ce soit des moments festifs ou des moments de militantisme pour aller
dénoncer l'antisémitisme où il est quel qu'il soit et évidemment
rappeler que l'extrême droite ne peut pas lutter contre l'antisémitisme
que l'extrême droite est antisémite mais aussi dire à nos alliés à nos
copainscopines de gauche faites attention arrêtez de minimiser arrêtez
de dire que ça existe pas arrêz de dire que c'est pas vrai là je l'ai
vu un peu passer dans le chat beaucoup qui disaient ouais mais non ou
montrer aussi que F sont en fait c'est pas la question on n'est pas en
train de faire un concours qui est le plus plus ou moins antisémite on
est en train de dire que il faut lutter contre l'antisémitisme et le
premier préalable à ça c'est d'accepter de le voir même quand c'est
devant sa porte parce oui Jonas il quelqu'un qui demande dans le chat
le collectif n'est pas ouvert qu'au personnes de confession juiv on est
d'accord non alors c'est c'est encore en discussion mais pour l'instant
c'est c'est ouvert à à toutes et tous après évidemment que on veut
que ce soit des juifs et des juives euh qui prennent les décisions
importantes qui voilà des personnes concernées parce que sinon on on
retombe dans des travers que beaucoup de d'organisations puissent avoir
et surtout je crois que ça fait un un grand bien à beaucoup de personnes
juif et juive qui ressentaient cette solitude dont je parlais au début de
pouvoir se retrouver avec des gens qui pensent comme eux merci beaucoup
Jonas Cardoso pouz l'applaudir merci beaucoup d'être venu camer sur le
plateau de B je te laisse quitter le plateau merci à toi chers amis on
continue cette émission et Le jeu à la con on enchaîne
