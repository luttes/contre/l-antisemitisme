.. index::
   pair: Robert Hirsch ; Antisémitisme : « À gauche, la prise de conscience est limitée (2023-11-18)

.. _robert_hirsch_2023_11_18:

========================================================================================================
2023-11-18 **Antisémitisme : « À gauche, la prise de conscience est limitée »** par Robert Hirsch
========================================================================================================

- https://www.mediapart.fr/journal/politique/181123/antisemitisme-gauche-la-prise-de-conscience-est-limitee

.. note:: l’appel à la manifestation du 12 novembre 2023 est venue de la
   gauche (David repris par Faure).

   Puis très vite les représentants des deux chambres ont appelé, souhaitant
   élargir l’arc républicain et ne pas laisser l’initiative à la gauche.


Introduction |GaucheEtJuives|
=====================================

- :ref:`la_gauche_et_les_juifs`

Dans son ouvrage « La Gauche et les juifs », l’ancien militant trotskiste
Robert Hirsch analyse les conséquences de deux décennies de « myopie »
sur la montée des actes antisémites, au-delà de l’extrême droite.

Il pointe les ambiguïtés de l’antisionisme contemporain.


L’augmentation considérable des actes antisémites depuis
plus d’un mois et les débats qui agitent la gauche sur la manière
d’affronter le phénomène sont l’acmé de tendances à l’œuvre
depuis au moins deux décennies.

C’est ce que l’on comprend en lisant :ref:`La Gauche et les juifs (Le Bord de l’eau) <la_gauche_et_les_juifs>`,
publié en 2022 par Robert Hirsch.

Ancien militant trotskiste et professeur d’histoire-géographie, ce
dernier a livré une synthèse abordant aussi bien la longue durée
des persécutions antisémites que les variantes du sionisme et de
l’antisionisme.

Mais son ouvrage se voulait aussi une interpellation
de son propre camp. Robert Hirsch fait partie de ceux et celles qui ont
reproché à la gauche, notamment dans ses traditions les plus radicales,
sa « myopie » face à la montée des actes antisémites à partir
des années 2000.



Il a pointé, chez des organisations partisanes ou antiracistes, ainsi que
chez des figures intellectuelles, une relativisation peu convaincante des
faits, une incapacité à les prendre au sérieux sans aussitôt mettre
en avant d’autres oppressions relevant de logiques différentes, et une
négligence du « ressenti » des juifs eux-mêmes. Autant d’ingrédients
qui ont concouru à éloigner ces derniers du « camp du progrès »,
alors que les affinités entre les juifs et la gauche de transformation
furent si puissantes par le passé.

Dans cet entretien, Robert Hirsch revient sur la manifestation de dimanche
dernier, la gêne de la gauche face à la diffusion de l’antisémitisme
dans d’autres milieux que ceux de l’extrême droite, et les
interférences entre le destin de la communauté juive en France et la
question israélo-palestinienne.

Mediapart : Dimanche 12 novembre, on a vu défiler l’extrême droite dans une manifestation contre l’antisémitisme dont était absente toute une composante de la gauche. Comment analysez-vous ce moment ?
==============================================================================================================================================================================================================

Robert Hirsch : La conjoncture actuelle est façonnée par le poids des
vingt-cinq années écoulées.

Durant tout ce temps-là, la gauche, et en
particulier la gauche radicale, n’a pas pris la mesure de la montée
objective de l’antisémitisme, quand elle n’a pas carrément été
dans le déni.

Aujourd’hui, elle n’est donc pas en position d’être à l’avant-garde du combat contre l’antisémitisme
---------------------------------------------------------------------------------------------------------

**Aujourd’hui, elle n’est donc pas en position d’être à l’avant-garde du
combat contre l’antisémitisme**.

Par conséquent, elle a dû affronter un problème considérable.
En effet, c’est la droite qui a été à l’initiative de la marche du 12 novembre 2023,


.. note:: l’appel à la manifestation du 12 novembre 2023 est venue de la
   gauche (David repris par Faure).

   Puis très vite les représentants des deux chambres ont appelé, souhaitant
   élargir l’arc républicain et ne pas laisser l’initiative à la gauche.

à laquelle l’extrême droite s’est immédiatement invitée afin de
poursuivre son entreprise de « dédiabolisation ».

Il faut rappeler que d’après les enquêtes sérieuses régulièrement menées sur le sujet,
l’électorat de Marine Le Pen reste celui qui a le plus de préjugés et
d’hostilité contre les juifs.

Elle-même est cependant assez cohérente dans sa stratégie : depuis qu’elle
a pris la tête du parti fondé par son père, elle n’a pas fait de sortie
antisémite comme lui avait pu en faire.

À partir de là, il n’y avait guère que des mauvaises solutions :
s’abstenir de participer, ou y aller en dépit de la présence de
l’extrême droite, que les organisateurs n’avaient pas explicitement
exclue.

En l’occurence, la manifestation me semble avoir été digne et
massive.

Et je trouve inadmissible que Jean-Luc Mélenchon, qui aurait pu se contenter de dire qu’il n’y allait pas, ait suggéré qu’il s’agissait d’une marche pour soutenir les assassins de Gaza
==========================================================================================================================================================================================

- :ref:`raar`

**Et je trouve inadmissible que Jean-Luc Mélenchon, qui aurait
pu se contenter de dire qu’il n’y allait pas, ait suggéré qu’il
s’agissait d’une marche pour soutenir les assassins de Gaza**.

Pour autant, des dirigeants de La France insoumise (LFI) ont tout de même tenté un rassemblement au square des Martyrs-Juifs-du-Vélodrome-d’Hiver. Le « déni » que vous évoquiez n’est-il pas en train de reculer ?
========================================================================================================================================================================================================================

- :ref:`jjr_ras_2023_11_14`

::

    Pour autant, des dirigeants de La France insoumise (LFI) ont tout de
    même tenté un rassemblement au square des Martyrs-Juifs-du-Vélodrome-d’Hiver
    Ailleurs à gauche, des partis comme Les Écologistes (ex-EELV) ont
    lancé des formations en interne sur la question de l’antisémitisme.

    Le « déni » que vous évoquiez n’est-il pas en train de reculer ?

Ce qui est certain, c’est que depuis l’attentat de l’Hyper
Cacher à Paris, en 2015, une certaine prise de conscience a eu lieu à
gauche.

Des mouvements se sont créés, comme le :ref:`Réseau d’action contre
l’antisémitisme et tous les racismes (RAAR) <raar>` dont je fais partie, qui
ont interpellé la gauche sur ses manquements, avec quelques réussites
en effet.


Cependant, la prise de conscience est limitée
==================================================

- https://www.mediapart.fr/journal/politique/091023/apres-l-attaque-du-hamas-la-gauche-se-dechire-sur-le-sens-des-mots

Cependant, la prise de conscience est limitée.

Les réactions aux
attaques du Hamas le 7 octobre 2023 l’ont illustré.

`Celles de LFI et du Nouveau Parti anticapitaliste (NPA) (la seconde pire que la première) <https://www.mediapart.fr/journal/politique/091023/apres-l-attaque-du-hamas-la-gauche-se-dechire-sur-le-sens-des-mots>`_
ont témoigné d’un manque d’empathie flagrant.

Bien évidemment, je ne suis pas d’accord avec celles et ceux qui caricaturent Mélenchon
en supporteur du Hamas. Il reste que le 7 octobre 2023 était le plus grand
massacre de juifs depuis la Seconde Guerre mondiale, et que les partis que
j’ai nommés n’ont pas saisi cette importance historique.

Ensuite, tout cela a été recouvert par ce qui se passe à Gaza, qui provoque
l’indignation justifiée de toute la gauche. Le 7 octobre 2023 ne doit
cependant pas être occulté.

Pour revenir sur la participation à la manifestation de dimanche 12 novembre 2023,
bien sûr que la question était compliquée à cause du RN.

Cela fait vingt ans que des organisations de gauche trouvent des raisons pour ne pas se rendre à des rassemblements contre l’antisémitisme
==============================================================================================================================================

Mais cela fait vingt ans que des organisations de gauche trouvent des raisons pour ne
pas se rendre à des rassemblements contre l’antisémitisme.

Pendant un temps, c’était à cause des positions du Crif [Conseil représentatif
des institutions juives de France – ndlr].

D’autres manifestations
ont eu lieu « contre l’antisémitisme et l’islamophobie », mais
quand il s’agit de s’insurger contre cette dernière, beaucoup ne se
sentent pas obligés d’ajouter « … et contre l’antisémitisme ».

Les mêmes préjugés, les mêmes mythes, voire la même haine ont été portés par de nouveaux milieux

Depuis les années 2000, expliquez-vous, non seulement les expressions et les actes antisémites ont connu une recrudescence, mais celle-ci a touché de nouveaux milieux. Lesquels ?
========================================================================================================================================================================================

Depuis la Seconde Guerre mondiale, l’antisémitisme n’était plus porté
que par des groupuscules d’extrême droite, puis par Jean-Marie Le Pen
avec les saillies qu’on connaît, comme à propos des chambres à gaz qui
auraient été un « détail » de l’histoire. Dans les profondeurs de
cette extrême droite, il y a toujours des réflexes antisémites, c’est
le milieu qui les produit avec le plus d’intensité et de constance.

À partir des années 2000, les choses ont changé. Pas au sens où un
« nouvel antisémitisme » aurait vu le jour. Simplement, les mêmes
préjugés, les mêmes mythes, voire la même haine ont été portés
par de nouveaux milieux. On peut schématiquement en distinguer trois.

D’une part, l’antisémitisme s’est développé au sein d’une partie
de la jeunesse arabo-musulmane des quartiers populaires. La situation en
Palestine s’est combinée avec une forme de jalousie sociale envers les
juifs, censés disposer de pouvoir et d’argent, de la part d’individus
défavorisés, discriminés, très contrôlés par la police.

Cet antisémitisme plutôt populaire s’est développé, d’autre part,
parmi les couches sociales peu politisées et peu syndiquées qui ont
fourni les rangs des « gilets jaunes » à partir de 2018.

Le mouvement ne s’est bien sûr pas réduit à cela, mais des expressions hostiles
aux juifs y ont bien été repérées à plusieurs reprises, de même
que des slogans problématiques, comme ceux associant Macron au banquier
Rothschild, qui n’auraient à mon avis jamais été formulés si Macron
avait travaillé pour le Crédit lyonnais.

Enfin, des expressions antisémites ont également été nombreuses
parmi les personnes qui se sont mobilisées contre les vaccins et/ou le
passe sanitaire, au moment de l’épidémie de Covid. Il s’agit de
la réactivation d’un vieux schème complotiste, les juifs ayant été
accusés d’avoir propagé la peste noire ! Là encore, cela n’a pas
caractérisé l’ensemble du mouvement, mais ce n’était pas anecdotique
et surtout cela s’est fait sans grande réaction dans les cortèges,
ce qui témoigne d’une sorte de « consentement à l’antisémitisme ».

Il peut y avoir des porosités entre ces milieux, parfois exposés aux
mêmes propagandistes de discours antisémites, comme Alain Soral ou
Dieudonné, qui ont touché des audiences considérables. Le moment de
fusion le plus significatif a été la manifestation « Jour de colère
» en janvier 2014 : il y avait des catholiques intégristes, des gens
qui faisaient la quenelle [geste popularisé par Dieudonné – ndlr],
on entendait des slogans comme « Juif, casse-toi ! »… La police a
compté 17 000 personnes ce jour-là.

Pourquoi la gauche a-t-elle manqué de vigilance, selon vous ?
======================================================================

Cet antisémitisme a été exprimé par des personnes dont elle peut prendre
la défense par ailleurs, parce qu’elles sont victimes de la crise sociale
ou de racisme.

Cette gêne n’a rien de nouveau.

En France, à la fin du
XIXe siècle, le mouvement ouvrier était imprégné d’antisémitisme
avant l’affaire Dreyfus, et beaucoup s’en accommodaient.

Jaurès, après
des réticences, est tombé du bon côté, et ensuite l’antisémitisme
est devenu, pour un temps, quelque chose d’« interdit » à gauche.

La question israélo-palestinienne a été une autre source de gêne et de
confusion, avec la colonisation et l’oppression dont ont été victimes
les Palestiniens, notamment en Cisjordanie, tandis qu’après 2000 le
Crif connaissait une évolution droitière. Elle a été bien incarnée
par Roger Cukierman, son président entre 2001 et 2007. J’observe que
son nouveau président, Yonathan Arfi, est plus prudent et plus clair
sur l’extrême droite.

À ce propos, beaucoup de débats se concentrent sur la question du sionisme et de l’antisionisme. Vous rappelez que le sionisme n’est pas un colonialisme classique : en quoi ?
=================================================================================================================================================================================

Je relève d’abord que, même parmi ceux qui assimilent Israël au
colonialisme classique, presque personne ne défend que les juifs venus
de Pologne et d’Allemagne doivent y retourner, de la même manière
que les pieds-noirs d’Algérie sont revenus en France.

De fait, une différence majeure avec le colonialisme tient à l’absence
de métropole, vers laquelle les acteurs d’un projet impérialiste
pourraient revenir.

Le projet sioniste était de trouver un refuge
----------------------------------------------------

**Le projet sioniste était de trouver un refuge**.

Avant
la Seconde Guerre mondiale, les alyas [l’acte d’immigration en
terre d’Israël – ndlr] étaient toutes liées à des violences
antisémites. Le grand mouvement palestinien de grève générale de 1936
contre l’occupation anglaise était aussi dirigé contre l’immigration
de juifs fuyant le nazisme. On est là au cœur du drame historique qui
s’est noué jusqu’à aujourd’hui.

Ensuite, évidemment, il y a eu la Shoah. Trois cent mille personnes
restaient déplacées en Europe en 1945, dont des gens qui n’avaient pas
envie de retourner là où ils avaient été dénoncés, quand ils n’ont
pas carrément été expulsés. Il faut rappeler qu’en Pologne, après
1945, des pogroms ont été menés contre des juifs qui avaient survécu
à la destruction planifiée par les nazis…

Depuis, une nouvelle nation
est née dans l’État d’Israël, créée d’après une résolution
de l’ONU – une nation d’ailleurs diverse, comme en témoignaient
les manifestations contre le gouvernement de Nétanyahou avant la guerre.

À partir de 1967, en revanche, s’est développé un phénomène colonial classique auquel il faut clairement mettre fin
=====================================================================================================================

**À partir de 1967, en revanche, s’est développé un phénomène colonial
classique auquel il faut clairement mettre fin**.

Mais il faut distinguer les deux phénomènes.

Si l’on prend au pied de la lettre le slogan «
Libérer la Palestine de la mer au Jourdain », expliquez-moi où vont
les juifs.


On n’a plus vraiment besoin de ce terme, **l’antisionisme**, qui est de fait devenu très ambigu dans ses implications
==========================================================================================================================

**On n’a plus vraiment besoin de ce terme, "l’antisionisme"**, qui est
de fait devenu très ambigu dans ses implications.


vous pointez que les glissements sont d’autant plus aisés que la signification de l’antisionisme ne peut plus être la même aujourd’hui qu’à la fin du XIXe siècle
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

::

    Dans la gauche radicale, on rappelle volontiers que l’antisionisme
    ne se confond pas avec l’antisémitisme. Vous en êtes d’accord,
    mais vous pointez que les glissements sont d’autant plus aisés que la
    signification de l’antisionisme ne peut plus être la même aujourd’hui
    qu’à la fin du XIXe siècle…

**L’assimilation entre antisionisme et antisémitisme relève d’une part
d’ignorance, mais aussi de manœuvres politiciennes**.

Cela étant dit, je ne crois pas, en effet, qu’on puisse revendiquer le terme de la même
façon que les nombreux milieux juifs qui ne pensaient pas que s’établir
en Palestine était une bonne solution.

**Je dis cela alors que j’aurais certainement été parmi eux si j’avais vécu
à cette époque !**


Aujourd’hui, un État et une nation existent
=================================================

Aujourd’hui, un État et une nation existent. Il faudrait que cette nation
soit beaucoup moins injuste qu’elle ne l’est. À cet égard, on peut
critiquer vigoureusement les politiques du gouvernement israélien.

Mais alors on n’a plus vraiment besoin de ce terme, l’antisionisme, qui
est de fait devenu très ambigu dans ses implications.


Dans nos colonnes , l’historien Enzo Traverso a parlé de « guerre génocidaire » la situation à Gaza lui donne-t-elle raison ?
=================================================================================================================================

::

    Dans nos colonnes , l’historien Enzo Traverso a parlé de « guerre
    génocidaire ». La formule illustre-t-elle des façons de parler
    d’Israël que vous dénonciez dans votre livre publié en 2022, ou la
    situation à Gaza lui donne-t-elle raison ?

La situation à Gaza correspond à une guerre meurtrière et abominable,
dont il faut exiger qu’elle s’arrête au plus vite.

Le génocide suppose une intention de détruire un groupe en tant que tel,
et je ne pense pas qu’on dispose d’éléments suffisants pour qualifier ainsi
la guerre actuellement menée par Israël.

Je suis donc plus prudent que lui sur le sujet, mais d’autres choses
me gênent chez Traverso. Il considère que les juifs sont globalement
passés du côté du conservatisme, mais il ne rend pas vraiment compte
des raisons qui ont mené à cet état de fait. En France, ces trente
dernières années, il se trouve que les principaux actes significatifs
de sympathie envers les juifs sont venus de la droite.

Le discours sur le Vél’ d’Hiv qu’a tenu Jacques Chirac, lorsqu’il
a reconnu la responsabilité de l’État français, aurait dû être
tenu par un président de gauche.

Puis il y a eu Nicolas Sarkozy,
avec des arrière-pensées antimusulmanes évidentes.


Et désormais, la principale figure de la gauche, Jean-Luc Mélenchon, apparaît comme un repoussoir
====================================================================================================

Et désormais,
la principale figure de la gauche, Jean-Luc Mélenchon, apparaît comme
un repoussoir.

À cause du manque d’empathie que j’ai mentionné,
et des clichés antisémites dans plusieurs de ses propos, que j’ai
recensés dans mon livre.

Vous appartenez à une génération, écrivez-vous, pour laquelle la mémoire de la Shoah a été un ressort de l’engagement pour transformer le monde. Ce ressort vous semble-t-il intact ?
===========================================================================================================================================================================================

Il n’est plus ce qu’il était.

Entre 1968 et aujourd’hui, deux fois
plus de temps s’est écoulé qu’entre la Seconde Guerre mondiale et
1968.

Beaucoup de jeunes juifs engagés avaient recueilli les récits
de leurs parents sur la Shoah, dont la spécificité doit nous prémunir
contre toute concurrence mémorielle de mauvais goût : il s’est agi de
la destruction d’un groupe, à éliminer comme un ensemble d’êtres
mauvais.

Cette histoire est sans doute mieux enseignée qu’après 1945,
mais constatons que cela n’empêche pas l’antisémitisme, et que la
disparition des témoins directs présente un défi supplémentaire.

Il faudrait cependant nuancer. Au sein du RAAR, il y a des gens âgés
comme moi – je suis né en 1951 – mais aussi des étudiants très
marqués par cette histoire.

Être juif, au-delà de la religion,
c’est avoir un rapport à une histoire d’oppression séculaire,
tout en ayant à l’esprit les apports des juifs aux pays dans lesquels
ils résidaient.

La chose douloureuse, pour ma génération, c’est que
malgré notre conscience aiguë de l’entreprise de destruction nazie,
nous avions l’impression, dans les années 1960 et 1980, que tout cela
était derrière nous.


.. _robert_hirsch_2023_11_18_conclusion:

Vous disiez dans votre livre que le glissement à droite de la communauté juive pouvait n’être que conjoncturel. Qu’est-ce qui peut renverser la tendance ?
===================================================================================================================================================================

J’étais peut-être plus optimiste qu’aujourd’hui…

On a vu qu’un certain nombre de partis de gauche ont compris le problème.

De même, nous avons eu des contacts avec la CGT : Sophie Binet est venue
à notre commémoration de la Nuit de cristal.

Il faudrait également des évolutions du côté de LFI.
