.. index::
   pair: Viols ; Hamas (2023-10-17)


.. _viols_2023_11_23:

======================================================================================================
2023-11-23 **Global women’s rights groups silent as Israeli women testify about rapes by Hamas**
======================================================================================================

- https://www.timesofisrael.com/global-womens-rights-groups-silent-as-israeli-women-testify-about-rapes-by-hamas/
- :ref:`pogroms`

Introduction
===============

Despite widespread evidence of terrorists’ systematic acts of sexual
brutality on Oct. 7, most worldwide organizations either have no comment
or bash IDF’s military campaign in Gaza


As the United Nations promotes an awareness campaign ahead of the International
Day for the Elimination of Violence Against Women on November 25, chilling
accounts from survivors and first responders who witnessed Hamas’s October 7
massacre of 1,200 Israelis paint a horrifying picture of systemic sexual
assaults perpetrated against women and girls of all ages.

One survivor of the Supernova music festival, where about 360 people were
slaughtered, described how she witnessed Hamas terrorists rape an
Israeli girl: “As I am hiding, I see in the corner of my eye that
[a terrorist] is raping her,” the witness recounted.

“They bent her over and I realized they were raping her and simply
passing her on to the next [terrorist].”

**These denials of the sexual abuse perpetrated by Hamas have far-reaching consequences**
============================================================================================

Yet many feminist and women’s rights organizations worldwide have
remained conspicuously silent — and some are even questioning the
veracity of the accusations.

These denials of the sexual abuse perpetrated by Hamas have far-reaching
consequences, including the deterrence of other sexual abuse victims
from seeking help.

Among others, the United Nations Entity for Gender Equality and the
Empowerment of Women (also known as UN-Women) released a statement on
October 13 equating the Hamas brutalities with Israel’s self-defense.

Likewise, the UN Committee on the Elimination of Discrimination Against Women (CEDAW)
**neglected to explicitly condemn Hamas’s atrocities.**


The international #MeToo movement completely failed to mention Hamas or the Israeli victims
================================================================================================

And the international #MeToo movement completely failed to mention Hamas
or the Israeli victims.

On Wednesday, Israeli women’s rights experts met with UN-Women for the
first time to advocate for official recognition of Hamas crimes against
women and children on October 7.

It marked the first meeting that the United Nations mission dedicated
to upholding the rights of women and children has held with Israeli
advocates since the Hamas onslaught.


Following the meeting with Israelis, the UN Security Council met in
New York on “The situation in the Middle East, including the Palestinian
question.”
There, UN-Women executive director Sima Bahous said she was “alarmed by
disturbing reports of gender-based and sexual violence.”

In her speech, which focused on the plight of women in Gaza and in the
Palestinian Authority, she also condemned Hamas’s crimes inside Israel
and promised they would be investigated.

But Bahous’s slim acknowledgment of reports of sexual abuse is unfortunately the exception
=============================================================================================

But Bahous’s slim acknowledgment of reports of sexual abuse is unfortunately
the exception, not the rule.

In one high-profile case of sexual abuse denial, on November 18 Samantha
Pearson, the former director of the University of Alberta’s sexual
violence center, was fired for endorsing an open letter that denied
Hamas terrorists had committed rape.

The letter criticized Canada’s New Democratic Party leader Jagmeet Singh
for repeating “the unverified accusation that Palestinians were guilty
of sexual violence.”

Orit Sulitzeanu called the failure of groups to condemn Hamas’s abuses against women a **betrayal**
======================================================================================================

Orit Sulitzeanu, executive director of the Association of Rape Crisis
Centers in Israel, called the failure of groups to condemn Hamas’s abuses
against women a betrayal.

“The very essence of gender equality and women empowerment groups worldwide
is to assist victims of such atrocities.
A pregnant woman was cut open and her unborn baby was shot. How could
anyone stay silent when faced with such horrific acts?” said Sulitzeanu
in a conversation with The Times of Israel.


Referring to Pearson’s signing the open letter, Sulitzeanu added that
“the denial of October 7 rapes by the head of the Rape Crisis Center at
the University of Alberta is unbelievable.”

“Denying the horrific sexual assaults, gang rapes, the sadistic acts of
abuse of children and women is simply incomprehensible,” Sulitzeanu said.

“That Pearson chose to take a political stance against the fundamental
principles of working with victims — to believe in what happened, understand
the difficulty of coming forward and testifying, and acknowledge that
the ability to speak up is a time-consuming process — is disheartening.”


Michal Herzog published an opinion expressing outrage and betrayal over the international  community’s failure to condemn the gender-based sexual violence perpetrated  by Hamas on October 7
===================================================================================================================================================================================================

- https://www.newsweek.com/silence-international-bodies-over-hamas-mass-rapes-betrayal-all-women-opinion-1845783

On Wednesday, Israeli First Lady Michal Herzog published an opinion
piece in Newsweek expressing **outrage and betrayal over the international
community’s failure to condemn the gender-based sexual violence perpetrated
by Hamas on October 7**.

“A Hamas video from a kibbutz shows terrorists torturing a pregnant woman
and removing her fetus.
Our forensic scientists have found bodies of women and girls raped with
such violence that their pelvic bones were broken,” wrote Herzog


Conclusion
============

To combat this, Tal Hochman,  government relations officer at the Israel
Women’s Network, which promotes gender equality in Israel, started a
petition supported by over 140 organizations demanding condemnation by
all UN bodies of the crimes committed against women on October 7.

Hochman told The Times of Israel that UN organizations’ inertia could
encourage Hamas to perpetrate further sexual crimes against the roughly
240 hostages being held in Gaza.

“UN Resolution 1325 specifies that women and children should receive
special protection in times of conflict or in captivity.

We need our feminist and human rights allies to condemn those crimes
and push for the release of women and children held in Gaza,” said Hochman.

She explained that clear condemnation might have prompted the UN to
send task forces to Israel to help document the gender-based violence,
collect evidence and help treat the victims.

Elkayam, of the Civil Commission gathering evidence of Hamas’s sexual
abuse, reiterated the betrayal she says all women felt from UN bodies’
failure to condemn Hamas’s crimes against women and young girls.

“Their silence is deafening,” Elyakam said.


History will judge their inability to express solidarity with victims  of these despicable crimes in a country that suffered its worst attack  since the Holocaust
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

**History will judge their inability to express solidarity with victims
of these despicable crimes in a country that suffered its worst attack
since the Holocaust.**
