.. index::
   pair: LDH ; 2023-11-10

.. _ldh_2023_11_10:

========================================================================================================================================================
2023-11-10 |ldh| Communiqué LDH **Non à l’antisémitisme, au racisme et aux semeurs de haine. La LDH appelle à manifester dimanche 12 novembre 2023**
========================================================================================================================================================

- https://www.ldh-france.org/
- https://www.ldh-france.org/non-a-lantisemitisme-au-racisme-et-aux-semeurs-de-haine-la-ldh-appelle-a-manifester-dimanche/
- https://www.ldh-france.org/wp-content/uploads/2023/11/CP-LDH-Manifestation-antisemitisme-10-11-2023.pdf

**La lutte contre l’antisémitisme a toujours été et reste pour la LDH
(Ligue des droits de l’Homme), créée lors de l’affaire Dreyfus, un
combat fondateur**.

Elle a manifesté sa condamnation de la montée des actes et propos
antisémites survenus depuis le 7 octobre 2023 et combattu toutes les
manifestations d’islamophobie et/ou de racisme quelles qu’en soient
les victimes.

**Fidèle à ses engagements, elle appelle à participer largement aux
manifestations du dimanche 12 novembre 2023 contre l’antisémitisme**.

**La LDH regrette profondément que cette mobilisation soit l’objet de
calculs et d’instrumentalisations**.

Elle s’inquiète de voir l’appel lancé par la Présidente de l’Assemblée
nationale et le Président du Sénat, adressé à toutes les citoyennes et
tous les citoyens de notre pays, faire l’objet **d’une récupération
politicienne particulièrement insupportable de la part des extrêmes droites**.

La lutte contre l’antisémitisme, contre tous les racismes doit se
poursuivre au-delà de dimanche 12 novembre 2023.

C’est pourquoi la LDH appelle les citoyennes et les citoyens, celles et
ceux qui vivent et travaillent en France, à se mobiliser au quotidien
en refusant toute discrimination et toute stratégie de division et de haine.

.. figure:: images/appel_ldh.png

  https://www.ldh-france.org/
