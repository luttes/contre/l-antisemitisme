.. index::
   pair: AACCE ; Contre la recrudescence en France de l’antisémitisme ! (2023-11-10)


.. _aacce_2023_11_10:

===========================================================================================================================================
**Communiqué de l'AACCE : appel à participer massivement à la marche contre l’antisémitisme du dimanche 12 novembre 2023 à Paris !**
===========================================================================================================================================

Communiqué de l'AACCE 10 Novembre 2023 Contre la recrudescence en France de l’antisémitisme !
===============================================================================================

- https://www.aacce.fr/2023/11/communique-de-l-aacce.html

Dans cette période où les actes antisémites se multiplient de façon
extrêmement inquiétante, **l’Association des Amis de la Commission Centrale
de l’Enfance (AACCE) appelle à participer massivement à la marche contre
l’antisémitisme du dimanche 12 novembre 2023 à Paris**.

**Nous manifesterons avec l’ensemble des forces progressistes et républicaines
en évitant toute confusion avec l’extrême droite**.

Cette marche partira à 15h de l’Esplanade des Invalides pour se terminer
au Palais du Luxembourg.


Départ, Esplanade des Invalides
=============================================================

- https://www.openstreetmap.org/#map=16/48.8597/2.3193


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3051118850708012%2C48.8539856815748%2C2.333436012268067%2C48.865462888195424&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8597/2.3193">Afficher une carte plus grande</a></small>



Arrivée, Palais du Luxembourg
=============================================================

- https://www.openstreetmap.org/#map=16/48.8493/2.3365



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.325067520141602%2C48.84360734401206%2C2.3533916473388676%2C48.85508692992307&amp;layer=mapnik"
     style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8493/2.3392">Afficher une carte plus grande</a></small>
