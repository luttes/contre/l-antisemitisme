.. index::
   pair: RAAR; 2023-11-11

.. _raar_2023_11_11:

======================================================================================================================================
2023-11-11 |raar| **Nous combattons l'#antisémitisme d'où qu'il vienne dc les tentatives de récupération #LFi ne trompent personne**
======================================================================================================================================

- :ref:`compte_rendu_cgt_2023_11_09`
- :ref:`RAAR <raar>`

#NuitdeCristal #GymnaseJapy

Message
===========

:ref:`Succès du rassemblement #NuitdeCristal <raar_2023_11_09>` #GymnaseJapy avec @98Memorial et
@Raar21, 600 personnes rassemblées contre l'#antisémitisme + ts les #racismes.

La lutte continue.

Nous combattons l'#antisémitisme d'où qu'il vienne dc les tentatives
de récupération #LFi ne trompent personne

2 rue Japy 75011 Paris
-----------------------------

.. figure:: images/gymnase_japy.png


|memorial98| Memorial 98 et RAAR |raar|
--------------------------------------------

.. figure:: images/memorial_98_et_raar.png


À l'appel du @RAAR2021 et du @98Memorial, 18h30 devant le gymnase Japy à Paris 2 Rue Japy
----------------------------------------------------------------------------------------------

A tout à l'heure 85e anniversaire #NuitdeCristal
On est là en solidarité avec victimes #Shoah+ génocides Arménien+ Tutsi,
Contre #antisémitisme et #racisme
À l'appel du @RAAR2021 et du @98Memorial, 18h30 devant le gymnase Japy à Paris 2 Rue Japy

.. figure:: images/devant_jappy.png


Le gymnase Japy (2 rue Japy 75011, métro Voltaire ou Charonne)
====================================================================

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.380398809909821%2C48.854755146111515%2C2.3839393258094788%2C48.856189918809484&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/48.85547/2.38217">Afficher une carte plus grande</a></small>
