.. index::
   pair: JJR; 2023-11-11
   pair: CJQ; 2023-11-11
   pair: ORAAJ; 2023-11-11

.. _jjr_2023_11_11:

====================================================================================================================================================================================================
2023-11-11 |jjr| CJQ / JJR / ORAAJ **Pour l'émancipation de toutes et tous, contre l'antisémitisme d'où qu'il vienne nous appelons à faire de la lutte contre l'antisémitisme un combat central**
====================================================================================================================================================================================================


- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/11/cjq-jjr-oraaj-pour-lemancipation-de-toutes-et-tous-contre-lantisemitisme-dou-quil-vienne-nous-appelons-a-faire-de-la-lutte-contre-lantisemitisme-un-combat-central/
- https://kolektiva.social/@jjr/111391938007905855
- :ref:`articles_oraaj_2023`


.. _lutte_centrale_2023_11_11:

**Pour l'émancipation de toutes et tous, contre l'antisémitisme d'où qu'il vienne nous appelons à faire de la lutte contre l'antisémitisme un combat central**
================================================================================================================================================================

Pour l'émancipation de toutes et tous, contre l'antisémitisme d'où qu'il
vienne nous appelons à faire de la lutte contre l'antisémitisme un combat central.

**Nous partageons les peines, les inquiétudes et la solitude que ressentent
les Juifs et Juives autour de nous**.

Nous savons à quel point les choses ont changé pour elles et eux depuis
le 7 octobre 2023, et les attaques antisémites perpétrées par le Hamas
en Israël.

Depuis ce jour, les actes antisémites dans le monde ont augmenté d'une
manière inédite depuis des décennies.

**Cela fait plus de vingt ans que l'antisémitisme augmente dans notre pays**.
Il n'avait jamais disparu.


.. _gauche_2023_11_11:

Face à cela, **les réactions de notre camp social n'ont pas toujours été à la hauteur**
==============================================================================================

- :ref:`raar_2023_10_15`
- :ref:`contre_antisemitisme_2023_04_24`

Face à cela, **les réactions de notre camp social n'ont pas toujours été à la hauteur**.

.. _fachos_2023_11_11:

**Nous ne laisserons pas l'extrême-droite du Rassemblement national, de Reconquête ou d'autres s'approprier cette lutte**
=============================================================================================================================

Nous ne laisserons pas l'extrême-droite du Rassemblement national, de
Reconquête ou d'autres s'approprier cette lutte.

**Ils sont nos ennemis, autant par leur passé ouvertement antisémite qu'ils
n'ont jamais renié, que par leur présent, toujours antisémite, xénophobe,
raciste et conspirationniste**.

On ne combat pas l'antisémitisme avec des racistes, on ne combat pas le
racisme avec des antisémites.
Les thèses du "grand remplacement" et du "lobby LGBT" que diffusent
l'extrême droite permettent de comprendre qu'on ne combat sérieusement
l'antisémitisme qu'en prenant également au sérieux le combat contre les
autres racismes, notamment l'islamophobie et contre les discriminations
de genre.


.. _macron_2023_11_11:

**Nous dénonçons tout autant l'hypocrisie des macronistes**
=============================================================

Nous dénonçons tout autant **l'hypocrisie des macronistes**, qui prétendent
lutter contre l'antisémitisme:

- **tout en portant un roman national antisémite (avec la réhabilitation des
  mesures antisémites de Napoléon et les commémorations de Pétain et de Maurras)**,
- en menant la casse sociale,
- en appliquant des politiques brutales contre les migrants et les migrantes
- et en faisant des lois islamophobes.

**En associant les Juives et les Juifs à leurs politiques xénophobes et
antisociales (qui attaquent aussi les Juives et les Juifs), ils nuisent
à la cause qu'ils proclament défendre**.

.. _pas_injonstions_2023_11_11:

Nous refusons aussi les injonctions à se positionner politiquement sur quelque sujet que ce soit
===================================================================================================

**Nous refusons aussi les injonctions à se positionner politiquement sur
quelque sujet que ce soit**, faites aux Juifs et aux Juives pour qu'iels
reçoivent un soutien.

**Depuis l'affaire Dreyfus, les choses sont entendues, nous défendrons
toutes les personnes qui subissent du racisme.**

L'antisémitisme, ce ne sont pas seulement des préjugés
===========================================================

**L'antisémitisme, ce ne sont pas seulement des préjugés**, dont certains
sont millénaires, c'est aussi une explication du monde, une vision de
la société dans laquelle une élite complote dans l'ombre contre la masse.
Dans certains récits, c'est une élite financière, un milliardaire ou un
banquier juif ; dans d'autres, c'est une élite médiatique, un patron de
presse juif ; dans d'autres encore c'est une élite intellectuelle qui
est en cause.

**Ces récits se retrouvent dans tout l'échiquier politique.**

**Ces explications manichéennes** du monde sont à la base de la pyramide des
violences antisémites, des brimades aux agressions et jusqu'aux meurtres.


**Pour celui qui a l'émancipation humaine comme horizon, l'antisémitisme révolte**
=======================================================================================

Pour celui qui a l'émancipation humaine comme horizon, l'antisémitisme
révolte. Car en plus d'apporter du malheur à une minorité, **il restreint
l'horizon de l'émancipation de toutes et tous**.

Nous appelons à l'unité et à la clarté contre le racisme, contre
l'antisémitisme, contre l'extrême droite et ses idées.

.. _rue_2023_11_11:

**Pour l'émancipation de toutes et tous ! Nous ne laisserons jamais la rue à la droite et l'extrême droite !**
================================================================================================================

**Pour l'émancipation de toutes et tous ! Nous ne laisserons jamais la rue
à la droite et l'extrême droite !**

**Nous appelons toutes celles et ceux qui refusent de laisser la rue aux
antisémites et aux racistes à leur faire comprendre clairement, dimanche
et tous les autres jours qu'iels ne sont pas les bienvenu·es lors de
cette marche du 12 novembre 2023 et qu’iels ne seront jamais les bienvenu·es
dans nos luttes.**


.. _perturber_2023_11_11:

**Nous appelons à perturber la parole et la présence des représentants politiques de l'extrême-droite**
==========================================================================================================

Nous appelons donc chacun et chacune à se rendre aux manifestations de
dimanche et à **perturber la parole et la présence des représentants
politiques de l'extrême-droite**, nous appelons les organisations de notre
camp social à investir les autres endroits de luttes contre l’extrême
droite et l’antisémitisme, demain et par la suite.

Soyons le plus nombreux.ses possible pour rappeler que la lutte contre
l'antisémitisme est une lutte dans laquelle la gauche doit prendre, et
parfois reprendre, toute sa place et qu'il est de notre responsabilité
d'en empêcher toute forme de manipulation par l'extrême-droite et tous
les racistes.


**Signatures**
=================

- :ref:`Collages Judéités Queer (CJQ) <cjq>`
- :ref:`Juives et Juifs Révolutionnaires (JJR) <jjr>`
- :ref:`Organisation Révolutionnaire Antiraciste Antipatriarcale Juive (ORAAJ) <oraaj>`
