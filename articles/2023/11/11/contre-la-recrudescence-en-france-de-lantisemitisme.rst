.. index::
   pair: UJRE ; 2023-11-11

.. _ujre_2023_11_11:

=========================================================================================================
2023-11-11 **Second** communiqué de l'UJRE **Contre la recrudescence en France de l’antisémitisme !**
=========================================================================================================


Texte
=======

Chers amis,

Vous le savez, l'Union des Juifs pour la Résistance et l'Entraide s'est
jointe à **l'appel initialement lancé par le PCF, le PS et EELV**.

Depuis, d'autres forces politiques de gauche s'y sont jointes telles que
Gauche Républicaine et Socialiste, Les Radicaux de Gauche, Le Mouvement
Républicain et Citoyen, La Nouvelle Gauche ainsi que la Ligue des Droits
de l'Homme, le Mouvement de la Paix ou encore Mémoire des Résistants Juifs
de la Main d'Œuvre Immigrée, l'Association des Amis de la Commission Centrale
de l'Enfance ainsi que des organisations de jeunesse telles que le
Mouvement des Jeunes Communistes de France, Les Jeunes Socialistes,
des syndicats étudiants...

**Tous participeront au cortège commun qui formera un cordon républicain
contre l’antisémitisme et tous les fauteurs de haine et de racisme**.

Alors que nous vivons une recrudescence d’actes antisémites, il est
important de participer à cette marche pour dire notre volonté de combattre
sans faiblesse l’antisémitisme.

Ce combat contre l’antisémitisme, pour la République, doit se faire
dans la clarté.

**C’est la raison pour laquelle nous affirmons que les forces d’extrême
droite n’y ont pas leur place**.

**Ne leur laissons pas cette place, soyons présents !**

Lieu de rendez-vous, Esplanade des Invalides
=================================================

.. figure:: images/lieu_rendez_vous.jpg


Lieu de rendez-vous, Esplanade des Invalides openstreetmap
---------------------------------------------------------------------

- https://www.openstreetmap.org/#map=16/48.8597/2.3193



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3051118850708012%2C48.8539856815748%2C2.333436012268067%2C48.865462888195424&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8597/2.3193">Afficher une carte plus grande</a></small>


Arrivée, Palais du Luxembourg
-----------------------------------

- https://www.openstreetmap.org/#map=16/48.8493/2.3365



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.325067520141602%2C48.84360734401206%2C2.3533916473388676%2C48.85508692992307&amp;layer=mapnik"
     style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8493/2.3392">Afficher une carte plus grande</a></small>
