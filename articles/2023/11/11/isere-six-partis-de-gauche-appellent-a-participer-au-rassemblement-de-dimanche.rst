.. index::
   pair: Isère; 2023-11-11

.. _isere_2023_11_11:

=====================================================================================================================
2023-11-11 **Isère. Six partis de gauche appellent à participer au rassemblement de dimanche 12 novembre 2023**
=====================================================================================================================

- https://travailleur-alpin.fr/2023/11/11/isere-six-partis-de-gauche-appellent-a-participer-au-rassemblement-de-dimanche/


Préambule
============

Six partis de gauche appellent à participer à la manifestation contre
l’antisémitisme dimanche 12 novembre 2023.

**Ils soulignent que l’extrême-droite n’aura pas sa place dans ce rassemblement**.

Le rassemblement aura lieu dimanche 12 novembre 2023 à 14h30,
place de Verdun à Grenoble.

« Les extrême-droites de Mme Le Pen et de M. Zemmour sont non seulement
les héritiers de ceux qui livrèrent des dizaines de milliers de juifs
aux camps de la mort, mais aujourd’hui encore, ils attisent les haines
et les divisions pour faire advenir leur projet ethno-racialiste.

La lutte contre l’antisémitisme et toutes les formes de racisme, est un
combat du quotidien et concerne chacune et chacun d’entre nous »,
écrivent les partis signataires de l’appel.


Le texte de l’appel
=========================

Depuis l’attaque terroriste du 7 octobre 2023 d’Israël par le Hamas et
l’offensive israélienne meurtrière sur la bande de Gaza qui a suivi,
les actes antisémites, racistes et xénophobes se sont fortement
accentués en Europe et notamment dans notre pays.

Une marche contre l’antisémitisme est organisée le dimanche 12 novembre 2023
à Paris.

Plusieurs députés et sénateurs de gauche et certains Renaissance de
l’Isère appellent à un rassemblement le même jour à Grenoble pour
manifester contre ces actes haineux.

**Nous, partis de gauche, nous joignons à cet appel à rassemblement en
étant clairs sur un élément essentiel : l’extrême-droite n’aura pas sa
place dans ces rassemblements**.

Alors que nous allons commémorer les 80 ans de la « Saint Barthelemy grenobloise (1) »,
l’Isère est, et restera une terre de Résistance.

Les extrême-droites de Mme Le Pen et de M. Zemmour sont non seulement
les héritiers de ceux qui livrèrent des dizaines de milliers de juifs
aux camps de la mort, mais aujourd’hui encore, ils attisent les haines
et les divisions pour faire advenir leur projet ethno-racialiste.

La lutte contre l’antisémitisme et toutes les formes de racisme, est un
combat du quotidien et concerne chacune et chacun d’entre nous.

Le conflit israélo-palestinien ne doit en aucun cas être l’outil
d’instrumentalisation politiciennes et mérite que la France appelle à
un cessez-le-feu immédiat dans ce conflit.

Ainsi, nous donnons rendez-vous à toutes les Iséroises et Isérois
républicains à se joindre à nous le :

Dimanche 12 novembre 2023
à 14h30 à la place de Verdun (Grenoble)


(1) Vague d’assassinats perpétrée entre le 25 et le 30 novembre 1943 par
les miliciens de Vichy alliés du régime nazi.
Vingt et un résistants seront tués, cinq déportés.
