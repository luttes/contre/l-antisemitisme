
.. _autres_lieux_2023_11_11:

====================================================================
2023-11-11 **Autres appels à manifester le 12 novembre 2023**
====================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/2023/11/11/autres-appels-a-manifester-le-12-novembre/


Annonce JJR **nous appelons celles et ceux qui y participeraient à être vigilant·es quant à toute minorisation, justification ou relativisation de l’antisémitisme**
===========================================================================================================================================================================

Pour celles et ceux qui ne souhaitent pas se rendre à la grande manifestation
de demain, cette possibilité existe.

.. danger:: **Comme pour l’autre rassemblement, nous appelons celles et ceux qui y
   participeraient à être vigilant·es quant à toute minorisation,
   justification ou relativisation de l’antisémitisme**.

Également nos camarades d’ORAAJ ont fixé un rendez-vous à 12h30 place Bourbon



.. _oraaj_2023_11_11:

2023-11-11 ORAAJ **Appel à rassemblement contre l'antisémitisme d'où qu'il vienne** le dimanche 12 novembre 2023 à 12h30 place Bourbon
============================================================================================================================================

- :ref:`oraaj`
- https://www.instagram.com/p/CzgxLVkNust/?utm_source=ig_web_copy_link&igshid=MzRlODBiNWFlZA==
- :ref:`articles_oraaj_2023`

Tract ORAAJ pour la manifestion du 12 novembre 2023 **rassemblement contre l'antisémitisme d'où qu'il vienne**
----------------------------------------------------------------------------------------------------------------

Nous assistons à une **augmentation terrible d'actes et discours antisémites
ces dernières semaines**.

Ces faits ne sont pas des phénomènes isolés, ils ne sortent pas de nulle part.

Ils sont l'expression de **l'antisémitisme, racisme strucurel** qui fonctionne
sur une vision du monde selon laquelle les juifves auraient une capacité
de s'associer entre eux pour **prendre le pouvoir et assoir une domination**.

**Ils seraient donc un danger pour les nations**.

Pour se débarrasser de cette menace que représenteraient les juifves, les
antisémites veulent faire  disparaitre "les traîtres”, les juifves, et
ceci depuis 3000 ans, par, tour à tour, des exactions, des accusations
de sorcellerie, des lois interdisant le culte, des expulsions, des
pogroms, des ghettoïsations, la Shoah, des meurtres plus récemment,
jusqu'à la stigmatisation et aux fantasmes sur leurs liens avec Israël.

L'antisémitisme d'aujourd'hui est le fruit d'un long processus de
racialisation, et est nourri par le contexte géopolitique.

L'antisémitisme s'articule à d'autres racismes, et notamment l'islamophobie.
Ces deux racismes, dans leur mise en concurrence, peuvent fonctionner
ensemble et s'alimenter, et c'est exactement ce à quoi nous assistons
aujourd’hui.

**C'est ce que fait le RN quand il tente de s'emparer de la lutte contre
l'antisémitisme, tout en étant historiquement et actuellement, raciste,
antisémite, et antiféministe**.

La lutte contre l'antisémitisme doit être une lutte pour l'émancipation
et doit donc s’accompagner d’une lutte contre la discrimination et
contre l'acharnement contre les immigré.es.

**Dans une perspective de lutte pour l'émancipation** de toutes et tous,
il ne peut s'associer à la politique migratoire du gouvernement, ni à sa
politique fémonationaliste, et **plus largement à la droite, qui a toujours
été traversée par toutes formes de racismes**.

L'antisémitisme fonctionne par des **rhétoriques complotistes**, par
l'association des juifves à la finance, et **se développe de cette manière
dans tous les discours populistes**.

C'est notre devoir de déjouer partout ces rhétoriques **d’où qu'elles
s'expriment**, et même lorsqu'elles s'insinuent dans notre camp politique
de la gauche et dans les mouvements sociaux auxquels nous participons !

Il est urgent de ne plus laisser penser l'antisémitisme comme un
problème collatéral, ou de nier son existence actuelle.

La lutte contre l'antisémitisme doit enfin être portée par les organisations
féministes antiracistes, anticapitalistes et décoloniales.

**Elle doit être l'apanage de la gauche, et il est grand temps qu’enfin,
elle le redevienne !**

:ref:`Oraaj`


Place du palais Bourbon
---------------------------

- https://www.openstreetmap.org/#map=18/48.86028/2.31853


.. _autres_2023_11_11:

Appel des organisations de jeunesse : Union Etudiante, Union Syndicale Lycéenne, Les jeunes générations, Les Jeunes Insoumis, NPA, JOC
============================================================================================================================================

**Dimanche 12 novembre 2023 11h devant le monument commémoratif de la rafle du Vel d'Hiv**


.. figure:: images/autres_affiche_1.png
   :width: 400


Signatures
------------

.. figure:: images/signatures.png
   :width: 300


.. danger:: **Comme pour l’autre rassemblement, nous appelons celles et ceux qui y
   participeraient à être vigilant·es quant à toute minorisation,
   justification ou relativisation de l’antisémitisme**.


Square de la place des Martyrs Juifs du Vélodrom d'Hiver
-----------------------------------------------------------

- https://www.openstreetmap.org/#map=19/48.85385/2.28700

Compte-rendu JJR
-------------------

- :ref:`jjr_ras_2023_11_14`
