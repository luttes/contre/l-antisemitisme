.. index::
   pair: RAAR ; 2023-11-11

.. https://web.archive.org/web/20231112100227/https://luttes.frama.io/contre/l-antisemitisme/articles/2023/11/11/raar-dehors-lextreme-droite.html

.. _raar_2023_11_11_dehors:

=======================================================================================================
2023-11-11 |raar| **Contre l’antisémitisme ! Contre tous les racismes ! Dehors l’extrême-droite !**
=======================================================================================================

- https://raar.info/2023/11/dehors-lextreme-droite/
- https://web.archive.org/web/20231112101902/https://raar.info/2023/11/dehors-lextreme-droite/


**Le RAAR vous appelle à à participer nombreux-ses aux marches contre les actes antisémites**
================================================================================================

**Le RAAR vous appelle à à participer nombreux-ses aux marches contre
les actes antisémites**.

**Ne laissons pas la rue à l’extrême-droite**.

Ce sont eux qui doivent rester à la maison, pas nous.

Nous porterons aussi des exigences contre tous les racismes et les lois
discriminatoires et nous dirons haut et fort  «Dehors l’extrême droite! »

Le RN et Zemmour n’ont rien à faire dans cette marche, comme l’explique
notre déclaration ci-dessous

**A Paris nous donnons rdv le 12 novembre 2023 à 14H aux Invalides devant l’aérogare autour de la banderole du RAAR**
=========================================================================================================================

A Paris nous donnons rdv le 12 novembre 2023 à 14H aux Invalides devant
l’aérogare autour de la banderole du RAAR

**Nous sommes rassemblés aujourd’hui contre l’antisémitisme**.

Parce qu’il y a eu, depuis le 7 octobre 2023, plus de 1000 actes antisémites,
soit deux fois plus que durant toute l’année 2022.

**Parce que les Juifs et Juives de ce pays se sentent menacé.es**.

Et, depuis des années, ils et elles ont des raisons de l’être.
On se souvient des assassinats de personnes tuées parce que juives.

Nous sommes présent.es aujourd’hui parce que nous luttons également
contre tous les racismes.

**C’est pourquoi nous protestons contre la suppression de l’Aide Médicale
d’Etat (AME) et contre les autres dispositions du projets de loi Darmanin
que votent ou se préparent à voter les majorités dont sont issu.es les
organisateur/trices de cette manifestation**.

**Nous sommes présent.es également pour dire notre refus de la présence  d’une extrême droite qui n’a jamais cessé de diffuser des campagnes de  haine contre les Juifs et les Musulmans**
===============================================================================================================================================================================================

Nous sommes présent.es également pour dire notre refus de la présence
d’une extrême droite qui n’a jamais cessé de diffuser des campagnes de
haine contre les Juifs et les Musulmans.

Ainsi le président du RN, Jordan Bardella, vient de réaƯirmer que le
fondateur de son parti, Jean-Marie Le Pen, n’était pas antisémite, alors
qu’il a été condamné à six reprises pour ses propos nauséabonds, notamment
pour l’aƯirmation maintes fois répétée que les chambres à gaz nazies se
réduisaient à «un détail de l’histoire».

**Ces individus n’ont rien à faire dans une manifestation contre
l’antisémitisme**.

Le combat contre l’antisémitisme est à mener en lien avec celui contre
tous les racismes.

Les organisations syndicales, les associations, toute la gauche et le
camp des progressistes doivent s’unir pour le poursuivre, en mobilisant
largement la population.

Il en va de notre avenir commun et de la préservation du vivre ensemble.

Solidairement, le 11 novembre 2023

Le :ref:`RAAR <raar>`

**Esplanade des Invalides** => **Palais du Luxembourg**
=============================================================


**Esplanade des Invalides**
------------------------------

- https://www.openstreetmap.org/#map=16/48.8597/2.3193



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.3051118850708012%2C48.8539856815748%2C2.333436012268067%2C48.865462888195424&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8597/2.3193">Afficher une carte plus grande</a></small>


**Palais du Luxembourg**
-----------------------------------

- https://www.openstreetmap.org/#map=16/48.8493/2.3365



.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.325067520141602%2C48.84360734401206%2C2.3533916473388676%2C48.85508692992307&amp;layer=mapnik"
     style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=16/48.8493/2.3392">Afficher une carte plus grande</a></small>
