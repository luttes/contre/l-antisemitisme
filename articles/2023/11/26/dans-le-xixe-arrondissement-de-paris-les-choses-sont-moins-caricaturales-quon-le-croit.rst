

.. _graulle_2023_11_26:

======================================================================================================================================
2023-11-26 **Dans le XIXe arrondissement de Paris, « les choses sont moins caricaturales qu’on le croit »** par Pauline Graulle
======================================================================================================================================

Préambule
===========

Loin du tumulte médiatique, un calme précaire règne depuis le 7 octobre 2023
dans ce quartier cosmopolite où vit l’une des plus grandes communautés
juives d’Europe. Un climat qui s’explique aussi par la grande prudence des
responsables politiques locaux, qui veillent à ne pas souffler sur les braises.

HorsHors de question d’ôter sa kippa. « Tant que la police fait son travail,
on est en sécurité en France », souffle Michaël, affairé à empiler
des cagettes de fruits sur le porte-bagage de son scooter. Sous la bruine de
ce jeudi 23 novembre, le marché de Joinville, niché entre le bassin de la
Villette et une synagogue Loubavitch, bat son plein.

« Le problème, c’est les médias : ils montent les gens les uns contre
==========================================================================

« Le problème, c’est les médias : ils montent les gens les uns contre les
autres », poursuit Michaël qui s’inquiète en revanche pour ses deux fils,
l’un au front à Gaza, l’autre à Jénine. « Mais ici, ça se passe comme
d’habitude », assure celui qui n’a pas trouvé utile de participer à la
marche contre l’antisémitisme du 12 novembre.

Michaël n’est pas le seul à le dire. Des habitants du quartier aux élus
locaux, tout le monde s’en étonnerait presque : depuis l’attaque terroriste
du Hamas en Israël, le calme règne dans le XIXe arrondissement de Paris.

À entendre certains politiques et médias d’extrême droite, qui attisent depuis
quatre semaines la haine intercommunautaire, cet arrondissement aurait pourtant
tout de la poudrière.

Accueillant 70 synagogues et abritant l’une des plus grandes communautés juives d’Europe – quelque 40 000 personnes sur 185 000 habitants
=============================================================================================================================================

Accueillant 70 synagogues et abritant l’une des plus
grandes communautés juives d’Europe – quelque 40 000 personnes sur 185 000
habitants –, il héberge aussi une importante communauté musulmane et une
jeunesse potentiellement éruptive, parfois mêlée à des rixes entre quartiers.

Mais si la sécurité aux abords des écoles et des lieux de culte a
été renforcée, aucun incident majeur n’a encore été relevé par les
autorités.

Rien à voir avec l’ambiance post-deuxième intifada, au début
des années 2000, quand la température était montée d’un cran dans ce
melting pot du nord-est de la capitale.

**Sur les murs de la place des Fêtes, les affiches des otages sont restées
intactes**.

Rien à signaler non plus sur la dizaine d’œuvres de Yad Vashem sur la libération
accrochées aux grilles du parc des Buttes-Chaumont.

« La communauté juive est inquiète, mais il n’y a pas eu de vrais actes
antisémites, confirme Salomon, commerçant dans le secteur casher et habitant de
la rue Manin depuis quarante ans. Bien sûr, il suffit d’une étincelle pour
allumer le feu car les braises sont là. Mais pour l’instant, ça tient. »

Venue faire ses courses au marché de Joinville, une dame portant le voile
indique avoir « envoyé un message à tous [ses] voisins juifs ». « Si on
n’est pas là quand nos amis ne vont pas bien, à quoi ça sert ? », dit-elle.

À côté d’elle, Yael Lerer, candidate de la Nouvelle Union populaire
écologique et sociale (Nupes) aux dernières élections législatives dans
la circonscription des Français vivant en Israël, savoure la scène :
« Il faut arrêter de croire qu’il y aurait un antisémitisme “arabo-musulman”
généralisé, dit celle qui habite le XIXe depuis plusieurs années.

Regardez, tout se passe bien ici comme, à ma connaissance, à Aubervilliers ou
à Bobigny. Je ne nie pas qu’il y ait une peur authentique ressentie par
certains juifs, et je peux le comprendre car elle est sans cesse entretenue par
le monde médiatico-politique. Mais les choses sont infiniment plus complexes. »

La Franco-Israélienne, militante de longue date pour l’égalité et la
justice en Israël et Palestine, fait défiler une série de photos sur son
smartphone prises le jour de Roch Hachanah.

On y voit une ribambelle d’hommes en tenue traditionnelle qui, au milieu des
badauds bobos du quartier, jettent symboliquement leurs péchés dans l’eau du
canal de l’Ourcq.
« À Tel-Aviv, ce serait mal vu, pas ici », affirme Yael Lerer.

Est-ce du fait de l’organisation urbaine de l’arrondissement, où les
quartiers s’emboîtent en petits îlots ? Du fait de l’histoire de ce
quartier où les vagues d’immigration se sont sédimentées au fil des
siècles, formant aujourd’hui un puzzle culturel et religieux hétérogène
**(127 nationalités en présence)** ?

Toujours est-il que « pour l’instant,
il ne se passe pas grand-chose », observe Mahor Chiche, adjoint socialiste à
la mémoire et aux commerces à la mairie, qui met aussi ce calme précaire
sur le compte des « stratégies d’évitement : les gens ne veulent pas
casser le vivre-ensemble fragile qui fonctionne ici, alors ils évitent de
trop parler du conflit ».

Pour Léa Filoche, élue Génération·s de l’arrondissement au conseil de
Paris, la raison de cette relative quiétude serait également à chercher
dans l’histoire récente. Notamment dans l’affaire de la « filière
des Buttes-Chaumont », ce groupe djihadiste basé dans le XIXe, qui fut la
couveuse idéologique des frères Kouachi, auteurs de l’attentat contre
Charlie Hebdo. « Nous sommes sur une terre difficile, mais le traumatisme
de 2015 a donné lieu à une introspection générale et la mairie a mené
un gros travail sur la tolérance avec les centres sociaux, les clubs de
prévention et les associations de quartier », explique l’adjointe aux
solidarités d’Anne Hidalgo.

Par ailleurs, souligne-t-elle, « depuis le 7
octobre, les élus locaux se sont montrés responsables. La gauche est restée
soudée. Globalement, il n’y a pas eu d’instrumentalisation du conflit ».

Quelques rares incidents politiques
=========================================

Avec deux Insoumises comme députées du territoire, l'histoire aurait pourtant
pu s’écrire autrement.

D’autant que si la circonscription de Sarah Legrain recouvre la majeure partie
de l’arrondissement, celle de Danièle Obono, vilipendée pour ses propos sur
le Hamas, chevauche la partie la plus populaire du XIXe – de Stalingrad à la
Porte d’Aubervilliers, en passant par la rue Curial.

Mais à l’inverse d’autres villes à forte communauté juive,
à l'instar de Sarcelles (Val-d’Oise) où le contexte politique est tendu,
tout le monde fait preuve ici d’une certaine pondération.

À commencer par le maire socialiste de l’arrondissement, François Dagnaud
– joint par Mediapart, il n’a pas trouvé le temps de répondre à nos
questions –, dont la conduite précautionneuse semble faire l’unanimité,
de sa majorité plurielle (communistes, écologistes et Génération·s)
à l’opposition municipale.

Durant une journée, des photos d’otages ont
été projetées sur l’hôtel de ville, place Armand-Carrel, mais le drapeau
d’Israël n’a pas été accroché au fronton. L’édile a aussi refusé
les prises de parole après la minute de silence observée lors du premier
conseil municipal organisé après le 7 octobre 2023. Pas question de prendre le
moindre risque.

Hadrien Bortot, dirigeant de la section PCF du XIXe arrondissement

	En ce moment, on est tous un peu prudents.


Une attitude jugée « raisonnable et intelligente » par Marie Toubiana,
élue Les Républicains (LR) du XIXe, aussi bien que par l’élu écologiste
Andréas Pilartz, qui salue « la stratégie de tempérance du maire ».

« En ce moment, on est tous un peu prudents », reconnaît Hadrien Bortot, dirigeant
de la section PCF du secteur, qui souligne que « dans le XIXe, les choses sont
moins caricaturales qu’on le croit, y compris sur la question palestinienne ».

Il précise : « Non seulement, on y trouve une jeunesse issue de l’immigration
qui demande à avoir des informations sur Gaza, mais même chez les juifs, la
population est composite : il y a aussi bien des juifs originaires d’Afrique du
Nord, qui votent parfois pour Zemmour, que des Ashkénazes sécularisés très à
gauche et des ultra-orthodoxes qui se détournent de la politique française…

**En réalité, la problématique de l’arrondissement est moins l’identité
que la précarité et la misère, qui touchent toutes les communautés. »**

Il y a quinze jours, les communistes, dont le local jouxte une librairie
orthodoxe, ont distribué des tracts sur la Palestine place des Fêtes.

Pas d’anicroches. La dernière réunion du comité Palestine de l’arrondissement,
à l’initiative de plusieurs organisations politiques, syndicales et
associatives, a d’ailleurs fait le plein.

Certes, il y a eu cette mésaventure, le lendemain des attaques du Hamas.

Venus tracter sur le marché de Joinville, des militants du Nouveau Parti
anticapitaliste (NPA), qui avait publié la veille un communiqué apportant
son soutien « aux moyens de luttes [que les Palestiniens] ont choisi[s] pour
résister », ont été violemment pris à partie par deux hommes.
« Des gens d’extrême droite », raconte Danièle Obono, présente ce jour-là,
qui a aussitôt envoyé un courrier au maire.

Pour la députée, qui note que les fauteurs de trouble ont été expulsés
du marché par les habitants, l’incident est loin d’être révélateur
d’une bronca généralisée.

« Oui, certaines personnes m’interpellent,
mais on discute et on sort rapidement de la caricature », relate ainsi celle
qui a récemment quitté en plein direct le plateau d’I24 après avoir vu
son parti qualifié d’« antisémite » par un chroniqueur.

« Dans la vraie vie, les gens sont moins virulents que sur les réseaux
sociaux et à la télé. Ils savent faire la part des choses », veut croire
Danièle Obono. « Les jours qui ont suivi l’altercation à Joinville, on
a fait des collages, des diffusions, on n’a pas eu de problème.
Il faut dire que vu ce qu’il se passe là-bas, l’opinion évolue », ajoute Alex,
militant du NPA dans l’arrondissement.

La gauche locale n’est toutefois pas à l’abri de quelques turbulences.

À la veille de la manifestation contre l’antisémitisme, lors de la cérémonie
du 11 Novembre 2023, Mahor Chiche a ainsi ostensiblement tourné le dos quand
Danièle Obono et Sarah Legrain ont déposé leur gerbe de fleurs.

« Ce n’était pas dirigé contre les députées, c’était un acte spontané de
protestation contre la position de LFI qui a refusé de qualifier le Hamas de
groupe terroriste puis d’aller à la marche contre l’antisémitisme »,
avance **l’adjoint au maire, qui « salue » en revanche la présence de
Clémentine Autain et de François Ruffin lors du rassemblement à Strasbourg**.

Quoiqu’il affirme avoir ainsi voulu faire écho à un « émoi partagé par des
anciens combattants et des habitants », le geste a été diversement apprécié
dans la classe politique locale.

« C’est une question de valeurs, justifie le socialiste.

**LFI n’a pas eu la moindre empathie pour les victimes du 7 octobre 2023, je ne comprends
toujours pas pourquoi. »**

Des questions sur la position de LFI
============================================

Une incompréhension teintée de colère que ressent lui aussi Ludovic.

Le jeune homme, qui travaille à Lucien-de-Hirsch, l’un des plus importants
groupes scolaires confessionnels juifs en Europe (plus de 1 000 élèves),
parcourt la rue Petit ce soir de novembre, chapeau noir sur la tête et tsitsits
dépassant de la veste.

« Pour les juifs de gauche, il y a un avant et un après le 7 octobre 2023»,
estime celui qui se classe « très à gauche ».

« On n’a pas entendu de parole forte de Mélenchon sur l’antisémitisme.

LFI a eu une position bancale inexcusable. »

« Les Insoumis provoquent le rejet, abonde Salomon, le commerçant cité plus tôt.

Quant à François Dagnaud, il a été trop hésitant, de peur de froisser son électorat.

Quand je fais le tour des synagogues, je constate que la communauté juive ne
se sent pas représentée politiquement dans le XIXe. »

Auprès de Mediapart, la conseillère de Paris LR Marie Toubiana est plus
virulente encore : « Sarah Legrain a été élue avec une très forte
d’abstention [un peu plus de 50 % – ndlr] et je suis persuadée que si
les législatives avaient lieu aujourd’hui, elle perdrait largement car,
cette fois, la communauté juive se mobiliserait contre elle. »


Élue dès le premier tour sur la circonscription en juin 2022 grâce à
l’alliance de la Nupes, après une première tentative infructueuse en
2017, Sarah Legrain affiche pour sa part une certaine tranquillité.

Certes, elle constate un peu plus de mails « d’interpellation » ces dernières
semaines.

« Il y a de l’incertitude, de l’incompréhension, on peut
me demander pourquoi LFI ne soutient pas davantage Israël ou pourquoi nous
n’avons pas voulu employer le mot “terroriste” concernant le Hamas.

Mais certains habitants que je croise me disent aussi de “tenir bon”.
Et je n’ai pas eu de remarques sur le supposé antisémitisme de LFI », affirme
celle **qui s’est toutefois gardée de relayer le tweet polémique de Jean-Luc
Mélenchon où il fustigeait la marche du 12 novembre 2023**.

Il n’empêche que la jeune députée ne s’est pas privée pas de critiquer
ouvertement la manifestation où s’est rendue l’extrême droite.

Et relaie abondamment les appels au cessez-le-feu et les informations sur le drame des
Palestiniens bombardés par Tsahal. « Je ne me suis pas spécialement exposée
durant la période car la politique étrangère n’est pas ma spécialité,
mais je n’ai pas dévié de la ligne de mon mouvement », insiste-t-elle.

Le 9 novembre 2023, Sarah Legrain s’est rendue à la :ref:`commémoration des 85 ans de la
Nuit de cristal <nuit_de_cristal_2023_11_09>` en compagnie notamment de Clémence Guetté, élue à Créteil
(Val-de-Marne), où vit également une importante communauté juive.

Mardi dernier, lors d’une AG du groupe local du XIXe, elle recevait le collectif
Tsedek regroupant des juifs décoloniaux. Pour elle, une chose est sûre :
« Les habitants et moi, nous ne voulons qu’une chose : la paix. »

Pauline Graulle
