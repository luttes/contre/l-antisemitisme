
.. _kezzof_2023_11_26:

===============================================================================================
2023-11-26 **Face à la drague du RN, la communauté juive s’interroge** par Youmni Kezzouf
===============================================================================================

- https://www.mediapart.fr/journal/politique/261123/face-la-drague-du-rn-la-communaute-juive-s-interroge
- https://www.mediapart.fr/biographie/youmni-kezzouf-0


Préambule
===========

Si les institutions juives continuent de considérer le Rassemblement national
comme un adversaire politique, le soutien affiché du parti de Marine Le Pen
à Israël et son entreprise de « dédiabolisation » séduisent certains membres
de la communauté

Depuis le 7 octobre 2023, les cadres du Rassemblement national (RN) répètent
la formule à longueur d’interview : le parti d’extrême droite, cofondé
par un ancien Waffen-SS et présidé pendant trente-huit ans par Jean-Marie
Le Pen, serait « un bouclier pour les juifs de France ».

L’expression n’est certes pas nouvelle – Marine Le Pen l’avait déjà utilisée dans
un entretien à Valeurs actuelles en 2014, en désignant « le seul vrai ennemi:
le fondamentalisme islamique » – mais elle est désormais martelée sur
toutes les antennes pour mieux imprégner les esprits.


Louis Aliot, maire de Perpignan (Pyrénées-Orientales), avait théorisé comme le dernier « verrou idéologique » empêchant « les gens » de voter pour lui : l’antisémitisme
====================================================================================================================================================================================

Fort de 88 député·es à l’Assemblée nationale, mais aussi d’une
vice-présidence dans le groupe d’étude sur l’antisémitisme et d’une
autre dans le groupe d’amitié France-Israël, le RN œuvre quotidiennement à
faire sauter ce que Louis Aliot, maire de Perpignan (Pyrénées-Orientales),
avait théorisé comme **le dernier « verrou idéologique » empêchant «
les gens » de voter pour lui : l’antisémitisme**.

Une entreprise qui passe aussi par un soutien très affirmé à la politique
d’Israël, à rebours de certaines positions historiques du parti.

La rhétorique du RN pour s’adresser à la communauté juive reprend
des méthodes largement éprouvées : jouer sur les peurs et l’émotion,
particulièrement fortes depuis les attaques du 7 octobre 2023 et la montée des
actes antisémites en France. Judith Cohen Solal, qui a coécrit avec Jonathan
Hayoun *La Main du diable. Comment l’extrême droite a voulu séduire les
juifs de France* (Grasset, 2019), en analyse la réception : « Il y a des gens
dans la communauté juive qui sont pris dans la séduction de Marine Le Pen,
parce que ce discours a l’air magique, **il désigne l’ennemi**. »

La psychanalyste insiste sur l’importance de ces marques de soutien
======================================================================

La psychanalyste insiste sur l’importance de ces marques de soutien dans
un moment de profonde angoisse : « Ce qui ressort, c’est la sensation
d’extrême solitude des Français juifs, cette angoisse perpétuelle et cette
soif de voir qui va être à nos côtés.

Dans ce moment-là, pour atténuer l’inquiétude, certains ont minimisé le danger
que représente l’extrême droite, pour la France mais aussi pour les juifs plus
spécifiquement. »

Lors de la marche contre l’antisémitisme, un collectif de militants juifs de gauche, Golem, s’est par exemple fermement opposé à la présence du parti d’extrême droite
================================================================================================================================================================================

Ce n’est évidemment pas le cas de tous les membres de la communauté juive,
diverse et hétérogène. Lors de la marche contre l’antisémitisme, un
collectif de militants juifs de gauche, Golem, s’est par exemple fermement
opposé à la présence du parti d’extrême droite.

Alain* et Francis*, commerçants juifs rencontrés à Sarcelles (Val-d’Oise),
où vit une importante communauté juive, confirment ce sentiment de peur : « En
ce moment, on se sent vulnérables, on est contents de trouver un peu de soutien,
peu importe d’où il vient. »

Marine Le Pen avait été la première à parler de « pogrom » à l’Assemblée nationale
pour désigner les attaques du Hamas contre Israël.

Patrick Haddad, maire socialiste de la commune francilienne, a
d’ailleurs constaté un changement de discours parmi ses administré·es.
«Dans la communauté juive locale, Marine Le Pen n’apparaît plus comme un
danger, dit-il. Elle n’est pas encore une force d’attraction, mais elle
n’est plus un sujet, il y a une sensibilité à la dédiabolisation du RN. »

Alain abonde : « Sur la question de l’antisémitisme, depuis le 7 octobre,
je considère que le RN tient la route. » Son ami Francis et lui assurent
cependant ne pas être dupes face à ce qu’ils perçoivent comme une manœuvre
électorale : « C’est de la politique, chacun essaye de tirer la couverture
vers lui. C’est facile de parler à des gens en détresse comme nous,
mais si ce n’est pas sincère, on s’en rendra compte. »

Surtout, les deux amis confient leur « profonde déception » à l’égard de la gauche
----------------------------------------------------------------------------------------

Surtout, les deux amis confient leur « profonde déception » à l’égard de la gauche,
focalisant leurs critiques sur le chef de file de La France insoumise (LFI),
Jean-Luc Mélenchon.

Des positions individuelles qui évoluent
===============================================

Selon le documentariste et essayiste Jonathan Hayoun, également ancien
président de l’Union des étudiants juifs de France (UEJF), « il y a dans
le discours ambiant un désir de raconter l’histoire d’une extrême gauche
de plus en plus antisémite, et en même temps de désigner une évolution
positive de l’extrême droite, parce que ça rassure tout le monde de se dire
qu’un parti qui arrive plusieurs fois au second tour de la présidentielle
évolue positivement et se débarrasse d’un des éléments centraux de sa
construction et de son existence : l’antisémitisme ».

Lors de la marche contre l’antisémitisme organisée à Paris, le 12
novembre 2023, par les présidents des deux chambres du Parlement, plusieurs dizaines
d’élu·es RN ont défilé, en fin de cortège, accompagné·es par des membres
de la Ligue de défense juive (LDJ).

Ce groupuscule nationaliste aux méthodes
musclées entretient depuis les années 2000 des liens avec le Front national
(FN), dirigé à l’époque par Jean-Marie Le Pen, qui frayait parallèlement
avec des figures telles qu’Alain Soral et Dieudonné, condamnées ensuite
pour des propos antisémites.

Jean-Claude Nataf, un des cofondateurs de la LDJ
--------------------------------------------------------

Jean-Claude Nataf, un des cofondateurs de la LDJ, a également côtoyé
Philippe Péninque, ancien président du Groupe union défense (GUD) et
cofondateur d’Égalité et réconciliation, devenu un temps l’éminence
grise de Marine Le Pen.

Les deux hommes étaient notamment apparus ensemble au 1er Mai du FN, en 2013.
En 2014, Jean-Claude Nataf déclarait à Libération :
« Le FN n’a pas notre sympathie, mais pourquoi détourner les gens du danger
réel ? Aujourd'hui, 100 % de nos agresseurs viennent de la “diversité”».

Quatre ans plus tard, quelques dizaines de ses membres étaient présents
aux côtés du service de sécurité du RN pour protéger Marine Le Pen, huée
par la foule pendant la marche en hommage à Mireille Knoll.
Contactée par Mediapart, la LDJ n’a pas répondu.

Serge Klarsfeld occupe une place singulière
-------------------------------------------------

Si les institutions juives, à commencer par le Conseil représentatif
des institutions juives de France (Crif), ont dénoncé la présence
du RN dans la manifestation du 12 novembre, plusieurs personnalités
s’en sont satisfaites. Parmi celles-ci, Serge Klarsfeld occupe une place
singulière. L’historien et avocat, figure de la traque des criminels nazis
avec sa compagne Beate, a trouvé « tout à fait positive » la venue du RN.

Une position personnelle qui s’inscrit dans l’évolution de son discours à
l’égard du parti d’extrême droite, qu’il qualifiait encore en 2016,
dans La Main du diable, de « parti attrape-tout qui pratique la démagogie
envers tous les groupes sociaux, qui veut attraper les juifs pour atteindre
une certaine respectabilité ».

Quatre ans plus tard, Serge Klarsfeld avait accepté d’être décoré par
Louis Aliot, provoquant de nombreux débats au sein des institutions juives.

Il assumait alors, lors d’une rencontre organisée par le Crif, vouloir saluer
les évolutions positives du parti sur la question de l’antisémitisme : «
C’est une manière de combattre le RN que de l’aligner en partie sur nos
valeurs, s’était-il défendu. Je salue chaque pas en avant. Quand Marine Le
Pen condamne la rafle du Vél’ d’Hiv’, c’est tout de même mieux que
si elle s’alignait sur les thèses de Faurisson. » Pour les institutions,
une ligne claire


Du côté des institutions juives, on insiste fortement sur une ligne qui ne dévie pas : le RN est toujours un ennemi politique
---------------------------------------------------------------------------------------------------------------------------------

Du côté des institutions juives, on insiste fortement sur une ligne qui ne
dévie pas : le RN est toujours un ennemi politique, et les prises de paroles
qui remettent cela en question ne sont que des positions individuelles. « Les
institutions juives tiennent la ligne, affirme à Mediapart Yonathan Arfi,
président du Crif.

**On n’invite pas le RN ni Zemmour. Le monde juif reste un verrou pour Le Pen. »**

Samuel Lejoyeux, président de l’Union des étudiants juifs de France (UEJF),
abonde : **« Le verrou saute partout, mais pas du côté des institutions
juives**.

On devrait saluer ça. Les institutions juives ont été beaucoup plus
claires que Larcher et Braun-Pivet sur le RN et sa place dans la marche contre
l’antisémitisme. » Pour lui, on ne peut, dans ces conditions, parler d’une
extrême-droitisation de la communauté juive en France.

Il concède toutefois
que la posture du RN depuis le 7 octobre rend la lutte contre l’extrême
droite plus ardue : « Le vrai sujet, c’est de continuer à expliquer aux
gens que l’extrême droite, qui a un discours très en soutien, reste le
mal absolu. Le travail de pédagogie sur le danger du RN est plus difficile. »

Dans les urnes, analyser un supposé « vote juif » est impossible, comme il
est illusoire de vouloir corréler mécaniquement appartenance religieuse et
comportement électoral. En 2014, une enquête avait observé les résultats
électoraux dans les bureaux de vote du quartier de Sarcelles où réside
une importante communauté juive. Les données montrent, outre un important
soutien à Nicolas Sarkozy, que le vote en faveur de Marine Le Pen progresse,
mais dans les mêmes proportions que dans la commune entière, avec des scores
qui restent faibles. Si l’on poursuit l’observations de ces cinq bureaux
sur les scrutins suivants, on voit que le score du RN était en moyenne de 2,7 %
au premier tour en 2007 et qu’il est passé à 7,3 % en 2022.

Dans le quartier de la « petite Jérusalem » de Sarcelles
--------------------------------------------------------------

Dans le quartier de la **« petite Jérusalem » de Sarcelles**, Marine Le Pen n’a
donc pas réalisé de scores importants lors de l’élection présidentielle de
2022.

Pour autant, l’extrême droite n’était pas absente des urnes. Éric
Zemmour a obtenu de très bons scores, jusqu’à 39 % des suffrages exprimés
dans ces bureaux, bien au-delà de son score national (7 %), tout comme en
Israël, où la participation était en revanche extrêmement faible (10 %).

Des résultats qui font dire au maire de Sarcelles, Patrick Haddad, qu’Éric
Zemmour « est devenu l’extrême droite acceptable pour la communauté juive,
puisque juif lui aussi ».

Pour lui, « le zemmourisme est une traduction
française, à peu de choses près, de la vision du monde de Nétanyahou».

Samuel Lejoyeux, président de l’UEJF, s’inquiète de la diffusion
des idées xénophobes du patron de Reconquête dans la communauté juive
française : « Zemmour a profité d’un réflexe de vote communautaire et
représente une porte d’entrée idéologique vers l’extrême droite.
Il a pu faire infuser des idées, faire sauter quelques verrous. »

Au second tour de la présidentielle, Éric Zemmour a appelé ses électeurs et
électrices à voter Marine Le Pen.

Une consigne de vote qui n’a pas été suivie mécaniquement dans les bureaux de la
« petite Jérusalem ».

« Son appel à voter Marine Le Pen n’a pas marché, les gens ne sont pas passés de
l’autre côté », se satisfait Patrick Haddad, tout en s’inquiétant des
prochaines échéances électorales et du moment où un candidat ne portant
pas le nom Le Pen se présentera sous l’étiquette RN.

Pour l’heure, Alain et Francis restent sur leurs gardes : « Le RN a plein d’idées
qui ne nous conviennent pas, qui sont dangereuses, pour nous comme pour les
musulmans.
**S’ils sont élus, ce sont les juifs et les musulmans qui vont prendre. »**

Youmni Kezzouf
