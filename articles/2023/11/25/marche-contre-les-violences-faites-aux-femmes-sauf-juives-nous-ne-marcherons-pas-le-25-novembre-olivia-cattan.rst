.. index::
   pair: Olivia Cattan ; Nous ne marcherons pas le 25 novembre 2023 (2023-11-25)

.. _cattan_2023_11_25:

================================================================================================================================================
2023-11-25 **Marche contre les violences faites aux femmes… sauf juives : « Nous ne marcherons pas le 25 novembre 2023 »** par Olivia Cattan
================================================================================================================================================

- https://atlantico.fr/article/decryptage/marche-contre-les-violences-faites-aux-femmes-sauf-juives-nous-ne-marcherons-pas-le-25-novembre-olivia-cattan
- :ref:`raar_2023:communique_2023_11_24`

#violencesSexistes #feminisme #manifestation #Feminicides #FeminicidesAntisemites #Pogrom7octobre2023
#Massacre7octobre2023 #RAAR #jewdiverse #mazeldon
#manifestation #25novembre #25novembre2023

.. figure:: images/parole_de_femmes.jpg
   :width: 500

Olivia Cattan est écrivaine, journaliste, présidente de Paroles de Femmes.

Présentation d'Olivia Cattan
==============================

- https://fr.wikipedia.org/wiki/Olivia_Cattan
- https://www.oliviacattan.fr/qui-suis-je/
- https://sosfemmes.com/archives_bulletin_info/pdf/Charte_Paroles_de_Femmes.pdf

Je m’appelle Olivia Cattan, je suis juive et féministe, présidente (d'honneur [NDLR]
de l’association Paroles de femmes qui lutte contre toutes les violences faites
aux femmes.

Certains me décrivent comme une passionaria, une activiste mais je préfère
me voir comme une infatigable militante idéaliste qui pense que rien n’est
impossible.

Le 7 octobre 2023
===================

Le 7 octobre 2023, alors que je me trouvais en Israël, j’ai été saisie de
douleur, sidérée par le massacre de civils, hommes, femmes, et enfants.

Alors que le soleil brillait encore, des sirènes retentissaient et les chaines
d’infos nous parlaient d’intrusion et de tueries sans précédent.

Certaines personnes avaient été prises en otages, d’autres avaient été tués dans
d’ignobles conditions.
Plus je découvrais les images filmées par le Hamas, plus je pleurais.

Les corps des femmes étaient dénudés, exhibés, couverts
de sang, la culotte en bas des pieds.

Leurs viols me sautaient au visage comme une déflagration me ramenant à ce que
j’avais vécu dans ma chair.

Les informations se succédaient sans que la spécificité des meurtres de femmes
soient précisée.
Les filles les plus jolies étaient emmenées, les plus âgées ou handicapées, tuées.
Les femmes semblaient être considérées comme un butin de guerre.
Elles étaient traitées comme une de simples marchandises, tirées par le bras
ou les cheveux, par des hommes en furie.

Alors, j’ai eu envie d’écrire comme dans un instinct de survie pour
adoucir ma peur et évacuer mon angoisse. J’ai écrit un premier texte qui
est devenu une pétition publiée dans Libération. Je l’ai envoyée à des
amis artistes par crainte que les paroles de ces femmes soient étouffées par
le bruit de la guerre. Je leur ai demandé de signer ces mots pour partager
ma souffrance. La plupart ont signé. Plus de 19 000 signatures et des noms
prestigieux de journalistes comme Laurence Ferrari, Alba Ventura et Anne
Sinclair, des comédiennes comme Muriel Robin, Julie Gayet, Charlotte Gainsbourg
et Isabelle Carré, des personnalités politiques comme Sandrine Rousseau ou
Anne Hidalgo, des députés, des sénatrices.

D’autres ont été effrayés par le sujet parce qu’Israël n’est pas un pays comme
les autres, et qu’ils craignaient de subir des répercussions sur leurs carrières,
voire sur leurs vies.

**Puis j’ai envoyé mon texte à de nombreux collectifs et associations féministes. Mais aucune d’elles n’a répondu présentes**
===============================================================================================================================

**Puis j’ai envoyé mon texte à de nombreux collectifs et associations
féministes. Mais aucune d’elles n’a répondu présentes**.

Pire, certaines d’entre elles ont écrit une tribune sur le conflit sans même
parler de ces femmes, les réduisant à une simple ligne dans leur tribune.

Alors j’ai expliqué, raconté, transmis des images mais rien. Juste le silence, la
solitude et l’effroi.
**Ces féministes continuaient de se taire ou d’avoir
peur pendant que d’autres m’insultaient**.

**Les victimes ne semblaient pas être mortes au bon endroit, et tout simplement
niées parce que juives**.

Comme la marche du 25 novembre 2023 arrivait, je demandais à quelques féministes de
nous accueillir dans le cortège.

Nous voulions porter des panneaux d’otages et de femmes tuées en Israël dont
certaines étaient françaises.
Mais leur peur fut la seule réponse qu’elles me donnèrent. Elles voulaient protéger
les femmes et les enfants qui allaient défiler. Elles voulaient surtout nous
protéger de la haine qui pourrait nous atteindre physiquement et moralement.

« Urgence Palestine » défilait et nous n’étions pas en sécurité comme
si parler des féminicides en Israël empêchait d’autres féministes de
parler des civiles palestiniennes.

La mort des femmes juives, chrétiennes, musulmanes était évacuée par ces collectifs
comme si ce n’était qu’un détail dans l’histoire du conflit israélo-palestinien.


Je décidais pour la première fois de ne pas marcher le 25 novembre, non je ne marcherais pas !
=================================================================================================

Alors je n’avais plus le choix et je décidais pour la première fois de ne
pas marcher le 25 novembre, non je ne marcherais pas !

Pourtant, j'avais usé les pavés parisiens par mes cris féministes. "A bas le
patriarcat" "Mon corps est à moi" "Mon corps, mon choix". Mais aujourd’hui,
la rue féministe n'était désormais plus à moi, ces jeunes collectifs me
l'avaient fait comprendre en me traitant de « blanche », de « bourgeoise »,
de sioniste « d’extrême-droite ! ».

Moi, la brune, fille de forain, qu'on surnommait à l'école « la noiraude
» à cause de son teint olive, de juive arabe, et de ses cheveux noirs.

Moi, la mère isolée qui avais élevé sa fille toute seule, prenant le premier
métro chaque matin pour partir travailler dans une usine de ciment.

Moi qui ne mangeais pas à ma faim et qui vivais sous les toits dans une chambre de
bonne sans toilettes ni douche.
Moi qui avais toujours voté à gauche et marché pour l’union entre les fils d’Abraham.

En 2006, je distribuais déjà des tracts dans la rue avec des militantes en
rêvant de changer le sort des femmes dans mon pays.
J'avais subi une tentative de viol, des violences de mon ex-compagnon, et je
créée Paroles de femmes dans un besoin de réparation, de la mienne et de celles des autres.

J'ouvrais ma porte à toutes celles qui venaient frapper à mon cœur
======================================================================

J'ouvrais ma porte à toutes celles qui venaient frapper à mon cœur sans
me préoccuper jamais de leur couleur de peau, de leur religion ou de leur
genre. Je les accueillais parfois même à dormir chez moi.

Être ou devenir une femme était un parcours long et douloureux.
Je ne le savais que trop bien. Ne pas se faire agresser, ne pas se faire abuser, ne
pas se faire exploiter, chercher à tout prix à montrer qu'on est l'égale de
l’homme.
Travailler d’arrache-pied pour un moindre salaire tout en mettant
au monde les futures générations. Des vies en une seule toujours à courir
après le temps, après la reconnaissance de son mari ou de son patron.

Je les trouvais belles et courageuses les femmes, dignes et parfois
si résilientes avec une force peu commune qui leur donnait un air de
Résistantes. Je les aimais et voulais défendre leurs droits, en faisant
entendre leurs paroles brisées que l’on tentait d’étouffer. J’essayais
de révolutionner le monde en leur offrant un meilleur avenir, en obtenant
toujours plus d'équité et de justice pour elles. Je pensais que nous étions
toutes là, les unes pour les autres, damnées de la terre d'être nées avec
un vagin et une paire de seins.

Mais aujourd'hui je suis orpheline de mes sœurs de combat
==============================================================

Le féminisme n'a jamais été pour moi un concept politique, intellectuel et
philosophique, un fonds de commerce, un tremplin pour monter les échelons,
le féminisme a toujours été une question de survie.

**Mais aujourd'hui je suis orpheline de mes sœurs de combat**.

« Intersectionnalité », « racisée ». Quels autres mots allez-vous donc
encore inventer pour continuer à nous diviser, entre noires et blanches,
juives-chrétiennes et musulmanes, riches et pauvres, gauche et droite.
Pourquoi créer des catégories de femmes en vous persuadant que vous avez
souffert plus que moi ?


Allons-nous être triées en début de cortège, le 25 novembre 2023, pour savoir qui est digne d'être une femme ?
====================================================================================================================

Allons-nous être triées en début de cortège, le 25 novembre 2023, pour savoir
qui est digne d'être une femme ? Digne de manifester à vous côtés, selon
vos critères ? C'est ce que vous semblez faire comme au temps de l’apartheid
lorsque toutes les couleurs et tous les êtres ne se valaient pas.

Vous dénoncez les discriminations, mais vous discriminez à votre tour, nous
empêchant de venir à la manifestation, sous prétexte que nous n’aurions
pas le droit de porter les panneaux des femmes françaises et israéliennes
mortes le 7 octobre 2023 ou en otages ?

Je n’avais qu’un seul espoir, que d'autres voix féministes s’émancipent
et s'élèvent enfin pour affirmer, haut et fort, qu'elles soutenaient notre
lutte. Mais ces associations, qui nous soutiennent sans le dire publiquement,
se censurent d’elles-mêmes, dominées par le terrorisme intellectuel que
ces collectifs exercent sur elles, dominées par la violence et la haine de
leurs diatribes.


Finalement ces collectifs n’ont rien à envier aux hommes. Leur volonté de domination est la même
===================================================================================================

Finalement ces collectifs n’ont rien à envier aux hommes. Leur volonté de
domination est la même.


**Je ne marcherais jamais avec des intégristes de la pensée, des femmes qui agissent comme des mâles dominants**
===================================================================================================================

Je renonce donc avec tristesse mais avec détermination et conviction à
cette marche de lutte contre les violences faites aux femmes parce que **je ne
marcherais jamais avec des intégristes de la pensée, des femmes qui agissent
comme des mâles dominants**, des femmes qui discriminent d’autres femmes.

**Toute ma vie je fus assignée à mon genre et à ma condition sociale, je ne
pensais pas l'être un jour à mon identité**.
