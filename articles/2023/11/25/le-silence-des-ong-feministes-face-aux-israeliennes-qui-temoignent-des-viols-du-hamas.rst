.. index::
   pair: Amélie Abitbol; Le silence des ONG féministes face aux Israéliennes qui témoignent des viols du Hamas (2023-11-25)

.. _abitbol_2023_11_25:

===========================================================================================================================
2023-11-25 **Le silence des ONG féministes face aux Israéliennes qui témoignent des viols du Hamas** par Amélie Abitbol
===========================================================================================================================

- https://fr.timesofisrael.com/le-silence-des-ong-feministes-face-aux-israeliennes-qui-temoignent-des-viols-du-hamas/
- https://fr.timesofisrael.com/writers/amelie-botbol/
- :ref:`raar_2023:communique_2023_11_24`


Le silence des ONG féministes face aux Israéliennes qui témoignent des viols du Hamas
=========================================================================================

En dépit des preuves des brutalités sexuelles commises par les terroristes,
le 7 octobre 2023, la plupart des organisations ne disent rien, si ce n’est critiquer
la riposte militaire d’Israël.

Au moment-même où les Nations unies font la promotion d’une campagne de
sensibilisation à l’occasion de la Journée internationale pour la fin
des violences faites aux femmes, ce 25 novembre, les récits glaçants de
rescapées et premiers secours témoins du massacre de 1 200 Israéliens par
le groupe terroriste palestinien du Hamas, le 7 octobre dernier, brossent
un tableau terrifiant des agressions sexuelles systémiques perpétrées
contre les femmes et filles de tous âges.

Une rescapée du Festival Supernova, où plus de 360 personnes ont été
massacrées, dit avoir vu des terroristes du Hamas violer une jeune fille
israélienne : « Quand j’étais cachée, j’ai vu du coin de l’œil
un terroriste en train de la violer », raconte la témoin. « Ils l’ont
penchée et j’ai réalisé qu’ils la violaient et se la passaient l’un
après l’autre. »

Malgré tout, nombreuses sont les organisations féministes et de défense
des droits des femmes, partout dans le monde, à rester étonnamment
silencieuses – quand elles ne remettent pas en question la véracité
de ces accusations. **Ces dénégations des abus sexuels perpétrés par le
Hamas ont de graves conséquences, car elles pourraient dissuader d’autres
victimes d’abus sexuels de demander de l’aide**.

Par exemple, l’ONU Femmes (UN Women) – l’entité des Nations Unies
consacrée à l’égalité des sexes et l’autonomisation des femmes – a
publié une déclaration, le 13 octobre 2023, assimilant les brutalités
du Hamas à l’autodéfense d’Israël.

De la même manière, le Comité
de l’ONU pour l’élimination des discriminations envers les femmes
(CEDAW) s’est abstenu de condamner explicitement les atrocités commises
par le Hamas. Et le mouvement #MeToo international a complètement omis de
mentionner le Hamas et les victimes israéliennes.

Mercredi, pour la première fois, des experts israéliens des droits des
femmes se sont entretenus avec des représentants d’UN Women pour plaider
en faveur de la reconnaissance officielle des crimes commis par le Hamas
contre les femmes et les enfants, le 7 octobre dernier. Il s’agissait de
la première réunion de la mission des Nations unies chargée de la défense
des droits des femmes et des enfants avec des défenseurs israéliens depuis
l’attaque du groupe terroriste palestinien.

Suite à cette rencontre, le Conseil de sécurité de l’ONU s’est réuni à
New York pour évoquer « la situation au Moyen-Orient, notamment la question
palestinienne ». La directrice exécutive d’UN Women, Sima Bahous, s’est
dite « alarmée par les informations inquiétantes faisant état de violences
sexistes et sexuelles ». Dans son discours, qui s’est concentré sur le sort
des femmes à Gaza et dans l’Autorité palestinienne (AP), elle a également
condamné les crimes du Hamas en Israël et promis qu’ils feraient l’objet
d’une enquête.

Mais cette timide reconnaissance par Bahous des informations d’abus sexuels est
malheureusement l’exception, et non la règle. Dans un cas très médiatisé
de déni d’abus sexuels, le 18 novembre dernier, Samantha Pearson, alors
directrice du centre de lutte contre les violences sexuelles de l’Université
de l’Alberta, a été congédiée pour avoir approuvé une lettre ouverte
niant que des terroristes du Hamas avaient commis des viols. La lettre s’en
prend au chef du Parti national démocratique (NDP) de centre-gauche, Jagmeet
Singh, pour avoir repris « l’accusation non vérifiée selon laquelle des
Palestiniens étaient coupables de violences sexuelles ».

Orit Sulitzeanu, directrice exécutive de l’Association des centres d’aide
aux victimes de viol en Israël, qualifie de trahison l’absence de condamnation
des exactions du Hamas envers les femmes de la part de ces organisations.

« La raison d’être des organisations de défense de l’égalité des sexes
et de l’émancipation des femmes dans le monde est d’aider les victimes
de telles atrocités. Une femme enceinte a été éventrée et son bébé à
naître abattu. Comment rester silencieux face à des actes aussi odieux ? »,
a déclaré Sulitzeanu lors d’une conversation avec le Times of Israël.

« Le déni des viols du 7 octobre par le directeur du Rape Crisis Center de
l’Université de l’Alberta est à peine incroyable », a ajouté Sulitzeanu
à propos de la lettre ouverte signée par Pearson.

« La négation des effroyables agressions sexuelles, viols collectifs et
actes sadiques perpétrés sur des enfants et des femmes est tout bonnement
incompréhensible. Il est affligeant que Pearson ait choisi d’adopter
une position politique allant à l’encontre des principes fondamentaux
du travail avec les victimes, à savoir croire en ce qui s’est passé,
comprendre la difficulté que les victimes ont à se manifester et témoigner
et enfin reconnaître que la capacité à s’exprimer est un processus qui
prend du temps », a ajouté Sulitzeanu.

Mercredi, la Première dame d’Israël, Michal Herzog, a publié un éditorial
dans Newsweek pour dire son indignation et son sentiment de trahison face
à l’absence de condamnation, de la part de la communauté internationale,
des violences sexuelles de genre perpétrées par le Hamas le 7 octobre.

Une vidéo du groupe terroriste palestinien filmée dans un kibboutz montre des
terroristes en train de torturer une femme enceinte et de lui retirer son fœtus.

Nos médecins légistes ont trouvé des corps de femmes et de filles violées
avec une telle violence que leurs os pelviens ont été brisés », a écrit
la Première dame.

« Ceux d’entre nous qui ont eu la malchance de voir des preuves vidéo
diffusées par les terroristes eux-mêmes ont vu le corps d’une femme nue
exhibé à travers Gaza, et une autre, encore vivante, dans un pantalon
ensanglanté, retenue captive sous la menace d’une arme, être tirée
par les cheveux dans une jeep. Ces preuves, ainsi que les aveux explicites
enregistrés des terroristes capturés, montrent très clairement que le viol
de masse était une partie préméditée du plan du Hamas », a-t-elle souligné.

Un grand nombre de femmes et d’enfants figurent parmi les 240 otages détenus
par le groupe terroriste du Hamas dans la bande de Gaza, a écrit Herzog,
ajoutant que « ce n’est que lorsqu’ils seront libérés que nous saurons
ce qu’ils ont enduré ».

Mme Herzog a critiqué le silence des organisations internationales
======================================================================

La collecte de preuves matérielles d’agressions sexuelles a été difficile
parce que la zone dans laquelle les massacres ont eu lieu est restée une
zone de guerre active pendant des jours, alors que les kits de viol doivent
être collectés dans un délai de 48 heures. En outre, de nombreux cadavres
de victimes de viol étaient trop abîmés pour permettre la collecte de
preuves matérielles.

En dépit de ces difficultés, la police israélienne mène l’enquête
et constitue plusieurs dossiers d’agression sexuelle à l’encontre des
terroristes du Hamas dans le but de les poursuivre. Les autorités affirment
avoir recueilli des preuves vidéo, des photographies des corps des victimes et
des témoignages de terroristes confirmant les récits d’agressions sexuelles.

Israël a récemment écrit sur son compte officiel X : « Le 2 novembre,
un glossaire de translittération arabe-hébreu appartenant au Hamas a été
découvert en Israël avec une terminologie sexuelle, notamment ‘enlève
ton pantalon’. Ces preuves laissent entendre que les terroristes du Hamas
devaient violer systématiquement les femmes israéliennes. »

Le choix délibéré de ne pas soutenir les femmes israéliennes
================================================================

La campagne d’UN Women du 25 novembre demande aux gouvernements du monde
entier de dire de quelle manière ils agissent pour éradiquer les violences
de genre. Mais la déclaration de cette organisation en date du 13 octobre
ne faisait mention ni du Hamas ni de ses crimes contre l’humanité, et se
contente simplement de dire que « UN Women condamne les attaques contre les
civils en Israël et dans les Territoires palestiniens occupés ».

Jusqu’à mercredi, Bahous – qui était diplomate jordanienne avant de
devenir directrice exécutive d’UN Women – n’avait pas non plus fait
mention du massacre ou du ciblage des femmes israéliennes, comme dans son tweet
du 8 octobre, mais s’en était prise à plusieurs reprises à Israël pour
son incursion visant à chasser le Hamas du pouvoir. « Nous condamnons les
frappes contre le camp de réfugiés #Jabalia, tous les camps de réfugiés et
les infrastructures civiles. Ces bombardements ininterrompus, qui occasionnent
des destructions terribles et des pertes en vies humaines, ne laissent aucun
refuge aux habitants de #Gaza, en premier lieu les femmes et les enfants »,
a-t elle souligné dans son tweet du 3 novembre.

Le CEDAW n’a pas non plus condamné explicitement les atrocités commises
par le Hamas, alors qu’il avait dénoncé les attaques systématiques et les
violences sexuelles de l’État islamique (EI) contre les femmes yézidies en
Irak, les violences de Boko Haram contre les femmes nigérianes et le ciblage
des femmes et des filles rohingyas au Myanmar.

Contacté par le Times of Israël, le CEDAW a confirmé qu’il « n’a pas
adopté en tant que tel de déclaration sur les attaques terroristes du 7
octobre 2023 ».


**Le sentiment d'Hanna Assouline : la honte**
===============================================

- :ref:`raar_2024:hanna_2024_01_28`
