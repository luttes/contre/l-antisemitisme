.. index::
   pair: Fabien Escola; Extrême droite : le jour où les repères ont fini de disparaître

.. _escola_2023_11_13:

====================================================================================================
2023-11-13 **Extrême droite : le jour où les repères ont fini de disparaître** par Fabien Escola
====================================================================================================

- https://www.mediapart.fr/biographie/fabien-escalona
- https://www.mediapart.fr/journal/politique/131123/extreme-droite-le-jour-ou-les-reperes-ont-fini-de-disparaitre


Que l’extrême droite ait pu parader dans une manifestation contre
l’antisémitisme est une étape choquante de sa normalisation. Les failles
de la gauche ne sauraient justifier la complaisance de toute une partie
du champ politique et médiatique à l’endroit d’une force dangereuse.

Ce sont des images que l’on aurait encore pensé improbables il y
a quelques années, et qui cristallisent le brouillage des valeurs à
l’œuvre dans la vie politique française. À l’occasion de marches
contre l’antisémitisme organisées ce dimanche, on a ainsi pu voir en
tête de cortège à Nice la figure de l’extrême droite identitaire
Philippe Vardon. Aujourd’hui conseiller municipal de cette ville au nom
du parti Reconquête, l’homme a aussi pour caractéristique peu commune
d’avoir interprété, voici vingt ans, des chants néonazis au milieu
de skinheads au bras tendu.

Le même jour à Paris, quoique reléguée en fin de manifestation, Marine
Le Pen savourait sa présence, saluée de quelques applaudissements et
signe de la normalisation qu’elle poursuit depuis de nombreuses années
à la tête du Rassemblement national (RN). Non loin, Éric Zemmour en
profitait pour se livrer, devant les micros, à une énième diatribe
contre « l’immigration venue des contrées musulmanes ». Comble d’une
ironie désolante, tout ce petit monde xénophobe s’était initialement
donné rendez-vous place Salvador-Allende dans la capitale.

Disons-le d’emblée : les manifestations de dimanche valaient bien mieux
que ces présences paradoxales. Le grand nombre de personnes réunies,
leur relative diversité, et une ambiance générale empreinte de gravité,
se voulaient une réponse digne à l’envolée des actes d’hostilité
envers les juifs et juives de France. Ceux-ci n’ont jamais été aussi
nombreux « dans un temps aussi court ces dernières décennies »,
soulignait récemment la chercheuse Nonna Mayer. Les rassemblements
organisés ont été les plus massifs depuis ceux de 1990, à la suite
de la profanation du cimetière de Carpentras.

Cependant, on retiendra aussi de la journée du 12 novembre 2023 qu’un
cap a été franchi dans la dissolution des repères qui subsistaient dans
notre vie politique, et donnaient un contenu démocratique à l’idée de
« front républicain ». Pour le dire vite, droite et gauche convenaient
qu’au-delà de leurs différences, elles avaient en partage un certain
nombre de principes hérités des Lumières, incorporés à notre histoire
constitutionnelle, et justifiant une politique de « cordon sanitaire
» à l’égard de l’extrême droite, porteuse d’une idéologie de
l’inégalité naturelle.

On savait déjà que si le tabou des alliances électorales tient encore,
le « barrage » dans les urnes est davantage moqué que défendu –
sans quoi le RN n’aurait pas obtenu 89 député·es à l’Assemblée
nationale. On savait également que les thèmes de l’extrême droite
ont volontiers été cooptés par des partis rivaux, que sa dangerosité
spécifique a été perdue de vue, et que sa normalisation institutionnelle
et médiatique s’est dramatiquement accentuée ces dernières années.

Mais si sa parade aux manifestations de dimanche est si choquante, c’est
qu’elle ne s’effectue pas en dépit de ce qu’elle est, mais bien
sur les enjeux mêmes qui justifiaient son rejet et sa mise à l’écart.

Même dans un monde fictif où le RN aurait répudié ses racines
antisémites, le problème de sa participation au défilé de dimanche
resterait entier.

On n’en serait évidemment pas là sans l’incapacité des gauches et
de la « société civile » à avoir lancé elles-mêmes une marche de
ce type. Mais on paie aussi plusieurs années de confusionnisme politique,
entretenu au plus haut sommet de l’État et prolongé par les initiateurs
de la marche qui a eu lieu, à savoir le président (LR) du Sénat, Gérard
Larcher, et la présidente (Renaissance) de l’Assemblée nationale,
Yaël Braun-Pivet.

Ces derniers, tout en ayant précisé qu’ils ne défileraient pas « aux
côtés » des partis d’extrême droite, n’ont pas signifié clairement
que ceux-ci n’étaient pas bienvenus. Ils n’ont pas non plus calibré
leur appel pour que lepénistes et zemmouristes passent leur chemin. Ils
ont ainsi cru bon de ne mentionner « la laïcité » que pour cibler «
l’islamisme » (et aucune autre menace contre ce principe). De même
qu’ils ont évoqué « la libération des otages » du Hamas, sans un
mot pour le déluge de feu en cours contre la population gazaouie.

Le jour de la manifestation, les militants juifs du collectif Golem étaient
bien seuls à vouloir les chasser du rassemblement. Même l’expression
moins remuante de SOS Racisme a parfois été jugée avec réprobation
par d’autres manifestants. Comme si le refus de la polémique ou la
recherche de l’apolitisme suffisaient à faire de l’extrême droite
une force participante comme une autre, critiquable par ailleurs mais
bienvenue sur le moment, puisque partageant en façade le mot d’ordre
officiel. Une attitude rejoignant les propos de Serge Klarsfeld, figure
de la cause des déportés juifs, estimant « tout à fait positive »
la participation du RN.

On en est donc à devoir rappeler pourquoi la présence de l’extrême
droite ce dimanche était choquante. À cause de son passé, bien sûr,
dont elle s’est faite la propre révisionniste pour n’avoir pas
à le désavouer clairement. À cet égard, Marine Le Pen aura été
bien servie par le questionnement complaisant du JDD ce week-end, lui
permettant de qualifier de « mensonge historique » le rappel du rôle
d’anciens collaborationnistes aux origines de son parti. Ce dernier,
a-t-elle osé, aurait été supposément accompagné par « des dizaines
de grands résistants ».

Dans ce même entretien, rien n’était dit non plus, au passage, de la
matrice « Algérie française » du RN, si importante pour comprendre
son rejet viscéral de l’immigration et du multiculturalisme. Car
même dans un monde fictif où ce parti aurait répudié ses racines
antisémites, le problème de sa participation au défilé de dimanche,
et des mots d’ordre des organisateurs lui ouvrant cette possibilité,
resterait entier.

En effet, l’extrême droite représente une menace pour le pays ici
et maintenant, que ce soit dans les urnes (avec un scénario de bascule
autoritaire en 2027, qui ne serait que facilitée par les institutions
de la Ve République), ou dans la rue (comme en témoigne, parmi tant
d’autres exemples récents, le coup de poing de néofascistes lyonnais
contre une conférence sur la Palestine ce samedi).

Dans `son entretien croisé avec Arié Alimi <https://www.mediapart.fr/journal/france/121123/marche-contre-l-antisemitisme-les-gauches-qui-appellent-ne-pas-manifester-renoncent-leur-role-historique>`_, le militant Jonas Pardo met
à juste titre en garde contre la vision du monde générale propagée
par la dirigeante du RN : « Quand Marine Le Pen accuse le mondialisme,
le cosmopolitisme, la finance internationale, les élites intellectuelles,
elle lâche des bombes codées pour désigner les juifs, cette idée
qu’il y aurait un groupe malfaisant qui agirait dans l’ombre. »

On peut ajouter que l’islamophobie du RN ou de Reconquête est
disqualifiante en elle-même, en plus de jeter le doute sur leur sincérité
quant au combat contre l’antisémitisme. De fait, leur noyau doctrinal
nationaliste et identitariste est incompatible avec le traitement équitable
et libéral de quelque minorité que ce soit, juifs compris.


Un « front républicain inversé »
======================================

Comme un pendant à cette normalisation ahurissante de l’extrême droite,
La France insoumise (LFI) est désormais presque davantage ostracisée
par toute une partie des droites et du champ médiatique.

Le parti et ses dirigeants sont certes critiquables, et donnent
parfois prise, plus qu’il ne serait nécessaire, aux attaques de leurs
adversaires. Sous prétexte de ne jamais donner le point à ces derniers,
ils s’enferment dans des attitudes incomprises par des secteurs
de l’opinion dont le soutien leur serait pourtant nécessaire pour
prétendre au pouvoir.

À propos de la marche de dimanche, les provocations de Jean-Luc Mélenchon
et les commentaires réducteurs de son entourage n’auront pas aidé à
faire comprendre les motivations de leur absence.

Mais de là à forger une sorte de « front républicain inversé »
– comme le dénonçait le leader insoumis lors des émeutes urbaines
à la suite de la mort du jeune Nahel –, il y a un pas qu’il est
irresponsable de franchir, dans la mesure où il risque d’entraîner
toute une nation vers l’abîme.

Cette version conservatrice du front républicain a bien sûr sa
logique. Le politiste Émilien Houard-Vial en avait bien repéré
les principaux éléments, qui rassemblent « les acteurs politiques
extérieurs » aux gauches, et sont fortement « mis en avant dans le
débat public de ces dernières années : ordre public, sécurité,
légalisme, anti-communautarisme et défense des intérêts patrimoniaux ».

Pour le centre et la droite républicaine, pivoter définitivement vers
cette logique négligerait cependant que ni la tradition historique,
ni les alliances, ni le programme de LFI ne témoignent des obsessions
ethnocentristes, liberticides et intrinsèquement inégalitaires qui n’ont
en revanche jamais quitté l’extrême droite. C’est bien ce camp-là,
aujourd’hui incarné par le Rassemblement national et Reconquête,
qui représente une menace concrète et imminente pour le pays.

L’oublier ou s’en accommoder est une pente coupable malheureusement
suivie par une majorité du spectre politique.

`Fabien Escalona <https://www.mediapart.fr/biographie/fabien-escalona>`_
