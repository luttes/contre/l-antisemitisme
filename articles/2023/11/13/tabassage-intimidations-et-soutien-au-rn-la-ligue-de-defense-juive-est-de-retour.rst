.. index::
   pair: Extrême droite juive; LDJ

.. _streetpress_2023_11_13:

====================================================================================================
2023-11-13 **Tabassage, intimidations et soutien au RN, la Ligue de défense juive est de retour**
====================================================================================================

- https://www.mediapart.fr/biographie/fabien-escalona
- https://www.mediapart.fr/journal/politique/131123/extreme-droite-le-jour-ou-les-reperes-ont-fini-de-disparaitre


Par Christophe-Cécil Garnier, Mathieu Molard, Eva Douley, Johan Weisz
13.11.2023

Marine Le Pen s’est rendue à la manifestation contre l’antisémitisme,
**protégée par les militants de la Ligue de défense juive (LDJ)**.

Des membres de ce groupuscule juif d’extrême droite ont agressé et menacé
plusieurs manifestants.


Boulevard Saint-Germain, Paris (75) – Plus de 100.000 personnes
manifestent ce dimanche 12 novembre contre l’antisémitisme.

Marine Le Pen et ses fidèles sont de la partie et défilent en toute fin de
cortège.
**Devant eux, quelques dizaines de militants de Ligue de défense
juive (LDJ), un groupuscule d’extrême droite juif, jouent les tampons**.

À quelques encablures du métro Assemblée nationale, un manifestant qui
pousse un vélo alpague la foule. Il crie : « Dégage Marine Le Pen,
vous êtes une bande de fachos », raconte le journaliste de Libération
Tristan Berteloot, qui a pu échanger avec lui quelques instants plus
tard. Les gros bras de la LDJ se précipitent sur le cycliste.

« Il a été tabassé au sol aux cris de : “Défoncez le, ce fils de pute”»,
rapporte toujours le journaliste. La police va finalement intervenir
pour protéger la victime. L’homme est amené sous un porche voisin,
rembobine le photojournaliste indépendant Arnaud Charlie Vilette, qui a
filmé la fin de l’agression. Aux policiers, la victime raconte avoir
été rouée « de coups de pied, de manches de drapeau et de coups de
poing » alors qu’elle était au sol, explique à StreetPress Tristan
Berteloot qui a assisté à la scène. Le cycliste annonce aussi son
intention de porter plainte.

Les différentes séquences filmées enflamment les réseaux
sociaux. La Ligue de défense juive, groupuscule violent dont les
membres collectionnaient les condamnations au milieu des années 2000,
est de retour à Paris. Elle n’a en fait jamais totalement disparu.


Ils attaquent le collectif Golem |golem|
===========================================

- :ref:`golem_jjr_2023_11_14`

Ce dimanche, le groupuscule a repris de la vigueur. Plus tôt dans
l’après-midi, la LDJ s’est aussi accrochée avec des manifestants
antiracistes quand, un peu avant 15h, Marine Le Pen entourée de
parlementaires du Rassemblement national débarquent sur l’esplanade des
Invalides, point de départ du défilé. La présence du parti d’extrême
droite à une manifestation contre l’antisémitisme fait débat et une
centaine de militants anti-racistes tentent de l’empêcher de rejoindre
le cortège.

Les membres du :ref:`collectif Golem <golem>`, un groupe de juifs et juives
de gauche ont pris la tête de la troupe. Ils scandent::

    « Le Pen casse-toi. Les Juifs ne veulent pas de toi. »

« On était là contre l’antisémitisme d’où qu’il vienne »,
détaille au téléphone :ref:`Jonas Pardo, formateur sur l’antisémitisme <jonas_pardo>`
et membre du collectif Golem.

La voix éraillée après avoir chanté tout le dimanche, il poursuit : « L’antisémitisme ne vient pas que de
l’extrême droite mais il nous semble insupportable que l’extrême
droite, qui a été, qui est et qui sera toujours antisémite, puisse
marcher aux côtés des juifs ». Rapidement, les militants sont victimes
de violences :

Pour l’avocat et membre de la Ligue des droits de l’homme Arié Alimi,
également présent lors de l’action, c’est la LDJ qui est à la
manœuvre. Les policiers nassent les militants du collectif Golem.

Des membres de la LDJ tentent de se faufiler entre les CRS pour les frapper,
retrace Arié Alimi. « Ils nous ont menacés et insultés », se souvient
aussi Manu, un autre membre du collectif Golem. Un cadre âgé de la LDJ,
qu’il a déjà croisé en 2021, lors d’un rassemblement pour Ilan
Halimi (un jeune juif séquestré, torturé et tué en 2006 par le gang des
barbares), lui a mimé un égorgement.

Une organisation presque disparue
====================================

Même si elle ne faisait plus parler d’elle, la LDJ n’avait pas
totalement disparu. À un certain nombre de manifestations organisées
par la communauté juive, les membres du groupuscule ressortent drapeaux,
brassards et foulards. Déjà, le 16 octobre dernier, des militants de
SOS Racisme repèrent à la sortie d’une réunion une quinzaine de
personnes sur la place de la République, masquées par des casques de
moto. Ils arborent le drapeau jaune au poing noir de la LDJ. Les membres
de SOS voient le groupe agresser un homme. La petite bande intimide les
antiracistes présents avant de repartir vers Châtelet. Dans les jours qui
suivent, le compte Facebook LDJ-Paris revendique l’altercation. Depuis
l’attaque du 7 octobre, le groupuscule a également mené quelques
actions symboliques, comme des collages d’affiches ou des tags contre
une vitrine ou la boîte aux lettres d’une militante anti-Israël.


https://backend.streetpress.com/sites/default/files/capture_decran_2023-11-13_a_12.18.25.png

Le soir du 16 octobre 2023, des militants de SOS Racisme assistent
à une altercation entre la LDJ et un passant place de la République
à Paris. Après l’altercation, la vice-présidente de SOS Racisme
Saphia Aït Ouarabi fait un tweet pour alerter sur la situation. Elle
est accusée de diffuser une fausse information et supprime son tweet
après un cyberharcèlement. L'action a pourtant été revendiquée par
le compte Facebook parisien de la LDJ. / Crédits : Facebook

Une résurgence qui nourrit les fantasmes. Car dans les années 2000, la
LDJ multiplie les tabassages de militants pro-palestiniens, de figures
antisémites ou de militants de la gauche juive en France. Violemment
anti-arabes, ils scandent en manifestations des slogans racistes comme :
« Pas d’arabes, pas d’attentats ». Proche de l’extrême droite
israélienne, ils défendent la colonisation. Mais la LDJ semblait en
décrépitude depuis une dizaine d’années. Plus nébuleuse que groupe
organisé, elle a perdu de sa superbe. Ses militants ont désormais les
tempes grisonnantes et ne mènent guère plus d’actions, en dehors de
ces apparitions épisodiques dans les rassemblements communautaires. Une
partie du noyau d’activistes multicondamnés pour des faits de violence
dans les années 2000 a quitté la France, souvent pour Israël. L’autre
a fait profil bas, en disparaissant des réseaux sociaux. Le Betar,
le mouvement de jeunesse rattaché au parti israélien Likoud, où la
Ligue trouvait certaines de ses recrues, a fermé ses portes en 2006,
faute de moyens et de membres.

Des liens avec l’extrême droite
====================================

Dans la manifestation de ce dimanche 12 novembre, le noyau dur
semble composé de militants historiques sortis de leur retraite pour
l’occasion. Des jeunes sont venus gonfler leurs rangs. Quelques dizaines
de militants au total, qui vont se placer en queue de manifestation,
entre les troupes de Marine Le Pen et le reste du cortège, tout au long de
l’après-midi. « C’est des kapos en fait », rigole un militant d’un
mouvement de jeunesse juif sioniste de gauche qui assiste à la scène. Une
allusion à ces détenus recrutés pour encadrer les prisonniers dans les
camps de concentrations nazis. La LDJ semble de fait jouer les services
d’ordre du RN.

Pas si étonnant : l’idylle entre le parti d’extrême droite et le
groupuscule juif démarre en 2006, avec la manifestation en hommage à
Ilan Halimi. Ce jeune juif est tué puis séquestré cette année-là. Les
gros bras de la LDJ incrustent deux élus frontistes dans le défilé,
parmi lesquels une proche de Marine le Pen, Marie-Christine Arnautu. À
l’époque, les contacts vont bon train, comme StreetPress le racontait
dans le troisième volet de l’enquête sur la Ligue de défense juive,
parue en 2014. Marine le Pen avait obtenu d’un coup de fil au boss de
la Ligue qu’ils arrêtent de taper sur son ami Alain Soral, qui était
dans le collimateur du groupe pour ses sorties antisémites.

Avec l’arrivée à la tête de la Ligue d’un ancien skinhead (ça
ne s’invente pas) en 2010, les choses s’accélèrent. Le mouvement
participe, par exemple, au service d’ordre des « assises contre
l’islamisation », organisées par les islamophobes du Bloc Identitaire
et de Riposte Laïque, en décembre de cette année.

Une enquête en 3 parties
=================================

- https://www.streetpress.com/sujet/1410186250-la-ligue-de-defense-juive-en-vrai
- https://www.streetpress.com/sujet/1410338182-la-ligue-de-defense-juive-en-vrai-partie-2
- https://www.streetpress.com/sujet/1410446959-ligue-defense-juive-3-actions-extreme-droite-racisme

En 2014, StreetPress avait écrit un dossier en trois parties sur la LDJ :

- `Partie 1 : Milice ou bullshit ? <https://www.streetpress.com/sujet/1410186250-la-ligue-de-defense-juive-en-vrai>`_
- `Partie 2 : LDJ, la story <https://www.streetpress.com/sujet/1410338182-la-ligue-de-defense-juive-en-vrai-partie-2>`_
- `Partie 3 : La guerre parallèle, les liens avec l’extrême droite et le racisme Mélenchon dans le viseur <http://www.streetpress.com/sujet/1410446959-ligue-defense-juive-3-actions-extreme-droite-racisme>`_


Mélenchon dans le viseur
============================

La LDJ voit dans le Rassemblement National converti à la doctrine
identitaire, un allié contre « l’islamisation » de la société.

À l’inverse, elle voit dans La France Insoumise un soutien aux « islamistes
». Ainsi, ce dimanche 12 novembre, à intervalles réguliers, le petit
cortège de la LDJ scande : ??

Depuis plusieurs années, la LDJ a le leader mélenchoniste dans
le viseur. En 2018, la France est en deuil après le meurtre de
Mireille Knoll, 85 ans et rescapée de la shoah, poignardée à son
domicile. Une marche blanche est organisée pour lui rendre hommage et
dénoncer l’antisémitisme, le 28 mars 2018. Jean-Luc Mélenchon, qui
défile aux côtés d’autres cadres insoumis, est pris à partie par
la Ligue de défense juive. « Eliahou », le porte-parole et le chef
de la « branche politique » de la LDJ, rameute ses troupes pour virer
l’homme politique du cortège, au cri déjà de « Mélenchon, salaud,
les juifs auront ta peau ». Pressé par une foule hostile, l’insoumis
est exfiltré de la manifestation.

Sur les réseaux sociaux, la Ligue de défense juive s’en prend
régulièrement aux cadres de la France Insoumise. Mi-octobre, ils
ont, comme l’avait révélé RMC, diffusé sur leur page Facebook les
numéros de téléphone de Jean-Luc Mélenchon et du député Louis Boyard,
incitant au passage leurs abonnés à les harceler (1).

Sur Instagram cette fois, c’est David Guiraud qui est visé. Le
parlementaire insoumis s’est retrouve tagué il y a quelques jours sur
une photo qui montre ce qui semble être un obus de char sur lequel est
inscrit au marqueur noir : « David Giraud, LFI. Dans ton cul. » Le compte
(2) qui partage l’image et le mentionne appartient à un certain Tony
Attal. Jusqu’au mitan des années 2000, la LDJ est justement dirigée par
Anthony Attal, une grande gueule adepte des provocations et des opérations
coups de poing. Il est à l’origine de l’attaque d’une librairie
qui organisait une dédicace d’Alain Soral, mais aussi de plusieurs
actions violentes contre des événements organisés en soutien à la
cause palestinienne. Il a lâché les rênes du groupuscule quand il a fait
son aliyah (le fait de quitter la France pour rejoindre Israël). Rien ne
permet d’affirmer avec certitude qu’il s’agit du même homme. Mais
un autre indice vient accréditer l’hypothèse : la LDJ a relayé sur
sa page Facebook plusieurs autres photos d’obus barrés de messages
similaires, inscrits également au feutre noir et en français (3).

Le compte qui partage l’image et insulte le député David Guiraud
appartient à un certain Tony Attal. Jusqu’au mitan des années 2000, la
LDJ a justement été dirigée par Anthony Attal, une grande gueule adepte
des provocations et des opérations coups de poing. / Crédits : Instagram

Une série d’actions qui pourraient envoyer certains membres de la Ligue
de défense juive devant les tribunaux. En effet, Louis Boyard et Jean-Luc
Mélenchon ont porté plainte pour la diffusion de leurs numéros, détaille
à StreetPress l’avocate de ce dernier, Jade Dousselin. David Guiraud
annonce son intention d’en faire de même pour la photo de l’obus. Selon
nos informations, le député insoumis Thomas Portes envisagerait quand
à lui d’appeler à la dissolution de la Ligue de défense juive.

(1) Le poste a depuis été retiré.  (2) Le compte a été supprimé
depuis.  (3) Les messages visaient l’activiste franco-palestinienne
Rima Hassan ou l’influenceur luxembourgeois Dylan Thiry.
