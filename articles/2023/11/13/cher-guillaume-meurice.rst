.. index::
   pair: Dog whistle; Guillaume Meurice

.. _paco_tizon_2023_11_13:

=======================================================
2023-11-13 **Cher Guillaume Meurice** par Paco Tizon
=======================================================

- https://blogs.mediapart.fr/paco-tizon/blog/131123/cher-guillaume-meurice
- https://blogs.mediapart.fr/paco-tizon
- https://www.lemonde.fr/m-le-mag/article/2023/11/08/jonas-pardo-antidote-a-l-antisemitisme-au-sein-de-la-gauche_6199014_4500055.html
- https://www.lexpress.fr/societe/jean-yves-camus-nazifier-les-juifs-cest-vouloir-en-faire-le-mal-absolu-SHIWMXCRUZBYTGFJEIARLCDJPU/

Préambule
==========

Mr Mondialisation est un gros compte sur les réseaux sociaux et un site
militant, anticapitaliste et écolo, contre les puissants et pour le peuple,
mais un brin antidémocratique et qui verse parfois dans le confusionnisme.

Il vient de publier un bouquin qui regroupe ses chroniques, préfacé par Meurice.
https://mrmondialisation.org/livre-evolution-mondialisation/

Article
==========

Cher Guillaume Meurice, vos chroniques sont une bouffée d'air
frais. Dans notre petit foyer elles comblent le vide laissé par les
regrettées chroniques "Bonjour tristesse" de Matthieu Longatte.

Merci pour ces moments, donc, salutaires, combattifs et joyeux, qui nous
montrent quels sont nos réels adversaires.  Aujourd'hui, après votre
dernière chronique, je vous adresse une critique "issue du même camp":
mes combats sont l'antispécisme, les luttes contre le capitalisme et
les criminels climatiques, le féminisme, l'antiracisme, la défense de
toutes les personnes racisées ou fragilisées par les normes dictées par
les puissants, y compris la défense des Palestinien.ne.s et des Juif.ve.s.

Votre blague sur Netanyahou "nazi sans prépuce" aurait pu passer comme
une maladresse. Personne n'est à l'abri d'une maladresse, surtout
les humoristes équilibristes.

Parfois on rate le dosage. Parfois la période n'est pas propice à des
punchlines qui sont alors des armes blessantes. Bref.

Je pensais que votre tendresse pouvait vous amener,
après qu'on ait bien expliqué en quoi cette blague avait une teneur
antisémite, à présenter des excuses. Pas à la rédac, pas au autorités
républicaines, mais aux auditeurs. Ça se fait et il n'est pas trop tard.

Au lieu de ça, on a eu droit à une réponse façon Dieudonné (un "pardon"
qui n'est qu'une autre méchanceté), à un tacle sur Guillon dont vous
reprenez pourtant l'esprit, et à la stratégie Morano ("regardez, j'ai des
amis juifs").

Vous noterez d'ailleurs que vos amis (**je ne vais défoncer
Tsedek ici, c'est pas l'endroit**) n'ont pas cautionné la comparaison du
dirigeant juif d'extrême-droite avec un nazi.

Et que personne, à part ceux qui rient des pratiques musulmanes et juives,
n'a ri de la blague réitérée sur "le problème avec cette absence de prépuce".

Entendez donc celleux qui vous disent pourquoi cette blague les a
choqué.e.s. Tout comme on a su entendre les voix des personnes insultées
par les caricatures de Ch. Hebdo ou par les blagues de G. Proust.

Entendez celleux qui vous expliquent le `dog whistle antisémite <https://www.lemonde.fr/m-le-mag/article/2023/11/08/jonas-pardo-antidote-a-l-antisemitisme-au-sein-de-la-gauche_6199014_4500055.html>`_
`perceptible dans votre blague <https://www.lexpress.fr/societe/jean-yves-camus-nazifier-les-juifs-cest-vouloir-en-faire-le-mal-absolu-SHIWMXCRUZBYTGFJEIARLCDJPU/>`_, pour que, au lieu de fanfaronner, vous puissiez reconnaître une erreur.

Charline Vanhoenacker rame fort, et ce matin Waly Dia a eu du mérite en
empruntant une étroite ligne de crête : dénoncer le violent racisme
qui gangrène plusieurs médias sans toutefois donner du crédit à vos
propos. Chapeau l'ami.

Au fait, votre blague, ajoutée à votre préface
au livre confusionniste de Mr Mondialisation, me font craindre un virage
fâcheux.

Faites comme d'habitude, restez libre, mais prêtez l'oreille,
apprenez des autres, on a besoin de vous dans nos combats.
