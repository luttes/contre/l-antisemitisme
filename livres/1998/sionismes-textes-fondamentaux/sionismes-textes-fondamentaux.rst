.. index::
   pair: Livre ; Sionismes : textes fondamentaux (Denis Charbit, 1998)

.. _sionismes_textes_fondamentaux_1998:
.. _charbit_livre_1998:
.. _charbit_livre_sionismes_1998:

====================================================================
**Sionismes : textes fondamentaux** (Denis Charbit, 1998)
====================================================================

- :ref:`denis_charbit`
- https://www.lalibrairie.com/livres/sionismes--textes-fondamentaux_0-1023421_9782226100382.html
- https://www.librairielesquare.com/livre/9782226100382-sionismes-textes-fondamentaux-denis-charbit/

Issu d'un groupe de travail du Collège international de philosophie sur le
sionisme, ce livre présente une anthologie des textes fondamentaux des courants
sionistes depuis 1880.

Les différentes conceptions et les pensées de:

- Theodor Herzl,
- Bernard Lazare,
- Martin Buber,
- Yossef Brenner,
- David Ben-Gourion,
- Gershom Scholem,
- Albert Memmi,
- Yeshayahou Leibovitch
- Amos Oz
- ...
