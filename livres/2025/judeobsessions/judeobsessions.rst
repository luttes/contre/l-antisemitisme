.. index::
   pair: Livre; Judéobsessions (2025-01-22)
   pair: Guillaume Erner; Judéobsessions (2025-01-22)

.. _judeobsessions_2025:

====================================================================
**Judéobsessions**  par Guillaume Erner (2025-01-22)
====================================================================

- https://editions.flammarion.com/index.php/judeobsessions/9782080439093

.. figure:: images/recto.webp


.. _guillaume_erner:

Guillaume Erner
==============================

- https://fr.wikipedia.org/wiki/Guillaume_Erner

Guillaume Erner anime les matins de France Culture et, sur la même chaîne,
l’émission "Superfail", programme dédié à l’échec, aux erreurs et aux
catastrophes.

Il est l’auteur d’Expliquer l’antisémitisme (PUF) et de La Souveraineté du
peuple (Gallimard).

Description
==============

"À un moment donné, tout le monde ou presque s’est mis à parler des Juifs :
sur les réseaux sociaux, dans les débats télévisés, sans compter les couvertures
de magazines ou les unes de journaux.

Alors, j’ai pris peur et j’ai décidé d’écrire.

Pour comprendre les raisons de l’obsession que je voyais surgir autour de moi,
mais aussi pour transmettre les raisons de la mienne."

Guillaume Erner tisse ici un fil entre son histoire familiale, son regard
avisé sur la société contemporaine et ses travaux sur l’histoire de l’antisémitisme.

Un essai singulier qui, tantôt avec humour et tendresse, tantôt avec ironie
et acuité, exprime une profonde inquiétude sur l’avenir de la présence juive
en diaspora et plaide pour le droit des Juifs à l’indifférence.

Comme l’écrivait Marc Bloch dans L’Étrange défaite : "Je ne revendique jamais
mon origine que dans un cas : en face d’un antisémite."


Articles/emissions 2025
==========================

- https://www.radiofrance.fr/franceculture/podcasts/le-cours-de-l-histoire/guillaume-erner-des-matins-au-soir-fou-d-histoire-6617772 (De l’Antiquité au 7 octobre 2023, Guillaume Erner fait dialoguer l’histoire de l’antisémitisme avec sa mémoire familiale)
- :ref:`raar_2025:judeobessions_2025_01_09`
