.. index::
   pair: Livre; Auschwitz (2025-01-23)
   pair: Guillaume Erner; Auschwitz (2025-01-22)

.. _auschwitz_2025:

====================================================================
**Auschwitz**  par Tal Bruttmann (2025-01-23)
====================================================================

- https://www.librairiesindependantes.com/product/9782348086588/

.. figure:: images/recto_auschwitz.webp


Tal Bruttmann
==============================

- :ref:`tal_bruttmann`


Description
==============

Auschwitz est devenu le symbole des camps de concentration et de l'assassinat
de masse, occupant aujourd'hui une place centrale d'un point de vue tant mémoriel
qu'historique.

Marqué par le gigantisme, qu'illustrent en premier lieu les chiffres

- 1,3 million de personnes acheminées là depuis toute l'Europe,
  dont 1,1 million  y meurent
- le site fut à la fois le plus important des camps de concentration et le
  plus meurtrier des centres de mise à mort de la " solution finale ".
  Pourtant, il s'agit d'un lieu d'une rare complexité, constitué d'une multitude
  d'espaces
- camps de concentration, centre de mise à mort, industries de tous types
- articulés autour de la ville d'Auschwitz, choisie par le régime nazi pour
  devenir un modèle de développement urbain et industriel au sein du IIIe Reich.

C'est dans cet espace que se sont croisées et concentrées les politiques
répressives contre différentes catégories de populations (Polonais, Tsiganes,
Soviétiques...), les politiques d'assassinat, mais aussi les politiques de
colonisation et de développement industriel, faisant d'Auschwitz une quintessence
des projets nazis.



Articles 2025
================

- :ref:`raar_2025:tal_bruttmann_2025_01_26`
- :ref:`raar_2025:raar_2025_01_27`
