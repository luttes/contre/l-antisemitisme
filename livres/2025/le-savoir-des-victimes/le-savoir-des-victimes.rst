.. index::
   pair: Livre; Le savoir des victimes Comment on a écrit l'histoire de Vichy et du génocide des juifs de 1945 à nos jours (Laurent Joly, 2025-02)

.. _savoir_des_victimes_2025_02:

===============================================================================================================================================
2025-02 **Le savoir des victimes Comment on a écrit l'histoire de Vichy et du génocide des juifs de 1945 à nos jours** par Laurent Joly
===============================================================================================================================================

- https://www.grasset.fr/livre/le-savoir-des-victimes-9782246823995/



Articles 2025
=================

Les nazis, Vichy et l’antisémitisme
-----------------------------------------

- https://monde-libertaire.fr/?articlen=8237
