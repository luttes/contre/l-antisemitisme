.. index::
   pair: Livre; Le temps qui reste (2023, par Patrick Boucheron)

.. _le_temps_qui_reste_2023:

===========================================================================================================================
2023 **Le temps qui reste** (2023, par Patrick Boucheron) |PatrickBoucheron|
===========================================================================================================================

- https://www.seuil.com/ouvrage/le-temps-qui-reste-patrick-boucheron/9782021504965

.. figure:: images/recto.webp

Auteur : Patrick Boucheron |PatrickBoucheron|
=================================================

- https://fr.wikipedia.org/wiki/Patrick_Boucheron

Patrick Boucheron, né le 28 octobre 1965 à Paris, est un historien français.

Spécialiste du Moyen Âge et de la Renaissance, particulièrement en Italie,
il est, depuis 2015, professeur au Collège de France de la chaire
"Histoire des pouvoirs en Europe occidentale, XIIIe – XVIe siècles".

A propos
===========

On nous l’annonce comme imminente et inéluctable : une catastrophe lente à
venir.

On nous l’annonce depuis si longtemps.

Mais est-ce pour nous alerter ou pour nous habituer ?

Il est grand temps d’en décider. Car on peut craindre, ou espérer, un événement
qui, lorsqu’il advient n’est pas le surgissement de l’inconnu mais la poursuite
de ce que l’on connaissait très bien et qu’on n’a pas su éviter.

On se rend compte alors, mais trop tard, qu’à force de l’attendre, on n’a pas
compris qu’il était déjà advenu.


Critiques
=============

https://www.babelio.com/livres/Boucheron-Le-temps-qui-reste/1570152/critiques/3704473

Dans ce petit opus, l'historien développe une mise en garde contre l'immobilisme
et l'aveuglement dont sont pris d'une part les dirigeants politiques d'autre
part les citoyens.
Pas de leçons dans ce court texte mais une réelle inquiétude face à la montée
de certains phénomènes, poussée de l'extrême droite, accroissement des inégalités, ......
appréhendés avec beaucoup de passivité, de laisser-faire,... tels les pêcheurs
du bord de Seine lors de l'exécution de Louis XVI.

Une parole libre, claire, clairvoyante, espérons qu'elle ne soit pas prémonitoire.



Extraits
=============

Un laboratoire italien ? p. 49
---------------------------------

Est-ce à dire que l'arrivée au pouvoir de Marine Le Pen ne changerait rien ?
Non bien entendu, tout changera, mais pas comme on le pense.

...

et pourtant les exemples abondent qui auraient dû nous alerter: **déjà en 2015,
le coup de bluff de Matteo Renzi annonçait celui d'Emmanuel Macron**, et l'aventure
désastreuse des Cinque Stelle de Beppe Grillo devrait, à elle seule, nous
garantir contre la tentation d'un populisme prétendument de gauche - pour ne
rien dire du précédent absolu, matriciel, du berluconisme qui a légué à tous
ses continuateurs une recette imparable **si vous souhaitez oeuvrer à l'effondrement
rapide et durable de l'esprit public** commencez par promouvoir le saccage
télévisuel de la diversité culturelle.

..

p.51 **Timothy Snyder met par exemple en garde contre ce qu'il appelle "L'obéissance anticipée"**
----------------------------------------------------------------------------------------------------

- https://fr.wikipedia.org/wiki/Timothy_Snyder


`Timothy Snyder <https://fr.wikipedia.org/wiki/Timothy_Snyder>`_
(spécialiste du nazisme et du stalinisme) **met par exemple en garde contre
ce qu'il appelle "L'obéissance anticipée"**, en montrant qu'en mars 1938,
"l'obéissance anticipée" des Autrichiens apprit aux dirigeants nazis ce
qu'il était possible.
L'historien adresse donc cette leçon (je préfère dire, dans la langue
classique, un *avertissement* pour aujourd'hui : "Pour l'essentiel, le pouvoir
autoritaire est librement consenti. Dans les temps comme ceux-ci, les
individus sur ce que voudra un Etat plus répressif et s'offrent à lui
sans qu'on leur demande rien.
Un citoyen qui s'adapte ainsi enseigne au pouvoir ce qu'il peut faire."

Je pense que nous en sommes là, et même un peu au delà si l'on consent
à regarder ce que Machiavel appelerait les "Cose d'Italia", la chose
politique telle qu'elle se dégrade dans l'Italie de Giorgia Meloni
depuis la fin de l'année 2022.
