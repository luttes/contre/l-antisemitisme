.. index::
   pair: Livre; Israël et ses paradoxes (2023)

.. _israel_et_ses_paradoxes_2023:
.. _charbit_livre_paradoxe_2023:

====================================================================
**Israël et ses paradoxes** (Denis charbit 3e edition, 2023)
====================================================================

- :ref:`denis_charbit`
- http://www.lecavalierbleu.com/livre/israel-et-ses-paradoxes-2/
- https://www.librairielesquare.com/livre/9791031805870-israel-et-ses-paradoxes-idees-recues-sur-un-pays-qui-attise-les-passions-3e-edition-denis-charbit/

Introduction
================

Israël, plus que tout autre pays, suscite les passions.

Pro- et anti- attisent la polémique à coup d’idées reçues.

À force de combattre les préjugés des uns et des autres, c’est débattre qui
est devenu impossible.

C’est la grande ambition de ce livre que de favoriser le retour au débat
exigeant, argumenté, contradictoire, conjuguant les divergences, autorisant
les convergences.

Quiconque tient que toute la vérité vient d’Israël et que la Palestine est
mensonge ; quiconque pense que le Mal est israélien et que le Bien est palestinien,
ne trouvera guère dans ce livre de quoi blinder ses convictions.

Ni État d’exception ni État exceptionnel, Israël est "un État comme les autres",
avec ses paradoxes.

Cela ne dispense nullement de rendre compte de ce qu’il est et de rendre des
comptes sur ce qu’il fait.

3e édition revue et augmentée

Sommaire
==============

Un État et une société complexes
----------------------------------------

- "Israël est la seule démocratie au Moyen-Orient."
- La Cour suprême, contre-pouvoir ou gouvernement des juges ?
- "Le scrutin proportionnel paralyse le système politique."
- "Israël est un état militariste."
- "Tsahal est l’armée du peuple."
- La jeunesse israélienne, semblable et différente à la fois
- "Israël est la nation start-up."
- Les séries israéliennes : un phénomène de société
- "Israël est un état théocratique."
- "La Loi du Retour est raciste."
- "Les tensions entre Séfarades et Ashkénazes sont en voie de disparition."
- "Les Arabes d’Israël sont des citoyens de seconde zone."

Le contentieux israélo-palestinien
---------------------------------------

- "Israël est une conséquence de la Shoah, la Nakba aussi."
- "Israël est la dernière puissance coloniale."
- "Israël pratique l’apartheid."
- "Le gouvernement israélien fait preuve d’intransigeance."
- "Jérusalem est la capitale d’Israël, une et indivisible."
- "Le conflit israélo-palestinien a fait d’Israël l’ennemi du monde arabe."

Israël et le monde
-----------------------

- "Israël instrumentalise la Shoah à des fins politiques."
- "Israël est un État-voyou."
- "Israël et le lobby juif dictent la politique des États-Unis au Moyen-Orient."
- "Les Israéliens sont francophobes."
- Des juifs de France en Israël
- "Les communautés juives en diaspora sont des alliés inconditionnels d’Israël."
- "L’antisionisme est le nouvel habit de l’antisémitisme."
- Albert Camus : "Conseils de prudence"



Presse
================================

- Denis Charbit était l’invité d’Affaires étrangères sur France Culture, le 3 février 2024. A réécouter `ICI <https://www.radiofrance.fr/franceculture/podcasts/affaires-etrangeres/otages-et-cessez-le-feu-le-calvaire-israelo-palestinien-4126246>`_.
- Denis Charbit était l’invité d’Un jour dans le monde sur France Inter, le 30 janvier 2024. A réécouter ICI.
- Denis Charbit a participé à une table-ronde organisé par la revue Esprit, le 29 janvier 2024. Voir la vidéo ICI.
- Denis Charbit était l’invité de France 24, le 26 janvier 2024. A revoir ICI.
- Entretien avec Denis Charbit dans L’Opinion, le 24 janvier 2024. A lire ICI.
- Denis Charbit était l’invité de l’émission C ce soir sur France 5, le 23 janvier 2024. A revoir ICI.
- Analyse de Denis Charbit dans un article de France Info, le 7 janvier 2024. A lire ICI.
- Denis Charbit était l’invité du Temps du débat sur France Culture, le 27 novembre 2023. A réécouter ICI.
- Interview de l’auteur dans Libé, le 9 novembre 2023. A lire ICI.
- Entretien avec Denis Charbit dans le Parisien, le 5 novembre 2023. A lire ICI.
- Entretien avec l’auteur dans Réforme, le 11 octobre 2023. A lire ICI.
- Recension du livre dans la newsletter de la FEGEPRO 2023-24, à lire ICI.
- Denis Charbit était l’invitée de l’émission Géopolitique sur RFI, le 29 juillet 2023. A réécouter ICI.
- Tribune de l’auteur dans Libération, le 26 juillet 2023. A lire ICI.
- Analyse de Denis Charbit sur Public Sénat, le 24 juillet 2023. A lire ICI.
- Entretien avec Denis Charbit sur RFI le 23 juillet 2023. A lire ICI.
- Denis Charbit était l’invité de l’émission Orient hebdo sur RFI, le 7 juillet 2023. A réécouter ICI.
- Entretien avec Denis Charbit sur Regards protestants, le 3 juillet 2023. A voir ICI.
- Entretien avec Denis Charbit dans Le Soir, le 11 mai 2023. A lire ICI (abonnés).
- Tribune du collectif mené par l’auteur dans Le Monde, le 15 avril 2023. A lire ICI.
- Denis Charbit était l’invité de l’émission Affaires étrangères sur France Culture le 15 avril 2023. A réécouter ICI.
- L’auteur était l’invité de France Inter dans Le portrait, le 2 avril 2023. A réécouter ICI.
- L’auteur était l’invité de l’émission Un jour dans le monde sur France Inter, le 27 mars 2023. A réécouter ICI.
- Denis Charbit était l’invité de Cultures Monde sur France Culture, le 14 mars 2023. A réécouter ICI.
- Tribune de l’auteur dans Le Figaro, le 2 mars 2023. A lire ICI (abonnés).
