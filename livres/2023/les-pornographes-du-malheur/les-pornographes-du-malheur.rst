.. index::
   Livre ; Les rapaces (par Camille Vigogne Le Coat)
   ! Les rapaces

.. _les_pornographes:

===================================================================================================
|Pornographes| **Les pornographes du malheur** de Fabienne Messica (2023-05) |FabienneMessica|
===================================================================================================

- :ref:`fabienne_messica`
- https://ruedeseine.fr/livre/les-pornographes-du-malheur/

Description
=============

Les médias, les émissions de télévision TouteInfo déferlent sur nos
écrans, nous plongeant dans un gouffre de voyeurisme saupoudré de
fausse bienveillance.

Faisant de nous de pauvres lucioles prises au piège d’un voyeurisme
consentant.

Fabienne Messica, avec audace, courage et une saine rage, démonte les
mécanismes des pornographes du malheur.

Baba et les autres n’ont qu’à bien se tenir.

**Voici le scalpel des mots d’une femme qui ne courbe pas l’échine**.

Liens
========

- :ref:`jjr_rapaces`
