.. index::
   pair: Livre; Un album d'Auschwitz Comment les nazis ont photographié leurs crimes (2023)

.. _livre_bruttmann_2023:

===============================================================================================================================================
2023-01 **Un album d'Auschwitz Comment les nazis ont photographié leurs crimes** par Tal Bruttmann, Stefan Hördler et Christoph Kreutzmüller
===============================================================================================================================================

- https://www.seuil.com/ouvrage/un-album-d-auschwitz-tal-bruttmann/9782021491067
- https://www.seuil.com/auteur/tal-bruttmann/15519


.. figure:: images/couverture.jpg


Entre mi-mai et début juillet 1944, des centaines de milliers de Juifs
de Hongrie sont déportés à Auschwitz-Birkenau.

Pour montrer à leur hiérarchie la « bonne mise en œuvre » de cette opération
logistique d’envergure, des SS photographient les étapes qui mènent de
l’arrivée des convois jusqu’au seuil des chambres à gaz, ou au camp pour
la minorité qui échappa à la mort immédiate.

Ces photographies, connues sous le nom d’« Album d’Auschwitz », ont été
retrouvées par une rescapée, Lili Jacob, à la libération des camps, avant
de servir de preuves dans différents procès et de faire l’objet de
plusieurs éditions.

Certaines de ces photographies sont même devenues iconiques.

Par-delà l’horreur dont elles témoignent, ces images restent pourtant
méconnues et difficiles d’interprétation.

Ce livre permet d’y jeter un regard neuf. Préfacé par Serge Klarsfeld,
fruit de cinq années de recherches franco-allemandes, il analyse l’album
dans ses multiples dimensions.

Pour quelle raison a-t-il été réalisé et quand ?

Comment a-t-il été constitué ? Que peut-on voir, ou ne pas voir, sur ces
photographies ?

Trois historiens reconnus et spécialistes de la persécution des Juifs
d’Europe, Tal Bruttmann, Stefan Hördler, Christoph Kreutzmüller, ont
mené un remarquable travail d’enquête, recomposant les séries de photographies,
analysant des détails passés inaperçus, permettant un travail d’identification
et de chronologie inédit.

Dans le même temps, c’est une véritable réflexion sur l’usage des images
et de la photographie, de leur violence potentielle mais aussi de leur
force de témoignage et de preuve que les historiens proposent.

Ce faisant, ils élargissent la connaissance tout en redonnant vie,
mouvement et dignité aux personnes photographiées quelques minutes avant
une mort dont elles n’avaient pas idée.
