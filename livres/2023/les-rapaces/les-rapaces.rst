.. index::
   Livre ; Les rapaces (par Camille Vigogne Le Coat, 2023-11)
   ! Les rapaces

.. _les_rapaces:

==========================================================================================================
**Les rapaces** enquête sur la "Mafia Varoise" par **Camille Vigogne Le Coat, 2023-11**
==========================================================================================================

- https://arenes.fr/livre/les-rapaces/
- https://x.com/Camille_Vigogne

.. figure:: images/les_rapaces.png


Description
=============

Grand reporter à L’Obs, Camille Vigogne Le Coat raconte dans ce livre
l’exercice du pouvoir par le Rassemblement National.

L’enquête fourmille de détails, d’informations et d’enregistrements inédits.

« Tu n’as jamais gardé de liquide en grande quantité, toi? »

David Rachline, le maire de Fréjus, s’inquiète. Que faire de ce cash qui
sert à payer ses soirées VIP avec son ami Jordan Bardella ?

D’où viennent ces liasses qui financent bijoux et montres de luxe ?

Lors de son élection en 2014, «le meilleur ami»de Marine Le Pen promettait
pourtant une gestion exemplaire, la fin de la corruption et du clientélisme.

Mais une fois élu, David Rachline a signé toujours plus de contrats avec
un entrepreneur tout-puissant, noyant sa ville sous le béton et les
passe-droits.

**À la mairie, les fêtes s’enchaînent jusqu’au bout de la nuit,
l’antisémitisme et le racisme ne sont jamais loin**, pendant que les
policiers municipaux jouent aux cow-boys en Harley Davidson.

Voilà l’homme dont Marine Le Pen a fait son directeur de campagne
présidentielle et le vice-président de son parti.


Liens
========

- :ref:`jjr_rapaces`
