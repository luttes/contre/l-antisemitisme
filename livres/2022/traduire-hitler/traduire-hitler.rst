.. index::
   pair: Livre; Traduire Hitler (2022-10, Olivier Mannoni)
   pair: Olivier Mannoni; Traduire Hitler (2022-10)

.. _traduire_hitler_2022:

=================================================================================
2022-10 **Traduire Hitler** par Olivier Mannoni |OlivierManonni|
=================================================================================

- https://journals.openedition.org/traduire/3766


.. figure:: images/recto.webp


Auteur  Olivier Mannoni |OlivierManonni|
=============================================

- :ref:`olivier_manonni`


Description dans actualitte par Hocine Bouhadjera
=====================================================

- https://actualitte.com/article/108198/edition/olivier-mannoni-comment-historiciser-mein-kampf
- https://actualitte.com/auteurs/221/hocine-bouhadjera

« La langue est la mère, non la fille de la pensée », disait le journaliste
et écrivain autrichien, Karl Kraus, avant que Georges Orwell n'établisse un
lien direct entre totalitarisme et manipulation d’un idiome perverti et réduit.

Dans ce même mouvement, Hannah Arendt relia totalitarisme à l'abolition du
vrai et du faux – la préfiguration des fake news.

Traduire Hitler, disponible en librairie depuis le 13 octobre, est né d’un
double désir : celui des éditions Héloïse d’Ormesson de proposer à Olivier Mannoni
de raconter ses 9 ans de sombre compagnonnage avec Mein Kampf, et du traducteur,
qui avait en tête un tel récit, dès la fin de sa traduction.

Le fondateur de l’école de traduction littéraire nous le détaille : « Quand
on travaille sur un texte de cette nature, il est difficile de ne pas porter
une réflexion sur ce que l’on fait. J’avais vraiment besoin de l’écrire, et
par là même, mettre une sorte de point final. »

Deux jours après la proposition des éditions Héloïse d’Ormesson, le plan de
l’ouvrage était couché sur le papier.

“Un smog linguistique”
==========================

Olivier Mannoni en était conscient dès le départ : « Je n’allais pas traduire
un livre, mais un symbole, un “grimoire” », reprenant l’expression de la
journaliste, Florence Aubenas.

C’est en 2011 que l’historien Fabrice d’Almeida contacte le traducteur installé
en Touraine pour exposer le grand projet de Fayard : soumettre une nouvelle
traduction du brûlot d’Adolf Hitler, logorrhée de plus de 600 pages écrite
en prison entre 1924 et 1925.

L'ensemble en vue d'une édition scientifique et critique de l’oeuvre, loin
de toute polémique.
Une intention de la maison d’édition et de son directeur de l’époque, Olivier Nora,
avant l'arrivée de Sophie de Closets en 2015, qui répondait, entre autres,
à l’actualité.

En 2016, l’ouvrage maudit entrerait dans le domaine public.
Jusqu’à cette année, les droits étaient, comme tous les textes des hauts
dignitaires nazis condamnés à Nuremberg ou disparus après la guerre, gérés
par le Land de Bavière, qui ne les accordait qu'au compte-gouttes.

Pour Mein Kampf, la cession des droits était systématiquement refusée, et
la région allemande avait toute liberté de bloquer les parutions et d'intenter
un procès si le texte en question était republié.

En France, il était toujours en vente libre aux Nouvelles Éditions latines
depuis 1934, dans une traduction d’André Calmette.

La seule période d’interruption a été du fait d’Hitler lui-même, qui avait
fait interdire son livre en France, pour des raisons financières…

Deux ans après avoir entamé ce labeur, Olivier Mannoni rend une première version,
d'abord acceptée,  avant qu'il lui soit demandé de reprendre le travail sous
une autre perspective : Florent Brayard, arrivé à la tête du projet en 2015,
accepte la traduction avant de lui demander un reboot complet…

Le traducteur  explique cette nécessité : « Très concrètement, la langue
d’Hitler est extraordinairement chargée, comme un fleuve qui charrierait
des débris de tous les côtés, jusqu’à rendre invisible le fleuve lui-même.
Dans ma première traduction, j’ai réalisé un travail de traducteur standard :
générer un texte le plus lisible possible à un lecteur classique, corrigeant
notamment les incorrections de langage. »

L’historien lui propose « de revenir au plus près du texte », afin de restituer
sa réalité, non pas mot à mot, mais avec la totalité des défauts « qui
précisément rendait la lecture confuse et difficile. »

Olivier Mannoni remet l'ouvrage sur le métier : il enlevait par exemple deux
ou trois adverbes « inutiles » sur les six que contenait une simple phrase,
afin de s’approcher d’une certaine vérité du texte.
Un travail « très long, minutieux, pénible, pointilleux », et au bout une
révélation : Hitler n’était pas (seulement) un écrivain brouillon, confus,
« en résumé mauvais ». Il portait surtout une volonté, plus ou moins consciente,
de noyer le propos dans un brouillard épais. Les mécanismes s’exhibent.

Avant ce projet de Fayard, très peu de réelles études sur la prose d’Hitler
et la structure du texte avaient été entreprises.

Le philologue Victor Klemperer, dans son Lingua Tertii Imperii édité en 1947,
avait défini la langue nazie, comme destinée à produire  de la croyance.
Hitler et ses affiliés s’adressaient au lecteur d’une manière tout à fait
nouvelle, basée sur la confusion.

« C’est révélateur, parce que découlant d'une technique de communication :
d’une certaine manière, vous hypnotisez les gens à qui vous vous adressez.
Il faisait la même chose dans ses discours publics. »
Et de développer : « Vous bloquez la pensée, et au terme de ces longues
tirades, une idée très simple s’expose, comme

- “les socio-démocrates sont  responsables de la faillite de l’Allemagne”,
- “les communistes ont poignardé l’armée allemande dans le dos”,
- ou “les juifs sont coupables de notre malheur et contrôlent tout”…

Toute une phraséologie qu’on connait bien maintenant, nimbée d’un smog linguistique. »

De la confusion du langage à celle de l'esprit
===================================================

Pourquoi avoir choisi une telle forme ?

Selon le traducteur, d’abord parce qu’un texte plus lisible aurait par trop
clarifié le langage opaque d'un personnage dangereux, mais plus profondément,
car cela participe d’une communication opérative – à base de travestissement
des termes et de production d’incompréhension : « Qui dit confusion du langage,
dit confusion mentale. »

Pour preuve, les autres écrits des cadres de la NSDAP, également traduits
par Olivier Mannoni, comme ceux du « théoricien du nazisme », Alfred Rosenberg,
tout aussi « fumeux et ésotérique, comme rédigé dans une grande lourdeur ».

En parallèle, l’appareil politique suivait la même ligne, avec un Goebbels
qui se montrait bien plus « direct et violent » dans son approche, à base
de « coups de poing ».
Si Mein Kampf ne se vend pas beaucoup au début, le parti essaime massivement
les idées du livre.

Il est tout de même diffusé à 12 millions d'exemplaires en Allemagne, et à
partir de 1934, inclus dans la note de mariage des jeunes époux…
En vérité, l’ouvrage élabore un échafaudage théorique qui prépare la suite,
bien que, précise Olivier Mannoni, ni en 1924-1925 ni en 1933 quand Adolf Hitler
est élu, la Shoah n’était exprimée ou théorisée de la manière dont elle a eu lieu.

La France serait historiquement une construction politique, l’Angleterre
forgée par son caractère insulaire de thalassocratie...

Et l'on prête à l’Allemagne de reposer sur le concept d’un sang germanique.

Olivier Mannoni, qui travaille actuellement sur Bismarck, s’inscrit en faux.
Selon le traducteur d’une nouvelle édition du Zéro et l’infini d’Arthur Koestler
(chez Calmann-Levy), l’unité façonnée par Bismarck est avant tout politique :
« Il n’y a pas chez lui l’idée d’un sang allemand à défendre.
Cette pensée vient après, avec cette Révolution conservatrice née à la suite
de la défaite subie durant la 1ere Guerre mondiale. »

« S’il avait avancé un tel projet, les circonstances de cette époque l’auraient
vraisemblablement empêché d’accéder au pouvoir », avance le traducteur.
Les Allemands savaient en revanche ce qu'il adviendrait en votant Hitler  : il
avait largement fait état de ses intentions belliqueuses, promu son antisémitisme
virulent, ou déballé ses griefs contre la culture ou la démocratie.

Olivier Mannoni questionne : « Les gens y ont adhéré, alors que l’Allemagne
était certes secouée, mais dotée d’une civilisation et d’une culture exceptionnelle. »

Il faut en effet remonter aux courants de pensée de la Révolution conservatrice
allemande, portée par des figures comme Carl Schmitt, Ernst Jünger, Ernst Niekisch,
Oswald Spengler, ou plus tard, Armin Mohler, tous plus ou moins proches du
mouvement völkisch.

Ces mouvements apparus au sortir de la Première Guerre mondiale ont « sciemment tout fait pour rompre des tabous qui pesaient en Allemagne
----------------------------------------------------------------------------------------------------------------------------------------------

**Ces mouvements apparus au sortir de la Première Guerre mondiale ont « sciemment
tout fait pour rompre des tabous qui pesaient en Allemagne**.

J’exhume par exemple des textes de Jünger de 1930 qui sont d’un antisémitisme
effrayant, aussi violent que théorisé ». Selon Carl Schmitt, la politique,
c’est désigner l’ennemi.

À partir de 1919, la gauche allemande prend de l’ampleur, notamment en Bavière
où le climat est révolutionnaire. Afin d’empêcher cette « République des conseils »,
ou « République soviétique de Bavière », ou d’arrêter certaines forces du pays,
comme les spartakistes de Rosa Luxembourg, d’anciens militaires réunis dans
des corps francs (lire Les Réprouvés d’Ernst Von Salomon), épaulent l’armée
allemande dans la répression sanglante des forces de gauche.

Ce sont ces anciens combattants et leurs élans de vengeances contre l’Europe
entière qui alimenteront les forces vives du nazisme en germe.
Ce ressentiment se tournera vers des cibles : les juifs, les communistes… et
ce dès 1919.

« Ces textes des penseurs de la Révolution conservatrice, abominables, virilistes,
ont servi de bulldozer pour ouvrir la voie à ce que va porter le nazisme,
quoiqu'en 1930, le NSDAP est déjà devenu très puissant », explique Olivier Mannoni.

La polémique
===============

Ainsi, on le comprend, éditer Mein Kampf est tout sauf anodin, ce dont l’équipe
autour du directeur de l’édition, Florent Brayard a pu faire l'expérience,
même si ses membres auraient apprécié s’en passer.

Fin 2015, à l’approche des élections régionales, Jean-Luc Mélenchon s’empare
d’une dépêche erronée annonçant une nouvelle édition de Mein Kampf à paraître
prochainement.
La polémique est lancée par l’entremise d’un article partagé sur son blog.

En parallèle, il envoie un courrier à son éditrice chez Fayard, Sophie Hogg,
lui demandant de renoncer à cette publication.
Son message en substance : pas Mein Kampf quand il y a déjà Marine Le Pen.
« Formulé tel quel, c’est un sophisme, car ce n’est pas la question »,
réagit Olivier Mannoni.
S’en suivra un débat par médias interposés durant plus de six mois.
Le traducteur constate : « La polémique a tourné à vide, d’une part parce
qu’on nous a fait des procès d’intention qui n’avaient aucun sens, et de
l’autre, car on était très loin d’avoir achevé le travail. »

Dans son essai, Olivier Mannoni synthétise certaines critiques à l’endroit
du projet  : « À quoi bon ressortir ce texte de haine de l’oubli où il a sombré ?
À quoi bon retraduire un texte illisible dont certains, comme Johann Chapoutot,
affirment qu’il n’a pas joué un rôle central dans l’histoire du nazisme et
qu’il ramène Hitler à son statut de pilier d’un régime qui, comme l’ont
amplement démontré aujourd’hui les historiens — entre autres, justement,
Florent Brayard dans son livre sur Auschwitz, enquête sur un complot nazi —,
était avant tout le produit des rapports de force entre des sous-pouvoirs rivaux ? »


Cette querelle a permis de poser le débat sur l’intérêt ou non d’une traduction
de ce type, comme de mettre en évidence les précautions nécessaires à établir
pour la sortie future, « avec des remarques tout à fait justifiées sur notre
démarche et les risques qu’il y avait à publier un livre comme ça ».

Une des craintes de l’époque était notamment celle du best-seller.
« Toutes les dispositions ont été prises du côté de Sophie de Closets et
Florent Brayard pour que ce livre soit véritablement ce qu’il voulait être :
un ouvrage scientifique, utilisable par des chercheurs, des historiens, des
étudiants, qui ne pourrait en aucune façon être mis en valeur pour en faire
un succès commercial dans les librairies et autres points de vente. »

Concrètement, le nom d’Hitler n’est pas sur cette couverture blanche dépourvue
de toute recherche esthétique.
Le titre original de l’ouvrage est en sous-titre d’Historiciser le mal,
« qui résume tout à fait la démarche des historiens, et la mienne : donner
des éléments historiques sur le langage qui est utilisé ».

En outre, il est seulement disponible sur commande sur les sites de ventes
par correspondance, comme Amazon, qui ajoute systématiquement un avertissement
exceptionnel sur la page du livre.

.. figure:: images/avertissement.webp

Difficile à transporter à cause de ses 4 kilos et ses quasi 1000 pages, il
enserre le texte d’Adolf Hitler de notes explicatives et contextualisées
d’une cinquantaine de chercheurs mobilisés, donnant un texte « quasi aussi long »
que la traduction elle-même.

Les bénéfices de la vente sont reversés à la Fondation Auschwitz-Birkenau.
« La notion de travail de mémoire est donc respectée jusqu’au bout. »

Ce format a été inspiré de l’Allemagne, où l’ouvrage était interdit depuis 1945.
Une équipe de scientifiques de l’Institut für Zeitgeschichte (l’institut
d’histoire contemporaine) ont sorti en 2016 une édition en deux « énormes »
volumes, avec un très large appareil critique.

Un format qui ne favorise pas l'accessibilité de l’édition, outre la nécessité
de débourser pas moins de 100 euros pour se la procurer, la condamnant à se
destiner à une certaine élite ?

« L’ouvrage a été donné aux bibliothèques qui le demandaient », répond le
traducteur, et ajoute avec honnêteté : « Il s’adresse à tous ceux qui s’intéressent
à l’Histoire et qui ont un contact soutenu avec les livres.
Je ne pense que le niveau d’érudition des notes rebuterait ceux qui iraient
voir le texte par simple curiosité. »

Il continue : « Ceux qui s’y intéressent vraiment y trouveront des arguments
scientifiques éprouvés, vérifiés, étayés, mais également contre la biographie
que dépeint Hitler de lui-même, qui est en grande partie mensongère.

Par exemple son pseudo-héroïsme pendant la guerre.

Tout est passé au crible. » Et conclut sur la question : « Donc oui, tout le
monde ne va pas lire ce livre imposant, mais les gens qui l’auront lu pourront
répondre.
C’est une manière de donner des phares antibrouillards pour ceux qui savent conduire. »


Que nous dit Mein Kampf des temps présents ?
==============================================

Si le traducteur a vu dans la charge de Jean-Luc Mélenchon une occasion de
« conflictualiser », selon la propre terminologie de l’homme politique,
Olivier Mannoni ne s’extrait pas des problématiques actuelles, souvent soulevées
par le politicien.

Toute la quatrième partie de Traduire Hitler est consacrée au lien entre
les conclusions tirées de sa traduction et les conditions du temps présent :
« Quand on m’a proposé de réaliser ce texte, on était en pleine campagne
électorale en France. Si je ne fais pas de parallèle strict, car la période
historique n’est pas la même et que les idées ne sont pas exactement les mêmes,
je me méfie de la résurgence de certains thèmes, d’un certain vocabulaire,
qui apparaissent avec de nouveaux habits.

Ils sont, à mon avis, tout aussi, voire plus dangereux qu’il y a 30 ans,
parce qu’une mémoire de ce langage a disparu. »

Olivier Mannoni est formel : Il y a une sape intellectuelle qui a été menée
pour banaliser des notions tout sauf banales. « Ce que j’ai entendu, notamment
durant la dernière campagne, et même avant, m’a profondément effrayé »,
nous confie-t-il.

Afin d’étayer son propos dans l’essai, il s’attelle à une histoire de la
résurgence des pratiques linguistiques, sur lesquelles s'est appuyé le nazisme.

Il remonte pour la France aux années 1980, où un travail a été entrepris
pour briser les barrières, que ce soit par Jean-Marie Le Pen et son parti,
ou La Nouvelle Droite d’Alain de Benoist, très inspirés des penseurs de la
Révolution conservatrice allemande.
À partir de sa grille de lecture linguistique, il analyse comment le langage
entrouvre la porte aux idées.

La victoire de Giorgia Meloni et son parti néo-fasciste, Fratelli d’Italia,
aux élections italiennes dans une coalition de droite, en est un exemple
caractéristique selon le germaniste : « Quand on souligne qu’une candidate
se revendique de Mussolini, une dizaine de personnes vous répondent que vous
êtes passéiste.

Non, si on se réclame de Mussolini, on est dans la lignée du fascisme tout simplement. »
--------------------------------------------------------------------------------------------

Non, si on se réclame de Mussolini, on est dans la lignée du fascisme tout simplement. »

Olivier Mannoni ose les comparaisons, appuyé sur 30 ans de traduction de
textes sur le nazisme et faisant fi du fameux « Point Godwin », et dresse
un parallèle entre le langage d’Adolf Hitler et celui de Donald Trump :
« C’est évidemment très exagéré de faire un lien politique ou historique
entre Trump et Hitler, même s’il y a quelques petites comparaisons à faire,
comme la tentative de putsch raté de 9 novembre 1923 et l’assaut du Capitole
du 6 janvier 2021.

Trump n’a rien d’un exterminationniste et ses volontés belliqueuses n’existent pas ;
c’est simplement un populiste, qui lui aussi s’exprime souvent de manière
incompréhensible, cafouilleuse.

Or c’est un homme de marketing, un patron d’entreprise. On ne peut pas penser
que sa communication ne soit pas réfléchie.
Les parallèles peuvent donc se faire au niveau du langage utilisé. »

Celui qui est également critique littéraire et biographe s’est appuyé sur
l’ouvrage de la traductrice de presse, Bérengère Viennot,
La Langue de Trump, paru aux Arènes en 2019.

Elle s’est penchée sur les discours de Donald Trump, à base de mots génériques
tels que sad, great, bad… « C’est impressionnant 3/4 des phrases qu’il prononce
sont incompréhensibles, parce qu’elles sont comme chez Hitler, absurdes,
surchargées, avec des sophismes qui passent par des incorrections de langage. »

On cherche à parler aux tripes directement, à quoi s’accompagne un mélange
de victimisation et d’autosatisfaction de celui qui a le courage d'être,
dans la posture, seul contre tous.
Dans les deux dynamiques, Olivier Mannoni expose une rhétorique « anti-intellectuelle »,
qui caractérise d’ailleurs ce qu’on désigne aujourd’hui par le vocable de
« populiste ». Un « parler-vrai ».
On retrouve aussi ce même discours de la « nation blessée ».

Olivier Mannoni résume : « Je fais bonnement un rapprochement entre la langue
de Mein Kampf et la langue complotiste, qui reprend véritablement des attributs
extrêmement similaires à celle du texte d’Adolf Hitler ou de Rosenberg,
soit avant tout, une accumulation d’arguments indémontrables.

Il y a la volonté de rameuter ce qu’ils appellent le peuple autour d’idées
simples et brumeuses qui empêchent toute réflexion réelle. »

« À l’époque », écrit Hitler en 1924, repris par Olivier Mannoni dans son essai, 
« j’ai adopté ce point de vue : peu importe qu’ils se moquent de nous ou
qu’ils nous injurient, qu’ils nous accusent d’être des pitres ou des criminels ;
l’essentiel est qu’ils parlent de nous, qu’ils n’arrêtent pas de s’occuper
de nous et que nous apparaissions peu à peu aux yeux des ouvriers eux-mêmes,
réellement, comme la seule puissance avec laquelle se déroule aujourd’hui
encore une confrontation. »

Eric Zemmour et Le grand remplacement
==========================================

Si l’ambition de Donald Trump a été de conquérir le pouvoir, d’autres semblent
chercher à casser des tabous forgés par l’Histoire.

Certes, Éric Zemmour s’est présenté aux présidentielles Françaises et sa
participation au Second tour a longtemps été envisagée au vu des sondages,
mais dans les faits, son travail a consisté à imposer un discours :
« Quand vous poussez loin le langage, ce qui était considéré comme radical,
devient modéré.
Comme pour le hors-jeu au football : vous allez tellement près des buts que
le hors-jeu s’amenuise. »


Cette ambition de faire sauter ce qui est pointé sous le terme de « bien-pensance »,  ce qu’Hitler appelait déjà ça « gutdenken », est assumée par l’extrême droite  depuis 30 ans, constate le traducteur
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


Cette ambition de faire sauter ce qui est pointé sous le terme de « bien-pensance »,
ce qu’Hitler appelait déjà ça « gutdenken », est assumée par l’extrême droite
depuis 30 ans, constate le traducteur.

Dans Mein Kampf, les ennemis désignés le sont à tout point de vue : économique,
« ils nous pillent et nous dominent » ; sécuritaire, « les juifs sont dangereux,
la population immigrée qui viennent nous voler, nous tuer… » ;

même médicale,  avec l’idée que les juifs apporteraient des maladies…
« Une téléologie nationaliste qui s’exprime dans la volonté de reprendre
les armes idéologiques pour lutter contre une invasion supposée. »

Sur les questions raciales, Olivier Mannoni est formel : « Il y a un parallèle
évident entre le chapitre sur la race (Le 11e du 1er livre) de Mein Kampf
et certaines théories de l’extrême droite aujourd’hui. »

Selon ce dernier, la notion de « grand remplacement » est une vieille idée
déjà présente dans le nazisme, même si elle ne portait pas ce nom.
Elle fit ensuite florès dans l’après-Seconde guerre mondiale en Allemagne, aux États-Unis…


Le traducteur condense le concept, comme « l’idée paranoïaque qu’il y a une
volonté de l’extérieur (l’Union européenne, manipulateurs mondiaux…)
d’imposer à une population européenne, comme si le peuple français était
un peuple de sang pur, des politiques globales, comme celle du métissage généralisé. »

Le concept de grand remplacement est passé d’une théorie paranoïaque de
« certaines individualités », à repris par les grands partis institutionnels,
comme Valérie Pécresse, candidate des Républicains durant la dernière présidentielle.

« Le terme est à présent appliqué à tout bout de champ, faisant totalement
abstraction de la problématique sociale », résume Olivier Mannoni, qui
ajoute : « En refaisant des ghettos sociaux, avec 45 % de chômage et s’étonner
que ça ne fonctionne pas, alors on va gloser sur la race, qui fait l’économie
de la répartition des richesses dans un pays.


La simplification permet de désigner un ennemi. » « Nous assistons à la remontée des égouts de l’histoire. Et nous nous y accoutumons. »
-------------------------------------------------------------------------------------------------------------------------------------------

**La simplification permet de désigner un ennemi. » « Nous assistons à la remontée des égouts de l’histoire. Et nous nous y accoutumons. »**


Dans son essai, Traduire Hitler, le traducteur de Freud et de Goethe conclut :
« Dans ces conditions, à quoi pouvait bien servir de rééditer ce livre et
d’en refaire une traduction ?

Sur le plan politique, sans doute pas à grand-chose.

Mais ce n’était pas son but. Le travail des historiens n’est pas destiné à
rééduquer les esprits.

Il est là pour étudier les faits, les exposer, les commenter en les replaçant
dans leur contexte.

C’est un travail scientifique qui pose les fondations d’un savoir, donne aux
lecteurs les moyens d’élaborer une pensée à partir d’éléments vérifiés.

Historiciser le mal répond à ces exigences. »
