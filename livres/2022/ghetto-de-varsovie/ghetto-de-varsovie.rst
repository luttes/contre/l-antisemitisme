.. index::
   pair: Livres; Ghetto de Varsovie (2022)

.. _livre_ghetto_de_varsovie:

=================================================================================
**Ghetto de Varsovie** Carnets retrouvés avant-propos d'Aleksander Edelman
=================================================================================

- https://www.odilejacob.fr/catalogue/histoire-et-geopolitique/guerres-mondiales/ghetto-de-varsovie_9782415001766.php
- :ref:`aleksander_edelmann_2023_04_16`

.. figure:: images/ghetto_varsovie.png

2 octobre 2009, Varsovie. Marek Edelman s’éteint.

Figure de l’opposition au régime communiste polonais, il est célèbre
d’abord pour avoir été l’un des dirigeants du soulèvement du ghetto
de Varsovie en 1943.

Membre du Bund, le mouvement socialiste des travailleurs juifs, il
participe à ses publications clandestines.

Quand les nazis décident de liquider le ghetto, il fait partie de ceux
qui se savent condamnés mais ne veulent pas mourir sans combattre.

Une poignée d’hommes contre une armée.

Marek Edelman ne posait pas au héros.

« Nous avions décidé de mourir les armes à la main. C’est tout.

C’est plus facile que de donner ses habits à un Allemand et de marcher
nu vers la chambre à gaz. »

Juif non religieux, non sioniste, c’était un éternel insoumis.

Il avait publié en 1945 un récit sur le ghetto et son soulèvement, puis
des entretiens.

Le jour de son enterrement, ses enfants, Aleksander et Ania Edelman,
retrouvent dans son appartement trois carnets, où il avait consigné à
la fin des années 1960 des souvenirs du ghetto, sans aborder le soulèvement.

Ce sont ces carnets retrouvés que nous publions ici, avec un appareil
de notes et d’annexes permettant la compréhension de ce document exceptionnel.

Édition établie par Constance Pâris de Bollardière, historienne
spécialiste du Bund et des rescapés de la Shoah, directrice adjointe
du George and Irina Schaeffer Center for the Study of Genocide, Human Rights
and Conflict Prevention (The American University of Paris).
