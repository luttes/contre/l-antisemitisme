

.. _leschi_2022_cinema:

===================================================================================
2022 **Être juif dans le cinéma hollywoodien classique** par Lorenzo Leschi
===================================================================================

- https://www.vrin.fr/livre/9782711630561/etre-juif-dans-le-cinema-hollywoodien-classique


.. figure:: images/cinema_juif.webp

Heureux comme un Juif à Hollywood ?
===========================================

Le cinéma hollywoodien constitue l’un des meilleurs exemples de la réussite
des Juifs aux États-Unis et de leurs apports à la culture et à l’imaginaire
américain.

**Paradoxalement, sur les écrans, les personnages juifs ont surtout brillé
par leur absence**.

En effet, le succès des immigrés juifs qui ont fondé Hollywood s’est construit
sur le refoulement brutal du judaïsme et des thématiques juives.

Seuls quelques films portent la trace des contradictions internes des
"Juifs d’Hollywood", tiraillés entre assimilation et affirmation d’une
identité juive, entre haine de soi et résistance à l’antisémitisme,
entre espoir et angoisse de disparaitre en tant que Juifs.

- Quels sont ces films ?
- Que révèlent-ils du rapport à l’identité juive à Hollywood et aux États-Unis ?
- Quelle influence ont-ils eu sur la culture et la société américaine?


L’Auteur
============

Lorenzo Leschi, diplômé en études cinématographiques de l’Université de Paris,
consacre actuellement ses recherches aux relations entre Juifs et Africains-Américains
dans le cinéma américain.
