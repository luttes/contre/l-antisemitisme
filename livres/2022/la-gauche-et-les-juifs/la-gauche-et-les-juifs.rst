.. index::
   pair: Livre; La gauche et les juifs (Robert Hirsh, 2022)

.. _la_gauche_et_les_juifs:

=================================================================================
|GaucheEtJuives| **La gauche et les juifs** de Robert Hirsch
=================================================================================

- :ref:`robert_hirsh`
- https://www.editionsbdl.com/produit/la-gauche-et-les-juifs/
- https://www.odilejacob.fr/catalogue/histoire-et-geopolitique/guerres-mondiales/ghetto-de-varsovie_9782415001766.php
- :ref:`aleksander_edelmann_2023_04_16`

.. figure:: images/la_gauche_et_les_juifs.png

Des pancartes dénonçant des personnalités juives comme responsables de
la situation sanitaire ou comparant le pass à l’étoile jaune, c’était en
France à l’été 2021.

L’antisémitisme ne cesse d’interpeller la société française, et
particulièrement la gauche.

Elle fut souvent proche des Juifs depuis la Révolution française.

L’Affaire Dreyfus, puis la Résistance, confirmèrent ce rapprochement,
même si, à chacun de ces moments, des réticences à combattre
l’antisémitisme se firent jour à gauche.

Nombre de jeunes Juifs furent des acteurs de mai 68 et la gauche, toutes
tendances confondues, se montrait alors intransigeante à l’égard d’un
antisémitisme relativement cantonné à l’extrême droite.

Le début du XXIe siècle marqua un tournant, avec une recrudescence
d’actes antisémites suivis des assassinats d’Ilan Halimi, des enfants
de l’école juive de Toulouse, de l’hyper cacher…

Une partie de la gauche ne voulut pas réagir à cet antisémitisme, issu
de milieux eux-mêmes victimes de racisme alors que les hésitations de
la gauche radicale se mélangeaient à la question de l’antisionisme.

Le présent ouvrage se propose de tenter de comprendre les raisons de ce
divorce avec des Juifs légitimement inquiets du renouveau antisémite.

Pour ce faire, il revient sur l’histoire des populations juives, marquée
par l’antisémitisme.
Une histoire qui montre la force de ce qui relie entre eux les Juifs
du monde entier. L’ouvrage analyse la réalité de l’antisémitisme dans
la France de ce début du XXIe siècle.

Analyser l’antisémitisme actuel, c’est aussi se poser la question d’Israël.

Au-delà de la dramatique actualité israélo-palestinienne, il importe de
revisiter l’histoire du projet sioniste et de sa mise en œuvre.


Rencontres 2024
============================

- :ref:`raar_2024:robert_hirsch_2024_10_14`
- :ref:`raar_2024:hirsch_2024_05_26`

Cité en 2023
================

- :ref:`robert_hirsch_2023_11_18`
