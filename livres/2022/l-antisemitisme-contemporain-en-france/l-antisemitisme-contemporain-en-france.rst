.. index::
   pair: Livre; L'antisémitisme contemporain en France : rémanences ou émergences ? (2022)

.. _livre_antisem_2022_remanences:

===========================================================================
**L'antisémitisme contemporain en France** : *rémanences ou émergences ?*
===========================================================================

- https://www.cairn.info/l-antisemitisme-contemporain-en-france--9791037021717.htm


.. figure:: images/couverture.webp

Auteures
===========

- https://www.cairn.info/publications-de-Jo%C3%ABlle-Allouche-Benayoun--51196.htm
- https://www.cairn.info/publications-de-Claudine-Attias-Donfut--12023.htm
- https://www.cairn.info/publications-de-G%C3%BCnther-Jikeli--139088.htm
- https://www.cairn.info/publications-de-Paul-Zawadzki--6172.htm


Description
==============

Contre toute attente, l’antisémitisme est redevenu un phénomène politique
et social d’actualité en France.

Au-delà d’une simple persistance de préjugés anciens, il s’incarne depuis
la fin de la guerre de 1939-1945 dans une expansion sans précédent des
violences antijuives, allant des insultes jusqu’au meurtre.

Sa reviviscence dans l’une des plus vieilles nations
démocratiques d’Europe est le signe d’un malaise social et politique qui
reste à interpréter.

- Quels sont les lieux, les milieux et les significations de cette animosité haineuse ?
- Doit-on y voir l’effet de la persistance de l’ancien ? d’un déplacement ?
- de la recomposition de formes inédites, portées par des acteurs nouveaux,
  dans un contexte pourtant gagné aux principes démocratiques ?

Alors même que l’ampleur du phénomène est de nature à mobiliser les
sciences sociales pour l’élucider et aider à le combattre, cet antisémitisme
s’est accompagné d’un certain déni : longtemps minimisé dans les médias,
il peine à être reconnu dans le monde judiciaire et reste peu exploré par les
sciences sociales.

Cet ouvrage entend donc contribuer à l’analyse rigoureuse du phénomène,
en proposant à la réflexion les textes d’historiens, de philosophes,
de sociologues et de politistes.


Mis en ligne sur Cairn.info le 22/12/2022

ISBN
    9791037021717

SBN en ligne
    9791037021724
    https://doi.org/10.3917/herm.allou.2022.01

Articles/confrérences 2022
===============================

- :ref:`raar_2024:livre_antisem_2022_remanences_2024_02_01`
