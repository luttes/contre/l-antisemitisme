.. index::
   pair: Livre; La Rafle du Vél d'Hiv (Lautent Joly, 2022)

.. _joly_livre_2022:
.. _rafle_vel_dhiv:

====================================================================
**La Rafle du Vél d'Hiv** par Laurent Joly (2022)
====================================================================

- :ref:`laurent_joly`

.. figure:: images/recto.webp

La rafle dite du "Vel d’Hiv" est l’un des événements les plus tragiques
survenus en France sous l’Occupation.

En moins de deux jours, les 16 et 17 juillet 1942, 12 884 femmes, hommes et
enfants, répartis entre Drancy (près de 4 900) et le Vel d’Hiv (8 000), ont
été arrêtés par la police parisienne à la suite d’un arrangement criminel
entre les autorités allemandes et le gouvernement de Vichy.

Seule une petite centaine de ces victimes survivra à l’enfer des camps nazis.

Cette opération  emblématique et monstrueuse demeure pourtant relativement
méconnue.

L’arrière-plan administratif et la logistique policière de la grande rafle
n’ont été que peu étudiés, et jamais dans le détail.

Légendes (tel le nom de code"opération Vent Printanier") et inexactitudes
(sur le nombre de personnes arrêtées ou celui des effectifs policiers) sont
répétées de livre en livre.

Et l’on ignore que jamais Vichy ne livra plus de juifs français à l’occupant
que le 16 juillet 1942  !

D’où l’ambition, dans cet ouvrage, d’une histoire à la fois incarnée et globale
de la rafle du Vel d’Hiv.
Une histoire incarnée, autrement dit au plus près des individus, persécutés
comme persécuteurs, de leur état d’esprit, de leur vécu quotidien,
de leurs marges de décision.

Mais aussi une histoire globale, soucieuse de restituer la multiplicité des
points de vue, des destinées, et attentive au contexte de la politique nazie
et de la collaboration d’État.

Une recherche largement inédite, la plus riche et variée possible, de la
consultation de centaines de témoignages à une exploitation inédite des
"fichiers juifs" de la Préfecture de police de Paris.

Mais la partie la plus importante de l’enquête a consisté à rechercher des
"paroles" de policiers : 4 000 dossiers d’épuration des agents de la préfecture
de police ont été dépouillés.

Parmi eux, plus de 150 abordent la grande rafle et ses suites.

Outre les justifications de policiers, ces dossiers contiennent des
paroles de victimes, des témoignages (souvent accablants) de concierges, et
surtout des copies de rapports d’arrestation, totalement inédits.

Fruit de plusieurs années de recherche menées par l'auteur, où les archives
de la police et de l’administration auront été méticuleusement fouillées, La
Rafle du Vel d’Hiv  apporte une lumière nouvelle sur l’un des événements
les plus terribles et les plus difficiles à appréhender de notre histoire
contemporaine.
