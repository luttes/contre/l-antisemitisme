.. index::
   pair: Livre; "Des Blancs comme les autres ? Les Juifs, angle mort de l'antiracisme" par  2022

.. _weizman_livre_2022:

=====================================================================================================
**Des Blancs comme les autres ? Les Juifs, angle mort de l'antiracisme** par Illana Weizman (2022)
=====================================================================================================

- :ref:`illana_weizman`

Auteure: Illana Weizman
============================

- https://www.editions-stock.fr/auteur/illana-weizman/


.. figure:: images/recto_livre.webp

Description
=================

Dans un contexte où le combat antiraciste revient sur le devant la scène, la
lutte contre l’antisémitisme semble être restée en marge.

Pire, l’extrême droite, vecteur historique et premier de cette vieille haine,
ose même prétendre être le chantre de la "défense" des Juifs".

C’est oublier que les Juifs ne sont pas "blancs" au sens sociologique du terme.

Comme les autres racismes, il fait système : du cliché sur des traits physiques
ou moraux, à l’insulte, jusqu’au meurtre, il y a un continuum.

L’antisémitisme est un racisme, mais pourquoi n’est-il pas considéré comme
tel au sein des luttes antiracistes ?

Illana Weizman s’attache à en décrypter les raisons en partant de sa propre
expérience :

- **l’idée que les Juifs sont privilégiés (cliché antisémite s’il en est)**,
- la mise en compétition des différentes minorités,
- la confusion avec l’antisionisme…

Et si les milieux de gauche ne sont pas intrinsèquement antisémites, leur
complaisance laisse le terrain à des discours stigmatisant en particulier
les Noirs et les Arabes, leur faisant porter le chapeau de l’antisémitisme contemporain.

Reconnaître ces biais relève de la décence, mais également de l’efficacité.

Si toutes les luttes antiracistes ne convergent pas, nous en sortons tous perdants.

Car, rappelle l’autrice avec Frantz Fanon::

    "Quand vous entendez dire du mal des juifs, dressez l’oreille : on parle de vous."
