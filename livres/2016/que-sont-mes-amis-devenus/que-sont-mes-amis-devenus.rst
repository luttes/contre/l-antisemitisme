.. index::
   pair: Livre; Que sont mes amis devenus...: Les Juifs, Charlie, puis tous les nôtres (2016, Brigitte Stora)

.. _stora_livre_2016:

================================================================================================
**Que sont mes amis devenus...: Les Juifs, Charlie, puis tous les nôtres** par Brigitte Stora
================================================================================================

- https://www.editionsbdl.com/
- https://www.editionsbdl.com/produit/que-sont-mes-amis-devenus-les-juifs-charlie-puis-tous-les-notres/

.. figure:: images/couverture.png

   https://www.editionsbdl.com/produit/que-sont-mes-amis-devenus-les-juifs-charlie-puis-tous-les-notres/


Introduction Editions BDL
==================================

Janvier 2015. "Le pire c’est que nous savions…".

Comme si cette destruction nous était intime, connue.

Depuis quinze ans, nous étions presque seuls, ciblés, abandonnés.

Charlie aussi avait été lâché, puis criminalisé et assassiné.

A sa manière, il était devenu un peu juif.

Onze mois plus tard, plus de 130 jeunes seront assassinés au Bataclan et
sur les terrasses des cafés à Paris.
La France toute entière est désormais visée.

Pendant des décennies, l’idéal révolutionnaire d’une certaine gauche a
épousé le désir d’émancipation, la soif de fraternité.
Nous pensions qu’après la Catastrophe, la source du venin était tarie,
nous avons cru en un monde pluriel où nous avions enfin notre place.

Mais les amants du chaos se sont réveillés et abreuvent les jeunes
générations de leur fiel et de leur ressentiment.

Aux mots ont succédé les meurtres : celui d’Ilan Halimi, celui des militaires
et des enfants juifs de Toulouse,  des clients d’un magasin cacher puis
le mitraillage de toute une rédaction.

L’horreur des attentats de novembre a plus clairement encore ciblé le "nous",
celui que depuis longtemps ils détruisent.

Aujourd’hui la "révolte" s’avance trop souvent contaminée par la haine
au point parfois de relayer insidieusement la terreur fasciste, de lui
donner une justification.

Alors que du temps de nos engagements,  le choix de la vie dominait, je
me demande "que sont mes amis devenus ?".

Après des études de sociologie et d’Ethique, Brigitte Stora est devenue
journaliste indépendante et chanteuse.
