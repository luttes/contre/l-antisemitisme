.. index::
   pair: Comment la Palestine fut perdue Et pourquoi Israël n'a pas gagné. Histoire d'un conflit (XIXe-XXIe siècle); Jean-Pierre Filiu (2024)

.. _livre_filiu_2024_02:

=====================================================================================================================================================
2024-02-09 **Comment la Palestine fut perdue Et pourquoi Israël n'a pas gagné. Histoire d'un conflit (XIXe-XXIe siècle)** par Jean-Pierre Filiu
=====================================================================================================================================================

- https://www.seuil.com/ouvrage/comment-la-palestine-fut-perdue-jean-pierre-filiu/9782021538335
- https://www.librairiesindependantes.com/product/search/?query=Comment+la+Palestine+fut+perdue+Et+pourquoi+Isra%C3%ABl+n%27a+pas+gagn%C3%A9


Auteur Jean-Pierre Filiu
============================

- https://www.seuil.com/auteur/jean-pierre-filiu/33198

Jean-Pierre Filiu est professeur des universités en histoire du Moyen-Orient
contemporain à Sciences Po (Paris).

Ses travaux sur le monde arabo-musulman ont été publiés dans une quinzaine
de langues.

Il est l'auteur de nombreux ouvrages, dont:

- Les Arabes, leur destin et le nôtre,
- et Généraux, gangsters et jihadistes (La Découverte, 2015), Le Milieu des mondes.
- Une histoire laïque du Moyen-Orient de 395 à nos jours (Seuil, 2021 ; "Points Histoire", 2023)
- et Stupéfiant Moyen-Orient. Une histoire de drogue, de pouvoir et de société (Seuil, 2023).

Description du livre
==========================

::

    Histoire
    Date de parution 09/02/2024
    24.00 € TTC
    432 pages
    EAN 9782021538335
    ÉDITIONS DU SEUIL 57, rue Gaston-Tessier, Paris XIXe
    ISBN 978-2-02-153833-5 © Éditions du Seuil, février 2024


Si vous estimez connaître assez du conflit israélo-palestinien pour en
nourrir des opinions définitives, mieux vaut ne pas ouvrir ce livre.

Vous risqueriez d’y apprendre que le sionisme fut très longtemps chrétien avant
que d’être juif.

Et que l’évangélisme anglo-saxon explique beaucoup plus qu’un fantasmatique
"lobby juif" le soutien déterminant de la Grande-Bretagne, puis des États-Unis
à la colonisation de la Palestine.

Vous pourriez aussi découvrir que **la soi-disant "solidarité arabe" avec la
Palestine a justifié les rivalités entre régimes pour accaparer cette cause
symbolique, quitte à massacrer les Palestiniens qui résistaient à de telles
manoeuvres**.

Ou que la dynamique factionnelle a, dès l’origine, miné et affaibli le nationalisme
palestinien, culminant avec la polarisation actuelle entre le Fatah de Ramallah
et le Hamas de Gaza.

La persistance de l’injustice faite au peuple palestinien n’a pas peu contribué
à l’ensauvagement du monde actuel, à la militarisation des relations internationales
et au naufrage de l’ONU, paralysée par Washington au profit d’Israël durant
des décennies, bien avant de l’être par Moscou sur la Syrie, puis sur
l’Ukraine.

L’illusion qu’un tel déni pouvait perdurer indéfiniment a volé en éclat dans
l’horreur de la confrontation actuelle, d’autant plus tragique qu’aucune
solution militaire ne peut être apportée au défi de deux peuples vivant
ensemble sur la même terre.

Comprendre comment la Palestine fut perdue, et pourquoi Israël n’a pourtant
pas gagné, participe dès lors d’une réflexion ouverte sur l’impératif d’une
paix enfin durable au Moyen-Orient et, donc, sur le devenir de ce nouveau millénaire.


Extrait
==========

- https://ref.lamartinieregroupe.com/media/9782021538335/153833_extrait_Extrait_0.pdf


Je ne fais pas la différence entre l’optimisme et le pessimisme,
et je me demande bien lequel des deux me caractérise le mieux. Le matin, quand
je me réveille, je remercie Dieu de ne m’avoir pas fait périr pendant la
nuit. Si, dans la journée, il m’arrive quelque chose de désagréable, je
le remercie de ce qu’il ne me soit rien arrivé de pire. Que suis-je donc,
optimiste ou pessimiste ?  Émile Habibi, Les Aventures extraordinaires de
Saïd le peptimiste, 19741
