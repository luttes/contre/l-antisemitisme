.. index::
   pair: Livre; Nous vivrons (2024)

.. _sfar_2024:

===============================================================================================
**Nous vivrons** Enquête sur l'avenir des Juifs de Joahnn Sfar (2024)
===============================================================================================

- https://arenes.fr/livre/nous-vivrons/


.. figure:: images/nous_vivrons.webp


Un roman graphique puissant et intime
==========================================

Après le 7 octobre 2023, des millions de Juifs se sont réveillés avec une
cible sur la tête. Même les plus éloignés de la tradition ou d’Israël ont
été rattrapés par l’onde de choc. Le traumatisme des pogroms millénaires et
de l’extermination des Juifs d’Europe a refait surface. Que faire ? Effacer
son nom sur la boîte aux lettres ? Avoir peur pour les enfants ? Où aller si
« cela » recommence ?

Dans ce livre fondateur, Joann Sfar mène l’enquête. Il discute avec ses amis,
convoque son père et son grand-père, cherche des réponses dans les livres et
dans l’humour. Il se rend en Israël à la rencontre des Juifs et des Arabes,
avec toujours la même question, obsédante : quel avenir pour les Juifs ?
