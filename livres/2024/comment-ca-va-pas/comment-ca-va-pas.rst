.. index::
   pair: Livre; Comment ça va pas ? Conversations après le 7 octobre 2023 par Delphine Horvilleur (2024)

.. _horvilleur_2024_02:

===============================================================================================
**Comment ça va pas ? Conversations après le 7 octobre 2023** par Delphine Horvilleur (2024)
===============================================================================================

- https://www.grasset.fr/livre/comment-ca-va-pas-9782246838463/

.. figure:: images/couverture_recto.webp


Fracassée comme tant d’autres après le massacre perpétré par le Hamas le
7 octobre 2023 en Israël, l’auteur voit son monde s’effondrer.

Elle dont la mission consiste à porter la souffrance des autres sur ses épaules
et à la soulager par ses mots, se trouve soudain en état de sidération,
impuissante et aphasique.

Dans la fièvre, elle écrit alors ce petit traité de survie, comme une tranche
d’auto-analyse qui la fait revenir sur ses fondements existentiels.

Le texte est composé de dix conversations réelles ou imaginaires  :

- conversation avec ma douleur,
- conversation avec mes grands-parents,
- conversation avec la paranoïa juive,
- conversation avec Claude François,
- conversation avec les antiracistes,
- conversation avec Rose,
- conversation avec mes enfants,
- conversation avec ceux qui me font du bien,
- conversation avec Israël,
- conversation avec le Messie.

Ce livre entre en résonnance avec Vivre avec nos morts (puisqu’il s’agit ici,
a contrario,   de l’angoisse de mourir avec les vivants), avec Réflexions s
ur la question antisémite (puisque c’est le pendant personnel, intime et
douloureux à l’essai plus intellectuel et réflexif) et à Il n’y a pas de Ajar
(puisque la musique, le ton, la manière des dialogues oraux font écho à
ceux du monologue théâtral).

Comme toujours avec l’auteur, le va et vient entre l’intime et l’universel,
entre l’exégèse des textes sacrés et l’analyse de la société actuelle,
entre la gravité du propos et l’humour comme politesse du désespoir, parvient
à transformer le déchirement en réparation, l’inconfort en force, l’inquiétude
en réassurance et le doute en savoir.
