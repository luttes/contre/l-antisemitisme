.. index::
   pair: Livre; Histoire de l’autre, Israël-Palestine (2024-01)

.. _histoire_de_lautre_2024:

====================================================================================================================================
**Histoire de l’autre, Israël-Palestine** nouvelle édition 2024  par Collectif, Préface David Chemla, Postface Élie Barnavi
====================================================================================================================================

- https://www.lianalevi.fr/catalogue/histoire-de-lautre/
- https://www.librairiesindependantes.com/

.. figure:: images/histoire_de_l_autre.webp

Traductrice et traducteur
==============================

- Traduit de l’arabe par Rachid Akel,
- traduit de l’hébreu par Rosie Pinhas-Delpuech

Description
=================

Deux peuples, deux récits.

En temps de guerre, chacun raconte l’histoire d’un seul point de vue – le sien –,
le seul considéré comme «juste».

C’est pourquoi six professeurs palestiniens et six professeurs israéliens
ont décidé de réunir dans un même livre l’histoire racontée d’un côté et
de l’autre autour de trois dates clés

- la déclaration Balfour de 1917,
- la guerre de 1948
- et la première Intifada de 1987.

Utilisé depuis 2002 dans de nombreux lycées d’Israël et de Palestine, puis
de France, cet ouvrage constituait un défi quand il a été publié,
il y a vingt ans. Un défi indispensable aujourd’hui, après le 7 octobre 2023.

Préface de David Chemla
Postface d’Élie Barnavi

Commentaires
=============

Valérie Zenatti
---------------------

«Un double récit qui donne beaucoup de clés pour comprendre les événements
et la narration qui en est faite, des deux côtés.
Je recommande ce livre aux adolescents. Et aux adultes aussi.»

Valérie Zenatti

France Inter
------------------

::

    "L’amorce d’un dialogue dans un contexte de violence exacerbée." Télérama
    "Deux récits parallèles qui permettent de mieux comprendre les divergences."

France Inter

L’Obs
-----------

::

    Un texte indispensable.

L’Obs

Élie Barnavi
-----------------

::

    Un petit livre promesse d’un avenir meilleur.

Élie Barnavi

David Chemla
-----------------

- https://www.youtube.com/watch?v=K7XkpxaaDOA&ab_channel=TV5MONDEInfo

David Chemla sur TV5 Monde pour "Histoire de l'Autre".

.. youtube:: K7XkpxaaDOA

Mentionné
============

- :ref:`st_france:charte_recits_divers`

Mentionné en 2024
-------------------------

- :ref:`raar_2024:histoire_autre_2024_11_27`
