.. index::
   pair: Livre; Petit manuel de lutte contre l'antisemitisme (2024, Jonas Pardo et Samuel Delor)

.. _petit_manuel_2024:

==================================================================================================================
**Petit manuel de lutte contre l'antisemitisme** par Jonas Pardo et Samuel Delor (paru le 25 octobre 2024)
==================================================================================================================

- https://www.hobo-diffusion.com/catalogue/9791095630791-petit-manuel-de-lutte-contre-l-antisemitisme
- :ref:`jonas_pardo`

.. figure:: images/couverture.png


.. figure:: images/rencontre.webp


.. figure:: images/message_jjr.webp



Résumé
==============

Ce livre est un outil à destination du mouvement social dont les auteurs
se revendiquent.

Il retrace la longue histoire de la construction des accusations faites
aux Juifs, de l'antijudaïsme antique aux théories du complot et à l'antisémitisme
contemporain.

Outil pratique, le manuel permet de comprendre le processus de racialisation
des Juifs sur le temps long et développe des analyses et propositions
concrètes à propos de l'antisémitisme en développement depuis les années 2000.

Le manuel permet au lecteur d'embrasser la complexité du phénomène, son
caractère systémique, ses implications sociales et politiques, et s'approprier
des outils pour rejoindre le combat antiraciste.


Références
==============

- https://www.editionsducommun.org/

- `Éditions du commun <https://www.editionsducommun.org/>`_ Collection Petit manuel
- Préface Pierre SAVY
- Format 14 x 21 cm
- Page 300 pages
- Prix 18 €
- Date de parution 25/10/2024
- ISBN= 9791095630791
- Genre Essais


Dans les media 2025
======================


- :ref:`raar_2025:jonas_pardo_2025_02_19`
- :ref:`raar_2025:jonas_pardo_2025_02_14`
- https://juivesetjuifsrevolutionnaires.fr/2025/01/27/petit-manuel-de-lutte-contre-lantisemitisme-ils-en-ont-parle/
- https://www.humanite.fr/politique/antisemitisme/le-vecu-des-juifs-en-france-doit-etre-entendu-estime-jonas-pardo-directeur-de-la-boussole-antiraciste

Dans les media 2024
======================

- https://x.com/jonaspardoform/status/1850178828502507729 (Une 100e de
  personnes présentes pour le lancement du Petit manuel de lutte contre
  l'antisémitisme à la librairie Le Pied à Terre @lepiedaterreLib
  Ici nous lisons un extrait de la lettre des ouvriers juifs de Paris au
  Parti socialiste rédigée en 1898, en pleine affaire Dreyfus.
- https://drive.google.com/file/d/10oR1-fR_dyZRmN-SSADw0CxrnPHRIuoc/view
  (Une recension du livre, par La Revue Prolétarienne. Pascal Busquet, "Un outil indispensable", à décryter)
- :ref:`raar_2024:jonas_pardo_2024_12_10`
- https://revolutionproletarienne.wordpress.com/2024/12/09/la-revolution-proletarienne-n827/
- https://www.youtube.com/watch?v=1DyrIFyFSS8 (mardi 10 décembre 2024, Le Tambour Université Rennes 2, [ Mardi de l'égalité ] Introduction à la lutte contre l'antisémitisme)
- https://www.nouvelobs.com/idees/20241128.OBS97113/il-ne-faut-pas-opposer-la-lutte-contre-l-antisemitisme-et-la-solidarite-avec-les-palestiniens.html
- :ref:`raar_2024:mahj_2024_11_19`
- https://k-larevue.com/tsedek-ujfp-exception/
- :ref:`raar_2024:jonas_pardo_2024_10_25`
- :ref:`raar_2024:jjr_2024_10_09`
- :ref:`raar_2024:pardo_2024_07_13`
- :ref:`raar_2024:ref_manuel_pardo_2024_06_26`
- https://www.tenoua.org/comment-le-diable-antisemite-se-loge-dans-les-details/
- https://www.tenoua.org/lantisemitisme-na-pas-de-couleur-politique/


Extraits
===========

.. toctree::
   :maxdepth: 3

   chapitre-3/chapitre-3
