
.. _petit_manuel_chapitre_3:

==================================================================================================================
**Combattre l'antisémitisme aujourd'hui**
==================================================================================================================


.. toctree::
   :maxdepth: 4

   armer-les-gauches-et-le-mouvement-social-contre-l-antisemitisme/armer-les-gauches-et-le-mouvement-social-contre-l-antisemitisme
