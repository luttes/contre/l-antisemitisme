.. index::
   pair: Livre; Israël-Palestine : Une Terre Pour Deux (2024-09)

.. _une_terre_pour_deux_2024:

=========================================================================================================
2024-09 **Israël-Palestine : Une Terre Pour Deux** par Véronique Corgibet et Gérard Dhôtel
=========================================================================================================

- https://www.actes-sud.fr/catalogue/jeunesse/israel-palestine-une-terre-pour-deux
- https://www.librairiesindependantes.com/product/9782330191078/


.. figure:: images/recto.webp


Israël-Palestine : Une Terre Pour Deux
===============================================

Nouvelle édition d'un livre indispensable pour comprendre le conflit israélo palestinien.


Auteures: Véronique Corgibet et Gérard Dhôtel et Arno
===========================================================

Véronique Corgibet
-----------------------

- https://www.actes-sud.fr/contributeurs/veronique-corgibet

Enseignante puis journaliste, Véronique Corgibet a déjà écrit plusieurs
ouvrages pour la jeunesse parmi lesquels

- L’Encyclo verte aux éditions Casterman et un roman,
- Mes parents ne pensent qu’à eux aux éditions Milan.

Chez Actes Sud Junior, elle est l’auteur de trois ouvrages dans la collection
“À petits pas” :

- Les Transports,
- La guerre et la paix,
- Les Inventions et Les transports (également édité dans la collection "À très petits pas").


Arno
-----

- https://www.actes-sud.fr/contributeurs/arno
