.. index::
   pair; BD ; Ken Krimstein

.. _laine_2024_01_17:

======================================================================================================================================================================
2024-01-17 Ken Krimstein adapte en BD des récits des années 30 : «J’ai voulu montrer que le quotidien de ces jeunes juifs était difficile» par Virginie Bloch-Lainé
======================================================================================================================================================================


- https://www.liberation.fr/culture/livres/ken-krimstein-adapte-en-bd-des-recits-des-annees-30-jai-voulu-montrer-que-le-quotidien-de-ces-jeunes-juifs-etait-difficile-20240117_TKCM4GH6LJETVODSHWLXFDYG5Y/

Journaliste
=============

- https://www.liberation.fr/auteur/virginie-bloch-laine/


Ken Krimstein adapte en BD des récits des années 30 : «J’ai voulu montrer que le quotidien de ces jeunes juifs était difficile»
=================================================================================================================================

Ken Krimstein adapte en BD des récits des années 30 : «J’ai voulu montrer
que le quotidien de ces jeunes juifs était difficile»

A partir d’autobiographies de jeunes découvertes en 2017 à Vilnius,
le dessinateur américain retrace la vie dans le Yiddishland à l’aube de
la guerre.


Cachées dans les orgues et dans les confessionnaux, 180 000 pages de documents en yiddish ont réapparu
----------------------------------------------------------------------------------------------------------

Dans «Vivre», Ken Krimstein adapte six autobiographies d'adolescents des
années 1930. (Ed. Christian Bourgeois)

Est-ce la découverte «la plus importante pour l’histoire juive depuis les
manuscrits de la mer Morte» ? David Fishman, membre du conseil du département
d’histoire du séminaire théologique juif des Etats-Unis, le déclare en
tout cas en 2017 lorsqu’il voit de ses propres yeux, à Vilnius, des textes
tout juste exhumés par des ouvriers qui remettaient en état une cathédrale
désaffectée. Cachées dans les orgues et dans les confessionnaux, 180 000
pages de documents en yiddish ont réapparu. Dans cette masse se trouvent des
autobiographies rédigées dans les années 1930 par des jeunes âgés de 16
à 22 ans. Ils avaient été incités à prendre la plume par le Yivo, une
institution créée en 1925 à Wilno, le nom de Vilnius quand la ville était
polonaise. Le Yivo était le centre d’étude du Yiddishland, territoire
sans frontières nettes de l’Europe de l’Est qui, avant la Seconde Guerre
mondiale, comptait dix millions de Juifs.


Ces autobiographies, le dessinateur américain Ken Krimstein part les découvrir à son tour à Vilnius
--------------------------------------------------------------------------------------------------------

Ces autobiographies, le dessinateur américain Ken Krimstein part les découvrir
à son tour à Vilnius, peu de temps après leur résurrection. Il sélectionne
six récits, les met en forme avec ses propres mots, et compose avec eux un
roman graphique dominé par la couleur orange, «parce que c’est une couleur
polyvalente. Elle peut symboliser la vie, mais aussi le feu et le danger,
sans être aussi dure que le rouge. C’est une couleur qui attire le regard
mais elle a aussi quelque chose de poétique. Je trouve d’ailleurs que la
couverture de l’édition française, tout orange, est plus réussie que celle
de l’édition américaine. Ma femme dit que c’est parce que la France a
une plus longue tradition de la bande dessinée que les Etats-Unis. Je ne sais
pas si elle a raison.» Intitulé Vivre, le volume est un document, certes
incomplet mais très intéressant sur le quotidien, les idées, les mœurs,
la culture de jeunes Juifs d’Europe de l’Est avant la Shoah. Ils courent
après la liberté et le savoir, après la lecture ou la musique, moyens
d’accéder à l’émancipation. Certains vivent des situations familiales
ou historiques plus dangereuses ou compliquées que d’autres. Certains
sont politisés. Parmi ces auteurs il y a quatre filles et deux garçons. Ken
Krimstein n’a pu retrouver la trace que d’une seule d’entre eux, Beba
Epstein, qui a dérogé à la règle de l’anonymat.

Le Yivo suggère aux candidats, s’ils sont en manque d’inspiration,
d’aborder les thèmes suivants : leurs affiliations politiques, l’amitié,
la guerre, leurs amours, leur famille, les événements qui les ont beaucoup
marqués. Elle les encourage à privilégier une langue simple, et à ne
pas hésiter à raconter les petits riens, les choses qui ne sont même pas
extraordinaires. «Mais le vrai coup de génie était que toute participation
au concours serait anonyme», précise Ken Krimstein dans sa préface.

Le gagnant de ce qui se présente comme un concours resterait inconnu mais il
serait récompensé par une somme d’argent. Sept cents jeunes ont joué le
jeu. Annette Wieviorka note dans sa postface : «A l’instar de leurs parents,
ils s’intéressent vivement à la marche du monde, dont les nombreux journaux
en yiddish rendent compte, notamment les deux grands quotidiens, que sont le
Moment et le Haynt. Même quand ils adhèrent à des mouvements laïcs, voire
antireligieux, la Tradition imprègne leurs vies quotidiennes : Bat-Mitsvah,
passage sous le dais nuptial…» On ne saura jamais qui fut l’heureux
gagnant car, et cette donnée se situe à la croisée de l’ironie tragique
et de l’humour juif, le prix devait être décerné le 1er septembre 1939,
le jour où les nazis ont envahi la Pologne. Il ne fut jamais remis.


Francophile, Ken Krimstein était récemment à Paris pour la promotion de son livre
------------------------------------------------------------------------------------

Francophile, Ken Krimstein était récemment à Paris pour la promotion de
son livre. Il a le teint hâlé et une bouille ronde. Il est «cartoonist»,
comme on dit aux Etats-Unis. Longtemps il a dessiné pour The New Yorker, Punch
et The Wall Street Journal. Il a été professeur à l’université mais il
ne l’est plus. L’un de ses précédents romans graphiques a été traduit
en français. Il s’intitule les Trois Vies de Hannah Arendt (Calmann-Lévy,
traduit par Claire Desserrey, 2018), et il a été finaliste du Jewish Book
Award. En ce moment Krimstein relit les épreuves de son prochain livre qui
porte sur Kafka et Einstein.

Pouvez-vous retracer les étapes de ce que vous nommez «l’odyssée des autobiographies» ?
--------------------------------------------------------------------------------------------

Le 24 juin 1941, les nazis envahissent Wilno et pillent la bibliothèque
du Yivo, pour remplir l’Institut de recherche sur la question juive à la
demande de Hitler. Des Juifs reçoivent l’ordre de trier ces archives et
cachent les plus précieuses à l’abri des regards. Le 13 juillet 1944, les
Soviétiques arrachent Wilno aux mains des Allemands et créent le musée juif
de la ville. Ils dédient la première exposition à Staline qui, en 1949,
furieux qu’Israël se range aux côtés de l’Ouest et non de l’Est,
ordonne la destruction du musée. Antanas Ulpis, non Juif, fonctionnaire,
et membre du Parti communiste, rassemble les trésors du Yivo et les place
dans l’orgue, d’où ils ont été sortis en 2017.

Comment avez-vous été mis au courant de cette découverte ?
------------------------------------------------------------------

Par un article du New York Times titré «Trésors d’objets yiddishs sauvé
des nazis et de l’oubli». Je suis parti à Vilnius et sur place j’ai
été aidé par des interprètes car je ne parle pas yiddish. J’ai opéré
une sélection parmi les autobiographies. Je voulais de la diversité : des
religieux, des non religieux, des garçons et des filles, des jeunes plus
ou moins cultivés. La diversité était facile à obtenir, mais je me suis
rendu compte qu’ils étaient tous cultivés et qu’ils étaient, dans les
années 1930, des adolescents assez peu différents de celui que je fus dans
les années 1970 à Chicago, où j’ai grandi. Ils lisent. L’une des jeunes
qui figure dans le livre a vu au cinéma la Case de l’Oncle Tom [film muet
de 1927, ndlr] qui venait d’être adapté du livre de Harriet Beecher Stowe
[sorti en 1852 et traduit en yiddish en 1868]. Ils connaissent les grands
noms de la culture européenne, Dostoïevski, Flaubert et Freud. Ils veulent
découvrir le monde, ils sont amoureux et se posent des tas de questions,
comme tous les adolescents. Il arrive que leurs parents se séparent et que ça
se passe très mal. Certains veulent émigrer, à cause des pogroms. C’est
l’histoire de celui que je surnomme «l’Epistolier» J’ai voulu montrer
que leur quotidien est difficile. Le Yiddishland, ce n’est pas seulement
les chansons et les plaisanteries.

Un garçon, comme le veut la tradition, reçoit un crayon pour sa bar-mitsvah, cérémonie qui symbolise l’entrée dans la maturité. Vous aussi ?
------------------------------------------------------------------------------------------------------------------------------------------------

Absolument. Cependant mes parents nous ont élevés assez loin de la
religion. Nous sommes quatre enfants et ma plus jeune sœur est devenue
orthodoxe. Elle habite à New York. Mon premier roman graphique abordait ce
monde juif, l’observant sous un angle comique. Grâce à elle, j’avais du
matériel à portée de main. Ma sœur ne m’en a pas voulu, nous avons de
l’humour dans la famille, à commencer par mon père, qui est un admirateur
de Jacques Tati. Mon arrière-grand-père venait d’Ukraine, mes grands-parents
parlaient yiddish mais ils ont refusé de nous parler de leur passé et de leurs
origines. Ils souhaitaient devenir pleinement des Américains. Si nous leur
demandions d’où ils venaient, ils nous envoyaient promener, en grommelant.

Vos parents sont-ils également artistes ?
-------------------------------------------------

Il y a plusieurs artistes dans ma famille. Mon oncle et mon grand-oncle,
qui a combattu contre les Allemands pendant la Seconde Guerre mondiale,
dessinaient. Mon père aussi. Il fut étudiant dans l’une des meilleures
écoles d’art de Chicago mais il est devenu publicitaire. Moi j’ai fait
des études d’histoire mais j’ai toujours dessiné. En classe j’ai eu
du succès grâce à mes dessins de Santa Claus, alors que j’étais assez
médiocre au base-ball.

Pourquoi avoir écrit sur Hannah Arendt ?
-----------------------------------------------------

J’ai d’abord pensé écrire quelque chose pour les jeunes, mais j’avais
déjà des enfants et ils m’énervaient tellement, parfois, que j’y ai
renoncé. J’ai pensé à une biographie, parce que je suis un grand lecteur de
biographies et que je m’intéresse au chemin qu’emprunte la créativité de
ceux que j’admire. J’ai lu toutes les biographies des Beatles, par exemple,
pour essayer de comprendre de quelle façon ils avaient travaillé. Comme
j’ai fait des études de philosophie, également, j’ai pensé que ce
serait intéressant de me demander, comme je le fais avec les artistes, comment
venaient les idées des philosophes. Je me suis intéressé d’abord à Leo
Strauss (1899-1973), né en Allemagne, spécialiste de philosophie politique,
qui a été professeur à l’université de Chicago. Cette proximité entre
lui et ma ville natale m’intéressait. Il était de la génération de mes
grands-parents. C’est l’époque où l’on pouvait faire cours en fumant
des cigarettes, vous vous rendez compte ? Puis je me suis dit qu’il faudrait
que je choisisse quelqu’un de plus connu et j’ai décidé que ce serait
Arendt. Strauss et elle furent tous deux des élèves de Heidegger. Je me suis
aussi aperçu qu’elle avait habité près de l’endroit où nous habitions,
ma femme et moi, à New York, dans l’Upper West Side – nous avons déménagé
à Chicago il y a une douzaine d’années. Tout ce que je lisais sur Arendt se
transformait en images dans mon esprit. En ce moment je lis David Copperfield,
de Dickens. Eh bien, je mets chaque page en image, dans ma tête. La bande
dessinée est selon moi la meilleure façon de raconter l’Histoire.

C’est peu ou prou ce que je pense Riad Sattouf…
-----------------------------------------------------------

Riad Sattouf ? [Ken Krimstein étouffe un cri d’admiration]. Oh mon dieu ! Je
l’adore ! J’ai dit à des amis qu’il fallait lui donner le Jewish Book
Award. Ils étaient étonnés, mais il faut le faire, parce que Sattouf raconte
la vérité sur la façon dont on éduque les enfants en Syrie. Lui-même été
en classe en Syrie. Vous savez, lorsqu’on est dessinateur, c’est difficile de
représenter le Mal, je trouve. Riad Sattouf, lui, il y arrive.  Ken Krimstein,
Vivre, traduit de l’anglais (Etats-Unis) par Gaïa Maniquant-Rogozyk,
postface d’Annette Wieviorka, Bourgois, 248 pp., 25 € ebook : 18,99€).
