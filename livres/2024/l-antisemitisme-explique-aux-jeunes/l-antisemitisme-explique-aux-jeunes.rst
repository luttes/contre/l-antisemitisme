.. index::
   pair: Livre; L'Antisémitisme expliqué aux jeunes (2024-08)

.. _livre_wieviorka_2024_08:

====================================================================
**L'Antisémitisme expliqué aux jeunes** de Michel Wieviorka
====================================================================

- https://www.seuil.com/ouvrage/l-antisemitisme-explique-aux-jeunes-michel-wieviorka/9782021569001


- Pourquoi les Juifs sont-ils l’objet d’une haine particulière ?
- Quand l’antisémitisme est-il apparu ?
- Est-ce une forme du racisme ?
- Pourquoi Hitler détestait-il les Juifs ?
- Comment l’antisémitisme a-t-il pu renaître après le génocide des Juifs ?
- Nier le génocide, est-ce être antisémite ?
- Les Juifs ont-ils le monopole historique de la souffrance ?
- Existe-t-il un « business de la Shoah » ?
- Pourquoi une partie de la jeunesse est-elle séduite par des discours antisémites ?
- A-t-on le droit de critiquer Israël ?
- L’antisionisme ou le soutien à la Palestine, est-ce de l’antisémitisme ?

Ce petit livre n’hésite pas à poser les questions les plus dérangeantes.

Cette nouvelle édition mise à jour démonte avec clarté et tranquillité
les idées fausses, les pièges et les théories du complot qui prennent
de l'ampleur depuis l’attaque terroriste du Hamas du 7 octobre 2023
contre Israël.
