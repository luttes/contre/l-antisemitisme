.. index::
   pair: Livre; Antisémitisme : un meurtre intime (2024, Brigitte Stora)

.. _livre_brigitte_stora_2024:
.. _meurtre_intime_stora_2024:

====================================================================================
**L'antisémitisme : un meurtre intime** par Brigitte Stora
====================================================================================

- :ref:`brigitte_stora`
- https://www.editionsbdl.com/produit/antisemitisme-un-meurtre-intime/
- https://www.librairie-des-femmes.fr/livre/9782385190354-antisemitisme-un-meurtre-intime-brigitte-stora/

.. figure:: images/couverture.webp

.. figure:: images/verso.webp


Introduction
===============

Malgré la Shoah, le discours antisémite qui l’avait permis, n’a pas
changé : les Juifs, peuple coupable, tout entiers situés du côté de la
domination, du privilège et de la spoliation, se seraient accaparé l’avenir
du monde.

L’antisémitisme, d’où qu’il vienne, proclame l’urgence de déloger les
Juifs d’une place imméritée, d’une "élection" usurpée …

Difficile de ne pas reconnaitre l’imaginaire ancestral de ce discours délirant
qui de nouveau se parle un peu partout, et, c’est le plus terrible, parfois
même à l’insu de ses locuteurs.

Mais comment un si petit groupe humain peut-il demeurer l’obsession de
centaines de millions d’individus ?

Que peut bien signifier cette "domination juive", ce terrifiant empire
que les Juifs exercent sur les antisémites ?

L’antisémitisme est d’abord une très ancienne vision du monde qui postule
l’abolition du judaïsme comme condition d’une rédemption universelle.

Mais il est aussi une rage intime contre les Juifs qui, dès lors occupent la place
originelle de l’altérité fondamentale. Celle qui oblige et nous grandit
mais aussi celle que beaucoup considèrent comme une menace à éliminer.

Ce rejet essentiel de l’Autre apparaît comme un modèle de la dénonciation
de l’origine et du nom, comme un affranchissement de toute dette mais aussi
comme la hantise de tout désir que cet Autre, et sans doute tout Autre,
peut susciter en soi.

Pour les antisémites, les Juifs portent le grand nom coupable, cette accusation
est à l’origine de toutes les démissions du courage.
Car seul le nom de l’Autre nous permet de penser la responsabilité d’un monde commun.

Franz Fanon avait prévenu : "Quand vous entendez dire du mal des juifs,
dressez l’oreille, on parle de vous."

Docteure en psychanalyse et essayiste, Brigitte Stora a notamment publié chez
le même éditeur Que sont mes amis devenus…: Les Juifs, Charlie, puis tous
les nôtres (2016).


Articles/rencontres 2024
============================

- :ref:`raar_2024:philippe_corcuff_2024_10_21`
- :ref:`raar_2024:brigitte_stora_2024_10_14`
- :ref:`raar_2024:brigitte_stora_2024_10_13`
- :ref:`raar_2024:webinaire_2024_09_16`
- :ref:`raar_2024:stora_2024_06_26`
- :ref:`cbl_grenoble:stora_2024_06_20`
- :ref:`raar_2024:stora_2024_05_26`
- :ref:`raar_2024:stora_2024_03_28`
