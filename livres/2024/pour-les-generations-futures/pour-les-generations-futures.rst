.. index::
   pair: Livre; Pour les générations futures (2024-09)
   pair: Simone Veil; Pour les générations futures (2024-09)

.. _pour_generations_futures_2024:

==============================================================================
**Pour les générations futures**  par Simone Veil (2024-09) |SimoneVeil|
==============================================================================

- https://www.albin-michel.fr/pour-les-generations-futures-9782226489883

.. figure:: images/recto.webp

Auteur: Simone Veil |SimoneVeil|
=====================================

- https://fr.wikipedia.org/wiki/Simone_Veil


Description
================

En avril 2005, deux ans avant la parution d’Une vie, Simone Veil prononce une
conférence devant les élèves de la rue d’Ulm.
Inédit à ce jour, le texte qui en est issu constitue une véritable adresse
à la jeunesse.

Elle y évoque sa déportation et celle de sa famille, mais aussi les thèmes
et les préoccupations qui lui sont les plus chers : la mémoire de la Shoah
et sa transmission aux nouvelles générations, le sort des enfants cachés,
la réconciliation et l’unité européenne, le rôle de la fiction et de la jeunesse
pour ne jamais oublier les tragédies du passé.

Un document et un témoignage exceptionnels.

Postface de Dan Arbib, maître de conférences en philosophie à Sorbonne Université.
