.. index::
   pair: Livre; Coulée brune (2024-10, Olivier Mannoni)
   pair: Olivier Mannoni; Coulée brune (2024-10)

.. _coulee_brune_2024:

=========================================================================================================
2024-10 **Coulée brune, Comment le fascisme inonde notre langue par** Olivier Mannoni  |OlivierManonni|
=========================================================================================================

- https://actualitte.com/article/119463/avant-parutions/coulee-brune-quand-la-democratie-perd-ses-mots
- https://www.librairiesindependantes.com/product/9782350878959/


.. figure:: images/recto.webp

Auteur: Olivier Mannoni
=========================================

- :ref:`olivier_manonni`

Né en 1960, Olivier Mannoni est traducteur d'allemand Récipiendaire du prix
Eugen-Helmlé, il a fondé L'École de traduction littéraire en partenariat avec
le CNI et a présidé PATIE.

Traducteur de plus de 200 titres, spécialisé dans les textes sur le IIIe Reich,
il est aussi critique littéraire et biographe.
Entre 2011 et 2021, il s'est attelé à la réédition critique de Mein Kampf.
Historiciser le mal.


Verso
===========

"J'ai décidé de vous redonner le choix de notre avenir parlementaire par le vote.
Je dissous donc l'Assemblée. "

Silence retentissant dans un pays sonné. Pourtant, l'écrasante victoire de
l'extrême droite aux élections européennes n'est pas une surprise.

Le glissement s'opère depuis longtemps dans notre langage.

À quand cela remonte-t-il ?

Au second tour de 2002 ? à la crise des Gilets jaunes ?  à celle du COVID-19 ?

Olivier Mannoni, qui a traduit Mein Kampf et qui connaît les pièges du discours
et de la sémantique, sait, lui, qu'il faut creuser plus loin, jusque dans
les entrailles de notre Histoire européenne.

Avec une pensée claire et des mots incisifs, il analyse les prises de parole
de nos politiciens et passe au crible les médias vecteurs de fausses informations.

D'une lucidité redoutable, ce livre uppercut met à nu les menaces linguistiques
qui pèsent sur nos démocraties.


Description
=============

Olivier Mannoni explore les distorsions du langage et leurs impacts sur la
démocratie.

Il dénonce les manipulations et confusions qui freinent les débats essentiels.
Dans Traduire Hitler (2022), il évoquait déjà la montée de discours toxiques

Depuis, les dérives se sont intensifiées.
Les débats politiques se sont réduits à de simples faits divers et à des
expressions d'indignation.
Une extrême droite en pleine expansion, nourrie par des discours complotistes
et haineux, menace la stabilité démocratique.

L'auteur examine le glissement du discours politique vers une rhétorique
violente et la confusion des concepts.
Cette dérive a rendu le débat démocratique presque impossible, remplacé par
des rumeurs, des invectives et des falsifications historiques.

Il décortique les canaux de diffusion de ces fausses informations, notamment
les réseaux sociaux et certaines émissions télévisées.
