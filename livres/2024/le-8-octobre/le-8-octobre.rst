.. index::
   pair: Livre ; Le 8 octobre , généalogie d'une haine vertueuse (2024-10, Eva Illouz)
   pair: Eva Illouz ; Le 8 octobre (2024-10)

.. _le_8_octobre_2024_10:

===============================================================================================================
2024-10 **Le 8 octobre, généalogie d'une haine vertueuse** par Eva Illouz
===============================================================================================================

- :ref:`eva_illouz`
- https://tracts.gallimard.fr/products/le-8-octobre


.. figure:: images/recto.webp

Description
===============

Quand les Lumières et leurs vertus ont été rejetées, l’antisionisme devient
la seule vertu capable de rassembler ceux qui ont tout déconstruit.

Eva Illouz Les grands événements ont leur jour d’après.

C’est le sujet de ce Tract, qui s’interroge sur la révélation d’un antisémitisme
de gauche au lendemain de l’attaque du Hamas contre Israël.

**Aurions-nous pu penser que, dans les milieux progressistes occidentaux,
le 8 octobre 2023 puisse ne pas être le jour de la compassion unanime à
l’égard des victimes des atrocités de la veille ?**

Au lieu de cela, on entendit, à New York comme à Paris, des voix autorisées
saluer, avec une émotion jubilatoire, un acte de résistance venant châtier
l’oppresseur israélien.

Décomplexé, cet antisionisme radical a eu pour terreau un système de pensées,
la ' théorie ' qui, avec sa passion déconstructiviste, tend à plaquer une
structure décoloniale sur les événements du monde, au mépris du fait brut
et de sa complexité.

On peut mettre au jour les causes d’une guerre ; on cherchera plutôt ici à
retracer la généalogie intellectuelle de ce qui nie l’évidence du crime…

Et à remonter aux sources de cet antisémitisme de confort où le Juif cristallise
ce que certains esprits jugent bon de reprocher à une partie de l’humanité.

Commentaires
==================

Marc-Olivier Bherer, Le Monde, 10 octobre 2024
------------------------------------------------------

- https://tracts.gallimard.fr/products/le-8-octobre#:~:text=C'est%2C%20pour%20la,la%20cause%20palestinienne.%C2%A0%C2%BB


« Eva lllouz s'identifie toujours à la gauche, à son combat pour la dignité
humaine, c'est pourquoi elle ne sous-estime pas la “négativité” et la critique
“que la politique [du] gouvemement [israélien] peut susciter”.

Mais l'universitaire entend ici faire le procès d'un “antisémitisme vertueux”,
qui emprunte de tortueux méandres pour que la haine des Israéliens apparaisse
juste et morale.

C'est, pour la sociologue, un dévoie ment de la cause palestinienne. »

Marc-Olivier Bherer, Le Monde, 10 octobre 2024
