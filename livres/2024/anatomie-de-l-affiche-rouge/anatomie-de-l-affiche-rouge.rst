.. index::
   pair: Livre ; Anatomie de l'affiche rouge (2024-02)

.. _wieviorka_2024_02:

=========================================================
**Anatomie de l'affiche rouge** par Annette wieviorka
=========================================================

- https://fr.wikipedia.org/wiki/Annette_Wieviorka
- https://www.librairie-des-femmes.fr/livre/9782021559422-anatomie-de-l-affiche-rouge-annette-wieviorka/


Le 21 février 2024 Missak Manouchian entre au Panthéon, avec son épouse,
Mélinée.

L'histoire des Arméniens mérite d'être connue et reconnue.

Missak, le militant, le résistant est une figure digne d'être honorée.

Mais je suis saisie par un double sentiment, celui d'une injustice à
l'égard des 21 autres résistants étrangers fusillés en même temps que
lui par les nazis et d'Olga Bancic guillotinée ;

celui d'un malaise devant un récit historique qui distord les faits pour
construire une légende.

Or, à l'époque des **vérités alternatives**, si on souhaite donne une
leçon d'histoire, la moindre des précautions est de l'établir
