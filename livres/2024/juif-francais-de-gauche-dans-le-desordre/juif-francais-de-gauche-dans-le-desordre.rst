.. index::
   pair: Livre; Juif, français, de gauche... dans le désordre par Arié Alimi (2024)


.. _alimi_livre_2024_03:

========================================================================================
**Juif, français, de gauche... dans le désordre** par Arié Alimi (2024) |ArieAlimi|
========================================================================================

- https://www.editionsladecouverte.fr/juif_francais_de_gauche_dans_le_desordre-9782348083747

.. figure:: images/couverture_recto.webp
   :width: 500


Présentation
================

Né à Sarcelles de parents sépharades exilés d'Algérie et de Tunisie, de culture
et de langue françaises, attaché à Israël et à l'idée d'un " foyer juif ",
humaniste engagé contre tous les racismes, je fais partie de cette génération
qui a vu ses repères exploser.

C'est l'échec des accords d'Oslo et la politique israélienne de plus en plus
agressive à l'égard des Palestiniens qui ont profondément bouleversé la
coexistence intérieure de ces identités.

Comment dès lors articuler mon identité juive et mon identité de gauche,
attachée à l'émancipation des peuples ?

Comment être juif et français, attaché à une certaine conception de la
république, fondée sur l'égalité ?

À ces complexités s'ajoutent la **menace renouvelée de l'antisémitisme** et la
nécessité d'appréhender les causes de son surgissement, y compris lorsque
ces haines émanent de celles et ceux qui se réclament, comme je le fais, de
la gauche décoloniale, ou lorsque les antisémites d'hier afﬁchent un **philosémitisme
de façade au détriment des musulmans**, devenus l'ennemi commun d'une partie
de la communauté juive et, plus largement, française.

Contre tous ces vents contraires, cet ouvrage a pour but de montrer la
possibilité de réarticuler cet écheveau d'identités:

- juif attaché à Israël,
- de gauche à l'émancipation des peuples,
- français à l'égalité entre toutes et tous

seul à même de lutter contre tous les racismes.


Table des matières
=====================

Introduction
---------------

1. De la question juive à la réponse des identités
--------------------------------------------------------

- Juif de gauche
- Faire partie du peuple juif ?
- Juif de gauche ou sioniste de gauche

2. La question antisémite
------------------------------

- D'Alfred Dreyfus à Mireille Knoll
- La coexistence des antisémitismes
- L'antisionisme est-il un antisémitisme ?
- L'instrumentalisation politique de l'antisémitisme
- Juif, français, de gauche... dans le désordre

3. Après le 7 octobre 2023. Être Juif, décolonial et humaniste
-------------------------------------------------------------------

- Juif universaliste et décolonial
- Le judaïsme sera décolonial ou ne sera pas

Conclusion.
---------------

Juif, français, de gauche : un exemple de sujet stratifié.


Interviews/rencontres 2024
================================

- :ref:`raar_2024:arie_alimi_2024_11_07`
- :ref:`raar_2024:arie_alimi_2024_10_25`
- :ref:`raar_2024:alimi_le_monde_2024_04_07`
- :ref:`raar_2024:alimi_2024_04_07`
