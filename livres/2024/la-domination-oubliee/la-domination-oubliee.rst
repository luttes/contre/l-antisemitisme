.. index::
   pair: Tal Piterbraut-Merx ; La domination oubliée (2024)

.. _domination_oubliee_2024_10_25:

====================================================================
**La domination oubliée** par Tal Piterbraut-Merx
====================================================================

- :ref:`tal_piterbraut_merx`
- https://www.editionsblast.fr/la-domination-oubliee
- https://www.editionsblast.fr/auteur-es
- https://x.com/editionsblast

La domination oubliée
==========================

.. figure:: images/couverture.webp

Texte revu, adapté et enrichi par
======================================

- Anaïs Bonanno,
- Élise de la Gorce,
- Selma (Sam) Ducourant,
- :ref:`Félicien Faury <felicien_faury>`,
- Léo Manac'h,
- Margaux Nève,
- Pierre Niedergang,
- Marion Pollaert,
- Audrey Smadja Iritz
- et Léa Védie-Bretêcher.

Le projet de Tal dans son travail de thèse était d’approcher les relations
adulte-enfant au prisme des rapports sociaux de domination.

Dans l’article "Conjurer l’oubli", il explique que les rapports adulte-enfant
"appartiennent à la grande famille des rapports de pouvoir (classe sociale,
genre, race, etc.) et doivent [être] analysés en tant que tels".

Cette approche en termes de rapports sociaux de domination vise à une
repolitisation de la question de la catégorie de l’enfance, voire de
"la classe", des enfants, et de la relation adulte-enfant.

On met dans la nature ce qu’on veut évacuer de l’horizon politique : contrairement
aux rapports de pouvoir que sont la classe, le genre et la race, les rapports
adulte-enfant semblent encore inscrits dans un ordre naturel.

Pourtant, l’institution familiale, censée protéger, est le lieu où les
violences faites aux enfants sont les plus fréquentes.

Dont acte : loin de remédier à la fragilité naturelle de l’enfant, les
nstitutions sociale et familiale produisent cette vulnérabilité.

En croisant philosophie politique et théories féministes, Tal Piterbraut-Merx
dénaturalise et repolitise la domination adulte.

La classe des enfants est dominée à la fois par le statut de minorité,
par la dépendance économique et par les pratiques d’éducation.

C’est l’oubli, par les adultes, de leur expérience de la minorité, qui
contribue à perpétuer la domination en imposant le silence.

Quelles stratégies de lutte pour combattre cette domination oubliée ?
L’auteur en appelle à rester fidèles aux enfants que nous étions en nous
remémorant collectivement nos enfances.

Tal Piterbraut-Merx était chercheuse en philosophie politique, écrivain
et militant.

Suite à son suicide, un collectif d'ami·es a établi cet ouvrage à partir
du manuscrit inachevé de sa thèse.
