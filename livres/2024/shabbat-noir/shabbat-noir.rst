.. index::
   pair: Liza Hazan; Shabbat noir (2024-08)

.. _livre_hazan_2024:
.. _shabbat_noir_2024:

===================================================================================
2024-08 **Shabbat noir** par Liza Hazan
===================================================================================

- https://editionsdesequateurs.fr/livre/Shabbat-noir/334

.. figure:: images/shabbat_noir.webp


Auteure : Liza Hazan
===========================


- Date de parution : 21/08/2024
- Collection :  Littérature
- Nombre de pages : 192

Présentation
==============

"Peut-être que je veux la paix avec beaucoup de naïveté, peut-être que
je veux la paix comme les jeunes qui ne connaissent rien à la vie, comme
les artistes perchés, comme les déconnectés de la réalité, comme ceux qui
ont été épargnés par la souffrance, qui l’ont vécue d’assez loin pour ne
pas disjoncter, ceux qui ne connaissent pas assez le monde pour savoir
que ça ne marche pas comme ça, que c’est trop simple de vouloir la paix,
qu’il faut choisir un camp, que sinon on lutte dans le vent.

Mais je m’en fous, tu vois.

L’avantage d’avoir vingt et un ans, c’est qu’on peut penser ce qu’on veut,
et même le bien, qu’on peut se battre pour toutes les vies, même les petites,
qu’on peut lutter pour l’idéal qu’on a dans la tête, même si ça paraît
impossible ; on nous le pardonnera."



Le 7 octobre 2023, une étudiante juive se réveille à Paris au bruit de
la guerre.

Shabbat noir est le roman de sa journée qui en contient mille autres.

Le roman d’une jeunesse dont la furieuse envie de vivre se heurte au
fracas du monde.



Articles 2024 “Shabbat Noir” de Lisa Hazan (Tenoua, 2024-09)
================================================================

- https://www.tenoua.org/shabbat-noir-de-lisa-hazan/

Quand on a vingt ans, ou à peu près, qu’on a le cœur plein, qu’on aime la vie,
malgré son amertume et ses détours assassins, et que subitement,
celle-ci se réduit à une inquiétude identitaire, cela donne Shabbat Noir
de Lisa Hazan.

Non pas un roman "post-7-octobre" mais un roman-du-7-octobre, qui raconte
cette journée maudite, interminable, qui dure encore pour tous les Juifs
attachés à Israël, du point de vue d’une jeune étudiante en lettres dont
les yeux sont grands ouverts sur la tragédie passée et en cour

La narratrice étudie les lettres modernes dans une université parisienne.
Partie vivre en Israël avec ses parents quand elle était adolescente, elle
y a vécu les années les plus éprouvantes de sa courte vie.

Celles de la fin de l’adolescence, de l’entrée dans le monde adulte, des
responsabilités et de la guerre.

Elle accomplit son service militaire en tant que "guetteuse" : elle doit
signaler les événements anormaux ou potentiellement dangereux à la frontière é
gyptienne.

Indocile et rétive à toute forme d’obéissance passive, elle est régulièrement
convoquée par le tribunal militaire :

::

    "Tu t’es sauvée de la base pour aller à la plage",
    "Tu as nourri des soldates
    alors qu’elles étaient privées de repas",
    "Tu as cligné des yeux durant ta garde",
    "Tu as rendu visite à une soldate punie",
    "Tu as défendu ta camarade face à un officier",
    "Tu t’es endormie pendant une formation",
    "Tu refuses d’effectuer ton rôle depuis déjà deux semaines",
    "Tu as déserté deux jours entiers" […]

À mon dixième procès, on décida de me virer de l’armée.

À cause de mon trop-plein de bêtises ou peut-être aussi à cause de mon
trop-plein de détresse. Je ne sais pas vraiment.

Il est forcément raide, forcément désespéré, ce récit d’une jeune
franco-israélienne, seule avec sa peine dans sa chambre d’étudiante le 7
octobre 2023, qui décide de sortir et de retrouver des amis dans Paris, pour
défier l’effroi et ne pas s’endormir.

**Elle découvre alors non seulement le cynisme de ses camarades vis-à-vis
de ses sentiments au terme de cette journée cauchemardesque, mais surtout
la haine de l’autre, cachée sous le masque de l’analyse intellectuelle.**

Mais ici, on avait le recul dès le départ, parce qu’on était né dans le bon pays, qu’on y avait vécu
toute sa vie et qu’on parlait sans savoir vraiment de quoi on parlait.
On était loin de tout ce qui se passait, on restait dans l’extérieur, le
superficiel.

Pour formuler une pensée, on citait un philosophe qui avait
déjà pensé pour nous.

Pour exprimer un ressenti, on citait un écrivain qui avait déjà ressenti
pour nous.

On mangeait du pop-corn ou des chips
selon les goûts, et on débattait de la vie des gens, de ceux qui vivaient
loin de chez nous, qu’on n’avait jamais vus ailleurs qu’à la télé,
et on se demandait qui méritait de mourir et qui ne le méritait pas,
qui méritait d’être massacré et qui ne le méritait pas, et on jugeait
les populations, qui avec leur guerre nous offraient un bon divertissement,
et on se sentait héroïques parce qu’on souhaitait la mort des méchants.

On peut parfois croire certains romanciers trop tendres pour être satiriques;
en réalité la satire s’ajuste mal à certains sujets, comme ceux qui
donnent lieu à la stupeur à force de révéler la cruauté indomptable
de l’homme qui noircit son cœur atrophié.

Notre jeune héroïne, à force d’être jeune et héroïque, en vient à s’accuser elle-même,
puis toute sa lignée : "J’en voulais à ma judéité d’être autant
là, si présente, de déranger les autres à ce point."

**Par de modestes phrases qui ne paient pas de mine, c’est avec une finesse renversante
que Lisa Hazan parvient à décrire la mécanique perverse de la haine,
qui aboutit à retourner la faute contre soi et à entretenir soi-même le
mal qui se repaît goulûment de nos anéantissements**.

Quand dire le rejet de politiques fachistes ne suffit pas, quand dire la solidarité avec les
civils palestiniens ne suffit pas, quand reconnaître qu’"On avait vu
mieux, comme privilège, qu’être Juif en France" ne suffit pas, alors on
danse. L’énergie de ses vingt ans prend le dessus, elle flirte, séduit
même, dans un cri inaudible qui creuse quelque chose à l’intérieur,
puis retombe dans une détresse liquide et fourbe, qui tache et s’étale
sans qu’elle y prenne garde.

Pour se donner du courage, elle répète « Je n’ai pas peur !", comme à la guerre.
Au fait, quand le mot "peur" apparaît 83 fois dans le roman d’une jeune
juive qui n’a pas peur, ça veut dire quoi ?

Au cours du récit, on souhaiterait parfois voir la narratrice exploser de
fureur devant le mépris de jeunes ignorants jouant avec l’intimité des
Israéliens et des Juifs ayant tant souffert pendant ces heures impitoyables
qui suivirent les révélations du pogrom.

Rien de tel dans Shabbat Noir.

Dans les méandres d’une douleur ancestrale liée à l’exil et à l’errance,
accablé par le rêve vain d’une identité qui ne serait pas esclave de
la haine des autres, le roman de Lisa Hazan déborde sans crier gare d’une
fraîcheur saisissante : celle de la jeunesse qui veut tout, tout de suite,
qui ne fait pas de concessions, attentive à l’inattendu, accueillant
avec une bonté fraternelle à force d’être naïve les êtres et leurs
mystères, leurs douleurs et leurs impuissances.
