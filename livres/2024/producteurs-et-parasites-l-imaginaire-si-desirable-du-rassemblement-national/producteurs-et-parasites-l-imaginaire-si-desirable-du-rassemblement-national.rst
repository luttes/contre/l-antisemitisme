.. index::
   pair: Livre; Producteurs et parasites L'imaginaire si désirable du Rassemblement national (2024)

.. _producteurs_et_parasites_2024:

====================================================================================================
**Producteurs et parasites L'imaginaire si désirable du Rassemblement national** par Michel Feher
====================================================================================================

- https://www.editionsladecouverte.fr/producteurs_et_parasites-9782348084881


.. figure:: images/recto.webp

Description
==============


Le RN est rarement crédité d'un vote d'adhésion.

Jugeant l'hypothèse trop décourageante, ses détracteurs préfèrent évoquer le
désaveu qui frappe ses rivaux, la toxicité de l'espace médiatique ou le
délitement des solidarités ouvrières.

**Producteurs et parasites entreprend au contraire d'examiner la popularité
e l'extrême droite à la lumière des satisfactions que sa vision du monde
procure à ses électeurs**.

Le parti lepéniste divise la société française en deux classes moralement
antinomiques : les producteurs qui n'aspirent qu'à vivre du produit de leurs
efforts et les parasites réfractaires à la " valeur travail " mais rompus à
l'accaparement des richesses créées par autrui.

Les premiers contribuent à la prospérité nationale par leur labeur, leurs
investissements et leurs impôts, tandis que les seconds sont tantôt des
spéculateurs impliqués dans la circulation transnationale du capital,
financier ou culturel, et tantôt des bénéficiaires illégitimes de la
redistribution des revenus.

Ancrée dans la critique des privilèges et des rentes, l'assimilation de la
question sociale à un antagonisme entre producteurs et parasites n'a pas
toujours été la chasse gardée de l'extrême droite.

Sa longue histoire révèle toutefois que le désir d'épuration auquel elle
donne naissance passe toujours par une racialisation des catégories réputées
parasitaires.

**Pour résister au RN, il est donc aussi nécessaire de dénoncer son imaginaire
que de reconnaître l'attrait qu'il exerce**.


Cité en 2025
================

- :ref:`raar_2025:imaginaire_rn_2025_05_09`

Articles et vidéos en 2024
==============================

- https://esprit.presse.fr/article/michel-feher/la-tentation-de-l-epuration-entretien-avec-michel-feher-45548
- https://www.youtube.com/watch?v=2kh_V4jqQsA
