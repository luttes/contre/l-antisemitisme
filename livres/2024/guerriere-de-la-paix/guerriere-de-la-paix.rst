.. index::
   pair: Hanna Assouline; Guerrière de la paix, Juifs et musulmans, quand les femmes engagent le dialogue (2024-05)

.. _hanna_assouline_livre_2024:

===================================================================================================================
**Guerrière de la paix, Juifs et musulmans, quand les femmes engagent le dialogue** par Hanna Assouline (2024)
===================================================================================================================

- https://www.seuil.com/ouvrage/guerriere-de-la-paix-hanna-assouline/9782021563887


.. figure:: images/recto_guerriere_de_la_paix.webp

Guerrière de la paix
=========================

4 octobre 2023, Jérusalem. Des milliers de femmes israéliennes et palestiniennes
marchent main dans la main pour demander d’une même voix l’arrêt du cycle
de la violence et de la haine.
Hanna Assouline et des militantes du mouvement Les Guerrières de la paix,
venues du monde entier, sont à leurs côtés.

Pendant plusieurs jours, elles sillonnent la région à la rencontre d’activistes
de la paix israélien.nes et palestinien.nes, de Jérusalem à Huwara et Ramallah
en Cisjordanie, en passant par les kibboutz du Sud, à la frontière de Gaza.

**Le 7 octobre, quelques heures après leur retour chez elles, le monde bascule**.

Ce livre est né de la nécessité de témoigner du courage de ces femmes et de
ces hommes, de relayer leur combat pour que leurs deux peuples, dressés l’un
contre l’autre, entament un processus d’acceptation mutuelle. Il rapporte
aussi les questionnements que ces rencontres ont provoqués. Comment reconnaître
l’autre quand tous les récits le représentent en ennemi ?

Cette question résonne de façon intime pour Hanna Assouline, jeune femme séfarade
qui a grandi dans un quartier populaire et multiculturel de Paris. Elle a vu les
relations se tendre entre communautés, les idéo-logies figer les identités,
nourrir le racisme et l’antisémitisme, le conflit israélo-palestinien servir
de prétexte à la haine.

Mêlant récits collectif et personnel, elle défend une autre voie – une voie
où les femmes ont un rôle particulier à jouer : celle de la reconnaissance
pleine et entière de la légitimité de chacun, celle de la solidarité, de la
responsabilité et de la sororité.

Militante pacifiste engagée contre le racisme et l’antisémitisme, Hanna
Assouline est présidente fondatrice du mouvement de femmes Les Guerrières de la
paix. Journaliste et documentariste, elle a réalisé Les Guerrières de la paix
(2018) et À notre tour ! (2020).


Articles 2024
===============

2024-04-19 Annonce par Hanna Assouline de la sortie de son livre le 10 mai 2024
----------------------------------------------------------------------------------------

- https://www.instagram.com/p/C58XlNpNqeY/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==

Aujourd’hui c’est mon anniversaire, c’est aussi le jour d’une révolte celle
du Ghetto de Varsovie, et c’est le jour où je vous annonce avec émotion, gratitude
et aussi une petite dose de stress et d’appréhension que mon livre
"Guerrières de la Paix" sortira le 10 mai 2024.

Ce témoignage, écrit dans l’urgence d’une actualité tragique et mouvante, mêle
l’intime et le collectif, les fragments de mon histoire qui se heurtent à
la grande Histoire. J’y reviens sur la création des Guerrieres de la Paix,
sur la manière dont l’actualité de ces dernières années a impacté nos vies
et notre rapport au monde, sur notre voyage en Israël et en Palestine qui
s’est achevé dans la nuit du 6 au 7 octobre 2023, sur les rencontres avec
les courageuses et courageux militants de la paix israéliens et palestiniens.

Sur nos engagements et nos espoirs.

Une ode à la solidarité et la sororité.

Un hommage aux @guerrieres_paix_mvt du monde entier.

Il est à vous et j’espère qu’il vous plaira 🙏🤍
