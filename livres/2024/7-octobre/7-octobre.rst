.. index::
   pair: Livre; 7 octobre (2024-05)

.. _yaron_livre_2024:

===============================================================================================
**7 octobre** de Lee Yaron (2024-05)
===============================================================================================

.. figure:: images/7_octobre.webp

Lee Yaron
=============

- https://www.grasset.fr/auteur/lee-yaron/

Née à Tel Aviv, Lee Yaron travaille depuis près de dix ans pour le quotidien
Haaretz, le plus célèbre quotidien israélien.

Ses articles et enquêtes portent sur les affaires de corruption au sein du
gouvernement, les inégalités sociales, le changement climatique,
le droit des migrants ainsi que les questions de genre et d’orientation sexuelle.

Membre du comité exécutif du Syndicat des journalistes israéliens, Lee Yaron
partage son temps entre sa ville natale et New York.

Description
============


La journée la plus meurtrière de l'histoire d'Israël racontée par les victimes
et leurs proches.

Le 7 octobre 2023, le Hamas lance une attaque sans précédent contre Israël.

Des civils sont massacrés, torturés, violés, brûlés ou enlevés, lors de cette
journée la plus meurtrière pour le peuple juif depuis la Seconde Guerre mondiale.

Afin de donner un visage à ces femmes, ces hommes et ces enfants, Lee Yaron a
écrit leur histoire.

Du festival de musique "Tribe of Nova" au kibboutz Be’eri, d’une famille de
Bédouins à un rescapé de la Shoah, d’ouvriers agricoles népalais à des
réfugiés ukrainiens, la journaliste israélienne recueille chaque détail
de cette tragédie pour restituer la violence inouïe qui s’est déchaînée
ce jour-là.

S’appuyant sur des centaines d’entretiens, de transcriptions d’appels et de
messages échangés –  précédant parfois l'horreur de quelques secondes  –,
ce livre est la première grande enquête publiée sur cette journée noire.

Suivi d’une postface de Joshua Cohen et en cours de traduction à travers
le monde, **7 octobre** dresse un bouleversant mémorial pour les victimes de ce
massacre.

Traduit par Colin Reingewirtz et Laurent Trèves.
