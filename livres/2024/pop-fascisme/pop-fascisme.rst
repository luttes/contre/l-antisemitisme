.. index::
   pair: Livre; Pop fascisme 2024
   pair: Pierre Plottu et Maxime Macé; Pop fascisme 2024

.. _pop_fascisme_2024:

====================================================================
**Pop fascisme** par Pierre Plottu et Maxime Macé
====================================================================

- https://www.editionsdivergences.com/livre/pop-fascisme


Auteurs: Maxime MACÉ et Pierre PLOTTU
=========================================

Maxime MACÉ et Pierre PLOTTU sont journalistes spécialisés dans la couverture
de l’extrême droite et de sa marge radicale.

Après plusieurs années en tant qu’indépendants, ils œuvrent désormais à
Libération où ils animent notamment la newsletter hebdomadaire dédiée Frontal.
Ce livre est leur premier ouvrage.


Description
=============

"Grand remplacement", "immigrationnisme", "bataille de civilisation"...

Les mots de l’extrême droite, et par là ses obsessions racistes, ont envahi
les débats politique et médiatique grâce à un intense combat mené par la
fachosphère et ses troufions sur Internet.

Cet écosystème coordonné, pensé et interconnecté a permis à ces "idées" de
se répandre jusque dans les médias, avec l’appui de Bolloré et de ses sbires
littéralement en croisade.

Combien de vues se transforment en voix pour le Rassemblement national ?
Comment en est-on arrivé là ?

Plongée dans le "pop fascisme", cette extrême-droitisation des esprits qui
joue avec les codes de la culture populaire en ligne et infiltre l’époque.


Cité en 2024
===============

2024-08-22 Pierre Plottu
----------------------------------------------------------------------------

- https://www.radiofrance.fr/franceinter/podcasts/un-monde-nouveau/un-monde-nouveau-du-jeudi-22-aout-2024-3110480

Dans Un Monde nouveau, où les mots de l'extrême droite ont inondé les réseaux
et l'époque, le chercheur Pierre Plottu nous initie au pop-fascisme.

Pierre Plottu, introduction au "pop-fascisme"
+++++++++++++++++++++++++++++++++++++++++++++++++

Le constat est sans appel : petit à petit, année après année, les idées de
l'extrême-droite s’imposent dans la société.
Et ses idées qui, longtemps, n’ont pas eu droit de cité dans le débat public
en France, sont aujourd’hui devenues mainstream.

Par quel processus ? Notre invité formule une hypothèse qui tient en un
seul mot : Internet.

Journaliste spécialiste des mouvances d'extrême-droite, Pierre Plottu est le
coauteur d'un essai passionnant, "Pop-Fascisme, comment l’extrême droite a gagné
la bataille culturelle sur internet.", aux éditions Divergences.

Une nouvelle forme d'extrême droite, dont l'essor passe essentiellement par
internet, séduit une partie de la jeunesse "connectée".

S'enracinant dans la "dissidence", la nouvelle droite ou la pensée identitaire,
elle répand ses idées sur les réseaux sociaux, les médias alternatifs et les
forums avec une vitalité qu'on ne lui connait pas dans la rue.

De l'influenceuse lifestyle au dessinateur de BD, de l'humoriste au lanceur
d'alerte, elle s'appuie sur des stratégies diverses pour séduire hors du
cadre de la politique traditionnelle.

Ce livre propose une enquête immersive sur ces figures influentes du web,
sur leurs moyens de diffusion et de subsistance.

L'extrême droite a-t-elle gagné la bataille culturelle en ligne ?
