.. index::
   pair: Livre; Le Monde nazi 1919-1945 (2024-09-19)

.. _monde_nazi_1919_1945:

===================================================================================================
**Le Monde nazi 1919-1945** par Johann CHAPOUTOT, Christian INGRAO et Nicolas PATIN (2024-09-19)
===================================================================================================

- https://www.tallandier.com/livre/le-monde-nazi/
- https://x.com/ed_tallandier/

.. figure:: images/recto.webp

::

    Date de parution : 19 septembre 2024
    27.50 €
    640 pages
    Format : 15.5x23 cm
    EAN papier : 9791021045804
    EAN numérique : 9791021045828
    Droits disponibles à l'étranger


Auteurs
==========

Johann CHAPOUTOT
---------------------

- https://www.tallandier.com/auteur/johann-chapoutot/

Johann CHAPOUTOT est ancien élève de l’ENS, agrégé d’histoire, docteur
de l’université Paris I, Dr. Phil. de la TU Berlin et professeur d’histoire
contemporaine à la Sorbonne.

Il est l’auteur de La Loi du sang. Penser et agir en nazi (2014, rééd. 2020)
et d’une dizaine d’autres ouvrages traduits en quinze langues.

Christian INGRAO
----------------------

- https://www.tallandier.com/auteur/christian-ingrao/
- :ref:`christian_ingrao`

Christian INGRAO est agrégé d’histoire, docteur de l’université d’Amiens,
Dr. Phil. de l’université de Stuttgart, directeur de recherches au CNRS
et ancien directeur de l’IHTP.

Il est l’auteur de La Promesse de l’Est. Espérance nazie et génocide, 1939-1943 (2016)
et d’une dizaine d’ouvrages individuels et collectifs, traduits en une
dizaine de langues.

Nicolas PATIN
------------------

- https://www.tallandier.com/auteur/nicolas-patin/

Nicolas PATIN est ancien élève de l’ENS, agrégé d’histoire, docteur de
l’université Paris-Nanterre et Dr. Phil. de la Ludwig-Maximilians-Universität
de Munich, membre junior de l’IUF et maître de conférences à l’université
Bordeaux-Montaigne.

Il est l’auteur de Krüger, un bourreau ordinaire (2017) traduit en allemand,
et d’une dizaine d’ouvrages individuels et collectifs.

Résumé
============

Le 30 janvier 1933, Adolf Hitler est nommé chancelier du Reich.

Les nazis avaient développé, depuis 1919 et le traumatisme de la Grande Guerre,
une vision du monde qui n’avait d’original que sa cohérence raciste et
son élan utopique.

Ils surent exploiter le contexte d’une crise majeure, celle de 1929,
pour subjuguer les consciences et accéder au pouvoir.

Le pouvoir leur fut donné, avec une inconséquence sidérante, par les élites
en place qui pensaient que Hitler ne tiendrait que quelques semaines et
que ses partisans seraient « domestiqués ».

Or les nazis prirent immédiatement le contrôle du pays avant de le conduire
à la destruction, réduisant finalement le continent tout entier à un
immense charnier.

Le monde intérieur nazi, cet imaginaire politique pétri de haine, d’angoisse
et d’utopie, avait donné naissance en l’espace de douze années à un monde
infernal ; un monde qui impliquait la mort de dizaines de millions de
personnes, dont la majorité des Juifs du continent.

Dans cet ouvrage, trois historiens du nazisme proposent un récit inédit,
une histoire totale du national-socialisme, de sa naissance en 1919 à son
effondrement en 1945.

En se fondant sur les renouvellements de l’historiographie internationale
de ces trente dernières années ainsi que sur une pratique constante des
sources, Johann Chapoutot, Christian Ingrao et Nicolas Patin analysent
le nazisme de l’intérieur : le système de croyances, les émotions fanatiques
et la culture militante des années 1920 ; la nature du « Troisième Reich »
comme « dictature de la participation » fondée sur un consentement massif
de la population ; enfin, la « guerre génocide » de 1939-1945, apocalypse
raciale qui réalise les potentialités de l’eschatologie nazie.


Sur les réseaux sociaux
===========================

https://x.com/Ed_Tallandier/status/1836753373472719328
-----------------------------------------------------------

- https://x.com/Ed_Tallandier/status/1836753373472719328

"Le Monde nazi - 1919-1945"

En se fondant sur les renouvellements de l’historiographie de ces trente
dernières années, Johann Chapoutot, Christian Ingrao et Nicolas Patin
proposent une histoire totale du national-socialisme de 1919 à 1945.
