.. index::
   pair: Livres; 2024

.. _droitisation_2024_09_04:

=====================================================================================
2024-09-04 **La droitisation française, mythe et réalités** par Vincent Tiberj
=====================================================================================

- :ref:`vincent_tiberj`
- https://www.puf.com/la-droitisation-francaise-mythe-et-realites

.. figure:: images/recto_droitisation.webp



Vincent Tiberj
=====================

- :ref:`vincent_tiberj`

Résumé
===========

La France deviendrait conservatrice.

C’est une évidence pour beaucoup d’intellectuels et de journalistes, et les résultats
électoraux semblent leur donner raison.

Pourtant ce n’est pas la thèse de ce livre.

Les citoyens français sont devenus beaucoup plus ouverts et progressistes qu’il
n’y paraît.
Face à cette situation paradoxale, Vincent Tiberj analyse comment offre politique
et citoyens divergent.
Il pointe l’importance de la manière dont on parle des inégalités sociales et
des questions de société « en haut », qui vont à rebours des préoccupations
d’« en bas ».

Il met en avant la grande démission citoyenne face aux partis, aux candidats : avec
ce silence électoral grandissant, les voix des urnes sont de moins en moins
représentatives.

La droitisation est un mythe, mais comme tous les mythes il pourrait bien avoir
des lourdes conséquences sur les équilibres politiques et l’avenir des Français.


Emission en 2024
======================

- https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-8h20-le-grand-entretien/l-invite-de-8h20-le-grand-entretien-du-mardi-03-septembre-2024-4694700
