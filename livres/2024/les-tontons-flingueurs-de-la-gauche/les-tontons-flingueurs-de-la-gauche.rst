.. index::
   pair: Livre; Les Tontons flingueurs de la gauche (2024, par Philippe CORCUFF et Philippe MARLIERE, éditions Textuel)

.. _tontons_flingueurs_2024:

===========================================================================================================================
2024-04-03 **Les Tontons flingueurs de la gauche** (2024, par Philippe CORCUFF et Philippe MARLIERE, éditions Textuel)
===========================================================================================================================

- https://www.editionstextuel.com/livre/les_tontons_flingueurs_de_la_gauche

.. figure:: images/couverture.webp
   :width: 500


Auteurs
=========

- :ref:`philippe_corcuff`
- :ref:`philippe_marliere`



pré-Présentation par presseagence
====================================

- https://presseagence.fr/paris-les-tontons-flingueurs-de-la-gauche-par-philippe-corcuff-et-philippe-marliere-2/

Le livre de Philippe Corcuff et Philippe Marlière, "Les Tontons flingueurs de
la gauche, lettres ouvertes à Hollande, Macron, Mélenchon, Roussel, Ruffin,
Onffray" sort le 3 avril 2024 aux éditions Textuel.

Philippe Corcuff est à Paris du 1er au 4 avril puis du 30 avril au 2 mai.
Un entretien de deux pages avec Julie Clarini sera publié dans les pages idées
de l’Obs.

L’extrême droite progresse à grandes enjambées.
À gauche, peu de gens mesurent la gravité de la situation.

Ce petit livre se présente comme un cri d’alarme, inspiré par le souffle
encore chaud de Rosa Luxemburg et de Pierre Mendès France ; une révolutionnaire
éprise de pluralisme et un réformateur soucieux d’éthique.

La gauche a trop souffert des hommes providentiels qui prétendent lui offrir
un nouvel avenir mais dont l’action, finalement, l’affaiblit.

Dans cet essai incisif, deux intellectuels engagés adressent une lettre
ouverte à six personnalités politiques issues de leur propre famille :
un ancien président qui a perdu la gauche, un président marchepied vers
l’extrême droite, un insoumis autoritaire, un communiste sécuritaire,
un rebelle nationaliste et un libertaire ultraconservateur. Ils ont tous
contribué, à des degrés divers, au présent naufrage.

Reste à réinventer une gauche d’émancipation, qui fera le deuil du leader
charismatique mais qui aura besoin d’incarnations, plurielles et polyphoniques.

Philippe Corcuff est professeur de science politique à l’Institut d’études
politiques de Lyon. Parmi les intellectuels de gauche, il est l’une des voix
les plus stimulantes de notre époque. Ses recherches se situent au croisement
de la sociologie et de la philosophie politique.

**Il est l’auteur d’une dizaine d’ouvrages chez Textuel, dont le remarqué
La Grande Confusion (2022)**.

Philippe Marlière est professeur de science politique au University College
de Londres.
Spécialiste de la social-démocratie européenne, ses recherches actuelles portent
sur le populisme de gauche et l’idéologie républicaine en France.

Il a notamment cosigné La gauche ne doit pas mourir !
Le manifeste des socialistes affligés (Les liens qui libèrent, 2014).



Présentation par  editionstextuel
======================================

- https://www.editionstextuel.com/livre/les_tontons_flingueurs_de_la_gauche

L’extrême droite progresse à grandes enjambées. À gauche, peu de gens mesurent
la gravité de la situation.

Ce petit livre se présente comme un cri d’alarme, inspiré par le souffle encore
chaud de Rosa Luxemburg et de Pierre Mendès France ; une révolutionnaire éprise
de pluralisme et un réformateur soucieux d’éthique.

La gauche a trop souffert des hommes providentiels qui prétendent lui offrir
un nouvel avenir mais dont l’action, finalement, l’affaiblit.

Dans cet essai incisif, deux intellectuels engagés adressent une lettre ouverte
à six personnalités politiques issues de leur propre famille :

- un ancien président qui a perdu la gauche,
- un président marchepied vers l’extrême droite,
- un insoumis autoritaire,
- un communiste sécuritaire,
- un rebelle nationaliste
- et un libertaire ultraconservateur.

Ils ont tous contribué, à des degrés divers, au présent naufrage.

Reste à réinventer une gauche d’émancipation, qui fera le deuil du leader
charismatique mais qui aura besoin d’incarnations, plurielles et polyphoniques.


Présentation par Philippe MARLIERE
=======================================

Ce livre, inscrit sous le double patronage de Rosa Luxemburg et de Pierre Mendès-France,
traite de questions habituelles pour le RAAR, comme l'antisémitisme et l'islamophobie.

Les Tontons flingueurs de la gauche. Lettres ouvertes à Hollande, Macron,
Mélenchon, Roussel, Ruffin, Onfray

Par Philippe Corcuff (professeur de science politique à Sciences po Lyon) et
Philippe Marlière (professeur de science politique à University College à Londres)

Editions Textuel, collection "Petite Encyclopédie critique", 94 p., 11,90 euros, 3 avril 2024

Sommaire
-----------

- Introduction
- En finir avec les "sauveurs suprêmes" qui affaiblissent la gauche
- Un contexte confusionniste favorisant l'extrême droitisation
- Impasses d'un républicanisme autoritaire et d'un populisme sans démocratie
- Lettre ouverte à un ancien président qui a perdu la gauche
- Lettre ouverte à un président barrage-marchepied de l'extrême droite
- Lettre ouverte à un "insoumis" autoritaire
- Lettre ouverte à un communiste sécuritaire
- Lettre ouverte à un rebelle fermé au monde
- Lettre ouverte à un anarchiste à la dérive ultraconservatrice

Conclusion : Pour une gauche concrètement d'émancipation



Cité en 2025
=================

-  :ref:`raar_2025:politique_2025_01_07`

Cité en 2024
=================

- :ref:`raar_2024:corcuff_2024_07_01`
- :ref:`raar_2024:corcuff_2024_04_29`


Liens
=======

2023-12-10 leftrenewal **Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche**
-----------------------------------------------------------------------------------------------------------------------------------------------

- https://leftrenewal.net/
- https://leftrenewal.net/french-version/
- :ref:`raar_2023:leftrenewal_fr_2023_12_10`
