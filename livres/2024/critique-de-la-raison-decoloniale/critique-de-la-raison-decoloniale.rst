.. index::
   pair: Livre; Critique de la raison décoloniale (2024-10-04)

.. _critique_decoloniale_2024:

==========================================================================================================
2024-10-04 **Critique de la raison décoloniale** Sur une contre-révolution intellectuelle par Collectif
==========================================================================================================

- https://www.lechappee.org/collections/versus/critique-de-la-raison-decoloniale


.. figure:: images/recto.webp


Critique de la raison décoloniale
=========================================

Traduit de l'espagnol par Mikaël Faujour et Pierre Madelin

Avant-propos de Mikaël Faujour

Le capitalisme et la modernité seraient intrinsèquement liés à un racisme
d’essence coloniale et à la domination de l’Occident sur le Sud global :
tel est le postulat des décoloniaux.

Face à une rationalité considérée comme eurocentrique, face à un système
de pouvoir qui chercherait à maintenir les « non-Blancs » dans une position
subalterne, ils prônent un retour aux formes de savoir et aux visions du
monde des peuples indigènes.

À l’heure où les théories décoloniales, nées en Amérique latine,
gagnent du terrain dans les milieux universitaires et militants, les auteurs
de ce livre, ancrés eux aussi dans ce continent, font entendre une autre
voix.

Ils démontrent comment **ces théories propagent une lecture simpliste** de
l’histoire et des rapports de pouvoir, et comment leur focalisation sur les
questions d’identité ethno-raciale relègue au second plan l’opposition
pourtant fondamentale entre riches et pauvres.

À l’horizon, une conviction: seul un anticolonialisme fondé sur une critique
radicale du capitalisme permettra de sortir de cette impasse, en dépassant
toute soif de revanche pour retrouver le contenu universel des luttes d’émancipation.

Les auteurs de ce recueil
============================

- Pierre Gaussens,
- Gaya Makaran,
- Daniel Inclán,
- Rodrigo Castro Orellana,
- Bryan Jacob Bonilla Avendaño,
- Martín Cortés et
- Andrea Barriga

sont des universitaires qui travaillent au carrefour de la sociologie,
de la philosophie et de l’histoire.


Vidéos/textes 2025
======================

- :ref:`raar_2025:pensee_decoloniale_2025_03_14`

Vidéos/textes 2024
======================

- :ref:`raar_2024:critique_decoloniale_2024_11_06`
- https://leftrenewal.org/fr/book-reviews-fr/sidi-moussa-sur-critique-de-la-raison-decoloniale/
  (https://oclibertaire.lautre.net/spip.php?article4327)

Recension de Critique de la raison décoloniale par Sébastien Navarro dans lechappee
------------------------------------------------------------------------------------------

- https://www.lechappee.org/actualites/misere-de-la-pensee-decoloniale


Critique de la raison décoloniale par Nedjib SIDI MOUSSA dans OCL libertaire
----------------------------------------------------------------------------------

- https://oclibertaire.lautre.net/spip.php?article4327
- https://oclibertaire.lautre.net/spip.php?auteur4 (Nedjib SIDI MOUSSA)


Recension de Critique de la raison décoloniale par Christophe Fourel dans Alternatives Économiques.
--------------------------------------------------------------------------------------------------------

- https://www.lechappee.org/actualites/un-remarquable-livre-collectif


2024-10-02 Recension de Critique de la raison décoloniale par Denis Bayon dans La Décroissance (n°213, octobre 2024).
---------------------------------------------------------------------------------------------------------------------------

- https://www.lechappee.org/actualites/decoloniaux

Depuis plusieurs années, la pensée dite "décoloniale" est très présente
à l'université et chez certains militants.

Selon celle-ci, tous les maux des peuples "non blancs" s'expliqueraient
par le "fait colonial", qui constituerait le coeur de la domination
occidentale sur les personnes "racisées".

Ces théories se sont développées dans universités états-uniennes avant
de se diffuser partout.

Non sans rencontrer des oppositions : le recueil Critique de la raison
décoloniale en témoigne.

Les auteurs - des chercheurs d'Amérique du Sud - n'y vont pas par quatre
chemins et affirment que celle-ci n'est rien d'autre qu'une "contre-révolution intellectuelle" (...).

2024-10-13 « Grandeur et errances du mouvement décolonial » Publication des bonnes feuilles de Critique de la raison décoloniale dans l'Antichambre des éditions Agone
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.lechappee.org/actualites/grandeur-et-errances-du-mouvement-decolonial
- https://agone.org/grandeur-et-errances-du-mouvement-decolonial/

- https://www.lechappee.org/collections/versus/critique-de-la-raison-decoloniale
- https://www.academia.edu/44583790/Piel_blanca_m%C3%A1scaras_negras_Cr%C3%ADtica_de_la_raz%C3%B3n_decolonial
- https://www.leddv.fr/entretien/pierre-gaussens-les-militants-decoloniaux-pensent-et-pratiquent-un-antiracisme-a-la-fois-individualiste-et-elitiste-20211222
- https://www.radiofrance.fr/franceculture/podcasts/questions-du-soir-le-debat/etudes-decoloniales-renouveau-ou-simplisme-theorique-1843589
