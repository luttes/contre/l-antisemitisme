.. index::
   pair: Livre ; Des électeurs ordinaires Enquête sur la normalisation de l'extrême droite (2024-05)
   pair: Félicien Faury ; Des électeurs ordinaires Enquête sur la normalisation de l'extrême droite (2024-05)

.. _electeurs_ordinaires_2024_05:

===============================================================================================================
2024-05 **Des électeurs ordinaires Enquête sur la normalisation de l'extrême droite** par Félicien Faury
===============================================================================================================

- :ref:`felicien_faury` |FelicienFaury|
- https://www.seuil.com/ouvrage/des-electeurs-ordinaires-felicien-faury/9782021518948

Le livre
==========

Recto
-------

.. figure:: images/recto.webp

Verso
--------

.. figure:: images/verso.webp


Description
===============

Ils sont artisans, employés, pompiers, commerçants, retraités…

Ils ont un statut stable, disent n’être « pas à plaindre » même si les
fins de mois peuvent être difficiles et l’avenir incertain.

Et lorsqu’ils votent, c’est pour le Rassemblement national.

De 2016 à 2022, d’un scrutin présidentiel à l’autre, le sociologue
Félicien Faury est allé à leur rencontre dans le sud-est de la France,
berceau historique de l’extrême droite française.

Il a cherché à comprendre comment ces électeurs se représentent le monde
social, leur territoire, leur voisinage, les inégalités économiques,
l’action des services publics, la politique.
Il donne aussi à voir la place centrale qu’occupe le racisme, sous ses
diverses formes, dans leurs choix électoraux.

Le vote RN se révèle ici fondé sur un sens commun, constitué de normes
majoritaires perçues comme menacées – et qu’il s’agit donc de défendre.

À travers des portraits et récits incarnés, cette enquête de terrain
éclaire de façon inédite comment les idées d’extrême droite se diffusent
au quotidien.

Félicien Faury est sociologue et politiste, chercheur postdoctoral
au CESDIP (Centre de recherches sociologiques sur le droit et les institutions pénales).

Débat sur le livre en 2025
==============================

- :ref:`extremes_droites:felicien_faury_2025_01_11`

Rencontres 2024
=================

- :ref:`raar_2024:webinaire_2024_11_07`
