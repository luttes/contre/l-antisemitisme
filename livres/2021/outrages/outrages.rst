.. index::
   pair: Outrages; Tal Piterbraut-Merx , 2021-03-12

.. _outrages_2021_03_12:

====================================================================
**Outrages** par Tal Piterbraut-Merx (2021-03-12)
====================================================================

- :ref:`tal_piterbraut_merx`
- https://www.editionsblast.fr/outrages
- https://www.editionsblast.fr/auteur-es


.. figure:: images/couverture.webp

Description
==============

Être out c’est passer de l’ombre à... ? Cette lumière permanente sur elle
l’aveugle, ses rayons l’écrasent.

Elle craint de ne jamais plus pouvoir rentrer au-dedans, de demeurer
toujours exposée.

Pourquoi les autres, les normaux, n’ont-ils jamais ce souci ?

D’être pris par une nuit trop sombre dans un phare indiscret.

Une femme, infirmière en psychiatrie, est sur le chemin menant au quartier
de son enfance et à une famille qu’elle n’a pas vue depuis des années.

Dans ce qui la fait avancer, il y a l’un des espaces de sa vie, celui
de la lesbianité qu’elle pense et raconte.

Il y a aussi la trace de violences anciennes que d’autres voudraient
indicibles.
Ici, pas de confrontation, de retrouvailles ou de retour mais un roman
de passage et d’insoumission dans lequel se mêlent mémoire juive,
histoire familiale et violence de l’inceste.

Et à la honte s’oppose alors l’outrage de la parole.


**À la mémoire de Tal Piterbraut-Merx**
==========================================

C’est avec une immense tristesse que nous vous faisons part du décès de Tal Piterbraut-Merx.
Tal était une personne lumineuse.
Tal était juive, gouine, féministe et fière.
Tal était aussi victime d’inceste et luttait contre les violences faites aux enfants.
Ses engagements étaient pluriels, toujours profondément politiques et à
l’intersection de nombreuses luttes contre les oppressions systémiques.
Tal était chercheur et auteur. Nous le remercions encore de la confiance
qu’il nous a portée avec Outrages.

D’autres projets s’annonçaient, passionnants : un essai tiré de sa thèse
sur les rapports et la domination adulte-enfant et un nouveau roman.

Ces ouvrages ne verront pas le jour et c’est une immense perte pour la
littérature queer, féministe, antiraciste.

Autant de pistes tracées avec une si grande justesse et qui resteront à
jamais ouvertes.

Tal était brillant. Travailler avec elle a été d’une très grande richesse :
nos multiples discussions ont nourri et nourrissent toujours notre travail
quotidien.

Nous gardons précieusement en tête tous nos échanges, qui dépassaient le
cadre professionnel et ont transformé notre relation en amitié.

Tal s’est suicidé. Ce geste, aussi brutal et difficile à recevoir,
soulève des questions politiques.

À toustes celles et ceux qui ont perdu Tal et à qui il a tant apporté,
nous exprimons nos profondes pensées.

Nous continuerons de porter avec force et fierté la voix de Tal.
Que jamais on ne l’oublie
