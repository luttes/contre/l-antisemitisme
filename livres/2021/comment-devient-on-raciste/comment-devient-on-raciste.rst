.. index::
   pair: Livre; Comment devient-on raciste ? (2021)

.. _comment_raciste_2021:

=================================================================================================
**Comment devient-on raciste ?** par  Evelyne Heyer, Carole Reynaud-Paligot et Ismaël Méziane
=================================================================================================

- https://www.casterman.com/Bande-dessinee/Catalogue/comment-devient-on-raciste/9782203211902


Comment devient-on raciste ?
===============================

Comprendre la mécanique de la haine pour mieux s'en préserver

Scénario
==========

- Evelyne Heyer,
- Carole Reynaud-Paligot

Dessin
=========

Ismaël Méziane


À de multiples reprises, Ismaël Méziane a vécu personnellement le racisme.

En 2017, lorsqu’il visite l’exposition « Nous et les Autres » au Musée de
l’Homme à Paris, c’est un véritable choc qu’il a aussitôt envie de partager.

Il entreprend alors de réaliser une bande dessinée avec les deux commissaires
de l’exposition, respectivement anthropologue généticienne et historienne.

Sous forme de déambulation, il propose un mélange de réflexions personnelles
et d’échanges avec ces deux spécialistes afin de comprendre en profondeur
les mécanismes à l’origine du racisme.

L’album entremêle savoir et émotions pour susciter une prise de conscience,
qui reste malheureusement plus qu’utile aujourd’hui.
