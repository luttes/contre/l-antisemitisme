.. index::
   pair: Livres; 2021

.. _livre_courouble_2021:

===============================================================================
**Les idées fausses ne meurent jamais** par Stéphanie Courouble Share (2021)
===============================================================================

- https://www.editionsbdl.com/produit/les-idees-fausses-ne-meurent-jamais/
- https://stephanieshare.com/
- https://www.conspiracywatch.info/author/stephanie-courouble-share


.. figure:: images/recto.webp
   :width: 500

Présentation
=============

"Salut, je m’appelle Anon, je pense que l’Holocauste n’a jamais eu lieu."

Tels sont les mots qu’a prononcés Stephan Balliet juste avant de commettre
l’attentat de Halle contre une synagogue, le 9 oct. 2019. C’était un jour de kippour.

Cette "pensée", tout droit issue du négationnisme, terme qui désigne la négation
de la réalité du génocide des Juifs durant la Seconde guerre mondiale, octroie
ainsi un permis de tuer : les Juifs sont le mal par essence.

Affabulateurs, ils n’ont pas été tués en masse par les Nazis et perdent leur
statut de victimes.
Sous couvert d’une pseudo-démarche rationnelle, les négationnistes prétendent
le démontrer et réhabilitent le nazisme en le disculpant de ses crimes.

Partant de ce constat, cet ouvrage remonte le cours de l’histoire et observe
le négationnisme dans sa dimension internationale depuis la fin de la Seconde
Guerre mondiale jusqu’à nos jours.

Exhumant des archives de France, d’Angleterre, d’Allemagne, des États-Unis
et du Canada, l’historienne Stéphanie Share a enquêté sur l’émergence d’un
mouvement, sa pénétration dans la sphère publique des démocraties occidentales,
et analysé les scandales négationnistes qui ont défrayé la chronique de ces
différents pays.

Ce livre alerte sur l’urgence du combat à mener : les idées fausses
ne meurent jamais et peuvent tuer.

Préface de Pascal Ory, de l’Académie française


Stéphanie Courouble Share
==========================

Stéphanie Courouble Share est historienne, spécialiste du négationnisme,
petite-fille de déporté.

Ancienne élève de l’historien Pierre Vidal-Naquet, elle est actuellement
chercheure associée à l’Institute for the Study of Global Antisemitism and
Policy (ISGAP-New York).


Rencontres 2024
============================

- :ref:`raar_2024:hirsch_2024_05_26`
