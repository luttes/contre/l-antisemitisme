.. index::
   pair: Livre; La grande confusion (Philippe corcuff, 2021)

.. _la_grande_confusion:

====================================================================
**La grande confusion** de Philippe corcuff (2021) |corcuff|
====================================================================

- https://www.editionstextuel.com/livre/la_grande_confusion
- https://www.librairiesindependantes.com/product/9782845978546/
- :ref:`philippe_corcuff`


.. figure:: images/couverture.webp

Description
==============

À travers une cartographie précise, exhaustive et référencée des débats
idéologiques actuels, Philippe Corcuff montre comment l'extrême droite
parvient à imposer ses haines et ses obsessions au coeur du débat public.

Il dresse pour cela un vaste panorama des idées politiques en vogue dans la
France contemporaine.

D'Éric Zemmour à Frédéric Lordon en passant par Michel Onfray, Emmanuel Macron
et Jean-Luc Mélenchon. <br />

C'est avant tout une boussole que nous tend Corcuff : notre époque est traversée
par des vents violents.

Comment y voir plus clair dans le brouillard confusionniste ?

Un livre qui va sans nul doute faire le buzz par sa charge explosive et la
liste des discours et personnalités finement cités.

Cité en 2025
================

- :ref:`raar_2025:la_grande_confusion_2025_01_07`

Cité en 2024
=================

- :ref:`raar_2024:corcuff_2024_07_04`
- :ref:`raar_2024:corcuff_2024_07_01`
- :ref:`raar_2024:corcuff_2024_04_29`
