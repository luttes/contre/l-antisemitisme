.. index::
   pair: Livre; Israël et ses paradoxes (Denis Charbit, 2015)

.. _israel_et_ses_paradoxes_2015:
.. _charbit_livre_paradoxe_2015:

====================================================================
**Israël et ses paradoxes (Denis Charbit, 1ere edition 2015)**
====================================================================

- :ref:`denis_charbit`
- https://www.lalibrairie.com/livres/israel-et-ses-paradoxes--idees-recues-sur-un-pays-qui-attise-les-passions_0-1427298_9782846704427.html
- https://www.cairn.info/israel-et-ses-paradoxes--9782846704427.htm

Israël et ses paradoxes
==============================

Idées reçues sur un pays qui attise les passions

Israël, plus que tout autre pays, suscite les passions. Pro- et anti-attisent
la polémique à coup d'idées reçues : « Israël et le lobby juif dictent la
politique des États-Unis au Moyen-Orient », « Israël est la seule démocratie au Moyen-Orient »,
« Israël pratique l'apartheid », « Tsahal est l'armée du peuple », « Israël est un État théocratique », etc.

À force de combattre les préjugés des uns et des autres, c'est débattre qui
est devenu impossible.

C'est la grande ambition de ce livre que de favoriser le retour au débat : exigeant,
argumenté, contradictoire, conjuguant les divergences, autorisant les convergences.

Quiconque tient que toute la vérité vient d'Israël et que la Palestine est
mensonge ; quiconque pense que le Mal est israélien et que le Bien est palestinien,
ne trouvera guère dans ce livre de quoi blinder ses convictions.

Il ne s'agit pas non plus de décréter une symétrie entre les causes et de croire
à une neutralité possible.

Ni État d'exception ni État exceptionnel, Israël est « un État comme les autres »,
avec ses paradoxes.
Cela ne le dispense nullement de rendre compte de ce qu'il est et de rendre
des comptes sur ce qu'il fait.

Biographie
==============

Denis Charbit est maître de conférences au département de sociologie, science
politique et communication à l'Open University d'Israël.

Ses recherches portent sur le sionisme et sur les intellectuels français de
l'Affaire Dreyfus à nos jours.
