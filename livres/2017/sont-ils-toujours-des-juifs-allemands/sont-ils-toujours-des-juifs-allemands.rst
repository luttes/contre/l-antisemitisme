.. index::
   pair: Livre; Sont-ils toujours des Juifs allemands ? (Robert Hirsch, 2017)

.. _juifs_allemands:

==================================================================================================================================================
|JuifsAllemands|  **Sont-ils toujours des Juifs allemands ?** La gauche radicale et les Juifs depuis 1968 par Robert Hirsch |RobertHirsch|
==================================================================================================================================================

- :ref:`robert_hirsh`
- http://arbre-bleu-editions.com/sont-ils-toujours-des-juifs-allemands.html

.. figure:: images/juifs_allemands.png


Tout ce qui concerne les Juifs est sujet à maintes discussions dans
l’Europe d’après la Shoah.

Ces dernières années, le débat sur l’antisémitisme a pris une ampleur
considérable, le mal se manifestant à nouveau sur le Vieux Continent.

En France, ce fut sous une forme meurtrière, dont l’assassinat d’enfants
à Toulouse en 2012 ou l’attentat de l’Hyper Cacher en 2015.

Ce livre tente, après d’autres, d’expliquer les racines de ce renouveau
antisémite.
Il le fait du point de vue d’un historien engagé dans la gauche radicale
depuis 1968, et qui tente de comprendre pourquoi « la gauche de la gauche »
s’est si peu mobilisée dans les années 2000 contre ce nouvel antisémitisme,
elle qui avait proclamé en 1968 « Nous sommes tous des Juifs allemands ».

Les explications ne résideraient-elles pas dans le fait que des populations
nouvelles, elles-mêmes discriminées, reprennent à leur compte la haine
des Juifs ?

Et dans la politique de l’État d’Israël, qui viendrait perturber les
combats anciens contre l’antisémitisme ?


Mais ce n’est pas la seule question qu’aborde Robert Hirsch dans cet ouvrage.

Il part à la recherche du lien entre la jeunesse juive des années 1960-1970
et la gauche radicale.

Intéressé par l’ampleur de la participation de ces jeunes Juifs et Juives
à la radicalisation gauchiste, il tente d’en explorer les raisons.

Le livre décrit également la distanciation qui s’opère entre la gauche
radicale et les Juifs, ainsi que les modifications générationnelles
qui l’expliquent.



Au total, un ouvrage qui, du sein de la gauche radicale, tente d’étudier
l’évolution des liens forts entre elle et les Juifs, mais aussi ses
hésitations à propos du retour de l’antisémitisme, en prenant garde de
ne pas tomber dans les caricatures trop souvent présentées à ce sujet.

Un livre qui relie une actualité souvent dramatique à une histoire plus
complexe qu’on ne veut bien le dire.

Une voix qui rappelle combien ce qui concerne les Juifs est au cœur
des problèmes contemporains et qui affirme que la gauche radicale n’a
pas le droit de s’en désintéresser.


Robert Hirsch, docteur en histoire, est l’auteur d’Instituteurs et
institutrices syndicalistes, 1944-1967 (Paris, Syllepse, 2010) et a
participé à des ouvrages collectifs.

Professeur agrégé dans le secondaire, il a également été chargé de
cours à l’Université Paris XIII de 1999 à 2006.
