.. index::
   pair: Livre; Qu'est-ce que le sionisme ? (Denis Charbit, 2007)

.. _charbit_livre_sionisme_2007:

====================================================================
**Qu'est-ce que le sionisme ?** (Denis Charbit, 2007)
====================================================================

- :ref:`denis_charbit`
- https://www.lalibrairie.com/livres/qu-est-ce-que-le-sionisme--_0-368577_9782226173133.html

Description
=============

Le mot **sionisme**, né il y a à peine plus de cent ans, ne semble plus compris
de nos jours et subit toutes les altérations qu'entraîne la polémique.

Ce livre vise à rétablir son sens véritable et à examiner ce qu'il en est dans
la réalité israélienne d'aujourd'hui : revenir à Sion, terre ancestrale,
reconstituer les juifs en tant que nation, créer un État démocratique,
rétablir la langue hébraïque et offrir aux juifs en détresse un refuge
et une patrie.

Biographie
=============

Denis Charbit, maître de conférences en science politique à l'Open University d'Israël,
a notamment publié :ref:`Sionismes. Textes fondamentaux, chez Albin Michel <charbit_livre_1998>`.
