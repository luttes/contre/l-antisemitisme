.. index::
   ! SUD-education

.. _sud_education:

======================================
|SudEducation| **SUD-education**
======================================

- https://www.sudeducation.org/

.. _sud_education_2023:

Communiqués 2023
=====================

- :ref:`sud_education_2023_11_16`
