.. index::
   pair: SJO ; Solidarité Judéo-Arabe
   ! SJO

.. _sjo:

======================================================================
**Solidarité Judéo-Arabe** (SJO)
======================================================================

#solidaritejudeoarabe #solidarité #antiracisme #paix #israel #palestine

- https://www.instagram.com/solidaritejudeoarabe/

Description
=============

Colleur-euses juif-ves LGBTQIA+,
en questionnement, queer, en lutte contre tous systèmes d'oppression
et de domination.

Paris/banlieues▫️
