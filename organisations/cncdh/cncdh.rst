.. index::
   pair: CNCDH ; Commission nationale consultative des droits de l'homme
   ! CNCDH

.. _cncdh:

=================================================================================
**CNCDH** (Commission nationale consultative des droits de l'homme) |CNCDH|
=================================================================================

- https://fr.wikipedia.org/wiki/Commission_nationale_consultative_des_droits_de_l%27homme

Description
=============

La Commission nationale consultative des droits de l’homme (CNCDH)
est une institution nationale française de protection et de promotion
des droits de l’homme fondée en 1947 à l'initiative de René Cassin, et
accréditée de statut A auprès des Nations unies.

En droit français, elle est assimilée à une autorité administrative
indépendante, dotée d’une mission de conseil auprès des décideurs
publics en matière de droits de l’homme et de droit international
humanitaire, et d’une mission de contrôle des engagements internationaux
de la France en ces matières.

La CNCDH a cinq mandats de Rapporteur national indépendant. Elle évalue :

- la politique de lutte contre le racisme, l’antisémitisme et la xénophobie
  depuis 19901;
- la politique menée pour lutter contre la traite et l’exploitation des
  êtres humains depuis 20142;
- la mise en œuvre des Principes directeurs des Nations unies sur les
  Entreprises et les droits de l'homme depuis 2017 3
- la mise en œuvre du Plan national de lutte contre toutes les formes
  de haines anti-LGBT depuis 20184
- l'effectivité des droits des personnes en situation de handicap
  depuis 20205.

La CNCDH est également la Commission de mise en œuvre du droit international
humanitaire au sens du CICR.


Citée en 2023
===============

- :ref:`cncdh_2023_04_24`


Rapports
============

.. toctree::
   :maxdepth: 3

   rapports/rapports
