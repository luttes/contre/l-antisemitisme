.. index::
   ! Collectif Golem

.. _golem:

===========================================================================
|golem| **Collectif Golem**
===========================================================================

- https://www.instagram.com/collectif_golem/
- https://piaille.fr/@collectifgolem
- https://x.com/Collectif_Golem
- https://x.com/Golem21182346
- :ref:`golem:golem`
- contact : :ref:`fabienne_messica`

#raar #antisemitisme  #golem #CollectifGolem #Manif12Nov #marchecontrelantisemitisme #StopReconquête
#StopRN #StopZemmour #Antisémitisme
#mazeldon #jewdiverse


Description
=============

- https://www.instagram.com/p/Czi2AyMM0Uj/?utm_source=ig_web_copy_link&igshid=MzRlODBiNWFlZA==

Golem est un collectif de juifs et de juives de gauche (et allié.e.s)
qui lutte contre l'antisémitisme.

**L'augmentation, la multiplication des actes antisémites nous révolte
et nous oblige à agir**.

Nous nous mobilisons donc contre l'antisémitisme en participant à cette marche.

**Nous ne pouvons lutter contre l'antisémitisme aux côtés des antisémites**.
L'extrême-droite instrumentalise la lutte afin de réhabiliter son passé,
sa vision et ses structures antisémites, racistes et islamophobes.

D'autres informations et mobilisations à venir.

Articles 2024
====================

- :ref:`raar_2024:golem_2024_10_25`


Messages 2023
================

- :ref:`suivez_golem_2023_11_12`
- :ref:`golem_colere_2023_11_11`

Liens
=========


- https://en.wikipedia.org/wiki/Golem
- https://fr.wikipedia.org/wiki/Maharal
- https://fr.wikipedia.org/wiki/Maharal#Le_Golem
