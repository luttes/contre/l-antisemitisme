
.. figure:: images/riposte_unitaire.png
   :align: center

.. _nupes:

======================================================================
️✌️ NUPES/EELV (charte contre l'antisémitisme) ️✌️
======================================================================

.. figure:: images/antisemitisme_2022.png
   :align: center


.. toctree::
   :maxdepth: 5

   presentation/presentation
   charte/charte
   gt/gt
   motion/motion
