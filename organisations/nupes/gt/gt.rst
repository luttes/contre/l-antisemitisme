.. index::
   pair: Groupe de travail ; Lutte contre l’antisémitisme
   pair: Groupe de travail ; Nadine Herrati
   pair: Groupe de travail ; Grégory Gutierez
   pair: Groupe de travail ; Nathalie Fromont
   pair: Groupe de travail ; Nathan Guedj

.. _gt_antisemitisme:
.. _gt_eelv:

==========================================================================
Création du groupe de travail : **Lutte contre l’antisémitisme** EELV ️✌️
==========================================================================

- https://www.charte-lutte-antisemitisme.fr/les-signataires
- https://sites.google.com/view/contre-lantismitisme/accueil
- https://www.eelv.fr/files/2021/10/MB-GT-antisemitisme-CF2021100203.pdf
- :ref:`motion_gt_antisemitisme`

:download:`Télécharger le document au format PDF <pdf/MB-GT-antisemitisme_2021_10_02.pdf>`

.. @nadineherrati @greguti @FromontNathali4 @NathanGuedj_
.. Cyrielle Chatelain ✌️

Le GT Lutte contre l'antisémitisme
=======================================

:ref:`Créé en octobre 2021 <motion_gt_antisemitisme>` par un vote à
l'unanimité du Conseil Fédéral du parti Europe Ecologie - Les Verts, le
groupe de travail **Lutte contre l'antisémitisme** s'est notamment donné
pour mission "d'interroger les programmes de l’écologie politique aux
échéances électorales (y compris les élections présidentielles et législatives
de 2022) du point de vue de la lutte contre l’antisémitisme"
(lire `l'intégralité de la motion sur le site du parti <https://www.eelv.fr/mb-gt-ri-antisemitisme-cf2021100203/>`_).

Le groupe de travail est animé par:

- Nadine Herrati |Nadine Herrati| élue à Gentilly et co-secrétaire EELV pour le Val-de-Marne,
- :ref:`Grégory Gutierez <gregory_gutierez>` |Gregory Gutierez|, élu à Malakoff dans les Hauts-de-Seine,
- Nathalie Fromont |Nathalie Fromont|, militante EELV à Toulouse
- Nathan Guedj |Nathan Guedj|, co-secrétaire EELV Rhône.
