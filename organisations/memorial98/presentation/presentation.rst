.. index::
   pair: Présentation ; Memorial 98

.. _presentation_memorial_98:

===========================================================================
**Présentation de L'association MEMORIAL98**
===========================================================================

- https://fr.wikipedia.org/wiki/Affaire_Dreyfus

L'association MEMORIAL98, qui combat contre le racisme, l'antisémitisme
et le négationnisme a été créée en janvier 1998, lors du centenaire de
l'affaire Dreyfus.

Son nom fait référence aux premières manifestations organisées en janvier
1898, pendant l'affaire Dreyfus, par des ouvriers socialistes et
révolutionnaires parisiens s'opposant à la propagande nationaliste
et antisémite.

Ce site en est l'expression dans le combat contre tous les négationnismes

(Arménie, Rwanda, Shoah ...)
