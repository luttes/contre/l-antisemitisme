.. index::
   pair: Poutine ; Syrie-Ukraine


.. _syrie_ukraine_98_2023_03_17:

===========================================================================
2023-03-17 **Syrie-Ukraine: le combat commun des victimes de Poutine**
===========================================================================

- http://www.memorial98.org/2023/03/syrie-ukraine-le-combat-commun-des-victimes-de-poutine.html


La nouvelle
===========

Mise à jour importante du 17 mars : mandat d'arrêt contre Poutine,
responsable de la déportation d'enfants ukrainiens en Russie

Enfin ! Grande nouvelle : la Cour pénale internationale (CPI) émet un
mandat d'arrêt contre Vladimir Poutine et son acolyte Maria Lvova-Belova,
commissaire présidentielle aux droits de l'enfant en Russie et organisatrice
de la déportation des enfants ukrainiens en Russie ( ci-dessous avec Poutine)


La Cour pénale internationale (CPI) a déclaré vendredi 17 mars 2023 avoir
émis un mandat d'arrêt contre le président russe Vladimir Poutine,
l'accusant d'être responsable de crimes de guerre commis en Ukraine.


Des accusations que Moscou a niées à plusieurs reprises. "Aujourd'hui,
17 mars 2023, la Chambre préliminaire II de la Cour pénale internationale
a émis des mandats d'arrêt contre deux personnes dans le cadre de la
situation en Ukraine : M. Vladimir Vladimirovitch Poutine et Mme Maria
Alexeïevna Lvova-Belova", commissaire présidentielle aux droits de
l'enfant en Russie" ( organisatrice de la déportation des enfants NDLR) ,
a déclaré la CPI dans un communiqué. Vladimir Poutine "est présumé
responsable du crime de guerre de déportation illégale de population (enfants)
et de transfert illégal de population (enfants) des zones occupées
d'Ukraine vers la Fédération de Russie", a ajouté la cour.


Un ordre du tribunal salué par le parquet ukrainien qui parle d'une
décision "historique" et par la la présidence ukrainienne selon qui :
"Ce n'est que le début". Moscou a dénoncé dans la foulée une décision
"insignifiante" et "dénuée de sens"

