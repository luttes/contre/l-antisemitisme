
.. _djerba_memorial_98_2023_05_14:

======================================================================================================================
2023-05-14 **Rassemblement suite à l'attentat antisémite de Djerba, dimanche 14 mai à 16h sur la Place des Fêtes**
======================================================================================================================

- http://www.memorial98.org/2023/05/rassemblement-suite-a-l-attentat-antisemite-de-djerba-dimanche-14-mai-a-16h-sur-la-place-des-fetes.html
- :ref:`djerba_2023_05_14`

Rassemblement antiraciste contre l'antisémitisme ce dimanche 14 mai 2023
à 16h à Place des Fêtes (Paris 19ème-sortie du métro)

En hommage aux victimes de l'attaque antisémite meurtrière qui a eu lieu
ce mardi 9 mai à la Ghriba à Djerba,  et en soutien aux personnes ciblées
par cette attaque,  dont des camarades et ami.es présent.es sur place,
à l'appel de l'ORAAJuive (organisation révolutionnaire antiraciste antipatriarcale juive)
avec le soutien du Réseau d'actions contre l’Antisémitisme et tous les
Racismes (RAAR), de Memorial 98 et de JJR.

