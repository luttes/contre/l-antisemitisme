.. index::
   pair: Manifeste ; RAAR
   pair: Constats ; RAAR
   pair: Actions ; RAAR
   ! Manifeste

.. _raar_manifeste:

====================================================================================
**Manifeste pour un Réseau d’Actions contre l’Antisémitisme et tous les Racismes**
====================================================================================

- https://raar.info/a_propros-manifeste-raar/


.. _raar_constats:

La création de ce réseau s'appuie sur plusieurs constats partagés
======================================================================


La recrudescence de l’antisémitisme sous toutes ses formes
------------------------------------------------------------

La recrudescence de l’antisémitisme sous toutes ses formes, dans un
contexte de montée des racismes et d’aggravation de la crise sociale.
Antisémitisme qui reprend les thèmes traditionnels sur le prétendu
pouvoir des Juifs et le fantasmatique contrôle des médias ; leur situation
de privilégiés, voire leur richesse (comme les allusions à la banque
Rothschild), la protection dont ils bénéficieraient ; l’affirmation
qu’ils seraient des agents d’influence de l’étranger.


Nous constatons que l’antisémitisme a tué plusieurs fois au cours des 16 dernières années
----------------------------------------------------------------------------------------------

Nous constatons que l’antisémitisme a tué plusieurs fois au cours des 16
dernières années :

- Sébastien Selam en 2003 ;
- Ilan Halimi en 2006 à Bagneux ;
- Jonathan, Arié et Gabriel Sandler, et Myriam Monsonego en 2012 à
  Toulouse à l’école Ozar Hatorah ;
- Yohan Cohen, Philippe Braham, François-Michel Saada et Yoav Hattab
  en 2015 à l’Hyper Cacher ;
- Sarah Halimi en 2017
- et Mireille Knoll en 2018, toutes deux à Paris.

À ces meurtres s’ajoutent des viols à caractère antisémite comme celui
de L. à Créteil en 2014.


L’antisémitisme se traduit au quotidien par des paroles ou comportements à caractère discriminatoire
--------------------------------------------------------------------------------------------------------

L’antisémitisme se traduit au quotidien par des paroles ou comportements
à caractère discriminatoire, des harcèlements et agressions à l’école,
à l’université, au travail, dans la rue ou sur les réseaux sociaux.


Nous notons que beaucoup trop de militants ... reproduisent des mécanismes qui les conduisent à minimiser ou à taire les actes et énoncés antisémites
----------------------------------------------------------------------------------------------------------------------------------------------------------

Nous notons que beaucoup trop de partisans de l’émancipation sociale et
des combats quotidiens contre l’exploitation et les discriminations
reproduisent des mécanismes qui les conduisent à minimiser ou à taire
les actes et énoncés antisémites.

Ils ne se soucient pas de cette situation alarmante et vont jusqu’à nier
cette recrudescence de l’antisémitisme.

**Ils se sont absentés des mobilisations**:

- lors de l’assassinat d’Ilan Halimi en 2006,
- des meurtres de l’école Ozar Hatorah à Toulouse en 2012
- et de l’Hyper Cacher en 2015,

**abandonnant ainsi le terrain aux forces réactionnaires**.


Cela a contribué à un brouillage délétère.
Ces mécanismes doivent être analysés pour mieux être détruits.

**Nous combattons l’antisémitisme d’où qu’il vienne**
-------------------------------------------------------

Nous combattons l’antisémitisme d’où qu’il vienne, quels que soient ses
prétextes, ses motivations et ses justifications.

**Notre réseau entend lutter contre l’antisémitisme**
===========================================================

En rappelant que la droite et l’extrême droite, fidèles à leur xénophobie, veulent faire croire que l’antisémitisme viendrait d’« ailleurs »
-------------------------------------------------------------------------------------------------------------------------------------------------

En rappelant que la droite et l’extrême droite, fidèles à leur xénophobie,
veulent faire croire que l’antisémitisme viendrait d’« ailleurs » et ne
plongerait pas ses racines dans plusieurs traditions politiques françaises
vieilles de plusieurs siècles.


En rappelant qu’un antisémitisme diffus est resté bien vivace et qu’il est nourri aujourd’hui par tous les intégrismes religieux
------------------------------------------------------------------------------------------------------------------------------------

En rappelant qu’un antisémitisme diffus est resté bien vivace et qu’il
est nourri aujourd’hui par tous les intégrismes religieux.


En rejetant toute interprétation de l’antisémitisme comme provenant essentiellement de la population musulmane
-------------------------------------------------------------------------------------------------------------------

En rejetant toute interprétation de l’antisémitisme comme provenant
essentiellement de la population musulmane.

**Cette position est instrumentalisée par la droite et l’extrême droite
et reprise dans certains secteurs de la gauche**.


En répertoriant et analysant les formes de banalisation de l’antisémitisme du côté de la gauche et l’extrême gauche
-------------------------------------------------------------------------------------------------------------------------

**En répertoriant et analysant les formes de banalisation de l’antisémitisme
du côté de la gauche et l’extrême gauche**, hier comme aujourd’hui.

Cette banalisation plonge dans une histoire du mouvement ouvrier qui
n’en a jamais été exempte.

**En critiquant les positionnements qui, à gauche, refusent de voir la
réalité de l’antisémitisme et le cantonnent exclusivement à l’extrême droite**.


En dénonçant toute complaisance envers l’antisémitisme, que ce soit au prétexte de l’antisionisme ou de la “lutte contre la finance”
---------------------------------------------------------------------------------------------------------------------------------------

**En dénonçant toute complaisance envers l’antisémitisme, que ce soit au
prétexte de l’antisionisme ou de la “lutte contre la finance”**.

La lutte contre l’antisémitisme doit faire partie intégrante de l’action
contre toutes les formes de racisme.

**Et nous refusons l’injonction selon laquelle il faudrait d’abord adopter
une position sur Israël avant de pouvoir dénoncer la haine des Juifs**.


En participant aux luttes contre tous les autres racismes et en apportant notre solidarité à toutes leurs victimes
-------------------------------------------------------------------------------------------------------------------

En participant aux luttes contre tous les autres racismes et en apportant
notre solidarité à toutes leurs victimes.

**Nous refusons notamment l’opposition entre les actions contre l’antisémitisme
et celles contre l’islamophobie, le racisme antimusulman**.


En nous solidarisant avec les étrangers et les étrangères en butte à la répression de l’État
--------------------------------------------------------------------------------------------------

En nous solidarisant avec les étrangers et les étrangères en butte à la
répression de l’État.


.. _actions_raar:

**Nos actions**
=================

Mettre à la disposition du plus grand nombre des outils (argumentaires, objets de diffusion physiques et numériques) pour agir contre l’antisémitisme vécu au quotidien
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Mettre à la disposition du plus grand nombre des outils (argumentaires,
objets de diffusion physiques et numériques) pour agir contre l’antisémitisme
vécu au quotidien. Y compris dans les luttes et sur les lieux de travail.


Organiser des réunions publiques ou des manifestations, seuls ou avec des mouvements et personnes qui partagent nos préoccupations
-----------------------------------------------------------------------------------------------------------------------------------------

Organiser des réunions publiques ou des manifestations, seuls ou avec des
mouvements et personnes qui partagent nos préoccupations.


Réagir contre des actes, manifestations et publications antisémites et/ou négationnistes
------------------------------------------------------------------------------------------------

Réagir contre des actes, manifestations et publications antisémites et/ou négationnistes.


Participer aux manifestations contre l’antisémitisme en y faisant apparaître nos positions
---------------------------------------------------------------------------------------------

Participer aux manifestations contre l’antisémitisme en y faisant
apparaître nos positions.


Continuer à débattre des aspects historiques et politiques de l’antisémitisme, proposer des espaces de formation
--------------------------------------------------------------------------------------------------------------------

Continuer à débattre des aspects historiques et politiques de l’antisémitisme,
proposer des espaces de formation et analyser de manière plus approfondie
les mécanismes de l’antisémitisme contemporain. !
