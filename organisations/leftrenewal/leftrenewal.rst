.. index::
   ! Leftrenewal

.. _leftrenewal:

===========================================================================
**Left renewal**
===========================================================================

- https://leftrenewal.net/


Articles 2024
================

- :ref:`raar_2024:leftrenewal_fr_2024_05_20`

Articles 2023
================

- :ref:`raar_2023:leftrenewal_fr_2023_12_10`


- https://leftrenewal.frama.io/linkertree/
- https://leftrenewal.frama.io/leftrenewal-info/
