.. index::
   pair: LDH ; Ligue des Droits de l'Homme
   ! LDH

.. _ldh:

======================================================================
**LDH (Ligue des Droits de l'Homme et du citoyen)**
======================================================================

- https://www.ldh-france.org/
- https://piaille.fr/@LDH_Fr
- https://fr.wikipedia.org/wiki/Ligue_des_droits_de_l%27homme_(France)


Description
============

La Ligue des droits de l'Homme (abrégée en LDH, de son nom complet Ligue
française pour la défense des droits de l'Homme et du citoyen) est une
association de défense des droits humains se donnant pour objectif
d'observer, défendre et promulguer les droits de l'Homme au sein de la
République française dans toutes les sphères de la vie publique.

Elle est fondée en 1898 par le sénateur de la Gironde Ludovic Trarieux,
et elle est présidée depuis 2022 par Patrick Baudouin.

À partir de la fin du XXe siècle, la défense du droit des étrangers
fournit la plus grande partie de son activité contentieuse.
