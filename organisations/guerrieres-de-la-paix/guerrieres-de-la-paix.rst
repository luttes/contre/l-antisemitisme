.. index::
   ! Guerrières de la paix

.. _guerrieres_paix:

===========================================================================
|guerrieres| **Les Guerrières de la paix**
===========================================================================

- :ref:`guerrieres:guerrieres_paix`
- https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/
- https://linktr.ee/lesguerrieresdelapaix
- https://www.instagram.com/guerrieres_paix_mvt/
- https://www.lesguerrieresdelapaix.com/
- https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/
- https://www.imarabe.org/fr/evenement-exceptionnel/les-guerrieres-de-la-paix-presentent-le-forum-mondial-des-femmes-pour-la-paix
- https://rstockm.github.io/mastowall/?hashtags=paix,peace,pace&server=https://framapiaf.org

2024
========

- :ref:`raar_2024:raar_guerrieres_paix_2024_01_28`
