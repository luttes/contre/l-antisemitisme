.. index::
   pair: UJFP ; Juifves de caution

.. _jjr_2021_03_09:

======================================================================
2021-03-09 **Interview de Juives et Juifs Révolutionnaires**
======================================================================

- https://engrenagesjournal.wordpress.com/2021/03/09/interview-de-juives-et-juifs-revolutionnaires/


Introduction
===============

Tout d’abord, bonjour et merci de répondre à nos questions. Nous sommes
Engrenages – Journal. Nous voulons faire de notre journal un média
de la démocratie populaire, qui puisse répercuter les différentes
luttes et leur offrir un espace de dialogue et de discussion.

Nous voulons permettre d’avancer vers une synthèse politique des différents
combats, supprimer les murs qui les séparent, et permettre qu’ils se
renforcent mutuellement.

Bonjour, merci de nous avoir invité-es dans ce cadre, nous nous réjouissons
toujours de voir des initiatives de ce type émerger dans notre camp politique.

Nous voudrions savoir si vous pourriez définir rapidement votre organisation. Quelles sont les raisons qui vous ont poussé à la fonder ?
===========================================================================================================================================

**Nous sommes un collectif de personnes issues de la minorité nationale
juive fondée il y a maintenant six ans suite à une prise de conscience
de certains d’entre nous du manque de prise en compte de l’antisémitisme
dans les organisations d’extrême gauche et le mouvement social, dans
lesquels nous militons**.

**Cette prise de conscience a été particulièrement forte suite aux assassinats
antisémites d’Ilan Halimi, de l’école Ozar HaTorah, et de l’Hyper
Casher, ainsi qu’aux différentes réactions au développement de la
mouvance Dieudonné**.

Nos objectifs sont les suivants :

Porter au sein du mouvement révolutionnaire une voix juive autonome afin d’inscrire la
lutte contre l’antisémitisme comme une lutte antiraciste à gauche, lutter
contre le révisionnisme/négationnisme historiques d’où qu’il vienne,
ainsi que de porter une voix progressiste, notamment féministe, antiraciste,
queer, internationaliste et anticolonialiste, au sein de notre propre minorité
nationale afin d’y lutter contre les idées et réflexes réactionnaires
qui y circulent.

**Nous souhaitons l’auto organisation du prolétariat juif diasporique dans
la lutte générale pour le socialisme**.

Pour ce faire nous travaillons à notre échelle à la construction d’une solidarité de classe
inter et intra communautaires entres les différentes minorités opprimées
en France, comme à l’international, car pour nous l’émancipation de
la diaspora juive est d’une part liée à l’éradication du systême
raciste, ce qui suppose la construction d’une lutte commune avec les autres
minorités victimes de racisme : Afrodescendant-es, Roms, Asiatiques,
Arabes, Musulman-nes, Kurdes, Arménien-nes, Amazighs… De même, du fait
de la nature même de l’antisémitisme, l’émancipation juive suppose
**d’opposer la lutte des classes aux théories complotistes**, et de revisibiliser
la classe bourgeoise comme classe dominante, pour contrer le discours raciste
consistant à faire des juifs et des juives la pseudo-classe dominante.

Elle suppose enfin la libération du prolétariat juif, des femmes juives
et des LGBTI+ et handies juives, car la libération sociale des juifs et
des juives suppose la libération de toutes les catégories sociales qui
composent la minorité.

Tout cela, encore une fois, dans l’optique
d’un front unitaire où la lutte contre chaque oppression a pleinement
sa place et où les forces progressistes se coordonnent au lieu de jouer
sur le terrain infertile de la concurrence des luttes et du sectarisme.

L’antisémitisme est il fortement présent en France ? Pour vous, comment se manifeste t-il ? Est il un problème croissant ou en voie de résolution ?
=======================================================================================================================================================

**L’antisémitisme est un élément structurant de la société française,
du roman national français**.

C’est une oppression structurelle, au même titre que tous les racismes.

**C’est une oppression qui peut être violente et brutale comme “douce” ou
subtile avec par exemple la tendance dans la société française à l’indifférence,
à la minimisation voire au déni des manifestations de l’antisémitisme**.

Cela se traduit également par le vocabulaire antisémite qui est pas mal présent dans toutes les
strates de la société, une fétichisation des corps juifs (vivants et
morts) ou une **essentialisation de notre communauté** à des stéréotypes
antisémites qui pour certains datent du moyen âge.

L’ensemble des discours complotistes qui inondent nos sociétés, aussi lunaires soient-ils, ramènent
irrémédiablement vers l’antisémitisme et le fameux “complot mondial
juif”.

Cela se traduit aussi par des discriminations à l’embauche
ou au logement, à des injonctions au positionnement sur Israël ou à
l’inverse a une injonction a l’adhésion au roman national sous peine
d’être considéré-e comme une cinquième colonne dans les deux cas,
et bien sûr par des agressions voire des assassinats.

Cet antisémitisme qui n’a jamais disparu connaît aujourd’hui un regain de puissance
qui va de paire avec la montée en puissance des thèses complotistes, la
banalisation d’un discours ultra réactionnaire et raciste au sein de la
société et de l’assaut généralisé envers l’ensemble des minorités
nationales qui en découle.

Le rapport annuel de la CNCDH sur le racisme l’illustre d’ailleurs très bien :
quand un racisme spécifique est en augmentation par exemple l’antisémitisme,
c’est l’ensemble des thèmes racistes (négrophobie, islamophobie, romophobie,
asiaphobie etc) qui augmentent. La résolution de la question antisémite ne peut donc
se faire que conjointement avec la lutte des autres formes de racismes et
donc qu’aux cotés de nos camarades racisé-es.

Est-ce que, pour vous, il existe une symétrie entre l’antisémitisme (actuel ou passé) et des conceptions comme l’islamo-gauchisme ?
=======================================================================================================================================

On peut rapprocher certains traits de la vague islamophobe actuelle et l’antisémitisme.

Il s’agit par exemple de deux processus de racialisation qui se fondent sur des
assignations racistes structurées à partir d’une identité religieuse,
mais qui sert avant tout de marqueur au processus de racialisation : le fait
que les personnes qui en sont la cible soit croyante ou non, pratiquante ou
non importe en réalité peu au producteur de ces deux formes de racisme,
puisque les « juifs » et les « musulmans » ciblés par ces discours
racistes le sont sur des bases raciales et non sur la base de leur adhésion
au judaïsme ou à l’islam. La chasse à « l’islamogauchisme » actuel
joue le même rôle que joue celle, historique, au « judéobolchévisme
»/ »judéomaçonisme » : il s’agit de deux éléments qui vient à
désigner dans le discours nationaliste « l’antifrance » auquel le « pays
réel » devrait s’opposer en réalisant la « révolution nationale »
fasciste. C’est assez visible dans la reprise par Macron, Zemmour, Renaud
Camus de la référence à Maurras, qui déjà désignait les « 4 états
confédérés » que représentaient l’antiFrance : il s’agit finalement
d’une adaptation contemporaine du même thème raciste et antisémite. Il est
à noter également qu’on retrouve ce même thème du « grand remplacement
» diffusée par des auteurs de la droite sioniste comme Bat Yeor, mais aussi
une forme d’adaptation des discours forgés par les suprématistes blancs
à l’international, notamment aux Etats Unis sur le « marxisme culturel
» et les attaques par exemple contre l’Ecole de Francfort, composée
de marxistes juifs, accusée de diffuser des idées qui fragmenteraient la
société blanche aux Etats Unis.

Mais contrairement à ce que certains
affirment, à droite comme à gauche, la seconde offensive n’a pas effacée
la première : derrière les discours sur ‘l’islamo-gauchisme » on
retrouve régulièrement la thèse antisémite du « grand remplacement » :
antisémite, car les théoriciens nationalistes qui évoquent ce prétendu
« complot » visant à « remplacer les français de souche » (sic) par
les « musulmans, les noirs et les arabes » désignent les juifs comme
les auteurs de ce « grand remplacement » ( parfois à travers des mots
codes comme « élites cosmopolites » par exemple…)

Sans parler de
« symétrie », on voit donc qu’il existe des traits communs. Il y a
cependant des différences notables : les juifs et juives sont définis par
les antisémites à la fois comme l’élément dissolvant de la société
mais aussi comme l’incarnation du pouvoir, par le biais des théories du
complot. On leur reproche à la fois d’être partout mais d’être aussi
« cachés ». Les musulmans, eux, sont définis comme une menace exogène,
mais ne sont pas considérés comme exerçant le pouvoir, et c’est d’être
prétendument trop visible qui leur est reproché, même si certains thèmes
peuvent converger comme l’accusation de « dissimulation » (Taqiyya).

Pensez vous que le problème de l’antisémitisme soit sous-estimé dans les organisations d’extrême-gauche ?
===============================================================================================================

Oui.

Très souvent,
l’antisémitisme n’est pas considéré comme un sujet à part entière,
mais comme subordonné au conflit israélo-palestinien, voire comme réaction
plus ou moins naturelle face à un « philosémitisme » ou encore à
un problème du passé qui ne serait plus d’actualité. Selon la théorie
qui le subordonne au conflit israélo-palestinien, de fait antisémite,
les responsables de l’antisémitisme se trouveraient donc parmi ceux qui
en sont victimes ( les juifs, le CRIF, les “sionistes” qui seraient plus
ou moins omniprésents et tout puissants, etc.).

C’est une théorie aussi
raciste que ne l’est la théorie qui accuse les musulmans/Le CCIF/ les «
islamistes » d’être responsable de l’islamophobie dont les musulmans ou
désignés comme tels sont victimes

La conséquence pratique est donc de
laisser de côté le combat contre l’antisémitisme et d’invoquer la lutte
contre le colonialisme israélien pour éluder la question (généralement
sans faire avancer d’un iota la cause palestinienne d’ailleurs) tout en
véhiculant des poncifs antisémites sur la communauté juive. Parallèlement,
cela renforce le sentiment d’isolement et l’angoisse de la minorité juive,
qui se replie alors vers sa frange la plus réactionnaire face a une gauche
qui n’apparaît pas comme une famille politique favorable aux juifs. Une
grande partie des militant-es juifves à gauche restent ainsi au placard quand
a leur judéité à cause de ce non traitement de l’antisémitisme dans
notre camp politique et aux discours assimilationnistes et antisémites qui y
pullulent. Discours assimilationniste hérité à ce propos de l’idéologie
nationaliste dont elle se veut la face « progressiste » et qui partagé par
les grandes figures révolutionnaires du 19ème et 20ème fut un aveuglement
dramatique sur l’analyse politique de la question juive et la lutte contre
l’antisémitisme. Aveuglement dont l’héritage pèse encore lourdement
dans le camp du socialisme aujourd’hui dans sa relation à notre diaspora.

Les camarades qui ont écrit la brochure Le Juif de Schrödinger illustrent
d’ailleurs parfaitement la situation paradoxale dans laquelle se trouvent les
juifves à gauche et de la violence qu’iels y rencontrent.

Nous ne sommes toléré-es, en tant que juifves, dans notre camp politique que si nous
sommes des « bon juifves ». C’est a dire que nous servions de caution
juive justifiant toutes les prises de positions antisémites à gauche,
que nous nions notre judéité pour un assimilationnisme forcé, que nous
prouvions notre non allégeance a des entités bien souvent fantasmées que
sont l’état israélien et le « sionisme », que nous manifestions en
permanence des gages de notre loyauté à nos organisations politique tout
en nous effaçant en son sein sous peine d’être de nouveau le mauvais
juif qui complote et trahis forcement la cause, car rien n’est acquis
quand on est juifve à gauche.


.. _ujfp_juifves_de_caution_2021_03_09:

L’Union Juive Française Pour la Paix, juifves de caution qui servent la soupe aux antisémites en France
===========================================================================================================

**L’ Union Juive Française Pour la Paix en est d’ailleurs un exemple caricatural
de ces juifves de caution qui servent la soupe aux antisémites en France
et qui finalement portent eux même un discours antisémite d’une rare
violence envers la communauté juive et sa lutte pour l’émancipation**.

Penser également qu’une
idéologie qui imprègne l’ensemble de la société s’arrêterait à la
frontière de la gauche ou des mouvements révolutionnaires est absurde.

Si l’on étudie sans complaisance l’histoire du mouvement ouvrier, de la
gauche et de l’antisémitisme, on s’aperçoit qu’il existe, à toutes
les époques, une influence de l’antisémitisme. Cela ne signifie pas que
celui-ci fasse partie du cœur idéologique de la gauche ou du mouvement
révolutionnaire, mais que les uns et les autres ne sont pas immunisés
par nature contre l’influence de cette idéologie dominante. Il en va
de même avec les discours négationniste et révisionnistes qui sont
loin d’être l’unique apanage des franges réactionnaires, il suffit
de voir les discours de justifications ou de réécriture des faits quant
à l’antisémitisme soviétique avec par exemple le complot des blouses
blanches, les purges antisémites staliniennes, la chasse aux ”sionistes”
et aux “cosmopolites”, les discours antisémites qu’on trouve dans les
milieux “antisionistes” avec par exemple le slogan “pour la séparation
du Crif et de l’État”, des discours antisémites de gauche relayés par
des plateformes tel qu’Acta ou Lundimatin pour n’en citer que quelque
unes, la défense des thèses antisémite du Pir, l’influence encore vive
des thèses négationnistes de Bordiga dans les sphères marxistes pour
ne citer que quelques exemples d’une très longue liste des casseroles
qu’on traîne à gauche en matière d’antisémitisme et d’héritage
antisémite.

En fin de compte **on attend de nous, juifves, que nous
cessions d’exister en tant que tel à gauche, comme dans l’ensemble de
la société. Hors il n’en n’est pas question**.

**C’est pourquoi Juives et Juifs Révolutionnaires avec nos camarades des Juifves VNR, de l’Union
Juive pour la Résistance et l’Entraide (UJRE) et du Réseau d’Action contre
l’Antisémitisme et tout les Racismes (RAAR)** nous mettons un point d’honneur
à porter une voix juive progressiste dans les luttes sociales, dans notre
communauté et à constituer une autodéfense juive antiraciste populaire
face à l’antisémitisme et ses partisan-nes.

Tant que la question de la lutte contre l’antisémitisme aux cotés de la
communauté juive ne deviendra pas un enjeu de la lutte antiraciste dans
notre camp politique, **la gauche ne sortira pas du bourbier antisémite
dans lequel elle patauge depuis trop longtemps**.

Un certain nombre de politiciens se sont placés sur le créneau de la lutte contre l’antisémitisme (Valls, par exemple), que pensez-vous de leur action ?
===========================================================================================================================================================

La politique a horreur du vide, hors **la récupération de la lutte contre
l’antisémitisme par les politiciens bourgeois n’a pu se faire que parce
que la gauche de combat à globalement déserté cette lutte et le travail
antiraciste spécifique en direction de la communauté juive**.

La droite, qui a historiquement joué le rôle de vecteur
de l’antisémitisme, a très bien perçu l’opportunité stratégique. Si
la gauche radicale et la gauche avaient tenu le terrain de la lutte contre
l’antisémitisme, plutôt que de l’abandonner en considérant qu’il
s’agissait au mieux de quelque chose de résiduel, au pire d’un prétexte
invoqué par les réactionnaires, ces derniers n’auraient jamais pu
imposer leur discours. Nous ne nous faisons d’ailleurs aucun faux espoir
sur le traitement de l’antisémitisme par les politiciens bourgeois
qui ne se penchent sur le sujet qu’à des fins d’instrumentalisation
politique. Derrière l’antisémitisme se cache le capital, la fin de
l’un ne vas pas sans l’autre.

Comment résoudre définitivement la problème de l’antisémitisme ? Est-ce possible ?
======================================================================================

Avant de répondre nous vous demandons de faire attention aux expressions
utilisées, « résoudre définitivement un problème » peut sonner
douloureusement, surtout quand on parle d’antisémitisme.

[A la suite de l’arrivée de ce document,
nous avons d’ailleurs présenté nos excuses pour cette tournure de phrase
maladroite et profondément stupide. Elle illustre bien qu’il nous reste
un chemin à arpenter, nous aussi, sur la voie de l’amélioration. NdlR]

Bref. **Pour nous la lutte contre l’antisémitisme passe par la démonstration
de la fraternité des opprimé.es, par la démonstration, théorique, mais
surtout par le fait, que les travailleurs et travailleuses juifs, musulmans,
rroms, etc. vivent des situations d’oppression qui, sans être identiques,
leur sont communes : le capitalisme, le racisme, le sexisme, etc**.

Surtout,
l’idéologie nationale qui sert de socle aux États modernes suppose la
définition d’un groupe majoritaire par exclusion des minorités nationales
Cette définition peut, selon le contexte, être plus ou moins agressive,
mais le racisme en sera toujours une conséquence au moins latente. Une
société sans racisme c’est donc nécessairement une société sans nation,
sans patriarcat, sans capitalisme, donc qui aura connu une transformation
révolutionnaire globale.

Auriez-vous un message particulier à transmettre à nos lecteurs et lectrices ?
========================================================================================

Ralliez les luttes antiracistes, soutenez
matériellement et politiquement les combats des minorités nationales dont
la communauté juive, faites vous les relais de ces voix qui luttent.

**Formez vous sur l’antisémitisme** et soyez de réel allié-es en luttant contre
l’antisémitisme partout ou celui ci apparaît, chez vous et dans vos
organisations y compris. On espèrent vous voir nombreux-ses prochainement
dans les luttes antiracistes. Car s’il fallait le répéter une fois de
plus sans un front antiraciste puissant, coordonné et dont la gauche de
combat doit être partie prenante, pas de perspectives révolutionnaires.
