.. index::
   pair: JJR ; Juives et Juifs Révolutionnaires
   pair: Mediapart ; Juives et Juifs Révolutionnaires
   ! JJR

.. _jjr:

======================================================================
|jjr| **JJR (Juives et Juifs Révolutionnaires)**
======================================================================

- https://juivesetjuifsrevolutionnaires.wordpress.com/
- https://kolektiva.social/@jjr (Mastodon)
- https://blogs.mediapart.fr/juives-et-juifs-revolutionnaires
- https://www.instagram.com/Juifvesrevolutionnaires/
- https://jjr.frama.io/juivesetjuifsrevolutionnaires
- :ref:`jjr:jjr`

.. figure:: images/jjr_logo.png
   :align: center


Juives et juifs révolutionnaires (JJR) est un collectif réunissant des
personnes juives de gauche radicale luttant contre l'antisémitisme,
notamment dans les espaces au sein desquels elles militent.


.. toctree::
   :maxdepth: 5

   actions/actions
