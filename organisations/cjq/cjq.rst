.. index::
   pair: CJQ ; Collages Judéités Queer
   ! CJQ

.. _cjq:

======================================================================
**Collages Judéités Queer** (CJQ)
======================================================================

- https://www.instagram.com/collages.judeites.queer/

Description
=============

Colleur-euses juif-ves LGBTQIA+,
en questionnement, queer, en lutte contre tous systèmes d'oppression
et de domination.

Paris/banlieues▫️
