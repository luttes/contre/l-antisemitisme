.. index::
   ! Goysplaining

.. _goysplaining_2024_01_19:

================================================================
2024-01-19 **goysplaining** par Volia Vizeltzer |Volia|
================================================================

- :ref:`sender_vizel`
- https://www.instagram.com/p/C2Sbj8otP9B/?img_index=1

Annonce par Gregory Gutierez |Gregory Gutierez| sur mastodon
===============================================================

- https://pouet.chapril.org/@Greguti/111804121498886649

Excellente publication de :ref:`Sender Vitz (NDLR, alias Volia Vizeltzer) <sender_vizel>`
sur Instagram à propos du #goysplaining en une dizaine de vignettes.


Commentaire de Volia Vizeltzer
==================================

- https://www.instagram.com/p/C2Sbj8otP9B
- :ref:`sender_vizel`

J'ai du mal a dessiner en ce moment alors voilà une petite BD sans dessins.
Allez, bonne lecture.


Le goysplaining. qu'est-ce que c'est ? (1/10)
===============================================

.. figure:: images/goysplaining_quest_ce_que_cest.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=1

Le **goysplaining**, qu'est-ce que c'est ?

Les citations à suivre sont certes reformulées de mémoire à partir
de vrais échanges...

--------------


C'est ce moment où un goy **t'explique ce que c'est que d'être juifve** (2/10)
==============================================================================

.. figure:: images/goysplaining_ce_moment.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=2

C'est ce moment où un goy **t'explique ce que c'est que d'être juifve**.

goysplaining

    Pourquoi tu te dis juif alors que t'es même pas religieux ?

--------------


Ce que tu devrais dire **en tant que juifve** (3/10)
=======================================================

.. figure:: images/goysplaining_ce_que_tu_devrais_dire.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=3

Ce que tu devrais dire **en tant que juifve**

goysplaining

    Non mais accuser la gauche d'antisémitisme, ça aide la droite,
    c'est contre-productif.

--------------


Ce que tu devrais faire **en tant que juifve** (4/10)
============================================================

.. figure:: images/goysplaining_ce_que_tu_devrais_faire.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=4


**en tant que juifve**

goysplaining

    Faut que vous parliez avec des gens qui vous insultent. Vous devez
    vous unir en fait.

--------------

Ce qui est ou n'est pas de l'antisémitisme (5/10)
========================================================


.. figure:: images/goysplaining_ce_qui_est_ounnest_pas.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=5


Ce qui est ou n'est pas de l'antisémitisme.

goysplaining

    Mais c'était pas antisémite, moi comme non-vacciné, je me sentais
    discrimné aussi...

--------------


Comment tu devrais lutter contre l'antisémitisme (6/10)
===============================================================

.. figure:: images/goysplaining_comment_tu_devrais_lutter.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=6

**Comment tu devrais lutter contre l'antisémitisme.**

goysplaining

    Non mais la meilleure manière de lutter contre l'antisémitisme
    c'est de soutenir les palestiniens.

--------------

Ce qui fait que y'a de l'antisémitisme (7/10)
========================================================

.. figure:: images/goysplaining_ce_qui_fait_qu_il_n_y_a_pas_d_antisememitisme.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=7

**Ce qui fait que y'a de l'antisémitisme.**

goysplaining

    Moi je comprends les gens qui en ont marre de la Shoah, on parle que
    de ça...

--------------


Ce qu'est ta propre histoire, celle des juifves (8/10)
===========================================================

.. figure:: images/goysplaining_ce_qu_est_ta_propre_histoire.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=8


Ce qu'est ta propre histoire, celle des juifves .

goysplaining

    Bah si, **on peut comparer le Hamas au groupe Manouchian**...

--------------

Ce qu'est réellement ton propre vécu (9/10)
=======================================================

.. figure:: images/goysplaining_ce_qu_est_ton_propre_vecu.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=9

Ce qu'est réellement ton propre vécu

goysplaining

    Non mais les juifs, vous êtes pas assez noirs ou arabes pour être
    vraiment discriminés.

--------------


**Faites preuve d'humilité, plutôt que de parler, ecoutez...** (10/10)
=========================================================================

.. figure:: images/goysplaining_faites_preuve_d_humilite.webp
   :width: 400

   https://www.instagram.com/p/C2Sbj8otP9B/?img_index=10

|Volia|

    PS: c'est pas parce que vous avez trouvé une ou des juifves d'accord
    avec vous **que vous pouvez nous goysplainer la vie**.

    Faites preuve d'humilité, **plutôt que de parler, ecoutez**...
