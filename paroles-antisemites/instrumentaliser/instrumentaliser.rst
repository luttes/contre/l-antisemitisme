

.. _poncif_instrumentaliser:

=====================================================================================================
2023-11-05 **Instrumentaliser/l'antisémitisme existe ... à gauche** par Volia Vizeltzer |Volia|
=====================================================================================================

- :ref:`sender_vizel`
- https://www.instagram.com/p/CzRlDosrFVu/?img_index=1


Commentaire de Volia Vizeltzer
===================================

- :ref:`sender_vizel`

Ressenti du mood en ce moment.
S'y reconnaîtra qui s'y reconnaîtra.
**C'est pas pour être méchant c'est pour pointer un problème.
Allez, prenez soin de vous toustes.**

Merci de la lecture.


L'antisémitisme exist... (1/9)
==================================



.. figure:: images/instrumentaliser_1.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=1

|Volia| un·e juifve

   L'antismétisme exist...

|antisem| un antisémite

   Mais oui ! T'as vu comment Israël et la droite vous instrumentalisent ?!
   Ca dessert tellement la vraie lutte contre l'antisem-...

--------------

aussi à gauche (2/9)
==========================

.. figure:: images/instrumentaliser_2.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=2

|Volia| ... aussi à gauche


|antisem| un antisémite

    ... attends

    ...

    t'as dit quoi là ? ...

--------------


Bah que ... NON MAIS C'EST BON LA !!! (3/9)
===============================================

.. figure:: images/instrumentaliser_3.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=3

|Volia|

    Bah que ...

|antisem| un antisémite

    NON MAIS C'EST BON LA !!!

    C'est toujours pareil avec vous les juifves !

--------------

Vous êtes toujours là à nous critiquer à gauche ! Vous comprenez rien, on est pas antisémite, on est AN-TI-SIO-NISTE !!! (4/9)
==================================================================================================================================

.. figure:: images/instrumentaliser_4.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=4

|antisem| un antisémite

    Vous êtes toujours là à nous critiquer à gauche !

    Vous comprenez rien, on est pas antisémite, on est AN-TI-SIO-NISTE !!!


--------------

Mais c'est vous là qui rendez les gens antisémites en fait ! (5/9)
=======================================================================

.. figure:: images/instrumentaliser_5.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=5


|antisem| un antisémite

    Mais c'est vous là qui rendez les gens antisémites en fait !

    A vous plaindre tout le temps ! Shoah par ci, Shoah par là...

    On en a marre vous comprenez pas ?! Vous nous saoulez !!!

--------------


Y'en a toujours que pour VOUS ! On vous détesterait peut-être pas si vous êtiez pas tous des gros connards racistes !(6/9)
================================================================================================================================

.. figure:: images/instrumentaliser_6.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=6

|antisem| un antisémite

    Y'en a toujours que pour VOUS !

    On vous détesterait peut-être pas si vous êtiez pas tous des gros
    connards racistes !

    Vraiment qu'une bande de gros privilégiés vous !

--------------

On accuse pas les gens d'antisémites comme ça ! C'est hyper blessant ! Vous imaginez un peu la violence que MOI je subis là ?!! (7/9)
=========================================================================================================================================

.. figure:: images/instrumentaliser_7.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=7

|antisem| un antisémite

    On accuse pas les gens d'antisémites comme ça ! C'est hyper blessant !

    Vous imaginez un peu la violence que MOI je subis là ?!!

    Bien sûr que non ! Vous savez pas ce que c'est de souffrir VOUS là !

--------------


T'sais quoi, **moi je les comprends le Hamas** ! Après tout ce que VOUS avez fait... (8/9)
===============================================================================================


.. figure:: images/instrumentaliser_8.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=8

|antisem| un antisémite

    T'sais quoi, **moi je les comprends le Hamas !**

    Après tout ce que VOUS avez fait...

    Bande de colons NAZIS !

--------------

Bon...voilà qui est fait (9/9)
===================================

.. figure:: images/instrumentaliser_9.webp
   :width: 400

   https://www.instagram.com/p/CzRlDosrFVu/?img_index=9

|Volia|

    Bon...

    ...

    Voilà qui est fait...
